//  APIConstants.swift
//  BusinessDirectory
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 Shubham Dhingra All rights reserved.


import Foundation

internal struct APIConstants {
    
    //franciscanecarebasePath
    static let basePath       = "http://ecare.franciscanecare.com/api/"
    static let imgbasePath    = "http://ecare.franciscanecare.com/"
    static let basePathExecutive  = "http://webapi.theempcare.com/api/"
    static let busGpsurl = "http://ecare.franciscansolutions.com/VehicleDTL/Vehicle?RegNumber="

    
    //static let basePath     = "http://webapi.palmboard.in/api/"
    //static let imgbasePath  = "http://webapi.palmboard.in/"
    static let leaveSetting   = "leave/setting"
    static let leaveRequest   = "Leave/Request"
    static let leaveAction    = "Leave/Action"
    static let leaveReport    = "Leave/report"
    static let leaveDelete    = "leave/delete"
    static let searchLeave    = "leave/Search"
    static let changePassword = "user/ChangePassword"
    static let myClasses      = "Teacher/MyClass"
    static let sendMessage    = "Message/SendMessage"
    static let appSMS          = "SMSService/AppSMS"
    static let leaveStatus    = "leave/status"
    static let language       = "language"
    static let login          = "vendor/login"
    static let myProfile      = "user/MyProfile"
    static let status         = "Status"
    static let message        = "Message"
    static let errorCode      = "ErrorCode"
    
    static let userDetails     = "user/Details"
    static let  verifyuser     = "user/verify"
    static let validate        = "user/validate"
    static let registerDevice  = "user/RegisterDevice"
    static let SchoolCode      = "/school/DTL"
    static let uploadCoverImg  = "Upload/CoverImg"
    static let verifyRegisterUser = "/VarifyUser"
    static let  logout         = "user/logout"
    static let appVersion      = "/School/AppVersion"
    static let getSchoolList   = "school/lst"
    static let verifyBarCode   = "Library/VerifyBarcode"
    static let recentUpdate    = "School/CurrentUpdates"
    static let pushSetting     = "PushNotification/AppSeting"
    static let setSetting      = "PushNotification/SetSetting"
    static let generateToken   = "School/GenerateToken?"
    static let attendanceSummary = "Academic/AttendanceSummury"

    //Appointment Module API
    
    //Use to identity face
    static let teamCareBasePath        = "https://www.teamcareonline.com/api/api/"
    static let teamCarBasePath2        = "https://www.esmartguard.com/api/Ecare/"
    static let teamCarBasePath3        = "http://api.teamcareonline.com/api/"

    static let teamCareImageBasePath   = "https://www.teamcareonline.com/api/"
    static let smartGuardImageBasePath = "https://www.esmartGuard.com/"
    
    //Escort Module
    static let varifyUserEscort         = "varifyUserEscort"
    static let escortIdentityList       = "GetIdentityList"
    static let getEscortList            = "GetEscortList"
    static let escortListApprove        = "EscortListApprove"
    static let escortSaveUser           = "SaveUser"
    static let registerEscort           = "RegisterEscort"
    static let activateDeactivateEscort = "EscortActivateDeactivate"
    
    //Other Module API
    static let smartGuardBasePath      = "https://www.esmartguard.com/api/DigitalGuard/"
    static let testUploadface          = "testuploadface"
    static let GetIdentityList         = "GetIdentityList"
    static let saveParents             = "SaveParents"
    static let sendSms                 = "SendSms"
    static let getPurposeList          = "GetPurpousNew"
    static let GetWhom                 = "GetWhom"
    static let CheckLimit              = "CheckLimit"
    static let saveAppointment         = "SaveAppointment"
    static let getParentDetails        = "getParentsDetails"
    static let checkLimit              = "CheckLimit"
    static let maxAppointTogether      = "getMaxAppointTogether"
    static let activeDeactivateParents = "ActiveDeactiveParents"
    static let varifyMobileNo          = "VarifyMobileNo"
    static let eSmartGuardAppLink      = "https://itunes.apple.com/us/app/esmartguard/id1423013676?ls=1&mt=8"
    static let eCareProAppLink         = "https://itunes.apple.com/us/app/e-carepro/id1435475202?ls=1&mt=8"
    static let sendBulkSms             = "/SMSService/SendSMS"
    static let matchmultifaces        = "matchmultifaces"
    static let userShortProfile       = "user/Profile"

    
    //sms module
    static let inboxMsg       = "Message/Inbox"
    static let sentMsg        = "Message/SentMessage"
    static let messagesFrom   = "Message/From"
    static let messagesSearch = "Message/SearchInbox?"
    
    static let messageDTL    = "Message/DTL"
    static let sentMsgDTL    = "Message/SentMsgDTL"
    static let replyMessage  = "Message/ReplyMessage"
    static let smsType       = "/Message/SMSType"
    
    
    //classAttendance
    static let teacherAssignedClass = "/Teacher/ClassToMarkAtt"
    static let studentListClassWise = "/Teacher/StudentListToMark"
    static let savedAttendance      = "/Teacher/PostAttendance"
    static let subjectClassWise     = "/Teacher/ClassSubject"
    
    //weeklyplan
    static let IsweeklyPanCreated      = "WeeklyPlan/hasCreated"
    static let createWeeklyPlan        = "WeeklyPlan/Post"
    static let GetWeeklyPlans          = "WeeklyPlan/Report"
    static let weeklyPlanDTL           = "WeeklyPlan/DTL"
    static let weeklyPlanTakeAction    = "WeeklyPlan/TakeAction"
    static let weeklyPlanForStudent    = "WeeklyPlan/ForStudent"
    static let weeklyPlanModify        = "WeeklyPlan/Modify"
    //academics
    static let studentSubject          = "Academic/StudentSubject"
    static let surveyList              = "Survey/List"
    static let surveyQuestions         =  "Survey/Questions"
    static let postAnswer              = "Survey/PostAnswer"
    static let surveyResult            = "Survey/Result"
    static let messageSetting          = "/Message/Setting"
    static let surveyQueResponse       = "/Survey/QueRes"
    static let attRpt                  = "Teacher/AttRpt"
    static let staffType               = "Teacher/StaffType"
    static let executiveAttendance     = "Report/ExecutiveAttendance"
    static let payslip                 = "Teacher/Payslip"
    static let allSyllabus             = "Academic/ALLSyllabus"
    static let staffSyllabus           = "Academic/StaffSyllabus"
    static let thoughts                = "thoughts/lst"
    static let likeThought             = "like/Thought"
    static let thoughtsCreate          = "Thoughts/Create"
    static let questionLst             = "Question/lst"
    static let likeQuestion            = "Like/Question"
    static let questionDeleteAns       = "Question/DeleteAns"
    static let questionPostAnswer      = "Question/PostAnswer"
}

enum Keys : String{
    
    //send Message
    case SchoolCode
    case key
    case CountryCode
    case MsgType
    case UserID
    case UserType
    case Subject
    case Body
    case Device
    case IPAddress
    case Attachment
    case Recipient
    case SchCode
    case StID
    case FromDate
    case TillDate
    case Duration
    case Reason
    case LvID
    case pg
    case RoleName
    case query
    case Status
    case ord
    case Action
    case StName
    case LeaveDate
    case SearchBy
    case ClassID
    case NewPassword
    case username
    case password
    case DeviceID
    case DeviceType
    case Uid
    case ImgUrl
    case PrId
    case DocPath
    case Document
    case MobileNo
    case EmailId
    case IDName
    case Name
    case ImageTamplate
    case IsParent
    case Relation
    case LeaveID
    case sno
    case msg
    case pid
    case tomeet
    case ExpectedDate
    case NoOfPeople
    case ExpectedTimeSlot
    case Purpous
    case ApproxTime
    case AdditionalPersonList
    case OtherPurpose
    case isActive
    case toMeet
    case SenderID
    case MsgID
    case SenderType
    case AdmissionNo
    case StUInfo
    case UserDetails
    case Sid
    case Class
    case FatherName
    case MotherName
    case PhotoPath
    case ContectMob
    case ImageUrl
    case body
    case Imgurl
    case ReceiverID
    case ReceiverType
    case ClassIDs
    case RecipientType
    case filter
    case Barcode
    case StudentAtt
    case SMSType
    case FromUserID
    case FromUserType
    case Data
    case isUnicode
    case SMS_Prvd_ID
    case SMS
    case MDLID
    case SubID
    case EndDate
    case ViewType
    case WPID
    case TeacherID
    case PlnID
    case SubIDs
    case PlanDate
    case SurID
    case HalfdayDTL
    case ApplType
    case QueID
    case isReport
    case AttDate
    case StaffType
    case isSubject
    case all
    case dir
    case ThID
    case isLike
    case Quotation
    case Author
    case CatID
    case QID
    case AnID
    case Answer
}
enum Validate : String {
    
    case none
    case success = "0"
    case failure = "1"
    case errorCodeTwo = "2"
    case notFound = "404"
    case invalidAccessToken = "401"
    case adminBlocked = "403"
    
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success ,.errorCodeTwo:
            return message
            
        case .failure :
            return message
            
        case .notFound :
            return message
            
        case .invalidAccessToken :
            return message
            
        case .adminBlocked:
            return message
        default:
            return nil
        }
        
    }
}

enum Response {
    case success(AnyObject?)
    case failure(String?)
}

typealias OptionalDictionary = [String : Any]?

struct Parameterss {
    
    static let sendMessage  : [Keys]   = [.SchoolCode , .key , .CountryCode , .MsgType , .UserID , .UserType , .Subject , .Body , .Device , .IPAddress ,.Recipient, .Attachment , .ClassIDs , .RecipientType]
    static let leaveSetting : [Keys]   = [.SchCode , .key , .UserID , .UserType]
    static let appVersion   : [Keys]   = [.SchCode , .key]
    static let leaveRequest : [Keys]   = [.SchoolCode , .key , .StID , .UserID , .UserType, .FromDate , .TillDate , .Duration , .Reason , .HalfdayDTL , .LeaveID]
    static let leaveStatus  : [Keys]   = [.SchCode , .key ,.UserID , .UserType ,.pg]
    static let deleteLeave  : [Keys]   = [.SchCode , .key , .LvID , .UserType]
    static let leaveReport  : [Keys]   = [.SchCode , .key , .UserID , .Status , .ord , .ApplType , .pg]
    static let leaveAction  : [Keys]   = [.SchoolCode , .key , .LvID , .Action , .UserID , .UserType]
    static let searchLeave  : [Keys]   = [.SchCode , .key , .UserID , .Status , .ord , .SearchBy , .pg , .StName , .ClassID , .LeaveDate , .UserType]
    static let myClasses      : [Keys] = [.SchCode , .key , .UserID]
    static let changePassword : [Keys] = [.SchoolCode , .key , .UserID , .UserType , .NewPassword]
    static let userDetails    : [Keys] = [.SchCode  , .key , .UserID  , .UserType]
    static let myProfile      : [Keys] = [.SchCode , .key  , .UserID , .UserType]
    static let verifyUser     : [Keys] = [.SchCode , .key , .username]
    static let validate       : [Keys] = [.SchoolCode , .key , .username, .password]
    static let registerDevice : [Keys] = [.SchoolCode , .key , .DeviceID , .DeviceType , .UserID , .UserType ]
    static let schoolCode  : [Keys] = [.SchCode , .key]
    static let getIdentityList : [Keys] = [.SchoolCode]
    static let saveParents : [Keys] = [.sno , .SchoolCode , .Uid , .Name , .DocPath , .Document , .MobileNo , . EmailId , .IDName , .ImageTamplate , .PrId , .ImgUrl , .IsParent , .Relation]
    
    static let varifyUser : [Keys] = [.SchoolCode , .Uid]
    static let sendSms : [Keys] = [.SchoolCode , .MobileNo , .msg]
    static let getPurposeList : [Keys] = [.SchoolCode]
    static let GetWhom : [Keys] = [.SchoolCode , .pid]
    static let checkLimit : [Keys] = [.SchoolCode , .tomeet , .ExpectedDate]
    static let saveAppointment : [Keys] = [.SchoolCode ,.NoOfPeople , .Uid , .ExpectedDate , .ExpectedTimeSlot , .tomeet , .Purpous , .ApproxTime , .AdditionalPersonList , .OtherPurpose]
    static let getParentDetails : [Keys] = [.SchoolCode , .PrId]
    static let getMessages : [Keys] = [.SchCode , .key , .UserID , .UserType , .pg]
    static let activeDeactivateParents : [Keys] = [.SchoolCode , .PrId, .Uid , .isActive]
    static let messageFrom : [Keys] = [.SchCode , .key , .SenderID, .SenderType , .UserID , .UserType , .pg]
    static let messageSearch : [Keys] = [.SchCode , .key , .SenderID, .SenderType , .UserID , .UserType , .pg, .query]
    
    static let messageDTL : [Keys] = [.SchCode , .key ,.MsgID , .SenderID, .SenderType , .UserID , .UserType]
    static let escortList : [Keys] = [.AdmissionNo , .SchoolCode]
    static let escortMainBody : [Keys] = [.StUInfo , .UserDetails]
    static let registerEscort : [Keys] = [.body]
    static let escortUserDetails : [Keys] = [.sno , .SchoolCode , .Uid , .Name ,.Document , .MobileNo , .EmailId , .IDName , .PrId, .ImageTamplate, .Imgurl]
    static let stuInfo : [Keys] = [.Sid , .PrId , .StName , .AdmissionNo , .Class , .FatherName , .MotherName , .PhotoPath , .ContectMob , .SchoolCode , .Relation , .isActive , .Uid]
    static let varifyEscort : [Keys] = [.SchoolCode , .PrId , .StName , .AdmissionNo , .Class, .FatherName , .MotherName , .PhotoPath , .ContectMob, .ImageTamplate , .Uid, .Imgurl]
    static let EscortActivateDeactivate : [Keys] = [.SchoolCode , .AdmissionNo , .isActive , .Uid]
    static let sentMsgDTL : [Keys] = [.SchCode , .key , .MsgID , .UserID , .UserType]
    static let replyMessage : [Keys] = [.SchoolCode , .key , .CountryCode , .MsgID , .MsgType , .UserID , .UserType , .Body , .Device , .IPAddress , .Attachment , .ReceiverID , .ReceiverType]
    static let smsType : [Keys] = [.SchCode , .key , .UserType]
    static let getSchoolList : [Keys] = [.key ,.filter]
    static let varifyMobileNo : [Keys] = [.SchoolCode , .MobileNo]
    static let logout : [Keys] = [.SchCode , .key , .UserID , .UserType , .DeviceID , .DeviceType]
    static let verifyBarCode : [Keys] = [.key , .SchCode , .Barcode]
    static let teacherAssignedClass : [Keys] = [.SchCode , .key ,  .UserID , .isSubject]
    static let studentListClassWise : [Keys] =  [.SchCode , .key ,  .ClassID , .SubID]
    static let savedAttendance : [Keys] = [.body]
    static let savedAttendanceModal : [Keys] = [.SchoolCode , .key , .ClassID , .StudentAtt , .SubID]
    static let sendBulkSms : [Keys] =  [.SchoolCode , .key , .SMSType , .FromUserID , .FromUserType , .Device , .Data , .isUnicode ,.SMS_Prvd_ID  ,.CountryCode ,.IPAddress]
    
    static let RecentUpdate: [Keys] = [.SchCode , .key , .RoleName , .UserID , .UserType , .pg]
    static let appSMS : [Keys] = [.SchoolCode , .key , .SMS_Prvd_ID , .CountryCode , .SMSType , .SMS , .isUnicode , .UserID , .UserType ,
                                  .RecipientType , .Recipient  , .ClassIDs , .Device , .DeviceID]
    
    static let pushSetting: [Keys] = [.SchCode , .key , .RoleName , .DeviceID , .DeviceType]
    static let setSetting: [Keys]  = [.SchCode , .key ,.MDLID, .isActive , .DeviceID , .DeviceType , .RoleName]
    static let subjectClassWise : [Keys] = [.SchCode , .key , .ClassID , .UserID]
    static let hasCreatedWeeklyPlan : [Keys] = [.SchCode , .key , .ClassID , .SubID ,.UserID,  .FromDate , .EndDate]
    static let createWeeklyPlan : [Keys] = [.body]
    static let getWeeklyPlans   : [Keys] = [.SchCode , .key , .ViewType , .TeacherID , .ClassID , .SubID , .pg , .UserID]
    static let weeklyPlanDTL    : [Keys] = [.SchCode , .key , .WPID , .UserType]
    static let weeklyPlanTakeAction : [Keys] = [.SchCode , .key , .PlnID , .Status , .UserID]
    static let weeklyPlanForStudent : [Keys] = [.SchCode ,.key ,.UserID,.SubIDs ,.PlanDate ,.pg]
    static let StudentSubject : [Keys] =  [.SchCode , .key , .UserID]
    static let weeklyPlanModify      : [Keys] = [.body]
    static let matchmultifaces      : [Keys] = []
    static let surveyList           : [Keys]  = [.SchCode , .key , .UserID , .UserType , .isReport , .pg]
    static let surveyResult           : [Keys]  = [.SchCode , .key , .UserID , .UserType , .SurID]
    static let surveyQuestions      : [Keys] = [.SchCode , .key , .SurID] 

    static let generateToken : [Keys] = [.SchCode , .key , .username,.password]
    static let messageSetting : [Keys] = [.SchCode , .key , .UserID , .UserType]
    static let surveyQueReponse : [Keys] =  [.SchCode , .key , .SurID , .QueID]
    static let attRpt : [Keys] = [.SchCode , .key ,.AttDate ,.UserID , .StaffType]
    static let executiveAttendance: [Keys] = [.SchCode , .key ,.AttDate]
    static let staffType: [Keys]      = [.SchCode , .key]
    static let attendanceSummary : [Keys] = [.SchCode, .key , .AttDate , .UserID]
    static let payslip : [Keys]       = [.SchCode, .key , .UserID]
    static let allSyllabus : [Keys]   = [.SchCode, .key , .ClassID]
    static let staffSyllabus : [Keys] = [.SchCode, .key , .UserID]
    static let thoughts: [Keys]       = [.SchCode, .key , .UserID,.UserType,.all,.dir,.pg]
    static let likeThought: [Keys]    = [.SchCode, .key , .UserID,.UserType,.ThID,.isLike]
    static let thoughtsCreate: [Keys] = [.SchoolCode, .key ,.Quotation,.Author,.UserID,.UserType]
    static let questionLst: [Keys]    = [.SchCode, .key , .UserID,.UserType,.CatID,.pg,.dir,.all]
    static let likeQuestion: [Keys]      = [.SchCode, .key , .UserID,.UserType,.QID,.isLike]
    static let questionDeleteAns: [Keys] = [.SchCode,.key,.UserID,.UserType,.AnID]
    static let questionPostAnswer: [Keys] = [.SchoolCode,.key,.UserID,.UserType,.QID,.Answer]
}




