//  APIHandler.swift
//  BusinessDirectory
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 Shubham Dhingra All rights reserved.

import Foundation
import SwiftyJSON
import ObjectMapper

enum ResponseKeys : String {
    case data
}
extension HomeEndpoint {
    
    func handle(data : Any) -> AnyObject?{
        //let parameters = JSON(data)
        
        switch self {
        case .sendMessage(_) , .LeaveDelete(_) , .replyMessage(_) ,.savedAttendance(_) , .sendBulkSmS(_):
            let json = JSON(data)
            return json[APIConstants.message].stringValue as AnyObject
            
        case .LeaveApply(_), .LeaveStatus(_), .LeaveReport(_) , .SearchLeave(_) , .leaveRequest(_) , .LeaveAction(_):
            let object = Mapper<LeaveDetailsModel>().map(JSONObject: data)
            return object
            
        case .myClasses(_) , .getClassListForAttendance(_):
            let object = Mapper<ContactsModel>().map(JSONObject: data)
            return object
            
        case .myProfile(_) , .verifyUser(_) , .userDetails(_) :
            let object = Mapper<MainProfileModel>().map(JSONObject : data)
            return object
            
        case .validate(_) , .validateForChangeUser(_):
            return data as AnyObject
            
        case .validateSchoolCode(_):
            let object = Mapper<SchoolModal>().map(JSONObject : data)
            return object
            
        case .getMessages(_), .getAllMessages(_), .smsType(_) , .getMessageSearch(_) , .messageSetting(_):
            let object = Mapper<MessageMainModal>().map(JSONObject : data)
            return object
            
        case .getMessageDTL(_), .sentMessageDTL(_):
            let object = Mapper<MessageDTLModal>().map(JSONObject : data)
            return object
            
        case .appVersion():
            let object = Mapper<VersionModal>().map(JSONObject : data)
            return object?.versionData
            
        case .getSchoolList():
            let object = Mapper<SchoolListDTL>().map(JSONObject : data)
            return  object
            
        case .getStudentListClassWise(_) , .SubjectClassWise(_):
            let object = Mapper<StudentListModal>().map(JSONObject : data)
            return  object
            
        case .RecentUpdate(_):
            let object = Mapper<RecentUpdate>().map(JSONObject : data)
            return  object
            
        case .PushSetting(_):
            let object = Mapper<SettingModal>().map(JSONObject : data)
            return  object
            
        case .Matchmultifaces(_):
            let object = Mapper<MatchmultifacesModal>().map(JSONObject : data)
            return  object
            
        case .GenerateToken(_):
            let object = Mapper<SettingModal>().map(JSONObject : data)
            return  object
            
        case .SurveyRes(_):
            let object = Mapper<SurveyPieChartModal>().map(JSONObject : data)
            return  object
            
        case .AttRpt(_),.ExecutiveAttendance(_):
            let object = Mapper<TeacherAtteReportModel>().map(JSONObject : data)
            return  object
        case .StaffType(_):
            let object = Mapper<StaffTypeModal>().map(JSONObject : data)
            return  object
        case .AttendanceSummary(_):
            let object = Mapper<AtttendanceSummaryModal>().map(JSONObject : data)
            return  object
        case .Payslip(_):
            let object = Mapper<PaySlipModal>().map(JSONObject : data)
            return  object
        case .AllSyllabus(_),.StaffSyllabus(_):
            let object = Mapper<AllSyllabusModal>().map(JSONObject : data)
           return  object
        case .Thoughts(_):
            let object = Mapper<ThoughtsModal>().map(JSONObject : data)
            return  object
        case .LikeThought(_),.ThoughtsCreate(_),.LikeQuestion(_),.QuestionDeleteAns(_),.QuestionPostAnswer(_):
            let object = Mapper<ThoughtsLikeModal>().map(JSONObject : data)
            return  object
        case .QuestionLst(_):
            let object = Mapper<QuestionModal>().map(JSONObject : data)
            return  object
            
        default :
            return "" as? AnyObject
        }
    }
}

extension ECareEndPoint {
    
    func handle(data : Any) -> AnyObject?{
        //let parameters = JSON(data)
        
        switch self {
        case .isWeeklyPlanCreated(_) , .getWeeklyPlans(_) , .weeklyPlanDTL(_) :
            let object = Mapper<PlanModal>().map(JSONObject: data)
            return object
        
        case .createWeeklyPlan(_) , .weeklyPlanTakeAction(_) ,.weeklyPlanModify(_) , .postAnswer(_):
            let json = JSON(data)
            return json[APIConstants.message].stringValue as AnyObject
            
        case .weeklyPlanForStudent(_):
            let object = Mapper<WeeklyPlanModelStudent>().map(JSONObject: data)
            return object
            
        case .getStudentSubject(_):
            let object = Mapper<StudentListModal>().map(JSONObject : data)
            return  object
            
        case .getSurveyList(_)  , .surveyQuestions(_)  , .surveyResult(_):
            let object = Mapper<SurveyModal>().map(JSONObject : data)
            return  object
        }
    }
}



