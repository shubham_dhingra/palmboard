
//
//  ECareEndPoint.swift
//  PalmBoard
//
//  Created by Shubham on 27/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import EZSwiftExtensions

enum ECareEndPoint {
    case isWeeklyPlanCreated(ClassID : String? , SubId : String? , UserId : String? , FromDate : String? , EndDate : String?)
    case createWeeklyPlan(body : [String : Any]?)
    case getWeeklyPlans(viewType : Int , TeacherID : String? , ClassID : String? , SubID : String? , pg : Int , UserID : String?)
    case weeklyPlanDTL(WPID : Int? , UserType : String?)
    case weeklyPlanTakeAction(PlIn : String? , Status : String? , UserID : String?)
    case weeklyPlanForStudent(UserID : String? , SubIDs : String? , PlanDate : String? , pg : String?)
    case weeklyPlanModify(body : [String : Any]?)
    case getStudentSubject(UserId : String?)
    case getSurveyList(UserID : String? , UserType : String? , isReport : String? , pg : Int?)
    case surveyQuestions(SurId : String?)
    case postAnswer(body : [String : Any]?)
    case surveyResult(UserID : String? , UserType : String? , SurID : Int?)

}


extension ECareEndPoint : Router{
    
    var route : String  {
        
        switch self {
        case .isWeeklyPlanCreated(_) :               return APIConstants.IsweeklyPanCreated
        case .createWeeklyPlan(_) :                  return APIConstants.createWeeklyPlan
        case .getWeeklyPlans(_):                    return APIConstants.GetWeeklyPlans
        case .weeklyPlanDTL(_) :                    return APIConstants.weeklyPlanDTL
        case .weeklyPlanTakeAction(_):              return APIConstants.weeklyPlanTakeAction
        case .weeklyPlanForStudent(_):              return APIConstants.weeklyPlanForStudent
        case .weeklyPlanModify(_):                  return APIConstants.weeklyPlanModify
        case .getStudentSubject(_) :                return APIConstants.studentSubject
        case .getSurveyList(_):                     return APIConstants.surveyList
        case .surveyQuestions(_):                   return APIConstants.surveyQuestions
        case .postAnswer(_) :                       return APIConstants.postAnswer
        case .surveyResult(_):                      return APIConstants.surveyResult
        }
    }
    
    var parameterss: OptionalDictionary{
        return format()
        
    }
    
    func format() -> OptionalDictionary {
        
        let schoolCode  : String = /DBManager.shared.getAllSchools().first?.schCode
        let key         : String =  APP_CONSTANTS.KEY
//        let CountryCode : String = "\(DBManager.shared.getAllSchools().first?.countryCode ?? 0)"
//        let ipaddress   : String = "192.168.103.106"
//        let deviceID    : String = ((UDKeys.deviceToken.fetch() as? String) ?? "")
        switch self {
        case .isWeeklyPlanCreated(let classId , let subId , let UserId , let fromDate , let endDate):
            return Parameterss.hasCreatedWeeklyPlan.map(values: [schoolCode , key , classId , subId , UserId ,fromDate , endDate])
            
        case .createWeeklyPlan(let body) , .weeklyPlanModify(let body) , .postAnswer(let body):
            return Parameterss.createWeeklyPlan.map(values :[body])
            
        case .getWeeklyPlans(let viewType, let TeacherID, let ClassID, let SubID,let pg , let UserID):
            return Parameterss.getWeeklyPlans.map(values: [schoolCode , key , viewType.toString , TeacherID , ClassID , SubID , pg.toString , UserID])
            
        case .weeklyPlanDTL(let WPID ,let userType):
            return Parameterss.weeklyPlanDTL.map(values: [schoolCode , key , WPID , userType])
        
        case .weeklyPlanTakeAction(let PInID, let Status,let UserID):
            return Parameterss.weeklyPlanTakeAction.map(values: [schoolCode , key ,PInID , Status , UserID])
            
        case .weeklyPlanForStudent(let UserID,let SubIDs,let PlanDate,let pg):
            return Parameterss.weeklyPlanForStudent.map(values :[schoolCode , key,UserID,SubIDs,PlanDate,pg])
            
        case .getStudentSubject(let UserId):
            return Parameterss.StudentSubject.map(values: [schoolCode , key , UserId])
            
        case .getSurveyList(let UserID , let UserType, let isReport , let pg):
            return Parameterss.surveyList.map(values: [schoolCode , key , UserID , UserType , isReport , pg?.toString])
       
        case .surveyResult(let UserID , let UserType , let SurID):
             return Parameterss.surveyResult.map(values: [schoolCode , key , UserID , UserType , SurID?.toString])
            
        case .surveyQuestions(let SurId):
             return Parameterss.surveyQuestions.map(values: [schoolCode , key , SurId])
            
            
        }
    }
        
    var method : Alamofire.HTTPMethod {
        switch self {
        case .isWeeklyPlanCreated(_) , .getWeeklyPlans(_) , .weeklyPlanDTL(_) , .weeklyPlanTakeAction(_),.weeklyPlanForStudent(_)  , .getStudentSubject(_) , .getSurveyList(_) , .surveyQuestions(_) , .surveyResult(_):
            return .get
        default :
            return .post
        }
    }
    
    var baseURL: String{
        return APIConstants.basePath
    }
    var teamCarebaseURL: String{
        return APIConstants.teamCarBasePath3
    }

    var header : [String: String]{
        return [:]
    }
}


