 //
//  HTTPClient.swift
//  BusinessDirectory
//
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 Shubham Dhingra All rights reserved.
//

import Foundation
import Alamofire

typealias HttpClientSuccess = (Any?) -> ()
typealias HttpClientFailure = (String) -> ()


class HTTPClient {
    
    func JSONObjectWithData(data: NSData) -> Any? {
        do { return try JSONSerialization.jsonObject(with: data as Data, options: []) }
        catch { return .none }
    }
    
    func postRequest(withApi api : Router , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure ){
        
        let fullPath = api.baseURL + api.route
        var params : [String : Any]?
        var parameterEncoding  : ParameterEncoding   =  URLEncoding.default
        print(fullPath)
       
        switch  api.route {
            
        case APIConstants.saveAppointment , APIConstants.registerEscort , APIConstants.replyMessage ,APIConstants.sendMessage , APIConstants.savedAttendance , APIConstants.sendBulkSms , APIConstants.appSMS , APIConstants.createWeeklyPlan , APIConstants.weeklyPlanModify, APIConstants.postAnswer, APIConstants.leaveRequest:
            parameterEncoding = JSONEncoding.default
       
        default:
            break
        }
        
        switch api.route{
            
        case APIConstants.registerEscort , APIConstants.savedAttendance , APIConstants.sendBulkSms , APIConstants.createWeeklyPlan , APIConstants.weeklyPlanModify , APIConstants.postAnswer:
            params = api.parameterss?.first?.value as? [String : Any]
            
        default:
            params = api.parameterss
            
            //Parameters from particular api is allowed to print
            print(api.parameterss ?? "")
            break
        }
        
        let startDate = Date()
        Alamofire.request(fullPath, method: api.method, parameters: params, encoding: parameterEncoding , headers: [:]).responseJSON { (response) in
            
            switch response.result {
            case .success(let data):
                let executionTime = Date().timeIntervalSince(startDate).toString
                print("API Response Time :\(executionTime)")
                success(data)
                
            case .failure(let error):
                failure(error.localizedDescription)
                print("\n\n===========Error===========")
                print("Error Code: \(error._code)")
                print("Error Messsage: \(error.localizedDescription)")
                if let data = response.data, let str = String(data: data, encoding: String.Encoding.utf8){
                    print("Server Error: " + str)
                }
                debugPrint(error as Any)
                print("===========================\n\n")
            }
        }
    }
    
    
    func postRequestWithImage(withApi api : Router, image : UIImage?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        
        guard let params = api.parameterss else {failure("empty"); return}
        var fullPath = String()
        switch api.route{
        case APIConstants.matchmultifaces :
            fullPath = api.teamCarebaseURL   + api.route + "?key=\(TeamCareAPP_CONSTANTS.KEY)&code=\(/DBManager.shared.getAllSchools().first?.schCode)"//
        default:
            fullPath = api.baseURL + api.route
            break
        }
        //   let fullPath = api.baseURL + api.route
        print(fullPath)
        print(api.parameterss ?? "")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            if let img = image {
                print("image Uploaded size")
                let imageData = img.jpegData(compressionQuality: 0.2)
                let imageSize: Int = NSData(data: img.jpegData(compressionQuality: 1)!).length
                print("size of image in KB: %f ", Double(imageSize) / 1024.0)
                let  currentDate = Date().getCurrentDate(format: "yyyy-dd-M--HH-mm-ss")
                print("fileName : file_ios_\(currentDate)")
                multipartFormData.append(imageData!, withName: "image", fileName: "file_ios_\(currentDate)", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        },to: fullPath ,method : api.method, headers : headerNeeded(api: api) ? api.header : nil) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_,_):
                
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    func postRequestWithDocument(withApi api : Router,documentUrl : URL?, success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        guard let params = api.parameterss else {failure("empty"); return}
        let fullPath = api.baseURL + api.route
        //print(fullPath)
        //print(params)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let url = documentUrl {
                guard let data = try? Data(contentsOf: url) else {
                    return
                }
                multipartFormData.append(data, withName: "file", fileName: "Notes.pdf", mimeType: "application/pdf")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue
                    )!, withName: key)
            }
            
        }, to: fullPath) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_,_):
                
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let data):
                        success(data)
                    case .failure(let error):
                        failure(error.localizedDescription)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    
    func headerNeeded(api : Router) -> Bool{
        switch api.route {
        case APIConstants.registerDevice:
            return false
        default:
            return false
        }
    }
}

