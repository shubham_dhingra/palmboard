//  Login.swift
//  APISampleClass
//  Created by Aseem 13 on 04/01/17.
//  Copyright © 2017 Shubham Dhingra All rights reserved.
import UIKit
import Alamofire
import SwiftyJSON
//import EZSwiftExtensions

protocol Router {
    var route : String { get }
    var baseURL : String { get }
    var parameterss : OptionalDictionary { get }
    var method : Alamofire.HTTPMethod { get }
    func handle(data : Any) -> AnyObject?
    var header : [String: String] {get}
    var teamCarebaseURL : String { get }

}

extension Sequence where Iterator.Element == Keys {
    
    func map(values: [Any?]) -> OptionalDictionary {
        
        var params = [String : Any]()
        
        for (index,element) in zip(self,values) {
            if element as? String != "" || (((element as? [NSMutableDictionary]) != nil)) || (((element as? [String : Any]) != nil)) || (((element as? [[String : Any]]) != nil)){
                params[index.rawValue] = element
            }
        }
        return params
    }
}


enum HomeEndpoint {
    case sendMessage(MsgType : String? , UserID : String? , UserType : String? , Subject : String? , Body : String? , Device : String? , Attachment : String? , Recipient : [NSMutableDictionary]?, ClassID : String? , RecipientType : String?)
    case leaveRequest(fromDate : String?, tillDate : String? , duration : String? , Reason : String? , StID : String? , UserID : String? , UserType : String? , halfDayDTL : [[String : Any]]? , LeaveID : String?)
    case LeaveApply(UserID : String? , UserType : String?)
    case LeaveStatus(UserID : String? , UserType : String? , pageNo : String?)
    case LeaveDelete(LvID : String? , UserType : String?)
    case LeaveReport(UserID: String? , Status : String? , ord : String? , UserType : String? , pg : String?)
    case LeaveAction(UserID: String?, Action : String? , LvID : String? , UserType : String?)
    case SearchLeave(UserID : String? , Status : String? , ord : String? , SearchBy : String? , pg : String? , StName : String? , ClassID : String? , LeaveDate: String? , UserType : String?)
    case myClasses(UserID : String?)
    case ChangePassword(UserID : String? , UserType : String? , NewPassword : String?)
    case userDetails(UserID : String? , UserType : String?)
    case myProfile(UserID : String? , UserType : String?)
    case verifyUser(schoolCode : String? , username : String?)
    case validate(schoolCode : String? , username : String? , password : String?)
    case registerDevice(userID: String? , userType : String?)
    case validateForChangeUser(schoolCode : String? , username : String? , password : String?)
    case validateSchoolCode(schoolCode : String?)
    case getMessages(userID : String? , userType : String? , pg : Int? , segmentIndex : Int)
    case getAllMessages(senderID : Int , senderType : Int , userID : String? , userType : String? , pg : Int?)
    case getMessageDTL(msgID : Int, senderID : Int , senderType : Int , userID : String? , userType : String?)
    case getMessageSearch(senderID : Int , senderType : Int , userID : String? , userType : String?, pg : Int?,query: String?)
    
    case sentMessageDTL(msgID : Int , userID : String? , userType : String?)
    case replyMessage(msgID : Int , msgType : Int , userID : String? , userType : String? , Body : String? , Attachment : String? , ReceiverID : Int , ReceiverType : Int)
    case smsType(userType : String?)
    case appVersion()
    case RecentUpdate(userID : String? , userType : String? , roleName : String? , pg : String?)
    case PushSetting( roleName : String?)
    case SetSetting(MDLID : Int?, isActive: String , roleName : String)

    case logout(schoolCode : String? , userID : Int32 , userType : Int32)
    case getSchoolList()
    case verifyBarCode(BarCode : String?)
    case getClassListForAttendance(UserID : String? , isSubject : Int?)
    case getStudentListClassWise(ClassID : String? , SubID : String?)
    case savedAttendance(body : [String : Any]?)
    case sendBulkSmS(body : [String : Any]?)
    case AppSMS(UserID : String? , UserType : String? , SMSType : String? , SMS : String? , RecipientType : String? , Recipient : [NSMutableDictionary]? , ClassID : String? , sMS_Prvd_ID : Int?)
    case SubjectClassWise(UserID : String? , classID : String?)
    case Matchmultifaces()
    case GenerateToken(schoolCode : String? , username : String? , password : String?)
    case messageSetting(UserId : String? , UserType : String?)
    case SurveyRes(SurId : String? , QueID : String?)
    case AttRpt(AttDate: String? ,UserId : String? , StaffType : String?)
    case ExecutiveAttendance(AttDate: String?)
    case StaffType()
    case AttendanceSummary(attDate : String? , UserID : String?)
    case Payslip(UserID : String?)
    case AllSyllabus(ClassID : String?)
    case StaffSyllabus(UserId : String?)
    case Thoughts(UserID : String?,UserType : String?,all : String?,dir: String?,pg: String?)
    case LikeThought(UserID : String?,UserType : String?,ThID : String?,isLike: String?)
    case ThoughtsCreate(Quotation : String?,Author : String?,UserID : String?,UserType: String?)
    case QuestionLst(UserID : String?,UserType : String?,CatID: String?,pg: String?,dir: String?,all : String?)
    case LikeQuestion(UserID : String?,UserType : String?,QID : String?,isLike: String?)
    case QuestionDeleteAns(UserID : String?,UserType : String?,AnID: String?)
    case QuestionPostAnswer(UserID : String?,UserType : String?,QID: String?,Answer: String?)
}

extension HomeEndpoint : Router{
    
    
    var route : String  {
        
        switch self {
            
        case .sendMessage(_) :               return APIConstants.sendMessage
        case .LeaveApply(_) :                 return APIConstants.leaveSetting
        case .leaveRequest(_) :              return APIConstants.leaveRequest
        case .LeaveDelete(_) :               return APIConstants.leaveDelete
        case .LeaveStatus(_) :               return APIConstants.leaveStatus
        case .LeaveReport(_) :               return APIConstants.leaveReport
        case .LeaveAction(_) :               return APIConstants.leaveAction
        case .SearchLeave(_)  :              return APIConstants.searchLeave
        case .myClasses(_) :                 return APIConstants.myClasses
        case .ChangePassword(_) :            return APIConstants.changePassword
        case .userDetails(_) :               return APIConstants.userDetails
        case .myProfile(_) :                 return APIConstants.myProfile
        case .verifyUser(_) :                return APIConstants.verifyuser
        case .validate(_) :                  return APIConstants.validate
        case .registerDevice(_) :            return APIConstants.registerDevice
        case .validateForChangeUser(_) :     return APIConstants.validate
        case .validateSchoolCode(_) :        return APIConstants.SchoolCode
        case .getAllMessages(_) :            return APIConstants.messagesFrom
        case .getMessageSearch(_) :          return APIConstants.messagesSearch
            
        case .getMessageDTL(_) :             return APIConstants.messageDTL
        case .sentMessageDTL(_) :            return APIConstants.sentMsgDTL
        case .replyMessage(_) :              return APIConstants.replyMessage
        case .smsType(_) :                   return APIConstants.smsType
        case .getSchoolList() :              return APIConstants.getSchoolList
        case .appVersion() :                 return APIConstants.appVersion
        case .logout(_) :                    return APIConstants.logout
        case .verifyBarCode(_) :             return APIConstants.verifyBarCode
        case .getClassListForAttendance(_) : return APIConstants.teacherAssignedClass
        case .getStudentListClassWise(_) :   return APIConstants.studentListClassWise
        case .savedAttendance(_) :           return APIConstants.savedAttendance
        case .sendBulkSmS(_) :               return APIConstants.sendBulkSms
        case .RecentUpdate(_):               return APIConstants.recentUpdate
        case .AppSMS(_):                     return APIConstants.appSMS
        case .PushSetting(_):                return APIConstants.pushSetting
        case .SetSetting(_):                 return APIConstants.setSetting
        case .SubjectClassWise(_):           return APIConstants.subjectClassWise
        case .Matchmultifaces(_):            return APIConstants.matchmultifaces //+ "?key=\(TeamCareAPP_CONSTANTS.KEY)&code=\(/DBManager.shared.getAllSchools().first?.schCode)"
        case .GenerateToken(_):              return APIConstants.generateToken
        case .messageSetting(_):             return APIConstants.messageSetting
        case .SurveyRes(_):                  return APIConstants.surveyQueResponse
        case .AttRpt(_):                     return APIConstants.attRpt
        case .ExecutiveAttendance(_):        return APIConstants.executiveAttendance
        case .StaffType(_):                  return APIConstants.staffType
        case .AttendanceSummary(_):          return APIConstants.attendanceSummary
        case .Payslip(_):                    return APIConstants.payslip
        case .AllSyllabus(_):                return APIConstants.allSyllabus
        case .StaffSyllabus(_):              return APIConstants.staffSyllabus
        case .Thoughts(_):                   return APIConstants.thoughts
        case .LikeThought(_):                return APIConstants.likeThought
        case .QuestionLst(_):                return APIConstants.questionLst
        case .LikeQuestion(_):               return APIConstants.likeQuestion
        case .QuestionDeleteAns(_):          return APIConstants.questionDeleteAns
        case .QuestionPostAnswer(_):         return APIConstants.questionPostAnswer

        case .getMessages(_,_,_,let segmentIndex) :
            if segmentIndex == 0  {
                return APIConstants.inboxMsg
            }
            else {
                return APIConstants.sentMsg
            }
        default:
            return ""
        }
        
       
    }
    
    var parameterss: OptionalDictionary{
        return format()
        
    }
    
    func format() -> OptionalDictionary {
        
        let schoolCode  : String = /DBManager.shared.getAllSchools().first?.schCode
        let key         : String =  APP_CONSTANTS.KEY
        let Executivekey : String =  ExecutiveAPP_CONSTANTS.KEY

        let CountryCode : String = "\(DBManager.shared.getAllSchools().first?.countryCode ?? 0)"
        let ipaddress   : String = "192.168.103.106"
        let deviceID    : String = ((UDKeys.deviceToken.fetch() as? String) ?? "")
        switch self {
            
        case .sendMessage(let MsgType,let UserID,let UserType ,let Subject,let Body
            , let Device, let Attachment, let Recipient , let classID , let RecipientType):
            return Parameterss.sendMessage.map(values: [schoolCode, key, CountryCode, MsgType, UserID, UserType, Subject, Body, Device, ipaddress, Recipient, Attachment, classID , RecipientType])
            
        case .LeaveApply(let userID , let UserType):
             return Parameterss.leaveSetting.map(values: [schoolCode , key , userID , UserType])
        case .appVersion():
            return Parameterss.appVersion.map(values: [schoolCode , key])
            
        case .leaveRequest(let fromDate , let tillDate,let duration,let Reason,let StID,let UserID, let UserType , let halfDayDTL , let LeaveID) :
            return Parameterss.leaveRequest.map(values : [schoolCode , key , StID , UserID , UserType, fromDate , tillDate , duration , Reason , halfDayDTL , LeaveID])
            
        case .LeaveDelete(let LvID , let UserType):
            return Parameterss.deleteLeave.map(values : [schoolCode , key ,LvID , UserType])
            
        case .LeaveStatus(let UserID ,let UserType , let pageNo):
            return Parameterss.leaveStatus.map(values : [schoolCode , key ,UserID , UserType ,pageNo])
            
        case .LeaveReport(let UserID,let Status, let ord , let ApplType , let pg):
            return Parameterss.leaveReport.map(values: [schoolCode , key , UserID , Status , ord , ApplType , pg])
            
        case .LeaveAction(let UserID ,let Action,let LvID , let UserType):
            return Parameterss.leaveAction.map(values: [schoolCode , key , LvID , Action , UserID , UserType])
            
        case .SearchLeave(let UserID, let Status,let ord,let SearchBy,let pg ,let StName,let ClassID,let LeaveDate , let UserType):
            return Parameterss.searchLeave.map(values: [schoolCode , key , UserID , Status , ord , SearchBy, pg, StName , ClassID ,LeaveDate, UserType])
            
        case .myClasses(let UserID):
            return Parameterss.myClasses.map(values: [schoolCode , key, UserID])
            
        case .ChangePassword(let UserID, let UserType ,let NewPassword):
            return Parameterss.changePassword.map(values: [schoolCode  , key , UserID , UserType , NewPassword])
            
        case .userDetails(let UserID, let UserType):
            return Parameterss.userDetails.map(values: [schoolCode  , key , UserID , UserType])
            
        case .myProfile(let UserID , let UserType):
            return Parameterss.myProfile.map(values: [schoolCode  , key , UserID , UserType])
            
        case .verifyUser(let schoolcode ,let username):
            return Parameterss.verifyUser.map(values: [schoolcode  , key , username])
            
        case .validate(let schoolcode , let username, let password):
            return Parameterss.validate.map(values: [schoolcode , key , username , password])
            
        case .registerDevice(let UserID , let UserType):
            return Parameterss.registerDevice.map(values : [schoolCode , key , deviceID , 2 , UserID , UserType])
            
        case .validateForChangeUser(let newSchoolCode ,let  username , let  password) :
            return Parameterss.validate.map(values: [newSchoolCode , key , username , password])
            
        case .validateSchoolCode(let schoolCode):
            return Parameterss.validate.map(values: [schoolCode , key])
            
        case .getMessages(let userID , let userType , let pageNo, _):
            return Parameterss.getMessages.map(values: [schoolCode  , key , userID , userType , pageNo?.toString])
            
        case .getAllMessages(let senderID , let senderType ,let userID , let userType , let pageNo) :
            return Parameterss.messageFrom.map(values: [schoolCode  , key , senderID.toString , senderType.toString , userID , userType , pageNo?.toString])
            
        case .getMessageSearch(let senderID , let senderType ,let userID , let userType , let pageNo, let query) :
            return Parameterss.messageSearch.map(values: [schoolCode  , key , senderID.toString , senderType.toString , userID , userType , pageNo?.toString, query?.toNSString])
            
        case .getMessageDTL(let msgID, let  senderID,let  senderType,let  userID,let  userType):
            return Parameterss.messageDTL.map(values:  [schoolCode  , key , msgID.toString ,senderID.toString , senderType.toString , userID , userType])
            
        case .sentMessageDTL(let msgID, let userID, let userType):
            return Parameterss.sentMsgDTL.map(values: [schoolCode , key , msgID.toString , userID , userType])
            
        case .replyMessage(let msgID, let msgType, let userID, let userType, let Body, let Attachment, let ReceiverID, let ReceiverType):
            return Parameterss.replyMessage.map(values: [schoolCode , key , CountryCode , msgID.toString , msgType.toString , userID , userType , Body , "2" , ipaddress , Attachment , ReceiverID.toString , ReceiverType.toString])
            
        case .smsType(let userType):
            return Parameterss.smsType.map(values: [schoolCode , key , userType])
            
        case .logout(let schoolCode,let userID, let userType):
            return Parameterss.logout.map(values: [schoolCode , key , String(userID) , String(userType) , deviceID , "2"])
            
        case .getSchoolList():
            return Parameterss.getSchoolList.map(values : [key, "2"])
            
        case .verifyBarCode(let BarCode):
            return Parameterss.verifyBarCode.map(values: [key , schoolCode , BarCode])
            
        case .getStudentListClassWise(let ClassID , let SubID):
            return Parameterss.studentListClassWise.map(values: [schoolCode , key , ClassID, SubID])

        case .getClassListForAttendance(let userID , let isSubject):
            return Parameterss.teacherAssignedClass.map(values: [schoolCode , key , userID , isSubject?.toString])
            
        case .savedAttendance(let body) , .sendBulkSmS(let body):
            return Parameterss.savedAttendance.map(values: [body])
            
        case .RecentUpdate(let userID , let userType , let roleName, let pg):
            return Parameterss.RecentUpdate.map(values: [schoolCode, key, roleName , userID , userType , pg])
        
        case .PushSetting(let roleName):
           return Parameterss.pushSetting.map(values: [schoolCode, key, roleName, deviceID , 2])
            
        case .SetSetting(let MDLID , let isActive ,let roleName):
            return Parameterss.setSetting.map(values: [schoolCode,key,MDLID,isActive,deviceID, 2 , roleName])

        
        case .AppSMS(let UserID , let UserType , let SMSType ,let SMS, let RecipientType, let Recipient,let ClassID, let sMS_Prvd_ID):
            return Parameterss.appSMS.map(values: [schoolCode , key , /sMS_Prvd_ID?.toString , CountryCode , SMSType , SMS , "1" , UserID , UserType , RecipientType , Recipient , ClassID , "2", "abcd"])
            
        case .SubjectClassWise(let UserID , let ClassID):
            return Parameterss.subjectClassWise.map(values: [schoolCode , key , ClassID , UserID])
            
        case .GenerateToken(let schoolcode , let username, let password):
            return Parameterss.generateToken.map(values: [schoolcode , key , username , password])
            
        case .messageSetting(let UserId , let UserType):
            return Parameterss.messageSetting.map(values: [schoolCode , key , UserId , UserType])
        
        case .SurveyRes(let SurId,let QueID):
            return Parameterss.surveyQueReponse.map(values: [schoolCode , key , SurId , QueID])
            
        case .AttRpt(let AttDate , let UserId, let StaffType):
            return Parameterss.attRpt.map(values: [schoolCode , key , AttDate, UserId , StaffType])
            
        case .ExecutiveAttendance(let AttDate):
            return Parameterss.executiveAttendance.map(values: [schoolCode, Executivekey ,AttDate])
        //return Parameterss.executiveAttendance.map(values: ["SJSSGN", Executivekey ,"2018-09-05"])
        case .StaffType():
            return Parameterss.staffType.map(values : [schoolCode, key])
            
        case .AttendanceSummary(let attDate, let UserID):
            return Parameterss.attendanceSummary.map(values :[schoolCode , key , attDate , UserID])
        case .Payslip(let UserID):
            return Parameterss.payslip.map(values :[schoolCode , key ,UserID])
        case .AllSyllabus(let ClassID):
            return Parameterss.allSyllabus.map(values :[schoolCode , key ,ClassID])
        case .StaffSyllabus(let UserID):
            return Parameterss.staffSyllabus.map(values :[schoolCode , key ,UserID])
        case .Thoughts(let UserID,let UserType,let all,let dir,let pg):
            return Parameterss.thoughts.map(values :[schoolCode , key ,UserID,UserType,all,dir,pg])
        case .LikeThought(let UserID,let UserType,let ThID,let isLike):
            return Parameterss.likeThought.map(values :[schoolCode , key ,UserID,UserType,ThID,isLike])
        case .ThoughtsCreate(let Quotation,let Author,let UserID,let UserType):
            return Parameterss.thoughtsCreate.map(values :[schoolCode , key ,Quotation,Author,UserID,UserType])
        case .QuestionLst(let UserID,let UserType,let CatID,let pg,let dir,let all):
            return Parameterss.questionLst.map(values :[schoolCode , key ,UserID,UserType,CatID,pg,dir,all])
        case .LikeQuestion(let UserID,let UserType,let QID,let isLike):
            return Parameterss.likeQuestion.map(values :[schoolCode , key ,UserID,UserType,QID,isLike])
        case .QuestionDeleteAns(let UserID,let UserType,let AnID):
            return Parameterss.questionDeleteAns.map(values :[schoolCode , key ,UserID,UserType,AnID])
        case .QuestionPostAnswer(let UserID,let UserType,let QID,let Answer):
            return Parameterss.questionPostAnswer.map(values :[schoolCode,key,UserID,UserType,QID,Answer])

        case .Matchmultifaces:
            return [:]
        }
        
    }
    var method : Alamofire.HTTPMethod {
        switch self {
        case .LeaveApply(_) , .LeaveDelete(_) , .LeaveStatus(_) , .LeaveReport(_) ,.SearchLeave(_) , .myClasses(_) ,.userDetails(_) , .myProfile(_) , .verifyUser(_) , .validateSchoolCode(_) , .getMessages(_), .getAllMessages(_) , .getMessageDTL(_),.getMessageSearch, .sentMessageDTL(_) , .smsType(_) , .appVersion(), .logout(_),.getSchoolList() , .verifyBarCode(_) , .getStudentListClassWise(_) , .getClassListForAttendance(_),.RecentUpdate(_),.PushSetting(_),.SetSetting(_),.SubjectClassWise(_),.GenerateToken(_) , .messageSetting(_) , .SurveyRes(_),.AttRpt(_),.ExecutiveAttendance(_),.StaffType(_)  , .AttendanceSummary(_),.Payslip(_),.AllSyllabus(_),.StaffSyllabus(_),.Thoughts(_),.LikeThought(_),.QuestionLst(_),.LikeQuestion(_):
                return .get
        default :
               return .post
        }
    }
    
    var baseURL: String{
        
        switch self {
        case .ExecutiveAttendance(_):
            return APIConstants.basePathExecutive
        default:
            return APIConstants.basePath
        }
    }
    var teamCarebaseURL: String{
        return APIConstants.teamCarBasePath3
    }

    var header : [String: String]{
        return [:]
    }
}

