
import UIKit
import CoreData
import AVFoundation
import Firebase
import FirebaseMessaging
import UserNotifications
import FirebaseInstanceID
import SwiftyJSON
import EZSwiftExtensions
import Fabric
import Crashlytics

let screenWidth  = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate ,MessagingDelegate{
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var userInfo : [AnyHashable : Any]?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        /************************************************************************************************/
        checkUserExists()
        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true
        //FirebaseApp.configure()
        setupFireBase(application)
        return true
    }
    //MARK::- checkUserExists
    func checkUserExists(){
        var initialViewController = UIViewController()
        let initialCheck =  UserDefaults.standard.integer(forKey:"logged_in")
        
        //print("initialCheck = \(initialCheck)")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch initialCheck {
        case 0,1,2:
            initialViewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        case 3:
            initialViewController = storyboard.instantiateViewController(withIdentifier: "customTabbarViewController")
        default:
            break
        }
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        /************************************************************************************************/
        do {
            try AVAudioSession.sharedInstance().setMode(AVAudioSession.Mode.moviePlayback)
        }
        catch let error as NSError{
            print(error)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    func applicationDidEnterBackground(_ application: UIApplication) {}
    func applicationWillEnterForeground(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Reset the application badge to zero when the application as launched. The notification is viewed.
        if application.applicationIconBadgeNumber > 0 {
            application.applicationIconBadgeNumber = 0
        }
        connectToFcm()
    }
    //MARK::- setupFireBase
    func setupFireBase(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        // [START add_token_refresh_observer]
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name(rawValue: "firInstanceIDTokenRefresh"),
                                               object: nil)
        // [END add_token_refresh_observer]
    }
    
    // [START refresh_token]
    @objc func tokenRefreshNotification(_ notification: Notification) {
        
        if let refreshedToken = InstanceID.instanceID().token() {
            //  print("InstanceID token: \(refreshedToken)")
            UDKeys.deviceToken.save(refreshedToken)
            if UserDefaults.standard.integer(forKey:"logged_in") != 1 {
                self.updateToken(token : refreshedToken)
            }
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    //MARK::- CONNECT TO FCM
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        Messaging.messaging().disconnect()
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                  print("Connected to FCM.")
                 print("second ",InstanceID.instanceID().token() ?? "")
                guard let token = InstanceID.instanceID().token() else {return}
                UDKeys.deviceToken.save(token)
                // print(token)
                
                //                self.updateToken(token : token)
            }
        }
    }
    // [END connect_to_fcm]
    public func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String){
        
    }
    /// The callback to handle data message received via FCM for devices running iOS 10 or above.
    public func application(received remoteMessage: MessagingRemoteMessage) {
        // print(remoteMessage.appData)
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        if let messageID = userInfo[gcmMessageIDKey] {
            // print("Message ID: \(messageID)")
        }
        
        // Print full message.
        // print(userInfo)
        
        if UIApplication.shared.applicationState == .active  {
        
        } else {
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.handleNotifcation(userInfo: userInfo)
            }
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    @available(iOS 10.0, *)
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if !(UIApplication.shared.applicationState == .active)  {
            completionHandler([UNNotificationPresentationOptions.alert,
                               UNNotificationPresentationOptions.sound,
                               UNNotificationPresentationOptions.badge])
        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        //  print("Device Token = ", token)
        //        UDKeys.deviceToken.save(token)
        //For production
        InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: .prod)
        
        //For Testing
        //  InstanceID.instanceID().setAPNSToken(deviceToken as Data, type: .sandbox )
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "PalmBoard")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
/************************************************************************/
//MARK::- HANDLE PUSH
extension AppDelegate{
    
    //MARK::- SHOW CUSTOM NOTIFICATION
    func showNotification(userInfo : [AnyHashable : Any]) {
    
    }
    
    //MARK::- Token Refresh Api
    func updateToken(token : String?){
        if let user = DBManager.shared.getUsersTableAssociateWithSchools().first {
            if  user.userID != 0 && user.userType != 0 {
                if (UDKeys.deviceToken.fetch() as? String) != nil  {
                    DBManager.shared.registerDevice(userID: String(user.userID), userType: String(user.userType))
                }
            }
        }
    }
    
    //MARK: - Tap on IOS9 notification action
    func tappedOnNotification(_ sender: UIGestureRecognizer) {
        guard let data = userInfo else { return }
        // print(userInfo ?? "")
        handleNotifcation(userInfo: data)
    }
    
    
    //MARK::- HANDLE PUSH
    func handleNotifcation(userInfo : [AnyHashable : Any]) {
        
        if let body = userInfo["Body"] as? String {
            guard let data = body.data(using: .utf8) else {return}
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            if let pushData = json as? [String : Any] {
                if let moduleID = pushData["ModuleID"] as? Int {
                    // print(moduleID)
                    redirectToController(moduleID)
                }
            }
        }
    }
    
    //MARK::- redirectToController
    func redirectToController(_ moduleID : Int) {
        
        switch moduleID {
            
        //circular and notice :
        case 1,3:
            guard let vc = ez.topMostVC as? CustomTabbarViewController else {return}
            vc.getIndexBasedOnModuleID(moduleID: moduleID)
            break
        default:
            break
        }
    }
}
