//
//  SchoolCell.swift
//  e-Care Pro
//  Created by ShubhamMac on 18/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
protocol  DeleteSchoolDelegate {
    func deleteSchool(_ index : Int)
}
class SchoolCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBg:        UIView?
    @IBOutlet weak var imgView:       UIImageView?
    @IBOutlet weak var lblSchoolCode: UILabel?
    @IBOutlet weak var btnCross:      UIButton?
    
    var index : Int?
    var selectSchoolCode : String?
    var delegate : DeleteSchoolDelegate?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? SchoolCodeTable , let index = index {
            viewBg?.addShadow(offset: CGSize(width: -1.0, height: 1.0), radius: 4.0, color: UIColor.flatLightBlack, opacity: 0.5)
            if let schoolImage = modal.schLogo?.getImageUrl() {
                imgView?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: nil)
            }
            lblSchoolCode?.text = modal.schCode
            btnCross?.tag = index
            btnCross?.isHidden =  modal.schCode?.lowercased() == selectSchoolCode?.lowercased()
        }
    }
    @IBAction func btnCrossAct(_ sender : UIButton){
        delegate?.deleteSchool(sender.tag)
    }
}
