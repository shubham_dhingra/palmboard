//  BoardreportCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class BoardreportCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblModuleName: UILabel?
    @IBOutlet weak var imgViewCell:   UIImageView?
    
}
