//
//  ClassCell.swift
//  PalmBoard
//
//  Created by Shubham on 05/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit

class ClassCell: UICollectionViewCell {

    @IBOutlet weak var lblTotalStudents : UILabel?
    @IBOutlet weak var lblTotalPresent : UILabel?
    @IBOutlet weak var lblTotalAbsent : UILabel?
    @IBOutlet weak var lblTotalLeave : UILabel?
    @IBOutlet weak var lblClassName : UILabel?
    @IBOutlet weak var lblTotalLateDays : UILabel?
    @IBOutlet weak var classView : UIView?{
        didSet {
            self.classView?.addShadow(offset: CGSize(width: 0, height: 1), radius: 4, color: UIColor.black.withAlphaComponent(0.18), opacity: 1.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var isLateEnabled : Bool = false
    var modal  : Any? {
        didSet{
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? MyClasse {
            let totalStudents = /modal.present + /modal.absent + /modal.leave
            lblTotalStudents?.text = "Total Students: \(totalStudents)"
            lblTotalPresent?.text = "Present: \(/modal.present)"
            lblTotalAbsent?.text = "Absent: \(/modal.absent)"
            lblTotalLeave?.text = "Leave: \(/modal.leave)"
            lblClassName?.text = "Class: \(/modal.className)"
             lblTotalLateDays?.isHidden = !isLateEnabled
             lblTotalLateDays?.text = isLateEnabled ? "Late: \(/modal.late)" : nil
         }
    }
}
