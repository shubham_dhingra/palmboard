//
//  UsersTypeCell.swift
//  e-Care Pro
//  Created by Shubham on 19/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class UsersTypeCell: UICollectionViewCell {
    
    @IBOutlet weak var lblType : UILabel?
    @IBOutlet weak var linerView : UIView?
    
    var selectIndex : Int = 0
    var currentIndex : Int = 0
    var model : Any? {
        didSet {
            configure()
        }
    }
    
    
    func configure() {
        if let model = model as? Users {
            lblType?.text = model.rawValue
            lblType?.textColor = selectIndex == currentIndex ? UIColor.flatGreen : UIColor.cccccc
            linerView?.isHidden = !(selectIndex == currentIndex)
        }
    }
    
}
