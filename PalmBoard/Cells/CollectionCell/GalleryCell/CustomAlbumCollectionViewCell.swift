//
//  CustomAlbumCollectionViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomAlbumCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBg:             UIView?
    @IBOutlet weak var imgView:            UIImageView?
    @IBOutlet weak var lblAlbumName:       UILabel?
    @IBOutlet weak var lblTotalDatePhotos: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius  = 5
        self.layer.masksToBounds = true
    }

}
