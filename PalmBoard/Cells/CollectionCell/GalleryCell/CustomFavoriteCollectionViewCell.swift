//
//  CustomFavoriteCollectionViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 27/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomFavoriteCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var viewBg:   UIView?
    @IBOutlet weak var imgView:  UIImageView?
    @IBOutlet weak var imgThumb: UIImageView?


}
