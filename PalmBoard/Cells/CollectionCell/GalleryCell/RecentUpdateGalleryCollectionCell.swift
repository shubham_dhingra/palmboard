
//
//  RecentUpdateGalleryCollectionCell.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 31/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class RecentGalleryCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var imgView    :  UIImageView?
    @IBOutlet weak var btnPlay    :  UIButton?
    
    var schoolCode : String?
    var isVideoGallery : Bool = false
    var AwsURL : String?
    
    var modal : Any? {
        didSet {
        configure()
        }
    }
    
     func configure() {
        if let modal = modal as? String ,let schoolCode = schoolCode {
            if !isVideoGallery {
                ez.runThisInMainThread {
                    self.imgView?.loadURL(imageUrl: modal.getUrlForGallery(fromAWS: self.AwsURL != nil, AWSURL: self.AwsURL, schoolCode: schoolCode), placeholder: nil, placeholderImage: R.image.photogalleryPlaceholder())
                }
            }
            else {
               let strVideoID = modal.extractYoutubeIdFromLink()
                guard let Id = strVideoID else{
                    return
                }
               let strPhotoURL = "https://img.youtube.com/vi/\(Id)/hqdefault.jpg"
                if let url = URL(string : strPhotoURL) {
                    imgView?.loadURL(imageUrl: url , placeholder: nil, placeholderImage : R.image.photogalleryPlaceholder())
                }
            }
        }
    }
}

