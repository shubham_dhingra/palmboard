//
//  customDetailsGalleryCollectionViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class customDetailsGalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBg:   UIView?
    @IBOutlet weak var imgView:  UIImageView?
    @IBOutlet weak var imgThumb: UIImageView?
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        self.layer.cornerRadius  = 5
//        self.layer.masksToBounds = true
//    }

}
