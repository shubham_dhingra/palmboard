//  customGalleryCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class customGalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBg:          UIView?
    @IBOutlet weak var imgView:         UIImageView?
    @IBOutlet weak var lblModuleName:   UILabel?
    @IBOutlet weak var lblTotalAlbum:   UILabel?
    @IBOutlet weak var favouriteWidth:  NSLayoutConstraint?
    @IBOutlet weak var favouriteHeight: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius  = 5
        self.layer.masksToBounds = true
    }

}
