//
//  HalfDayCell.swift
//  PalmBoard
//
//  Created by Shubham on 06/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
protocol DeleteHalfDayDelegate {
    func deleteHalfDay(index : Int?)
}
class HalfDayCell: UICollectionViewCell {
    
    @IBOutlet weak var lblLeaveDate : UILabel?
    @IBOutlet weak var lblLeaveDay :  UILabel?
    @IBOutlet weak var btnCross : UIButton?
    @IBOutlet weak var lblFirstDay : UILabel?
    @IBOutlet weak var containerView : UIView? {
        didSet{
            self.containerView?.setCornerRadius(radius: 6.0)
            self.containerView?.addBorder(width: 1.0, color: UIColor.flatBlack70Color)
        }
    }
    
    var delegate : DeleteHalfDayDelegate?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? HalfDayModal {
            lblLeaveDate?.text = modal.leaveDate?.dateToString(formatType:"dd-MM-yyyy")
            lblLeaveDay?.text = modal.leaveDate?.getDayOnParticularDate()
            lblFirstDay?.text = modal.halfType == 1 ? "First half" : "Second half"
        }
    }
    
    @IBAction func btnCrossAct(_ sender : UIButton) {
        delegate?.deleteHalfDay(index : sender.tag)
    }
}
