//  NWSImageToken.swift
//  NWSTokenView
//  Created by Shubham Dhingra on 8/11/15.
/*
Copyright (c) 2015 NitWit Studios, LLC
*/

import UIKit

protocol DeleteTokenFromCrossDelegate {
    func deleteToken(_ index : Int , _ object : Any?)
}
open class NWSImageToken: NWSToken
{
    @IBOutlet weak var imageView  : UIImageView?
    @IBOutlet weak var titleLabel : UILabel?
    @IBOutlet weak var classLabel : UILabel?
    @IBOutlet weak var btnDelete  : UIButton?
    @IBAction func btnDeleteAct(_ sender : UIButton){
        delegate?.deleteToken(sender.tag , object)
    }
  
    var delegate : DeleteTokenFromCrossDelegate?
    open class func initWithTitle(_ title : String? = nil, image: String? = nil, _ className : String? = nil ,  _ index : Int? = 0 , _ colorAssignToClass : UIColor? = nil) -> NWSImageToken?
    {
        if let token = UINib(nibName: "NWSImageToken", bundle:nil).instantiate(withOwner: nil, options: nil)[0] as? NWSImageToken
        {
            token.backgroundColor  = UIColor.white
            let oldTextWidth       = token.titleLabel?.bounds.width
            token.titleLabel?.text = /title
            token.titleLabel?.sizeToFit()
            token.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            token.btnDelete?.tag = /index
            
            if let url = image?.getImageUrl() {
                
                token.imageView?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: nil)
                token.classLabel?.isHidden = true
                token.imageView?.isHidden = false
            }
            
            if let className = className {

                token.classLabel?.backgroundColor = colorAssignToClass
                token.classLabel?.text  = /className.first?.toString
                token.classLabel?.isHidden = false
                token.imageView?.isHidden = true
            }
            
            let newTextWidth = token.titleLabel?.bounds.width

            token.layer.cornerRadius = 5.0
            token.clipsToBounds = true
            
            // Resize to fit text
            token.frame.size = CGSize(width: token.frame.size.width + (newTextWidth!-oldTextWidth!), height: token.frame.height)
            token.setNeedsLayout()
            token.frame = token.frame
            
            return token
        }
        return nil
    }
}
