//
//  AllReportsCell.swift
//  PalmBoard
//
//  Created by Shubham on 12/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import Charts

class AllReportsCell: UICollectionViewCell {

    @IBOutlet weak var pieChart_View:  PieChartView?
    @IBOutlet weak var barChartView : BarChartView!
    @IBOutlet weak var studentsColorView : UIView?
    @IBOutlet weak var parentColorView : UIView?
    @IBOutlet weak var staffColorView : UIView?
    
    var months = [String]()
    var valuesArr = [Double]()
    var colorsArr = [UIColor]()
    var modal : SurveyPieChartModal?
    var drawPieChartFirstTime : Bool?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
    func updatePieChart(){
       
        pieChart_View?.drawEntryLabelsEnabled = false
        pieChart_View?.highlightPerTapEnabled = false
        pieChart_View?.usePercentValuesEnabled = false
        pieChart_View?.legend.enabled         = false
        pieChart_View?.chartDescription?.text = ""
        
        guard let modal = modal  , let pieChartArr = modal.surveyUserDTL   else {
            return
        }
        
        let total = modal.total
        
        var pieChartEntryArr = [PieChartDataEntry]()
        var colorArr = [UIColor]()
        _ = pieChartArr.enumerated().map {(index ,value) in
            
           switch value.usertype {
           
           case 1:
            
            studentsColorView?.isHidden = modal.surveyUserDTL?.filter({ (survey) -> Bool in
                return survey.usertype == 1
            }).count == 0
            
            
            if value.total != 0 {
                pieChartEntryArr.append(PieChartDataEntry(value: Double(/value.total) , label: ""))
                colorArr.append(UIColor.pieChartStudentColor)
            }
            
           case 2:
            
            parentColorView?.isHidden = modal.surveyUserDTL?.filter({ (survey) -> Bool in
                return survey.usertype == 2
            }).count == 0
            
            
            if value.total != 0 {
                    pieChartEntryArr.append(PieChartDataEntry(value:  Double(/value.total) , label: ""))
                    colorArr.append(UIColor.pieChartParentColor)
            }
               
           case 3:
            
            staffColorView?.isHidden = modal.surveyUserDTL?.filter({ (survey) -> Bool in
                return survey.usertype == 3
            }).count == 0
            
            
            if value.total != 0 {
                 pieChartEntryArr.append(PieChartDataEntry(value:  Double(/value.total) , label: ""))
                 colorArr.append(UIColor.pieChartStaffColor)
            }
            
            default:
               break
            }
        }

        let dataSet = PieChartDataSet(values: pieChartEntryArr, label: " ")
        let data    = PieChartData(dataSet: dataSet)
        pieChart_View?.data = data
        
       
        let str1 = NSMutableAttributedString(string: "\(/total)", attributes: [NSAttributedString.Key.font:R.font.ubuntuBold(size: 18.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x2B2B2B)])
        let str2 = NSMutableAttributedString(string: "Total\n", attributes: [NSAttributedString.Key.font:R.font.ubuntuRegular(size: 16.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
       
      
        dataSet.colors                        = colorArr
        pieChart_View?.rotationEnabled        = true
        str2.append(str1)
        pieChart_View?.centerText   = str2.string
        
        //This must stay at end of function
        pieChart_View?.notifyDataSetChanged()
        pieChart_View?.showAnimate()
        
    }
    
    
    func updateBarGraph() {
        barChartView.isUserInteractionEnabled = false
        barChartView.noDataText = "You need to provide data for the chart."
//        barChartView.chartDescription?.text = ""
        setChart(dataPoints: months,values: valuesArr)

    }

    func setChart(dataPoints : [String], values: [Double]) {

        barChartView.noDataText = "You need to provide data for the chart."
        
        //legend
        let legend = barChartView.legend
        legend.enabled = true
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .vertical
        legend.drawInside = true
        legend.yOffset = 5.0
        legend.xOffset = 5.0
        legend.yEntrySpace = 0.0
        
        
        let xaxis = barChartView.xAxis
        //            xaxis.valueFormatter = axisFormatDelegate
        xaxis.drawGridLinesEnabled = false
        xaxis.labelPosition = .bottom
        xaxis.centerAxisLabelsEnabled = false
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = true
        barChartView.rightAxis.enabled = false
        
        
        var dataEntries: [BarChartDataEntry] = []

        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), yValues: [values[i]])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Option Color")
        chartDataSet.colors = colorsArr
        var dataSets = [IChartDataSet]()
        dataSets.append(chartDataSet)
        
        let chartData = BarChartData(dataSets: dataSets)
        chartData.barWidth = 0.3
       
        self.barChartView.data = chartData
        self.barChartView.chartDescription?.text = ""
        if /self.drawPieChartFirstTime {
        self.barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .linear)
        }
    }
}
