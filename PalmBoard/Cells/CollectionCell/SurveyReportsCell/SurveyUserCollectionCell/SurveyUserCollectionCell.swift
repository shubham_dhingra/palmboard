//
//  SurveyUserCollectionCell.swift
//  PalmBoard
//
//  Created by Shubham on 14/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit

class SurveyUserCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblUser : UILabel?
    @IBOutlet weak var linearView : UIView?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? SurveyUserDTL {
            lblUser?.text = modal.name
        }
    }
}
