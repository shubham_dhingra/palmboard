//
//  UserReportsCell.swift
//  PalmBoard
//
//  Created by Shubham on 12/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import Charts

class UserReportsCell: UICollectionViewCell {

    @IBOutlet weak var pieChart_View:      PieChartView?
    @IBOutlet weak var barChartView : BarChartView!
    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var tblHeightConstraint : NSLayoutConstraint?
    
    var modal : SurveyPieChartModal?
    var labelsForTableArr = [String]()
    var drawPieChartFirstTime : Bool?
    var labelsArr = [String]() {
        didSet {
            labelsForTableArr = []
            labelsForTableArr.append("Option")
            labelsArr.enumerated().map {(_,value)  in
                labelsForTableArr.append(value)
            }
        }
    }
    var  maleArr = [Int]()
    var femaleArr =  [Int]()
    var totalResponseArr = [Int]()
    var totalUsersArr = [Int]()
    var isFirstTime : Bool = true
    var colorArr = [UIColor]()
    var tableDataSource : TableViewDataSource? {
        didSet {
            tableView?.delegate = tableDataSource
            tableView?.dataSource = tableDataSource
            tableView?.reloadData()
        }
    }
    
    var value : SurveyUserDTL?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView?.register(R.nib.surveyGrid)
    }

    
    func updatePieChart(){
        
        var pieChartEntryArr = [PieChartDataEntry]()
        colorArr = []
        guard let value = value else {
            return
        }
        
        if value.male != 0 {
            pieChartEntryArr.append(PieChartDataEntry(value: Double(/value.male), label: ""))
            colorArr.append(UIColor.pieChartStudentColor)
        }
        if value.female != 0 {
            pieChartEntryArr.append(PieChartDataEntry(value: Double(/value.female), label: ""))
            colorArr.append(UIColor.pieChartParentColor)
        }
        
        
        let dataSet = PieChartDataSet(values: pieChartEntryArr, label: "")
        let data    = PieChartData(dataSet: dataSet)
        pieChart_View?.data = data
        pieChart_View?.chartDescription?.text = ""
        
        
        let str1 = NSMutableAttributedString(string: "Total\n", attributes: [NSAttributedString.Key.font:R.font.ubuntuRegular(size: 16.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
        let str2 = NSMutableAttributedString(string: "\(/value.total)", attributes: [NSAttributedString.Key.font:R.font.ubuntuBold(size: 18.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x2B2B2B)])
        
        str1.append(str2)
        pieChart_View?.centerText   = str1.string
        pieChart_View?.drawEntryLabelsEnabled = false
        pieChart_View?.chartDescription?.text = ""
        pieChart_View?.highlightPerTapEnabled = true
        pieChart_View?.legend.enabled         = false
        dataSet.colors                        = colorArr
        pieChart_View?.rotationEnabled        = true
        
        
        //This must stay at end of function
        pieChart_View?.notifyDataSetChanged()
    }
    
    func updateBarGraph() {
        
        
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.isUserInteractionEnabled = false
        //legend
        let legend = barChartView.legend
        legend.enabled = true
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .vertical
        legend.drawInside = true
        legend.yOffset = 10.0
        legend.xOffset = 10.0
        legend.yEntrySpace = 0.0
        
        let xaxis = barChartView.xAxis
        //            xaxis.valueFormatter = axisFormatDelegate
        xaxis.drawGridLinesEnabled = false
        xaxis.labelPosition = .bottom
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values:self.labelsArr)
        xaxis.granularity = 1
        
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        let yaxis = barChartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = true
        
        barChartView.rightAxis.enabled = false
        //axisFormatDelegate = self
        
        setChart()
    }
    
    
    func setChart() {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.chartDescription?.text = ""
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        
        for i in 0..<self.labelsArr.count {
            
            let dataEntry = BarChartDataEntry(x: Double(i) , y: self.maleArr[i].toDouble)
            dataEntries.append(dataEntry)
            
            let dataEntry1 = BarChartDataEntry(x: Double(i) , y: self.self.femaleArr[i].toDouble)
            dataEntries1.append(dataEntry1)
        
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Male")
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Female")
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
        chartDataSet.colors = [UIColor.pieChartStudentColor]
        chartDataSet1.colors = [UIColor.pieChartParentColor]
      
        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3
        
        let groupCount = self.labelsArr.count
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        barChartView.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChartView.xAxis.axisMaximum = Double(startYear) + gg * Double(groupCount)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        //chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChartView.notifyDataSetChanged()
        
        barChartView.data = chartData
        
        //background color
        barChartView.backgroundColor = UIColor.white
        
        //chart animation
        if /drawPieChartFirstTime {
         barChartView.animate(xAxisDuration: 1.50, yAxisDuration: 1.50, easingOption: .linear)
        }
    }
    }
    

extension UserReportsCell {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: labelsForTableArr , height: 40.0, tableView: tableView, cellIdentifier: R.reuseIdentifier.surveyGridCell.identifier)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            if let index = indexpath?.row {
                
                if index != 0 {
                    (cell as? SurveyGridTableViewCell)?.maleCount = self.maleArr[index - 1]
                    (cell as? SurveyGridTableViewCell)?.femaleCount = self.femaleArr[index - 1]
                    (cell as? SurveyGridTableViewCell)?.totalAllUserCount = self.totalUsersArr[index - 1]
                    (cell as? SurveyGridTableViewCell)?.totalSpecificUserCount = self.totalResponseArr[index - 1]
                }
                (cell as? SurveyGridTableViewCell)?.index = index
                (cell as? SurveyGridTableViewCell)?.modal = item
            }
            
        }
    }
    
    func reloadTable() {
        
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else{
            tableDataSource?.items = labelsForTableArr
            tableView?.reloadData()
        }
        tblHeightConstraint?.constant = CGFloat(labelsForTableArr.count * 40)
        tableView?.isScrollEnabled = false
    }
}

