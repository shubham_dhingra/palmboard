//  StaffSyllabusCollectionViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 02/05/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import UIKit

class StaffSyllabusCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSubjectName: UILabel?
    @IBOutlet weak var lblLine: UILabel?
    
    var index      : Int?
    var eventIndex : Int?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? String {
            let themeColor: String    = /DBManager.shared.getAllSchools().first?.themColor
            lblSubjectName?.text      = modal
            lblLine?.tag              = /index + 1
            lblLine?.backgroundColor  = eventIndex == /index ? themeColor.hexStringToUIColor() : UIColor.clear
            lblSubjectName?.textColor = eventIndex == /index ? themeColor.hexStringToUIColor() : UIColor.flatBlack
        }
    }
}
