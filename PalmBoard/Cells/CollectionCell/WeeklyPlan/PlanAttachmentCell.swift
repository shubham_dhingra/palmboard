

//
//  PlanAttachmentCell.swift
//  PalmBoard
//
//  Created by Shubham on 27/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

protocol DeleteAttachmentDelegate {
    func deleteAttachment(tag : Int)
}

class PlanAttachmentCell: UICollectionViewCell {
    
    var delegate : DeleteAttachmentDelegate?
    var user : Users?
    
    @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var btnDelete : UIButton?
    @IBOutlet weak var viewWidthHeightConstraint : NSLayoutConstraint?
    
    func updateUI() {
        self.contentView.addBorder(width: 1.0, color: user == .Cordinator ? UIColor.flatGreen  : UIColor.flatBlack)
        lblName?.textColor = user == .Cordinator ? UIColor.flatGreen  :UIColor.flatBlack
        self.contentView.setCornerRadius(radius: 2.0)
    }
    
    @IBAction func btnDeleteAct(_ sender : UIButton){
        delegate?.deleteAttachment(tag : sender.tag)
    }
}
