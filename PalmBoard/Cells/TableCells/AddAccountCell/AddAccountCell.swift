//  AddAccountCell.swift
//  e-Care Pro
//  Created by ShubhamMac on 04/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.

protocol SelectAccountDelegate{
    func selectAccount(_ index : Int)
}
import UIKit

class AddAccountCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblName         : UILabel?
    @IBOutlet weak var imgUser         : UIImageView?{
        didSet {
            imgUser?.layer.cornerRadius = /imgUser?.frame.size.width / 2
            imgUser?.clipsToBounds = true
        }
    }
    @IBOutlet weak var imgSch          : UIImageView?
    @IBOutlet weak var lblSchName      : UILabel?
    @IBOutlet weak var lblSchLocation  : UILabel?
    @IBOutlet weak var btnAddRemoveAcc : UIButton?
    
    
    //extra outlets
    @IBOutlet weak var lblStudentName : UILabel?
    @IBOutlet weak var imgStudent     : UIImageView?
    @IBOutlet weak var childView      : UIView?
    
    //MARK::- VARIABLES
    var modal : Any?{
        didSet {
            configure()
        }
    }
    var index : Int?
    var delegate : SelectAccountDelegate?
    
    //MARK::- FUNCTIONS
    func configure() {
        
        if let modal = modal as? UserTable {
            lblName?.text        = "\(/modal.name) (\(/modal.role))"
            lblSchName?.text     = modal.schCodes?.schName
            lblSchLocation?.text = modal.schCodes?.schCity
            btnAddRemoveAcc?.setImage(UIImage(named : modal.isActive == 1 ? "Right Selected" : "Cross_ac") , for: .normal)
            if let index = index {
                btnAddRemoveAcc?.tag = index
            }
            
            if let schoolImage = modal.schCodes?.schLogo?.getImageUrl() {
                imgSch?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: nil)
            }
            
            if let userImage = modal.photoUrl?.getImageUrl() {
                imgUser?.clearImageFromCache(validUrl: userImage)
                imgUser?.loadURL(imageUrl: userImage, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            }
            else {
                imgUser?.image = R.image.noProfile_Big()
            }
            //When User is Parent to Show there child Information
            childView?.isHidden = modal.userType != 2
            
            //childImage
            if let childImage = modal.childPhoto?.getImageUrl() {
                imgStudent?.loadURL(imageUrl: childImage, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            }
            else {
                imgStudent?.image = R.image.noProfile_Big()
            }
            
            lblStudentName?.text = "\(/modal.childName) (\(/modal.childClass))"
        }
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnAddRemoveAccAct(_ sender : UIButton){
        delegate?.selectAccount(sender.tag)
    }
}
