//  AttendanceReportDetailsTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 26/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class AttendanceReportDetailsTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
class AttendanceAllCell : UITableViewCell {
    
    @IBOutlet weak var calView:             UIView?
    @IBOutlet weak var lblAttendanceDate:   UILabel?
    @IBOutlet weak var lblAttendanceDay:    UILabel?
    @IBOutlet weak var lblAttendanceStatus: UILabel?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? Attendance {
            lblAttendanceDate?.text   = /modal.attDate?.dateChangeFormat()
            lblAttendanceDay?.text    = /modal.attDate?.dateChangeFormatDays()
            lblAttendanceStatus?.isHidden = modal.status == 0
            switch /modal.status
            {
            case 1:
                lblAttendanceStatus?.text = "P"
                lblAttendanceStatus?.backgroundColor = /modal.isLate ? UIColor(rgb: 0xffbb00) : UIColor(rgb: 0x78CC41)
            case 2:
                lblAttendanceStatus?.text            = "A"
                lblAttendanceStatus?.backgroundColor = UIColor(rgb: 0xFB4E4E)
            case 3:
                lblAttendanceStatus?.text = "L"
                lblAttendanceStatus?.backgroundColor = UIColor(rgb: 0x44A8EB)
            default:
                
                break
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
    override func awakeFromNib() {
        lblAttendanceStatus?.layer.cornerRadius = (lblAttendanceStatus?.frame.width)! / 2
        lblAttendanceStatus?.clipsToBounds      = true
    }
}
class AttendanceReportCell : UITableViewCell {
    
    @IBOutlet weak var lblSchoolDays: UILabel?
    @IBOutlet weak var lblPresent:    UILabel?
    @IBOutlet weak var lblAbsent:     UILabel?
    @IBOutlet weak var lblLeave :     UILabel?
    @IBOutlet weak var lblLateDays : UILabel?
    
    var isLateEnabled : Bool?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? AttSummary {
            
            lblSchoolDays?.text = "School days\n\(/modal.schoolDays)"
            lblPresent?.text    = "Present\n\(/modal.prasent)"
            lblAbsent?.text     = "Absent\n\(/modal.absent)"
            lblLeave?.text      = "Leave\n\(/modal.leave)"
            lblLateDays?.text      = "Late Days\n\(/modal.late)"
            
        }
        lblLateDays?.isHidden = !(/isLateEnabled)
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
}

class AttendanceReportHeaderCell : UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    
    var strMonth = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let check = DBManager.shared.getAuthenticatedUser().first?.sessionStart  {
            strMonth =  check
        }
        strMonth     = strMonth.dateChangeFormatMonth()
        lblDate.text = "From \(strMonth) - Till Today"
    }
}

class AttendanceReportDetailCell : UITableViewCell {
    
    
    @IBOutlet weak var lblTotalSchoolDays: UILabel?
    @IBOutlet weak var lblTotalPresents:   UILabel?
    @IBOutlet weak var lblTotalAbsents:    UILabel?
    @IBOutlet weak var lblTotalLeaves :    UILabel?
    @IBOutlet weak var lblTotalLate :      UILabel?
    @IBOutlet weak var lblTotalLateValue : UILabel?
    
    var isLateEnabled : Bool?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? AttSummary {
            
            lblTotalSchoolDays?.text = "\(/modal.totalSchoolDays)"
            lblTotalPresents?.text   = "\(/modal.totalPrasent)"
            lblTotalAbsents?.text    = "\(/modal.totalAbsent)"
            lblTotalLeaves?.text     = "\(/modal.totalLeave)"
            lblTotalLate?.text =    "\(/modal.totalLateDays)"
        
        }
        
        lblTotalLateValue?.isHidden = !(/isLateEnabled)
        lblTotalLate?.isHidden = !(/isLateEnabled)
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
}

class AttendanceReportPercentCell : UITableViewCell {
   
    @IBOutlet weak var lblPresentCircle: UILabel?
    @IBOutlet weak var lblAbsentCircle:  UILabel?
    @IBOutlet weak var lblLeaveCircle :  UILabel?
    @IBOutlet weak var lblLateCircle : UILabel?
    @IBOutlet weak var lblLateDaysValue : UILabel?
    @IBOutlet weak var heightConstant : NSLayoutConstraint?
    
    var isLateEnabled : Bool?
    var heightFrame : CGFloat?

    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? AttSummary {
            
            var percentFloat =  Float()
            var AbsentFloat  =  Float()
            var leaveFloat   =  Float()
            var lateFloat    =  Float()
//           heightConstant?.constant = /isLateEnabled ? ((UIScreen.main.bounds.width - 50) / 4) : ((UIScreen.main.bounds.width - 50) / 3)
            percentFloat     =  (Float((Float(/modal.totalPrasent) / Float(/modal.totalSchoolDays)) )) * 100
            AbsentFloat      = (Float((Float(/modal.totalAbsent) / Float(/modal.totalSchoolDays)) )) * 100
            leaveFloat  = (Float((Float(/modal.totalLeave) / Float(/modal.totalSchoolDays)) )) * 100
            lateFloat = (Float((Float(/modal.totalLateDays) / Float(/modal.totalSchoolDays)) )) * 100

            if !(percentFloat.isNaN || AbsentFloat.isInfinite || leaveFloat.isNaN)
            {
                lblPresentCircle?.text  = "\( String(format: "%.2f",percentFloat))%"
                lblAbsentCircle?.text   = "\(String(format: "%.2f",AbsentFloat))%"
                lblLeaveCircle?.text    = "\(String(format: "%.2f",leaveFloat))%"
                lblLateCircle?.text =     "\(String(format: "%.2f",lateFloat))%"
            }
            else{
                lblPresentCircle?.text  = "0%"
                lblAbsentCircle?.text   = "0%"
                lblLeaveCircle?.text    = "0%"
                lblLateCircle?.text     = "0%"
            }
            lblLateCircle?.isHidden = !(/isLateEnabled)
            lblLateDaysValue?.isHidden           = !(/self.isLateEnabled)
            lblPresentCircle?.layer.cornerRadius = CGFloat(/self.heightFrame / 2)//(/lblPresentCircle?.frame.size.width) / 2
            lblAbsentCircle?.layer.cornerRadius  = CGFloat(/self.heightFrame / 2)//(/lblAbsentCircle?.frame.size.width) / 2
            lblLeaveCircle?.layer.cornerRadius   = CGFloat(/self.heightFrame / 2)//(/lblLeaveCircle?.frame.size.width) / 2
            if /self.isLateEnabled {
                lblLateCircle?.layer.cornerRadius  = CGFloat(/self.heightFrame / 2)//(/lblLateCircle?.frame.size.width) / 2
            }
            self.contentView.setNeedsLayout()
            self.contentView.layoutIfNeeded()
        }
        
       
    }
   
}
