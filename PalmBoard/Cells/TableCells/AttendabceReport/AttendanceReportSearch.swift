//
//  AttendanceReportSearch.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 28/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class AttendanceReportSearch: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? Result {
            lblName?.text       = "\(/modal.name), \(/modal.className)"
            
            if let imgUrl = modal.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" + "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                // print("imgUrl \n \(url!)")
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgView?.image = img1
                }
            }
            else{
                self.imgView?.image = R.image.noProfile_Big()
            }
        }
    }
}
