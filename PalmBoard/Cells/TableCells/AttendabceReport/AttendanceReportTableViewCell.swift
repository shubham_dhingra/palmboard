//  AttendanceReportTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.


import UIKit
class AttendanceReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgStudent:          UIImageView?
    @IBOutlet weak var lblName:             UILabel?
    @IBOutlet weak var lblAttendanceStatus: UILabel?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? AttReport {
            lblName?.text                 = modal.name
            lblAttendanceStatus?.isHidden = modal.status == 0
            switch /modal.status
            {
            case 1:
                lblAttendanceStatus?.text = "P"
                lblAttendanceStatus?.backgroundColor = /modal.isLate ? UIColor(rgb: 0xffbb00) :  UIColor(rgb: 0x78CC41)
            case 2:
                lblAttendanceStatus?.text = "A"
                lblAttendanceStatus?.backgroundColor = UIColor(rgb: 0xFB4E4E)
            case 3:
                lblAttendanceStatus?.text = "L"
                lblAttendanceStatus?.backgroundColor = UIColor(rgb: 0x44A8EB)
                
            default:
                break
            }
            if let imgUrl = modal.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser    = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.no_profile_que(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgStudent?.image = img1
                }
            }
            else{
                self.imgStudent?.image = R.image.no_profile_que()
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgStudent?.clipsToBounds      = true
        imgStudent?.layer.cornerRadius = (imgStudent?.frame.size.width)! / 2
        
        lblAttendanceStatus?.clipsToBounds      = true
        lblAttendanceStatus?.layer.cornerRadius = (lblAttendanceStatus?.frame.size.width)! / 2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}
