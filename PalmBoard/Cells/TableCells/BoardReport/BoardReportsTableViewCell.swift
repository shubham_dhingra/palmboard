//  BoardReportsTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class BoardReportsTableViewCell: UITableViewCell {
    @IBOutlet weak var btnReport: UIButton?
    @IBOutlet weak var collectionViewReport: UICollectionView?
    
    var arrayStaffRPT = [[String : Any]]()
    var arraySubReports = [[String : Any]]()
    var isFirstTime : Bool = true
    var fromVc : UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewReport?.register(UINib(nibName: "BoardreportCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellReportCollection")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
extension BoardReportsTableViewCell {
    
    func reloadStaffCollectionModule() {
        
        if isFirstTime {
            collectionViewReport?.reloadData()
            isFirstTime = false
        }
    }
    
    func setUpStaffModule(_ arrayStaffRPT : [[String : Any]]?, _ arraySubReports : [[String : Any]]?){
        if let arr1 = arrayStaffRPT {
            self.arrayStaffRPT = arr1
        }
        
        if let arr2 = arraySubReports {
          self.arraySubReports = arr2
        }
        
        collectionViewReport?.delegate = self
        collectionViewReport?.dataSource = self
        collectionViewReport?.reloadData()
    }
}

extension BoardReportsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayStaffRPT.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellReportCollection", for: indexPath) as! BoardreportCollectionViewCell
        
        if let strTitle = self.arrayStaffRPT[indexPath.row]["Title"] as? String
        {
            cell.lblModuleName?.text =  strTitle
        }
        // cell.backgroundColor = UIColor.red
        // print("cell frame = \(cell.frame) ")
        
        let intReport = self.arrayStaffRPT[indexPath.row]["RptID"] as! Int
        
        switch intReport {
        case 1:
            cell.imgViewCell?.image = R.image.studentsReports()
        case 2:
            cell.imgViewCell?.image =  R.image.statisticalReports()
        case 3:
            cell.imgViewCell?.image =  R.image.teacherlist_report()
        case 4:
            cell.imgViewCell?.image =  R.image.sms_report()
        case 5:
            cell.imgViewCell?.image =  UIImage(named:"TimeTable")
        case 6:
            cell.imgViewCell?.image =  R.image.executiveAttendance()
        case 7:
            cell.imgViewCell?.image =  R.image.feeReport()
        case 8:
            cell.imgViewCell?.image =  R.image.convReport()
        case 9:
            cell.imgViewCell?.image = R.image.weeklyPlan_report()
        case 10:
              cell.imgViewCell?.image  =  R.image.survey_reports_icon()
        default:
            break
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        
        let intReport = self.arrayStaffRPT[indexPath.row]["RptID"] as! Int
        
        if intReport == 4 {
            // self.performSegue(withIdentifier: "smsStaffReportreport", sender: nil)
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let smsVc = storyboard.instantiateViewController(withIdentifier: "SMSReportViewController") as? SMSReportViewController else {return}
            fromVc?.pushVC(smsVc)
        }
        else if intReport == 5 {
            (fromVc as? BoardViewController)?.performSegue(withIdentifier: "timetableStaffReport", sender: nil)
        }
            
        else if intReport == 6 {
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let smsVc = storyboard.instantiateViewController(withIdentifier: "ExecutiveAttendanceReportViewController") as? ExecutiveAttendanceReportViewController else {return}
          fromVc?.pushVC(smsVc)
        }
            
        else if intReport == 7 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let feeVc = storyboard.instantiateViewController(withIdentifier: "FeeReportViewController") as? FeeReportViewController else {return}
           fromVc?.pushVC(feeVc)
        }
            
            
        else if intReport == 8 {
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let smsVc = storyboard.instantiateViewController(withIdentifier: "ConversationReportViewController") as? ConversationReportViewController else {return}
            fromVc?.pushVC(smsVc)
        }
            
        else if intReport == 9 {
            let storyboard = Storyboard.Main.getBoard()
            
            if let vc = storyboard.instantiateViewController(withIdentifier: "ClassmatesTeachers") as? ClassmatesTeachersViewController {
                vc.intListChoice = 0
                vc.users = .Cordinator
                fromVc?.pushVC(vc)
            }
        }
        else if intReport == 10 {
            let storyboard = Storyboard.Module.getBoard()
            
            if let vc = storyboard.instantiateViewController(withIdentifier: "SurveyViewController") as? SurveyViewController {
                vc.forCheckResponses = true
                fromVc?.pushVC(vc)
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let subReportsVc = storyboard.instantiateViewController(withIdentifier: "SubReportsViewController") as? SubReportsViewController else {return}
            
            
            if let array = self.arrayStaffRPT[indexPath.row]["SubReports"] as? [[String: Any]]{
                self.arraySubReports = array
            }
            if let strSubReportsTitle1 = self.arrayStaffRPT[indexPath.row]["Title"] as? String{
                subReportsVc.strTitle    = getTitle(strSubReportsTitle1)
            }
            subReportsVc.arraySubReports = self.arraySubReports
          fromVc?.pushVC(subReportsVc)
        }
    }
    func getTitle(_ subTitle : String?) -> String{
        
        switch subTitle {
        case HeaderTitle.Students.rawValue:
            return HeaderTitle.Students.getTitle()
            
        case HeaderTitle.Teachers.rawValue:
            return HeaderTitle.Teachers.getTitle()
            
        case HeaderTitle.Statistical.rawValue:
            return HeaderTitle.Statistical.getTitle()
            
        default :
            return subTitle ?? ""
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100.0, height: 118.0)
    }
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//          return CGSize(width: 45.0, height: 45.0)
////        return CGSize(width: 96.0, height: 118.0)
//    }
}
