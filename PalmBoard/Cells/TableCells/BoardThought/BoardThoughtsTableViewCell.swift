//
//  BoardThoughtsTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 06/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class BoardThoughtsTableViewCell: UITableViewCell {
    @IBOutlet weak var viewThoughtSlider: UIView?
    @IBOutlet weak var btnViewAll: UIButton?
    @IBOutlet weak var btnThoughts: UIButton?
    @IBOutlet weak var heightThoughts: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
