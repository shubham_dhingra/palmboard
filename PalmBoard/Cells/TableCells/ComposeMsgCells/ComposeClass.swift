//
//  ComposeClass.swift
//  e-Care Pro
//
//  Created by NupurMac on 21/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import UIKit


class ComposeContact: UITableViewCell {
    @IBOutlet weak var lblName:   UILabel?
    @IBOutlet weak var lblClass:  UILabel?
    @IBOutlet weak var imgView:   UIImageView?
    @IBOutlet weak var btnSelectContact: UIButton?
    
    override func awakeFromNib() {super.awakeFromNib()}
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        imgView?.layer.cornerRadius  = (imgView?.frame.width)! / 2
        imgView?.layer.masksToBounds = true
    }
}
