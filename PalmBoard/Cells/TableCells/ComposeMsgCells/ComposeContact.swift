//  ComposeContact.swift
//  e-Care Pro
//  Created by NupurMac on 21/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import UIKit

protocol ViewParentDelegate {
    func viewParent(_ index : Int)
}

class ComposeClass: UITableViewCell {
    
    @IBOutlet weak var lblClass:       UILabel?
    @IBOutlet weak var btnSelectClass: UIButton?
    @IBOutlet weak var btnViewParent:  UIButton?
        
    
    var delegate : ViewParentDelegate?
    var assignIndex : Int = 2
    
    func setUpText() {
        let yourAttributes : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
        ]
     let attributeString = NSMutableAttributedString(string: assignIndex == 2 ? "View Parent" : "View Student" , attributes: yourAttributes)
                btnViewParent?.setAttributedTitle(attributeString, for: .normal)
    }
    
    @IBAction func btnViewParentAct(_ sender : UIButton) {
        delegate?.viewParent(sender.tag)
    }
}
