//  ComposeHeader.swift
//  e-Care Pro
//  Created by NupurMac on 21/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import UIKit

class ComposeHeader: UITableViewCell {
    @IBOutlet weak var lblSelect:           UILabel?
    @IBOutlet weak var btnSelectAllContact: UIButton?
    
    override func awakeFromNib() {super.awakeFromNib()}
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btnSelectAllContact?.layer.borderWidth   = 2
        btnSelectAllContact?.layer.borderColor   = UIColor.black.cgColor//UIColor(rgb: 0xDBDBDB).cgColor
        btnSelectAllContact?.layer.cornerRadius  = 5
        btnSelectAllContact?.layer.masksToBounds = true
    }
}
