//
//  ConversationDetailsTableViewCell.swift
//  e-Care Pro
//
//  Created by FTC on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class ConversationDetailsTableViewCell: UITableViewCell {//convDetails

    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblBody: UILabel?
    @IBOutlet weak var lblDate: UILabel?
    @IBOutlet weak var imgView: UIImageView?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? MsgDTL {
            lblBody?.text    = modal.body
            lblName?.text    = modal.senderDTL?.name
            lblDate?.text    = modal.sentOn?.dateChangeFormat()
            
          //  var strName            = String()
            
            if let SenderType = modal.senderDTL?.senderType {
                
                if SenderType == 1 || SenderType == 2
                {
                    var strChildName  = String()
                    var strClass      = String()
                    var strName       = String()
                    
                    if let ChildName = modal.senderDTL?.childName as? String{
                        strChildName = ChildName
                    }
                    
                    if let ClassName = modal.senderDTL?.className as? String{
                        strClass = ClassName
                    }
                    
                    if let name = modal.senderDTL?.name{
                        strName = name
                    }
                    
                    if SenderType == 1{
                        lblName?.text = "\(strName), (\(strClass))"
                    }
                    else if SenderType == 2{
                        lblName?.text = "Parent of \(strChildName), (\(strClass))"
                    }
                }
                else if let name = modal.senderDTL?.name{
                    lblName?.text = "\(name)"
                    if let ChildName = modal.senderDTL?.designation{
                        lblName?.text = "\(name), \(ChildName)"
                    }
                    if let Abbreviation = modal.body{
                        lblBody?.text    =  Abbreviation
                    }
                }
                
            if let imgUrl = modal.senderDTL?.photo            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgView?.image = img1
                }
            }
            else{
                self.imgView?.image = R.image.noProfile_Big()
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgView?.clipsToBounds      = true
        imgView?.layer.cornerRadius = (imgView?.frame.size.width)! / 2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}

