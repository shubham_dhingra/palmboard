//  ConversationSearchTableViewCell.swift
//  e-Care Pro
//  Created by FTC on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class ConversationSearchTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTo:    UILabel?
    @IBOutlet weak var lblBody:  UILabel?
    @IBOutlet weak var lblDate:  UILabel?
    @IBOutlet weak var lblFrom:  UILabel?
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var imgView:  UIImageView?
    @IBOutlet weak var imgMedia : UIImageView?
    @IBOutlet weak var imgMediaHeightConstraint :  NSLayoutConstraint?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? Conversation {
            lblTitle?.text   = modal.subject
            lblFrom?.text    = "From: \(/modal.senderDTL?.name)"
            lblDate?.text    = modal.sentOn?.dateChangeFormat()
            lblBody?.text    = modal.abbreviation
            
            if (/modal.recipients?.count) > 1 {
                lblTo?.text      = "To: \(/modal.recipients?[0].name), more"
            }
            else{
                lblTo?.text      = "To: \(/modal.recipients?[0].name)"
            }
            
            imgMediaHeightConstraint?.constant = modal.msgType == 1 ? 0 : 16
            imgMedia?.isHidden = modal.msgType == 1
            
            if let imgUrl = modal.recipients?[0].photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgView?.image = img1
                }
            }
            else{
                self.imgView?.image = R.image.noProfile_Big()
            }
            
            switch /modal.msgType  {
                
            // Text
            case 1 :
                if let abbreviation = modal.abbreviation {
                    lblBody?.text = abbreviation
                }
                
            //Image
            case 2:
                let attributedString = Utility.shared.getAttributedString(firstStr: "Image ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
                lblBody?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "Camera_red-01-min")
                break
                
            //Audio
            case 3:
                let attributedString = Utility.shared.getAttributedString(firstStr: "Audio ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
                lblBody?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "Microphone-01-min")
                break
                
            // sms msg
            case 4:
                let attributedString = Utility.shared.getAttributedString(firstStr: "SMS ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
                lblBody?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "SMS-01-min")
                break
                
            default:
                break
            }
            
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgView?.clipsToBounds       = true
        imgView?.layer.cornerRadius = (imgView?.frame.size.width)! / 2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}
class ConversationSearchMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMenu:    UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}


