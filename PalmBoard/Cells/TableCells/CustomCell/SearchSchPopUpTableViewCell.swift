//
//  SearchSchPopUpTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant on 10/13/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class SearchSchPopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var lblSchoolNmae: UILabel?
    @IBOutlet weak var lblAddress: UILabel?
    @IBOutlet weak var lbl1: UILabel?
    @IBOutlet weak var lbl2: UILabel?
    @IBOutlet weak var lbl3: UILabel?
    @IBOutlet weak var lbl4: UILabel?
    @IBOutlet weak var lbl5: UILabel?
    @IBOutlet weak var lbl6: UILabel?
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
