//  ExecutiveAttendanceCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 25/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import UIKit

class ExecutiveAttendanceCell: UITableViewCell {
    
    @IBOutlet weak var lblName:           UILabel?
    @IBOutlet weak var lblDesignation:    UILabel?
    @IBOutlet weak var lblIsPrasent:      UILabel?
    @IBOutlet weak var lblLateBy:         UILabel?
    @IBOutlet weak var lblFirstIn:        UILabel?
    @IBOutlet weak var lblFirstInHeading: UILabel?
    @IBOutlet weak var lblFirstOut:       UILabel?
    @IBOutlet weak var lblFirstOutheading:UILabel?
    @IBOutlet weak var lblSecondIn:       UILabel?
    @IBOutlet weak var imgClock:          UIImageView?
    @IBOutlet weak var lblSecondOut:      UILabel?
    @IBOutlet weak var imgPhoto:          UIImageView?
    @IBOutlet var AttendanceStack:        [UIStackView]!
    
    @IBOutlet weak var stackViewFirstIn:  UIStackView?
    @IBOutlet weak var stackViewFirstOut: UIStackView?
    @IBOutlet weak var stackViewSecondIn: UIStackView?
    @IBOutlet weak var stackViewSecondOut: UIStackView?
    
    var intVCselection : Int?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? DTL
        {
            print("intVCselection = \(/intVCselection)")
            lblName?.text                 = modal.name
            lblDesignation?.text          = modal.designation
            lblIsPrasent?.text            = /modal.isPrasent ? "P" : "A"
            lblIsPrasent?.backgroundColor = /modal.isPrasent ? UIColor(rgb: 0x78CC41) : UIColor(rgb: 0xFB4E4E)
            lblLateBy?.isHidden           = !(/modal.isPrasent)
            imgClock?.isHidden            = !(/modal.isPrasent)

            for (_,value) in AttendanceStack.enumerated() {
                value.isHidden = !(/modal.isPrasent)
            }
            for (_,value) in AttendanceStack.enumerated() {
                if value.tag == 3 {
                    value.isHidden = true
                    lblFirstInHeading?.text       = "↓ In"
                }
                if value.tag == 4 {
                    value.isHidden = true
                    lblFirstOutheading?.text      =  "↑ Out"
                }
            }
            lblLateBy?.isHidden = modal.lateBy == nil
            let checkOfNull = intVCselection == 1 ? "00.00"  : "00:00"
            if let LateBy  = modal.lateBy //self.arrayCopyAttendance[indexPath.row]["LateBy"] as? String
            {
                if LateBy != checkOfNull  {
                    lblLateBy?.isHidden = false
                    imgClock?.isHidden = false
                    lblLateBy?.text = "Late By: " + LateBy
                    let amountText = NSMutableAttributedString.init(string: (lblLateBy?.text)!)
                    let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 16.0)!,
                                      NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
                    amountText.setAttributes(attributes, range:  NSMakeRange(0, 7))
                    lblLateBy?.attributedText = amountText
                }
                else {
                    lblLateBy?.isHidden = true
                    imgClock?.isHidden = true
                }
            }
            if let count = modal.markOn?.count {
                for i in 0..<count
                {
                    if i == 0 {
                        lblFirstIn?.text = modal.markOn?[i].markOn
                    }
                    else if i == 1 {
                        lblFirstOut?.text = modal.markOn?[i].markOn
                    }
                    else if i == 2 {
                        lblSecondIn?.text = modal.markOn?[i].markOn
                    }
                    else if i == 3 {
                        lblSecondOut?.text = modal.markOn?[i].markOn
                    }
                }
            }
            if let imgUrl = modal.photo?.getImageUrl()
            {
                imgPhoto?.loadURL(imageUrl: imgUrl, placeholder: nil, placeholderImage: nil)
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lblIsPrasent?.layer.cornerRadius  = (lblIsPrasent?.frame.size.width)! / 2
        lblIsPrasent?.layer.masksToBounds = true
    }
}
