//
//  FindSchoolCell.swift
//  eSmartGuard
//
//  Created by Shubham on 11/09/18.
//  Copyright © 2018 Franciscan Solutions Pvt. Ltd. All rights reserved.
//

import UIKit

class FindSchoolCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var lblSchoolNmae: UILabel?
    @IBOutlet weak var lblAddress: UILabel?
    @IBOutlet weak var lbl1: UILabel?
    @IBOutlet weak var lbl2: UILabel?
    @IBOutlet weak var lbl3: UILabel?
    @IBOutlet weak var lbl4: UILabel?
    @IBOutlet weak var lbl5: UILabel?
    @IBOutlet weak var lbl6: UILabel?
    
    var modal : Any? {
        didSet{
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? SchoolList {
            if let schName = modal.name , let city = modal.city {
            lblSchoolNmae?.text = "\(schName), \(city)"
            }
            lblAddress?.text  = modal.address
            guard let strSchoolCode      = modal.schoolCode else {return}
            let characters = Array(strSchoolCode)
            

           lbl1?.text = String(describing: characters[0])
           lbl2?.text = String(describing: characters[1])
           lbl3?.text = String(describing: characters[2])
           lbl4?.text = String(describing: characters[3])
           lbl5?.text = String(describing: characters[4])
           lbl6?.text = String(describing: characters[5])
            
//           lbl1?.backgroundColor = themeColor.hexStringToUIColor()
//           lbl2?.backgroundColor = themeColor.hexStringToUIColor()
//           lbl3?.backgroundColor = themeColor.hexStringToUIColor()
//           lbl4?.backgroundColor = themeColor.hexStringToUIColor()
//           lbl5?.backgroundColor = themeColor.hexStringToUIColor()
//           lbl6?.backgroundColor = themeColor.hexStringToUIColor()
//
            
            if let url = modal.logo?.getImageUrl() {
                imgView?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: UIImage(named : "No Profile_Big"))
            }
        }
    }
}
