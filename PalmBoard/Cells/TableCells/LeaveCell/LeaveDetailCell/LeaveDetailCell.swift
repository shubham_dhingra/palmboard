//
//  LeaveRequestCell.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

@objc protocol  DeleteLeaveDelegate {
    @objc optional func deleteLeave(index : Int)
    @objc optional func leaveStatusUpdate(leaveAction : String , index : Int)
}
class LeaveDetailCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var imgProfile       : UIImageView?{
        didSet {
            self.imgProfile?.setCornerRadius(radius: /self.imgProfile?.size.width / 2.0)
            self.imgProfile?.clipsToBounds = true
        }
    }
    @IBOutlet weak var lblName          : UILabel?
    @IBOutlet weak var lblNoOfLeave     : UILabel?
    @IBOutlet weak var lblFromDate      : UILabel?
    @IBOutlet weak var lblToDate        : UILabel?
    @IBOutlet weak var lblReason        : UILabel?
    @IBOutlet weak var btnDelete        : UIButton?
    @IBOutlet weak var lblStatus        : UILabel?
    @IBOutlet weak var lblAppliedOn     : UILabel?
    @IBOutlet weak var lblApprovedOn    : UILabel?
    @IBOutlet weak var lblApproverName  : UILabel?
    @IBOutlet weak var imgApprover      : UIImageView?
    @IBOutlet weak var lblDesignation   : UILabel?
    @IBOutlet weak var approverView   : UIView?
    @IBOutlet weak var approverViewHeightConst : NSLayoutConstraint?
    @IBOutlet weak var linearView: UIView!
    @IBOutlet weak var linearViewConst : NSLayoutConstraint?
    @IBOutlet weak var stackApproveReject: UIStackView?
    
    //Addition outlets for leaveReport
    @IBOutlet weak var lblApplicantName : UILabel?
    @IBOutlet weak var btnApprove       : UIButton?
    @IBOutlet weak var btnReject        : UIButton?
    @IBOutlet weak var lblLeaveAbbrev   : UILabel? {
        didSet {
            self.lblLeaveAbbrev?.setCornerRadius(radius: /self.lblLeaveAbbrev?.size.width / 2.0)
            self.lblLeaveAbbrev?.clipsToBounds = true
        }
    }
    
    
    //MARK::- VARIABLES
    var index : Int?
    var delegate : DeleteLeaveDelegate?
    var leaveUserType : Users?
    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    var model : Any? {
        didSet {
            configure()
        }
    }
    
    func configure(){
        
        if let model = model as? LeaveList {
            
            let firstAttr  = [NSAttributedString.Key.font: R.font.ubuntuLight(size: 16.0)!]
            let secondAttr =  [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!]
            lblNoOfLeave?.attributedText = Utility.shared.getAttributedString(firstStr: "Total Leave(s): ", firstAttr: firstAttr, secondStr: /model.duration?.toString + " Days", secondAttr: secondAttr)
            lblReason?.attributedText =  Utility.shared.getAttributedString(firstStr: "Reason: ", firstAttr: firstAttr, secondStr: /model.reason, secondAttr: secondAttr)
            lblFromDate?.text = model.fromDate?.dateChangeFormat()
            lblToDate?.text = model.tillDate?.dateChangeFormat()
            lblAppliedOn?.text = model.submittedOn?.dateChangeFormat()
            
            //leave report
            if topMostVC is LeavesReportViewController ||  ez.topMostVC is SearchLeaveViewController || ez.topMostVC is SelectClassViewController{
                
                lblLeaveAbbrev?.isHidden = true
                
                if leaveUserType == .Student {
                    if let name = model.studentName , let classs = model.studentClass {
                        lblName?.text = "\(name) ,\(classs) "
                    }
                    if let url = model.studentPhoto?.getImageUrl() {
                        imgProfile?.isHidden = false
                        //                    lblLeaveAbbrev?.isHidden = !(/imgProfile?.isHidden)
                        imgProfile?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: R.image.noProfile_Big())
                    }
                }
                else {
                    
                    if let applicantName = model.applicantName {
                        lblName?.text = applicantName
                    }
                    if let url = model.applicantPhoto?.getImageUrl() {
                        imgProfile?.isHidden = false
                        //                    lblLeaveAbbrev?.isHidden = !(/imgProfile?.isHidden)
                        imgProfile?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: R.image.noProfile_Big())
                    }
                }
            }
                
                //leave application
            else {
                
                if let name = model.applicantName {
                    lblName?.text = name
                }
                
                if let leaveType = model.leaveType {
                    lblName?.text = leaveType
                }
                
                //case of student leave request by parent
                if let url = model.applicantPhoto?.getImageUrl() {
                    imgProfile?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: R.image.noProfile_Big())
                    imgProfile?.isHidden = false
                    lblLeaveAbbrev?.isHidden = !(/imgProfile?.isHidden)
                }
                
                //case of teacher leave
                if let leaveAbbrv = model.leaveAbbrv {
                    imgProfile?.isHidden = true
                    lblLeaveAbbrev?.text = leaveAbbrv
                    lblLeaveAbbrev?.isHidden = !(/imgProfile?.isHidden)
                }
            }
            
            if let status = model.status {
                let isPending = status == LeaveStatus.Pending.get
                
                
                //leave reports
                if topMostVC is LeavesReportViewController || ez.topMostVC is SearchLeaveViewController || ez.topMostVC is SelectClassViewController {
                    if leaveUserType == .Student {
                        if let applicantName = model.applicantName {
                            lblApplicantName?.isHidden = false
                            lblApplicantName?.attributedText =   Utility.shared.getAttributedString(firstStr: "Applicant: ", firstAttr: firstAttr, secondStr: applicantName, secondAttr: secondAttr)
                        }
                    }
                    else {
                        if let leaveType = model.leaveType {
                            lblApplicantName?.isHidden = false
                            lblApplicantName?.attributedText =   Utility.shared.getAttributedString(firstStr: "Leave Type: ", firstAttr: firstAttr, secondStr: leaveType, secondAttr: secondAttr)
                        }
                    }
                    
                    btnReject?.isHidden = !isPending
                    btnApprove?.isHidden = !isPending
                    approverViewHeightConst?.constant = isPending ? 60.0 : 0.0
                    linearView?.isHidden = !isPending
                    linearViewConst?.constant = isPending ? 1.0 : 0.0
                }
                    
                    //leave Application
                else {
                    btnDelete?.isHidden = !isPending
                    approverView?.isHidden = isPending
                    lblStatus?.text = status
                    if !isPending {
                        if let url = model.photo?.getImageUrl() {
                            imgApprover?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: R.image.noProfile_Big())
                            // self.lblDesignation?.attributedText =   Utility.shared.getAttributedString(firstStr: "\(/model.teacherName), ", firstAttr: [NSAttributedString.Key.foregroundColor : UIColor.flatBlack], secondStr:  /model.designation, secondAttr: [NSAttributedString.Key.foregroundColor : UIColor.flatPending])
                            
                            self.lblDesignation?.attributedText =   Utility.shared.getAttributedString(firstStr: "\(/model.teacherName)", firstAttr: [NSAttributedString.Key.foregroundColor : UIColor.flatBlack], secondStr: nil, secondAttr: nil)
                            
                        }
                    }
                    approverViewHeightConst?.constant = isPending ? 0 : 30.0
                    
                    switch status {
                    case LeaveStatus.Pending.get:
                        lblStatus?.backgroundColor = UIColor.flatPending
                        lblApprovedOn?.text = nil
                        
                    case LeaveStatus.Approved.get:
                        lblStatus?.backgroundColor = UIColor.flatApproved
                        lblApproverName?.text = "Approved By"
                        lblApprovedOn?.text = model.actionOn?.dateChangeFormat()
                        
                        
                    case LeaveStatus.Rejected.get:
                        lblApproverName?.text = "Rejected By"
                        lblApprovedOn?.text = model.actionOn?.dateChangeFormat()
                        lblStatus?.backgroundColor = UIColor.flatRejected
                        
                    default :
                        break
                    }
                }
                
            }
        }
    }
    
    
    
    @IBAction func btnDeleteAct(_ sender: UIButton){
        if let index = index {
            delegate?.deleteLeave!(index: index)
        }
    }
    
    @IBAction func btnApproveAndReject(_ sender: UIButton) {
        let leaveStatus : LeaveStatus = sender.tag == 0 ? .Approved : .Rejected
        if let index = index {
            delegate?.leaveStatusUpdate!(leaveAction: leaveStatus.rawValue, index: index)
        }
    }
    
    
    
}
