//
//  LeaveReasonCell.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
protocol SuggestReasonDelegate{
    func reasonSelect(_ index : Int? , _ state : States?)
}
class LeaveReasonCell: UITableViewCell {

    //MARK::- OUTLETS
    @IBOutlet weak var lblReasonDesc  : UILabel?
    @IBOutlet weak var btnChecked     : UIButton?
    
    //MARK::- VARIABLES
    var reason : Any? {
        didSet {
            configure()
        }
    }
    var delegate : SuggestReasonDelegate?
    var index : Int?
    var states : States?
    
    //CONFIGURE
    func configure() {
        if let reason = reason as? String  {
           lblReasonDesc?.text = reason
           btnChecked?.isSelected = states == .yes
        }
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnCheckAct(_ sender : UIButton) {
       // btnChecked?.isSelected = !(/btnChecked?.isSelected)
        delegate?.reasonSelect(index, states)
    }
}


