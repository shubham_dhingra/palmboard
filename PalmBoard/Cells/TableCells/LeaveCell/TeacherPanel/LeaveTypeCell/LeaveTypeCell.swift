//
//  LeaveRequestCell.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class LeaveTypeCell : UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnExpand : UIButton?
    @IBOutlet weak var lblLeaveName : UILabel?
    @IBOutlet weak var lblTotalLeave : UILabel?
    @IBOutlet weak var lblTotalBalance : UILabel?
    @IBOutlet weak var lblTotalTaken : UILabel?
    @IBOutlet weak var lblTotalRemaining : UILabel?
    @IBOutlet weak var progressBar : LinearProgressView?
    
    
    var model : Any? {
        didSet{
            configure()
        }
    }
    
    func configure() {
        if let model = model as? LeaveType {
            btnExpand?.setImage(model.isExpand ? R.image.minus_green() : R.image.plus_green() , for: .normal)
            lblLeaveName?.text = model.name
            
            lblTotalLeave?.text = "Total Leave : \(/model.TotalLeave?.toString)"
            lblTotalTaken?.text = "Total Taken : \(/model.TotalTaken?.toString)"
            let balanceLeave = /model.TotalLeave - /model.TotalTaken
            
            lblTotalBalance?.text = "Total Balance : \(/balanceLeave.toString)"
            lblTotalLeave?.isHidden = !(model.isExpand)
            lblTotalTaken?.isHidden = !(model.isExpand)
            lblTotalBalance?.isHidden = !(model.isExpand)
            
            lblTotalRemaining?.text = "\(/balanceLeave.toString)/\(/model.TotalLeave?.toString)"
            progressBar?.maximumValue = Float(/model.TotalLeave)
            progressBar?.progress = Float(/balanceLeave)
        }
    }
}
