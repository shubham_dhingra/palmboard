//
//  DropDownCell.swift
//  e-Care Pro
//
//  Created by ShubhamMac on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
class DropDownCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblName : UILabel?
    
    //MARK::- VARIABLES
    var previousLeaveType : LeaveStatus?
    var index : Int?
    var selectTag : Int = 0
    var model : Any?{
        didSet{
            configure()
        }
    }
    
    //MARK::- Configure Cell
    func configure() {
        
        if selectTag == 5 {
            if  let index = index {
                lblName?.text = "\(index + 1)"
            }
        }
        else {
            if let val = model as? LeaveStatus , let previousLeaveType = previousLeaveType{
                lblName?.text = val.get
                lblName?.textColor = val == previousLeaveType ? UIColor.flatGreen : UIColor.flatLightBlack
            }
            
            if let model = model as? MyClasses {
                lblName?.text = model.ClassName
                lblName?.textColor = /model.isSelected ? UIColor.flatGreen : UIColor.flatLightBlack
            }
            
            if let model = model as? Subjects {
                lblName?.text = model.SubjectName
                lblName?.textColor = /model.isSelected ? UIColor.flatGreen : UIColor.flatLightBlack
            }
            
            
//            if let modal = model as? IdentityListModal{
//                if let name = modal.name {
//                    lblName?.text = name
//                    if let designation = modal.designation {
//                        lblName?.text = "\(name) (\(designation))"
//                    }
//                }
//                
//                //for meeting details Vc
//                if let purpose = modal.purpose {
//                    lblName?.text = purpose
//                }
//            }
            
            if let modal = model as? SMSTypeList {
                lblName?.text = modal.subject
            }
        }
        
    }
}



class FilterDropDownCell : UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var imgType : UIImageView?
    
    //MARK::- VARIABLES
    var previousFilterType : FilterType?
    var img : String?
    var model : Any?{
        didSet{
            configure()
        }
    }
    
    //MARK::- Configure Cell
    func configure() {
        if let val = model as? FilterType , let previousFilterType = previousFilterType{
            if let image = val.Image {
                imgType?.image = image
            }
            
            lblName?.text = val.get
            lblName?.textColor = val == previousFilterType ? UIColor.flatGreen : UIColor.flatLightBlack
        }
        
    }
}
