//
//  ChatParentCell.swift
//  e-Care Pro
//
//  Created by Shubham on 06/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ChatParentCell: UITableViewCell {
    
    @IBOutlet weak var parentView          : UIView?
    @IBOutlet weak var lblTime             : UILabel?
    @IBOutlet weak var lblMessage          : UILabel?
    @IBOutlet weak var img                 : UIImageView?
    @IBOutlet weak var viewAudio           : UIView?
    @IBOutlet weak var btnPlay             : UIButton?
    @IBOutlet weak var imgViewAudio        : UIImageView?
    @IBOutlet weak var lbltextWithImage    : UILabel?
    @IBOutlet weak var lbltextWithAudio    : UILabel?
    @IBOutlet weak var progressView        : UIProgressView?
    @IBOutlet weak var lblMsgDate          : UILabel?
    
    var index : Int?
    var jukebox : Jukebox!
    var isStopAll : Bool = false
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    
    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    
    func configure() {
        parentView?.setCornerRadius(radius: 8.0)
        
        if let modal = modal as? ChatMsgs , let msgType = modal.msgType {
            
            if isStopAll {
                setUpAudioPlayer()
                return
            }
            
            switch msgType {
                
            case 2:
                if let url = modal.filePath?.getImageUrl() {
                    img?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: nil)
                    img?.layer.backgroundColor = UIColor.lightGray.cgColor
                    img?.isUserInteractionEnabled = true
                }
                lbltextWithImage?.text =  /modal.body
                addTapGestureOnImage()
                
            case 3:
                lbltextWithAudio?.text =  /modal.body
                
            default:
                
                lblMessage?.attributedText = getAttributedString(input :/modal.body)
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
                lblMessage?.addGestureRecognizer(tapGesture)
                lblMessage?.isUserInteractionEnabled = true
            }
            
            lblTime?.text = modal.sentOn?.getTimeIn12HourFormat()
            
            if isStopAll {
                setUpAudioPlayer()
            }
        }
    }
    
    
    func addTapGestureOnImage() {
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(self.openImageViewer(_:)))
        self.img?.addGestureRecognizer(tapGesture)
    }
    
    @objc func openImageViewer(_ sender : UITapGestureRecognizer) {
        
        let appointmentStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = appointmentStoryboard.instantiateViewController(withIdentifier: "ImageViewerViewController") as? ImageViewerViewController else {return}
        vc.selectImage = img?.image
        ez.topMostVC?.presentVC(vc)
    }
    
    
    @IBAction func btnPlayAct(_ sender: UIButton) {
        setUpAudioPlayer()
    }
    
    
    func setUpAudioPlayer() {
        if let modal = modal as? ChatMsgs , let msgType = modal.msgType , let index = self.index{
            
            if msgType != 3 {
                return
            }
            
            if let url = modal.filePath?.getImageUrl() {
                let playingStatus = modal.isPlaying
                
                if let jukeBox = modal.jukeBoxRef {
                    self.jukebox = jukeBox
                }
                else {
                    configureAudioPlayer(audioUrl: url)
                }
                
                switch jukebox.state {
                    
                case .ready :
                    if !playingStatus {
                        jukebox.play(atIndex: 0)
                    }
                    //                    progressView?.progress = 0.0
                    (topMostVC as? ChatViewController)?.chatList?[index].isPlaying = true
                    btnPlay?.setImage(UIImage(named : "Pause"), for:  .normal)
                    
                    
                case .playing :
                    progressView?.progress = 0.0
                    (topMostVC as? ChatViewController)?.chatList?[index].isPlaying = true
                    btnPlay?.setImage(UIImage(named : "Pause"), for:  .normal)
                    jukebox.pause()
                    
                case .paused :
                    //                    progressView?.progress = 0.0
                    (topMostVC as? ChatViewController)?.chatList?[index].isPlaying = false
                    btnPlay?.setImage(UIImage(named : "Play"), for:  .normal)
                    jukebox.stop()
                    
                default:
                    progressView?.progress = 0.0
                    (topMostVC as? ChatViewController)?.chatList?[index].isPlaying = false
                    btnPlay?.setImage(UIImage(named : "Play"), for:  .normal)
                    jukebox.stop()
                    
                }
            }
            else {
                if jukebox != nil {
                    (topMostVC as? ChatViewController)?.chatList?[index].isPlaying = false
                    btnPlay?.setImage(UIImage(named : "Play"), for:  .normal)
                    progressView?.progress = 0.0
                    jukebox.pause()
                }
            }
            (topMostVC as? ChatViewController)?.chatList?[index].jukeBoxRef = self.jukebox
        }
    }
    
    func configureAudioPlayer(audioUrl : URL?) {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        if let url = audioUrl {
            jukebox = Jukebox(delegate: self, items: [JukeboxItem(URL: url)])!
        }
    }
    
}

extension ChatParentCell : JukeboxDelegate {
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            let value = Float(currentTime / duration)
            progressView?.progress = value
        } else {
            print("perform to reset UI")
            resetUI()
        }
    }
    
    func resetUI() {
        progressView?.progress = 0.0
        btnPlay?.setImage(UIImage(named : "Play"), for:  .normal)
    }
    
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
        if jukebox.state == .ready {
            btnPlay?.setImage(UIImage(named : "Play"), for:  .normal)
            progressView?.progress = 0.0
            (topMostVC as? ChatViewController)?.chatList?[/index].isPlaying = false
        }
        else if jukebox.state == .loading  {
            btnPlay?.setImage(UIImage(named : "Pause"), for:  .normal)
        }
        else {
            let imageName: String
            switch jukebox.state {
            case .playing, .loading:
                imageName = "Pause"
            case .paused, .failed:
                imageName = "Play"
                jukebox.stop()
                resetUI()
            case .ready:
                imageName = "Play"
                
            }
            btnPlay?.setImage(UIImage(named : imageName), for:  .normal)
            (topMostVC as? ChatViewController)?.chatList?[/index].jukeBoxRef = self.jukebox
        }
        print("Jukebox state changed to \(jukebox.state)")
    }
    
}



//Code to make label clickable and find url in text
extension ChatParentCell {
    
    //function to get attributed string with url
    func getAttributedString(input : String) -> NSMutableAttributedString {
        
        let matches = getURLRange(input: input)
        let attributedString = NSMutableAttributedString(string:input)
        
        for match in matches {
            let url = (input as NSString).substring(with: match.range)
            
            let linkText = NSMutableAttributedString(string:url, attributes:[NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor.blue , NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue])
            attributedString.replaceCharacters(in: match.range, with: linkText)
        }
        
        return attributedString
    }
    
    func getURLRange(input : String) -> [NSTextCheckingResult] {
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        return matches
    }
    
    //function to get index Of selected Character in string
    func didTapAttributedTextInLabel(label: UILabel, tapLocation: CGPoint) -> Int  {
        
        guard let lblMessage = lblMessage else {
            return 0
        }
        
        //here myLabel is the object of UILabel
        //added this from @warly's answer
        //set font of attributedText
        
        let attributedText = NSMutableAttributedString(attributedString: lblMessage.attributedText!)
        attributedText.addAttributes([NSAttributedString.Key.font: lblMessage.font], range: NSMakeRange(0, (lblMessage.attributedText?.string.count)!))
        
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize(width: lblMessage.frame.width, height: lblMessage.frame.height+100))
        let textStorage = NSTextStorage(attributedString: attributedText)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = lblMessage.lineBreakMode
        textContainer.maximumNumberOfLines = lblMessage.numberOfLines
        let labelSize = lblMessage.bounds.size
        textContainer.size = labelSize
        
        // get the index of character where user tapped
        let indexOfCharacter = layoutManager.characterIndex(for: tapLocation, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return indexOfCharacter
    }
    
    
    // function to check if URL is taped
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        
        guard let lblMessage = lblMessage else {
            return
        }
        let matches = getURLRange(input: (/lblMessage.text))
        
        let tapLocation = gesture.location(in: lblMessage)
        
        
        let indexOfCharacter = didTapAttributedTextInLabel(label: lblMessage, tapLocation: tapLocation)
        
        for match in matches {
            
            if NSLocationInRange(indexOfCharacter, match.range) {
                //open selected URL
                let mainText = lblMessage.text as NSString?
                if let urlToOpen = URL(string: (mainText?.substring(with: match.range))!) {
                    UIApplication.shared.open(urlToOpen)
                } else {
                    print("We have error with URL")
                }
                break
            } else {
                print("Tapped none")
            }
        }
    }
}
