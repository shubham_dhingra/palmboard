//
//  MessageCell.swift
//  e-Care Pro
//
//  Created by Shubham Dhingra on 04/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class MessageCell : TableParentCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblDate  : UILabel?
    @IBOutlet weak var lblUreadMsg : UILabel?
    @IBOutlet weak var sentSmsView : UIView?
    @IBOutlet weak var imgMedia : UIImageView?
    @IBOutlet weak var lblLastMsg : UILabel?
    @IBOutlet weak var backView : UIView?
    @IBOutlet weak var imgMediaHeightConstraint: NSLayoutConstraint?
    
    
    //MARK::- Variables
    var index : Int?
    var modal : Any? {
        didSet{
            configure()
        }
    }
    
    //MARK::- LIFECYCLES
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lblUreadMsg?.layer.cornerRadius  = /lblUreadMsg?.frame.width / 2
        imgUser?.layer.cornerRadius             = /imgUser?.frame.width / 2
    }
    
    
    
    //configure Cell
    func configure() {
        
        if let modal = modal as? IMessages, let senderType = modal.senderType{
            
            self.contentView.backgroundColor = modal.unread == 0 ? UIColor.white : UIColor.f3f3f3
            sentSmsView?.isHidden = true
            lblUreadMsg?.text = modal.unread?.toString
            lblUreadMsg?.isHidden = modal.unread == 0
            lblDate?.text = modal.sentOn?.dateChangeFormat()
            lblName?.text = modal.name
            
            if let photoUrl = modal.photo {
                imgUser?.loadURL(imageUrl: photoUrl.getImageUrl(), placeholder: nil, placeholderImage: nil)
            }
            
            switch senderType  {
            // Child
            case 1:
                if  let classs = modal.className {
                    lblDesignation?.text = "Class: (\(classs))"
                }
                break
            //Parent
            case 2:
                if let childName = modal.childName , let classs = modal.className {
                    lblDesignation?.text = "Parent of \(childName) (\(classs))"
                }
                break
                
            //Staff
            case 3:
                lblName?.text = modal.name
                if let designation = modal.designation {
                    lblDesignation?.text = designation
                }
                break
                
            default:
                break
            }
            
        }
        
        if let modal = modal as? SMessages , let msgType = modal.msgType {
            lblUreadMsg?.isHidden = true
            sentSmsView?.isHidden = false
            // means isReplying message is nil that men
            if modal.isReplyMsg != nil {
                 self.contentView.backgroundColor = modal.isReplyMsg == 0 ? UIColor.white : UIColor.f3f3f3
            }
            else {
                self.contentView.backgroundColor = modal.hasRead == 1 ? UIColor.white : UIColor.f3f3f3
            }
            lblDate?.text = modal.sentOn?.dateChangeFormat()
            lblName?.text = modal.subject
            imgMediaHeightConstraint?.constant = modal.msgType == 1 ? 0 : 16
            imgMedia?.isHidden = modal.msgType == 1
            let names = modal.recipients?.map({ (recipient) -> String in
                /recipient.name
            })
            
            if /names?.count != 0 {
                if let firstName = names?[0] {
                    let count = Int(/names?.count - 1)
                    lblDesignation?.text = count == 0 ? "To: \(firstName)" : "To: \(firstName) and \(count) more"
                }
            }
           
            if modal.recipients?.count != 0 {
                if let photoUrl = modal.recipients?[0].photo {
                    imgUser?.loadURL(imageUrl: photoUrl.getImageUrl(), placeholder: nil, placeholderImage: nil)
                }
            }
          
            
            switch msgType  {
                
            // Text
            case 1 :
                if let abbreviation = modal.abbreviation {
                    lblLastMsg?.text = abbreviation
                }
                
            //Image
            case 2:
                let attributedString = Utility.shared.getAttributedString(firstStr: "Image ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: modal.abbreviation, secondAttr: [:])
                lblLastMsg?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "Camera_red-01-min")
                break
                
            //Audio
            case 3:
                let attributedString = Utility.shared.getAttributedString(firstStr: "Audio ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: modal.abbreviation, secondAttr: [:])
                lblLastMsg?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "Microphone-01-min")
                break
                
            // sms msg
            case 4:
                let attributedString = Utility.shared.getAttributedString(firstStr: "SMS ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: modal.abbreviation, secondAttr: [:])
                lblLastMsg?.attributedText = attributedString
                imgMedia?.image = #imageLiteral(resourceName: "SMS-01-min")
                break
                
            default:
                break
            }
            
        }
    }
}
