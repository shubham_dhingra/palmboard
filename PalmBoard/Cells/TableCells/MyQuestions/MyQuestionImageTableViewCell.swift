//  MyQuestionImageTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 19/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

protocol QuestionImageDelegate {
    func ClkBtnLikeImageQuestion(btn: UIButton)
    func ClkbtnReportImagePressed(intTag: Int)
    func ClkbtnProfileImagePressed(intTag: Int)
    //  func ClkLikesAllList(tag : Int)
    // func ClkLikePressed(btn : UIButton)
    // func ClkDotsPressed(tag : Int)
}
class MyQuestionImageTableViewCell: UITableViewCell {
    
    //   @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName:                 UILabel?
    @IBOutlet weak var btnProfileImage:         UIButton?
    @IBOutlet weak var lblTime:                 UILabel?
    @IBOutlet weak var lblQuestion:             UILabel?
    @IBOutlet weak var imgViewQuestion:         UIImageView?
    @IBOutlet weak var btnAnswerImage:          UIButton?
    @IBOutlet weak var btnAnswerAllImage:       UIButton?
    @IBOutlet weak var btnLikeQuestionImage:    UIButton?
    @IBOutlet weak var btnLikeAllQuestionImage: UIButton?
    @IBOutlet weak var btnReportImage:          UIButton?
    @IBOutlet weak var stackViewImgOuter:       UIStackView?
    @IBOutlet weak var lblPending:              UILabel?
    
    @IBOutlet weak var imgQuestionHeight:       NSLayoutConstraint!
    @IBOutlet weak var imgProfile :             UIImageView?
    var delegate        : QuestionImageDelegate?
    
    var intUserID   : Int?
    var intUserType : Int?
    var index       : Int?
    var indexpath   : IndexPath?
    var tblQuestion : UITableView?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? ListQuestion
        {
            var strPhotoURL      = String()
            if let strPhotoURL1 = modal.photo {
                strPhotoURL      = strPhotoURL1
            }
            let intUserID       = modal.userID
            
            if let userID = DBManager.shared.getAuthenticatedUser().first?.userID{
                self.intUserID =  Int(userID)
            }
            (self.intUserID == intUserID) ?  (btnReportImage?.isHidden = false) : (btnReportImage?.isHidden = true)
            
            if modal.isVerified == 0 || modal.isVerified == 2 {
                stackViewImgOuter?.isHidden = true
                //isUserInteractionEnabled      = false
                lblPending?.isHidden               = false
                (modal.isVerified == 0) ? (lblPending?.text = "Pending") : (lblPending?.text = "Rejected")
                (modal.isVerified == 0) ? (lblPending?.backgroundColor = UIColor.orange) : (lblPending?.backgroundColor = UIColor.red)
            }
            else{
                stackViewImgOuter?.isHidden = false
                // isUserInteractionEnabled = true
                lblPending?.isHidden     = true
            }
            
            btnReportImage?.tag           = /index
            btnProfileImage?.tag          = /index
            btnLikeQuestionImage?.tag     = /index
            btnLikeAllQuestionImage?.tag  = /index
            btnLikeQuestionImage?.addTarget(self, action: #selector(ClkBtnLikeQuestion(button:)), for: .touchUpInside)
            
            // imageMyDelegate = self
            var strPrefix        = String()
            strPrefix           += strPrefix.imgPath1()
            
            var finalUrl1 = "\(strPrefix)" + "\(/modal.queImg)"
            finalUrl1     = finalUrl1.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let imgViewTempQuestn      = CustomImageView()
            imgViewTempQuestn.frame = (imgViewQuestion?.frame)!
            imgViewTempQuestn.imageFromServerURL(urlString: finalUrl1, tableView: tblQuestion, indexpath: indexpath)
            
            if imgViewTempQuestn.image != nil{
                
                let selectedColor   = UIColor(rgb: 0x56B54B)
                let unselectedColor = UIColor(rgb: 0x5E5E5E)
                
                if let usertype = DBManager.shared.getAuthenticatedUser().first?.userType{
                    intUserType =  Int(usertype)
                }
                //  lblName.text     = strUpdatedBy.replace(target: "|", withString: ", ")
                var str1 = String()
                var str2 = String()
                
                let stringArray        = modal.updatedBy?.components(separatedBy: "|")
                str1                   = "\(/stringArray?[0])"
                
                if(/stringArray?.count > 1){
                    str2 = "\(/stringArray?[1])"
                }else{
                    str2     = self.intUserType == 2 ? "Parent" : ""
                }
                str1 += ", "
                str1 += str2
                lblName?.text    = str1
                
                lblTime?.text     =  modal.updatedOn?.dateChangeFormatQuestion()//strUpdatedOn.dateChangeFormat()
                lblQuestion?.text = modal.que
                
                var strPrefix   = String()
                strPrefix       += strPrefix.imgPath1()
                var finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
                finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let urlPhoto    = URL(string: finalUrl)
                
                // let imgView1 = UIImageView()
                //        imgView1.loadURL(imageUrl: urlPhoto!, placeholder: nil, placeholderImage: UIImage(named: "No Profile_Big"))
                //        let img1 = imgView1.image?.af_imageRoundedIntoCircle()
                //        self.btnProfileImage?.setBackgroundImage(img1 as UIImage?, for: .normal)
                
                imgProfile?.loadURL(imageUrl: urlPhoto!, placeholder: nil, placeholderImage: R.image.noProfile_Big())
                //        imgView1.af_setImage(withURL: urlPhoto!, placeholderImage: UIImage(named: "No Profile_Big"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                //            let img1 = imgView1.image?.af_imageRoundedIntoCircle()
                //            self.btnProfileImage?.setBackgroundImage(img1, for: .normal)
                //        }
                
                //          imgView1.af_setImage(withURL: urlPhoto!, placeholderImage: UIImage(named: "No Profile_Big"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                //
                //            let img1 = imgView1.image?.af_imageRoundedIntoCircle()
                //            self.btnProfileImage?.setBackgroundImage(img1 as UIImage?, for: .normal)
                //        }
                
                if /modal.likes > 1{
                    btnLikeAllQuestionImage?.setTitle("\(/modal.likes) Likes",for: .normal)
                    self.btnLikeAllQuestionImage?.isHidden = false
                }
                else if /modal.likes == 1{
                    btnLikeAllQuestionImage?.setTitle("\(/modal.likes) Like",for: .normal)
                    self.btnLikeAllQuestionImage?.isHidden = false
                }
                else {
                    //self.btnLikeAllQuestionImage.isHidden = true
                    btnLikeAllQuestionImage?.setTitle("",for: .normal)
                    self.btnLikeAllQuestionImage?.isHidden = false
                }
                
                switch modal.isILike
                {
                case 0:
                    btnLikeQuestionImage?.setImage(R.image.like(), for: UIControl.State.normal)
                    btnLikeQuestionImage?.setTitleColor(unselectedColor, for: .normal)
                case 1:
                    btnLikeQuestionImage?.setImage(R.image.liked(), for: UIControl.State.normal)
                    btnLikeQuestionImage?.setTitleColor(selectedColor, for: .normal)
                default:
                    break
                }
                if /modal.totalAnswer > 1{
                    btnAnswerAllImage?.setTitle("\(/modal.totalAnswer) Answers",for: .normal)
                    self.btnAnswerAllImage?.isHidden = false
                }
                else  if /modal.totalAnswer == 1{
                    btnAnswerAllImage?.setTitle("\(/modal.totalAnswer) Answer",for: .normal)
                    self.btnAnswerAllImage?.isHidden = false
                }
                else  if /modal.totalAnswer == 0{
                    // btnAnswerAllNormal.setTitle("\(intTotalAnswer) Answer",for: .normal)
                    self.btnAnswerAllImage?.isHidden = true
                }
                
                switch modal.isAnswered
                {
                case 0:
                    btnAnswerImage?.setImage(R.image.answer(), for: UIControl.State.normal)
                case 1:
                    btnAnswerImage?.setImage(R.image.answerSelect(), for: UIControl.State.normal)
                default:
                    break
                }
                imgViewQuestion?.image      = imgViewTempQuestn.image
                imgQuestionHeight.constant = (imgViewTempQuestn.image?.size.height)!
            }
        }
    }
    // MARK: - ClkBtnLikeQuestion
    @objc func ClkBtnLikeQuestion(button: UIButton){
        delegate?.ClkBtnLikeImageQuestion(btn: button)
    }
    @IBAction func ClkBtnReportImage(_ sender: UIButton){
        delegate?.ClkbtnReportImagePressed(intTag: sender.tag)
    }
    @IBAction func ClkBtnProfileImage(_ sender: UIButton){
        delegate?.ClkbtnProfileImagePressed(intTag: sender.tag)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imgViewQuestion?.frame.size.width  = screenWidth
        self.imgViewQuestion?.frame.size.height = 170.67
        self.imgViewQuestion?.image             = nil
    }
    override func awakeFromNib(){
        super.awakeFromNib()
        self.layoutIfNeeded()
        lblPending?.layer.cornerRadius = 6.0
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        // print("Func: setSelected ()")
        super.setSelected(selected, animated: animated)
    }
}
