//  MyQuestionNormalTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 19/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

protocol QuestionNormalDelegate {
    func ClkBtnLikeQuestion(btn: UIButton)
    func ClkbtnReportNormalPressed(intTag: Int)
    func ClkbtnProfileNormalPressed(intTag: Int)
    //  func ClkLikesAllList(tag : Int)
    // func ClkLikePressed(btn : UIButton)
    // func ClkDotsPressed(tag : Int)
}
class MyQuestionNormalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName:            UILabel?
    @IBOutlet weak var lblTime:            UILabel?
    @IBOutlet weak var lblQuestion:        UILabel?
    @IBOutlet weak var btnAnswerNormal:    UIButton?
    @IBOutlet weak var btnAnswerAllNormal: UIButton?
    @IBOutlet weak var btnLikeQuestion:    UIButton?
    @IBOutlet weak var btnLikeAllQuestion: UIButton?
    @IBOutlet weak var btnReportNormal:    UIButton?
    @IBOutlet weak var btnProfile:         UIButton?
    @IBOutlet weak var stackViewOuter:     UIStackView?
    @IBOutlet weak var lblPending:         UILabel?
    @IBOutlet weak var imgProfile :        UIImageView?
    
    var delegate       : QuestionNormalDelegate?
    // var normalDelegate : MyQuestionNormalTableViewCellDelegate?
    // var imgView1:                       UIImageView?
    var intUserType:                       Int?
    var intUserID:                         Int?
    //var delegate: QuestionNormalTableViewCellDelegate?
    var index : Int?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? ListQuestion
        {
            var strPhotoURL      = String()
            let intQutType       = modal.qType
            if let strPhotoURL1  = modal.photo {
                strPhotoURL      = strPhotoURL1
            }
            let intUserID       = modal.userID
            
            if intQutType ==  1
            {
                if let userID = DBManager.shared.getAuthenticatedUser().first?.userID{
                    self.intUserID =  Int(userID)
                }
                
                (self.intUserID == intUserID) ?  (btnReportNormal?.isHidden = false) : (btnReportNormal?.isHidden = true)
                
                if modal.isVerified == 0 || modal.isVerified == 2 {
                    stackViewOuter?.isHidden  = true
                    //isUserInteractionEnabled = false
                    lblPending?.isHidden          = false
                    
                    (modal.isVerified == 0) ? (lblPending?.text = "Pending") : (lblPending?.text = "Rejected")
                    (modal.isVerified == 0) ? (lblPending?.backgroundColor = UIColor.orange) : (lblPending?.backgroundColor = UIColor.red)
                }
                else{
                    stackViewOuter?.isHidden = false
                    //isUserInteractionEnabled = true
                    lblPending?.isHidden     = true
                }
                
                let selectedColor   = UIColor(rgb: 0x56B54B)
                let unselectedColor = UIColor(rgb: 0x5E5E5E)
                if let usertype = DBManager.shared.getAuthenticatedUser().first?.userType{
                    intUserType =  Int(usertype)
                }
                //  lblName.text     = strUpdatedBy.replace(target: "|", withString: ", ")
                var str1 = String()
                var str2 = String()
                
                let stringArray        = modal.updatedBy?.components(separatedBy: "|")
                str1                   = "\(/stringArray?[0])"
                
                if(/stringArray?.count > 1){
                    str2 = "\(/stringArray?[1])"
                }else{
                    str2 = self.intUserType == 2 ? "Parent" : ""
                }
                str1 += ", "
                str1 += str2
                lblName?.text   = str1
                // print("strUpdatedON   =",strUpdatedOn )
                lblTime?.text     = modal.updatedOn?.dateChangeFormatQuestion()
                lblQuestion?.text = modal.que
                
                var strPrefix   = String()
                strPrefix       += strPrefix.imgPath1()
                var finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
                finalUrl        = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let urlPhoto    = URL(string: finalUrl)
                
                print("urlPhoto = \(urlPhoto!)")
                print("finalUrl = \(String(describing: finalUrl))")
                // let imgView1 = UIImageView()
                imgProfile?.loadURL(imageUrl: urlPhoto!, placeholder: nil, placeholderImage: R.image.noProfile_Big())
                //        imgView1.af_setImage(withURL: urlPhoto!, placeholderImage: UIImage(named: "No Profile_Big"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                //
                //            let img1 = imgView1.image?.af_imageRoundedIntoCircle()
                //            self.btnProfile?.setBackgroundImage(img1 as UIImage?, for: .normal)
                //        }
                if /modal.likes > 1 {
                    self.btnLikeAllQuestion?.setTitle("\(/modal.likes) Likes",for: .normal)
                    self.btnLikeAllQuestion?.isHidden = false
                }
                else if modal.likes == 1 {
                    self.btnLikeAllQuestion?.setTitle("\(/modal.likes) Like",for: .normal)
                    self.btnLikeAllQuestion?.isHidden = false
                }
                else {
                    //self.btnLikeAllQuestion.isHidden = true
                    self.btnLikeAllQuestion?.setTitle("",for: .normal)
                    self.btnLikeAllQuestion?.isHidden = false
                }
                switch modal.isILike
                {
                case 0:
                    btnLikeQuestion?.setImage(R.image.like(), for: UIControl.State.normal)
                    btnLikeQuestion?.setTitleColor(unselectedColor, for: .normal)
                case 1:
                    btnLikeQuestion?.setImage(R.image.liked(), for: UIControl.State.normal)
                    btnLikeQuestion?.setTitleColor(selectedColor, for: .normal)
                default:
                    break
                }
                
                if /modal.totalAnswer > 1{
                    btnAnswerAllNormal?.setTitle("\(/modal.totalAnswer) Answers",for: .normal)
                    self.btnAnswerAllNormal?.isHidden = false
                }
                else  if modal.totalAnswer == 1{
                    btnAnswerAllNormal?.setTitle("\(/modal.totalAnswer) Answer",for: .normal)
                    self.btnAnswerAllNormal?.isHidden = false
                }
                else  if modal.totalAnswer == 0{
                    // btnAnswerAllNormal.setTitle("\(intTotalAnswer) Answer",for: .normal)
                    self.btnAnswerAllNormal?.isHidden = true
                }
                
                switch modal.isAnswered
                {
                case 0:
                    btnAnswerNormal?.setImage(R.image.answer(), for: UIControl.State.normal)
                case 1:
                    btnAnswerNormal?.setImage(R.image.answerSelect(), for: UIControl.State.normal)
                default:
                    break
                }
                btnReportNormal?.tag       = /index
                btnProfile?.tag            = /index
                btnLikeQuestion?.tag       = /index
                btnLikeAllQuestion?.tag    = /index
                btnLikeQuestion?.addTarget(self, action: #selector(ClkBtnLikeQuestion(button:)), for: .touchUpInside)
                // normalDelegate = self
            }
        }
    }
    // MARK: - ClkBtnLikeQuestion
    @objc func ClkBtnLikeQuestion(button: UIButton){
        delegate?.ClkBtnLikeQuestion(btn: button)
    }
    @IBAction func ClkBtnReportNormal(_ sender: UIButton){
        delegate?.ClkbtnReportNormalPressed(intTag: sender.tag)
    }
    @IBAction func ClkBtnProfileNormal(_ sender: UIButton){
        delegate?.ClkbtnProfileNormalPressed(intTag: sender.tag)
    }
    override func awakeFromNib(){
        super.awakeFromNib()
        lblPending?.layer.cornerRadius = 6.0
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
}

