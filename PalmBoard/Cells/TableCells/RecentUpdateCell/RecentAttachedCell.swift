//  RecentAttachedCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 30/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class RecentAttachedCell: RecentUpdateParentCell {

    @IBOutlet weak var lblSenderName : UILabel?
    @IBOutlet weak var btnAttachment : UIButton?

    override func configure() {
        
        if let modal = modal as? Update{
            updateCellUI()
            if let senderDTL = modal.msgDTLs?.senderDTL{
                lblSenderName?.text = "From :\(/senderDTL.name)"
            }
            btnAttachment?.isHidden = !(/modal.hasAttachment)
        }
    }
}

