//  RecentGalleryCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 30/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import EZSwiftExtensions

class RecentGalleryCell: RecentUpdateParentCell {
    
    @IBOutlet weak var lblCount       : UILabel?
    @IBOutlet weak var viewCollection : UIView?
    @IBOutlet weak var collectionView : UICollectionView?
    
    var passContentOffSet     = IndexPath()
    var galleryItems          = [String]()
    var awsURL : String?
    var isFirstTime    : Bool = true
    var isVideoGallery : Bool = false
    var currentSchoolCode : String? {
        return /DBManager.shared.getAllSchools().first?.schCode
    }
    
    var collectionDataSource: CollectionViewDataSource?{
        didSet{
            collectionView?.dataSource = collectionDataSource
            collectionView?.delegate = collectionDataSource
            collectionView?.reloadData()
        }
    }
    override func configure() {
        
        if let modal = modal as? Update {
            updateCellUI()
            
            if let gallery = modal.galleryUpdate , let items = gallery.fileNames, let count = gallery.total{
                galleryItems = items
                isVideoGallery      = /gallery.sMdlID == 2
                lblCount?.text      =  count > 4 ? "+\(count - 4)" : nil
                lblCount?.addBorder(width: 1.0, color: UIColor.flatBlack)
                lblCount?.isHidden  =  count <= 4
                
                if galleryItems.count != 0 {
                    reloadGalleryItems()
                }
            }
        }
    }
}
extension RecentGalleryCell {
    
    func reloadGalleryItems() {
        configureCollectionView()
    }
}
extension RecentGalleryCell {
    
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: galleryItems, collectionView: collectionView, cellIdentifier: CellIdentifiers.recentGalleryCollectionCell.get, headerIdentifier: nil, cellHeight: 44.0 , cellWidth: 68.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell         = cell as? RecentGalleryCollectionCell else {return}
            cell.AwsURL             = self.awsURL
            cell.schoolCode        = self.currentSchoolCode
            cell.isVideoGallery    = self.isVideoGallery
            cell.btnPlay?.isHidden = !self.isVideoGallery
            
            cell.btnPlay?.isUserInteractionEnabled = false
            cell.modal             = item
        }
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            //  self.didSelect(indexpath.row)
            self.didSelect(indexpath.row, indexpath: indexpath)
        }
    }
    
    func didSelect(_ index : Int, indexpath: IndexPath){
        passContentOffSet = indexpath
        if let modal = modal as? Update  ,let galleryItems = modal.galleryUpdate{
            navigateToController(galleryItems: galleryItems)
        }
        
    }
    func navigateToController(galleryItems : GalleryUpdate){
        
        guard let arrayImag = galleryItems.fileNames else {return}
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "RecentUpdateGridViewController") as? RecentUpdateGridViewController else {return}
        
        vc.intAPIType          = /galleryItems.sMdlID
        vc.imgArray            = arrayImag
        vc.awsURL               = self.awsURL
        vc.passedContextOffset = passContentOffSet
        ez.topMostVC?.presentVC(vc)
    }
}

