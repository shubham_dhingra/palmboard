//  RecentMsgTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 30/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class RecentMsgTableViewCell: RecentUpdateParentCell {
    
    @IBOutlet weak var lblSenderName : UILabel?
    @IBOutlet weak var btnAttachment : UIButton?
    @IBOutlet weak var imgSender     : UIImageView?
    @IBOutlet weak var viewAudio     : UIView?
    @IBOutlet weak var sentSmsView : UIView?
    @IBOutlet weak var imgMedia : UIImageView?
    @IBOutlet weak var lblLastMsg : UILabel?
    @IBOutlet weak var backView : UIView?
    @IBOutlet weak var imgMediaHeightConstraint: NSLayoutConstraint?
    
    override func configure() {
        
        if let modal = modal as? Update {
            updateCellUI()
            btnAttachment?.isHidden = true
            
            if let msgDTL = modal.msgDTLs {
               
                if let senderDTL = msgDTL.senderDTL{
                    switch senderDTL.senderType {
                    case 1:
                        lblSenderName?.text =  "From: \(/senderDTL.name) ((\(/senderDTL.className)))"
                    case 2:
                        lblSenderName?.text =  "From: \(/senderDTL.name) P/o \(/senderDTL.childName) (\(/senderDTL.className))"
                    default:
                        lblSenderName?.text =  "From: \(/senderDTL.name) (\(/senderDTL.designation))"
                    }
                }
                
                if let senderDTL = msgDTL.senderDTL{
                    if let imageGal = senderDTL.photo?.getImageUrl() {
                        imgSender?.loadURL(imageUrl: imageGal, placeholder: nil, placeholderImage: R.image.noProfile_Big())
                    }
                    else {
                        imgSender?.image = R.image.noProfile_Big()
                    }
                }
                
                if let msgType = msgDTL.msgType {
                    
                    imgMediaHeightConstraint?.constant = msgType == 1 ? 0 : 16
                    imgMedia?.isHidden = msgType == 1
                    sentSmsView?.isHidden = msgType == 1
                    updateUI(msgType : msgType , msgDTL : msgDTL)
                    
                }
                
            }
        }
    }
    
    func updateUI(msgType : Int , msgDTL : MsgDTLS){
        
        switch msgType  {
            
        // Text
        //Image
        case 2:
            let attributedString = Utility.shared.getAttributedString(firstStr: "Image ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
            lblLastMsg?.attributedText = attributedString
            imgMedia?.image = #imageLiteral(resourceName: "Camera_red-01-min")
            break
            
        //Audio
        case 3:
            let attributedString = Utility.shared.getAttributedString(firstStr: "Audio ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
            lblLastMsg?.attributedText = attributedString
            imgMedia?.image = #imageLiteral(resourceName: "Microphone-01-min")
            break
            
        // sms msg
        case 4:
            let attributedString = Utility.shared.getAttributedString(firstStr: "SMS ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: nil, secondAttr: [:])
            lblLastMsg?.attributedText = attributedString
            imgMedia?.image = #imageLiteral(resourceName: "SMS-01-min")
            break
        default:
            break
        }
    }
}
