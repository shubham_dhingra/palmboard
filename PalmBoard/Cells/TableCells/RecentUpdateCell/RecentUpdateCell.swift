
//
//  RecentUpdateCell.swift
//  PalmBoard
//
//  Created by Shubham on 16/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class RecentUpdateCell: RecentUpdateParentCell {

     override func configure() {
        if let modal = modal as? Update{
            updateCellUI()
        }
    }
}
