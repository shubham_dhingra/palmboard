//
//  RecentUpdateParentCell.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 31/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class RecentUpdateParentCell: UITableViewCell {
    
    @IBOutlet weak var lblTime       : UILabel?
    @IBOutlet weak var lblDesc       : UILabel?
    @IBOutlet weak var lblModuleName : UILabel?
    @IBOutlet weak var imgModule     : UIImageView?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        
    }
    
    func updateCellUI() {
        if let modal = modal as? Update {
            
            switch modal.mdlID {
            case  3:
                imgModule?.image    = R.image.ru_Circular()
            case  1:
                imgModule?.image    = R.image.ru_NewsNotice()
            case  2:
                imgModule?.image    = R.image.ru_PhotoGallery()
            case 16:
                imgModule?.image    = R.image.ru_Message()
            case 14:
                imgModule?.image   = R.image.ru_Leave_Application()
            default:
                break
            }
            
            lblDesc?.text       = modal.caption
            
             lblTime?.text       = /modal.updtedOn?.dateChangeFormatQuestion()
            //lblTime?.text       = "\(/modal.updtedOn?.dateChangeFormat()) at \(/modal.updtedOn?.getTimeIn12HourFormat())"
            lblModuleName?.text = modal.module
        }
    }
    
}
