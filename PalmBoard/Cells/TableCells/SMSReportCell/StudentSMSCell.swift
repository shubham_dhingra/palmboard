//
//  StudentSMSCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 23/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

//MARK:- StudentSMSCell
class StudentSMSCell: UITableViewCell {
    @IBOutlet weak var imgStudent:  UIImageView?
    //@IBOutlet weak var imgTeacher:  UIImageView!
    @IBOutlet weak var btnImgTeacher: UIButton?
    
    @IBOutlet weak var lblParent: UILabel?
    
    @IBOutlet weak var txtMessage:  UILabel?
    @IBOutlet weak var dateSent:    UILabel?
    @IBOutlet weak var msgTitle:    UILabel?
    @IBOutlet weak var msgStatus:   UILabel?
    @IBOutlet weak var teacherName: UILabel?
    @IBOutlet weak var studentName: UILabel?

    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? SMS {
            
            txtMessage?.text  = modal.text
            dateSent?.text    = modal.sentOn?.dateChangeFormat()
            msgTitle?.text    = modal.sMSType
            if let status = modal.status {
                msgStatus?.text   = "Delivery Status : \(status)"
            }
           
            
            if let senderName = modal.senderName ,let designation = modal.designation {
               teacherName?.text = senderName + ", " + designation
            }
            
            studentName?.text = modal.receiver?.name
            let intReceiverType = modal.receiver?.receiverType

            if intReceiverType == 2 {
                if let childName = modal.receiver?.childName , let className = modal.receiver?.ClassName {
                      lblParent?.text = "Parent of " + childName + ", " + className
                }
            }
            
            if intReceiverType == 1 {
                if let className = modal.receiver?.ClassName {
                    lblParent?.text =  "Class: " + (className)
                }
            }
            
            if intReceiverType == 3 {
                if let designation = modal.receiver?.designation {
                    lblParent?.text = designation
                }
            }

            if let imgUrl = modal.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.btnImgTeacher?.setBackgroundImage(img1 as UIImage?, for: .normal)
                }
            }
            else{
                self.btnImgTeacher?.setBackgroundImage(R.image.noProfile_Big(), for: .normal)
            }
            
            
            if let imgUrl = modal.receiver?.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgStudent?.image = img1
                }
            }
            else{
                self.imgStudent?.image = R.image.noProfile_Big()
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgStudent?.clipsToBounds      = true
       // btnImgTeacher.clipsToBounds  = true
        msgStatus?.clipsToBounds       = true
        msgStatus?.layer.cornerRadius  = 3
       // btnImgTeacher.layer.cornerRadius = btnImgTeacher.frame.size.width / 2
        imgStudent?.layer.cornerRadius = (imgStudent?.frame.size.width)! / 2
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}
