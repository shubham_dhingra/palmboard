



//
//  SelectHalfDayCell.swift
//  PalmBoard
//
//  Created by Shubham on 06/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions
protocol SelectHalfTypeDelegate {
    func selectHalfTypeIndex(index : Int , halfType : Int)
}
class SelectHalfDayCell: UITableViewCell {

    @IBOutlet weak var lblLeaveDate : UILabel?
    @IBOutlet weak var btnFirstHalf : UIButton?
    @IBOutlet weak var btnSecondHalf : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var delegate : SelectHalfTypeDelegate?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var index : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? HalfDayModal {
            lblLeaveDate?.text = modal.leaveDate?.dateToString(formatType :"dd-MM-yyyy")
            btnFirstHalf?.setTitleColor(modal.halfType == 1 ? UIColor.white : UIColor.flatBlack, for: .normal)
            btnSecondHalf?.setTitleColor(modal.halfType == 2 ? UIColor.white : UIColor.flatBlack, for: .normal)
           
            btnFirstHalf?.setBackgroundColor( modal.halfType == 1 ? UIColor.flatGreen : UIColor.white, forState: .normal)
            btnSecondHalf?.setBackgroundColor( modal.halfType == 2 ? UIColor.flatGreen : UIColor.white, forState: .normal)
            
            btnFirstHalf?.addBorder(width: 1.0, color: modal.halfType == 1 ? UIColor.white : UIColor.flatBlack70Color)
            btnSecondHalf?.addBorder(width: 1.0, color: modal.halfType == 2 ? UIColor.white : UIColor.flatBlack70Color)

            btnFirstHalf?.setCornerRadius(radius: 12.0)
            btnSecondHalf?.setCornerRadius(radius: 12.0)
        }
    }
    
    @IBAction func selectHalfType(_ sender : UIButton){
        if let modal = modal as? HalfDayModal , let index = index {
            if sender.tag == 2 {
                delegate?.selectHalfTypeIndex(index: index, halfType: modal.halfType == 2 ? -1 : sender.tag)
            }
            else {
                delegate?.selectHalfTypeIndex(index: index, halfType: modal.halfType == 1 ? -1 : sender.tag)
            }
        }
    }
}

