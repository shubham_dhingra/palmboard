//
//  SettingsCell.swift
//  PalmBoard
//
//  Created by Shubham on 03/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
protocol ButtonActDelegate {
    func sendBtnTag(_ tag : Int)
}

class SettingsCell: UITableViewCell {

    //MARK::- OUtlets
    @IBOutlet weak var lblModuleName : UILabel?
    @IBOutlet weak var btnSwitch     : UISwitch?
    var delegate : ButtonActDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var index    : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        if let modal = modal as? ModuleList {
            lblModuleName?.text   = modal.module
            btnSwitch?.tag        = /index
            btnSwitch?.setOn(/modal.isActive, animated: true)
          //  delegate?.sendBtnTag(/btnSwitch?.tag)
        }
    }
    @IBAction func btnToggle(_ sender : UIButton){
        delegate?.sendBtnTag(sender.tag)
    }
}
