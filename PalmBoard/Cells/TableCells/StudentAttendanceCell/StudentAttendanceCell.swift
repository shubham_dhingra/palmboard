//  StudentAttendanceCell.swift
//  PalmBoard
//  Created by Shubham on 04/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class StudentAttendanceCell: TableParentCell {
    
    @IBOutlet weak var  lblAdmissionNo : UILabel?
    @IBOutlet weak var lblRollNo : UILabel?
    @IBOutlet weak var btnPresent: CustomButtom?
    @IBOutlet weak var btnAbsent: CustomButtom?
    @IBOutlet weak var btnLeave: CustomButtom?
    
    var attendanceMarkedStatus : Bool = false
    var index : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    
    func configure() {
        if let modal = modal as? Student {
            if let imgUrl = modal.photo?.getImageUrl() {
                imgUser?.loadURL(imageUrl: imgUrl, placeholder: nil, placeholderImage: nil)
            }
            else {
                imgUser?.image = R.image.noProfile_Big()
            }
            
            if let name = modal.stName {
                lblName?.text = name
            }
            
            if let otherDTL = modal.otherDTL {
                lblRollNo?.isHidden = !(otherDTL.count == 2)
                lblAdmissionNo?.isHidden = !(otherDTL.count == 1) || !(otherDTL.count == 2)
                
                if otherDTL.count == 1 {
                    lblAdmissionNo?.text = "\(/otherDTL[0].key) : \(/otherDTL[0].value)"
                }
                else if otherDTL.count == 2 {
                    lblAdmissionNo?.isHidden = false
                    lblRollNo?.isHidden = false
                    lblAdmissionNo?.text = "\(/otherDTL[0].key) : \(/otherDTL[0].value)"
                    lblRollNo?.text = "\(/otherDTL[1].key) : \(/otherDTL[1].value)"
                }
                else {
                    lblAdmissionNo?.isHidden = true
                    lblRollNo?.isHidden = true
                }
            }
            else {
                lblAdmissionNo?.isHidden = true
                lblRollNo?.isHidden = true
            }
            updateAttendance(tag : /modal.status)
        }
    }
    
    
    @IBAction func btnMarkAttendanceAct(_ sender : UIButton){
        
        if (modal as? Student)?.constant == 1 || attendanceMarkedStatus{
            return
        }
        else {
            (topMostVC as? MarkAttendanceViewController)?.studentArr?[/index].status = sender.tag
            updateAttendance(tag : sender.tag)
        }
    }
    
    func updateAttendance(tag : Int) {
        btnPresent?.borderColor = tag == 1 ? UIColor.markPresentColor :  UIColor.cccccc
        btnAbsent?.borderColor =  tag == 2 ? UIColor.markAbsentColor : UIColor.cccccc
        btnLeave?.borderColor = tag == 3 ?   UIColor.markLeaveColor : UIColor.cccccc
        
        btnPresent?.setTitleColor(tag == 1 ? UIColor.white :  UIColor.cccccc, for: .normal)
        btnAbsent?.setTitleColor(tag == 2 ? UIColor.white :  UIColor.cccccc, for: .normal)
        btnLeave?.setTitleColor(tag == 3 ? UIColor.white :  UIColor.cccccc, for: .normal)
        
        btnPresent?.backgroundColor = tag == 1 ? UIColor.markPresentColor :  UIColor.white
        btnAbsent?.backgroundColor =  tag == 2 ? UIColor.markAbsentColor : UIColor.white
        btnLeave?.backgroundColor = tag == 3 ?   UIColor.markLeaveColor : UIColor.white
    }
}
