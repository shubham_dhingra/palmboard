//  SurveyGridTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 15/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.

import UIKit

class SurveyGridTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName    : UILabel?
    @IBOutlet weak var lblOverall : UILabel?
    @IBOutlet weak var lblMale    : UILabel?
    @IBOutlet weak var lblFemale  : UILabel?
    @IBOutlet weak var viewName     : UIView?{
        didSet{
            viewName?.addBorder(width: 0.8, color: UIColor(rgb: 0xEBEBEB))
        }
    }
    @IBOutlet weak var viewOverall  : UIView?{
        didSet{
            viewOverall?.addBorder(width: 0.8, color: UIColor(rgb: 0xEBEBEB))
        }
    }
    @IBOutlet weak var viewMale     : UIView?{
        didSet{
            viewMale?.addBorder(width: 0.8, color: UIColor(rgb: 0xEBEBEB))
        }
    }
    @IBOutlet weak var viewFemale   : UIView? {
        didSet{
            viewFemale?.addBorder(width: 0.8, color: UIColor(rgb: 0xEBEBEB))
        }
    }
    
    var maleCount : Int?
    var femaleCount : Int?
    var totalSpecificUserCount : Int?
    var totalAllUserCount : Int?
    
    var index : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
       
        if let modal = modal as? String {
            lblName?.text   = modal
            if index == 0 {
                lblOverall?.text =  "Overall"
                lblMale?.text    = "Male"
                lblFemale?.text  = "Female"
            }
            else {
                if totalAllUserCount != 0 {
                    lblOverall?.text =  "\((((/totalSpecificUserCount?.toDouble) / (/totalAllUserCount?.toDouble  * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
                }
                else {
                    lblOverall?.text = "0.0%"
                }
                
                
                if totalSpecificUserCount != 0 {
                    lblMale?.text    = "\((((/maleCount?.toDouble) / (/totalSpecificUserCount?.toDouble * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
                    lblFemale?.text  = "\((((/femaleCount?.toDouble) / (/totalSpecificUserCount?.toDouble * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
                }
                else {
                    lblMale?.text    = "0.0%"
                    lblFemale?.text  = "0.0%"
                }
                
            }
         }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
