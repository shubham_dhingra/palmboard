//
//  SurveyListCell.swift
//  PalmBoard
//
//  Created by Shubham on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit

class SurveyListCell : TableParentCell {

    //MARK::- OUTLETS
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblDesc : UILabel?
    @IBOutlet weak var btnMore : UIButton?
    @IBOutlet weak var lblPublishedOn : UILabel?
    @IBOutlet weak var lblOpenTill : UILabel?
    @IBOutlet weak var lblRespondOn : UILabel?
    
    @IBOutlet weak var btnHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var forCheckResponses : Bool = false
    var indexpath : IndexPath?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    @IBAction func btnMoreAct(_ sender : UIButton){
        
        (topMostVC as? SurveyViewController)?.surveyList?[sender.tag].isExpanded  =  !(/(topMostVC as? SurveyViewController)?.surveyList?[sender.tag].isExpanded)
        if let indexPath = indexpath {
            (topMostVC as? SurveyViewController)?.tableView?.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    func configure() {
        
        if let survey = modal as? Survey {
            lblTitle?.text = survey.title
            lblDesc?.text = survey.descriptionField
            btnMore?.tag = /indexpath?.row
            if let frame = lblDesc?.frame {
              let lblTempDesc = UILabel(frame: frame)
                lblTempDesc.text = survey.descriptionField
                lblTempDesc.numberOfLines = 0
               
                if /survey.descriptionField?.count == 0 {
                    btnMore?.setTitle(nil, for: .normal)
                }
                lblDesc?.numberOfLines = survey.isExpanded ? 0 : 2
                btnMore?.setTitle(survey.isExpanded ? "Less" : "More", for: .normal)
                if  !(survey.isExpanded) {
                var descHeight = lblDesc?.getEstimatedHeight()
                if descHeight == 0.0 {
                    lblDesc?.text = nil
                    descHeight = 1.0
                }
                print("No of lines to survey \(/indexpath?.row) : \(Int(lblTempDesc.getEstimatedHeight()) / Int(/descHeight))")
                if (Int(lblTempDesc.getEstimatedHeight()) / Int(/descHeight)) >= 2 {
                   btnMore?.isHidden = false
                    btnHeightConstraint?.constant = 32.0
                }
                else {
                  btnMore?.isHidden = true
                  btnHeightConstraint?.constant = 0.0
                }
                }
                else {
                    btnMore?.isHidden = false
                     btnHeightConstraint?.constant = 32.0
                }
            }
           
            //publishedOn
            var publishedOnDate = "\(/survey.publishedOn?.getTimeIn12HourFormat(IPFormat: DateFormats.Date1WithMSec.get ,  OPFormat: DateFormats.Date2.get))"
            if publishedOnDate.count == 0 {
                publishedOnDate = /survey.publishedOn
            }
            lblPublishedOn?.text = "Published On : \(publishedOnDate.getTimeIn12HourFormat(IPFormat: DateFormats.Date2.get, OPFormat: "dd MMM, yyyy")) at \(/publishedOnDate.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "h:mm a"))"
            
            
            //open till
            lblOpenTill?.text =  /survey.isOpen ? "Open till : \(/survey.openEndDate)"
                : "Closed On : \(/survey.openEndDate)"
            
            
            //respond On
            if forCheckResponses {
                lblRespondOn?.isHidden = true
            }
            else {
               lblRespondOn?.isHidden = !(/survey.isResponded)
                if /survey.isResponded {
                    var respondOn = "\(/survey.respondedOn?.getTimeIn12HourFormat(IPFormat: DateFormats.Date1WithMSec.get, OPFormat:  DateFormats.Date2.get))"
                    
                    if respondOn.count == 0 {
                        respondOn = /survey.respondedOn
                    }
                    lblRespondOn?.text = "RESPONDED ON : \(respondOn.getTimeIn12HourFormat(IPFormat: DateFormats.Date2.get, OPFormat: "dd MMM, yyyy")) at \(/respondOn.getTimeIn12HourFormat(IPFormat: DateFormats.Date2.get, OPFormat: "h:mm a"))"
                }
            }
           
            
            
        }
    }
    
}
