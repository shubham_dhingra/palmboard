


//
//  SurveyQuestionCell.swift
//  PalmBoard
//
//  Created by Shubham on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit

protocol AnswerDelegate {
    func optionSelectDelegate(quesNo : Int , optionNo : Int)
}

class SurveyQuestionCell: TableParentCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblOption : UILabel?
    @IBOutlet weak var btnOption : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var index : Int?
    var quesNo : Int?
    var isResult : Bool = false
    var totalUserAttempt : Int?
    var ismultiSelectQues : Bool?
    var delegate : AnswerDelegate?
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    var option : Any? {
        didSet {
            configureForResponse()
        }
    }
    
    func configure() {
        if let modal = modal as? Option {
            if isResult {
                if let totalUserAttempt = totalUserAttempt {
                    if totalUserAttempt != 0 {
                        let percentage =  (Double(/modal.response) / Double(totalUserAttempt)) * 100.0
                        lblOption?.attributedText = Utility.shared.getAttributedString(firstStr: /modal.option, firstAttr: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 16.0)!], secondStr: " - \(/modal.response) (\(percentage.rounded())%)", secondAttr: [NSAttributedString.Key.font: R.font.ubuntuBold(size: 16.0)!])
                    }
                    else {
                        lblOption?.attributedText = Utility.shared.getAttributedString(firstStr: /modal.option, firstAttr: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 16.0)!], secondStr: " - \(/modal.response) (0.0%)", secondAttr: [NSAttributedString.Key.font: R.font.ubuntuBold(size: 16.0)!])
                    }
                    
                }
            }
            else {
                lblOption?.text = /modal.option
                /modal.isSelected ? btnOption?.setImage(UIImage(named : /ismultiSelectQues ?  "Checkbox-fill" : "radio_btn_selected") , for: .normal) :  btnOption?.setImage(UIImage(named : /ismultiSelectQues ?  "checkbox_unfill _gray" : "radio_btn") , for: .normal)
            }
        }
    }
    
    func configureForResponse() {
        if let modal = option as? Option {
            
            lblOption?.attributedText  = Utility.shared.getAttributedString(firstStr: "\(/index + 1)) ", firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr:  /modal.option, secondAttr: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 14.0)!])
             btnOption?.backgroundColor = modal.assignColor
            
        }
    }
    
    
    @IBAction func btnOptionAct(_ sender : UIButton){
        if !isResult {
            if let quesNo = quesNo , let optionNo = index{
                delegate?.optionSelectDelegate(quesNo: quesNo, optionNo: optionNo)
            }
        }
    }
    
    func saveColorWithOption(_ color : UIColor) {
        (self.topMostVC as? SurveyResDataViewController)?.questions?.options?[/index].assignColor = color
    }
}
