//
//  SurveyReportTableCell.swift
//  PalmBoard
//
//  Created by Shubham on 14/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class SurveyReportTableCell: UITableViewCell {

    @IBOutlet weak var resultCollectionView : UICollectionView?
    
    var surveyUserArr = [SurveyUserDTL]()
    var modal : SurveyPieChartModal?
    
    var selectIndex : Int = 0
    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    var isFirstTimeLoadResultCollection : Bool = true
    var resultCollectionDataSource : SurveyPieChartCustomDataSource?{
        didSet{
            resultCollectionView?.dataSource = resultCollectionDataSource
            resultCollectionView?.delegate = resultCollectionDataSource
            resultCollectionView?.reloadData()
        }
    }
   
}


// Collection View For Users Count
extension SurveyReportTableCell {
    
    func configureresultCollectionView() {
        
        resultCollectionDataSource =  SurveyPieChartCustomDataSource(items: surveyUserArr, collectionView: resultCollectionView, cellIdentifier: R.reuseIdentifier.surveyUserCollectionCell.identifier, headerIdentifier: nil, cellHeight: /self.frame.size.height , cellWidth: UIScreen.main.bounds.width , modal : self.modal, drawPieChartFirstTime : isFirstTimeLoadResultCollection)
        
        resultCollectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
        }
        
        resultCollectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
        
        resultCollectionDataSource?.scrollViewListener = {(scrollView) in
            self.onScrollCollection()
        }
    }
    
    func onScrollCollection(){
        var visibleRect = CGRect()
       
        guard let resultCollectionView = self.resultCollectionView else {
            return
        }
        visibleRect.origin = resultCollectionView.contentOffset
        visibleRect.size = resultCollectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = resultCollectionView.indexPathForItem(at: visiblePoint) else { return }
        print(indexPath.row)
        
        self.selectIndex = indexPath.row
        resultCollectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.right , animated: true)
        (topMostVC as? SurveyResDataViewController)?.selectIndex = self.selectIndex
        (topMostVC as? SurveyResDataViewController)?.reloadResultTable()
      
    }
    
    func reloadResultCollection() {
        if isFirstTimeLoadResultCollection {
            configureresultCollectionView()
            isFirstTimeLoadResultCollection = !isFirstTimeLoadResultCollection
        }
        else {
            resultCollectionDataSource?.items = surveyUserArr
            resultCollectionDataSource?.drawPieChartFirstTime = false
            resultCollectionView?.reloadData()
            resultCollectionView?.scrollToItem(at: IndexPath(row: self.selectIndex, section: 0), at: UICollectionView.ScrollPosition.right, animated: true)

        }
    }
}
