//
//  SurveyQuestionSectionHeader.swift
//  PalmBoard
//
//  Created by Shubham on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//
protocol OpenReportDelegate {
    func openReportDelegate(_ tag : Int)
}
import UIKit

class SurveyQuestionSectionHeader : UIView {


    //MARK::- OUTLETS
    @IBOutlet weak var lblQuestion : UILabel?
    @IBOutlet weak var lblTotalRes : UILabel?
    @IBOutlet weak var btnOpenReport : UIButton?
    @IBOutlet weak var btnHeightConstraint : NSLayoutConstraint?
    @IBOutlet weak var linearView : UIView?
    

    //MARK::- VARIABLES
    var forCheckResponses : Bool = false
    var model : Any? {
        didSet {
            configure()
        }
    }
    var delegate : OpenReportDelegate?
    
    //MARK::- CONFIGURE SECTION HEADER
    func configure() {
        btnHeightConstraint?.constant = forCheckResponses ? 28.0 : 0.0
       linearView?.isHidden = !forCheckResponses
    }
    
    class func instanceFromNib() -> UIView? {
        return UINib(nibName: "SurveyQuestionSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    
    @IBAction func btnOpenReportAct(_ sender : UIButton){
        if delegate != nil {
            delegate?.openReportDelegate(sender.tag)
        }
    }
    
 
}
