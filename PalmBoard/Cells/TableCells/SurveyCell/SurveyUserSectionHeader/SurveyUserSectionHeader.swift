//
//  SurveyUserSectionHeader.swift
//  PalmBoard
//
//  Created by Shubham on 14/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import Foundation
import EZSwiftExtensions

class SurveyUserSectionHeader : UIView {

    @IBOutlet weak var collectionView : UICollectionView?
    
    var surveyUserDTLArr = [SurveyUserDTL]()
    var selectIndex : Int = 0 
    var isFirstTimeLoadUserCollection : Bool = true
    
    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    var collectionDataSource: CollectionViewDataSource?{
        didSet{
            collectionView?.dataSource = collectionDataSource
            collectionView?.delegate = collectionDataSource
            collectionView?.reloadData()
        }
    }
    
    class func instanceFromNib() -> UIView? {
        
        return UINib(nibName: "SurveyUserSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: surveyUserDTLArr, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.surveyUserCollectionCell.identifier, headerIdentifier: nil, cellHeight: 52.0 , cellWidth: /UIScreen.main.bounds.width / 4.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell  = cell as? SurveyUserCollectionCell , let index = indexpath?.row else {
                return
            }
            cell.linearView?.isHidden = !(self.selectIndex == index)
            cell.modal = item
        }
        
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
             self.didSelect(indexpath.row)
        }
        collectionDataSource?.scrollViewListener = {(scrollView) in
        
        }
    }
    
    
    func didSelect(_ index : Int){
        (topMostVC as? SurveyResDataViewController)?.selectIndex = index
        (topMostVC as? SurveyResDataViewController)?.reloadResultTable()
    }
    
    func reloadUserCollection() {
        collectionView?.register(R.nib.surveyUserCollectionCell)
        if isFirstTimeLoadUserCollection {
            configureCollectionView()
            isFirstTimeLoadUserCollection = !isFirstTimeLoadUserCollection
            
        }
        else {
            collectionDataSource?.items = surveyUserDTLArr
            collectionView?.reloadData()
        }
        collectionView?.scrollToItem(at: IndexPath(row: self.selectIndex, section: 0), at: UICollectionView.ScrollPosition.right , animated: true)
    }

}
