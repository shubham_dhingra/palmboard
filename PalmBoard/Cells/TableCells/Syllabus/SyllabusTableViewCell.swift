//  SyllabusTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 25/04/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import UIKit
protocol SyllabusDelegate {
    func viewSyllabus(tag : Int)
}
class SyllabusTableViewCell: UITableViewCell {
    @IBOutlet weak var lblSubject : UILabel?
    @IBOutlet weak var lblTitle   : UILabel?
    @IBOutlet weak var lblFileSize: UILabel?
    @IBOutlet weak var btnView    : UIButton?
    var delegate : SyllabusDelegate?
    var index : Int?

    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? SyllabusLST
        {
            lblSubject?.text    = modal.subject
            lblTitle?.text      = modal.title
            let fileSize: Double =  Double(/modal.fileSize)
            let roundedValue2    = fileSize.roundToDecimal(2)
            print(roundedValue2)
            lblFileSize?.text   = roundedValue2 == 0.0 ? "\(fileSize.rounded()) kb" : "\(roundedValue2) mb"
            btnView?.tag        = /index
        }
    }
    @IBAction func ClkBtnView(_ sender : UIButton) {
        delegate?.viewSyllabus(tag:sender.tag)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
