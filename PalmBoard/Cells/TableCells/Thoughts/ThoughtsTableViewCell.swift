import UIKit
protocol ThoughtsDelegate {
    func ClkBtnProfile(tag : Int)
    func ClkLikesAllList(tag : Int)
    func ClkLikePressed(btn : UIButton)
    func ClkDotsPressed(tag : Int)
}
class ThoughtsTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblThought:     UILabel?
    @IBOutlet weak var lblAuthor:      UILabel?
    @IBOutlet weak var btnLike:        UIButton?
    @IBOutlet weak var imgStudent:     UIButton?
    @IBOutlet weak var btnDots:        UIButton?
    @IBOutlet weak var btnLikesNumber: UIButton?
    @IBOutlet weak var btnStudentInfo: UIButton?
    @IBOutlet weak var lblPending:     UILabel?
    @IBOutlet weak var ViewPending:    UIView?
    @IBOutlet weak var viewLike:       UIView?
    
    var delegate : ThoughtsDelegate?
    var index : Int?
    var segmentControl : UISegmentedControl?

    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? List
        {
            if let isVerified = modal.isVerified
            {
                btnDots?.isHidden     = isVerified == 0 ? false : true
                ViewPending?.isHidden = isVerified == 0 ? false : true
                viewLike?.isHidden    = isVerified == 0 ? true  : false
            }
            if segmentControl?.selectedSegmentIndex == 1 {
                btnDots?.isHidden     = false
            }
            btnLike?.tag        = /index
            btnDots?.tag        = /index
            btnLikesNumber?.tag = /index
            imgStudent?.tag     = /index
            btnStudentInfo?.tag = /index
            
            if let imgUrl = modal.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgStudent?.setBackgroundImage(img1, for: .normal)
                }
            }
            else{
                imgStudent?.setBackgroundImage(R.image.noProfile_Big(), for: .normal)
            }
            if let strQuotation = modal.quotation{
                lblThought?.text = "\"\(strQuotation)\""}
            if let strAuthor    = modal.author{
                lblAuthor?.text = "~\(strAuthor)"}
            if let strName = modal.updatedBy{
                btnStudentInfo?.setTitle(strName.replace(target: "|", withString: ", "),for: .normal)
            }
            if let intLikes = modal.likes{
                btnLikesNumber?.isHidden = intLikes == 0 ? true : false
                btnLikesNumber?.setTitle(intLikes == 1 ? "\(intLikes) Like" :"\(intLikes) Likes" ,for: .normal)
            }
            if let isLiked = modal.isILike {
                btnLike?.setImage(isLiked == 1 ? R.image.liked() : R.image.like() , for: .normal)
            }
        }
    }
    @IBAction func ClkBtnProfile(_ sender: UIButton){
        delegate?.ClkBtnProfile(tag:sender.tag)
    }
    @IBAction func ClkLikesAllList(_ sender: UIButton){
        delegate?.ClkLikesAllList(tag:sender.tag)
    }
    @IBAction func likePressed(_ sender: UIButton) {
        delegate?.ClkLikePressed(btn:sender)
    }
    @IBAction func dotsPressed(_ sender: UIButton) {
        delegate?.ClkDotsPressed(tag:sender.tag)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // self.selectionStyle = UITableViewSelectionStyle.none
        lblPending?.layer.cornerRadius = 6.0
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
}

