

//
//  DayPlanCell.swift
//  PalmBoard
//
//  Created by Shubham on 23/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class DayPlanCell: UITableViewCell {
    
    @IBOutlet weak var txtTitle : UITextField? {
        didSet {
            txtTitle?.delegate = self
        }
    }
    @IBOutlet weak var txtDesc : UITextView? {
        didSet {
            placeholderLabel = Utility.shared.setUpTextViewPlaceholder(txtDesc , msg : AlertMsg.enterDescription.get)
            txtDesc?.addSubview(placeholderLabel)
            txtDesc?.delegate = self
            
        }
    }
    @IBOutlet weak var btnAttachment : UIButton?
    @IBOutlet weak var collectionAttachmentHeight : NSLayoutConstraint?
    @IBOutlet weak var btnOral : UIButton?
    @IBOutlet weak var btnWritten : UIButton?
    @IBOutlet weak var collectionView : UICollectionView?
    
    var placeholderLabel : UILabel!
    var topMostVC : UIViewController?
    
    
    var collectionDataSource : CollectionViewDataSource? {
        didSet {
            collectionView?.delegate = collectionDataSource
            collectionView?.dataSource = collectionDataSource
            collectionView?.reloadData()
        }
    }
    
    var attachmentArr : [AttachmentModal]?
    var loadAttachmentFirstTime : Bool = true
    var workTypeIndex : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure() {
        
        if let modal = modal as? WorkTypeModal {
            txtTitle?.text = modal.Topic
            txtDesc?.text = modal.Description
            placeholderLabel.isHidden = !(/txtDesc?.text.isEmpty)
            attachmentArr = modal.Attachments
            if modal.Method == 0 {
                btnOral?.isSelected = false
                btnWritten?.isSelected = false
            }
            else if modal.Method == 1 {
                btnOral?.isSelected = true
                btnWritten?.isSelected = false
            }
            else if modal.Method == 2 {
                btnOral?.isSelected = false
                btnWritten?.isSelected = true
            }
            else {
                btnOral?.isSelected = true
                btnWritten?.isSelected = true
            }
        }
        
        //Maximum 2 attachments are allowed to be attached in a particular workType
        
        btnAttachment?.isHidden = self.attachmentArr?.count == 2
        
//        if /self.attachmentArr?.count != 0 {
            loadAttachmentCollection()
//        }
        
        if self.attachmentArr == nil || self.attachmentArr?.count == 0{
            collectionAttachmentHeight?.constant = 0.0
            btnAttachment?.isHidden = false
        }
    }
    
    @IBAction func btnAttachmentAct(_ sender: UIButton) {
        openMedia(sender)
    }
    
    @IBAction func btnOralAct(_ sender: UIButton){
        btnOral?.isSelected = !(/btnOral?.isSelected)
        updateModal()
    }
    
    @IBAction func btnWrittenAct(_ sender: UIButton){
        btnWritten?.isSelected = !(/btnWritten?.isSelected)
        updateModal()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtDesc?.delegate = self
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func updateModal() {
        
        if let vc = topMostVC as? WeekPlanListViewController {
            print("Day : \(vc.currentDayIndex) Type Work :\(/self.workTypeIndex)")
            let currentDayIndex = vc.currentDayIndex
            
            guard let workType = vc.weekPlanModal?.DayPlans[currentDayIndex].WorkPlans , let workTypeIndex = self.workTypeIndex else {
                print("No workType found for Day \(currentDayIndex)")
                return
            }
            
            // work title and desc update in the modal
            workType[workTypeIndex].Description = txtDesc?.text
            workType[workTypeIndex].Topic = txtTitle?.text
            
            
            //work Type is updated in the modal
            if /btnWritten?.isSelected && /btnOral?.isSelected {
                workType[workTypeIndex].Method = 3
            }
            else if /btnWritten?.isSelected{
                workType[workTypeIndex].Method = 2
            }
            else if /btnOral?.isSelected {
                workType[workTypeIndex].Method = 1
            }
            else {
                workType[workTypeIndex].Method = 0
            }
            vc.weekPlanModal?.DayPlans[currentDayIndex].WorkPlans = workType
        }
    }
    
    func updateMediaInParentModal(attachmentIndex : Int? = nil , action : Action , uploadAttachment : AttachmentModal? = nil) {
        
        
        guard let vc = topMostVC as? WeekPlanListViewController else {
            print("Wrong Controller")
            return
        }
        
        let currentDayIndex = vc.currentDayIndex
        
        guard let workType = vc.weekPlanModal?.DayPlans[currentDayIndex].WorkPlans , let workTypeIndex = self.workTypeIndex else {
            print("No workType found for Day \(currentDayIndex)")
            return
        }
        
        
        
        if action == .Add {
            
            guard let uploadAttachment = uploadAttachment else {
                print("No modal for attachment")
                return
            }
            workType[workTypeIndex].Attachments.append(uploadAttachment)
            //update in parent Modal
            vc.weekPlanModal?.DayPlans[currentDayIndex].WorkPlans = workType
        }
            
            //Delete
        else {
            guard let attachmentIndex = attachmentIndex else {
                return
            }
            
            if attachmentIndex < workType[workTypeIndex].Attachments.count {
                workType[workTypeIndex].Attachments.remove(at: attachmentIndex)
                self.attachmentArr = workType[workTypeIndex].Attachments
                self.loadAttachmentCollection()
                vc.weekPlanModal?.DayPlans[currentDayIndex].WorkPlans = workType
            }
        }
        vc.reloadTable()
    }
}

extension DayPlanCell {
    
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: attachmentArr, collectionView: collectionView, cellIdentifier: CellIdentifiers.PlanAttachmentCell.get, headerIdentifier: nil, cellHeight: 30.0 , cellWidth: (/self.collectionView?.size.width / 2))
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell         = cell as? PlanAttachmentCell , let index  = indexpath?.row else {return}
            cell.delegate           = self
            cell.lblName?.text      = "\(AlertConstants.Attachment.get) \(index + 1)"
            cell.btnDelete?.tag     = index
            cell.updateUI()
        }
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            
        }
    }
    
    func loadAttachmentCollection() {
        
        collectionAttachmentHeight?.constant = /self.attachmentArr?.count != 0 ? 32.0 : 0.0
        if loadAttachmentFirstTime {
            configureCollectionView()
            loadAttachmentFirstTime = false
        }
        else {
            collectionDataSource?.items = attachmentArr
            collectionView?.reloadData()
        }
    }
}

extension DayPlanCell : DeleteAttachmentDelegate {
    func deleteAttachment(tag : Int){
        self.updateMediaInParentModal(attachmentIndex: tag, action: .Delete, uploadAttachment: nil)
    }
}


extension DayPlanCell : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        updateModal()
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        updateModal()
        self.endEditing(true)
        return true
    }
}

extension DayPlanCell : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updateModal()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateModal()
    }
}

extension DayPlanCell {
    
    func openMedia(_ sender : UIButton) {
        
        MediaPicker.shared.captureMedia(from: topMostVC!, captureOptions: [.camera,.photoLibrary], allowEditting: true, fromView: sender) { (typeMedia, image, url, fileExt) in
            switch typeMedia {
            case .image :
                if image != nil {
                let attachment  = AttachmentModal.init(media: typeMedia, object: image , fileUrlForDocx : nil)
                self.updateMediaInParentModal(attachmentIndex: nil, action: .Add, uploadAttachment: attachment)
                break
                }
                
            case .pdf :
                if url != nil {
            
                let attachment  = AttachmentModal.init(media: typeMedia, object: image , fileUrlForDocx : url)
                self.updateMediaInParentModal(attachmentIndex: nil, action: .Add, uploadAttachment: attachment)
                break
                }
                
            default:
                break
                //openContactList()
                
            }
        }
    }
}

extension DayPlanCell {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 150
    }
}
