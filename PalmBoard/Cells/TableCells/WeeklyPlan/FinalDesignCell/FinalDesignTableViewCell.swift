//  FinalDesignTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
protocol ApprovalAndEditDelegate {
    func editPlan(tag : Int , section : Int?)
    func approveRejectPlan(section : Int? , planId : String? , tag : Int , Status : Int)
    func senderTag(tag: Int, strURL : String)
}

class FinalDesignTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblDesc : UILabel?
    @IBOutlet weak var lblMethod : UILabel?
    @IBOutlet weak var lblWorkType : UILabel?
    @IBOutlet weak var imgWorkType    : UIImageView?
    @IBOutlet weak var collectionAttachmentHeight : NSLayoutConstraint?
    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet weak var btnStatus     : UIButton?
    @IBOutlet weak var btnEdit       : UIButton?
    @IBOutlet weak var approvalView  : UIView?
    @IBOutlet weak var stackApproval : UIStackView?
    @IBOutlet weak var btnReject     : UIButton?
    @IBOutlet weak var btnAccept     : UIButton?
    
    
    var index : Int?
    var planId : Int?
    var section : Int?
    var userRole : Users?
    var action : Action?
    var status : Int?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    var delegate : ApprovalAndEditDelegate?
    
    var collectionDataSource : CollectionViewDataSource? {
        didSet {
            collectionView?.delegate = collectionDataSource
            collectionView?.dataSource = collectionDataSource
            collectionView?.reloadData()
        }
    }
    
    var attachmentArr : [AttachmentModal]?
    var loadAttachmentFirstTime : Bool = true
    func configure() {
        if let modal = modal as? WorkTypeModal {
            if let index = index {
                lblWorkType?.text = /modal.WorkType == 1 ? "Home Work" : "Class Work"
                imgWorkType?.image = UIImage(named: index == 1 ? "home_work_icon" : "class_work_icon")
            }
            status = modal.Status
            lblTitle?.text = "Title : \(/modal.Topic?.trimmed())"
            lblDesc?.text = "Description : \(/modal.Description?.trimmed())"
            self.planId = modal.PlnID
            if let method = modal.Method {
                if method == 0 {
                    lblMethod?.text = "Method : "
                }
                else if  method == 1 {
                    lblMethod?.text = "Method : Oral"
                }
                else if  method == 2 {
                    lblMethod?.text = "Method : Written"
                }
                else if  method == 3 {
                    lblMethod?.text = "Method : Oral , Written"
                }
                else {
                    lblMethod?.text = "Method : "
                }
            }
            self.attachmentArr = modal.Attachments
            
            if /self.attachmentArr?.count != 0 {
                loadAttachmentCollection()
            }
            else if  self.attachmentArr == nil || self.attachmentArr?.count == 0{
                self.attachmentArr = []
                loadAttachmentCollection()
            }
         
            if userRole != .Cordinator {
                btnEdit?.isHidden = false
                btnEdit?.isUserInteractionEnabled =  !(modal.Status == 1)
                btnEdit?.alpha =  modal.Status == 1 ? 0.5 : 1
            }
            
            if userRole == .Cordinator {
                
                btnReject?.tag = /index
                btnAccept?.tag = /index
                
                btnAccept?.setTitle(modal.Status == 1 ? "Approved" : "Approve", for: .normal)
                btnAccept?.setTitleColor(modal.Status == 1 ? UIColor.white : UIColor(rgb: 0x56B54B), for: .normal)
                btnAccept?.setBackgroundColor(modal.Status == 1 ?  UIColor(rgb: 0x56B54B) : UIColor.white, forState: .normal)
                
                btnReject?.setTitle(modal.Status == 2 ? "Rejected" : "Reject", for: .normal)
                btnReject?.setTitleColor(modal.Status == 2 ? UIColor.white : UIColor(rgb: 0xF3565D), for: .normal)
                btnReject?.setBackgroundColor(modal.Status == 2 ?  UIColor(rgb: 0xF3565D) : UIColor.white, forState: .normal)
                
                btnAccept?.isHidden = modal.Status == 2
                btnReject?.isHidden = modal.Status == 1
                
                btnAccept?.isUserInteractionEnabled = modal.Status == 0
                btnReject?.isUserInteractionEnabled = modal.Status == 0
                
            }
            
            if userRole != .Cordinator {
                switch status {
                case 0:
                    
                    btnStatus?.setTitle(LeaveStatus.Pending.rawValue, for: .normal)
                    btnStatus?.setBackgroundColor(LeaveStatus.Pending.color, forState: .normal)
                    
                case 1:
                    
                    btnStatus?.setTitle(LeaveStatus.Approved.rawValue, for: .normal)
                    btnStatus?.setBackgroundColor(LeaveStatus.Approved.color, forState: .normal)
                    
                case 2:
                    
                    btnStatus?.setTitle(LeaveStatus.Rejected.rawValue, for: .normal)
                    btnStatus?.setBackgroundColor(LeaveStatus.Rejected.color, forState: .normal)
                    
                default :
                    break
                }
            }
        }
    }
    
    @IBAction func btnEditAct(_ sender : UIButton){
        delegate?.editPlan(tag: /index , section : section)
    }
    
    @IBAction func btnApprovalAct(_ sender : UIButton){
        delegate?.approveRejectPlan(section : section , planId : planId?.toString , tag: sender.tag, Status: 1)
    }
    
    @IBAction func btnRejectAct(_ sender : UIButton){
        delegate?.approveRejectPlan(section : section  , planId : planId?.toString , tag: sender.tag, Status: 2)
    }
}

extension FinalDesignTableViewCell {
    
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: attachmentArr, collectionView: collectionView, cellIdentifier: CellIdentifiers.PlanAttachmentCell.get, headerIdentifier: nil, cellHeight: self.userRole == .Cordinator ? 47.0 : 32.0 , cellWidth: /self.collectionView?.frame.width /  2.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell          = cell as? PlanAttachmentCell , let index  = indexpath?.row else {return}
            cell.lblName?.text      = "\(AlertConstants.Attachment.get) \(index + 1)"
            cell.viewWidthHeightConstraint?.constant = self.userRole == .Cordinator ? 32.0 : 0.0
            cell.updateUI()
        }
        
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            
            if self.action == .Create {
               return
            }
            if self.userRole != .Cordinator {
                
                if (self.userRole == .Student) || (self.userRole == .Parent) || (self.userRole == .Staff)  {
                    self.didSelect(indexpath.row)
                }
                else{
                    return
                }
            }
            else {
                self.didSelect(indexpath.row)
            }
                
        }
   }
    func didSelect(_ index : Int){
        if index < /attachmentArr?.count {
            navigateToFinalPlan(fileURL : /self.attachmentArr?[index].FileURL, tag: index )
        }
    }
    func navigateToFinalPlan(fileURL : String,tag: Int) {
        delegate?.senderTag(tag: tag, strURL: fileURL)
    }
    func loadAttachmentCollection() {
        
        
        if self.attachmentArr == nil {
             collectionAttachmentHeight?.constant =  0
        }
        else {
        if userRole != .Cordinator {
            collectionAttachmentHeight?.constant = /self.attachmentArr?.count != 0 ? 32.0 : 0.0
        }
        else {
             collectionAttachmentHeight?.constant = /self.attachmentArr?.count != 0 ? 50.0 : 0.0
        }
        }
        
        
        if loadAttachmentFirstTime {
            configureCollectionView()
            loadAttachmentFirstTime = false
        }
        else {
            collectionDataSource?.items = attachmentArr
            collectionView?.reloadData()
        }
    }
}
