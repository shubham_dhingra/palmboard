//  NoDataCell.swift
//  PalmBoard
//  Created by Shubham on 27/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class NoDataCell: UITableViewCell {

    @IBOutlet weak var lblNoData : UILabel?
    @IBOutlet weak var imgNoData : UIImageView?
    @IBOutlet weak var lblWorkType : UILabel?
    @IBOutlet weak var imgWorkType : UIImageView?
    @IBOutlet weak var workTypeHeightConstraint : NSLayoutConstraint?
    @IBOutlet weak var btnStatus : UIButton?
    @IBOutlet weak var btnEdit  : UIButton?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var section : Int?
    var isSkip : Bool = false
    var index : Int?
    var delegate : ApprovalAndEditDelegate?
    var modal : Any? {
        didSet {
            configure()
        }
    }
    
    func configure(){
        workTypeHeightConstraint?.constant = isSkip ? 0.0 : 60.0
        imgWorkType?.isHidden = isSkip
        lblWorkType?.isHidden = isSkip
        if isSkip {
            return
        }
        else{
            if let modal = modal as? WorkTypeModal {
            if let index = index {
                lblWorkType?.text = /modal.WorkType == 1 ? "Home Work" : "Class Work"
                imgWorkType?.image = UIImage(named: index == 1 ? "home_work_icon" : "class_work_icon")
            }
        }
        }
    }
    
    @IBAction func btnEditAct(_ sender : UIButton){
        delegate?.editPlan(tag: /index , section : section)
    }
    
}
