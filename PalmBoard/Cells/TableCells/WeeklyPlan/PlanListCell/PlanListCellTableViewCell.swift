//  PlanListCellTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class PlanListCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblSubClass: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
        if let modal = modal as? WeeklyPlanList {
            lblPlanName?.text = /modal.Title?.trimmed()
            lblSubClass?.text = "\(/modal.ClassName) (\((/modal.Subject)))"
            let fromDate = modal.FromDate?.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "dd MMM, yyyy")
            let endDate = modal.EndDate?.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "dd MMM, yyyy")
            lblTime?.text = "\(/fromDate) to \(/endDate)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
