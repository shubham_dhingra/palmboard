//
//  PlanViewCell.swift
//  PalmBoard
//
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

protocol ButtonActViewDelegate {
    func sendBtnViewTag(_ tag : Int)
}

class PlanViewCell: UITableViewCell {
    
    var delegate : ButtonActViewDelegate?
    
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func ClkBtnView(_ sender : UIButton){
        delegate?.sendBtnViewTag(sender.tag)
    }
    
}
