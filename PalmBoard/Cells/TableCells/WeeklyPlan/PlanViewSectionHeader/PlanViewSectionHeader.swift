
//
//  PlanViewSectionHeader.swift
//  PalmBoard
//
//  Created by Shubham on 23/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

protocol EditPlanDelegate{
    func editPlan(tag : Int , section : Int?)
}
class PlanViewSectionHeader: UIView {

    @IBOutlet weak var lblDay  : UILabel?
    @IBOutlet weak var lblDate : UILabel?
    @IBOutlet weak var btnEdit : UIButton?
    @IBOutlet weak var containerView : UIView?
    
    var delegate : EditPlanDelegate?
    class func instanceFromNib() -> UIView? {
        return UINib(nibName: "PlanViewSectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    @IBAction func btnEditAct(_ sender : UIButton) {
        delegate?.editPlan(tag: -1 , section : sender.tag)
    }
}
