//
//  WeeklyPlanHeaders.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class WeeklyPlanHeaders: UIView {

    @IBOutlet weak var lblName           : UILabel?
    @IBOutlet weak var imgType           : UIImageView?
    @IBOutlet weak var btnExpandCollapse : UIButton?
    @IBOutlet weak var linearView : UIView?
    
    class func instanceFromNib() -> UIView? {
        return UINib(nibName: "WeekPlanHeaders", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}
