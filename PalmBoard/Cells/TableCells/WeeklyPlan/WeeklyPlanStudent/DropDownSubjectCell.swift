//  DropDownSubjectCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 30/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class DropDownSubjectCell: UITableViewCell {
    //MARK::- OUTLETS
    @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var imgView: UIImageView?
    
    var index : Int?
    var model : Any?{
        didSet{
            configure()
        }
    }
    //MARK::- Configure Cell
    func configure() {
        //if let model = model as? MyClasses {
        //lblName?.text = model.ClassName
        //lblName?.textColor = /model.isSelected ? UIColor.flatGreen : UIColor.flatLightBlack
        //}
        if let model = model as? Subjects {
            lblName?.text      = model.SubjectName
            lblName?.textColor = /model.isSelected ? UIColor.flatGreen : UIColor.flatLightBlack
            imgView?.image     = /model.isSelected ? UIImage(named: "Check") : UIImage(named: "Uncheck")
        }
        else if let model = model as? StaffType{
            lblName?.text      = model.staffType
            lblName?.textColor = /model.isSelected ? UIColor.flatGreen : UIColor.flatLightBlack
            imgView?.image     = /model.isSelected ? UIImage(named: "Check") : UIImage(named: "Uncheck")
        }
    }
}
