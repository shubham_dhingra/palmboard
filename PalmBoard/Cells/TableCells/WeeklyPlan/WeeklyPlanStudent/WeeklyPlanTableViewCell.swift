//  WeeklyPlanTableViewCell.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 28/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

//    protocol ButtonActWeekStudentDelegate {
//        func sendBtnViewTag(_ tag : Int)
//    }
    class WeeklyPlanTableViewCell: UITableViewCell {
        
        @IBOutlet weak var lblPlanName  : UILabel?
        @IBOutlet weak var lblSubject   : UILabel?
        @IBOutlet weak var lblDate      : UILabel?
        @IBOutlet weak var lblUserName  : UILabel?
        @IBOutlet weak var imgView      : UIImageView?
        @IBOutlet weak var btnProfile   : UIButton?

       // var delegate : ButtonActWeekStudentDelegate?
        
        var modal : Any? {
            didSet {
                configure()
            }
        }
        func configure() {
            if let modal = modal as? WeeklyPlanListSchool{
                lblPlanName?.text = modal.title
                lblSubject?.text = modal.subject
                lblDate?.text = "\(/modal.fromDate?.dateChangeFormat()) to \(/modal.endDate?.dateChangeFormat())"
               // lblUserName?.text = modal.title
            }
        }
        override func awakeFromNib() {
            super.awakeFromNib()
            //imgView?.layer.cornerRadius = (imgView?.frame.height)! / 2
        }
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
        }
        @IBAction func ClkBtnProfile(_ sender : UIButton){
            print("ClkBtnProfile")
           // delegate?.sendBtnViewTag(sender.tag)
        }
}
