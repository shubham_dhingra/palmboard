//
//  TableParentCell.swift
//  e-Care Pro
//
//  Created by franciscan on 24/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class TableParentCell: UITableViewCell {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblName : UILabel?
    @IBOutlet weak var lblDesignation : UILabel?
    @IBOutlet weak var lblRelation : UILabel?
    @IBOutlet weak var lblMobileNo : UILabel?
    @IBOutlet weak var imgUser : UIImageView?
    @IBOutlet weak var btnDelete: UIButton?
    @IBOutlet weak var btnEdit : UIButton?
    @IBOutlet weak var linearView : UIView?
    @IBOutlet weak var btnWidthConstraint : NSLayoutConstraint?

    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnDeleteAct(_ sender : UIButton){
        
    }
    
    @IBAction func btnEditAct(_ sender : UIButton){
        
    }

}
