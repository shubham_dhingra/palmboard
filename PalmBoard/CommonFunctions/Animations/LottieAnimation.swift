//
//  LottieAnimation.swift
//  e-Care Pro
//
//  Created by Shubham on 11/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import Lottie
import UIKit

enum LottieAnimation : String{
    case CheckMarkSuccess = "Check Mark Success Data"
    case EmailSent = "email"
    case DownloadIcon = "download_icon_success"
    
    func start(parentView : UIView?) -> LOTAnimationView?{
        
        switch self {
        case .CheckMarkSuccess , .EmailSent , .DownloadIcon:
            if let parentView = parentView {
            var view = LOTAnimationView()
            view.center = parentView.center
            view.frame = CGRect(x: (parentView.frame.width / 2.0) - 50.0  , y: (parentView.frame.height / 2.0) - 50.0 , w: 100.0, h: 100.0)
            view = LOTAnimationView(name: self.rawValue)
            return view
            }
        }
        return nil
    }
    
    func stop(lottieView : LOTAnimationView){
       lottieView.removeFromSuperview()
    }
}



