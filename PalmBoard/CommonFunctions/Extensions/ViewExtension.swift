//
//  ViewExtension.swift
//  cozo
//
//  Created by Sierra 4 on 05/05/17.
//  Copyright © 2017 monika. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON


extension NSObject {
    var appDelegate:AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
}


private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String {
    func safelyLimitedTo(length n: Int)-> String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
    
    func getImageUrl() -> URL {
        
        var finalUrl    = "\(APIConstants.imgbasePath)" +  "\(self)"
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let imgURL = URL(string: finalUrl)
        return imgURL ?? URL(string : "")!
    }
    
    func getUrlForGallery(fromAWS : Bool? = false, AWSURL : String?, schoolCode : String?) -> URL{
        
        
        if /fromAWS {
            guard let awsUrl = AWSURL else {
                return URL(string : "")!
            }
            var finalUrl    = "\(awsUrl)/SchImg/\(/schoolCode)/PhotoAlbum/Thumb/" + "\(self)"
            finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let imgURL = URL(string: finalUrl)
            return imgURL ?? URL(string : "")!
        }
        else {
       
        var finalUrl    = "\(APIConstants.imgbasePath)/SchImg/\(/schoolCode)/PhotoAlbum/Thumb/" + "\(self)"
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            print("FinalURL : \(finalUrl)")
        let imgURL = URL(string: finalUrl)
        return imgURL ?? URL(string : "")!
        }
    }
    
    
    func getTeamCareUrl() -> URL {
        
        var finalUrl    = "\(APIConstants.teamCareImageBasePath)" +  "\(self)"
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let imgURL = URL(string: finalUrl)
        return imgURL ?? URL(string : "")!
    }
    
    func getSmartGuardUrl() -> URL {
        
        var finalUrl    = "\(APIConstants.smartGuardImageBasePath)" +  "\(self)"
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let imgURL = URL(string: finalUrl)
        return imgURL ?? URL(string : "")!
    }
    
}

extension UIView{
    func showAnimate()
    {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 1.0
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.removeFromSuperview()
            }
        });
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: self.frame.width, height: self.frame.height))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

infix operator =>
infix operator =|
infix operator =<

typealias OptionalJSON = [String : JSON]?

prefix operator /
prefix func /(value : String?) -> String {
    return value.unwrap()
}


enum XibError : Error {
    case XibNotFound
    case None
}

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func loadViewFromNib(withIdentifier identifier : String) throws -> UIView? {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:identifier, bundle: bundle)
        let xibs = nib.instantiate(withOwner: self, options: nil)
        
        if xibs.count == 0 {
            return nil
        }
        guard let firstXib = xibs[0] as? UIView else{
            throw XibError.XibNotFound
        }
        return firstXib
    }
    
    func roundView() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}



//MARK::- Extension
// 1) convertImageToBase64String
// 2) convertImageToByteArray



extension UIImage {
    
    //convertImageToBase64String
    func convertImageToBase64() -> String {
        let imageData    = self.jpegData(compressionQuality: 0.5)!//UIImageJPEGRepresentation(self, 1.0)!
        let base64String = imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        return base64String
    }
    
    func covertImageToByteArray() -> [UInt8] {
        
        let base64String = self.convertImageToBase64()
        
        // Get the String.UTF8View.
        let bytes = base64String.utf8
        
        //convert into byte array
        var buffer = [UInt8](bytes)
        buffer[0] = buffer[0] + UInt8(1)
        
        return buffer
    }
    
    
}

//Get current Time ()
extension Int {
    public func getFutureAndPrevDate(isPreviousAllowed : Int) -> (Date? , String?) {
        var date = Date()
        let dateFormatter = DateFormatter()
        let noOfDays = isPreviousAllowed == 1 ? -(self) : self
        date = (Calendar.current as NSCalendar).date(byAdding: .day, value: noOfDays, to: Date(), options: [])!
        dateFormatter.dateFormat = "dd MMM, yyyy"
        return (date , dateFormatter.string(from: date))
    }
}


extension UIImageView {
    
    func loadURL(imageUrl : URL, placeholder : String?, placeholderImage : UIImage?,isCroppingReq : Bool? = false , croppedSize : CGSize? = CGSize(width: 50, height: 50)){
        
        //let plaecholderUrl = (placeholder != nil) ? URL(string: /placeholder) : imageUrl
        self.af_setImage(withURL: imageUrl, placeholderImage: placeholderImage == nil  ? R.image.noProfile_Big() : placeholderImage, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true) { (response) in
//            let image = self.image?.af_imageRoundedIntoCircle()
            if /isCroppingReq {
                let croppedImage = self.image?.scalingAndCropping(for: croppedSize ?? CGSize(width: 50.0, height: 50.0))
                self.image = croppedImage
            }
        }
    }
        
    func clearImageFromCache(validUrl: URL) {
        let urlRequest = Foundation.URLRequest(url: validUrl)
        let imageDownloader = UIImageView.af_sharedImageDownloader
        if let imageCache2 = imageDownloader.imageCache {
            _ = imageCache2.removeImage(for: urlRequest, withIdentifier: nil)
        }
    }
    
    func roundImageView() {
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}

extension Array where Element : AnyObject{
    
    func toJson() -> String {
        do {
            let data = self
            
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            //print(string)
            string = string.replacingOccurrences(of: "\\", with: "") as NSString
            //print(string)
//            string = string.replacingOccurrences(of: "\"", with: "") as NSString
            string = string.replacingOccurrences(of: " ", with: "") as NSString
            // print(string)
            return string as String
        }
        catch let error as NSError{
          //  print(error.description)
            return ""
        }
    }
}










