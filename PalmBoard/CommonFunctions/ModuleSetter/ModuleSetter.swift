//
//  ModuleSetter.swift
//  PalmBoard
//
//  Created by Shubham on 02/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit
import ObjectMapper
import SwiftyJSON

typealias MessageSettingResponse = (_ msgModal : MessageMainModal) -> ()
class ModuleSetter : NSObject {
    
    static let shared = ModuleSetter()
    var msgSettingResponse  : MessageSettingResponse?
    
    override init() {

    }
    
    func messageSetting(userID : String? , userType : String? , msgSettingResponse : @escaping  MessageSettingResponse){
        self.msgSettingResponse = msgSettingResponse
        APIManager.shared.request(with: HomeEndpoint.messageSetting(UserId: userID, UserType: userType), isLoader: false) { [weak self](response) in
            self?.handleAPIResponse(response: response)
        }
    }
    
    
    func handleAPIResponse(response : Response?){
        if let response = response {
            switch response {
            case .success(let object):
                if let object = object as? MessageMainModal {
                    if let block = self.msgSettingResponse {
                        block(object)
                    }
                }
                break
            case .failure(let error):
                Messages.shared.show(alert: .oops, message: /error?.description, type: .warning)
            break
            }
        }
    }
}
