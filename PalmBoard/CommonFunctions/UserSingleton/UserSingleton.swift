
enum UDKeys: String{

    case logged_in   = "logged_in"
    case deviceToken = "deviceToken"

    func save(_ value: Any) {
        switch self{
            default:
                UserDefaults.standard.set(value, forKey: self.rawValue)
                UserDefaults.standard.synchronize()
            }
    }

    func fetch() -> Any? {

        switch self{
        default:
            guard let value = UserDefaults.standard.value(forKey: self.rawValue) else { return nil}
            return value
        }
    }

    func remove() {
        UserDefaults.standard.removeObject(forKey: self.rawValue)
    }
}
