import UIKit
//import Toaster
import EZSwiftExtensions


typealias AlertBlock = (_ success: AlertTag) -> ()
enum AlertTag {
    case done
    case yes
    case no
}

class AlertsClass: NSObject, FCAlertViewDelegate{
  
    
    static let shared = AlertsClass()
    var responseBack : AlertBlock?
    
    var alertView: FCAlertView = {
        let alert = FCAlertView()
        alert.dismissOnOutsideTouch = true
        
        return alert
    }()
    
    override init() {
        super.init()
    }
    
    
    //MARK: Alert Controller
    func showAlertController(withTitle title : String?, message : String, buttonTitles : [String], responseBlock : @escaping AlertBlock){
        
        alertView = FCAlertView()
        alertView.delegate = self
        responseBack = responseBlock
//        if let color =  self.getCurrentSchool()?.themColor {
//        alertView.colorScheme = color.hexStringToUIColor()
//        }
//        else {
         alertView.colorScheme = UIColor.flatGreen
//        }
        alertView.numberOfButtons = 2
        buttonTitles.count > 0 ? (alertView.hideDoneButton = true) : (alertView.hideDoneButton = false)
        
        alertView.showAlert(inView: ez.topMostVC, withTitle: title, withSubtitle: message, withCustomImage: nil, withDoneButtonTitle: nil, andButtons: buttonTitles.count > 0 ? buttonTitles : nil)
        
    }
    
    
    func showNativeAlert(withTitle title : String?, message : String? , fromVC : UIViewController? , buttonTitle : String? = AlertConstants.Ok.get){
        let alert = UIAlertController(title: /title, message: /message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: /buttonTitle , style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
//            fromVC?.isNavBarHidden       = true
//            (fromVC as? DownloadWeeklyPlanViewController)?.viewHeader?.isHidden = false
        }));
        fromVC?.present(alert, animated: true, completion: nil)
    }
    
    func alertView(_ alertView: FCAlertView, clickedButtonIndex index: Int, buttonTitle title: String) {
        
        switch index {
        case 0:
            responseBack?(.yes)
        case 1:
            responseBack?(.no)
        default: return
        }
    }
    
}



