//
//  MediaManager.swift
//  Wade7Student
//
//  Created by OSX on 10/10/17.
//  Copyright © 2017 OSX. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import Photos
import MobileCoreServices
import SystemConfiguration
import AVKit
import AVFoundation
import EZSwiftExtensions

class MediaManager : NSObject {
    
    
    func getVideoDuration(videoUrl: URL?) -> Float64 {
        if let url = videoUrl {
            let asset : AVURLAsset = AVURLAsset.init(url: url)
            let duration : CMTime = asset.duration
            return CMTimeGetSeconds(duration) 
        }
       return CMTimeGetSeconds(CMTime.init())
    }
    
    
    func getThumbnailFrom(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
            
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
}





