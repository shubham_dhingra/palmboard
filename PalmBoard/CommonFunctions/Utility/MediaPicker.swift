//
//  CaptureImage.swift
//  Wade7Student
//
//  Created by Sierra 4 on 13/06/17.
//  Copyright © 2017 Neetika-CB. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import Photos
import MobileCoreServices
import SystemConfiguration
import EZSwiftExtensions
import PDFKit


enum MediaType {
    
    case image
    case video
    case pdf
    case contact
}

class MediaPicker : NSObject {
    
    static var shared = MediaPicker()
    var fromVC: UIViewController?
    //    var complete:((_ image: UIImage?) -> Void)?
    let imagePicker = UIImagePickerController()
    var complete : ((_ type: MediaType , _ image: UIImage?, _ url : URL? ,  _ fileExt : String?)  -> Void )?
    
    func getCameraPermission() {
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized: presentPicker()
        case .denied, .restricted:
            ez.runThisInMainThread {
                self.showAlert(AlertConstants.Settings.get , AlertMsg.SettingsCameraApp.get)
            }
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted) in
                if(granted){ // Access has been granted ..do something
                    print("Access has been granted")
                    self.presentPicker()
                }
            })
        }
    }
    
    
    
    func getPhotosPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized: presentPicker()
        case .denied, .restricted:
            ez.runThisInMainThread {
                self.showAlert(AlertConstants.Settings.get, AlertMsg.SettingsGalleryApp.get)
            }
        case .notDetermined:
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    self.presentPicker()
                }
            })
        }
    }
    
    func showAlert(_ title : String? , _ message : String?){
        
        AlertsClass.shared.showAlertController(withTitle: /title, message: /message, buttonTitles: [AlertConstants.Settings.get,AlertConstants.Cancel.get]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                self.openSettings()
            default:
                return
            }
        }
    }
    
    func openSettings(){
        
        let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)! as URL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
    
    func presentPicker() {
        self.imagePicker.navigationController?.setNavigationBarHidden(false, animated: false)
        DispatchQueue.main.async { [unowned self] in
            self.fromVC?.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func captureMedia(from vc: UIViewController , captureOptions sources: [UIImagePickerController.SourceType], allowEditting crop: Bool, fromView sender: UIButton?, callBack: ((_ type: MediaType , _ image: UIImage?, _ url : URL? , _ fileExt : String?) -> Void)?) {
        
        self.fromVC = vc
        self.complete = callBack
        imagePicker.delegate = self
        imagePicker.allowsEditing = crop
        imagePicker.mediaTypes = [kUTTypeImage as String]
        if sources.count > 1 {
            UtilityFunctions.sharedInstance.showActionSheetWithImages(buttons: [AlertConstants.Camera.get, AlertConstants.Gallery.get , AlertConstants.Document.get], success: { (string) in
                if string == AlertConstants.Camera.get{
                    self.imagePicker.sourceType = .camera
                    self.getCameraPermission()
                }
                
                if string == AlertConstants.Gallery.get{
                    self.imagePicker.sourceType = .photoLibrary
                    self.getPhotosPermission()
                }
                
                if string == AlertConstants.Document.get{
                    self.openDocument()
                }
            })
        }
    }

    func openDocument() {
        let importMenu =  UIDocumentPickerViewController.init(documentTypes: ["public.pdf","public.item"], in: .import)
        
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        fromVC?.present(importMenu, animated: true, completion: nil)
    }
    
    
    func openActionSheet(with imagePicker: UIImagePickerController, sources: [UIImagePickerController.SourceType], sender: UIButton?) {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for source in sources {
            let action = UIAlertAction(title: source.name, style: .default, handler: { (action) in
                imagePicker.sourceType = source
                if (source == .camera) {
                    self.getCameraPermission()
                }
                else {
                    self.getPhotosPermission()
                }
            })
            if source == .camera {
                if cameraExists { actionSheet.addAction(action) }
            }
            else {
                actionSheet.addAction(action)
            }
        }
        let cancel = UIAlertAction(title: AlertConstants.Cancel.get, style: .cancel) { (action) in }
        actionSheet.addAction(cancel)
        if let btn = sender {
            actionSheet.popoverPresentationController?.barButtonItem = UIBarButtonItem(customView: btn)
            fromVC?.present(actionSheet, animated: true, completion: nil)
        }
    }
}

extension MediaPicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var cameraExists: Bool {
        let front = UIImagePickerController.isCameraDeviceAvailable(.front)
        let rear = UIImagePickerController.isCameraDeviceAvailable(.rear)
        return front || rear
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.navigationController?.setNavigationBarHidden(true, animated: false)
        
        guard let callBack = complete else {
            fromVC?.dismiss(animated: true, completion: nil)
            return
        }
        callBack(.image, nil, nil, nil)
        fromVC?.dismiss(animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.imagePicker.navigationController?.setNavigationBarHidden(true, animated: false)

        var videoUrl : URL? = nil
        var image: UIImage? = nil
        var fileExt : String? = nil

        if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            videoUrl = videoURL
            if let videoThumbnail = MediaManager().getThumbnailFrom(path: videoURL){
                image = videoThumbnail
            }
        }

        else if let edittedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = edittedImage
        }
        else if let fullImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            image = fullImage
        }
        
        if let assetPath = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            if (assetPath.absoluteString.hasSuffix("JPG")) {
                fileExt = "jpg"
            }
            else if (assetPath.absoluteString.hasSuffix("PNG")) {
                 fileExt = "png"
            }
            else if (assetPath.absoluteString.hasSuffix("GIF")) {
                 fileExt = "gif"
            }
            else {
                fileExt = nil
            }
        }
        
        guard let callBack = complete else {
            fromVC?.dismiss(animated: true, completion: nil)
            return
        }

        if videoUrl != nil {
            callBack(.video, image, videoUrl , fileExt)
        }else {
            callBack(.image, image, nil , fileExt)
        }

        fromVC?.dismiss(animated: true, completion: nil)
    }
}

extension MediaPicker: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    
    func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        Messages.shared.show(alert: .oops, message: "You have cancelled the request", type: .warning)
        //        isAttachmentAdded = true
          fromVC?.dismiss(animated: true, completion: nil)
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        debugPrint("document picker")
        fromVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        debugPrint("didpick document at called")
        var fileExt : String? = nil
        guard let callBack = complete else {
            fromVC?.dismiss(animated: true, completion: nil)
            return
        }
         callBack(.pdf, nil ,url, fileExt)
    }   
}
extension UIImagePickerController.SourceType {
    var name: String {
        switch self {
        case .camera:
            return "Camera"
        case .photoLibrary:
            return "Gallery"
        case .savedPhotosAlbum:
            return "Saved Photos Album"
        }
    }
}



