


import UIKit
import EZSwiftExtensions
import Photos


class MediaPickerBlock : NSObject , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    typealias onPicked = (UIImage, String) -> ()
    typealias onCanceled = () -> ()
    var pickedListner : onPicked?
    var canceledListner : onCanceled?
    
    static let shared = MediaPickerBlock()
    
    override init(){
        super.init()
        
    }
    
    deinit{
        
    }
    
    func pickerImage(pickedListner : @escaping onPicked , canceledListner : @escaping onCanceled){
        
        
        UtilityFunctions.sharedInstance.showActionSheetWithStringButtons(buttons: [AlertConstants.Camera.get  , AlertConstants.Gallery.get], success: {[unowned self] (str) in
            
            if str == AlertConstants.Camera.get{
                
                UtilityFunctions.sharedInstance.isCameraPermission(actionOkButton: { (isOk) in
                    if !isOk{
                        
                        ez.runThisInMainThread {
                           self.showAlert(AlertConstants.Settings.get, AlertMsg.SettingsCameraApp.get)
                        }
                        
                    }else {
                        self.pickedListner = pickedListner
                        self.canceledListner = canceledListner
                        self.showCameraOrGallery(type: str)
                    }
                    
                })
            }else {
                
                
                UtilityFunctions.sharedInstance.accessToPhotos(actionOkButton: { (isOk) in
                    
                    if !isOk{
                        ez.runThisInMainThread {
                            self.showAlert(AlertConstants.Settings.get, AlertMsg.SettingsGalleryApp.get)
                        }
                    }else {
                        self.pickedListner = pickedListner
                        self.canceledListner = canceledListner
                        
                        self.showCameraOrGallery(type: str)
                    }
                })
            }
        })
    }
    
   
   
    
    func showAlert(_ title : String? , _ message : String?){
        
        AlertsClass.shared.showAlertController(withTitle: /title, message: /message, buttonTitles: [AlertConstants.Settings.get,AlertConstants.Cancel.get]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                self.openSettings()
            default:
                return
            }
        }
    }
    
    func showCameraOrGallery(type : String){
        
        let picker : UIImagePickerController = UIImagePickerController()
        picker.sourceType = /type == AlertConstants.Camera.get ? UIImagePickerController.SourceType.camera : UIImagePickerController.SourceType.photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        ez.runThisInMainThread {
            ez.topMostVC?.present(picker, animated: true, completion: nil)
        }
        
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        if let listener = canceledListner{
            listener()
        }
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        var fileName : String?
        if let image : UIImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            //self.cropImage(image: image)
            
            if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                let assets = result.firstObject
                if let filename =  assets?.value(forKey: "filename") {
                    fileName = filename as? String
                }
            }
            
            if let listener = pickedListner{
                listener(image , /fileName)
            }
        }
        
    }
    
    
    func openSettings(){
        
        let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)! as URL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.openURL(url)
            }
            
        }
        
    }
}


