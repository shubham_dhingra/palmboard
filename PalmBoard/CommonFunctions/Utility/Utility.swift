  //
  //  Utility.swift
  //  ComposeMessageModule
  //
  //  Created by NupurMac on 16/05/18.
  //  Copyright © 2018 Shubham. All rights reserved.
  //

  
  import UIKit
  import EZSwiftExtensions
  import CoreLocation
  import NVActivityIndicatorView
  import EZSwiftExtensions
 
  enum DateType{
    case time
    case date
  }
  
  class Utility: NSObject , NVActivityIndicatorViewable {
    
    static let shared = Utility()
  
    
    override init() {
        super.init()
        
    }
    static func appDelegate() -> AppDelegate {
            return UIApplication.shared.delegate as! AppDelegate
    }
    
    //MARK: Loader
    func loader()  {
        Utility.appDelegate().window?.rootViewController?.startAnimating(nil, message: nil, messageFont: nil, type: .ballSpinFadeLoader, color: UIColor.white, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
    }
    
    func removeLoader()  {
        Utility.appDelegate().window?.rootViewController?.stopAnimating()
    }
    
    //MARK: JSON Converiosn to String
    
    func converyToJSONString (array : [Any]?) -> String{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: array ?? [], options: JSONSerialization.WritingOptions.prettyPrinted)
            
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                return JSONString
            }
            
        } catch {}
        return ""
    }
 
    func internetConnectionAlert() {
        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        ez.topMostVC?.present(alert, animated: true, completion: nil)
    }
    
    func generateRandomNumber(numDigits : Int) -> String{
        var result = ""
        repeat {
            // Create a string with a random number 0...9999
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while Set<Character>(result).count < 4
        return result
    }
    
    func setUpPlaceholderFont(placeholderText : String) -> NSAttributedString{
        var attributedString = NSAttributedString()
        if let font = UIFont(name: "Ubuntu-Italic", size: 17.0){
            let attributes = [
                NSAttributedString.Key.font : font
            ]
            attributedString = NSAttributedString(string: placeholderText,attributes: attributes)
            
        }
        return attributedString
    }
    
    func makeDictObject(_ schoolDTL : SchoolCodeTable) -> NSMutableDictionary {
        
        let dict = NSMutableDictionary()
        dict["schoolCode"] = schoolDTL.schCode
        dict["schoolName"] = schoolDTL.schName
        dict["schoolLogo"] = schoolDTL.schLogo
        dict["schoolCity"] = schoolDTL.schCity
        dict["eSmartGuardActive"] = schoolDTL.eSmartGuard
        dict["eCareActive"] = schoolDTL.eCare
        dict["countryCode"] = schoolDTL.countryCode
        dict["themeColor"] = schoolDTL.themColor
        dict["isCurrentSchool"] = schoolDTL.isCurrentSchool
        return dict
    }
    
    //Get Full Identity
    func getFullIdentity(object : Any?) -> String? {
        
        if let object = object as? Recipients , let userType = object.receiverType {
            switch userType {
            case 1:
                return "\(/object.name), Class - (\(/object.className))"
            case 2:
                return  "\(/object.name), Parent of \(/object.childName) (\(/object.className))"
            default:
                return "\(/object.name),  \(/object.designation)"
                
            }
        }
        return nil
     }
    
    
    //Functions to remove whitespacees from a string (inbetween and on boundary)
    func insert(seperator: String, afterEveryXChars: Int, intoString: String) -> String {
        var output = ""
        intoString.enumerated().forEach { index, c in
            if index % afterEveryXChars == 0 && index > 0 {
                output += seperator
            }
            output.append(c)
        }
        return output
    }
    
    
    func makeDictObject(name :String?,  mobileNo :String?) -> NSMutableDictionary {
        let dict = NSMutableDictionary()
        dict["PersonName"] = name
        dict["MobileNo"] = mobileNo
        return dict
    }
    
    
    
//    func makeUserDictArr(arr : [Any]?) -> [NSMutableDictionary] {
//        
//        var usersArr = [NSMutableDictionary]()
//        usersArr.removeAll()
//        
//        if let users = arr as? [MeetingUser] {
//            _ = users.map {(user) in
//               usersArr.append(makeDictObject(name: user.name, mobileNo: user.mobileNo))
//            }
//        }
//        return usersArr
//        }
//        
    func setUpTextViewPlaceholder(_ txtView : Any? , msg : String? = AlertMsg.typeMessage.get) -> UILabel {
        
        let placeholderLabel = UILabel()
        placeholderLabel.text = msg
        placeholderLabel.font = UIFont(name: msg == AlertMsg.typeMessage.get ? "Ubuntu-Italic" : "Ubuntu-Regular", size: msg == AlertMsg.typeMessage.get ? 17.0 : 14.0)
        placeholderLabel.sizeToFit()
        placeholderLabel.textColor = UIColor.flatGray
        if let txtView = txtView as? UITextView {
        placeholderLabel.frame.origin = CGPoint(x: 2, y: (txtView.font?.pointSize)! / 2)
        }
        return placeholderLabel
    }
    
    
    func setUpTextFieldPlacholderUI(_ txtView : Any? , placeholderText : String? , placeholderFont : UIFont , placeholderTextColor  : UIColor){
        if let txtView = txtView as? UITextField {
            txtView.placeholder = placeholderText
            txtView.placeHolderFont = placeholderFont
            //txtView.textColor = placeholderTextColor
        }
    }
    
    func getAttributedString(firstStr : String? , firstAttr : [NSAttributedString.Key : Any]? , secondStr : String? , secondAttr : [NSAttributedString.Key : Any]?) -> NSMutableAttributedString{
        
        let attributedString1 = NSMutableAttributedString(string: /firstStr, attributes:firstAttr)
        let attributedString2 = NSMutableAttributedString(string: /secondStr, attributes: secondAttr)
        
        attributedString1.append(attributedString2)
        
        return attributedString1
    }
    

 }


  extension UIViewController : NVActivityIndicatorViewable{
    
  }
  
  
  extension NSMutableDictionary
  {
    func toJson() -> String {
        do {
            let data = self
            
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) ?? ""
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            print(string)
            string = string.replacingOccurrences(of: "\\", with: "") as NSString
            print(string)
            //            string = string.replacingOccurrences(of: "\"", with: "") as NSString
//            string = string.replacingOccurrences(of: " ", with: "") as NSString
            print(string)
            return string as String
        }
        catch let error as NSError{
            print(error.description)
            return ""
        }
    }
  }
  

