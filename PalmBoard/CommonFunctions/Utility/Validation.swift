//
//  Validation.swift
//  e-Care Pro
//
//  Created by cbl24 on 15/02/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

extension String {
    
    enum FieldType : String{
        case password
        case name
        case relation
        case email
        case newpassword
        case mobileNo
        case selectCard
        case IdentityProof
        case purposeofmeeting
        case whomtomeet
        case expectedDate
        case expectedTime
        case otherPurpose
        
        func value() -> String? {
            
            switch self {
                
            case .password :  return "Password"
            case .newpassword :  return "New Password"
            case .name : return "Name"
            case .email : return "Email"
            case .mobileNo : return "Mobile Number"
            case .selectCard : return "Select Card"
            case .IdentityProof : return "Identity Proof"
            case .purposeofmeeting : return "Purpose of meeting"
            case .whomtomeet : return "Whom To meet"
            case .expectedDate : return "Expected Date"
            case .expectedTime : return "Expected Time"
            case .otherPurpose : return "Purpose"
            case .relation : return "Relation"
                
            }
        }
        
        
    }
    
    enum Status : String {
        
        case empty
        case allSpaces
        case valid
        case inValid
        case allZeros
        case hasSpecialCharacter
        case notANumber
        case emptyCountrCode
        case mobileNumberLength
        case pwd
        case pinCode
        case zip
        case currency
        case misMatchPassword
        case image
        case alertUpperAndLowerCase
        case alertpwdContainSpecialCharactersOrNumber
        case NotChooseData
        case NotSelectData
        case NotUploadData
        case hasSpecialCharactersIgnoreSpaces
        
        func value() -> String? {
            switch self {
                
            case .empty : return "Please enter "
            case .allSpaces : return  "Enter the "
            case .valid : return nil
            case .inValid : return "Please enter a valid "
            case .allZeros : return "Please enter a valid "
            case .hasSpecialCharacter , .hasSpecialCharactersIgnoreSpaces : return " can only contain A-z, a-z characters only"
            case .notANumber  : return " must be a number "
            case .pwd : return "Password length must be 8 characters long"
            case .pinCode : return "PinCode length should be less than 6 characters"
            case .misMatchPassword : return "Password does not Match"
            case .image : return  "Please upload an Image"
            case .alertUpperAndLowerCase : return "Password include both lower and upper case"
            case .NotChooseData : return "Please choose "
            case .NotSelectData : return "Please select "
            case .NotUploadData : return "Please upload "
            case .alertpwdContainSpecialCharactersOrNumber  : return "Password must contain at least one number or symbol"
            case .mobileNumberLength : return "Please enter a valid mobile number of length 7-14 Characters"
            default : return "Empty"
            }
        }
        
        func message(type : FieldType) -> String? {
            
            switch self {
            case .hasSpecialCharacter, .hasSpecialCharactersIgnoreSpaces : return /type.value() + /self.value()
            case .valid: return nil
            case .emptyCountrCode: return /self.value()
            case .pwd , .alertUpperAndLowerCase , .alertpwdContainSpecialCharactersOrNumber : return self.value()
            case .image : return self.value()
            case .mobileNumberLength : return /self.value()
            case .pinCode , .zip : return self.value()
            case .notANumber : return /type.value() + /self.value()
            default: return /self.value() + /type.value()
            }
        }
    }
    
    
    func validateChangePassword(old : String? , new : String? , confirm : String?) -> Bool{
        
        if old == ""{
            Messages.shared.show(alert: .oops, message: AlertMsg.alertOldOPasswordEmpty.get, type: .warning)
            return false
        }
        else if !isValid(type: .password, info: new){
            return false
        }
        else if new == old{
            Messages.shared.show(alert: .oops, message: AlertMsg.alertSimilarOldPassword.get, type: .warning)
            return false
        }
        else if new != confirm{
            Messages.shared.show(alert: .oops, message: AlertMsg.alertEnterConfirmPassowrdEmpty.get, type: .warning)
            return false
        }
        else{
            return true
        }
    }
    
    func validateRegisterForm(name : String? , email : String?, mobileNo : String? , identityProof : String? , identityProofImage : UIImage? ,  sno : Int) -> Bool {
        
        if !isValid(type : .name , info : name) || !isValid(type : .email , info : email) || !isValid(type : .mobileNo , info : mobileNo){
            return false
        }
        
        if sno != 0 {
            return true
        }
        
        if !isValidateDataChoose(type : .selectCard , info : identityProof) || !isValidImage(uploadImage: identityProofImage, placeHolderImage: nil, actionType: .upload, type: .IdentityProof) {
            return false
        }
        
        return true
    }
   
    private func isValid(type : FieldType , info: String?) -> Bool {
        guard let validStatus = info?.handleStatus(fieldType : type) else {
            return true
        }
        
        let errorMessage = validStatus
      //  print(errorMessage)
        Messages.shared.show(alert: .oops, message: errorMessage, type: .warning)
        //AlertsClass.shared.showAlert(with: errorMessage)
        return false
    }
    
    func handleStatus(fieldType : FieldType) -> String? {
        
        switch fieldType {
        case .password , .newpassword :
            return  isValid(password: 8, max: 15).message(type: fieldType)
            
        case .name , .relation :
            return isValidName.message(type:fieldType)
            
        case .email :
            return isValidEmail.message(type:fieldType)
            
        case .mobileNo :
            return isValidPhoneNumber.message(type: fieldType)
            
        default:
            return  isValidInformation.message(type: fieldType)
        }
    }
    
    
    
    
    
    func isValidateDataChoose(type : FieldType, info : String?) -> Bool{
        if info == type.value() {
            Messages.shared.show(alert: .oops, message: /Status.NotChooseData.message(type: type), type: .warning)
          //  print(/Status.NotChooseData.message(type: type))
            return false
        }
        return true
    }
    
    func validateUser(username : String? , password : String?) -> Bool {
        if username?.trimmed().count ==  0 {
            Messages.shared.show(alert: .oops, message: AlertMsg.pleaseEnterUserName.get , type: .warning)
            return false
        }
        
        if password?.trimmed().count == 0 {
            Messages.shared.show(alert: .oops, message: AlertMsg.pleaseEnterPassword.get , type: .warning)
            return false
        }
        return true
    }
    
    
    var isNumber : Bool {
        if let _ = NumberFormatter().number(from: self) {
            return true
        }
        return false
    }
    
    
    var hasSpecialCharcters : Bool {
        return rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil || rangeOfCharacter(from: CharacterSet.letters.inverted) != nil
    }
    
    var hasSpecialCharactersIgnoreSpaces : Bool {
        let set = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ ")
        return rangeOfCharacter(from: set) != nil || rangeOfCharacter(from: set) != nil
    }
    
    var hasSpecialCharactersWithNumber : Bool {
        return rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
    }
    
    var isEveryCharcterZero : Bool{
        var count = 0
        self.forEach {
            if $0 == "0"{
                count += 1
            }
        }
        if count == self.count{
            return true
        }else{
            return false
        }
    }
    
    
    
    public var length: Int {
        return self.count
    }
    
    public var isEmail: Bool {
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    
    public var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
            return trimmed.isEmpty
        }
    }
    
    func isValid(password min: Int , max: Int) -> Status {
        
        if length < 0 { return .empty }
        if isBlank  { return .allSpaces }
        if !containUpperCaseAndLowerCase {return .alertUpperAndLowerCase}
        if !containAtleastOneNumberAndSpecialCharacters {return .alertpwdContainSpecialCharactersOrNumber}
        if self.count >= min && self.count <= max{
            return .valid
        }
        return .pwd
    }
    
    
    var containUpperCaseAndLowerCase : Bool {
        
        let nonUpperCase = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
        let letters = self.components(separatedBy: nonUpperCase)
        let strUpper: String = letters.joined()
        
        let smallLetterRegEx  = ".*[a-z]+.*"
        let samlltest = NSPredicate(format:"SELF MATCHES %@", smallLetterRegEx)
        let smallresult = samlltest.evaluate(with: self)
        return (strUpper.count >= 1) && smallresult
        
    }
    
    var containAtleastOneNumberAndSpecialCharacters : Bool {
        
        let numberRegEx  = ".*[0-9]+.*"
        let numbertest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = numbertest.evaluate(with: self)
        var isSpecial :Bool = false
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.count)) != nil {
           // print("could not handle special characters")
            isSpecial = true
        }else{
            isSpecial = false
        }
        return  (numberresult || isSpecial)
        
    }
    
    var isValidEmail : Status {
        
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if isEmail { return .valid }
        return .inValid
        
    }
    
    
    var isValidInformation : Status {
        
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        return .valid
        
    }
    
    var isValidExtension : Status {
        if hasSpecialCharcters { return .hasSpecialCharactersIgnoreSpaces }
        if self.count < 6  && isNumber { return .valid }
        if self.count == 0 { return .valid }
        return .inValid
    }
    
    var isValidPhoneNumber : Status {
        
        if length <= 0 { return .empty }
        if isBlank { return .allSpaces }
        if isEveryCharcterZero { return .allZeros }
        if !isNumber() { return .inValid }
        if self.count >= 7 && self.count <= 13 { return .valid
        }
        else{
            return .mobileNumberLength
        }
    }
    
    var isValidName : Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        //        if hasSpecialCharactersIgnoreSpaces { return .hasSpecialCharacter }
        return .valid
    }
    
    
    //Validation for City Country District
    var isValidWord : Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if hasSpecialCharcters{ return .inValid}
        return .valid
    }
    
    var isValidMiddleName : Status {
        if hasSpecialCharcters { return .hasSpecialCharacter }
        return .valid
    }
    
    func isValidCardNumber(length max:Int ) -> Status {
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if self.count >= 16 && self.count <= max{
            return .valid
        }
        return .inValid
    }
    
    var isValidCVV : Status {
        if hasSpecialCharcters { return .hasSpecialCharacter }
        if isEveryCharcterZero { return .allZeros }
        if isNumber{
            if self.characters.count >= 3 && self.characters.count <= 4{
                return .valid
            }else{ return .inValid }
        }else { return .notANumber }
    }
    
    var isValidZipCode : Status {
        
        if length == 0 { return .empty }
        if isEveryCharcterZero { return .allZeros }
        if isBlank { return .allSpaces }
        if !isNumber{ return .inValid }
        if hasSpecialCharactersWithNumber {return.zip}
        if length > 6 {return .pinCode}
        
        return .valid
        
    }
    
    var isValidAmount :  Status {
        
        if length < 0 { return .empty }
        if isBlank { return .allSpaces }
        if !isNumber{ return .notANumber }
        return .valid
        
    }
    
    func isValidImage(uploadImage : UIImage? , placeHolderImage : UIImage? , actionType : ActionType ,type : FieldType) -> Bool{
        
        //if uplaodImage is nil
        guard let uploadImage = uploadImage else {
            showAlertMessage(actionType : actionType , type : type)
            return false
        }
        
        if let placHolder = placeHolderImage {
            if uploadImage == placHolder{
                showAlertMessage(actionType : actionType , type : type)
                return false
            }
        }
        return true
    }
    
    func validateSendMessage(_ subjectLine : String? , _ message : String? , _ sendType : String?) -> Bool{
        
        if subjectLine?.trimmed().count == 0 {
           Messages.shared.show(alert: .oops, message: AlertMsg.SubjectLineEmpty.get, type: .warning)
            return false
        }
        if sendType ==  "1" {
        if message?.trimmed().count == 0 {
            Messages.shared.show(alert: .oops, message: AlertMsg.MessageEmpty.get, type: .warning)
            return false
        }
        }
        return true
    }
    
    func showAlertMessage(actionType : ActionType , type : FieldType){
        
        var msg : String?
        
        switch actionType {
        case .upload:
            msg = Status.NotUploadData.message(type: type)
            
        case .select:
            msg = Status.NotSelectData.message(type: type)
            
        case .choose:
            msg = Status.NotChooseData.message(type: type)
        }
        Messages.shared.show(alert: .oops, message : /msg, type: .warning)
    }
    
    enum ActionType : String {
        case upload
        case select
        case choose
    }
    
}

