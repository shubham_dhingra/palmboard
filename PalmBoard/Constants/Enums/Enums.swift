
//  Enums.swift
//  ComposeMessageModule
//  Created by NupurMac on 17/05/18.
//  Copyright © 2018 ShubhamMac. All rights reserved.

import Foundation
import UIKit

enum EntityType {
    case username
    case password
}


enum AttendanceType {
    case Subject
    case Class
}

enum Storyboard : String{
    
    case ComposeMessage = "ComposeMessage"
    case Reports        = "Reports"
    case Module         = "Module"
    case Main           = "Main"
    
    func getBoard() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
}

enum MsgConstants : String {
    
    case subjLine = "Subject Line"
    case browseFlyerMsg = "Browse Flyer Message"
    
    var get : String {
        return self.rawValue
    }
}
enum AuthenticationState {
    case loggedin, loggedout
}
struct  NOTI {
    static let ADD_USER = "ADD_USER"
    static let UPDATE_SCHOOL_DATA = "UPDATE_SCHOOL_DATA"
    static let CHANGE_SCHOOL = "CHANGE_SCHOOL"
    static let VERIFICATION_SUCCESS = "VERIFICATION_SUCCESS"
    static let UPDATE_MODULE_LIST = "ON_COME_BACK_AT_HOME"
}

struct APP_CONSTANTS{
    static let KEY = "jg98ATQO6XksZUmA5Ni7yw=="
}

struct TeamCareAPP_CONSTANTS{
    static let KEY = "Matchrecognition_549689450c$"
}
struct ExecutiveAPP_CONSTANTS {
    static let KEY = "PUpj1vw1vwMiFoA6h+eMBg=="
}

enum AlertConstants : String {
    
    case Settings        = "Settings"
    case Attention       = "Attention"
    case Camera          = "Camera"
    case Gallery         = "Gallery"
    case Ok              = "OK"
    case Delete          = "Delete"
    case Update          = "Update"
    case  Skip           = "Skip"
    case Next            = "Next"
    case Previous        = "Previous"
    case  Reapply        = "REAPPLY"
    case  Status         = "Status"
    case  Alert          = "Alert"
    case  Done           = "Done"
    case  Cancel         = "Cancel"
    case SelectDate      = "Select Date"
    case remove          = "Remove"
    case logout          = "Log out"
    case welcome         = "Welcome"
    case SelectTime      = "Select Time"
    case Discard         = "Discard"
    case Congratulations = "Congratulations"
    case eSmartGuard    = "eSmartGuard"
    case Install         = "Install"
    case Day             = "Day"
    case Submit          = "Submit"
    case Document       = "Document"
    case Attachment     = "Attachment"
    case No             = "No"
    case Yes            = "Yes"
    case Confirmation   = "Confirmation"
    case Subject        = "Subject"
     
    
    
    var get : String {
        return self.rawValue
    }
}

enum MediaImages  : String {
    case camera = "Camera"
    case gallery = "Gallery"
    case pdf = "Document"
    
    func getImage() -> UIImage?{
        switch  self {
        case .camera:
            return UIImage(named : "ic_camera")!
        case .gallery:
            return UIImage(named : "ic_photo")!
        case .pdf :
            return UIImage(named : "ic_document")!
       }
    }
}

enum AlertMsg : String {
    
    case SettingsCameraApp              = "Camera permissions need to be allowed to capture an image using camera . Please allow Camera Permissions from settings."
    case SettingsGalleryApp             = "Gallery permissions need to be allowed to pick an image from gallery . Please allow Photo Permissions from settings."
    case atLeastOneContact              = "Please select at least one contact to continue."
    case atLeastOneClass                = "Please select at least one class to continue."
    case atLeastOneRecipient            = "Please select at least one recipient to continue"
    case invalidDurationSelect          = "You have selected an incorrect time interval"
    case leaveAppproveSuccess           = "Leave approved successfully"
    case leaveRejectSuccess             = "Leave reject successfully"
    case alertOldOPasswordEmpty         = "Please enter current password"
    case alertEnterCurrentPasswordEmpty = "Please enter new password"
    case alertEnterConfirmPassowrdEmpty = "Password does not match"
    case alertPwdContainNumberAndSymbol = "Password must contain at least one number or symbol"
    case alertIncludeUpperAndLowerCase  = "Password must contain both upper and lower case"
    case alertSimilarOldPassword        = "New password and current password match"
    case changePasswordSuccess          = "Password changed successfully"
    case isAuthenticate                 = "Please check your username or password"
    case pleaseEnterUserName            = "Please enter username"
    case invalidUsername                = "Please enter registered username"
    case invalidPassword                = "Invalid Password"
    case pleaseEnterPassword            = "Please enter password"
    case userAlreadyExist               = "User is already Exists"
    case LogoutSure                     = "Are you sure you want to log out?"
    case removeUser                     = "Do you want to remove "
    case fromAccountList                = " from the added account?"
    case deleteConfirm                  = "Users are associated with this school"
    case deleteSure                     = "Are you sure you want to Delete?"
    case deleteSchoolSure               = "If you remove this school, all the associated user will also be removed. Are you sure you want to remove "
    case otpDescription                 = "We have sent OTP on your mobile number registered with us ending with xxxxxx"
    case emptyOtp                       = "Please enter OTP"
    case inValidOtp                     = "Otp not matched"
    case bookingLimitExceed             = "Please select another date concerned person is not available on that day"
    case  proposedDate                  = " Proposed Date"
    case time                           = "time"
    case toMeetThe                      = " to meet the "
    case subimmtted                     = "is submitted to school Admin for approval."
    case deleteParentConfirm            = "Are you sure you want to delete the guardian?"
    case parentDeleteSuccess            = "Guardian deleted successfully"
    case meetingUserEmptyField          = "Please fill all the people details"
    case EscortAlreadyExist             = "Escort Already Added"
    case activateEscortConfirm          = "Are you sure you want to activate this Escort?"
    case deactivateEscortConfirm        = "Are you sure you want to deactivate this Escort?"
    case activateEscortSuccess          = "Escort Activate Successfully"
    case deactivateEscortSuccess        = "Escort Deactivate Successfully"
    case incompleteSchoolCode           = "Please enter the School Code"
    case schoolNotRegistered            = "The school code is not registered with us. Kindly check again"
    case schoolAlreadyExist             = "Entered School code already exists, Please Select to login"
    case AlertDiscardMessage            = "Are you sure want to discard the current changes?"
    case eSmartGuardInstallation        = "eSmartGuard application seems not installed in your device. Please click on install to download from App Store."
    case  enterISBNValue                = "Please enter ISBN Number"
    case scanISBN                       = "Scan ISBN Bar code"
    case scanBarCode                    = "Scan Bar Code"
    case attendanceSavedSuccess         = "Attendance saved successfully"
    case attendanceLockedMsg1           = "Today's Attendance of Class"
    case attendanceLockedMsg2           = "has been locked. For any change please contact school admin."
    case InboxEmpty                     = "Inbox is Empty!"
    case SentEmpty                      = "Sentbox is Empty!"
    case MessageEmpty                   = "Please enter message"
    case SubjectLineEmpty               = "Please enter subject Line"
    case PleaseEnterText                = "Please enter text"
    case SelectClass                    = "Please select class"
    case NoClassAssigned                = "You are not allowed to mark attendance because there is no class assigned to you. Please contact school admin."
     case NoWeeklyPlanAssigned         = "You are not allowed to view the weekly plan because there is no class assigned to you. Please contact school admin."
    case ContactMobRequiredDetails      = "Guardian mobile number of all the absentees is not in the database So you are not allowed to send sms."
    case CreatePlan                     = "Create new plan"
    case ExisitingPlan                  = "View existing plan"
    case typeMessage                    = "Type Your Message"
    case enterDescription               = "Enter Description:"
    case skipAlertWeekPlan1             = "Please note that No plan will be recorded for "
    case skipAlertWeekPlan2             = "\nAre you sure you want to skip?"
    case weeklyPlan1                    = "Are you sure you want to "
    case weeklyPlan2                    = " this plan?"
    case subjectWiseAttendance          = "Subject Wise Attendance"
    case classWiseAttendance            = "Class Wise Attendance"
    case TodayAttendance                = "Today's Attendance"
    case SurveySubmitSuccess            = "Your response has been recorded \n Thanks for your response."
    case AlreadySubmitSurvey            = "Thanks for your response. Your response has already been recorded."
    case SurveyClosed                   = "Survey Closed"
    case mandatoryQuesAttempt           = "Please attempt all the mandatory questions."
    case AtleastOneQuestions            = "Please attempt atleast one questions."
    case NoSubjectAssigned              = "No Subject Found"
    
    var get : String {
        return self.rawValue
    }
}


enum CellIdentifiers : String {
    case LeaveReasonCell          = "LeaveReasonCell"
    case LeaveDetailCell          = "LeaveDetailCell"
    case DropDownCell             = "DropDownCell"
    case DropDownSubjectCell      = "DropDownSubjectCell"

    case FilterDropDownCell       = "FilterDropDownCell"
    case AddAccountCell           = "AddAccountCell"
    case SchoolCell               = "SchoolCell"
    case StudentAttendanceCell    = "StudentAttendanceCell"
    
    case smsType                  = "smsType"
    case smsReport                = "studentCell"
    case attendanceReport         = "cellAttendanceReport"
    case attendanceType           = "attendanceType"
    case Attendance1              = "Attendance1"
    case Attendance2              = "Attendance2"
    case Attendance3              = "Attendance3"
    case Attendance4              = "Attendance4"
    case Attendance5              = "Attendance5"
    case Attendance6              = "Attendance6"
    case searchCollectioncell     = "attReportSearchCell"
    case conversationCell         = "cellConversation"
    case conversationSearchCell   = "cellConversationSearch"
    case conversationDetailsCell  = "convDetails"
    case conversationListCell     = "cellConversationList"
    case conversationMenuCell     = "cellConversationMenu"
    case MainSearchCollection     = "listSearch"
    //SearchMenuTableViewCell
    case SearchCollectionCell     = "cellSearchMenu"
    case NewUserCell              = "NewUserCell"
    case ExistingUserCell         = "ExistingUserCell"
    case MeetingUserCell          = "MeetingUserCell"
    case MessageCell              = "MessageCell"
    
    case ReceiverImageCell        = "ReceiverImageCell"
    case ReceiverTextCell         = "ReceiverTextCell"
    case ReceiverAudioCell        = "ReceiverAudioCell"
    case SenderImageCell          = "SenderImageCell"
    case SenderTextCell           = "SenderTextCell"
    case SenderAudioCell          = "SenderAudioCell"
    case TimeStampCell            = "TimeStampCell"
    case GuideFrameCell           = "GuideFrameCell"
    case EscortUserCell           = "EscortUserCell"
    case RecipientCell            = "RecipientCell"
    case UsersTypeCell            = "UsersTypeCell"
    case FindSchoolCell           = "FindSchoolCell"
    case TokenCell                = "TokenCell"
    case recentUpdateCell         = "RecentUpdateCell"
    case recentAttachedCell       = "RecentAttachedCell"
    case recentGalleryCell        = "RecentGalleryCell"
    case recentGalleryCollectionCell = "RecentGalleryCollectionCell"
    case recentMsgTableViewCell   = "RecentMsgTableViewCell"
    case SettingsCell             = "SettingsCell"
    case DayPlanCell              = "DayPlanCell"
    case finalDesignCell          = "finalDesignCell"
    case PlanListCell             = "PlanListCell"
    case PlanViewCell             = "PlanViewCell"
    case NoDataCell               = "NoDataCell"
    case PlanAttachmentCell       = "PlanAttachmentCell"
    case WeeklyPlanStudentCell    = "WeeklyPlanStudentCell"
    case EditDayPlanCell          = "EditDayPlanCell"
    case SurveyListCell           = "SurveyListCell"
    case SurveyQuestionSectionHeader = "SurveyQuestionSectionHeader"
    case SurveyQuestionCell       = "SurveyQuestionCell"
    case LeaveTypeCell            = "LeaveTypeCell"
    case SelectHalfDayCell        = "SelectHalfDayCell"
    case HalfDayCell              = "HalfDayCell"
    case ExecutiveCell            = "cellExecutiveAttendance"
    case cellSyllabus            = "cellSyllabus"
    case cellStaffSyllabus       = "cellStaffSyllabusCollection"
    case cellThoughts            = "cellThoughts"
    case CellQuestionNormal      = "MyQuestionNormal"
    case CellMyQuestionImage     = "MyQuestionImage"
    case CellMyAnswer            = "myAnswer"
    case CellAnswer              = "cellAnswer"
    case CellHeader              =  "cellHeader"
    var get : String {
        return self.rawValue
    }
}

enum States : String {
    case yes
    case no
}

enum Action : String {
    case Next
    case Previous
    case Skip
    case None
    case Delete
    case Add
    case View
    case Create
    case Review
    case Edit
}

enum Module : String{
    case Appointment
    case Escort
    case Attendance
    case WeeklyPlan
    case MarksEntry
    case BusLocator
    case GradeBook
    case OnlineAssessmentMarks
    case ReportCard
    
    var id : String {
        return self.rawValue
    }
    
    
    func getTitle() -> String {
        switch self {
        case .BusLocator:
            return "Bus Location"
        case .MarksEntry:
            return "Marks Entry"
        case .OnlineAssessmentMarks:
            return "Online Assessment Marks"
        case .ReportCard:
            return "Report Card"
        default:
            return ""
        }
    }
    
}


class GetMonth {
    
    class func getMonthName(index : Int) -> String {
        
        switch index {
        
        case 1:
             return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
            
        default:
            return ""
        }
    }
    
    class func getMonthIndex(monthName : String) -> Int {
        
        switch monthName {
            
        case "January":
            return 1
        case "February":
            return 2
        case "March":
            return 3
        case "April":
            return 4
        case "May":
            return 5
        case "June":
            return 6
        case "July":
            return 7
        case "August":
            return 8
        case "September":
            return 9
        case "October":
            return 10
        case "November":
            return 11
        case "December":
            return 12
        default:
            return 0
        }
    }
}

enum MessageType : String {
    
    case Sent
    case Inbox
    case sms
    case app
    
    var id : String {
        switch self {
        case .Sent , .Inbox :
            return ""
        case .sms:
            return "Compose SMS"
        case .app:
            return "Compose Message"
        }
    }
    
    var placeholder : String {
        switch self {
        case .Sent , .Inbox :
            return ""
        case .sms:
            return "Please select sms type"
        case .app:
            return "Subject Line"
        }
    }
}

enum LeaveStatus : String {
    case Pending
    case Approved
    case Rejected
    
    var get : String{
        return self.rawValue
    }
    
    var id : String {
        switch self {
        case .Pending:
            return "0"
        case .Approved:
            return "1"
        case .Rejected:
            return "2"
            
        }
    }
    
    var color : UIColor {
        switch self {
        case .Pending:
            return UIColor(rgb: 0xFFBB00)
        case .Approved:
            return UIColor(rgb: 0x48C700)
        case .Rejected:
            return UIColor(rgb: 0xFB4E4E)
        }
    }
}


enum FilterType : String {
    case Name = "Search By Name"
    case Date = "Search By Leave Date"
    case Class = "Search By Class"
    
    var get : String{
        return self.rawValue
    }
    
    var placholderText : String {
        switch self {
        case .Name:
            return "Search By Name"
        case .Class:
            return "Select Class"
        case .Date:
            return "Select Date"
            
        }
    }
    
    
    var id : String {
        switch self {
        case .Name:
            return "1"
        case .Class:
            return "2"
        case .Date:
            return "3"
            
        }
    }
    
    var Image : UIImage? {
        switch self {
        case .Class:
            return UIImage(named : "search_by_class")
        case .Name:
            return #imageLiteral(resourceName: "Search Name")
        case .Date:
            return #imageLiteral(resourceName: "Leave Date")
        }
        
    }
}


enum Users : String {
    
    case Student
    case Staff
    case Parent
    case Cordinator
    
    var get : String {
        return self.rawValue
    }
    var userId : Int {
        switch self {
        case .Student:
            return 1
        case .Parent:
            return 2
        case .Staff , .Cordinator:
            return 3
        }
    }
}

enum AppointmentUsersType : String {
    
    case Father
    case Mother
    case Guardian
    
    
    var get : String {
        return self.rawValue
    }
}
enum Operation {
    case add
    case remove
}
enum FailureMessage : String {
    case ObjectNotFound = "Sorry!  Face not found"
    case BadPictureQuality = "Oops! Image quality is poor"
    case BadExposure = "Bad exposure"
    case BadSharpness = "Oops! Bad sharpness"
    case BadContrast = "Oops! Bad contrast"
    case badInterConnection = "Bad network connection. Try again."
}
enum HeaderTitle : String {
    
    case Students = "Students"
    case Statistical = "Statistical"
    case Teachers = "Teachers"
    
    
    func getTitle() -> String{
        
        switch self {
        case .Students:
            return "Students Report(s)"
        case .Statistical:
            return "Statistical Report(s)"
        case .Teachers:
            return "Teachers Report(s)"
        }
        
    }

}


enum IOSMessages : String{
    case SelectClass = "Select Class"
    case SelectSubject = "Select Subject"
    case SelectHalfDay = "Select halfday"
    case SelectAttendance = "Select Staff type"

    var get : String {
        return self.rawValue
    }
}
enum DateFormats : String {
    case Date1WithMSec = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    case Date2 = "yyyy-MM-dd'T'HH:mm:ss"
    
    var get : String {
        return self.rawValue
    }
}
