//  SchoolCodeTable+CoreDataProperties.swift
//  Created by ShubhamMac on 06/06/18.

import Foundation
import CoreData


extension SchoolCodeTable {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolCodeTable> {
        return NSFetchRequest<SchoolCodeTable>(entityName: "SchoolCodeTable")
    }

    @NSManaged public var addMobUrl: String?
    @NSManaged public var contactEmail: String?
    @NSManaged public var countryCode: Int32
    @NSManaged public var feeReportUrl: String?
    @NSManaged public var noteTime: String?
    @NSManaged public var paymentUrl: String?
    @NSManaged public var marksEntryURL: String?

    @NSManaged public var schAdd1: String?
    @NSManaged public var schAdd2: String?
    @NSManaged public var schCity: String?
    @NSManaged public var schCode: String?
    @NSManaged public var schLogo: String?
    @NSManaged public var schName: String?
    @NSManaged public var sMS_Prvd_ID: Int32
    @NSManaged public var supportEmail: String?
    @NSManaged public var supportPhone: String?
    @NSManaged public var themColor: String?
    @NSManaged public var assessmentMarksURL: String?

    @NSManaged public var webSite: String?
   // @NSManaged public var wordDate: String?
    @NSManaged public var smsService: Int32
    @NSManaged public var schCodes: NSSet?
    @NSManaged public var isCurrentSchool : Int32
    @NSManaged public var eCare : Int32
    @NSManaged public var eSmartGuard : Int32

    

}

// MARK: Generated accessors for schCodes
extension SchoolCodeTable {

    @objc(addSchCodesObject:)
    @NSManaged public func addToSchCodes(_ value: UserTable)

    @objc(removeSchCodesObject:)
    @NSManaged public func removeFromSchCodes(_ value: UserTable)

    @objc(addSchCodes:)
    @NSManaged public func addToSchCodes(_ values: NSSet)

    @objc(removeSchCodes:)
    @NSManaged public func removeFromSchCodes(_ values: NSSet)

}
