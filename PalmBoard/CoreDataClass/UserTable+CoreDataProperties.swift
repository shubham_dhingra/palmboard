//  UserTable+CoreDataProperties.swift
//  Created by ShubhamMac on 06/06/18.

import Foundation
import CoreData

extension UserTable {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserTable> {
        return NSFetchRequest<UserTable>(entityName: "UserTable")
    }
    
    @NSManaged public var categoryId: Int32
    @NSManaged public var id: String?
    @NSManaged public var isActive: Int32
    @NSManaged public var classID : Int32
    @NSManaged public var isAuthenticate: Bool
    @NSManaged public var name: String?
    @NSManaged public var vehicleNumber: String?
    @NSManaged public var searchEnabled: Bool

    @NSManaged public var password: String?
    @NSManaged public var photoUrl: String?
    @NSManaged public var role: String?
    @NSManaged public var schCode: String?
    @NSManaged public var updateDateTime: String?
    @NSManaged public var userClass: String?
    @NSManaged public var userID: Int32
    @NSManaged public var username: String?
    @NSManaged public var childName : String?
    @NSManaged public var childPhoto : String?
    @NSManaged public var childClass : String?
    @NSManaged public var sessionStart: String?
    @NSManaged public var sessionEnd:   String?
    @NSManaged public var session :   String?
    @NSManaged public var motherName: String?
    @NSManaged public var emailID:    String?
    @NSManaged public var contactMob: String?
    @NSManaged public var userType:   Int32
    @NSManaged public var schCodes:   SchoolCodeTable?
    @NSManaged public var firstName:  String?

}
