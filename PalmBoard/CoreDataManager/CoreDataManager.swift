//  CoreDataManager.swift
//  PalmBoard
//  Created by Nupur Sharma on 15/10/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import CoreData

class CoreDataManager: NSObject  {
    static let sharedInstance = CoreDataManager()
    
    private override init()
    {
        //print("Init Method")
        // super.init()
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "PalmBoard")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext ()
    {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - InsertUpdate
    func InsertUpdateUserTable(result: [String : Any]?, senderCode1: String)
    {}

}
