//
//  CollectionViewDataSource.swift
//  SafeCity
//  Created by Aseem 13 on 29/10/15.
//  Copyright © 2015 Taran. All rights reserved.
import UIKit

typealias ScrollViewScrolled = (UIScrollView) -> ()
typealias WillDisplay = (_ indexPath : IndexPath) -> ()

class CollectionViewDataSource: NSObject  {
    
    var items : Array<Any>?
    var cellIdentifier : String?
    var headerIdentifier : String?
    var collectionView  : UICollectionView?
    var cellHeight : CGFloat = 0.0
    var cellWidth : CGFloat = 0.0
    
    var scrollViewListener : ScrollViewScrolled?
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var willDisplay : WillDisplay?
    
    init (items : Array<Any>?  , collectionView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat)  {
        
        self.collectionView = collectionView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth = cellWidth
        self.cellHeight = cellHeight
    }
    override init() {
        super.init()
    }
    
}

extension CollectionViewDataSource : UICollectionViewDelegate , UICollectionViewDataSource, UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier ,
                                                      for: indexPath) as UICollectionViewCell
        
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if cellIdentifier == CellIdentifiers.recentGalleryCollectionCell.get || cellIdentifier == CellIdentifiers.PlanAttachmentCell.get  {
            return 8.0
        }
        return 0.0
    }
 
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if cellIdentifier == CellIdentifiers.recentGalleryCollectionCell.get || cellIdentifier == CellIdentifiers.PlanAttachmentCell.get {
            return 8.0
        }
        return 0.0
    }
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let block = self.aRowSelectedListener, let item: Any = self.items?[(indexPath).row]{
            block(indexPath ,item)
        } 
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let block = willDisplay {
            block(indexPath)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
}
extension CollectionViewDataSource : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if CellIdentifiers.PlanAttachmentCell.get == cellIdentifier {
            return CGSize(width: cellWidth + 32.0, height: cellHeight)
        }
        else if CellIdentifiers.HalfDayCell.get == cellIdentifier {
            return CGSize(width: cellWidth , height: cellHeight)
        }
        else if cellIdentifier == R.reuseIdentifier.surveyUserCollectionCell.identifier || cellIdentifier == R.reuseIdentifier.classCell.identifier {
            return CGSize(width: cellWidth , height: cellHeight)
        }
        else if CellIdentifiers.cellStaffSyllabus.get == cellIdentifier {
            return CGSize(width: cellWidth , height: cellHeight)
        }
        else {
            return CGSize(width: cellWidth - 16.0 , height: cellHeight)
        }
    }
}
