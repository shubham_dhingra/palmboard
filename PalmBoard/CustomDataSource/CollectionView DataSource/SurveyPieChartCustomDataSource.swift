//
//  SurveyPieChartCustomDataSource.swift
//  PalmBoard
//
//  Created by Shubham on 13/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit

class  SurveyPieChartCustomDataSource: NSObject  {
    
    var items : Array<Any>?
    var cellIdentifier : String?
    var headerIdentifier : String?
    var collectionView  : UICollectionView?
    var cellHeight : CGFloat = 0.0
    var cellWidth : CGFloat = 0.0
    
    var scrollViewListener : ScrollViewScrolled?
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var willDisplay : WillDisplay?
    var modal : SurveyPieChartModal?
    var drawPieChartFirstTime : Bool?
    
    init (items : Array<Any>?  , collectionView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat , modal : SurveyPieChartModal? , drawPieChartFirstTime : Bool?)  {
        
        self.collectionView = collectionView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth = cellWidth
        self.cellHeight = cellHeight
        self.modal = modal
        self.drawPieChartFirstTime = drawPieChartFirstTime
    }
    override init() {
        super.init()
    }
}

extension  SurveyPieChartCustomDataSource : UICollectionViewDelegate , UICollectionViewDataSource, UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        guard let identifier = cellIdentifier else{
//            fatalError("Cell identifier not provided")
//        }
        
        if let value = items?[indexPath.row] as? SurveyUserDTL {
            
            if /value.name == "All" {
                self.collectionView?.register(R.nib.allReportsCell)
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.allReportsCell.identifier ,
                                                                    for: indexPath) as? AllReportsCell else {
                return UICollectionViewCell()
                }
                getOptions { (labelArr, valuesArr , optionColorsArr) in
                    cell.modal = self.modal
                    cell.months = labelArr
                    cell.valuesArr = valuesArr
                    cell.colorsArr = optionColorsArr
                    cell.drawPieChartFirstTime = self.drawPieChartFirstTime
                    cell.updateBarGraph()
                    cell.updatePieChart()
                    
                }
               return cell
            }
                
            else {
                self.collectionView?.register(R.nib.userReportsCell)
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.userReportsCell.identifier ,
                                                              for: indexPath) as? UserReportsCell else {
                    return UICollectionViewCell()
                }
                 cell.modal = modal
                 cell.value = value
                
                getDetailsUserWise(userType: value.usertype) { (labelArr, totalArr, maleArr, femaleArr , totalUsersArr) in
                    cell.maleArr = maleArr
                    cell.femaleArr = femaleArr
                    cell.totalResponseArr = totalArr
                    cell.totalUsersArr = totalUsersArr
                    cell.labelsArr = labelArr
                    cell.drawPieChartFirstTime = self.drawPieChartFirstTime
                    cell.updatePieChart()
                    cell.reloadTable()
                    cell.updateBarGraph()
                  }
                
                return cell
            }
            
           
        }
        
       return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let block = self.aRowSelectedListener, let item: Any = self.items?[(indexPath).row]{
            block(indexPath ,item)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let block = willDisplay {
            block(indexPath)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
}
extension  SurveyPieChartCustomDataSource : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: cellWidth , height: cellHeight)
    }

}

extension SurveyPieChartCustomDataSource {
    
    func getOptions(Completion :  @escaping ([String],[Double],[UIColor]) -> ()) {
        guard let modal = self.modal ,let options = modal.options else {
            Completion([],[],[])
            return
        }
        
        //in case of ALL
        let optionIndexAllotment = options.enumerated().map {(index , value) -> (String,Double,UIColor) in
            return  ("Option \((index + 1).toString)" , /value.surveyUserDTL?.first?.total?.toDouble ,value.assignColor ?? UIColor.ColorApp)
        }
        
        print(optionIndexAllotment)
        Completion(optionIndexAllotment.map({
        return $0.0
        }),optionIndexAllotment.map({
            return $0.1
        }),optionIndexAllotment.map({
            return $0.2
        }))
    }
    
}

extension SurveyPieChartCustomDataSource {
    
    func getDetailsUserWise(userType : Int? , Completion :  @escaping ([String],[Int],[Int],[Int],[Int]) -> ()) {
        guard let modal = self.modal ,let options = modal.options else {
            Completion([],[],[],[],[])
            return
        }
       
        //get surveydetails of a particular User
        
        var surveyUserDTLArr = [SurveyUserDTL]()
        var totalResponseArr = [Int]()
        for (_ , option) in options.enumerated() {
            for (_ , survey) in (option.surveyUserDTL ?? []).enumerated() {
                if survey.usertype == userType {
                    surveyUserDTLArr.append(survey)
                    totalResponseArr.append(/option.response)
                }
            }
        }
        
        
//        //in case of ALL
        let surveyOptionAllotment = surveyUserDTLArr.enumerated().map {(arg) -> (String,Int,Int,Int) in
            let (index, value) = arg
            return  ("Option \((index + 1).toString)" , /value.total , /value.male , /value.female)
        }

        Completion(surveyOptionAllotment.map({
            return $0.0
        }),surveyOptionAllotment.map({
            return $0.1
        }),surveyOptionAllotment.map({
            return $0.2
        }), surveyOptionAllotment.map({
            return $0.3
        }),totalResponseArr)
    }
}

