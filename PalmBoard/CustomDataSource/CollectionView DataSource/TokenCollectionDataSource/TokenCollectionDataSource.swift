//  CollectionViewDataSource.swift
//  SafeCity
//  Created by Aseem 13 on 29/10/15.
//  Copyright © 2015 Taran. All rights reserved.
import UIKit

class TokenCollectionDataSource : NSObject  {
    
    var items            : Array<Any>?
    var cellIdentifier   : String?
    var headerIdentifier : String?
    var collectionView   : UICollectionView?
    var cellHeight       : CGFloat = 0.0
    var cellWidth        : CGFloat = 0.0
    
    var scrollViewListener   : ScrollViewScrolled?
    var configureCellBlock   : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var willDisplay          : WillDisplay?
    
    init (items : Array<Any>?  , collectionView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat)  {
        
        self.collectionView = collectionView
        self.collectionView?.isScrollEnabled = true
        self.items            = items
        self.cellIdentifier   = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth        = cellWidth
        self.cellHeight       = cellHeight
        
    }
    override init() {
        super.init()
    }
}
extension TokenCollectionDataSource : UICollectionViewDelegate , UICollectionViewDataSource, UIScrollViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier ,
                                                      for: indexPath) as UICollectionViewCell
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    /*
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
     return 0.0
     }
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
     return 0.0
     }
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.items?.count ?? 0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let block = self.aRowSelectedListener, let item: Any = self.items?[(indexPath).row]{
            block(indexPath ,item)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let block = willDisplay {
            block(indexPath)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
}
extension TokenCollectionDataSource : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let item = self.items?[indexPath.row] as? MyClasses {
            return CGSize(width: item.ClassName?.width(withConstraintedHeight: 70.0, font:  R.font.ubuntuMedium(size: 15.0)!) ?? 0.0, height: 50.0)
        }
        else if let item =  self.items?[indexPath.row] as? Contacts {
                return CGSize(width: item.name?.width(withConstraintedHeight: 70.0 , font: R.font.ubuntuMedium(size: 15.0)!) ?? 0.0 , height: 50.0)
        }
//        let w = self.tagNames[indexPath.item].width(withConstraintedHeight: 70.0, font: textFont)
        return CGSize(width: 50.0, height: 40.0)
    }
}
extension String {
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width) + 97.0
    }
}
