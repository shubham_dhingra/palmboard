//  TableViewDataSource.swift
//  SafeCity
//  Created by Aseem 13 on 29/09/15.
//  Copyright (c) 2015 Taran. All rights reserved.
import UIKit
import EZSwiftExtensions

class BoardTableDataSource : NSObject  {
    
    var items :  Array<Any>?{
        didSet {
            self.count = items?.count
        }
    }
    
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var headerHeight : CGFloat? = 50.0
    var count : Int?
    var awsURL : String?
    var intUserType : Int?
    var arrayStaffRPT    = [[String : Any]]()
    var arraySubReports  = [[String : Any]]()
    var fromVc : UIViewController?
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , intUserType : Int?, arrayStaffRPT : [[String : Any]]? ,
          arraySubReports : [[String : Any]]? , fromVc : UIViewController? , awsURL : String?
    ) {
        
        self.tableView          = tableView
        self.items              = items
        self.arraySubReports    = arraySubReports ?? [[:]]
        self.arrayStaffRPT      = arrayStaffRPT ?? [[:]]
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        self.fromVc             = fromVc
        self.awsURL             = awsURL
        if cellIdentifier       != CellIdentifiers.MeetingUserCell.get {
            self.count               = self.items?.count
            self.intUserType         = intUserType
        }
    }
    override init() {
        super.init()
    }
}

extension BoardTableDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        if self.intUserType == 3 && indexPath.section == 0 && indexPath.row == 0 {
            guard let boardReportCell = tableView.dequeueReusableCell(withIdentifier: "BoardReportsTableViewCell",for: indexPath) as? BoardReportsTableViewCell else {
                print("Cell is not load")
                return UITableViewCell()
            }
            boardReportCell.selectionStyle = UITableViewCell.SelectionStyle.none
            boardReportCell.fromVc = fromVc
            boardReportCell.setUpStaffModule(self.arrayStaffRPT, self.arraySubReports)
            return boardReportCell
        }
        else {
            
            if let item =  self.items?[indexPath.row] as? Update , let modID = item.mdlID {
                
                switch modID {

                case 2:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.recentGalleryCell.get , for: indexPath) as? RecentGalleryCell else {return UITableViewCell() }
                    cell.awsURL = self.awsURL
                    cell.modal = item
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell
                    
                case 16:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.recentMsgTableViewCell.get , for: indexPath) as? RecentMsgTableViewCell else { return UITableViewCell() }
                    cell.modal = item
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell

                    
                default:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.recentAttachedCell.get , for: indexPath) as? RecentAttachedCell else {return UITableViewCell() }
                    cell.modal = item
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    return cell
                }
        }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if intUserType == 3 && section == 0{
            return 1
        }
        return /self.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if intUserType == 3 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 && intUserType == 3 {
            return nil
        }
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 && intUserType == 3 {
            return 0.0
        }
        else {
            return headerHeight ?? 0.0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
}
