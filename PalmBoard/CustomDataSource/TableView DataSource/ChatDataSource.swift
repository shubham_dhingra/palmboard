//
//  ChatDataSource.swift
//  e-Care Pro
//
//  Created by Shubham on 06/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation

class ChatDataSource : NSObject  {
    
    var items : Array<Any>?
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var headerHeight : CGFloat?
    var userID : Int?
    var userType : Int?
    var count : Int?
    var isStopAll : Bool = false
    
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView?, userID : Int? , userType : Int? , isStopAll : Bool?) {
        
        self.tableView = tableView
        self.items = items
        self.userID = userID
        self.userType = userType
        self.tableViewRowHeight = height
        self.isStopAll = /isStopAll
    }
    
    override init() {
        super.init()
    }
}

extension ChatDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let item = self.items?[indexPath.row] as? ChatMsgs , let msgType = item.msgType , let senderID = item.senderDTL?.senderID ,let senderType = item.senderDTL?.senderType, let userType = self.userType , let userID = self.userID {
            
            switch msgType {
              
                
            //image
            case 2:
                
                if senderID != userID {
                    return loadReceiverImageCell(item: item, indexPath: indexPath)
                }
                else {
                    if senderType == userType {
                        return loadSenderImageCell(item: item, indexPath: indexPath)
                    }
                    else {
                        return loadReceiverImageCell(item: item, indexPath: indexPath)
                    }
                }
              
                
            //audio
            case 3:
                if senderID != userID {
                    return loadReceiverAudioCell(item: item, indexPath: indexPath)
                }
                else {
                    if senderType == userType {
                        return loadSenderAudioCell(item: item, indexPath: indexPath)
                    }
                    else {
                       return loadReceiverAudioCell(item: item, indexPath: indexPath)
                    }
                }
                
                
            //text and sms
            default:
                if senderID != userID {
                    return loadReceiverTextCell(item: item, indexPath: indexPath)
                }
                else {
                    if senderType == userType {
                        return loadSenderTextCell(item: item, indexPath: indexPath)
                    }
                    else {
                        return loadReceiverTextCell(item: item, indexPath: indexPath)
                    }
                }
            }
        }
            
        else {
            return UITableViewCell()
        }
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return /self.items?.count
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if cellIdentifier == CellIdentifiers.AddAccountCell.get {
            if let user = items?[indexPath.row] as? UserTable {
                return user.userType == 2 ? 100.0 : 95.0
            }
        }
        return self.tableViewRowHeight
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    
}

extension ChatDataSource {
    
    func loadReceiverImageCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.ReceiverImageCell.get , for: indexPath) as? ReceiverImageCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.modal = item
        return cell
    }
    
    func loadSenderImageCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.SenderImageCell.get , for: indexPath) as? SenderImageCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.modal = item
        return cell
    }
    
     func loadReceiverAudioCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.ReceiverAudioCell.get , for:    indexPath) as? ReceiverAudioCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.isStopAll = /isStopAll
        cell.modal = item
        return cell
    }
    
   
    func loadSenderAudioCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.SenderAudioCell.get , for:    indexPath) as? SenderAudioCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.isStopAll = /isStopAll
        cell.modal = item
        return cell
    }
    
     func loadReceiverTextCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell =  self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.ReceiverTextCell.get , for: indexPath) as? ReceiverTextCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.modal = item
        return cell
    }
    
    func loadSenderTextCell(item : ChatMsgs , indexPath : IndexPath)  -> UITableViewCell{
        guard  let cell = self.tableView?.dequeueReusableCell(withIdentifier: CellIdentifiers.SenderTextCell.get , for: indexPath) as? SenderTextCell else {return UITableViewCell() }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.index = indexPath.row
        cell.modal = item
        return cell
    }
    
    
}
