

//
//  WeeklyPlanDataSource.swift
//  PalmBoard
//
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

class FinalWeekPlanDataSource  : NSObject  {
    
    var items :  Array<Any>?{
        didSet {
            self.count = items?.count
        }
    }
    
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var headerHeight : CGFloat? = 67.0
    var currentDayIndex : Int = 0
    var fromVc : UIViewController?
    var count : Int?
    var action : Action?
    var userID : String?
    var sectionArr = [Int]()
    var userRole : Users?
    var userType : Int?
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0, fromVC : UIViewController? , action : Action? , userrole : Users? , userID : String? , userType : Int?) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        self.count              = self.items?.count
        self.currentDayIndex    = /currentDayIndex
        self.fromVc = fromVC
        self.action =  action
        self.userRole = userrole
        self.userID = userID
        self.userType = userType
        for(index,_) in (self.items ?? []).enumerated() {
            self.sectionArr.append(index)
        }
    }
    
    override init() {
        super.init()
    }
}

extension FinalWeekPlanDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier  else {
            fatalError("Cell identifier not provided")
        }
        
        if let item = self.items?[indexPath.section] as? DayPlanList {
            let isSkip = item.isSkip
            let workTypeModal = item.WorkPlans[indexPath.row]
            
            if !isSkip {
                
                if (workTypeModal.Description?.trimmed().count == 0 || workTypeModal.Description == nil) &&  (workTypeModal.Topic?.trimmed().count == 0 || workTypeModal.Topic == nil) && workTypeModal.Method == 0 && (workTypeModal.Attachments.count == 0) {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NoDataCell.get , for: indexPath) as? NoDataCell else {return UITableViewCell()}
                    cell.isSkip = isSkip
                    cell.index = indexPath.row
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    cell.btnStatus?.isHidden = true
                    cell.delegate = self
                    cell.section = indexPath.section
                    if userRole == .Cordinator {
                        cell.btnEdit?.isHidden = true
                        cell.btnStatus?.isHidden = true
                    }
                    if userRole == .Staff {
                        cell.btnEdit?.isHidden = false
                        cell.btnStatus?.isHidden = action == .Create
                    }
                    cell.modal = workTypeModal
                    return cell
                }
                else {
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as? FinalDesignTableViewCell else {return UITableViewCell()}
                    cell.index = indexPath.row
                    cell.delegate = self
                    if userRole == .Cordinator {
                        cell.btnStatus?.isHidden = true
                    }
                    else {
                        cell.btnStatus?.isHidden = action == .Create
                    }
                    cell.userRole = userRole
                    cell.action    = action
                    cell.section = indexPath.section
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    cell.btnEdit?.isHidden = action != .Create
                    if userRole == .Cordinator && self.userType == 3 {
                        cell.approvalView?.isHidden = userRole != .Cordinator
                        cell.stackApproval?.isHidden = userRole != .Cordinator
                        cell.btnReject?.isHidden = userRole != .Cordinator
                        cell.btnAccept?.isHidden = userRole != .Cordinator
                    }
                    
                    //student and parent case
                    if  (userRole == .Cordinator && self.userType == 1) || (userRole == .Cordinator && self.userType == 2) || (userRole == .Staff && self.userType == 3){
                        cell.approvalView?.isHidden = true
                        cell.stackApproval?.isHidden = true
                        cell.btnReject?.isHidden = true
                        cell.btnAccept?.isHidden = true
                    }
                    
                    cell.modal = workTypeModal
                    return cell
                }
            }
            else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.NoDataCell.get , for: indexPath) as? NoDataCell else {return UITableViewCell()}
                cell.isSkip = isSkip
                cell.delegate = self
                cell.section = indexPath.section
                cell.btnStatus?.isHidden = true
                cell.btnEdit?.isHidden = action != .View
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                cell.index = indexPath.row
                cell.btnEdit?.isHidden = true
                if userRole == .Cordinator {
                    cell.btnStatus?.isHidden = true
                }
                if userRole == .Staff {
                    cell.btnStatus?.isHidden = action == .Create
                }
                cell.modal = workTypeModal
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sectionArr.contains(section){
            
            //case of teacher and cordintor
            if userType == 3 {
                if /(self.items?[section] as? DayPlanList)?.isSkip {
                    return 1
                }
                else {
                    return 2
                }
            }
            else {
                //case of parent and student
                return /(self.items?[section] as? DayPlanList)?.WorkPlans.count
            }
        }
        else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return /items?.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let weekPlanHeaderView =  PlanViewSectionHeader.instanceFromNib() as? PlanViewSectionHeader else {
            return UIView()
        }
        
        if /currentDayIndex < 0 || /currentDayIndex > /self.items?.count {
            return UIView()
        }
        
        weekPlanHeaderView.lblDay?.text = "Day \(section + 1)"
        if action == .Review{
            if let date = (self.items?[section] as? DayPlanList)?.PlanDate {
                weekPlanHeaderView.lblDate?.text = "\(date.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "dd MMM, yyyy")) (\(date.convertStringIntoDate().getDayOnParticularDate()))"
            }
        }
        else if action != .View {
            if let date = (self.items?[section] as? DayPlanList)?.PlanDateForModal {
                weekPlanHeaderView.lblDate?.text = "\(/date.dateToString(formatType: "dd MMM, yyyy")) (\(date.getDayOnParticularDate()))"
            }
        }
        else {
            if let date = (self.items?[section] as? DayPlanList)?.PlanDate {
                weekPlanHeaderView.lblDate?.text = "\(date.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "dd MMM, yyyy")) (\(date.convertStringIntoDate().getDayOnParticularDate()))"
            }
        }
        weekPlanHeaderView.containerView?.isUserInteractionEnabled = true
        weekPlanHeaderView.delegate = self
        weekPlanHeaderView.btnEdit?.tag = section
        weekPlanHeaderView.btnEdit?.isHidden = (action == .View || action == .Review)
        weekPlanHeaderView.containerView?.tag = section
        weekPlanHeaderView.containerView?.addTapGesture(target: self, action:  #selector(self.openCloseHeader(_:)))
        return weekPlanHeaderView
    }
    
    @objc func openCloseHeader(_ sender : UITapGestureRecognizer){
        
        guard let tag = sender.view?.tag else {return}
        if sectionArr.contains(tag) {
            if let arrIndex = sectionArr.indexes(of: tag).first {
                sectionArr.remove(at: arrIndex)
            }
        }
        else {
            sectionArr.append(tag)
        }
        let sectionIndex = IndexSet(integer: tag)
        
        tableView?.reloadSections(sectionIndex, with: .none)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
}


extension FinalWeekPlanDataSource : EditPlanDelegate {
    
    func editPlan(tag : Int , section : Int?) {
        
        let moduleStoryBoard = Storyboard.Module.getBoard()
        
        guard let vc = moduleStoryBoard.instantiateViewController(withIdentifier: "WeekPlanListViewController") as? WeekPlanListViewController else {
            return
        }
        vc.userRole = userRole
        
        if let currentVc = fromVc as? FinalDesignPlanViewController {
            
            var copyWeekPlanModal = WeekPlanModal.init()
            
            //for update the new create plan before submit
            if currentVc.weekPlanModal?.WPID == nil {
                currentVc.weekPlanModal = (fromVc as? FinalDesignPlanViewController)?.weekPlanModal
                vc.currentDayIndex  = /section
                vc.weekPlanModal = currentVc.weekPlanModal
                vc.action = .Edit
                (ez.topMostVC?.children.last as? UINavigationController)?.pushViewController(vc, animated: false)
            }
            else {
                
                if let weekPlanModal = currentVc.weekPlanModal {
                    copyWeekPlanModal = weekPlanModal
                    if /section < weekPlanModal.DayPlans.count{
                        copyWeekPlanModal.DayPlans = [weekPlanModal.DayPlans[/section]]
                    }
                    
                    //remove the other weekly plan which is not selected from the selected day
                    if copyWeekPlanModal.DayPlans.count == 1 {
                        print("Selected day plan at index \(/section)")
                        for (workPlanIndex,_) in copyWeekPlanModal.DayPlans[0].WorkPlans.enumerated() {
                            if workPlanIndex != tag{
                                copyWeekPlanModal.DayPlans[0].WorkPlans.remove(at: workPlanIndex)
                            }
                        }
                    }
                }
                
                guard let editVc = moduleStoryBoard.instantiateViewController(withIdentifier: "EditSubmitPlanViewController") as? EditSubmitPlanViewController else {
                    return
                }
                editVc.currentDayIndex = 0
                editVc.dayNo = section
                editVc.weekPlanModal = copyWeekPlanModal
                (ez.topMostVC?.children.last as? UINavigationController)?.pushViewController(editVc, animated: true)
            }
        }
    }
}



extension FinalWeekPlanDataSource : ApprovalAndEditDelegate {
    func senderTag(tag: Int, strURL: String) {
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "DownloadWeeklyPlanViewController") as? DownloadWeeklyPlanViewController{
            vc.strURLFile = strURL
            (ez.topMostVC?.children.last as? UINavigationController)?.pushViewController(vc, animated: false)
        }
    }
    
    
    func approveRejectPlan(section : Int? , planId : String? , tag: Int, Status: Int) {
        guard let planId = planId else {
            return
        }
        self.showApprovalRejectAlert(section : section , planId : planId , tag: tag, Status: Status)
    }
    
    func showApprovalRejectAlert(section : Int? , planId : String? , tag: Int, Status: Int) {
        
        let msg = "\(AlertMsg.weeklyPlan1.get)\(/Status == 1 ? "Approve" : "Reject")\(AlertMsg.weeklyPlan2.get)"
        AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: msg, buttonTitles: [AlertConstants.No.get , AlertConstants.Yes.get]) { (Tag) in
            switch Tag {
            case .no:
                self.hitAPI(section : section , planId : planId , tag : tag, Status : Status)
                return
            default:
                return
            }
        }
    }
    
    func hitAPI(section : Int? , planId : String? , tag: Int, Status: Int) {
        APIManager.shared.request(with: ECareEndPoint.weeklyPlanTakeAction(PlIn: planId, Status: Status.description, UserID: self.userID)) { (response) in
            switch response {
            case .success(let responseVal):
                if (responseVal as? String) != nil {
                    Messages.shared.show(alert: .success, message: "Plan Update Successfully", type: .success)
                    (self.fromVc as? FinalDesignPlanViewController)?.dayWisePlanArr?[/section].WorkPlans[tag].Status = Status
                    (self.fromVc as? FinalDesignPlanViewController)?.reloadTable()
                }
            case .failure(_):
                Messages.shared.show(alert: .oops, message: "Something went wrong", type: .warning)
            }
            
        }
    }
    
}


