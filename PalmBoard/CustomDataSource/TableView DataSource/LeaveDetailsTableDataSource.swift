//
//  LeaveDetailsTableDataSource.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import Foundation
import UIKit

class LeaveDetailsTableDataSource : NSObject  {
    
    var items : Array<Any>?
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var scrollViewDidEndDragging : ScrollViewScrolled?
    var headerHeight : CGFloat?
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String?) {
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
    }
    override init() {
        super.init()
    }
}
extension LeaveDetailsTableDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return headerHeight ?? 0.0
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewDidEndDragging {
            block(scrollView)
        }
    }
    
    
}
