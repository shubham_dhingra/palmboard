//
//  QuestionTableDataSource.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 08/05/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import EZSwiftExtensions


class QuestionTableDataSource: NSObject  {
    
    var items : Array<Any>?{
        didSet {
            self.count = items?.count
        }
    }
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    var estimatedHeight : CGFloat = 100.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var scrollViewDidEndDragging : ScrollViewScrolled?
    var headerHeight : CGFloat?
    var count : Int?
    var topMostVC : UIViewController? {
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , estimatedHeight: CGFloat? = 100.0) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        self.count              = /items?.count
        self.estimatedHeight = /estimatedHeight
    }
    
    override init() {
        super.init()
    }
    
}
extension QuestionTableDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        guard let identifier = cellIdentifier else{
        //            fatalError("Cell identifier not provided")
        //        }
        if let item = self.items?[indexPath.row] as? ListQuestion , let queType = item.qType {
            
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: queType == 1 ? CellIdentifiers.CellQuestionNormal.get :CellIdentifiers.CellMyQuestionImage.get , for: indexPath) as UITableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
                block(cell , item , indexPath as IndexPath?)
            }
            // if let topVC = ez.topMostVC {
            // if topVC is MyQuestionsViewController {
            if queType == 1 {
                //(cell as? MyQuestionNormalTableViewCell)?.normalDelegate = ez.topMostVC as? MyQuestionsViewController
                (cell as? MyQuestionNormalTableViewCell)?.delegate       = self.topMostVC as? MyQuestionsViewController
                (cell as? MyQuestionNormalTableViewCell)?.index          = /indexPath.row
                (cell as? MyQuestionNormalTableViewCell)?.modal          = item
            }
            else {
                // (cell as? MyQuestionImageTableViewCell)?.imageMyDelegate = ez.topMostVC as? MyQuestionsViewController
                (cell as? MyQuestionImageTableViewCell)?.index           = /indexPath.row
                (cell as? MyQuestionImageTableViewCell)?.indexpath       = indexPath
                (cell as? MyQuestionImageTableViewCell)?.tblQuestion     = self.tableView
                (cell as? MyQuestionImageTableViewCell)?.delegate        = self.topMostVC as? MyQuestionsViewController
                (cell as? MyQuestionImageTableViewCell)?.modal           = item
            }
            // }
            // }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /self.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return estimatedHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewDidEndDragging {
            block(scrollView)
        }
    }
}
