

//
//  SurveyQuestionTableDataSource.swift
//  PalmBoard
//
//  Created by Shubham on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

class SurveyQuestionTableDataSource   : NSObject  {
    
    var items :  Array<Any>?{
        didSet {
            self.count = items?.count
        }
    }
    
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var headerHeight : CGFloat? = 67.0
    var currentDayIndex : Int = 0
    var fromVc : UIViewController?
    var count : Int?
    var userID : String?
    var userType : Int?
    var surveyResult : Bool = false
    var forCheckResponses : Bool = false
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , isSurveyResult : Bool? = false , forCheckResponses : Bool? = false , fromVc : UIViewController?) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        self.count = /items?.count
        self.surveyResult = /isSurveyResult
        self.forCheckResponses = /forCheckResponses
        self.fromVc = fromVc
    }
    
    override init() {
        super.init()
    }
}

extension SurveyQuestionTableDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.section]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if forCheckResponses {
            return 0
        }
        if let item = self.items?[section] as? SurveyQuestions {
            return /item.options?.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return /items?.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let surveyQuestionHeader =  SurveyQuestionSectionHeader.instanceFromNib() as? SurveyQuestionSectionHeader else{
            return UIView()
        }
        surveyQuestionHeader.forCheckResponses = self.forCheckResponses
        surveyQuestionHeader.configure()
        if forCheckResponses {
            surveyQuestionHeader.btnOpenReport?.tag = section
            surveyQuestionHeader.btnOpenReport?.layer.cornerRadius = 4.0
             surveyQuestionHeader.delegate = self
        }
        if let item = self.items?[section] as? SurveyQuestions , let isAnsMandatory = item.isAnsMandatory {
            
            if !surveyResult {
                if isAnsMandatory {
                    surveyQuestionHeader.lblQuestion?.attributedText = Utility.shared.getAttributedString(firstStr: item.question, firstAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!], secondStr: " *", secondAttr: [NSAttributedString.Key.font: R.font.ubuntuMedium(size: 16.0)!, NSAttributedString.Key.foregroundColor : UIColor.markAbsentColor])
                }
                else {
                    surveyQuestionHeader.lblQuestion?.text = item.question
                }
                
            }
            else {
                surveyQuestionHeader.lblQuestion?.text = item.question
            }
            surveyQuestionHeader.lblTotalRes?.text = surveyResult ? "Total Response : \(/item.response)" : nil
            surveyQuestionHeader.lblTotalRes?.isHidden = !surveyResult
        
        }
        return surveyQuestionHeader
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if let item = self.items?[section] as? SurveyQuestions {
            if forCheckResponses {
                 return (heightForView(text: /item.question) * 2 + 50.0)
            }
            else {
            if surveyResult {
                 return (heightForView(text: /item.question) * 2 + 20.0)
            }
            else {
                return heightForView(text: /item.question) * 2 + 8.0
            }
            }
         }
        return 36.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
}

extension SurveyQuestionTableDataSource {
    
    func heightForView(text : String) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 16 , y: 0, width : /self.tableView?.frame.width - 40, height : CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = R.font.ubuntuRegular(size: 18.0)
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
}

extension SurveyQuestionTableDataSource : OpenReportDelegate {
    func openReportDelegate(_ tag: Int) {
        guard let vc = R.storyboard.module.surveyResDataViewController() else {
            return
        }
        vc.questions = (self.items?[tag] as? SurveyQuestions)
        vc.surveyID = (self.fromVc as? SurveyResultViewController)?.survey?.surID
        self.fromVc?.pushVC(vc)
    }
}


