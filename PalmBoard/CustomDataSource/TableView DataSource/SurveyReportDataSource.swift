//
//  SurveyReportDataSource.swift
//  PalmBoard
//
//  Created by Shubham on 14/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import EZSwiftExtensions

class SurveyReportDataSource : NSObject  {
    
    var items : Array<Any>?{
        didSet {
            if cellIdentifier != CellIdentifiers.MeetingUserCell.get {
                self.count = items?.count
            }
        }
    }
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    var estimatedHeight : CGFloat = 100.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var scrollViewDidEndDragging : ScrollViewScrolled?
    var headerHeight : CGFloat? = 52.0
    var count : Int?
    var selectIndex : Int = 0
    // var headerHeight : CGFloat? = 50.0
    
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , estimatedHeight: CGFloat? = 100.0 , selectIndex :Int? = 0) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        if cellIdentifier      != CellIdentifiers.MeetingUserCell.get {
            self.count              = self.items?.count
        }
        self.estimatedHeight = /estimatedHeight
        self.selectIndex = /selectIndex
    }
    
    override init() {
        super.init()
    }
}
extension SurveyReportDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /self.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return estimatedHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
        
//        let surveySectionHeader =   SurveyUserSectionHeader.instanceFromNib() ?? UIView()
//        surveySectionHeader.frame.size.height = 52.0
//        surveySectionHeader.frame.size.width  = UIScreen.main.bounds.width
//        (surveySectionHeader as? SurveyUserSectionHeader)?.usersArr = ["All","Students","Parents","Staff"]
//        (surveySectionHeader as? SurveyUserSectionHeader)?.selectIndex = selectIndex
//        (surveySectionHeader as? SurveyUserSectionHeader)?.reloadUserCollection()
//        return surveySectionHeader
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 52.0
    }
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewDidEndDragging {
            block(scrollView)
        }
    }
}
