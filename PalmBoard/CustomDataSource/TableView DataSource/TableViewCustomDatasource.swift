//  TableViewDataSource.swift
//  SafeCity
//  Created by Aseem 13 on 29/09/15.
//  Copyright (c) 2015 Taran. All rights reserved.

import UIKit
import EZSwiftExtensions

typealias  ListCellConfigureBlock = (_ cell : Any , _ item : Any? , _ indexpath: IndexPath?) -> ()
typealias  DidSelectedRow = (_ indexPath : IndexPath , _ cell : Any) -> ()
typealias ViewForHeaderInSection = (_ section : Int) -> UIView?

class TableViewDataSource: NSObject  {
    
    var items : Array<Any>?{
        didSet {
            if cellIdentifier != CellIdentifiers.MeetingUserCell.get {
                self.count = items?.count
            }
        }
    }
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    var estimatedHeight : CGFloat = 100.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var scrollViewDidEndDragging : ScrollViewScrolled?
    var headerHeight : CGFloat?
    var count : Int?
    // var headerHeight : CGFloat? = 50.0
    
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , estimatedHeight: CGFloat? = 100.0) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        if cellIdentifier      != CellIdentifiers.MeetingUserCell.get {
            self.count              = self.items?.count
        }
        self.estimatedHeight = /estimatedHeight
    }
    
    override init() {
        super.init()
    }
}
extension TableViewDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if let block = self.configureCellBlock , let item: Any = self.items?[indexPath.row]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /self.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if cellIdentifier == CellIdentifiers.DayPlanCell.get {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if cellIdentifier == CellIdentifiers.AddAccountCell.get {
            if let user = items?[indexPath.row] as? UserTable {
                return user.userType == 2 ? 100.0 : 95.0
            }
        }
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return estimatedHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let block = viewforHeaderInSection else { return nil }
        return block(section)
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if cellIdentifier == CellIdentifiers.DayPlanCell.get {
            return 67.0
        }
        
        return headerHeight ?? 0.0
    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        guard let block = viewforHeaderInSection else { return nil }
    //        return block(section)
    //    }
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //
    //        if cellIdentifier == CellIdentifiers.DayPlanCell.get {
    //            return 67.0
    //        }
    //
    //        return headerHeight ?? 0.0
    //    }
    //
    /*******/
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewDidEndDragging {
            block(scrollView)
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let block = scrollViewDidEndDragging {
            block(scrollView)
        }
    }
}
