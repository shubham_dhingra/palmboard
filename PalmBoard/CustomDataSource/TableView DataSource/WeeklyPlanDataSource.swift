

//
//  WeeklyPlanDataSource.swift
//  PalmBoard
//
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation

import UIKit
import EZSwiftExtensions

class WeeklyPlanDataSource  : NSObject  {
    
    var items :  Array<Any>?{
        didSet {
            self.count = items?.count
        }
    }
    
    var cellIdentifier : String?
    var tableView  : UITableView?
    var tableViewRowHeight : CGFloat = 44.0
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    var viewforHeaderInSection : ViewForHeaderInSection?
    var scrollViewListener : ScrollViewScrolled?
    var scrollViewBeginDeaccerlate : ScrollViewScrolled?
    var scrollViewEndDeaccerlate : ScrollViewScrolled?
    var headerHeight : CGFloat? = 67.0
    var currentDayIndex : Int = 0
    var count : Int?
    var sectionArr = [Int]()
    //    var action : Action?
    
    
    init (items : Array<Any>? , height : CGFloat , tableView : UITableView? , cellIdentifier : String? , count : Int? = 0 , currentDayIndex: Int? , action : Action?) {
        
        self.tableView          = tableView
        self.items              = items
        self.cellIdentifier     = cellIdentifier
        self.tableViewRowHeight = height
        self.count              = self.items?.count
        self.currentDayIndex    = /currentDayIndex
        //        self.action             = action
        
    }
    
    override init() {
        super.init()
    }
}

extension WeeklyPlanDataSource : UITableViewDelegate , UITableViewDataSource, UIScrollViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let identifier = cellIdentifier  else {
            fatalError("Cell identifier not provided")
        }
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        if let block = self.configureCellBlock , let item: Any = (self.items?[currentDayIndex] as? DayPlanList)?.WorkPlans[indexPath.section]{
            block(cell , item , indexPath as IndexPath?)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener, case let cell as Any = tableView.cellForRow(at: indexPath){
            block(indexPath , cell)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sectionArr.contains(section){
            return 1
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        let arr = (self.items?[currentDayIndex] as? DayPlanList)?.WorkPlans.filter({ (workModal) -> Bool in
            return workModal.Status != 1
        })
        return /arr?.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewRowHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let weekPlanHeaderView =  WeeklyPlanHeaders.instanceFromNib() as? WeeklyPlanHeaders else {
            return UIView()
        }
        
        if /currentDayIndex < 0 || /currentDayIndex > /self.items?.count {
            return UIView()
        }
        
        
        weekPlanHeaderView.lblName?.text = (self.items?[currentDayIndex] as? DayPlanList)?.WorkPlans[section].WorkType == 1 ? "Home Work" : "Class Work"
        weekPlanHeaderView.imgType?.image = UIImage(named: section == 0 ? "home_work_icon" : "class_work_icon")
        weekPlanHeaderView.isUserInteractionEnabled = true
        weekPlanHeaderView.btnExpandCollapse?.setImage(UIImage(named : sectionArr.contains(section) ? "up_arrow" : "down_arrow"), for: .normal)
        weekPlanHeaderView.tag = section
        weekPlanHeaderView.linearView?.isHidden = sectionArr.contains(section)
        weekPlanHeaderView.addTapGesture(target: self, action:  #selector(self.openCloseHeader(_:)))
        return weekPlanHeaderView
    }
    
    @objc func openCloseHeader(_ sender : UITapGestureRecognizer){
        
        guard let tag = sender.view?.tag else {return}
        if sectionArr.contains(tag) {
            if let arrIndex = sectionArr.indexes(of: tag).first {
                sectionArr.remove(at: arrIndex)
            }
        }
        else {
            sectionArr.append(tag)
        }
        tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight ?? 0.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let block = scrollViewListener {
            block(scrollView)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let block = scrollViewBeginDeaccerlate {
            block(scrollView)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let block = scrollViewEndDeaccerlate {
            block(scrollView)
        }
    }
}

