//  DBManager.swift
//  e-Care Pro
//  Created by Shubham Dhingra on 02/06/18.
//  Copyright © 2019 Franciscan. All rights reserved.

import Foundation
import UIKit
import CoreData
import EZSwiftExtensions

typealias UserDeleteConfirmation = (Bool) -> ()

enum Entity : String{
    case SchoolCodeTable = "SchoolCodeTable"
    case UserTable = "UserTable"
    
    var get : String{
        return self.rawValue
    }
}

class DBManager : NSObject {
    
    static let shared = DBManager()
    let fetchRequest       = NSFetchRequest<NSFetchRequestResult>()
    
    //MARK::- Save the School Data
    func SaveDataIntoDB(strSchoolCode : String? , result: [String : Any]?, fromStarting : Bool? , forActiveSchool : Bool? = false) {
        
        if /fromStarting{
            DeleteAllData()
        }
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        fetchRequest.predicate = NSPredicate(format:"schCode = %@", /strSchoolCode)
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.SchoolCodeTable.get, in: context)
        fetchRequest.entity    = entityName
        do
        {
            let results =  try context.fetch(fetchRequest) as! [SchoolCodeTable]
            
            
            //MARK ::-  Entry of the new School
            if results.count == 0
            {
                let newEntity = NSEntityDescription.insertNewObject(forEntityName: Entity.SchoolCodeTable.get, into: context) as! SchoolCodeTable
                
                if let result = result {
                    // for the new entry
                    newEntity.schCode          = strSchoolCode//strSchoolCodeVerify
                    newEntity.schName          = (result["SchoolName"]     as? String)
                    newEntity.schCity          = (result["City"]           as? String)
                    newEntity.schLogo          = (result["Logo"]           as? String)
                    newEntity.supportPhone     = (result["SupportPhone"]   as? String)
                    newEntity.themColor        = (result["ThemColor"]      as? String)
                    newEntity.webSite          = (result["WebSite"]        as? String)
                    newEntity.supportEmail     = (result["SupportEmail"]   as? String)
                    newEntity.addMobUrl        = (result["AdditionalMobileURL"] as? String)
                    newEntity.assessmentMarksURL  = (result["AssessmentMarksURL"] as? String)
                    newEntity.paymentUrl       = (result["FeePayemtURL"]    as? String)
                    newEntity.marksEntryURL    = (result["MarksEntryURL"]   as? String)
                    newEntity.feeReportUrl     = (result["FeeReportURL"]    as? String)
                    newEntity.eCare            = (result["eCare"]       as? Int32)!
                    newEntity.eSmartGuard      = (result["eSmartGuard"] as? Int32)!
                    newEntity.smsService       = (result["SMSService"]  as? Int32)!
                    newEntity.sMS_Prvd_ID      = (result["SMS_Prvd_ID"] as? Int32)!
                    newEntity.countryCode      = (result["CountryCode"] as? Int32)!

                    //newEntity.wordDate   = Date().getCurrentDate(format: "dd.MM.yyyy")
                    if !(/forActiveSchool) {
                    let noOfRegisteredSchool   = getAllSchools().count
                    newEntity.isCurrentSchool  = (noOfRegisteredSchool == 1 || noOfRegisteredSchool == 0) ? 1 : 0
                    }
                    else {
                    newEntity.isCurrentSchool  =  1
                    }
                    do{
                        try context.save()
                        if /forActiveSchool {
                            print(" Current school date update successfully")
                        }
                        //print("New entity Saved in database = \(newEntity)")
                        NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.UPDATE_SCHOOL_DATA), object: nil)
                    }
                    catch let error as NSError
                    {
                        print("Entity couldn't save, error = \(error)")
                    }
                    
                }
            }
            if results.count != 0 && !(/forActiveSchool){
                Messages.shared.show(alert: .oops, message: AlertMsg.schoolAlreadyExist.get, type: .warning)
                return
            }
            //MARK ::-  Update the existing school entry
            else
            {
                for item in results
                {
                    item.schCode      = strSchoolCode
                    item.schName      = (result?["SchoolName"]   as? String)
                    item.schCity      = (result?["City"]         as? String)
                    item.schLogo      =  (result?["Logo"]        as? String)
                    item.supportPhone = (result?["SupportPhone"] as? String)
                    item.addMobUrl        = (result?["AdditionalMobileURL"] as? String)
                    item.themColor    = (result?["ThemColor"]    as? String)
                    item.webSite      = (result?["WebSite"]      as? String)
                    item.supportEmail = (result?["SupportEmail"] as? String)
                    item.sMS_Prvd_ID  = (result?["SMS_Prvd_ID"]  as? Int32)!
                    item.countryCode  = (result?["CountryCode"]  as? Int32)!
                    item.eCare        = (result?["eCare"]        as? Int32)!
                    item.eSmartGuard  = (result?["eSmartGuard"]  as? Int32)!
                    item.smsService   = (result?["SMSService"]   as? Int32)!
                    //item.wordDate        = Date().getCurrentDate(format: "dd.MM.yyyy")
                    if !(/forActiveSchool) {
                    let noOfRegisteredSchool = getAllSchools().count
                    item.isCurrentSchool  = (noOfRegisteredSchool == 1 || noOfRegisteredSchool == 0) ? 1 : 0
                    }
                    else {
                        item.isCurrentSchool  =  1
                    }
                    do{
                        if /forActiveSchool {
                            print(" Current school data update successfully")
                        }
                        try context.save()
                        // print("New entity saved in database = \(item)")
                        NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.UPDATE_SCHOOL_DATA), object: nil)
                    }
                    catch let error as NSError
                    {
                          print("Entity couldn't save, error = \(error)")
                    }
                }
            }
        }
        catch let error as NSError
        {
            print("Error in fetch result= \(error)")
        }
    }
    
    //MARK: - Save the User Data
    func saveUserData(result: [String : Any]?, senderCode : String? , username : String? ,password : String?, forVerification : Bool? = false){
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context     = appDelegate.persistentContainer.viewContext
        guard let senderCode = senderCode else {return}
        
        let predicate1 = NSPredicate(format:"schCode = %@", /senderCode)
        if ez.topMostVC is VerificationViewController {
            guard let currentUser = self.getAuthenticatedUser().first else {return}
            let predicate2 = NSPredicate(format: "userID == \(currentUser.userID)")
            let predicate3 = NSPredicate(format: "userType == \(currentUser.userType)")
            let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicate1, predicate2,predicate3])
            fetchRequest.predicate = andPredicate
        }
        else {
            let predicate4 = NSPredicate(format: "username = %@", /username?.lowercased())
            let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicate1, predicate4])
            fetchRequest.predicate = andPredicate
        }
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.UserTable.get, in: context)
        fetchRequest.entity    = entityName
        do
        {
            guard let user = result?["UserDTL"] as? [String: Any] else {return}
            let results =  try context.fetch(fetchRequest) as! [UserTable]
            var name , studentClass : String?
            if let studentName    = (user["Name"] as? String) {
                if studentName.contains("|"){
                    let stringArray = studentName.components(separatedBy: "|")
                    name      = stringArray[0]
                    studentClass = stringArray[1]
                }
                else{
                    
                    //In case the user is Parent
                    if Users.Parent.userId == (user["UserType"] as? Int) {
                        name = studentName
                        studentClass = Users.Parent.get
                    }
                }
            }
            
            if results.count == 0
            {
                let newEntity = NSEntityDescription.insertNewObject(forEntityName:  Entity.UserTable.get, into: context) as! UserTable
                //Student Name & class
                
                newEntity.name                    = name
                newEntity.userClass               = studentClass
                newEntity.username                = username?.lowercased()
                newEntity.photoUrl                = (user["Photo"] as? String)
                newEntity.userID                  = (user["UserID"]  as? Int32)!
                newEntity.userType                = (user["UserType"] as? Int32)!
                newEntity.role                    = (user["RoleName"] as? String)
                newEntity.classID                 = (user["ClassID"] as? Int32)!
                newEntity.schCode                 = senderCode
                newEntity.emailID                 = (user["EmailID"] as? String)
                newEntity.firstName               = (user["FirstName"] as? String)
                newEntity.password                = password
                newEntity.childClass              = studentClass?.trimmed()
                newEntity.updateDateTime          = Date().getCurrentDate(format: "dd.MM.yyyy")
                newEntity.categoryId              = (user["CatID"] as? Int32)!
                newEntity.isAuthenticate          = true
                newEntity.vehicleNumber           = (user["VehicleNumber"] as? String)
                newEntity.searchEnabled           = (user["SearchEnabled"] as? Bool) == false ? false : true
                
                if !(/forVerification) {
                    let noOfActiveUsers = getAuthenticatedUser(anyCondition: false).count
                    newEntity.isActive = (noOfActiveUsers == 1 || noOfActiveUsers == 0) ? 1 : 0
                }
                else {
                    newEntity.isActive  = 1
                }
                
                if let childDTL = user["ChildDTL"] as? [String: Any] {
                    newEntity.childName = childDTL["Name"] as? String
                    newEntity.childPhoto = childDTL["Photo"] as? String
                    newEntity.childClass = studentClass?.trimmed()
                    newEntity.childClass = childDTL["ClassName"] as? String
                    newEntity.motherName = childDTL["MotherName"] as? String
                    newEntity.contactMob = childDTL["Contactmob"] as? String
                }
                
                if let ActiveSession = result?["ActiveSession"] as? [String: Any] {
                    if let SessionStart  = (ActiveSession["SessionStart"] as? String){
                        newEntity.sessionStart = SessionStart
                    }
                    if let SessionEnd  = (ActiveSession["SessionEnd"] as? String){
                        newEntity.sessionEnd = SessionEnd
                    }
                    if let Session  = (ActiveSession["Session"] as? String){
                        newEntity.session = Session
                    }
                }
                
                do{
                    try context.save()
                    if newEntity.userID != 0 && newEntity.userType != 0 {
                        registerDevice(userID:
                        "\(newEntity.userID)", userType : "\(newEntity.userType)")
                    }
                    if /forVerification {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.UPDATE_SCHOOL_DATA), object: nil)
                    }
                    print("New entity = \(newEntity)")
                }
                catch let error as NSError{
                    print("Entity couldn't save, error = \(error)")
                }
            }
            else
            {
                for item in results
                {
                    
                    item.name          = name
                    item.userClass     = studentClass
                    item.username      = username
                    item.password      = password
                    item.photoUrl      = (user["Photo"] as? String)
                    item.userID        = (user["UserID"]  as? Int32)!
                    item.emailID       = (user["EmailID"] as? String)
                    item.userType      = (user["UserType"] as? Int32)!
                    item.classID       = (user["ClassID"] as? Int32)!
                    item.role          = (user["RoleName"] as? String)
                    item.firstName     = (user["FirstName"] as? String)
                    item.password      = password
                    item.categoryId    = (user["CatID"] as? Int32)!
                    item.schCode       = senderCode
                    item.childClass    = studentClass?.trimmed()
                    item.username      = username?.lowercased()
                    item.vehicleNumber = (user["VehicleNumber"] as? String)
                    item.searchEnabled = (user["SearchEnabled"] as? Bool) == false ? false : true
                    
                    if let childDTL      = user["ChildDTL"] as? [String: Any] {
                        item.childName   = childDTL["Name"] as? String
                        item.childPhoto  = childDTL["Photo"] as? String
                        item.childClass = studentClass
                        item.childClass  = childDTL["ClassName"] as? String
                        item.motherName  = childDTL["MotherName"] as? String
                        item.contactMob  = childDTL["Contactmob"] as? String
                    }
                    
                    if let ActiveSession = result?["ActiveSession"] as? [String: Any] {
                        if let SessionStart   = (ActiveSession["SessionStart"] as? String){
                            item.sessionStart = SessionStart
                        }
                        if let SessionEnd   = (ActiveSession["SessionEnd"] as? String){
                            item.sessionEnd = SessionEnd
                        }
                        if let Session   = (ActiveSession["Session"] as? String){
                            item.session = Session
                        }
                    }
                    
                    if  !(/forVerification) {
                        let noOfActiveUsers = getAuthenticatedUser(anyCondition: false).count
                        item.isActive  = (noOfActiveUsers == 1 || noOfActiveUsers == 0) ? 1 : 0
                    }
                    else {
                        item.isActive  = 1
                    }
                    item.isAuthenticate        = true
                    do{
                        try context.save()
                        
                        // print("\(item.userID)", separator : "\(item.userType)")
                        
                        if item.userID != 0 && item.userType != 0 {
                            if (UDKeys.deviceToken.fetch() as? String) != nil  {
                                registerDevice(userID: "\(item.userID)", userType : "\(item.userType)")
                            }
                        }
                        print("New entity = \(item)")
                        if /forVerification {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.VERIFICATION_SUCCESS), object: nil)
                        }
                        
                    }
                    catch let error as NSError
                    {
                        print("Entity couldn't save, error = \(error)")
                    }
                }
            }
        }
        catch let error as NSError{
            // print("Error in fetch result= \(error)")
        }
    }
    
    
    func saveNewUser(result : [String : Any]? , schoolCode : String? , username : String?){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.UserTable.get, in: context)
        fetchRequest.entity    = entityName
        do
        {
            let results =  try context.fetch(fetchRequest) as! [UserTable]
            if results.count == 0 {
                let newEntity = NSEntityDescription.insertNewObject(forEntityName:  Entity.UserTable.get, into: context) as! UserTable
                //Student Name & class
                
                if let studentName    = (result?["Name"] as? String) {
                    newEntity.name               = studentName
                    newEntity.photoUrl           = (result?["Photo"] as? String)
                    newEntity.username           = username?.lowercased()
                    newEntity.schCode           = schoolCode
                    newEntity.isAuthenticate   = false
                    newEntity.isActive          = 0
                }
                do {
                    try context.save()
                    // print("New entity = \(newEntity)")
                    
                    if newEntity.userID != 0 && newEntity.userType != 0 {
                        registerDevice(userID:
                        "\(newEntity.userID)", userType : "\(newEntity.userType)")
                    }
                }
                catch let error as NSError
                {
                    // print("Entity couldn't save, error = \(error)")
                }
            }
        }
        catch let error as NSError{
            // print("Error in fetch result= \(error)")
        }
    }
    
    
    //Get All Authenticated Users (True)
    func getAuthenticatedUser(anyCondition : Bool? = true , schoolCode : String? = nil) -> [UserTable] {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest  =  NSFetchRequest<NSFetchRequestResult>(entityName: Entity.UserTable.get)
        let sort = NSSortDescriptor(key: #keyPath(UserTable.isActive), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        if anyCondition == true {
            fetchRequest.predicate = NSPredicate(format:"isAuthenticate == %@", NSNumber(value: true))
            if let schoolCoode = schoolCode {
                fetchRequest.predicate = NSPredicate(format:"schCode = %@", /schoolCoode)
            }
        }
        do{
            if let results =  try context.fetch(fetchRequest) as? [UserTable] {
                if results.count == 0 {
                    return []
                } else {
                    return results
                }
            }
        }
        catch let error as NSError{
            //  print("Error in fetch result= \(error)")
        }
        return []
    }
    
    func deleteSchoolfromDB(_ schoolCode : String?, _ schoolName : String?) {
        if let schoolCode = schoolCode {
            showAlert(title: AlertConstants.Attention.get, message:"\(AlertMsg.deleteSchoolSure.get)\(/schoolName?.uppercased())?", buttons: [AlertConstants.Delete.get , AlertConstants.Cancel.get] , schoolCode:  schoolCode)
        }
    }
    
    func showAlert(title : String? , message : String? , buttons : [String] , schoolCode : String?) {
        AlertsClass.shared.showAlertController(withTitle: /title, message: /message, buttonTitles: buttons) { (value) in
            let type = value as AlertTag
            switch type {
                case .yes:
                self.deleteSchoolFinalAction(/schoolCode)
                default : return
            }
        }
    }
    
    func deleteSchoolFinalAction(_ schoolCode : String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        fetchRequest.predicate = NSPredicate(format:"schCode = %@", /schoolCode)
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.SchoolCodeTable.get, in: context)
        fetchRequest.entity    = entityName
        do {
            let results =  try context.fetch(fetchRequest) as! [SchoolCodeTable]
            updateCurrentUserIntoDatabase(schoolCode: schoolCode, username: nil, isActive: 0, forDelete: true, confirm: {(isUserDelete) in
                
                if results.count != 0 && isUserDelete{
                    context.delete(results[0])
                }
                else {
                    print("**** No School Found *******")
                }
            })}
        catch {
            print("Fetch Failed: \(error)")
        }
        do {
            try context.save()
            print("Delete Succcesfully")
            NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.UPDATE_SCHOOL_DATA), object: nil)
            
        }
        catch {
            print("Saving Core Data Failed: \(error)")
        }
    }
    
    func getDetailsOfSchoolfromSchoolCode(_ schoolCode : String?) -> SchoolCodeTable? {
        
        guard let schoolCode = schoolCode else {return nil}
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        fetchRequest.predicate = NSPredicate(format:"schCode = %@", schoolCode)
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.SchoolCodeTable.get, in: context)
        fetchRequest.entity    = entityName
        
        do{
            if let results =  try context.fetch(fetchRequest) as? [SchoolCodeTable] {
                return results.first
            }
        }
        catch let error as NSError{
            // print("Error in fetch result from school table = \(error)")
        }
        return nil
    }
    
    
    func updateCurrentSchool(schoolCode : String? , isCurrentSchool : Int){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        fetchRequest.predicate = NSPredicate(format:"schCode = %@", /schoolCode)
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.SchoolCodeTable.get, in: context)
        fetchRequest.entity    = entityName
        do {
            let results =  try context.fetch(fetchRequest) as! [SchoolCodeTable]
            if results.count != 0 {
                results[0].setValue(isCurrentSchool, forKey: "isCurrentSchool")
            }
            else {
            }
        }
        catch {
        }
        do {
            try context.save()
        }
        catch {
        }
    }
    
    func isSchoolPresentInUserDB(_ schoolCode : String) -> (Bool , [UserTable]) {
        let users = getAuthenticatedUser(anyCondition: true, schoolCode: schoolCode)
        return (users.count != 0 , users)
    }
    
    
    func updateCurrentUserIntoDatabase(schoolCode : String? , username : String? , isActive : Int , forDelete : Bool? = false , confirm : @escaping UserDeleteConfirmation) {
        var count : Int?
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let predicate1   = NSPredicate(format:"schCode = %@", /schoolCode)
        let predicate2   = NSPredicate(format: "username = %@", /username?.lowercased())
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicate1, predicate2])
        fetchRequest.predicate = andPredicate
        
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.UserTable.get, in: context)
        fetchRequest.entity    = entityName
        do {
            let results =  try context.fetch(fetchRequest) as! [UserTable]
            count = results.count
            if results.count != 0 {
                if /forDelete &&  results.count == 1 {
                    context.delete(results[0])
                }
                else if /forDelete && results.count > 1 {
                    for user in results {
                        context.delete(user)
                    }
                }
                else {
                    results[0].setValue(isActive, forKey: "isActive")
                    if let schooldata = results[0].schCodes {
                        schooldata.isCurrentSchool = isActive == 1 ? 1 : 0
                        results[0].setValue(schooldata, forKey: "schCodes")
                    }
                    updateCurrentSchool(schoolCode: schoolCode, isCurrentSchool: isActive)
                }
            }
            else {
                
                //print("**** No User Found *******")
            }
        }
        catch {
            //print("Fetch Failed: \(error)")
        }
        do {
            try context.save()
            /forDelete ? print(" \(/count) user Delete Succcesfully") : print("Update Successfully")
            if /forDelete{
                confirm(true)
                NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.ADD_USER), object: nil)
            }
        }
        catch {
            // print("Saving Core Data Failed: \(error)")
        }
    }
    //Update Current User when update from edit Profile Screen
    func updateUserDetailsIntoLocalDB(newValue : String , entity : EntityType) {
        
        if newValue.trimmed().count == 0 {
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        guard let currentUser = self.getAuthenticatedUser().first else {return}
        let predicate1 = NSPredicate(format:"schCode = %@", /currentUser.schCode)
        let predicate2 = NSPredicate(format: "userID == \(currentUser.userID)")
        let predicate3 = NSPredicate(format: "userType == \(currentUser.userType)")
        
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicate1, predicate2,predicate3])
        
        //check here for the sender of the message
        // let fetchRequestSender = NSFetchRequest<NSFetchRequestResult>(entityName: "Keyword")
        fetchRequest.predicate = andPredicate
        
        let entityName         = NSEntityDescription.entity(forEntityName: Entity.UserTable.get, in: context)
        fetchRequest.entity    = entityName
        do {
            
            let results =  try context.fetch(fetchRequest) as! [UserTable]
            if results.count != 0 {
                switch entity {
                    case .username:
                    results[0].setValue(newValue, forKey: "username")
                    case .password:
                    results[0].setValue(newValue, forKey: "password")
                }
            }
            else {
            }
        }
        catch {
        }
        do {
            try context.save()
        }
        catch {
        }
    }
    
    //Condition of check user Already Exist
    //1) username matches with our data
    //2) schoolcode of particular matches with our data
    //3) isAuthenticate -> True
    
    func isUserAlreadyExist(schoolCode : String? , username : String?) -> Bool {
        
        var existUserArr = [UserTable]()
        existUserArr = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest       =  NSFetchRequest<NSFetchRequestResult>(entityName: Entity.UserTable.get)
        do{
            if let results =  try context.fetch(fetchRequest) as? [UserTable] {
                existUserArr = results.filter { (user) -> Bool in
                    return (user.isAuthenticate && user.username == username?.trimmed().lowercased() && user.schCode == schoolCode?.trimmed())
                }
            }
        }
        catch let error as NSError{
            
            //print("Error in fetch result= \(error)")
        }
        existUserArr.count != 0 ? print("User already exist in our App") : print("New User please proceed")
        return existUserArr.count != 0
        
    }
    
    func getAllSchools() -> [SchoolCodeTable]{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest       =  NSFetchRequest<NSFetchRequestResult>(entityName: Entity.SchoolCodeTable.get)
        let sort = NSSortDescriptor(key: #keyPath(SchoolCodeTable.isCurrentSchool), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        do{
            if let results =  try context.fetch(fetchRequest) as? [SchoolCodeTable] {
                if results.count == 0 {
                    return []
                } else {
                    return results
                }
            }
        }
        catch let error as NSError{
            // print("Error in fetch result= \(error)")
        }
        return []
    }
    
    
    func getUsersTableAssociateWithSchools() -> [UserTable] {
        
        let authenicateUsersArr = getAuthenticatedUser()
        let usersArr = authenicateUsersArr
        let schoolsArr = getAllSchools()
        if authenicateUsersArr.count != 0 && schoolsArr.count != 0 {
            
            for (index , user) in authenicateUsersArr.enumerated() {
                for(_ , school) in schoolsArr.enumerated() {
                    if user.schCode == school.schCode {
                        usersArr[index].schCodes = school
                        break
                    }
                }
            }
            return usersArr
        }
        // print("USERS:\(authenicateUsersArr.count)   SCHOOLS:\(schoolsArr.count)")
        return []
    }
    
    
    func DeleteAllData(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequestUser = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.UserTable.get)
        fetchRequestUser.returnsObjectsAsFaults = false
        
        let fetchRequestSchool = NSFetchRequest<NSFetchRequestResult>(entityName: Entity.SchoolCodeTable.get)
        fetchRequestUser.returnsObjectsAsFaults = false
        deleteRows(context , fetchRequestUser)
        deleteRows(context , fetchRequestSchool)
        
    }
    
    func deleteRows(_ context : NSManagedObjectContext, _ fetchRequest : NSFetchRequest<NSFetchRequestResult>){
        do {
            let results = try context.fetch(fetchRequest)
            for object in results {
                if let managedObjectData:NSManagedObject = object as? NSManagedObject {
                    context.delete(managedObjectData)
                }
            }
            //  print("DATA DELETED SUCCESSFULLY")
        }
        catch {
            // print(error)
        }
    }
    
    
    
    func registerDevice(userID: String? , userType : String?) {
        APIManager.shared.request(with: HomeEndpoint.registerDevice(userID: userID, userType: userType)) { (response) in
            switch response{
                case .success(_):
                print(" ****** user is successfully registered with our database *******")
                case .failure(_):
                break
            }
        }
    }
}
