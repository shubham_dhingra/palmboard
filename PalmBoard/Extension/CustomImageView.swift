import Foundation
let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    var imageURLString : String?
    public func imageFromServerURL(urlString: String, tableView : UITableView?, indexpath : IndexPath?) {
        
        imageURLString = urlString
        
        if let url = URL(string: urlString)
        {
            self.image            = R.image.photogalleryPlaceholder()//"White"
            if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
                self.image        = imageFromCache
                //print("*******urlString === \(urlString)")
                return
            }
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil{
                    print(error as Any)
                    return
                }
                DispatchQueue.main.async{
                    if var imgaeToCache = UIImage(data: data!){
                        
                        if self.imageURLString == urlString {
                            imgaeToCache =  imgaeToCache.resizeImage(opaque: nil, imageView: self)
                            self.image   = imgaeToCache
                        }
                        imageCache.setObject(imgaeToCache, forKey: urlString as AnyObject)//set image in cache
                        DispatchQueue.main.async {
                            
                            if let indexpath = indexpath{
                                UIView.performWithoutAnimation({
                                    let indexSet = IndexSet(integer: indexpath.section)
                                    tableView?.reloadSections(indexSet, with: .none)
                                    
                                })
                            }
                        }
                    }
                }
            }) .resume()
        }
    }
}
extension UIImage {
    
    func resizeImage(opaque: Bool?, imageView: UIImageView) -> UIImage {
        var width   = CGFloat(0)
        var height  = CGFloat(0)
        let size    = self.size
        var newImage: UIImage
        let aspectRatio =  size.width/size.height
        
        if(imageView.frame.width > 170)
        {
            height = imageView.frame.width / aspectRatio //dimension
            width  = imageView.frame.width //dimension * aspectRatio
            
        }else
        {
            height = imageView.frame.height
            width  = imageView.frame.height * aspectRatio
        }
        let renderFormat = UIGraphicsImageRendererFormat.default()
        // renderFormat.opaque = opaque
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
        newImage = renderer.image {
            (context) in
            self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        }
        return newImage
    }
}


