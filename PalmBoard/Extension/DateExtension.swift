//  DateExtension.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 15/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import Foundation

extension Date {
    
    func getElapsedInterval() -> String {
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateF = DateFormatter()
        dateF.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateF.timeZone = TimeZone(identifier: "UTC")
        
        let dateToConvert = dateFormatter.string(from: date)
        let convertedDate = dateF.date(from: dateToConvert)
        
        
        let interval = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: self, to: convertedDate!)
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "year ago" :
                "\(year)" + " " + "years ago"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        }
        else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" :
                "\(minute)" + " " + "minutes ago"
        } else if let second = interval.second, second > 0 {
            return "a few seconds ago"
         ///   return second == 10 ? "\(second)" + " " + "second Ago" :
            //    "\(second)" + " " + "seconds Ago"
        } else {
            return "a moment ago"
        }
    }
    func getElapsedIntervalQuestion(date1: String) -> String {
        
        let date                 = Date()
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateF        = DateFormatter()
        dateF.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateF.timeZone   = TimeZone(identifier: "UTC")
        
        let dateToConvert = dateFormatter.string(from: date)
        let convertedDate = dateF.date(from: dateToConvert)
        
        let interval = Calendar.current.dateComponents([.year, .month, .day,.hour,.minute,.second], from: self, to: convertedDate!)
        
         if let day = interval.day, day > 0 {
            
            if day == 1 {
                return "\(day)" + " " + "day ago"
            }
            else{
                    dateFormatter.dateFormat = "dd MMM, yyyy"
                    let dateToConvert1       = dateFormatter.string(from: self)

                    return "\(dateToConvert1) at \(date1)"
            }
            }
        else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago" :
                "\(hour)" + " " + "hours ago"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "minute ago" :
                "\(minute)" + " " + "minutes ago"
        } else if let second = interval.second, second > 0 {
            return "a few seconds ago"
           //return second == 10 ? "\(second)" + " " + "second Ago" :
              //  "\(second)" + " " + "seconds Ago"
        } else {
            return "a moment ago"
        }
    }
    func getElapsedInterval123() -> String {
        
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: Bundle.main.preferredLocalizations[0]) //--> IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME IS GOING TO APPEAR IN SPANISH
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1
        formatter.calendar = calendar
        
        var dateString: String?
        
        let interval = calendar.dateComponents([.year, .month, .weekOfYear, .day], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            formatter.allowedUnits = [.year] //2 years
        } else if let month = interval.month, month > 0 {
            formatter.allowedUnits = [.month] //1 month
        } else if let week = interval.weekOfYear, week > 0 {
            formatter.allowedUnits = [.weekOfMonth] //3 weeks
        } else if let day = interval.day, day > 0 {
            formatter.allowedUnits = [.day] // 6 days
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: Bundle.main.preferredLocalizations[0]) //--> IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME IS GOING TO APPEAR IN SPANISH
            dateFormatter.dateStyle = .medium
            dateFormatter.doesRelativeDateFormatting = true
            
            dateString = dateFormatter.string(from: self) // IS GOING TO SHOW 'TODAY'
            
            return dateString!
            
        }
        
        if dateString == nil {
            dateString = formatter.string(from: self, to: Date())
        }
        
        return dateString! + " Ago"
    }
    
    func NoOfDaysBetweenTwoDates(secondDate : Date) -> Int{
        return /Calendar.current.dateComponents([.day], from: self, to: secondDate).day
    }
    
    func dateToString(formatType : String?) -> String?{
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier:"en_US_POSIX")
        formatter.dateFormat = formatType
        return formatter.string(from: self)
    }
    
    func getDayOnParticularDate() -> String {
        let index = Calendar.current.dateComponents([.weekday], from: self).weekday
        
        switch index {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
    

    
    var convertedDate:Date {
        
        let dateFormatter = DateFormatter();
        
        let dateFormat = "dd MMM yyyy";
        dateFormatter.dateFormat = dateFormat;
        let formattedDate = dateFormatter.string(from: self);
        
        dateFormatter.locale = NSLocale.current;
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00");
        
        dateFormatter.dateFormat = dateFormat as String;
        let sourceDate = dateFormatter.date(from: formattedDate as String);
        
        return sourceDate!;
    }
    
    //SD :- get the start date of current month
    func getStartDateOfMonth(format : String? = "yyyy-MM-dd") -> (String?,Date) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let components = Calendar.current.dateComponents([.year, .month], from: self)
        if let date = Calendar.current.date(from: components){
            let firstDate =  dateFormatter.string(from: date)
            return (firstDate,date)
        }
        return ("",Date())
    }
    
    //SD :- get the start date of current month
    func getStartDateOfMonth1(format : String? = "yyyy-MM-dd") -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let components = Calendar.current.dateComponents([.year, .month], from: self)
        if let date = Calendar.current.date(from: components){
            let firstDate =  dateFormatter.string(from: date)
            return firstDate
        }
        return ""
    }

    
    func getCurrentDate(format : String?) -> String{
        if let format = format {
            let date             = Date()
            let formatter        = DateFormatter()
            formatter.dateFormat = format
            let result           = formatter.string(from: date)
            return result
        }
        return ""
    }
}

