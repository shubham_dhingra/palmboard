//  DoubleExtention.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 15/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import Foundation

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}
