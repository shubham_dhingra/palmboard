

//
//  LabelExtension.swift
//  PalmBoard
//
//  Created by Shubham on 12/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    
    func countLabelLines() -> Int {
        // Call self.layoutIfNeeded() if your view is uses auto layout
        let myText     = self.text! as NSString
        let attributes = [NSAttributedString.Key.font : self.font]
        
        let labelSize  = myText.boundingRect(with: CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes as Any as! [NSAttributedString.Key : Any], context: nil)
        return Int(ceil(CGFloat(labelSize.height) / self.font.lineHeight))
    }
    
    func isTruncated() -> Bool {
        
        if (self.countLabelLines() > self.numberOfLines) {
            return true
        }
        return false
    }
}
