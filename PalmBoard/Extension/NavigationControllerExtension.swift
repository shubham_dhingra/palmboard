import Foundation
import UIKit
import EZSwiftExtensions
import CoreData

extension UINavigationController
{
    func moveToItemPage(itemSelected: Int , module : [String : Any]?)
    {
        var viewController = UIViewController()
        //        viewController.isNavBarHidden = false
        var intCheckviewcontroller = Int()
        let mainStoryboard   = UIStoryboard(name: "Main", bundle: Bundle.main)
        let moduleStoryboard = UIStoryboard(name: "Module", bundle:  Bundle.main)
        let cmStoryboard     = UIStoryboard(name: "ComposeMessage", bundle:  Bundle.main)
        
        intCheckviewcontroller = -1
        //        openControllerRemovePrevious(mainStoryboard)
        switch itemSelected
        {
        case 1:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "noticeStoryboardId") as! NoticeViewController
        case 2:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "galleryStoryboardID") as! GalleryViewController
            
        case 3:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "circularStoryboardId") as! CircularViewController
            
        case 4:
            var intUserType          = Int()
            
            if let userType = self.getCurrentUser()?.userType{
                intUserType =  Int(userType)
            }
            
            if  intUserType == 3
            {
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "staffsyllabusStoryboardId") as! StaffSyllabusViewController
            }
            else{
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "syllabusStoryboardId") as! SyllabusViewController
            }
        case 5:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "attendanceStoryboardId") as! AttendanceViewController
            
        case 6:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "timetableStoryboardID") as! TimeTableViewController
            
        case 7:
             if let module = module?["SubMenu"] as? [[String : Any]] {
                if let vc = mainStoryboard.instantiateViewController(withIdentifier: "libraryStoryBoardID") as? LibraryViewController {
                    viewController = vc
                    vc.subModule = module
                }
            }
             else {
               if let vc = mainStoryboard.instantiateViewController(withIdentifier: "libraryStoryBoardID") as? LibraryViewController {
                    viewController = vc
                    vc.subModule = nil
                }
            }
            
        case 8:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "activityCalStoryboardID") as! ActivityCalendarViewController
            
        case 9:
            // classmates
            if let vc = mainStoryboard.instantiateViewController(withIdentifier: "ClassmatesTeachers") as? ClassmatesTeachersViewController{
                viewController = vc
                vc.intListChoice = 1
                intCheckviewcontroller = 1
                print("vc.intListChoice \(vc.intListChoice)")
            }
            
        case 10:
            if let module = module?["SubMenu"] as? [[String : Any]] {
                
                if let vc = mainStoryboard.instantiateViewController(withIdentifier: "ClassmatesTeachers") as? ClassmatesTeachersViewController{
                    viewController = vc
                    vc.intListChoice = 0
                    vc.subModule = module
                    intCheckviewcontroller = 0
                }
            }
            else {
                if let vc = mainStoryboard.instantiateViewController(withIdentifier: "ClassmatesTeachers") as? ClassmatesTeachersViewController{
                    viewController = vc
                    vc.intListChoice = 0
                    vc.subModule = nil
                    if let moduleName = module?["Module"] as? String {
                        vc.heading = moduleName
                    }
                    intCheckviewcontroller = 0
                }
            }
            
        case 11:
          
            if let module = module?["SubMenu"] as? [[String : Any]] {
                
                viewController = moduleStoryboard.instantiateViewController(withIdentifier: "SelectAttendanceTypeViewController") as!
                SelectAttendanceTypeViewController
                (viewController as? SelectAttendanceTypeViewController)?.subModule = module
                (viewController as? SelectAttendanceTypeViewController)?.module = .GradeBook
            }
            else {
                var userId = Int()
                if let userID = self.getCurrentUser()?.userID {
                    userId = Int(userID)
                }
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "reportStoryboardID") as!
                ReportCardViewController
                (viewController as? ReportCardViewController)?.intUserID = userId
            }
            
        case 12:
            var intUserType1          = Int()
            if let userType = self.getCurrentUser()?.userType{
                intUserType1 =  Int(userType)
            }
            
            if  intUserType1 == 3
            {
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "staffassignmentStoryboardID") as! StaffAssignmentViewController
            }
            else{
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "assignmentStoryboardID") as! AssignmentViewController
            }
            
        case 13:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "birthdayStoryboardID") as! BirthdayViewController
            
        case 14:
            
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "LeaveDetailsViewController") as! LeaveDetailsViewController
            if self.getCurrentUser()?.userType == 3 {
                (viewController as? LeaveDetailsViewController)?.fromBoardScreen = true
            }
            
        case 15:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "paymentStoryboardID") as! PaymentViewController
            //case 16:
        //            viewController = mainStoryboard.instantiateViewController(withIdentifier: "SMSStoryboardID") as! SMSViewController
        case 17:
            openEsmartGuardApplication(module: .Escort)
            
        case 18:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "additionalStoryboardID") as! AdditionalNoViewController
        case 19:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "WebsiteStoryboardID") as! WebsiteViewController
            
        case 21:
            
            if let module = module?["SubMenu"] as? [[String : Any]] {
                
                viewController = moduleStoryboard.instantiateViewController(withIdentifier: "SelectAttendanceTypeViewController") as!
                SelectAttendanceTypeViewController
                (viewController as? SelectAttendanceTypeViewController)?.subModule = module
                (viewController as? SelectAttendanceTypeViewController)?.module = .Attendance
            }
            else {
                viewController = moduleStoryboard.instantiateViewController(withIdentifier: "MarkAttendanceViewController") as!
                MarkAttendanceViewController
                (viewController as? MarkAttendanceViewController)?.AttType = .Class
                (viewController as? MarkAttendanceViewController)?.subModuleId = -1
            }
            
        case 22:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: "ThoughtsViewController") as! AllThoughtsTableViewController
            
        case 23:
            viewController = cmStoryboard.instantiateViewController(withIdentifier: "MyQuestionsStoryboardID") as! MyQuestionsViewController
            break
        case 25:
            var intUserType1          = Int()
            if let userType = self.getCurrentUser()?.userType{
                intUserType1 =  Int(userType)
            }
            
            if  intUserType1 == 3
            {
                viewController = moduleStoryboard.instantiateViewController(withIdentifier: "SelectAttendanceTypeViewController") as!
                SelectAttendanceTypeViewController
                (viewController as? SelectAttendanceTypeViewController)?.module = .WeeklyPlan
            }
            else{
                viewController = moduleStoryboard.instantiateViewController(withIdentifier: "WeeklyPlanStudentViewController") as! WeeklyPlanStudentViewController
            }
        case 26:
            //Marks Entry
            if let vc = moduleStoryboard.instantiateViewController(withIdentifier: "DownloadWeeklyPlanViewController") as? DownloadWeeklyPlanViewController{
                viewController = vc
                vc.module = Module.MarksEntry
            }
            //            viewController = cmStoryboard.instantiateViewController(withIdentifier: "DownloadWeeklyPlanViewController") as! DownloadWeeklyPlanViewController
        //            viewController. = "xyz"
        case 27:
            //Bus Location
            if let vc = moduleStoryboard.instantiateViewController(withIdentifier: "DownloadWeeklyPlanViewController") as? DownloadWeeklyPlanViewController{
                viewController = vc
                vc.module = Module.BusLocator
            }
            
        case 28:
            if let vc = R.storyboard.module.surveyViewController(){
                vc.forCheckResponses = false
                viewController = vc
            }
        case 29:
            if let vc = R.storyboard.main.paySlipViewController() {
                viewController = vc
            }
        default:
            break
        }
        switch itemSelected {
        case 1 ... 19:
            viewController.isNavBarHidden = false
        case 21,25:
            viewController.isNavBarHidden = true
        default :
            viewController.isNavBarHidden = false
        }
        
        let controllers = self.viewControllers
        
        //        viewController.navigationController?.isNavBarHidden = false
        
        if(controllers.count > 1){
            
            for obj in controllers
            {
                
                if ((type(of: viewController) == type(of: obj)  || type(of: obj) == BoardViewController.self) )
                {
                    if type(of: obj) == ClassmatesTeachersViewController.self
                    {
                        print("classmates/teachers adding to navi \n")
                        if let addedClass = obj as? ClassmatesTeachersViewController{
                            if(addedClass.intListChoice == intCheckviewcontroller){
                                print("\n\n same class navigating")
                            }else{
                                print("same class not navigating \n")
                                viewController.isNavBarHidden = false
                                
                                self.pushViewController(viewController, animated: false)
                                self.navigationController?.viewControllers.remove(at: 1)
                                //     collectionView.delegate = customInstance
                                break
                            }
                        }
                    }
                }
                else{
                    print("else obj == \(obj)")
                    viewController.isNavBarHidden = false
                    self.pushViewController(viewController, animated: false)
                    
                    //                  self.pushViewController(viewController, animated: false)
                    self.viewControllers.remove(at: 1)
                    //collectionView.delegate = customInstance
                    break
                }
            }
        }else{
            //            openControllerRemovePrevious()
            self.pushViewController(viewController, animated: false)
            //collectionView.delegate = customInstance
        }
        print("Nav controllers \n\n \(String(describing: self.viewControllers)) \n\n\n")
    }
    func openEsmartGuardApplication(module : Module) {
        
        let allUser = DBManager.shared.getUsersTableAssociateWithSchools()
        var userArr = [String]()
        var schoolArr = [String]()
        var schoolCodeArr = [String]()
        for user in allUser {
            if  let schoolDTL = user.schCodes  {
                if !schoolCodeArr.contains(/user.schCode) && schoolDTL.eSmartGuard != 0{
                    schoolCodeArr.append(/user.schCode)
                    let json = Utility.shared.makeDictObject(schoolDTL).toJson()
                    schoolArr.append(json)
                }
                if user.userType == 2 && schoolDTL.eSmartGuard != 0{
                    if let json = user.toJSON() {
                        userArr.append(json)
                    }
                }
                
                //Role
                if user.userType == 3 && schoolDTL.eSmartGuard != 0 && user.role?.lowercased() == "teacher" {
                    if let json = user.toJSON() {
                        userArr.append(json)
                    }
                }
            }
        }
        
        let userString = userArr.joined(separator: ",")
        let schoolString = schoolArr.joined(separator: ",")
        
        
        var finalUrl = "eSmartGuardApp://e-CarePro/\(module.rawValue)?&schoolData=[\(/schoolString)]&userData=[\(/userString)]"
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let url = URL(string:finalUrl), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL)
        }
        else {
            showInstallationAlert()
        }
    }
    
    
    func showInstallationAlert() {
        
        AlertsClass.shared.showAlertController(withTitle: AlertConstants.eSmartGuard.rawValue, message: AlertMsg.eSmartGuardInstallation.rawValue, buttonTitles: [AlertConstants.Cancel.rawValue , AlertConstants.Install.rawValue]) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                return
            default :
                if let itunesUrl = NSURL(string: APIConstants.eSmartGuardAppLink) {
                    UIApplication.shared.open(itunesUrl as URL)
                }
            }
        }
        
    }
    
}



extension UINavigationController {
    func noNavBar() {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage     = UIImage()
        self.navigationBar.isTranslucent   = true
        self.navigationBar.tintColor       = .white
        self.navigationBar.backgroundColor = .clear
    }
}

extension NSManagedObject {
    func toJSON() -> String? {
        let keys = Array(self.entity.attributesByName.keys)
        let dict = self.dictionaryWithValues(forKeys: keys)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
            var string = NSString(data: jsonData, encoding:  String.Encoding.utf8.rawValue) ?? ""
            string = string.replacingOccurrences(of: "\n", with: "") as NSString
            //print(string)
            string = string.replacingOccurrences(of: "\\", with: "") as NSString
            //print(string)
            //            string = string.replacingOccurrences(of: "\"", with: "") as NSString
            //            string = string.replacingOccurrences(of: " ", with: "##") as NSString
            // print(string)
            return string as String
        }
        catch{}
        return nil
    }
}

