
//
//  NotificationExtension.swift
//  e-Care Pro
//
//  Created by Shubham on 24/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let RECIPEINTS_UPDATE   = Notification.Name("RecipientsUpdate")
    static let UPDATE_STUDENT_LIST = Notification.Name("UPDATE_STUDENT_LIST")
    static let SELECT_UNSELECT_TABBAR = Notification.Name("SELECT_UNSELECT_TABBAR")
    static let SELECT_UNSELECT_MENUITEM = Notification.Name("SELECT_UNSELECT_MENUITEM")
}
