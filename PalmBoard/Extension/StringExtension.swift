//  StringExtension.swift
//  e-Care Pro
//  Created by Ravikant on 10/13/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import Foundation


protocol StringType { var get: String { get } }

extension String: StringType { var get: String { return self } }

extension Optional where Wrapped: StringType {
    func unwrap() -> String {
        return self?.get ?? ""
    }
}


extension String
{
    func unwrapValue()-> String
    {
        return self.trimmed().count == 0  ? "N/A" : self
    }

    func keyPath() -> String{
        return APP_CONSTANTS.KEY
    }
    
    func webAPIDomainNmae() -> String{
        let    path = APIConstants.basePath
        return path
    }
    func imgPath() -> String{
        let path = APIConstants.imgbasePath
        return path
    }
    func imgPath1() -> String{
        let    path = APIConstants.imgbasePath
        return path
    }
    func replace(target: String, withString: String) -> String{
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func replaceSpaceWithPercent20() -> String {
        
        let string = self.replace(string: " ", replacement: "%20")
        return string
    }
    
    func dateChangeFormat(getFormat : String? = "yyyy-MM-dd") -> String{
        
        var strDate              = String()
        let formatter            = ISO8601DateFormatter()
        formatter.formatOptions   = [.withFullDate,.withDashSeparatorInDate]
        let date1                = formatter.date(from: self)
        strDate                     = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = getFormat
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        if let convertedDate = date {
            if abs((Date().year - convertedDate.year)) >= 100 {
                return "N/A"
            }
        
        }
        
        let dateInSpecificFormat = dateFormatter.string(from: date!)
        if dateInSpecificFormat == "01 Jan, 0001" {
            return "N/A"
        }
        else {
          return  dateInSpecificFormat
        }
    }
    
    func convertedTime() -> String {
        var strDate                 = String()
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                   = formatter.date(from: self)
        strDate                     = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "h:mm a"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    
    func getTimeIn12HourFormat(IPFormat : String? = "yyyy-MM-dd'T'HH:mm:ss.SSS", OPFormat : String? = "h:mm a") -> String {
        
        var strDate                 = String()

        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = IPFormat
        let serverDate = dateFormatterGet.date(from : self)
        
        if let date = serverDate {
            strDate = dateFormatterGet.string(from: date)
        }
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = OPFormat
        if !strDate.isEmpty{
            let date: Date? = dateFormatterGet.date(from: strDate)
            return dateFormatter.string(from: date!)
        }
        return ""
    }
    
    func convertStringIntoDate(getDateFormat : String? = "yyyy-MM-dd") -> Date{
        
        var strDate              = String()
        let formatter            = ISO8601DateFormatter()
        formatter.formatOptions   = [.withFullDate,.withDashSeparatorInDate]
        let date1                = formatter.date(from: self)
        strDate                  = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = getDateFormat
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        return dateFormatterGet.date(from: strDate) ?? Date()
    }
    
    func dateChangeFormatQuizPay() -> String{
        
        var strDate      = String()
        let formatter            = ISO8601DateFormatter()
        formatter.formatOptions   = [.withFullDate,.withDashSeparatorInDate]
        let date1                = formatter.date(from: self)
        strDate                  = formatter.string(from: date1!)
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd-MM-yyyy"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
  
    
    func dateChangeFormatSMS() -> String{
        
        var strDate      = String()
        let formatter            = ISO8601DateFormatter()
        formatter.formatOptions   = [.withFullDate,.withDashSeparatorInDate]
        let date1                = formatter.date(from: self)
        strDate                  = formatter.string(from: date1!)
        print("strDate = \(strDate)")
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "MMM d, h:mm a"
        dateFormatter.timeZone   = NSTimeZone(abbreviation: "GMT+5:30")! as TimeZone//GMT+5:30
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    
    func dateOrdinalFormat() -> String{
        
        let formatter              = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                  = formatter.date(from: self)
        
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date1!)
        
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMMM, yyyy"
        let newDate = dateFormate.string(from: date1!)
        let day  = anchorComponents.day!
        let day2 = "\(day)" + "\(day.ordinal)"
        
        return day2 + " " + newDate
    }
    
    func dateChangeFormatShortProfile() -> String{
        var strDate              = String()
        
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                   = formatter.date(from: self)
        strDate                     = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    
    func dateChangeFormatQuestion() -> String{
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withYear, .withMonth, .withDay, .withTime, .withDashSeparatorInDate, .withColonSeparatorInTime]
        let date = formatter.date(from: self)
        
        
      //  var strDate                 = String()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                   = formatter.date(from: self)

        
       // let timeAgo = (date?.getElapsedIntervalQuestion())
        let timeAgo = (date?.getElapsedIntervalQuestion(date1: self.getTimeIn12HourFormat()))

//        print("timeAgo   =    ",timeAgo!)
        return (timeAgo)!
    }
    
    func dateChangeFormatCalendar() -> String{
        var strDate              = String()
        
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                   = formatter.date(from: self)
        strDate                     = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd/MM/yyyy"
        let date: Date?             = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    func dateChangeFormatWeeklyCalendar() -> Date?{
        var strDate              = String()
        
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        let date1                   = formatter.date(from: self)
        strDate                     = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        //  let dateFormatter           = DateFormatter()
        // dateFormatter.dateFormat    = "dd/MM/yyyy"
        let date: Date?             = dateFormatterGet.date(from: strDate)
        return date
        // return dateFormatterGet.string(from: date!)
    }

    func dateChangeFormatDays() -> String{
        var strDate              = String()
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: self)
        strDate                 = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "EEEE"
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    
    func dateChangeFormatMonth() -> String{
        var strDate              = String()
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: self)
        strDate                 = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "MMMM"
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    func convertStringToDateWithSS(dateString: String) -> Date
    {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: dateString)!
        
        return date
    }
    func dateInseconds() -> Int {
        //string into date
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        //dateFormatter.locale     = Locale(identifier: "en_IN")//hi_IN
        dateFormatter.timeZone   = NSTimeZone(abbreviation: "GMT")! as TimeZone//GMT+5:30
        
        let myDate               = dateFormatter.date(from: self)!
        
        //date into milliseconds
        let since1970 = myDate.timeIntervalSince1970
        return Int(since1970)
    }
    func keyMonths() -> [String]{
        
        var monthArray = [String]()
        let dateFormatterGet = DateFormatter()
        
        for index in 0..<12
        {   let newDate  = Calendar.current.date(byAdding: .month, value:  index, to: Date())
            
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "MMMM"
            
            let strDate: String? = dateFormatterGet.string(from: newDate!)
            let date123: Date? = dateFormatterGet.date(from: strDate!)
            let strMonthFinal = dateFormatter.string(from: date123!)
            
            monthArray.append(strMonthFinal)
        }
        return monthArray
    }
    func dateChangeBirthdayUser() -> String{
        var strDate              = String()
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: self)
        strDate                 = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd - MMM"
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
        
    }
    func dateChangeBirthdayUserToday() -> String{
        var strDate              = String()
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: self)
        strDate                = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM"
        dateFormatter.timeZone     = NSTimeZone(abbreviation: "GMT+5:30")! as TimeZone
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    // MARK: - DateGetMonthName method
    func DateGetMonthName() -> String
    {
        var strDate              = String()
        
        let formatter            = ISO8601DateFormatter()
        formatter.formatOptions  = [.withFullDate,.withDashSeparatorInDate]
        let date1                = formatter.date(from: self)
        strDate                  = formatter.string(from: date1!)
        
        let dateFormatterGet       = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter          = DateFormatter()
        
        dateFormatter.dateFormat   = "MMMM"
        dateFormatter.timeZone     = NSTimeZone.system
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        return dateFormatter.string(from: date!)
    }
    
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches  = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.lightGray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
        var hasOnlyNewlineSymbols: Bool {
            return trimmingCharacters(in: CharacterSet.newlines).isEmpty
        }
    
        func replace(string:String, replacement:String) -> String {
            return self.replacingOccurrences(of: string, with: replacement)
        }
        
        func removeWhitespace() -> String {
            return self.replace(target: " ", withString: "")
        }

    func convertBase64ToImage() -> UIImage? {
        let decodedData  = NSData(base64Encoded: self, options: NSData.Base64DecodingOptions(rawValue: 0))
        guard let data   = decodedData as Data? else {return nil}
        let decodedimage = UIImage(data: data)
        return decodedimage
    }
    
    
    func extractYoutubeIdFromLink() -> String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        guard let regExp = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive) else {
            return nil
        }
        let nsLink = self as NSString
        let options = NSRegularExpression.MatchingOptions(rawValue: 0)
        let range = NSRange(location: 0, length: nsLink.length)
        let matches = regExp.matches(in: self as String, options:options, range:range)
        if let firstMatch = matches.first {
            return nsLink.substring(with: firstMatch.range)
        }
        return nil
    }
}


