
import Foundation
import UIKit

public extension UITextField
{
    func useUnderline()
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth        = screenSize.width
        let border             = CALayer()
        let borderWidth        = CGFloat(1.0)
        border.borderColor     = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha:1).cgColor
        border.frame           = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: screenWidth, height: 1)
        // print(border.frame)
        border.borderWidth      = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    var placeHolderFont: UIFont? {
        get {
            return self.placeHolderFont
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.font : newValue!])
        }
    }
    
    @IBInspectable var placeHolderColorQuiz: UIColor? {
        get {
            return self.placeHolderColorQuiz
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }

}

