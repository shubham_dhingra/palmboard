
import Foundation
import UIKit

public extension UITextView
{
    func useUnderline()
    {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth        = screenSize.width
        let border             = CALayer()
        let borderWidth        = CGFloat(1.0)
         border.borderColor     = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha:1).cgColor
        
       
            border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: screenWidth, height: 1)
     
        // print(border.frame)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func removeUnderline()
    {
        self.layer.borderWidth = 0
    }
}
