//
//  ValidateTextFields.swift
//  e-Care Pro
//
//  Created by Ravikant on 13/10/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
    func txtShakeAnimation()
    {
        let animation          = CABasicAnimation(keyPath: "position")
        animation.duration     = 0.07
        animation.repeatCount  = 4
        animation.autoreverses = true
        animation.fromValue    = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        animation.toValue      = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(animation, forKey: "position")
    }
    func borderWidthColor()
    {
        self.layer.borderColor  = UIColor.red.cgColor
        self.layer.borderWidth  = 1.0
        self.layer.cornerRadius = 5.0
    }
    func borderWidthColorQuiz()
    {
        self.layer.borderColor  = UIColor.red.cgColor
        self.layer.borderWidth  = 1.0
        // self.layer.cornerRadius = 5.0
    }
    
    func borderWidthColorGray()
    {
        self.layer.borderColor  =  UIColor(rgb: 0xB28D28).cgColor //UIColor.red.cgColor//
        self.layer.borderWidth  = 2.0
        self.layer.cornerRadius = 5.0
    }
    
    func borderColorWithGreen()
    {
        self.layer.borderColor  =  UIColor.flatGreen.cgColor //UIColor.red.cgColor//
        self.layer.borderWidth  = 1.0
    }
    
    func textColorError (text: String)
    {
        self.textColor = UIColor.red
        self.text = text
    }
    func textColorNoError (text: String)
    {
        //self.textColor = UIColor.red//
        self.textColor = UIColor(rgb: 0x545454)//0xFEC309
        self.text      = text
    }
    func textColorYellow (text: String)
    {
        self.textColor = UIColor(rgb: 0x56B54B)
        self.text      = text
    }
    
    func borderColorClean()
    {
        self.layer.borderColor  = UIColor.clear.cgColor
        self.layer.borderWidth  = 0.0
        self.layer.cornerRadius = 0.0
    }
    func borderColorCleanQuiz()
    {
        self.layer.borderColor  = UIColor.clear.cgColor
        self.layer.borderWidth  = 0.0
        //self.layer.cornerRadius = 0.0
    }
    
    
}

