//
//  ViewControllerExtension.swift
//  e-Care Pro
//
//  Created by ShubhamMac on 19/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func getCurrentSchool() -> SchoolCodeTable? {
        
        return DBManager.shared.getAllSchools().first
    }
    
    func getCurrentUser() -> UserTable?{
        return DBManager.shared.getAuthenticatedUser().first
    }
    
    func showMenu() {
        UIView.animate(withDuration: 0.5) {
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
    }
    
    func hideMenu() {
        UIView.animate(withDuration: 0.5) {
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: true)
            }
        }
    }
    
    
    func hideTabBar() {
        self.navigationController?.noNavBar()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        if let vc = self.navigationController?.parent as? CustomTabbarViewController {
            vc.mainTabbar?.isHidden = true
        }
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.tabCollectionView?.isHidden = true
        parent.hideSetUp(hide : true)
        parent.hideBar(boolHide: true)
    }
        
        
    func showTabBar() {
            if let vc = self.navigationController?.parent as? CustomTabbarViewController {
                vc.mainTabbar?.isHidden = false
            }
            let parent =  self.navigationController?.parent as! CustomTabbarViewController
            parent.tabCollectionView?.isHidden = false
            parent.hideSetUp(hide: false)
        }
}
