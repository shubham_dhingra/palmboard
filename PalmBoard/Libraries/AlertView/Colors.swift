//
//  Colors.swift
//  FCAlertView
//
//  Created by Kris Penney on 2016-08-26.
//  Copyright © 2016 Kris Penney. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
   
    // Preset Flat Colors
    @nonobjc public static let flatTurquoise = UIColor(red: 26/255, green: 188/255, blue: 156/255, alpha: 1)
    @nonobjc public static let flatBlue = UIColor(red: 41/255, green: 128/255, blue: 185/255, alpha: 1)
    @nonobjc public static let flatMidnight = UIColor(red: 44/255, green: 62/255, blue: 80/255, alpha: 1)
    @nonobjc public static let flatPurple = UIColor(red: 142/255, green: 68/255, blue: 173/255, alpha: 1)
    @nonobjc public static let flatOrange = UIColor(red: 243/255, green: 156/255, blue: 18/255, alpha: 1)
    @nonobjc public static let flatRed = UIColor(red: 192/255, green: 57/255, blue: 43/255, alpha: 1)
    @nonobjc public static let flatSilver = UIColor(red: 189/255, green: 195/255, blue: 199/255, alpha: 1)
    @nonobjc public static let flatGray = UIColor(red: 206/255, green: 206/255, blue: 206/255, alpha: 1)
    @nonobjc public static let flatAppGray = UIColor(red: 245.0, green: 245.0, blue: 245.0, alpha: 1.0)
    @nonobjc public static let successColor = UIColor(red: 0/255, green: 128.0/255, blue: 0.0/255, alpha: 1.0)
    @nonobjc public static let flatStepBlack = UIColor(red: 61/255, green: 61/255, blue: 61/255, alpha: 1.0)
    @nonobjc public static let flatStepProgress = UIColor(red: 231.0/255, green: 129.0/255, blue: 121.0/255, alpha: 1.0)
   
    @nonobjc public static let flatTopTextColor = UIColor.black.withAlphaComponent(0.36)
    
    //Color for Store status
    @nonobjc public static let pkgOrange = UIColor(red: 91.0/255, green: 51.0/255, blue: 47.0/255, alpha: 1.0)
    @nonobjc public static let pkgPurple = UIColor(red: 114.0/255, green: 116.0/255, blue: 149.0/255, alpha: 1.0)
    @nonobjc public static let pkgGreen = UIColor(red: 104.0/255, green: 168.0/255, blue: 173.0/255, alpha: 1.0)
    @nonobjc public static let flatGreen = UIColor(red: 86/255, green: 181/255, blue: 75/255, alpha: 1)
    @nonobjc public static let flatDisableColor = UIColor(red: 191/255, green: 191/255, blue: 191/255, alpha: 1.0)
    
    @nonobjc public static var ColorApp =   UIColor(red: 231.0/255.0, green: 129.0/255.0, blue: 121.0/255.0, alpha: 1.0)
    @nonobjc public static var flatPending =  UIColor(red: 57.0/255.0, green: 130.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    @nonobjc public static var flatRejected =  UIColor(red: 251.0/255.0, green: 78.0/255.0, blue: 78.0/255.0, alpha: 1.0)
    @nonobjc public static var flatApproved =  UIColor(red: 75.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    @nonobjc public static var flatBlack =  UIColor(red: 58.0/255.0, green: 58.0/255.0, blue: 58.0/255.0, alpha: 1.0)
     @nonobjc public static var flatLightBlack =  UIColor(red: 84.0/255.0, green: 84.0/255.0, blue: 84.0/255.0, alpha: 1.0)
    @nonobjc public static var f3f3f3  =  UIColor(red: 243.0/255.0, green:  243.0/255.0, blue:  243.0/255.0, alpha: 1.0)
     @nonobjc public static var cccccc  =  UIColor(red: 204.0/255.0, green:  204.0/255.0, blue:  204.0/255.0, alpha: 1.0)
     @nonobjc public static var markPresentColor  =  UIColor(red: 86.0/255.0, green:  181.0/255.0, blue:  75.0/255.0, alpha: 1.0)
    @nonobjc public static var markAbsentColor  =  UIColor(red: 251.0/255.0, green:  78.0/255.0, blue:  78.0/255.0, alpha: 1.0)
    @nonobjc public static var markLeaveColor  =  UIColor(red: 0.0/255.0, green:  152.0/255.0, blue:  254.0/255.0, alpha: 1.0)
    @nonobjc public static var flatBlack70Color = UIColor(red: 112.0/255.0, green:  112.0/255.0, blue:  112.0/255.0, alpha: 1.0)
    
    
      @nonobjc public static let pieChartStudentColor = UIColor(red: 44.0/255, green: 171.0/255, blue: 227.0/255, alpha: 1.0)
    
      @nonobjc public static let pieChartParentColor = UIColor(red: 255.0/255, green: 118.0/255, blue: 118.0/255, alpha: 1.0)
    
      @nonobjc public static let pieChartStaffColor = UIColor(red: 255.0/255, green: 195.0/255, blue: 109.0/255, alpha: 1.0)
    @nonobjc public static let piecChartLateColor = UIColor(red: 250.0/255, green: 204.0/255, blue: 72.0/255, alpha: 1.0)
   
        }

