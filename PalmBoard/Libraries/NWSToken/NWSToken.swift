//
//  NWSToken.swift
//  NWSTokenView
//
//  Created by Shubham Dhingra on 8/11/15.



import UIKit

open class NWSToken: UIView
{
    open var hiddenTextView = UITextView()
    open var isSelected: Bool = false
    var object : Any?

    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        // Hide text view (for using keyboard to delete token)
        hiddenTextView.isHidden = true
        hiddenTextView.text = "NWSTokenDeleteKey" // Set default text for detection in delegate
        hiddenTextView.autocorrectionType = UITextAutocorrectionType.no // Hide suggestions to prevent key from being displayed
        addSubview(hiddenTextView)
    }
}
