//  AttendanceDetailsModel.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 26/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class AttendanceDetailsModel: Mappable
{
    
    var attSummary : AttSummary?
    var attendance : [Attendance]?
    var className  : String?
    var errorCode  : Int?
    var message    : String?
    var name       : String?
    var photo      : String?
    var status     : String?
    var userID     : Int?
    var isLateEnabled : Bool?
    var result : [Result]?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        attSummary <- map["AttSummary"]
        attendance <- map["Attendance"]
        className  <- map["ClassName"]
        errorCode  <- map["ErrorCode"]
        message    <- map["Message"]
        name       <- map["Name"]
        photo      <- map["Photo"]
        status     <- map["Status"]
        userID     <- map["UserID"]
        isLateEnabled <- map["isLateEnabled"]
        result     <- map["Result"]
    }
}

class Attendance : Mappable{
    
    var attDate  : String?
    var markedAs : AnyObject?
    var status   : Int?
    var isLate   : Bool?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        attDate  <- map["AttDate"]
        markedAs <- map["MarkedAs"]
        status   <- map["Status"]
        isLate <- map["isLate"]
    }
}
class AttSummary :  Mappable{
    
    var absent          : Int?
    var leave           : Int?
    var prasent         : Int?
    var schoolDays      : Int?
    var totalAbsent     : Int?
    var totalLeave      : Int?
    var totalPrasent    : Int?
    var totalSchoolDays : Int?
    var totalLateDays   : Int?
    var late           : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        absent          <- map["Absent"]
        leave           <- map["Leave"]
        prasent         <- map["Prasent"]
        schoolDays      <- map["SchoolDays"]
        totalAbsent     <- map["TotalAbsent"]
        totalLeave      <- map["TotalLeave"]
        totalPrasent    <- map["TotalPrasent"]
        totalSchoolDays <- map["TotalSchoolDays"]
        totalLateDays   <- map["TotalLate"]
        late            <- map["Late"]
    }
}
class Result : Mappable{
    
    var addmissionNo : String?
    var billNumber   : String?
    var className    : String?
    var contactMob   : String?
    var designation  : String?
    var name         : String?
    var photo        : String?
    var userID       : Int?
    var userType     : Int?
    
    required init?(map: Map){}
    func mapping(map: Map)
    {
        addmissionNo <- map["AddmissionNo"]
        billNumber   <- map["BillNumber"]
        className    <- map["ClassName"]
        contactMob   <- map["ContactMob"]
        designation  <- map["Designation"]
        name         <- map["Name"]
        photo        <- map["Photo"]
        userID       <- map["UserID"]
        userType     <- map["UserType"]
    }
}
