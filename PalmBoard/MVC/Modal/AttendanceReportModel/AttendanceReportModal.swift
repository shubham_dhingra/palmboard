//  AttendanceReportModal.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import Foundation
import ObjectMapper

class AttReport : Mappable{
    var attDate   : String?
    var ClassName : String?
    var name      : String?
    var photo     : String?
    var status    : Int?
    var userID    : Int?
    var isLate    : Bool?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        attDate   <- map["AttDate"]
        ClassName <- map["ClassName"]
        name      <- map["Name"]
        photo     <- map["Photo"]
        status    <- map["Status"]
        userID    <- map["UserID"]
        isLate    <- map["isLate"]
    }
}
class MyClasse : Mappable{
    
    var classID   : Int?
    var className : String?
    var isSelect  : Bool?
    var late : Int?
    var leave : Int?
    var present : Int?
    var absent : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        classID   <- map["ClassID"]
        className <- map["ClassName"]
        isSelect  <- map["isSelect"]
        absent <- map["Absent"]
        late <- map["Late"]
        leave <- map["Leave"]
        present <- map["Present"]
    }
}
