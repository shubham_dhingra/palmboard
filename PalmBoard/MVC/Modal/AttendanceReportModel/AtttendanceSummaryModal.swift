//
//  AtttendanceSummaryModal.swift
//  PalmBoard
//
//  Created by Shubham on 29/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


class AtttendanceSummaryModal : Mappable{
    
    var myClassArr : [MyClasse]?
    var errorCode : Int?
    var message : String?
    var status : String?
    var totalAbsent : Int?
    var totalLate : Int?
    var totalLeave : Int?
    var totalPresent : Int?
    var isLateEnabled : Bool?
    
    
   
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        myClassArr <- map["ClassSummary"]
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        status <- map["Status"]
        totalAbsent <- map["TotalAbsent"]
        totalLate <- map["TotalLate"]
        totalLeave <- map["TotalLeave"]
        totalPresent <- map["TotalPresent"]
        isLateEnabled <- map["isLateEnabled"]
        
    }
}




