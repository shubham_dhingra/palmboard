//
//    Contact.swift
//
//    Create by NupurMac on 21/5/2018
//    Copyright © 2018. All rights reserved.
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper


class Contacts :  Mappable{
    
    var childName : String?
    var ClassName : String?
    var designation : String?
    var name : String?
    var photo : String?
    var receiverID : Int?
    var receiverType : Int?
    var isSelect : Bool = false
    var mobile : String?
    
    
    
    init(receiverId : Int? , receiverType : Int? , name : String? , photo : String?) {
        self.name = name
        self.receiverID = receiverId
        self.receiverType = receiverType
        self.photo = photo
    }
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName <- map["ChildName"]
        ClassName <- map["ClassName"]
        designation <- map["Designation"]
        name <- map["Name"]
        photo <- map["Photo"]
        receiverID <- map["ReceiverID"]
        receiverType <- map["ReceiverType"]
        mobile <- map["Mobile"]
        isSelect = false
    }
}
