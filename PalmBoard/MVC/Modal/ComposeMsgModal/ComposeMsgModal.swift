//
//    RootClass.swift
//
//    Create by NupurMac on 21/5/2018
//    Copyright © 2018. All rights reserved.

import Foundation
import ObjectMapper
import UIKit

class ContactsModel :  Mappable{
    
    var errorCode : Int?
    var message : String?
    var myClasses : [MyClasses]?
    var status : String?
    var contacts : [Contacts]?
    
    
    required init?(map: Map){
    
    }
    
    func mapping(map: Map) {
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        myClasses <- map["MyClasses"]
        status <- map["Status"]
        contacts <- map["Contacts"]
        
    }
}

class MyClasses : Mappable{
    
    var classID : Int?
    var ClassName : String?
    var isSelect : Bool = false
    var isSelected : Bool?
    var colorAssign : UIColor?
    
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        classID <- map["ClassID"]
        ClassName <- map["ClassName"]
        isSelect = false
        isSelected <- map["isSelect"]
        colorAssign = nil
    }
}

//struct ContactsModel: Codable {
//
//    let ReceiverID:   Int?
//    let ReceiverType: Int?
//    let Name:         String?
//    let Designation:  String?
//    let ChildName:    String?
//    let ClassName:    String?
//    let Photo:        String?
//    var isSelect : Bool  = false
//}
//
//struct  StudentParentContact : Codable{
//    let ErrorCode: Int
//    let Status:    String
//    let Message:   String
//    let Contacts: [ContactsModel]?
//
//    private enum CodingKeys: String, CodingKey {
//        case ErrorCode
//        case Status
//        case Message
//        case Contacts
//    }
//}
//
//struct  MyClassesCompose : Codable{
//
//    let ErrorCode: Int
//    let Status:    String
//    let Message:   String
//    var MyClasses: [MyClassesModel]?
//
//    private enum CodingKeys: String, CodingKey {
//        case ErrorCode
//        case Message
//        case Status
//        case MyClasses
//    }
//
//}
//
//
//struct MyClassesModel: Codable {
//    let ClassID:   Int?
//    let ClassName: String?
//    var isSelect : Bool = false
//
//    init(classID_ : Int? , className_ : String? , isSelect_ : Bool) {
//        ClassID = classID_
//        ClassName =  className_
//        isSelect = isSelect_
//    }
//}
//
//
//
