//
//  MatchmultifacesModal.swift
//  PalmBoard
//
//  Created by franciscan on 17/12/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class MatchmultifacesModal : Mappable {
    
    var clientAppId : String?
    var errorCode : Int?
    var message : String?
    var status : String?
    var dataClientAppDTO : AnyObject?
    var dataClientAppDTOList : AnyObject?
    var dataList : AnyObject?
    var dataprofileList : [DataprofileList]?
    var errormessage : String?
    var mode : Int?
    var result : AnyObject?
    var stid : Int?
    var stidImage : AnyObject?
    var stidName : AnyObject?
    var stidUsertype : Int?
    var tempId : AnyObject?
    var templatevalue : AnyObject?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        clientAppId <- map["ClientApp_Id"]
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        status <- map["Status"]
        dataClientAppDTO <- map["dataClientAppDTO"]
        dataClientAppDTOList <- map["dataClientAppDTO_list"]
        dataList <- map["data_list"]
        dataprofileList <- map["dataprofile_list"]
        errormessage <- map["errormessage"]
        mode <- map["mode"]
        result <- map["result"]
        stid <- map["stid"]
        stidImage <- map["stid_image"]
        stidName <- map["stid_name"]
        stidUsertype <- map["stid_usertype"]
        tempId <- map["tempId"]
        templatevalue <- map["templatevalue"]
    }
    
}
class DataprofileList : Mappable{
    
    var childName : AnyObject?
    var childPhoto : AnyObject?
    var className : String?
    var coverImg : String?
    var dOB : String?
    var designation : AnyObject?
    var fatherName : String?
    var motherName : String?
    var name : String?
    var photo : String?
    var roleName : String?
    var socialMedia : AnyObject?
    var userID : Int?
    var userType : Int?
    var username : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName <- map["ChildName"]
        childPhoto <- map["ChildPhoto"]
        className <- map["ClassName"]
        coverImg <- map["CoverImg"]
        dOB <- map["DOB"]
        designation <- map["Designation"]
        fatherName <- map["FatherName"]
        motherName <- map["MotherName"]
        name <- map["Name"]
        photo <- map["Photo"]
        roleName <- map["RoleName"]
        socialMedia <- map["SocialMedia"]
        userID <- map["UserID"]
        userType <- map["UserType"]
        username <- map["Username"]
    }
}
