//
//  MessageDTLModal.swift
//  e-Care Pro
//
//  Created by Shubham on 06/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageDTLModal : Mappable {
    
    var errorCode : Int?
    var message : String?
    var msgDTL : [ChatMsgs]?
    var msgID : Int?
    var recipients : Recipient?
    var status : String?
    var subject : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        msgDTL <- map["MsgDTL"]
        msgID     <- map["MsgID"]
        recipients <- map["Recipients"]
        status     <- map["Status"]
        subject    <- map["Subject"]
    }
    
}


class ChatMsgs : Mappable{
    
    var body : String?
    var filePath : String?
    var msgID : Int?
    var msgType : Int?
    var rplID : Int?
    var senderDTL : SenderDTL?
    var sentOn : String?
    var isPlaying : Bool = false
    var jukeBoxRef  : Jukebox?
    required init?(map: Map){}
    
    func mapping(map: Map){
        body      <- map["Body"]
        filePath  <- map["FilePath"]
        msgID     <- map["MsgID"]
        msgType   <- map["MsgType"]
        rplID     <- map["RplID"]
        senderDTL <- map["SenderDTL"]
        sentOn    <- map["SentOn"]
    }
}
