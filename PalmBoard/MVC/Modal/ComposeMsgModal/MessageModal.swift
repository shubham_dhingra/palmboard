//
//  MessageModal.swift
//  e-Care Pro
//
//  Created by Shubham on 05/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper


class MessageMainModal : Mappable{
    
    var errorCode : Int?
    var message : String?
    var messageArr : [IMessages]?
    var sentMessageArr : [SMessages]?
    var status : String?
    var total : Int?
    var smsTypeList : [SMSTypeList]?
    var compose : Bool?
    var msgWithSMS : Bool?
    var OnlyMsg : Bool?
    var AI_Msg : Bool?
    var media : Media?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        messageArr <- map["Sender"]
        status <- map["Status"]
        sentMessageArr <- map["SentMessages"]
        sentMessageArr <- map["AllMessages"]
        total <- map["Total"]
        smsTypeList <- map["SMSType"]
        compose <- map["Compose"]
        msgWithSMS <- map["MsgWithSMS"]
        OnlyMsg <- map["OnlyMsg"]
        AI_Msg <- map["AI_Msg"]
        media <- map["Media"]
    }

}

class IMessages : Mappable {
    
    var childName : String?
    var className : String?
    var designation : String?
    var iD : String?
    var name : String?
    var photo : String?
    var senderID : Int?
    var senderType : Int?
    var sentOn : String?
    var unread : Int?
    var hasRead : Int?
    
    init(childName_ : String? , className_ : String? , designation_ : String? , iD_ : String? , name_ : String? , photo_ : String? , senderID_ : Int? , senderType_ : Int? , sentOn_ : String? , unread_ : Int?) {
        
        self.childName = childName_
        self.className = className_
        self.designation = designation_
        self.iD = iD_
        self.name = name_
        self.photo = photo_
        self.senderID = senderID_
        self.senderType = senderType_
        self.sentOn = sentOn_
        self.unread = unread_
        
    }
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName <- map["ChildName"]
        className <- map["ClassName"]
        designation <- map["Designation"]
        iD <- map["ID"]
        name <- map["Name"]
        photo <- map["Photo"]
        senderID <- map["SenderID"]
        senderType <- map["SenderType"]
        sentOn <- map["SentOn"]
        unread <- map["Unread"]
        hasRead <- map["hasRead"]
    }
}

class SMessages :  Mappable{
    
    var abbreviation : String?
    var iD : String?
    var msgID : Int?
    var msgType : Int?
    var recipients : [Recipients]?
    var sentOn : String?
    var subject : String?
    var isReplyMsg : Int?
    var hasRead : Int?
    var photo : String?
   
    init(abbreviation_ : String? , iD_ : String? , msgID_ : Int? , msgType_ : Int? , recipients_ :[Recipients]? , sentOn_ : String? , subject_ : String? , isReplyMsg_ : Int? , hasRead_ : Int? , photo_ : String?) {
       
        self.abbreviation = abbreviation_
        self.iD = iD_
        self.msgID = msgID_
        self.msgType = msgType_
        self.recipients = recipients_
        self.sentOn = sentOn_
        self.subject = subject_
        self.isReplyMsg = isReplyMsg_
        self.hasRead = hasRead_
        self.photo = photo_
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        
        abbreviation <- map["Abbreviation"]
        iD <- map["ID"]
        msgID <- map["MsgID"]
        msgType <- map["MsgType"]
        recipients <- map["Recipients"]
        sentOn <- map["SentOn"]
        subject <- map["Subject"]
        isReplyMsg <- map["isReplyMsg"]
        hasRead <- map["hasRead"]
        photo <- map["Photo"]
    }
}

class Recipients :  Mappable{
    
    var childName : String?
    var childPhoto : String?
    var className : String?
    var designation : String?
    var mobile : String?
    var name : String?
    var photo : String?
    var receiverID : Int?
    var receiverType : Int?
    var isSelect : Bool?
   
    required init?(map: Map){}
    
    func mapping(map: Map) {
        
        childName <- map["ChildName"]
        childPhoto <- map["ChildPhoto"]
        className <- map["ClassName"]
        designation <- map["Designation"]
        mobile <- map["Mobile"]
        name <- map["Name"]
        photo <- map["Photo"]
        receiverID <- map["ReceiverID"]
        receiverType <- map["ReceiverType"]
        isSelect <- map["isSelect"]
        
    }
}

class SMSTypeList : Mappable {
    
    var TypeID : Int?
    var subject : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        TypeID <- map["TypeID"]
        subject <- map["Subject"]
    }
}

class Media : Mappable {
    
    var browseImg : Bool?
    var browseAudio : Bool?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        browseImg <- map["BrowseImg"]
        browseAudio <- map["BrowseAudio"]
    }
}
