//  ConversationDetailsModel.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 30/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class ConversationDetailsModel : Mappable{
    
    var errorCode  : Int?
    var message    : String?
    var msgDTL     : [MsgDTL]?
    var msgID      : Int?
    var recipients : [Recipient]?
    var status     : String?
    var subject    : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode  <- map["ErrorCode"]
        message    <- map["Message"]
        msgDTL     <- map["MsgDTL"]
        msgID      <- map["MsgID"]
        recipients <- map["Recipients"]
        status     <- map["Status"]
        subject    <- map["Subject"]
    }
}
//class Recipient : Mappable{
//
//    var childName    : AnyObject?
//    var className    : String?
//    var designation  : AnyObject?
//    var name         : String?
//    var photo        : String?
//    var receiverID   : Int?
//    var receiverType : Int?
//    var isSelect     : Bool?
//
//    required init?(map: Map){}
//
//    func mapping(map: Map)
//    {
//        childName    <- map["ChildName"]
//        className    <- map["ClassName"]
//        designation  <- map["Designation"]
//        name         <- map["Name"]
//        photo        <- map["Photo"]
//        receiverID   <- map["ReceiverID"]
//        receiverType <- map["ReceiverType"]
//        isSelect     <- map["isSelect"]
//    }
//}
class MsgDTL :  Mappable{
    
    var body      : String?
    var filePath  : AnyObject?
    var msgID     : Int?
    var msgType   : Int?
    var rplID     : Int?
    var senderDTL : SenderDTL?
    var sentOn    : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        body      <- map["Body"]
        filePath  <- map["FilePath"]
        msgID     <- map["MsgID"]
        msgType   <- map["MsgType"]
        rplID     <- map["RplID"]
        senderDTL <- map["SenderDTL"]
        sentOn    <- map["SentOn"]
    }
}
//class SenderDTL :  Mappable{
//    
//    var childName   : AnyObject?
//    var className   : String?
//    var designation : AnyObject?
//    var name        : String?
//    var photo       : String?
//    var senderID    : Int?
//    var senderType  : Int?
//    
//    required init?(map: Map){}
//    
//    func mapping(map: Map)
//    {
//        childName   <- map["ChildName"]
//        className   <- map["ClassName"]
//        designation <- map["Designation"]
//        name        <- map["Name"]
//        photo       <- map["Photo"]
//        senderID    <- map["SenderID"]
//        senderType  <- map["SenderType"]
//    }
//}
