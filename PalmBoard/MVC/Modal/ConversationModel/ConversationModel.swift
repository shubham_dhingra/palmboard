//  ConversationModel.swift
//  e-Care Pro
//  Created by FTC on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class ConversationModel : Mappable{
    
    var errorCode    : Int?
    var message      : String?
    var status       : String?
    var conversation : [Conversation]?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        conversation <- map["Conversation"]
        errorCode    <- map["ErrorCode"]
        message      <- map["Message"]
        status       <- map["Status"]
    }
}
class Conversation :  Mappable{
    
    var abbreviation : String?
    var msgID        : Int?
    var msgType      : Int?
    var recipients   : [Recipient]?
    var senderDTL    : SenderDTL?
    var sentOn       : String?
    var subject      : String?
    var isReplyMsg   : Int?
 
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        abbreviation <- map["Abbreviation"]
        msgID        <- map["MsgID"]
        msgType      <- map["MsgType"]
        recipients   <- map["Recipients"]
        senderDTL    <- map["SenderDTL"]
        sentOn       <- map["SentOn"]
        subject      <- map["Subject"]
        isReplyMsg   <- map["isReplyMsg"]
    }
}
class SenderDTL : Mappable{
    
    var childName   : AnyObject?
    var className   : AnyObject?
    var designation : String?
    var name        : String?
    var photo       : String?
    var senderID    : Int?
    var senderType  : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName   <- map["ChildName"]
        className   <- map["ClassName"]
        designation <- map["Designation"]
        name        <- map["Name"]
        photo       <- map["Photo"]
        senderID    <- map["SenderID"]
        senderType  <- map["SenderType"]
    }
}
class Recipient : Mappable{
    
    var childName    : String?
    var className    : String?
    var designation  : String?
    var name         : String?
    var photo        : String?
    var receiverID   : Int?
    var receiverType : Int?
    var isSelect     : Bool?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName    <- map["ChildName"]
        className    <- map["ClassName"]
        designation  <- map["Designation"]
        name         <- map["Name"]
        photo        <- map["Photo"]
        receiverID   <- map["ReceiverID"]
        receiverType <- map["ReceiverType"]
        isSelect     <- map["isSelect"]
    }
}
