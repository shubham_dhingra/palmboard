
//
//  SchoolDetailModal.swift
//  e-Care Pro
//
//  Created by Shubham on 23/08/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import  ObjectMapper


class SchoolDetail : Mappable {
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
    
    }
    var schoolCode : String?
    var schoolName : String?
    var schoolLogo : String?
    var schoolCity : String?
    var eSmartGuardActive : Int32?
    var eCareActive : Int32?
    var countryCode : Int32?
    var themeColor : String?
    var isCurrentSchool : Int32?
    
    init(schoolCode_ : String? , schoolName_ : String? , schoolLogo_ : String? , schoolCity_ : String? , eSmartGuardActive_ : Int32? , eCareActive_ : Int32? , countryCode_ : Int32? , themeColor_ : String?, isCurrentSchool_ : Int32?) {
        
         schoolCode = schoolCode_
         schoolLogo = schoolLogo_
         schoolName = schoolName_
         schoolCity = schoolCity_
         eSmartGuardActive = eSmartGuardActive_
         eCareActive = eCareActive_
         countryCode = countryCode_
         themeColor =  themeColor_
         isCurrentSchool = isCurrentSchool_
    }
}

