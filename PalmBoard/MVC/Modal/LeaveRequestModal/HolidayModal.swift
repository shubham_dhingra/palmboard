//
//  HolidayModal.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class HolidayList : Mappable{
    
    var holiday : [Holiday]?
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        holiday <- map["Holiday"]
    }
}

class Holiday : Mappable{
    
    var duration : Int?
    var fromDate : String?
    var tillDate : String?
    var title : String?


    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        duration <- map["Duration"]
        fromDate <- map["FromDate"]
        tillDate <- map["TillDate"]
        title <- map["Title"]
    }
}

