//
//  LeaveDataModal.swift
//  e-Care Pro
//
//  Created by NupurMac on 26/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class LeaveList :  Mappable {
    
    var actionOn : String?
    var applicantName : String?
    var applicantPhoto : String?
    var designation : String?
    var duration : Double?
    var fromDate : String?
    var lvID : Int?
    var photo : String?
    var reason : String?
    var sID : Int?
    var studentName : String?
    var studentPhoto : String?
    var studentClass : String?
    var submittedOn : String?
    var teacherName : String?
    var tillDate : String?
    var status : String?
    var leaveType : String?
    var leaveAbbrv : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        actionOn <- map["ActionOn"]
        applicantName <- map["ApplicantName"]
        applicantPhoto <- map["ApplicantPhoto"]
        designation <- map["Designation"]
        duration <- map["Duration"]
        fromDate <- map["FromDate"]
        lvID <- map["LvID"]
        photo <- map["Photo"]
        reason <- map["Reason"]
        sID <- map["SID"]
        studentName <- map["StudentName"]
        studentPhoto <- map["StudentPhoto"]
        studentClass <- map["StudentClass"]
        submittedOn <- map["SubmittedOn"]
        teacherName <- map["TeacherName"]
        tillDate <- map["TillDate"]
        status <- map["status"]
        leaveType <- map["LeaveType"]
        leaveAbbrv <- map["LeaveAbbr"]
    }
}
