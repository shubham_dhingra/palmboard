//
//  LeaveDetailsModal.swift
//  e-Care Pro
//
//  Created by NupurMac on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class LeaveDetailsModel  : Mappable{
    
    var errorCode : Int?
    var holidayList : HolidayList?
    var leaveReason : LeaveReason?
    var leaveTerms : LeaveTerm?
    var message : String?
    var serverDate : String?
    var status : String?
    var termCondition : TermCondition?
    var leaveList : [LeaveList]?
    var leaveTypeArr : [LeaveType]?
    
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        errorCode <- map["ErrorCode"]
        holidayList <- map["HolidayList"]
        leaveReason <- map["LeaveReason"]
        leaveTerms <- map["LeaveTerms"]
        message <- map["Message"]
        serverDate <- map["ServerDate"]
        status <- map["Status"]
        termCondition <- map["TermCondition"]
        leaveList <- map["DTL"]
        leaveTypeArr <- map["LeaveDetails"]
        
    }
}

class TermCondition : Mappable{
    
    var terms : String?
    var notes : String?
    var rules : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        terms <- map["TC"]
        notes <- map["Notes"]
        rules <- map["Rules"]
    }
}


class LeaveTerm :  Mappable{
    
    var backwardDays : Int?
    var daysLimit : Int?
    var forwardDays : Int?
    var weekOff : [String]?
    var isPrevDatesAllow : Int?
    
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        backwardDays <- map["BackwardDays"]
        daysLimit <- map["DaysLimit"]
        forwardDays <- map["ForwardDays"]
        weekOff <- map["WeekOff"]
        isPrevDatesAllow <- map["isPrevDatesAllow"]
        
    }
}

class LeaveReason :  Mappable{
    
    var reason : [String]?
    var isSelected : Bool?
    required init?(map: Map){}
    
    func mapping(map: Map){
        reason <- map["Reason"]
        isSelected = false
    }
}


class LeaveType : Mappable {
    
    var leaveId : Int?
    var name : String?
    var LeaveAbbr : String?
    var TotalLeave : Double?
    var TotalTaken : Double?
    var isExpand : Bool = false
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        leaveId  <- map["LeaveID"]
        name  <- map["LeaveType"]
        LeaveAbbr  <- map["LeaveAbbr"]
        TotalLeave  <- map["Total"]
        TotalTaken <- map["Taken"]
        
    }
}


class HalfDayModal : Mappable {
   
    var leaveDate : Date?
    var halfType : Int? = -1
    var leaveDateForBackend : String?
    
    init(leaveDate_ : Date?) {
        leaveDate = leaveDate_
        halfType = -1
        leaveDateForBackend = leaveDate_?.dateToString(formatType: "yyyy-MM-dd")
    }
    
    init() {
    }
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        leaveDate  <- map["leaveDate"]
        halfType  <- map["HalfdayType"]
        leaveDateForBackend <- map["HalfDayOn"]
    }
}


