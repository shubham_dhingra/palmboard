//  MainProfileModel.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 01/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import Foundation
import ObjectMapper

class MainProfileModel : Mappable{
    
    var details         : Detail?
    var errorCode       : Int?
    var message         : String?
    var status          : String?
    var myProfile       : Detail?
    var name            : String?
    var Photo           : String?
    var isVerified      : Bool?
    var userDTL         : UserDTL?
    var isAuthenticated : Bool?
    var isExists        : Bool?
    var currentUserType : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        details         <- map["Details"]
        errorCode       <- map["ErrorCode"]
        message         <- map["Message"]
        status          <- map["Status"]
        myProfile       <- map["Profile"]
        name            <- map["Name"]
        Photo           <- map["Photo"]
        isVerified      <- map["isVerified"]
        userDTL         <- map["UserDTL"]
        isAuthenticated <- map["isAuthenticated"]
        isExists        <- map["isExists"]
        currentUserType <- map["UserType"]
    }
}
class Detail : Mappable{
    
    var alternateMobile          : String?
    var additionalMobile         : String?
    var address                  : String?
    var admissionNo              : String?
    var billNumber               : String?
    var bloodGroup               : String?
    var className                : String?
    var contactEmailID           : String?
    var contactMobile            : String?
    var contactMob               : String?
    var contactPerson            : String?
    var coverImg                 : String?
    var admissionDate            : String?
    var dOB                      : String?
    var fatherDOB                : String?
    var fatherDesignation        : String?
    var fatherEmail1             : String?
    var fatherEmail2             : String?
    var fatherMob1               : String?
    var fatherMob2               : String?
    var fatherName               : String?
    var fatherOfficeAddress      : String?
    var fatherProfession         : String?
    var fatherResidentialAddress : String?
    var gender                   : String?
    var motherDOB                : String?
    var motherDesignation        : String?
    var motherEmail1             : String?
    var motherEmail2             : String?
    var motherMob1               : String?
    var motherMob2               : String?
    var motherName               : String?
    var motherOfficeAddress      : String?
    var motherProfession         : String?
    var motherResidentialAddress : String?
    var name                     : String?
    var nationality              : String?
    var parentAnniversaryDate    : String?
    var photo                    : String?
    var religion                 : String?
    var transport                : String?
    var stContact                : String?
    var Username                 : String?
    //
    var frContact                : String?
    var mrContact                : String?
    var designation              : String?
    var mobile                   : String?
    var emailID                  : String?
    var fatherHusbandMob         : String?
    var maritalStatus            : String?
    var p_Address                : String?
    var dOAnniversary            : String?
    var fatherHusbandName        : String?
    var dOJ                      : String?
    var emergencyContactNo       : String?
    var socialMedia              : SocialMedia?
    var userImgReq               : UserImgReq?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        alternateMobile          <- map["AlternateMobile"]
        additionalMobile         <- map["AdditionalMobile"]
        address                  <- map["Address"]
        admissionNo              <- map["AdmissionNo"]
        billNumber               <- map["BillNumber"]
        bloodGroup               <- map["BloodGroup"]
        className                <- map["ClassName"]
        contactEmailID           <- map["ContactEmailID"]
        contactMobile            <- map["ContactMobile"]
        contactMob               <- map["ContactMob"]
        contactPerson            <- map["ContactPerson"]
        coverImg                 <- map["CoverImg"]
        admissionDate            <- map["AdmissionDate"]
        dOB                      <- map["DOB"]
        fatherDOB                <- map["FatherDOB"]
        fatherDesignation        <- map["FatherDesignation"]
        fatherEmail1             <- map["FatherEmail_1"]
        fatherEmail2             <- map["FatherEmail_2"]
        fatherMob1               <- map["FatherMob_1"]
        fatherMob2               <- map["FatherMob_2"]
        fatherName               <- map["FatherName"]
        stContact                <- map["StContact"]
        fatherOfficeAddress      <- map["FatherOfficeAddress"]
        fatherProfession         <- map["FatherProfession"]
        fatherResidentialAddress <- map["FatherResidentialAddress"]
        gender                   <- map["Gender"]
        motherDOB                <- map["MotherDOB"]
        motherDesignation        <- map["MotherDesignation"]
        motherEmail1             <- map["MotherEmail_1"]
        motherEmail2             <- map["MotherEmail_2"]
        motherMob1               <- map["MotherMob_1"]
        motherMob2               <- map["MotherMob_2"]
        motherName               <- map["MotherName"]
        motherOfficeAddress      <- map["MotherOfficeAddress"]
        motherProfession         <- map["MotherProfession"]
        motherResidentialAddress <- map["MotherResidentialAddress"]
        name                     <- map["Name"]
        nationality              <- map["Nationality"]
        parentAnniversaryDate    <- map["ParentAnniversaryDate"]
        photo                    <- map["Photo"]
        religion                 <- map["Religion"]
        transport                <- map["Transport"]
        Username                 <- map["Username"]
        //
        frContact                 <- map["FrContact"]
        mrContact                 <- map["MrContact"]
        designation               <- map["Designation"]
        mobile                    <- map["Mobile"]
        emailID                   <- map["EmailID"]
        fatherHusbandMob          <- map["FatherHusbandMob"]
        maritalStatus             <- map["MaritalStatus"]
        p_Address                 <- map["P_Address"]
        dOAnniversary             <- map["DOAnniversary"]
        fatherHusbandName         <- map["FatherHusbandName"]
        dOJ                       <- map["DOJ"]
        emergencyContactNo        <- map["EmergencyContactNo"]
        socialMedia               <- map["SocialMedia"]
        userImgReq                <- map["UserImgReq"]
    }
}


class UserDTL : Mappable {
    
    var childName   : String?
    var className   : String?
    var designation : String?
    var name        : String?
    var photo       : String?
    var roleName    : String?
    var userID      : Int?
    var userType    : Int?
    var username    : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName <- map["ChildName"]
        className <- map["ClassName"]
        designation <- map["Designation"]
        name <- map["Name"]
        photo <- map["Photo"]
        roleName <- map["RoleName"]
        userID <- map["UserID"]
        userType <- map["UserType"]
        username <- map["Username"]
    }
}
class SocialMedia : Mappable{
    
    var linkedIn : String?
    var twitter : String?
    var facebook : String?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        linkedIn <- map["LinkedIn"]
        twitter  <- map["Twitter"]
        facebook <- map["facebook"]
    }
}
class UserImgReq :  Mappable{
    
    var coverImg   : Int?
    var profileImg : Int?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        coverImg   <- map["CoverImg"]
        profileImg <- map["ProfileImg"]
    }
}
