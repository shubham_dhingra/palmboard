
//
//  StudentListModal.swift
//  PalmBoard
//
//  Created by Shubham on 06/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class StudentListModal : Mappable{
    
    var classID : Int?
    var errorCode : Int?
    var message : String?
    var sMSType : Int?
    var sMSTemp : String?
    var status : String?
    var studentList : [Student]?
    var hasMarked : Bool?
    var hasSMSSent : Bool?
    var subjects : [Subjects]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        classID <- map["ClassID"]
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        sMSType <- map["SMSType"]
        sMSTemp <- map["SMS_Temp"]
        status <- map["Status"]
        studentList <- map["StudentList"]
        hasMarked <- map["hasMarked"]
        hasSMSSent <- map["hasSMSSent"]
        subjects <- map["Subjects"]
    }
}

class Student :  Mappable{
    
    var contactMob : String?
    var otherDTL : [OtherDTL]?
    var photo : String?
    var stID : Int?
    var stName : String?
    var status : Int?
    var constant : Int?
    var templateForAbsentee : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        contactMob <- map["ContactMob"]
        otherDTL <- map["OtherDTL"]
        photo <- map["Photo"]
        stID <- map["StID"]
        stName <- map["StName"]
        status <- map["Status"]
        constant <- map["isConstant"]
        templateForAbsentee = ""
    }
}

class OtherDTL : Mappable {
    
    var key : String?
    var value : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        key <- map["Key"]
        value <- map["Value"]
    }
}


class Subjects : Mappable {
    
    var SubID : Int?
    var SubjectName : String?
    var ShortName : String?
    var isSelected : Bool = false
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        SubID <- map["SubID"]
        SubjectName <- map["SubjectName"]
        ShortName <- map["ShortName"]
    }
    
    
}
