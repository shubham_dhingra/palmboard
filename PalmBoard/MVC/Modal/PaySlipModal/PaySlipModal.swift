//  PaySlipModal.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 04/04/19.
//  Copyright © 2019 Franciscan. All rights reserved.


import Foundation
import ObjectMapper

class PaySlipModal : Mappable{
    
    var errorCode : Int?
    var message : String?
    var status : String?
    var years : [Year]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        status <- map["Status"]
        years <- map["Years"]
    }
}
class Year : Mappable{
    
    var monthlyPaySlip : [MonthlyPaySlip]?
    var year : Int?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        monthlyPaySlip <- map["MonthlyPaySlip"]
        year <- map["Year"]
    }
}
class MonthlyPaySlip :  Mappable{
    
    var filePath : String?
    var month : String?
    var monthID : Int?
    var protectedFilePath : String?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        filePath <- map["FilePath"]
        month <- map["Month"]
        monthID <- map["MonthID"]
        protectedFilePath <- map["ProtectedFilePath"]
    }
}
