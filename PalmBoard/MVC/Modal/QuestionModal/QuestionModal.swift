//
//  QuestionModal.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 06/05/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import Foundation
import ObjectMapper

class QuestionModal :  Mappable{
    
    var errorCode : Int?
    var list      : [ListQuestion]?
    var message   : String?
    var status    : String?
    var updatedOn : String?
    required init?(map: Map){}
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        list      <- map["List"]
        message   <- map["Message"]
        status    <- map["Status"]
        updatedOn <- map["UpdatedOn"]
    }
}
class ListQuestion :  Mappable{
    
    var likes       : Int?
    var photo       : String?
    var qID         : Int?
    var qType       : Int?
    var que         : String?
    var queImg      : String?
    var totalAnswer : Int?
    var updatedBy   : String?
    var updatedOn   : String?
    var userID      : Int?
    var userType    : Int?
    var isAnswered  : Int?
    var isILike     : Int?
    var isReported  : Int?
    var isVerified  : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        likes           <- map["Likes"]
        photo           <- map["Photo"]
        qID             <- map["QID"]
        qType           <- map["QType"]
        que             <- map["Que"]
        queImg          <- map["QueImg"]
        totalAnswer     <- map["TotalAnswer"]
        updatedBy       <- map["UpdatedBy"]
        updatedOn       <- map["UpdatedOn"]
        userID          <- map["UserID"]
        userType        <- map["UserType"]
        isAnswered      <- map["isAnswered"]
        isILike         <- map["isILike"]
        isReported      <- map["isReported"]
        isVerified      <- map["isVerified"]
    }
}
