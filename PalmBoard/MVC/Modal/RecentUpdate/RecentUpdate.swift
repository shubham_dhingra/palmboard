import Foundation
import ObjectMapper

class RecentUpdate : Mappable {
    
    var errorCode : Int?
    var message   : String?
    var status    : String?
    var total     : Int?
    var aws_path   : String?
    var updates   : [Update]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message   <- map["Message"]
        status    <- map["Status"]
        total     <- map["Total"]
        updates   <- map["Updates"]
        aws_path   <- map["AWS_Path"]
    }
}
class Update :  Mappable{
    
    var caption            : String?
    var hasAttachment      : Bool?
    var iD                 : Int?
    var mdlID              : Int?
    var module             : String?
    var msgDTLs             : MsgDTLS?
    var galleryUpdate      : GalleryUpdate?
    var updtedOn           : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        caption            <- map["Caption"]
        galleryUpdate      <- map["GalleryUpdate"]
        hasAttachment      <- map["HasAttachment"]
        iD                 <- map["ID"]
        mdlID              <- map["MdlID"]
        module             <- map["Module"]
        msgDTLs            <- map["MsgDTL"]
        updtedOn           <- map["UpdtedOn"]
    }
}
class MsgDTLS : Mappable{
    
    var filePath              : String?
    var msgType               : Int?
    var senderDTL             : SenderDTLs?
    var subject               : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        filePath              <- map["FilePath"]
        msgType               <- map["MsgType"]
        senderDTL             <- map["SenderDTL"]
        subject               <- map["Subject"]
    }
}
class GalleryUpdate :  Mappable{
    
    var fileNames : [String]?
    var sMdlID    : Int?
    var subModule : Int?
    var total     : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        fileNames <- map["FileNames"]
        sMdlID    <- map["S_MdlID"]
        subModule <- map["SubModule"]
        total     <- map["Total"]
    }
}
class SenderDTLs : Mappable{
    
    var childName   : String?
    var className   : String?
    var designation : String?
    var name        : String?
    var photo       : String?
    var senderID    : Int?
    var senderType  : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        childName       <- map["ChildName"]
        className       <- map["ClassName"]
        designation     <- map["Designation"]
        name            <- map["Name"]
        photo           <- map["Photo"]
        senderID        <- map["SenderID"]
        senderType      <- map["SenderType"]
    }
}
