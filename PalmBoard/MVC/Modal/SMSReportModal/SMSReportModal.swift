//  SMSReportModal.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class SMSReportModal : Mappable{
    
    var errorCode : Int?
    var message :   String?
    var sMSs :      [SMS]?
    var status :    String?
    var SMSType :   [SMSType]?

    var AttendanceReport: [AttReport]?
    var MyClasse: [MyClasse]?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message   <- map["Message"]
        sMSs      <- map["SMSs"]
        status    <- map["Status"]
        SMSType   <- map["SMSType"]
        AttendanceReport <- map["AttReport"]
        MyClasse  <- map["MyClasses"]
    }
}

class SMSType : Mappable{
    
    var TypeID  : Int?
    var Subject : String?
    
    required init?(map: Map) {
        
    }
    
    init (typeID_ : Int? , subject_ : String?){
        TypeID  = typeID_
        Subject = subject_
    }
    
    func mapping(map: Map)
    {
        TypeID  <- map["TypeID"]
        Subject <- map["Subject"]
    }

}
class SMS : Mappable{
    
    var designation : String?
    var photo :       String?
    var receiver :    Receiver?
    var sMSType :     String?
    var senderName :  String?
    var sentOn :      String?
    var status :      String?
    var text :        String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        designation <- map["Designation"]
        photo       <- map["Photo"]
        receiver    <- map["Receiver"]
        sMSType     <- map["SMSType"]
        senderName  <- map["SenderName"]
        sentOn      <- map["SentOn"]
        status      <- map["Status"]
        text        <- map["Text"]
    }
}
    
    class Receiver : Mappable{
        
        var childName    : String?
        var ClassName    : String?
        var designation  : String??
        var name         : String?
        var photo        : String?
        var receiverID   : Int?
        var receiverType : Int?
        var isSelect     : Bool?
        
        required init?(map: Map){}
        
        func mapping(map: Map)
        {
            childName    <- map["ChildName"]
            ClassName    <- map["ClassName"]
            designation  <- map["Designation"]
            name         <- map["Name"]
            photo        <- map["Photo"]
            receiverID   <- map["ReceiverID"]
            receiverType <- map["ReceiverType"]
            isSelect     <- map["isSelect"]
            
        }
}

