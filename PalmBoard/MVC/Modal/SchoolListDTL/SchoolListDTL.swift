
//
//  SchoolListDTL.swift
//  eSmartGuard
//
//  Created by Shubham on 11/09/18.
//  Copyright © 2018 Franciscan Solutions Pvt. Ltd. All rights reserved.
//

import Foundation
import ObjectMapper

class SchoolListDTL : Mappable {
    
    var errCode : Int?
    var list : [SchoolList]?
    var msg : String?
    var status : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errCode <- map["ErrCode"]
        list <- map["List"]
        msg <- map["Msg"]
        status <- map["Status"]
        
    }
}

class SchoolList : Mappable{
    
    var address : String?
    var city : String?
    var logo : String?
    var name : String?
    var schoolCode : String?
    var state : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        address <- map["Address"]
        city <- map["City"]
        logo <- map["Logo"]
        name <- map["Name"]
        schoolCode <- map["SchoolCode"]
        state <- map["State"]
    }
}

