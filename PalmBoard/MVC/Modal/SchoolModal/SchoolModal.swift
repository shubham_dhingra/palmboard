
//
//  SchoolModal.swift
//  e-Care Pro
//
//  Created by ShubhamMac on 18/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper


class SchoolModal : Mappable{
    
    var active : Int?
    var additionalMobileURL : String?
    var marksEntryURL : String?
    var city : String?
    var contactEmail : String?
    var countryCode : Int?
    var errorCode : Int?
    var feePayemtURL : String?
    var feeReportURL : String?
    var logo : String?
    var logoNScName : String?
    var logoScName : String?
    var message : String?
    var packID : Int?
    var packeage : String?
    var plmQuiz : Int?
    var sMSPrvdID : Int?
    var schAdd1 : String?
    var schAdd2 : String?
    var schoolCode : String?
    var schoolName : String?
    var slider : [Slider]?
    var status : String?
    var supportEmail : String?
    var supportPhone : String?
    var themColor : String?
    var webSite : String?
   
    required init?(map: Map){}
  
    func mapping(map: Map)
    {
        active <- map["Active"]
        additionalMobileURL <- map["AdditionalMobileURL"]
        city <- map["City"]
        contactEmail <- map["ContactEmail"]
        countryCode <- map["CountryCode"]
        errorCode <- map["ErrorCode"]
        feePayemtURL <- map["FeePayemtURL"]
        marksEntryURL <- map["MarksEntryURL"]

        feeReportURL <- map["FeeReportURL"]
        logo <- map["Logo"]
        logoNScName <- map["LogoNScName"]
        logoScName <- map["LogoScName"]
        message <- map["Message"]
        packID <- map["PackID"]
        packeage <- map["Packeage"]
        plmQuiz <- map["PlmQuiz"]
        sMSPrvdID <- map["SMS_Prvd_ID"]
        schAdd1 <- map["SchAdd_1"]
        schAdd2 <- map["SchAdd_2"]
        schoolCode <- map["SchoolCode"]
        schoolName <- map["SchoolName"]
        slider <- map["Slider"]
        status <- map["Status"]
        supportEmail <- map["SupportEmail"]
        supportPhone <- map["SupportPhone"]
        themColor <- map["ThemColor"]
        webSite <- map["WebSite"]
        
    }
}

class Slider : Mappable{
    
    var descriptionField : String?
    var imgPath : String?
    var module : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        descriptionField <- map["Description"]
        imgPath <- map["ImgPath"]
        module <- map["Module"]
    }
}
