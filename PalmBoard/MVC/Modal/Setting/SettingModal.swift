import Foundation
import ObjectMapper

class SettingModal :  Mappable{
    
    var errorCode  : Int?
    var message    : String?
    var moduleList : [ModuleList]?
    var status     : String?
    var tokenKey   : String?

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode  <- map["ErrorCode"]
        message    <- map["Message"]
        moduleList <- map["ModuleList"]
        status     <- map["Status"]
        tokenKey   <- map["TokenKey"]
    }
}

class ModuleList : Mappable{
    
    var mdlID    : Int?
    var module   : String?
    var isActive : Bool?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        mdlID    <- map["MdlID"]
        module   <- map["Module"]
        isActive <- map["isActive"]
    }
}
