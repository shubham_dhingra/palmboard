

//
//  SurveyModal.swift
//  PalmBoard
//
//  Created by Shubham on 11/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class SurveyModal :  Mappable{
    
    var surveyList : [Survey]?
    var errorCode : Int?
    var message : String?
    var status : String?
    var total : Int?
    var title : String?
    var SurveyQuestions : [SurveyQuestions]?
    var SurID   : Int?
    var SchoolCode : String?
    var key : String?
    var UserID : Int?
    var UserType : Int?
    
    required init?(map: Map){}
    func mapping(map: Map){
        surveyList <- map["AllSurvey"]
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        status <- map["Status"]
        total <- map["Total"]
        title <- map["title"]
        SurveyQuestions <- map["Questions"]
        SurID <- map["SurID"]
        SchoolCode <- map["SchoolCode"]
        key <- map["key"]
        UserID <- map["UserID"]
        UserType <- map["UserType"]
        SurID <- map["SurID"]
    }
}


class Survey :  Mappable{
    
    var descriptionField : String?
    var openEndDate : String?
    var publishedOn : String?
    var respondedOn : String?
    var resultDeclared : Bool?
    var surID : Int?
    var title : String?
    var isOpen : Bool?
    var isResponded : Bool?
    var isExpanded : Bool = false
    
    required init?(map: Map){}
    func mapping(map: Map)
    {
        descriptionField <- map["Description"]
        openEndDate <- map["OpenEndDate"]
        publishedOn <- map["PublishedOn"]
        respondedOn <- map["RespondedOn"]
        resultDeclared <- map["ResultDeclared"]
        surID <- map["SurID"]
        title <- map["Title"]
        isOpen <- map["isOpen"]
        isResponded <- map["isResponded"]
    }
}


class SurveyQuestions : Mappable{
    
    var isAnsMandatory : Bool?
    var options : [Option]?
    var queID : Int?
    var question : String?
    var isMultiSelect : Bool?
    var response : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        isAnsMandatory <- map["IsAnsMandatory"]
        options <- map["Options"]
        queID <- map["QueID"]
        question <- map["Question"]
        isMultiSelect <- map["isMultiSelect"]
        response <- map["Response"]
        
    }
}


class Option : Mappable{
    
    var optID : Int?
    var option : String?
    var isSelected : Bool?
    var surveyUserDTL : [SurveyUserDTL]?
    var response : Int?
    var assignColor : UIColor?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        optID <- map["OptID"]
        option <- map["Option"]
        isSelected <- map["isSelected"]
         surveyUserDTL <- map["ResDTL"]
        response <- map["Response"]
        assignColor <- map["assignColor"]

    }
}
