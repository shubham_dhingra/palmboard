//
//  SurveyPieChartModal.swift
//  PalmBoard
//
//  Created by Shubham on 15/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class SurveyPieChartModal : Mappable{
    
    var errorCode : Int?
    var message : String?
    var options : [Option]?
    var surveyUserDTL : [SurveyUserDTL]?
    var status : String?
    var total : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        options <- map["Options"]
        surveyUserDTL <- map["PieResponse"]
        status <- map["Status"]
        total <- map["Total"]
        
    }
}

class SurveyUserDTL : Mappable{
    
    var female : Int?
    var male : Int?
    var name : String?
    var total : Int?
    var usertype : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        female <- map["Female"]
        male <- map["Male"]
        name <- map["Name"]
        total <- map["Total"]
        usertype <- map["Usertype"]
        
    }
    
}
