//  AllSyllabusModal.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 22/04/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import Foundation
import ObjectMapper
class AllSyllabusModal :  Mappable{
    
    var errorCode   : Int?
    var message     : String?
    var status      : String?
    var syllabusLST : [SyllabusLST]?
    var syllabus    : [Syllabus]?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode   <- map["ErrorCode"]
        message     <- map["Message"]
        status      <- map["Status"]
        syllabusLST <- map["SyllabusLST"]
        syllabus    <- map["Syllabus"]
    }
}
class SyllabusLST :  Mappable{
    
    var filePath : String?
    var fileSize : Float?
    var subject  : String?
    var title    : String?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        filePath <- map["FilePath"]
        fileSize <- map["FileSize"]
        subject  <- map["Subject"]
        title    <- map["Title"]
    }
}
class Syllabus :  Mappable{
    
    var classID     : Int?
    var className   : String?
    var filePath    : String?
    var syllabusLST : [SyllabusLST]?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        classID     <- map["ClassID"]
        className   <- map["ClassName"]
        filePath    <- map["FilePath"]
        syllabusLST <- map["SyllabusLST"]
    }
}
