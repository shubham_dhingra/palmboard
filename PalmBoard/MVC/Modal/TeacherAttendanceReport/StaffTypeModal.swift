//  StaffTypeModal.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 04/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import Foundation
import ObjectMapper

class StaffTypeModal :  Mappable{
    
    var errorCode : Int?
    var message   : String?
    var staffType : [StaffType]?
    var status    : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message   <- map["Message"]
        staffType <- map["StaffType"]
        status    <- map["Status"]
    }
}
class StaffType :  Mappable{
    
    var staffTypeID : Int?
    var staffType   : String?
    var isSelected  : Bool = false

    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        staffTypeID <- map["StaffTypeID"]
        staffType   <- map["Staff_Type"]
    }
}
