//  TeacherAtteReportModel.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 25/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class TeacherAtteReportModel : Mappable{
    
    var dTL       : [DTL]?
    var errorCode : Int?
    var message   : String?
    var status    : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        dTL       <- map["DTL"]
        errorCode <- map["ErrorCode"]
        message   <- map["Message"]
        status    <- map["Status"]
    }
}
class DTL :  Mappable{
    
    var designation : String?
    var empCode     : String?
    var empID       : Int?
    var isPrasent   : Bool?
    var lateBy      : String?
    var markOn      : [MarkOn]?
    var name        : String?
    var photo       : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        designation <- map["Designation"]
        empCode     <- map["EmpCode"]
        empID       <- map["EmpID"]
        isPrasent   <- map["IsPrasent"]
        lateBy      <- map["LateBy"]
        markOn      <- map["MarkOn"]
        name        <- map["Name"]
        photo       <- map["Photo"]
    }
}
class MarkOn : Mappable{
    
    var markOn : String?
    required init?(map: Map){}
    func mapping(map: Map)
    {
        markOn <- map["MarkOn"]
    }
}
