//
//  ThoughtsLikeModal.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 04/05/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import Foundation
import ObjectMapper

class ThoughtsLikeModal :  Mappable{
    
    var errorCode  : Int?
    var message    : String?
    var status     : String?
    var totalLikes : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode  <- map["ErrorCode"]
        message    <- map["Message"]
        status     <- map["Status"]
        totalLikes <- map["TotalLikes"]
    }
}
