//
//  ThoughtsModal.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 03/05/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import Foundation
import ObjectMapper


class ThoughtsModal : Mappable{
    
    var errorCode : Int?
    var list      : [List]?
    var message   : String?
    var status    : String?
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        list      <- map["List"]
        message   <- map["Message"]
        status    <- map["Status"]
    }
}
class List :  Mappable{
    
    var author     : String?
    var likes      : Int?
    var photo      : String?
    var quotation  : String?
    var thID       : Int?
    var updatedBy  : String?
    var userID     : Int?
    var userType   : Int?
    var isILike    : Int?
    var isReported : Int?
    var isVerified : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        author     <- map["Author"]
        likes      <- map["Likes"]
        photo      <- map["Photo"]
        quotation  <- map["Quotation"]
        thID       <- map["ThID"]
        updatedBy  <- map["UpdatedBy"]
        userID     <- map["UserID"]
        userType   <- map["UserType"]
        isILike    <- map["isILike"]
        isReported <- map["isReported"]
        isVerified <- map["isVerified"]
    }
}
