//
//  VersionModal.swift
//  e-Care Pro
//
//  Created by Shubham on 01/09/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper

class VersionModal : Mappable{
    
    var errorCode : Int?
    var message : String?
    var status : String?
    var versionData : VersionData?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        
        errorCode <- map["ErrorCode"]
        message <- map["Message"]
        status <- map["Status"]
        versionData <- map["iOS"]
    }
}

class VersionData : Mappable {
    
    var criticalVersion : String?
    var currentVersion : String?
    var descriptionField : String?
    var normalVersion : String?
    var oSType : Int?
    var releasedOn : String?
    var title : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        criticalVersion <- map["CriticalVersion"]
//        currentVersion <- map["CurrentVersion"]
        descriptionField <- map["Description"]
        normalVersion <- map["NormalVersion"]
        oSType <- map["OS_Type"]
        releasedOn <- map["ReleasedOn"]
        title <- map["Title"]
        
    }
}

