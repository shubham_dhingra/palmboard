
//
//  WeekPlanModal.swift
//  PalmBoard
//
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import Foundation
import ObjectMapper


class PlanModal : Mappable {
    
    var errorCode  : Int?
    var message    : String?
    var status     : String?
    var WPID        : Int?
    var hasCreated  : Bool?
    var total       : Int?
    var weekPlanModal : WeekPlanModal?
    var planList    : [WeeklyPlanList]?
    required init?(map: Map){}
    
    func mapping(map: Map){
        
        errorCode         <- map["ErrorCode"]
        message           <- map["Message"]
        status            <- map["Status"]
        WPID              <- map["WPID"]
        hasCreated        <- map["hasCreated"]
        weekPlanModal     <- map["PlanDTL"]
        total              <- map["Total"]
        planList            <- map["WeeklyPlanList"]
    }
}

class WeekPlanModal : Mappable {
    
    var SchoolCode : String?
    var key     : String?
    var ClassID : String?
    var SubID   : String?
    var UserID  : Int?
    var Title   : String?
    var FromDate : String?
    var EndDate    : String?
    var WPID        : Int?
    var DayPlans = [DayPlanList]()
    var editPlanDTL  = WorkTypeModal()
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        ClassID  <- map["ClassID"]
        SubID    <- map["SubID"]
        Title <- map["Title"]
        WPID <- map["WPID"]
        FromDate     <- map["FromDate"]
        EndDate        <- map["EndDate"]
        DayPlans <- map["DayPlans"]
        WPID        <- map["WPID"]
        SchoolCode <- map["SchoolCode"]
        key         <- map["key"]
        UserID      <- map["UserID"]
        editPlanDTL     <- map["PlanDTL"]
        
    }
    
    init() {
        
    }
    
    init(schoolCode_ : String? ,planForDays : Int , startDate : Date , classID: String, subID: String, planName_ : String, weekID: String) {
        
        var planIndex : Int = 0
        while planIndex < planForDays {
            let dayModal = DayPlanList.init(index: planForDays, planDate_: planIndex == 0 ? startDate : Date(timeInterval: TimeInterval(/planIndex * 86400), since: startDate))
            DayPlans.append(dayModal)
            planIndex = planIndex + 1
        }
        Title = planName_
        SubID = subID
        ClassID = classID
        key = APP_CONSTANTS.KEY
        SchoolCode = schoolCode_
    }
}

class DayPlanList : Mappable {
    
    var DayNo  : String?
    var isSkip    : Bool = false
    var PlanDate : String?
    var PlanDateForModal    : Date?
    var WorkPlans    = [WorkTypeModal]()
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        DayNo  <- map["DayNo"]
        PlanDate    <- map["PlanDate"]
        isSkip = false
        WorkPlans     <- map["WorkPlans"]
    }
    
    init(index : Int , planDate_ : Date) {
        DayNo = "Day \(index)"
        PlanDateForModal = planDate_
        
        let dateString = planDate_.dateToString(formatType :"yyyy-MM-dd")
        let formatter    = DateFormatter()
        formatter.dateFormat = dateString
        
        PlanDate = dateString
        //Get day from date work accordingly
        var typeIndex : Int = 0
        
        while typeIndex < 2 {
            let modal = WorkTypeModal.init(index: typeIndex)
            WorkPlans.append(modal)
            typeIndex = typeIndex + 1
        }
    }
}


class WorkTypeModal : Mappable {
    
    var Method : Int?
    var Topic : String?
    var Description    : String?
    var WorkType : Int?
    var ApprovedBy : String?
    var ApprovedOn : String?
    var Downloads : [String]?
    var Attachments = [AttachmentModal]()
    var isApproved : Int?
    var PlnID : Int?
    var Status : Int?
    
    required init?(map: Map){}
    
    init() {
        
    }
    
    func mapping(map: Map){
        
        Method          <- map["Method"]
        WorkType        <- map["WorkType"]
        Topic           <- map["Topic"]
        Description     <- map["Description"]
        isApproved      <- map["isApproved"]
        ApprovedBy      <- map["ApprovedBy"]
        ApprovedOn      <- map["ApprovedOn"]
        Attachments     <- map["Attachments"]
        Downloads       <- map["Downloads"]
        PlnID           <- map["PlnID"]
        Status          <- map["Status"]
    }
    
    init(index : Int) {
        Method = 0
        WorkType = index + 1
        Status = 0
    }
}


class AttachmentModal : Mappable {
    
    var Attachment : String?
    var FileExt  : String?
    var FileURL : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        Attachment  <- map["Attachment"]
        FileExt    <- map["FileExt"]
        FileURL     <- map["FileURL"]
    }
    
    init(media : MediaType , object : Any? ,fileUrlForDocx : URL? = nil) {
        
        switch media {
        case .image:
            self.Attachment = (object as? UIImage)?.convertImageToBase64()
            self.FileExt = "jpeg"
            
            
        case .pdf:
            
            if let file = fileUrlForDocx {
                do {
                    let fileData = try Data.init(contentsOf: file)
                    self.Attachment = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
                    self.FileExt = "pdf"
                }
                catch(let error){
                    print(error.localizedDescription)
                }
            }
        default:
            break
        }
    }
}

class WeeklyPlanList : Mappable {
    
    var WPID : Int?
    var Title   : String?
    var FromDate : String?
    var EndDate    : String?
    var Subject : String?
    var ClassName   : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map){
        
        WPID          <- map["WPID"]
        Title        <- map["Title"]
        FromDate     <- map["FromDate"]
        EndDate     <- map["EndDate"]
        Subject      <- map["Subject"]
        ClassName      <- map["ClassName"]
        
    }
}



