//  WeeklyPlanModelStudent.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 28/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import Foundation
import ObjectMapper

class WeeklyPlanModelStudent : Mappable{
    
    var errorCode : Int?
    var message   : String?
    var status    : String?
    var total     : Int?
    var WeeklyPlanListSchool : [WeeklyPlanListSchool]?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        errorCode <- map["ErrorCode"]
        message   <- map["Message"]
        status    <- map["Status"]
        total     <- map["Total"]
        WeeklyPlanListSchool <- map["WeeklyPlanList"]
    }
}
class WeeklyPlanListSchool :  Mappable{
    
    var className : String?
    var endDate   : String?
    var fromDate  : String?
    var subject   : String?
    var title     : String?
    var wPID      : Int?
    
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        className <- map["ClassName"]
        endDate   <- map["EndDate"]
        fromDate  <- map["FromDate"]
        subject   <- map["Subject"]
        title     <- map["Title"]
        wPID      <- map["WPID"]
    }
}
