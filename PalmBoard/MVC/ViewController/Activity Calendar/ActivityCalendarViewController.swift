//  ActivityCalendarViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class ActivityCalendarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITableViewDataSource, UITableViewDelegate, dateSelectedDelegate
{
    var btnTitle                    =  UIButton()
    
    @IBOutlet weak var tblViewActivity: UITableView?
    @IBOutlet weak var collectionViewActivity: UICollectionView?
    @IBOutlet weak var imgViewNoActivity: UIImageView?
    @IBOutlet weak var lblActivity: UILabel?
    
    @IBOutlet weak var scrollViewActivityCalendar: UIScrollView?
    @IBOutlet weak var viewStatus: UIView?
    @IBOutlet weak var viewCal: UIView?
    @IBOutlet weak var lblDate: UILabel?
    @IBOutlet weak var lblMonth: UILabel?
    @IBOutlet weak var lblLine: UILabel?
    @IBOutlet weak var lblTitleActivity: UILabel?
    @IBOutlet weak var viewCalHeight: NSLayoutConstraint?
    
    var listData :  ActivityModal!
    var strSender       = String()
    var intClassID      = Int()
    var monthIndex : Int?
    var months = [String]()
    
//    let months          = ["April", "May", "June", "July", "August", "September","October", "November","December","January", "February","March"]
    
    var eventIndex       = Int()
    var btnTogal         = UIButton()
    var boolAPI          = Bool()
    var intscrollToItem  = Int()
    var intGridListCheck = Int()
    var monthsNew        = [[String: Any]]()
    
    ///  private var month = 1
    private var year        = Int()
    private var yearStarted = Int()
    
    var arrayPresent    = [String]()
    var arrayAbsent     = [String]()
    var arrayTitleCal   = [String]()
    var resultDict     = [String:Any]()      
    var themeColor      = String()
    var sessionYearMonthWise  = [Int]()
    var isFirstTime : Bool = true
    
    lazy var sessionStartYear : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
    
    lazy var sessionNextYear : Int = {
        guard let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionEnd) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
    
    lazy var sessionStartMonth : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.month, from: Date())
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.month, from: Date())
        }
        return date.month
    }()
    
    
    lazy var noOfMonthsForParticularSession : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart , let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return 0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let sessionStartDate = dateFormatter.date(from: sessionStart) , let sessionStartEnd = dateFormatter.date(from: sessionEnd) else{
            return 0
        }
        
        return Calendar.current.dateComponents([.month], from: sessionStartDate, to: sessionStartEnd).month ?? 0
    }()
    
    // MARK: - loadView
    override func loadView() {
        super.loadView()
        sharedInstanceActivity.weekDayNamesArray = sharedInstanceActivity.returnWeekDayArray()
        //print("sharedInstanceActivity.weekDayNamesArray = \(sharedInstanceActivity.weekDayNamesArray)")
        
        lblDate?.isHidden  = true
        lblMonth?.isHidden = true
        lblLine?.isHidden  = true
        lblTitleActivity?.isHidden = true
    }
    
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monthIndex = 10
        boolAPI         = false
        
        
        intGridListCheck = 1
        
        lblDate?.layer.cornerRadius = (lblDate?.frame.width)! / 2
        lblDate?.clipsToBounds      = true
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        if let ClassId = self.getCurrentUser()?.classID {
            intClassID =  Int(ClassId)
        }
        
        
        btnTogal                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTogal.frame           = CGRect(x : 0,y:  0,width : 22, height: 22)
        btnTogal.addTarget(self, action: #selector(self.ClkBtnTogal(button:)), for: .touchUpInside)
        btnTogal.setImage(R.image.gridActivity(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnTogal)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.collectionViewActivity?.delegate   = nil
        self.collectionViewActivity?.dataSource = nil
        self.tblViewActivity?.delegate          = nil
        self.tblViewActivity?.dataSource        = nil
        
        tblViewActivity?.rowHeight          = UITableView.automaticDimension
        tblViewActivity?.estimatedRowHeight = 2000
        tblViewActivity?.tableFooterView    = UIView(frame: CGRect.zero)
    }
    
    
 
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = true
       
        
        scrollViewActivityCalendar?.isHidden = true
        themeColor = /self.getCurrentSchool()?.themColor
        
        collectionViewActivity?.backgroundColor = themeColor.hexStringToUIColor()
        lblLine?.backgroundColor     = themeColor.hexStringToUIColor()
        
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Activity Calendar", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        let date     = Date()
        let calendar = Calendar.current
        
//        let year     = calendar.component(.year, from: date)
//        let month    = calendar.component(.month, from: date)
//        let day      = calendar.component(.day, from: date)
        
        
        if isFirstTime {
           setUpMonthView()
           monthIndex = sessionStartMonth
           year = sessionStartYear
           self.isFirstTime = false
//           intscrollToItem = returnMonthIndex(intMonth: sessionStartMonth)
        }
      
        // print("year = \(year)")
        // print("month = \(month)")
        // print("day = \(day)")
        
       
        // print("monthIndex = \(monthIndex!)")
        
        // intscrollToItem = 8
        
        //print("intscrollToItem = \(intscrollToItem)")
        
        yearStarted     = year
        getActivityData(intMonth: monthIndex!, intYear: sessionStartYear)
    }
    
    
    func setUpMonthView() {
        
        
        print("month = \(self.sessionStartMonth)")
        print("year = \(self.sessionStartYear)")
        print("No of months for a particular Session = \(self.noOfMonthsForParticularSession)")
        var startMonth = sessionStartMonth
        var index = 0
        sessionYearMonthWise = []
        var YearToAppend : Int = sessionStartYear
        while(index < 12){
            if startMonth > 12 {
                startMonth = 1
                YearToAppend = sessionNextYear
            }
            months.append(GetMonth.getMonthName(index : startMonth))
            startMonth = startMonth + 1
            sessionYearMonthWise.append(YearToAppend)
            index = index + 1
        }
        print("Session Month Wise  \(sessionYearMonthWise)")
    }
    
    // MARK: - getActivityData
    func getActivityData(intMonth: Int, intYear: Int)
    {
        lblDate?.isHidden  = true
        lblMonth?.isHidden = true
        lblLine?.isHidden  = true
        lblTitleActivity?.isHidden = true
        self.collectionViewActivity?.isHidden = false
        Utility.shared.loader()
        
        imgViewNoActivity?.isHidden = true
        lblActivity?.isHidden       = true
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Academic/ActivityDTL?SchCode=\(strSender)&key=\(urlString.keyPath())&ClassID=\(intClassID)&Month=\(intMonth)&Year=\(intYear)"
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results{
                        if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0){
                            print("Result Activity== \(result)")
                            print("Result Activity== \(result.count)")
                            self.resultDict = result        //Added By : Madhur
                            if (result["Activities"] as? [[String: Any]]) != nil{
                                self.collectionViewActivity?.isHidden   = false
                                self.loadListData(dataDict: result)
                            }
                            else{
                                
                                self.loadListData(dataDict: result)
                                
                                self.imgViewNoActivity?.isHidden        = false
                                self.lblActivity?.isHidden              = false
                                self.tblViewActivity?.isHidden          = true
                                self.scrollViewActivityCalendar?.isHidden = true      //Added By : Madhur
                                
                                self.collectionViewActivity?.delegate   = self
                                self.collectionViewActivity?.dataSource = self
                                self.collectionViewActivity?.reloadData()
                                
                                
                            }
                        }
                        else{
                            self.collectionViewActivity?.isHidden   = true
                        }
                    }
                    else{
                        WebserviceManager.showAlert(message: "", title: "The internet connection appears to be offline", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                        }, secondBtnBlock: {})
                    }
            }
        }
        
    }
    
    
    // MARK: - loadListData
    func loadListData(dataDict : [String:Any])  {
        //dic:[String:Any]
        print("loadListData")
        listData = ActivityModal(dic: dataDict)//ListData(WithDict: dataDict)
        
        DispatchQueue.main.async(execute: {
            
            //  print("ListData = \(self.listData)")
            //print("ListData = \(self.listData.rpt)")
            //print("ListDataCount = \(self.listData.rpt.count)")
            
            self.arrayPresent.removeAll()
            self.arrayAbsent.removeAll()
            self.arrayTitleCal.removeAll()
            
            if self.listData.rpt.count > 0{
                
                for (index,_) in self.listData.rpt.enumerated()
                {
                    let intStatus = self.listData.rpt[index].isWorking
                    var strMonth = String()
                    var strTitleCal = String()
                    strMonth     = self.listData.rpt[index].FromDate
                    strTitleCal     = self.listData.rpt[index].Title
                    
                    let duration = self.listData.rpt[index].Duration
                    if duration == 1 {
                        let dateFormatter           = DateFormatter()
                        dateFormatter.dateFormat    = "dd/MM/yyyy"
                        let date: Date?             = dateFormatter.date(from: strMonth)
                        strMonth                    = "\(dateFormatter.string(from: date!))"
                        switch intStatus
                        {
                        case 1:
                            self.arrayPresent.append(strMonth)
                        case 0:
                            self.arrayAbsent.append(strMonth)
                        default:
                            break
                        }
                        self.arrayTitleCal.append(strTitleCal)
                    }
                        //if the duration is greater than 1
                    else {
//                        var strMonths = [String]()
//                        var strTitleCals = [String]()
                        strMonth = self.listData.rpt[index].FromDate
                        var counter : Int = 0
                        while counter < duration {
                            
                            let dateFormatter           = DateFormatter()
                            dateFormatter.dateFormat    = "dd/MM/yyyy"
                            let date: Date?             = dateFormatter.date(from: strMonth)
                            if let date = date {
                                 strMonth = "\(dateFormatter.string(from: counter == 0 ? date : date.addingTimeInterval(86400)))"
                            }
                           
                            switch intStatus
                            {
                            case 1:
                                self.arrayPresent.append(strMonth)
                            case 0:
                                self.arrayAbsent.append(strMonth)
                            default:
                                break
                            }
                            
                            self.arrayTitleCal.append(strTitleCal)
                            counter = counter + 1
                        }
                    }
                    
                }
                //            print("self.arrayPresent = \(self.arrayPresent)")
                //            print("arrayAbsent       = \(self.arrayAbsent)")
                //            print("arrayTitleCalModal= \(self.arrayTitleCal)")
                
                sharedInstanceActivity.arrayPresentModal.removeAll()
                sharedInstanceActivity.arrayAbsentModal.removeAll()
                sharedInstanceActivity.arrayTitleCalModal.removeAll()
                
                sharedInstanceActivity.arrayPresentModal  = self.arrayPresent
                sharedInstanceActivity.arrayAbsentModal   = self.arrayAbsent
                sharedInstanceActivity.arrayTitleCalModal = self.arrayTitleCal
                
                if self.listData.rpt.count == 0{
                    Utility.shared.removeLoader()
                    self.imgViewNoActivity?.isHidden        = false
                    self.lblActivity?.isHidden              = false
                    self.tblViewActivity?.isHidden          = true
                    self.collectionViewActivity?.delegate   = self
                    self.collectionViewActivity?.dataSource = self
                    self.collectionViewActivity?.reloadData()
                }
                else{
                    
                    
                    self.loadUI()
                    
                    self.collectionViewActivity?.delegate   = self
                    self.collectionViewActivity?.dataSource = self
                    self.collectionViewActivity?.reloadData()
                    
                    if self.intGridListCheck == 1{
                        self.tblViewActivity?.delegate   = self
                        self.tblViewActivity?.dataSource = self
                        self.tblViewActivity?.reloadData()
                        self.tblViewActivity?.isHidden       = false
                    }
                    else //Added By : Madhur
                    {        self.scrollViewActivityCalendar?.isHidden = false
                        self.tblViewActivity?.isHidden = true
                        self.generateCalendar()
                        
                    }
                }
            }
            else
            {
                sharedInstanceActivity.arrayPresentModal.removeAll()
                sharedInstanceActivity.arrayAbsentModal.removeAll()
                sharedInstanceActivity.arrayTitleCalModal.removeAll()
                sharedInstanceActivity.arrayPresentModal  = self.arrayPresent
                sharedInstanceActivity.arrayAbsentModal   = self.arrayAbsent
                sharedInstanceActivity.arrayTitleCalModal = self.arrayTitleCal
                
                Utility.shared.removeLoader()
                self.imgViewNoActivity?.isHidden        = false
                self.lblActivity?.isHidden              = false
                self.tblViewActivity?.isHidden          = true
                self.collectionViewActivity?.delegate   = self
                self.collectionViewActivity?.dataSource = self
                self.collectionViewActivity?.reloadData()
                self.generateCalendar()
                
                self.loadUI()
            
            }
        })
    }
    // MARK: - loadUI
    func loadUI()  {
        DispatchQueue.main.async(execute:
            {
                self.monthsNew.removeAll()
                Utility.shared.removeLoader()
                
//                var intCurrentYear = Int()
//                var intNextYear    = Int()
//                let stringArray    = self.listData.Session.components(separatedBy: "-")
//                intCurrentYear     = Int(stringArray[0])!
//                intNextYear        = Int(stringArray[1])!
                
                for index in 0..<12
                {
                    var monthsNewDict = [String: Any]()
                    monthsNewDict["MonthName"] = self.months[index]
                    monthsNewDict["Id"]        = GetMonth.getMonthIndex(monthName: self.months[index])
                    monthsNewDict["year"]      = self.sessionYearMonthWise[index]
                    self.monthsNew.append(monthsNewDict)
                }

                print("monthsNew count loadUI = \(self.monthsNew.count)")
                self.collectionViewActivity?.delegate   = self
                self.collectionViewActivity?.dataSource = self
                self.tblViewActivity?.delegate          = self
                self.tblViewActivity?.dataSource        = self
                
                self.collectionViewActivity?.reloadData()
                self.tblViewActivity?.reloadData()
                
                self.collectionViewActivity?.performBatchUpdates({},
                                                                 completion: { (finished) in
                                                                    // print("collection-view finished reload done")
                                                                    
                                                                    if !self.boolAPI{
                                                                        self.collectionViewActivity?.collectionViewLayout.invalidateLayout()
                                                                        let indexPathForFirstRow = IndexPath(row: self.intscrollToItem, section: 0)
                                                                        // self.collectionViewAttendance?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                                                        self.collectionView(self.collectionViewActivity!, didSelectItemAt: indexPathForFirstRow)
                                                                        
                                                                        self.collectionViewActivity?.scrollToItem(at: indexPathForFirstRow, at: .centeredHorizontally, animated: true)
                                                                    }
                })
                
        })
        
        //lblHeader.text    = listData.Title
        //lblSubHeader.text = listData.EventDate
    }
    // MARK: - collectionView Delegate& DataSource method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("monthsNew.count = \(monthsNew.count)")
        return monthsNew.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellActivity", for: indexPath) as! CustomActivityCollectionViewCell
        cell.lblMonthName?.text = monthsNew[indexPath.row]["MonthName"] as? String //months[indexPath.row]
        
        if eventIndex == indexPath.row {
            cell.lblMonthName?.font      =  R.font.ubuntuMedium(size: 16.0)
            cell.lblMonthName?.textColor = UIColor(white: 1, alpha: 1)
        }else {
            cell.lblMonthName?.font      =  R.font.ubuntuRegular(size: 16.0)
            cell.lblMonthName?.textColor = UIColor(white: 1, alpha: 0.42)
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 3.0) , height: 44.0)
    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: (collectionView.frame.width / 3.0) , height: 44.0)
    //    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // print("Func: didSelectItemAt = \(indexPath.row)")
        
        collectionView.setContentOffset(CGPoint.zero, animated: true)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        //eventIndex = indexPath.row
        //collectionView.reloadData()
        
        let intID        =  monthsNew[indexPath.row]["Id"] as? Int
        let intYear      = monthsNew[indexPath.row]["year"] as? Int
        intscrollToItem  = intID!
        eventIndex       = indexPath.row
        monthIndex       = intID
        
        yearStarted      = intYear!
        
        if (boolAPI){
            getActivityData(intMonth: intID!, intYear: intYear!)// getAttendanceData(intMonth: intID!, intYear: intYear!)
        }
        boolAPI = true
        self.tblViewActivity?.setContentOffset(CGPoint.zero, animated: true)
        
    }
    // MARK: - tableView Delegate& DataSource method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.rpt.count               //6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellActivity", for: indexPath) as! customActivityTableViewCell
        
        cell.lblTitle?.text = listData.rpt[indexPath.row].Title
        cell.lblLine?.backgroundColor = themeColor.hexStringToUIColor()
        cell.lblOuterCircle?.backgroundColor = themeColor.hexStringToUIColor()
        
        var strDate              = String()
        var strDay               = String()
        strDate                  = listData.rpt[indexPath.row].FromDate
        
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date1: Date?         = dateFormatter.date(from: strDate)
        strDay                   = self.daySuffix(from: date1!)
        
        if listData.rpt[indexPath.row].Duration > 1 {
            
            var fromDate              = String()
            var toDate                = String()
            var completeDate          = String()
            fromDate                  = listData.rpt[indexPath.row].FromDate
            toDate                    = listData.rpt[indexPath.row].TillDate
            let Startdate : Date?         = dateFormatter.date(from: fromDate)
            let EndDate   : Date?         = dateFormatter.date(from: toDate)
            
            guard let sDate = Startdate , let tDate = EndDate else {
                print("No date coming")
                return UITableViewCell()
            }
            
            let suffixStartDate       = self.daySuffix(from: sDate)
            let suffixEndDate         = self.daySuffix(from: tDate)
            
            dateFormatter.dateFormat = "MMM yyyy"
            completeDate = "\(/suffixStartDate) \(dateFormatter.string(from: sDate)) \n\(suffixEndDate) \(/dateFormatter.string(from: tDate))"
            cell.lblFromDate?.text    = completeDate
        }
        else {
            dateFormatter.dateFormat = "MMM yyyy, EEEE"
            cell.lblFromDate?.text    = "\(strDay) \(dateFormatter.string(from: date1!))"
            
        }
        if listData.rpt[indexPath.row].isWorking == 1
        {
            cell.lblInnerCircle?.backgroundColor = UIColor(rgb: 0x78CC41)
            cell.viewBackground?.backgroundColor  = UIColor(rgb: 0x78CC41)
            cell.lblFromDate?.textColor = UIColor(rgb: 0x78CC41)
            cell.lblHoliday?.text = ""
        }
        else if listData.rpt[indexPath.row].isWorking == 0 {
            cell.lblInnerCircle?.backgroundColor = UIColor(rgb: 0x44A8EB)
            cell.viewBackground?.backgroundColor = UIColor(rgb: 0x44A8EB)
            cell.lblFromDate?.textColor = UIColor(rgb: 0x44A8EB)
            cell.lblHoliday?.text = "(Holiday)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tblViewActivity?.dequeueReusableCell(withIdentifier: "TableSectionHeader") as! customHeaderActivity
        cell.lblHeader?.text = listData.Session
        cell.lblHeader?.textColor = themeColor.hexStringToUIColor()
        cell.lblLineVert?.backgroundColor = themeColor.hexStringToUIColor()
        cell.lblLinehoriz?.backgroundColor = themeColor.hexStringToUIColor()
        return cell
    }
    // MARK: - daySuffix method
    func daySuffix(from date: Date) -> String {
        
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "\(dayOfMonth)st"
        case 2, 22:     return "\(dayOfMonth)nd"
        case 3, 23:     return "\(dayOfMonth)rd"
        default:        return "\(dayOfMonth)th"
        }
    }
    
    
    // MARK: - returnMonthIndex method
    func returnMonthIndex(intMonth: Int) -> Int {
        switch intMonth {
        case 1:
            intscrollToItem = 9
        case 2:
            intscrollToItem = 10
        case 3:
            intscrollToItem = 11
        case 4:
            intscrollToItem = 0
        case 5:
            intscrollToItem = 1
        case 6:
            intscrollToItem = 2
        case 7:
            intscrollToItem = 3
        case 8:
            intscrollToItem = 4
        case 9:
            intscrollToItem = 5
        case 10:
            intscrollToItem = 6
        case 11:
            intscrollToItem = 7
        case 12:
            intscrollToItem = 8
            
        default:
            break
        }
        return intscrollToItem
    }
    
    
    // MARK: - aDateSelected method
    func aDateSelected ( aSelectedDate : String,dateActivity: Int, monthActivity: String, color: UIColor)
    {
        //  print("Do updates here: ")
        
        if aSelectedDate.count > 0{
            for (index, value) in listData.rpt.enumerated(){
                
                let dictvalue : RPTModelActivity = value
                
                if (dictvalue.FromDate == aSelectedDate){
                    // print("Values matched")
                    //  print(sharedInstanceActivity.arrayTitleCalModal[index])
                    lblTitleActivity?.text     = sharedInstanceActivity.arrayTitleCalModal[index]
                    break
                }
                //else{
                //             print("Values  not matched")
                //        }
            }}
        
        if color == UIColor.white
        {
            lblDate?.isHidden          = true
            lblMonth?.isHidden         = true
            lblTitleActivity?.isHidden = true
            lblLine?.isHidden          = true
        }
        else
        {
            lblDate?.text     = "\(dateActivity)"
            lblMonth?.text    = monthActivity
            lblDate?.backgroundColor =  color
            
            lblMonth?.textColor         = color
            lblTitleActivity?.textColor = color
            lblDate?.isHidden           = false
            lblMonth?.isHidden          = false
            lblTitleActivity?.isHidden  = false
            
            lblLine?.isHidden           = false
        }
    }
    
    
//    //MARK: - scrollview Delegate method
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//
//        if (scrollView == tblViewActivity){
//            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
//                parent.hideBar(boolHide: true)
//            }
//        }
//    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//
//        if (scrollView == tblViewActivity){
//            let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
//                if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
//                    parent.hideBar(boolHide: false)
//                }
//            }
//            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
//        }
//    }
    
    
    // MARK: - ClkBtnTogal method
    @objc func ClkBtnTogal(button: UIButton)
    {
        if button.hasImage(named: "gridActivity", for: .normal) {
            // print("YES")
            button.setImage(R.image.listActivity(), for: UIControl.State.normal)
            // intlist = 1
            //print("gridActivity")
            intGridListCheck = 2
            
            if (resultDict["Activities"] as? [[String: Any]]) == nil{           //Added By : Madhur
                
                Utility.shared.removeLoader()
                //  self.scrollViewActivityCalendar.isHidden = true
                tblViewActivity?.isHidden = true
                self.imgViewNoActivity?.isHidden        = false
                self.lblActivity?.isHidden              = false
            }
            else {
                self.scrollViewActivityCalendar?.isHidden = false
                self.tblViewActivity?.isHidden = true
                self.generateCalendar()
            }
            
        }
        else {
            print("NO")
            button.setImage(R.image.gridActivity(), for: UIControl.State.normal)
            //intgrid = 0
            print("listActivity")
            intGridListCheck = 1
            tblViewActivity?.isHidden = false
            //  tblViewActivity.reloadData()  //Edited by: Madhur
            
            self.loadListData(dataDict: resultDict)           //Edited by: Madhur
            
            if self.listData.rpt.count == 0{
                Utility.shared.removeLoader()
                self.imgViewNoActivity?.isHidden        = false
                self.lblActivity?.isHidden              = false
                self.tblViewActivity?.isHidden          = true
                self.collectionViewActivity?.delegate   = self
                self.collectionViewActivity?.dataSource = self
                self.collectionViewActivity?.reloadData()
                self.scrollViewActivityCalendar?.isHidden = true
                
            }
            
        }
    }
    
    
    // MARK: - generateCalendar method
    func generateCalendar()
    {
        let tempmonthTobeShown = monthIndex!
        var monthTobeShown     = tempmonthTobeShown
        
        if monthTobeShown > 12 {
            var tempMonth = monthTobeShown % 12
            if tempMonth == 0 {
                tempMonth = 12
            }
            monthTobeShown = tempMonth
        }
        
        year = yearStarted
        let numberOfYears = tempmonthTobeShown / 12 // to get year with january
        if numberOfYears > 0 {
            for _ in 0...numberOfYears-1 {
                year = year + 1
            }
        }
        
        //checking for iPhone5s
        if screenHeight == 568 {
            viewCalHeight?.constant = 275
        }
        
        if monthTobeShown == 12 {
            sharedInstanceActivity.delegate = self
            sharedInstanceActivity.createCalendar(withMonth: monthTobeShown , andYear: year-1, view: viewCal!)
        } else {
            sharedInstanceActivity.delegate = self
            sharedInstanceActivity.createCalendar(withMonth: monthTobeShown , andYear: year, view: viewCal!)
        }
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        // self.tblViewActivity.setContentOffset(CGPoint(x:0,y:0), animated: true)
        self.tblViewActivity?.setContentOffset(CGPoint.zero, animated: true)
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.tblViewActivity?.scrollToRow(at: indexPathForFirstRow, at: .top, animated: true)
        
        self.scrollViewActivityCalendar?.setContentOffset(CGPoint(x:0,y:0), animated: true)
        
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
// MARK: - customHeaderActivity
class customHeaderActivity: UITableViewCell {
    @IBOutlet weak var lblHeader:    UILabel?
    @IBOutlet weak var lblLinehoriz: UILabel?
    @IBOutlet weak var lblLineVert:  UILabel?
    
}
