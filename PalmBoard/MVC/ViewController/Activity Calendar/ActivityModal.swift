//  ActivityModal.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 12/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
import UIKit

class RPTModelActivity{
    
    var Title     :String
    var ActID     :Int
    var isWorking :Int
    var Duration  :Int
    var FromDate  :String
    var TillDate  :String
    
    init(Title:String,ActID:Int, isWorking: Int, Duration: Int,FromDate: String, TillDate: String) {
        
        self.Title     = Title
        self.ActID     = ActID
        self.isWorking = isWorking
        self.Duration  = Duration
        self.FromDate  = FromDate
        self.TillDate  = TillDate
    
    }
}

class ActivityModal: NSObject {
    
    var ErrorCode : Int
    var Session   :String

    var rpt         = [RPTModelActivity]()
    
    init(dic:[String:Any])
    {
        self.ErrorCode  = dic["ErrorCode"] as? Int ?? 0
        self.Session    = dic["Session"]   as? String ?? ""

        if let rpt = dic["Activities"] as? [[String:Any]]{
            
            self.rpt.removeAll(keepingCapacity: false)
            for rec in rpt{
                
                let strFromDate = rec["FromDate"] as? String ?? ""
                let strTillDate = rec["TillDate"] as? String ?? ""

                let Title       = rec["Title"]     as? String ?? ""
                let ActID       = rec["ActID"]     as? Int ?? 0
                let isWorking   = rec["isWorking"] as? Int ?? 0
                let Duration    = rec["Duration"]  as? Int ?? 0

                let FromDate    = strFromDate.dateChangeFormatCalendar()//rec["FromDate"] as? String ?? ""
                let TillDate    = strTillDate.dateChangeFormatCalendar()//rec["TillDate"] as? String ?? ""
                
                let model  = RPTModelActivity(Title: Title,ActID: ActID,isWorking: isWorking,Duration: Duration,FromDate: FromDate,TillDate: TillDate)
                self.rpt.append(model)
            }
        }
        print("self.rpt = \(self.rpt.count)")
        print("self.rpt = \(self.rpt)")
        //print("self.rpt = \(self.rpt[0].date)")
    }
}
