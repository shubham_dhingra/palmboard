//
//  CalendarActivityController.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 13/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit
let sharedInstanceActivity = CalendarActivityController()

protocol dateSelectedDelegate:class {
    func aDateSelected ( aSelectedDate : String, dateActivity: Int, monthActivity: String,color: UIColor)
}

class CalendarActivityController: NSObject {
    var arrayPresentModal = [String]()
    var arrayAbsentModal  = [String]()
    var arrayTitleCalModal   = [String]()

    var isYearCal         = false
    
    var weekDayNamesArray: [String] = Array()
    var currentMonth: Int!
    var currentYear: Int!
    
    var isShowPreviousNextMonthDays:Bool = false
    
    var todayDateBackGroundColor:       UIColor?
    var normalDateColor:                UIColor?
    var selectedDateColor:              UIColor?
    var highlightedDateColor:           UIColor?
    var highlightedDateBackgroundColor: UIColor?
    var disableDateColor:               UIColor?
    var inset:                          CGFloat?
    var delegate : dateSelectedDelegate?
    var aDateSelected = " "
    var status       = String()
    var colorSelection       = UIColor()


    @objc func dateSelected(_ sender:AnyObject) {
        
        aDateSelected = String(format: "%d/%d/%d",sender.tag, currentMonth, currentYear)
        
        let formatter        = DateFormatter.init()
        formatter.dateFormat = "dd/MM/yyyy"
        
        var date = Date()
        var strConvertDate = String()
        var strMonth = String()
        var intDate: Int
        intDate = sender.tag

        date               = formatter.date(from: aDateSelected)!
         strConvertDate    = formatter.string(from: date)
        
        formatter.dateFormat = "MMM"
        strMonth = formatter.string(from: date)
        checkWhichDateSelected(date: aDateSelected)
        
        if colorSelection == UIColor.white{
            delegate?.aDateSelected(aSelectedDate: "",dateActivity: 0,monthActivity: "", color: colorSelection)
        }
        else{
            delegate?.aDateSelected(aSelectedDate: strConvertDate,dateActivity: intDate,monthActivity: strMonth, color: colorSelection)
        }
       }
    func checkWhichDateSelected(date: String) {

        let formatter        = DateFormatter.init()
        formatter.dateFormat = "dd/MM/yyyy"
        var date1 = Date()
        var strConvertDate = String()
        date1               = formatter.date(from: date)!
        strConvertDate    = formatter.string(from: date1)

        if (arrayPresentModal.contains(strConvertDate)){
            self.status = "Present"
            colorSelection = UIColor(rgb: 0x78CC41)
        }
        else if(arrayAbsentModal.contains(strConvertDate)){
            self.status = "Absent"
            colorSelection = UIColor(rgb: 0x44A8EB)
        }
        else {
            colorSelection = UIColor.white
        }
    }
    func createCalendar(withMonth month:Int, andYear year: Int, view: UIView) {
        
        sharedInstanceActivity.currentMonth = month
        sharedInstanceActivity.currentYear  = year
        let date         = sharedInstanceActivity.returnDateFormatter().date(from:String(format: "1/%d/%d",sharedInstanceActivity.currentMonth, sharedInstanceActivity.currentYear))//"1/\\\(month)/\\\(year)"
        let numberOfDays = sharedInstanceActivity.getNumberOfDays(inMonth: month, withDate: date!)
        var  weekDay     = sharedInstanceActivity.getFirstWeekDay(forDate: date!)
        //Because Monday is the first Week Day!
        if (weekDay == 1)
        {weekDay = 7}
        else
        { weekDay = weekDay - 1}
        createMonthWith(numberofDays: numberOfDays, andStartingWeekDay: (weekDay), view: view)
    }
    
    func createMonthWith(numberofDays days:Int, andStartingWeekDay weekday:Int , view:UIView)  {
        
        for vw in view.subviews {
            if vw.tag != 300 {
                vw.removeFromSuperview()
            }
        }
        var xPos = 00.0
        var yPos = 00.0
        var startIndex = weekday
        
        let width  = view.frame.size.width / 7//screenWidth / 7
        let height = width
        
        var weekCount = 1
        for weekDaynames in sharedInstanceActivity.weekDayNamesArray {
            let weekDayLable = UILabel.init(frame: CGRect(x: xPos, y: yPos, width: Double(width) + 5, height: 38.0))//Double(height)
            weekDayLable.text            = weekDaynames
            weekDayLable.textAlignment   = .center
            weekDayLable.contentMode     = .scaleAspectFit
            weekDayLable.backgroundColor = UIColor(rgb: 0xF3F3F3)
            weekDayLable.font      = R.font.ubuntuRegular(size: 17.0)
            weekDayLable.textColor = UIColor(rgb: 0xBFBFBF)
            
            view.addSubview(weekDayLable)
            xPos=xPos+Double(width)
            if(weekCount == 7){
                weekDayLable.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
                weekDayLable.font = R.font.ubuntuRegular(size: 17.0)
            }
            weekCount += 1
        }
        
        xPos = 00.0
        yPos = 40//Double(yPos) + Double(height);
        
        for i in 1...7 {
            
            if i == weekday {
                break
            }
            xPos = xPos + Double(width)
        }
       var lblWidth = 0.0
        var lblHeight = 0.0
        
        if(!isYearCal)
        {
            lblWidth = Double(width)-8
            lblHeight = Double(height) - 8
        }
        else
        {
            lblWidth = Double(width)-4
            lblHeight = Double(height)-4
        }
        
        for date in 1...days {
            
            let dateBtn = UIButton(type: .custom)
            dateBtn.addTarget(self, action: #selector( dateSelected(_ :)), for: .touchUpInside)
            dateBtn.tag = date 

            
            let dateLable           = UILabel.init(frame: CGRect(x: xPos + 5, y: Double(yPos), width: lblWidth, height: lblHeight))
            dateLable.text          = String(describing: date)
            dateLable.textAlignment = .center
            dateLable.tag           = date
            
            dateBtn.frame = dateLable.frame
            dateBtn.center = dateLable.center

            if(isYearCal){
                dateLable.font = dateLable.font.withSize(14.0)
            }
            let date        = sharedInstanceActivity.returnDateFormatter().date(from:String(format: "%d/%d/%d",date, sharedInstanceActivity.currentMonth, sharedInstanceActivity.currentYear))
            let currentDate = sharedInstanceActivity.returnDateFormatter().date(from: sharedInstanceActivity.returnDateFormatter().string(from: Date()))
            
            if date?.compare(currentDate!) == .orderedSame {
                //current date
                //dateLable.backgroundColor    = UIColor.purple
                // dateLable.textColor          = UIColor.white
                dateLable.layoutIfNeeded()
                dateLable.clipsToBounds      = true
                dateLable.layer.cornerRadius = (dateLable.frame.size.width)/2
                
            }
            //Mark Sunday Blue
            sharedInstanceActivity.markSunday(date: date!, lbl: dateLable)
            ////Marking holidays
           // sharedInstanceActivity.markHolidays(date: date!, lbl: dateLable)
            //////Marking Present
            sharedInstanceActivity.markPresent(date: date!, lbl: dateLable)
            ////Marking Absent
            sharedInstanceActivity.markAbsent(date: date!, lbl: dateLable)
            
            view.addSubview(dateLable)
            view.addSubview(dateBtn)

            startIndex = startIndex + 1
            xPos       = xPos+Double(width) //+ 15
            if startIndex == 8 {
                xPos       = 00.0
                yPos       = Double(yPos)+Double(height);
                startIndex = 1;
            }
        }
    }
    func returnDateFormatter() -> DateFormatter {
        let formatter        = DateFormatter.init()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }
    func returnMonthFormatter() -> DateFormatter {
        let formatter        = DateFormatter.init()
        formatter.dateFormat = "MMMM/yyyy"
        return formatter
    }
    func returnWeekDayArray() -> Array<String> {
        let firstWeekday = 2 // -> Monday
        let formatter    = DateFormatter.init()
        var symbols      = formatter.veryShortWeekdaySymbols
        symbols          = Array(symbols![firstWeekday-1..<(symbols?.count)!]) + symbols![0..<firstWeekday-1]
        //    print(symbols!)
        return symbols!
    }
    
    func getNumberOfDays(inMonth month:Int, withDate date:Date) -> Int {
        let cal              = Calendar.current
        var dateComponents   = DateComponents.init()
        dateComponents.month = month
        let range            = cal.range(of: .day, in: .month, for: date)
        return (range?.count)!
    }
    
    func getFirstWeekDay(forDate date:Date) -> Int {
        let cal = Calendar.current
        let dateComponents = cal.dateComponents([.weekday], from: date)
        return (dateComponents.weekday!)
    }
    func markSunday(date: Date , lbl : UILabel)
    {
        let calendar      = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let components    = calendar!.components([.weekday], from: date)
        if components.weekday == 1 {
            lbl.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
        }
    }
    
    func markPresent(date: Date , lbl : UILabel){
        for i in 0..<arrayPresentModal.count{
            let isPresent = returnDateFormatter().date(from: arrayPresentModal[i])
            if date.compare(isPresent!) == .orderedSame {
                lbl.backgroundColor    = UIColor(rgb: 0x78CC41)//UIColor.green
                lbl.textColor          = UIColor.white
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
            }
        }
    }
    func markAbsent(date: Date , lbl : UILabel){
        for i in 0..<arrayAbsentModal.count{
            let isAbsent = returnDateFormatter().date(from: arrayAbsentModal[i])
            // print("isAbsent")
            if date.compare(isAbsent!) == .orderedSame {
                lbl.backgroundColor    = UIColor(rgb: 0x44A8EB)//UIColor.red
                lbl.textColor          = UIColor.white
                lbl.numberOfLines   = 0
                
                let str = NSMutableAttributedString(string: "\(lbl.tag)\n")
                let attributedString = NSAttributedString(string: "H", attributes: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 10.0) ])
                str.addAttribute(NSAttributedString.Key.font, value: R.font.ubuntuRegular(size: 17.0) , range: NSRange(location: 0, length: 2))

                str.append(attributedString)
                lbl.attributedText     = str
               lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
            }
        }

    }
}
