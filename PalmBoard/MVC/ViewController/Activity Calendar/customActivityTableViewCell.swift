//  customActivityTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 08/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class customActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var lblFromDate: UILabel?
    @IBOutlet weak var lblLine: UILabel?
    @IBOutlet weak var lblOuterCircle: UILabel?
    @IBOutlet weak var lblInnerCircle: UILabel?
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var lblHoliday: UILabel?
    @IBOutlet weak var viewBackground: UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblOuterCircle?.layer.cornerRadius    = (lblOuterCircle?.frame.width)! / 2
        lblOuterCircle?.layer.borderColor     = UIColor(rgb: 0x56B54B).cgColor
        lblOuterCircle?.layer.borderWidth     = 1.0
        lblOuterCircle?.clipsToBounds         = true
        lblOuterCircle?.layer.backgroundColor = UIColor.white.cgColor
        
        lblInnerCircle?.layer.cornerRadius    = (lblInnerCircle?.frame.width)! / 2
        lblInnerCircle?.clipsToBounds         = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
