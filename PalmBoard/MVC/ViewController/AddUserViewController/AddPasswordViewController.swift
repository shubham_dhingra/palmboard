//  AddPasswordViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 08/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import IQKeyboardManager

class AddPasswordViewController: BaseViewController {
    
    @IBOutlet weak var txtPassword  : UITextField?
    @IBOutlet weak var imgSchool    : UIImageView?
    @IBOutlet weak var lblSchoolName: UILabel?
    @IBOutlet weak var btnNext      : CustomButtom?
    @IBOutlet weak var lblUsername  : UILabel?
    @IBOutlet weak var imgUser      : UIImageView?{
        didSet {
            imgUser?.layer.cornerRadius = /imgUser?.frame.size.width / 2
            imgUser?.clipsToBounds = true
        }
    }
    
    
    var buttonRight: UIButton?
    var schoolData      : SchoolCodeTable?
    var profileModal    : MainProfileModel?
    var currentUserType : Int?
    var username        : String?
    var isChecked        = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        self.selectSchool = schoolData
        onViewDidLoad()
        
    }
    func onViewDidLoad(){
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        buttonRight                  = UIButton(type: .custom)
        buttonRight?.setImage(R.image.hideEye(), for: .normal)//eyes
        buttonRight?.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        buttonRight?.frame           = CGRect(x: 0, y: 0, width: CGFloat(25), height: CGFloat(25))
        buttonRight?.addTarget(self, action: #selector(self.ClkShowPwd), for: .touchUpInside)
        txtPassword?.rightView       = nil//buttonRight
        txtPassword?.rightViewMode   = .always
         self.isChecked               = true
    }
    
    @IBAction func ClkShowPwd(_ sender: Any){
        if txtPassword?.text != ""{
            (sender as AnyObject).setImage(UIImage(named: self.isChecked ?  "eyes" : "hideEye"), for: UIControl.State.normal)
            self.isChecked =  !self.isChecked
            txtPassword?.isSecureTextEntry = self.isChecked
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.noNavBar()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func setUpView() {
        if let schoolData = schoolData {
            lblSchoolName?.text = schoolData.schName
            if let schoolImage = schoolData.schLogo?.getImageUrl() {
                imgSchool?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: nil)
            }
        }
        
        if let modal = profileModal {
            if let name = modal.name {
                lblUsername?.text = "Hi, \(name)"
            }
            if let schoolImage = modal.Photo?.getImageUrl() {
                imgUser?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            }
            else{
                imgUser?.image = R.image.noProfile_Big()
            }
        }
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnNextAct(_ sender: UIButton) {
        authenticateUser()
    }
    
    @IBAction func btnForgotAct(_ sender : UIButton){
        
        guard let currentSchool = schoolData else {return}
        let storyboard          = UIStoryboard(name: "Main", bundle: nil)
        guard let fwdController = storyboard.instantiateViewController(withIdentifier: "FwdViewController") as? FwdViewController else {return}
        fwdController.forgotLabel    = "password"
        fwdController.username       = /username
        fwdController.intSMSPvdID    = Int(currentSchool.sMS_Prvd_ID)
        fwdController.intCountryCode = Int(currentSchool.countryCode)
        fwdController.strSchoolCode  = /currentSchool.schCode
        fwdController.intUserType    = /self.currentUserType
        fwdController.modalTransitionStyle   = .crossDissolve
        fwdController.modalPresentationStyle = .overCurrentContext
        self.navigationController?.presentVC(fwdController)
    }
    
    func authenticateUser(){
        txtPassword?.resignFirstResponder()
        
        if txtPassword?.text?.trimmed().count == 0 {
            shakeTextField(textField: txtPassword,errorMessage: AlertMsg.pleaseEnterPassword.get)
        }
            
        else {
            APIManager.shared.request(with: HomeEndpoint.validate(schoolCode : schoolData?.schCode , username: username, password: txtPassword?.text)) {[weak self](response) in
                switch response{
                case .success(let responseValue):
                    self?.handle(response : responseValue)
                    
                case .failure(let responseValue):
                    self?.handle(response : responseValue)
                }
            }
        }
    }
    
    func handle(response : Any?){
        
        if let response = response as? [String : Any] , let isAuthenticated = response["isAuthenticate"] as? Bool {
            isAuthenticated ? addAccount(response) :
                shakeTextField(textField: txtPassword,errorMessage: AlertMsg.invalidPassword.get)
        }
    }
    
    
    func addAccount(_ response : [String : Any]) {
        if sender != nil && username != nil {
            DBManager.shared.saveUserData(result: response, senderCode: /schoolData?.schCode, username: username ,password: txtPassword?.text)
            NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.ADD_USER), object: nil)
            popToRootVC()
        }
    }
}

// MARK: - TextField Delegate Method
extension AddPasswordViewController  :  UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
        textField.borderColorClean()
        self.lblError?.isHidden = true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        txtPassword?.rightView       = nil
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length == 0{
            buttonRight?.setImage(R.image.hideEye(), for: .normal)//eyes
        }
        if newString.length > 0 {
            txtPassword?.rightView       = buttonRight
        }
        return true
    }
}

