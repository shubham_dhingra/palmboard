//  AddUserViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 07/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import IQKeyboardManager

class AddUserViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtUsername :          UITextField?
    @IBOutlet weak var btnChangeCode :        UIButton?
    @IBOutlet weak var imgSchool:             UIImageView?
    @IBOutlet weak var lblSchoolName:         UILabel?
    @IBOutlet weak var btnNext:               CustomButtom?
    @IBOutlet weak var bluredView:            UIView?
    @IBOutlet weak var customAlertView:       UIView?
    @IBOutlet weak var tblView:               UITableView?
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView?
    
    //MARK::- VARIABLES
    var schoolData : SchoolCodeTable?
    var arrSearchData    = [[String : Any]]()
    var arraySchoolList  = [[String : Any]]()
    var strSchoolCode    = String()
    
    //MARK::- LIFE CYCLES
    override func viewWillAppear(_ animated : Bool){
        super.viewWillAppear(animated)
        strSchoolCode = ""
        setUpView()
        self.navigationController?.isNavigationBarHidden = true
        self.hideTabBar()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateSchool(_:)), name: NSNotification.Name(rawValue: NOTI.CHANGE_SCHOOL), object: nil)
    }
   
    //Change School Notification
    @objc func updateSchool(_ notification: NSNotification) {
        if let dict = notification.userInfo as NSDictionary? {
            if let selectSchool = dict["schoolData"] as? SchoolCodeTable{
                self.schoolData = selectSchool
                self.selectSchool =  self.schoolData
                setUpView()
            }
        }
    }
    //MARK:- setUpView
    func setUpView() {
        if self.selectSchool == nil {
        if let schoolData = self.getCurrentSchool() {
            lblSchoolName?.text = schoolData.schName
            if let schoolImage = schoolData.schLogo?.getImageUrl() {
                imgSchool?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: nil)
            }
        }
        }
        else {
            if let schoolData = self.selectSchool {
                lblSchoolName?.text = schoolData.schName
                if let schoolImage = schoolData.schLogo?.getImageUrl() {
                    imgSchool?.loadURL(imageUrl: schoolImage, placeholder: nil, placeholderImage: nil)
                }
            }
        }
    }
    
    
    @IBAction func btnChangeCodeAct (_ sender : UIButton){
        openSchoolCodeVc()
    }
    
    
    func openSchoolCodeVc(){
        guard let schoolCodeVc = storyboard?.instantiateViewController(withIdentifier: "SchoolCodeViewController") as? ViewController else {return}
        schoolCodeVc.fromStarting = false
        schoolCodeVc.forChangeCode = true
        txtUsername?.text = nil
        self.pushVC(schoolCodeVc)
    }
    
    @IBAction func btnNextAct(_ sender: UIButton) {
        
        if selectSchool == nil {
            if let school = self.getCurrentSchool(){
                addUser(school)
            }
        }
        else {
            if let school = self.selectSchool{
                addUser(school)
            }
        }
    }
    
    func addUser(_ school : SchoolCodeTable){
        if !(DBManager.shared.isUserAlreadyExist(schoolCode: school.schCode , username: txtUsername?.text)){
            verifyUser()
        }
        else {
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: AlertMsg.userAlreadyExist.get, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                return
            }
        }
    }
    
    
    func verifyUser(){
        txtUsername?.resignFirstResponder()
        if txtUsername?.text?.trimmed().count == 0 {
            shakeTextField(textField: txtUsername,errorMessage: AlertMsg.pleaseEnterUserName.get)
        }
        else {
            APIManager.shared.request(with: HomeEndpoint.verifyUser(schoolCode : schoolData?.schCode, username: txtUsername?.text)) { (response) in
                switch response{
                case .success(let responseValue):
                    self.handle(response : responseValue)
                    
                case .failure(_):
                    self.shakeTextField(textField: self.txtUsername,errorMessage: AlertMsg.invalidUsername.get)
                }
            }
        }
    }
    
    func handle(response : Any?){
        if let response = response as? MainProfileModel{
            if response.errorCode == 0 {
            guard let addPasswordVc = storyboard?.instantiateViewController(withIdentifier: "AddPasswordViewController") as? AddPasswordViewController else {return}
            addPasswordVc.schoolData = schoolData
            addPasswordVc.profileModal = response
            addPasswordVc.username = txtUsername?.text
            addPasswordVc.currentUserType = response.currentUserType
            self.pushVC(addPasswordVc)
        }
        else {
             Messages.shared.show(alert: .oops, message: /response.message, type: .warning)
            }
        }
    }
    
    
    @IBAction func btnForgotAct(_ sender : UIButton){
//        guard let currentSchool = self.getCurrentSchool() else { return }
        guard let currentSchool = schoolData else {return}
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let fwdController = storyboard.instantiateViewController(withIdentifier: "FwdViewController") as? FwdViewController else {return}
        
        fwdController.forgotLabel    = "username"
        fwdController.username       = /txtUsername?.text
        fwdController.intSMSPvdID    = Int(currentSchool.sMS_Prvd_ID)
        fwdController.intCountryCode = Int(currentSchool.countryCode)
        fwdController.strSchoolCode  = /currentSchool.schCode
        fwdController.intUserType    = /self.userType?.toInt()
        fwdController.modalTransitionStyle = .crossDissolve
        fwdController.modalPresentationStyle = .overCurrentContext
        self.navigationController?.presentVC(fwdController)
    }

}
extension AddUserViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
        textField.borderColorClean()
        self.lblError?.isHidden = true
    }
}
