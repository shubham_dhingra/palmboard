//
//  AdditionalNoViewController.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 19/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class AdditionalNoViewController: UIViewController,UIWebViewDelegate,UIScrollViewDelegate {
    @IBOutlet weak var webViewWebsite: UIWebView?
    var strWebsite = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        self.title = "Additional Mobile Number"
        webViewWebsite?.scrollView.delegate = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if let check = self.getCurrentSchool()?.addMobUrl  {strWebsite =  (check)}
        
        print("strWebsite = \(strWebsite)")
        self.webAPIWebsite()
    }
    //MARK:- webAPIWebsite
    func webAPIWebsite()
    {
        self.webViewWebsite?.isHidden        = false
        self.webViewWebsite?.delegate        = self
        self.webViewWebsite?.backgroundColor = UIColor.clear
        if let url = URL(string: self.strWebsite){
            let request = URLRequest(url: url)
            self.webViewWebsite?.loadRequest(request)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            }));
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()
        let alert = UIAlertController(title: "", message: "Can't Connect. Please check your internet Connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
        }));
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
}
