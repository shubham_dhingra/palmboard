import UIKit

class AssignmentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var heightCollection:         NSLayoutConstraint?
    @IBOutlet weak var viewSegment:              UIView?
    @IBOutlet weak var segmentCustom:            UISegmentedControl?
    @IBOutlet weak var tblAssignment:            UITableView?
    @IBOutlet weak var collectionViewAssignment: UICollectionView?
    @IBOutlet weak var imgViewNoAssignment:      UIImageView?
    @IBOutlet weak var lblNoAssignment:          UILabel?
    
    var intClassId             = Int()
    var arrayAssignment        = [[String: Any]]()
    var arrayAssignmentSubject = [[String: Any]]()
    
    
    var strSchoolCodeSender   = String()
    var intListChoice         = Int()
    
    var strFilePath           = String()
    var strPrefix             = String()
    var urlPDF: URL!
    var finalUrl              = String()
    var intUserIDAssignment   = Int()
    var intUserTypeAssignment = Int()
    var eventIndex            = Int() 
    var btnTitle              =  UIButton()
    var strAssDTL: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgViewNoAssignment?.isHidden = true
        self.lblNoAssignment?.isHidden     = true
        
        self.title = "Assignments"
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Assignments", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        
        self.navigationItem.titleView = btnTitle
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        segmentCustom?.selectedSegmentIndex = 0
        heightCollection?.constant = 45
        self.eventIndex = 0
        
        self.collectionViewAssignment?.delegate   = nil
        self.collectionViewAssignment?.dataSource = nil
        
        self.tblAssignment?.delegate   = nil
        self.tblAssignment?.dataSource = nil
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let classID = self.getCurrentUser()?.classID {
            intClassId =  Int(classID)
        }
        self.webApiAssignment()
       
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.tblAssignment?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showMenu()
        strAssDTL = ""
        finalUrl = ""
        navigationItem.hidesBackButton = true
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        guard let themeColor = DBManager.shared.getAllSchools().first?.themColor else { return}
        viewSegment?.backgroundColor = themeColor.hexStringToUIColor()
        segmentCustom?.backgroundColor = themeColor.hexStringToUIColor()
    }
    @IBAction func ClkSegment(_ sender: Any)
    {
        self.imgViewNoAssignment?.isHidden = true
        self.lblNoAssignment?.isHidden     = true
        
        switch segmentCustom?.selectedSegmentIndex
        {
        case 0:
            intListChoice  = 0
           // arrayAssignment.removeAll()
         case 1:
            intListChoice  = 1
            //arrayAssignment.removeAll()
        default:
            break
        }
        self.webApiAssignment()
    }
    func webApiAssignment()
    {
        Utility.shared.loader()
        // print("intListChoice = \(intListChoice)")
        
        self.tblAssignment?.isHidden = true
        self.collectionViewAssignment?.isHidden = true
        
        var urlString = String()
        
        if(intListChoice == 0){
           // urlString = "http://webapi.palmboard.in/api/Academic/AllAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"
            urlString = APIConstants.basePath + "Academic/AllAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"

        }else{
           // urlString = "http://webapi.palmboard.in/api/Academic/SubAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"
            urlString = APIConstants.basePath + "Academic/SubAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"

        }
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if(self.intListChoice == 0)
                        {
                            if let arrayAll = (result["Assignments"] as? [[String: Any]]){
                                self.arrayAssignment = arrayAll
                            }
                            else {
                                self.arrayAssignment.removeAll()
                            }
                        }
                        else if(self.intListChoice == 1)
                        {
                            
                            if let arraySub = (result["SubjectAssignments"] as? [[String: Any]]){
                                self.collectionViewAssignment?.isHidden = false
                                self.arrayAssignment = arraySub
                            }
                            else {
                                self.arrayAssignment = [[:]]
                                self.collectionViewAssignment?.isHidden = true
                            }
                        }
                        if self.arrayAssignment.count == 0
                        {
                            self.imgViewNoAssignment?.isHidden = false
                            self.lblNoAssignment?.isHidden     = false
                        }
                        else
                        {
                            
                            if(self.intListChoice == 1 )
                            {
                                self.heightCollection?.constant = 45
                                
                                self.tblAssignment?.delegate   = nil
                                self.tblAssignment?.dataSource = nil
                                self.tblAssignment?.isHidden   = false
                                
                                self.collectionViewAssignment?.isHidden   = false
                                self.collectionViewAssignment?.delegate   = self
                                self.collectionViewAssignment?.dataSource = self
                                self.collectionViewAssignment?.reloadData()
                                
                                self.collectionViewAssignment?.performBatchUpdates({},completion: { (finished) in
                                    //print("collection-view finished reload done")
                                    
                                    // self.tblTimeTable.delegate   = self
                                    //self.tblTimeTable.dataSource = self
                                    // self.tblTimeTable.isHidden   = false
                                    // self.collectionViewTimeTable.collectionViewLayout.invalidateLayout()
                                    //  self.collectionView(self.collectionViewTimeTable, didSelectItemAt: IndexPath(item: 0, section: 0))
                                    self.collectionViewAssignment?.collectionViewLayout.invalidateLayout()
                                    
                                    if(self.intListChoice == 1){
                                        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                                        self.collectionViewAssignment?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                        
                                        //                                    if let theLabel = self.view.viewWithTag( 1) as? UILabel {
                                        //                                        theLabel.backgroundColor = UIColor(rgb: 0xFFC026)
                                        //                                    }
                                        self.collectionView(self.collectionViewAssignment!, didSelectItemAt: IndexPath(item: 0, section: 0))
                                    }
                                })
                            }
                            else {
                                self.heightCollection?.constant = 0
                                self.collectionViewAssignment?.delegate   = nil
                                self.collectionViewAssignment?.dataSource = nil
                                self.collectionViewAssignment?.isHidden   = true
                                
                                if self.arrayAssignment.count > 0
                                {
                                    self.tblAssignment?.delegate   = self
                                    self.tblAssignment?.dataSource = self
                                    self.tblAssignment?.isHidden   = false
                                    self.tblAssignment?.reloadData()
                                }
                            }
                        }
                    }
                }
            }
           if (error != nil){
            
            DispatchQueue.main.async{
                        self.tblAssignment?.isHidden = false
                        self.view.isUserInteractionEnabled = true
                        if let code = errorCode {
                            switch (code){
                            case Int(-1009):
                                Utility.shared.internetConnectionAlert()
                            default:
                                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                                print("errorcode = \(String(describing: code))")
                            }
                        }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(self.intListChoice == 0){
            return self.arrayAssignment.count
        }
        else if(self.intListChoice == 1){
            return self.arrayAssignmentSubject.count
        }
        else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellAssignment", for: indexPath as IndexPath) as! CusstomAssignmentTableViewCell
        guard let themeColor = self.getCurrentSchool()?.themColor else { return UITableViewCell()}
        
        if(self.intListChoice == 0 )
        {
            var strTitleAssignment   = String()
            var strSubjectAssignment = String()
//            if self.arrayAssignment.count == 0 {
//                return cell
//            }

            strTitleAssignment   = arrayAssignment[indexPath.row]["Title"]   as! String
            if let subject = arrayAssignment[indexPath.row]["Subject"] as? String{
                strSubjectAssignment = "(\(subject))"
            }
            let firstAttr  =  [NSAttributedString.Key.foregroundColor : UIColor.flatLightBlack]
            let secondAttr =  [NSAttributedString.Key.foregroundColor : themeColor.hexStringToUIColor()]
            cell.lblTitle?.attributedText = Utility.shared.getAttributedString(firstStr: strTitleAssignment, firstAttr: firstAttr, secondStr: strSubjectAssignment, secondAttr: secondAttr)
            cell.selectionStyle = .none
            //cell.lblTitle.text     = arrayAssignment[indexPath.row]["Title"]     as? String
            cell.lblUpdatedBy?.text = arrayAssignment[indexPath.row]["UpdateBy"]  as? String
            cell.btnProfile?.tag    = indexPath.row
            cell.btnProfile?.addTarget(self, action: #selector(self.ClkBtnProfile(sender:)), for: .touchUpInside)
           cell.selectionStyle = .none
            cell.backgroundColor = UIColor.white
            
            
            var strUploadedOn           = String()
            var strAsgDate              = String()
            var strSubmitDate           = String()
            
            strUploadedOn               = arrayAssignment[indexPath.row]["UploadedOn"] as! String
            strAsgDate                  = arrayAssignment[indexPath.row]["AsgDate"]    as! String
            strSubmitDate               = arrayAssignment[indexPath.row]["SubmitDate"] as! String
            
            let formatter               = ISO8601DateFormatter()
            formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
            
            let dateUploadedOn          = formatter.date(from: strUploadedOn)
            strUploadedOn               = formatter.string(from: dateUploadedOn!)
            
            let dateAsgDate          = formatter.date(from: strAsgDate)
            strAsgDate               = formatter.string(from: dateAsgDate!)
            
            let dateSubmitDate          = formatter.date(from: strSubmitDate)
            strSubmitDate               = formatter.string(from: dateSubmitDate!)
            
            
            let dateFormatterGet        = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "dd MMM, yyyy"
            
            let dateUploadedOn1: Date?  = dateFormatterGet.date(from: strUploadedOn)
            cell.lblDate?.text     = "Updated On: \(dateFormatter.string(from: dateUploadedOn1!))"
            
            let dateUploadedOn2: Date?  = dateFormatterGet.date(from: strAsgDate)
            cell.lblAssigned?.text    = "\(dateFormatter.string(from: dateUploadedOn2!))"
            
            let dateUploadedOn3: Date?  = dateFormatterGet.date(from: strSubmitDate)
            cell.lblSubmitted?.text     = "\(dateFormatter.string(from: dateUploadedOn3!))"
            
            var strPhotoURL = String()
            strPhotoURL     = arrayAssignment[indexPath.row]["Photo"]     as! String
            
            var strPrefix   = String()
            strPrefix       += strPrefix.imgPath1()
            let finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
            let urlPhoto    = URL(string: finalUrl)
            
            let imgViewUser = UIImageView()
            imgViewUser.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                cell.btnProfile?.setBackgroundImage(img1, for: .normal)
            }
           /* var inthasAttachment           = Int()
            inthasAttachment               = arrayAssignment[indexPath.row]["hasAttachment"] as! Int
            
            switch inthasAttachment
            {
            case 0:
                cell.accessoryType  = UITableViewCell.AccessoryType.none
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
            //cell.isUserInteractionEnabled = false
            case 1:
                cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCell.SelectionStyle.gray
                //cell.isUserInteractionEnabled = true
                
            default:
                break
            }*/
            cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCell.SelectionStyle.gray

            
        }
        else if(self.intListChoice == 1)
        {
            var strTitleAssignment   = String()
            var strSubjectAssignment = String()
            
            strTitleAssignment   = arrayAssignmentSubject[indexPath.row]["Title"]   as! String
            
            strSubjectAssignment = "("
            strSubjectAssignment += arrayAssignmentSubject[indexPath.row]["Subject"] as! String
            strSubjectAssignment += ")"
            
            strTitleAssignment += " \(strSubjectAssignment)"
            
            let range = (strTitleAssignment as NSString).range(of: strSubjectAssignment)
            
            let attribute = NSMutableAttributedString.init(string: strTitleAssignment)
            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: themeColor.hexStringToUIColor() , range: range)
            
            cell.lblTitle?.attributedText = attribute
            
            cell.lblUpdatedBy?.text = arrayAssignmentSubject[indexPath.row]["UpdateBy"]  as? String
            cell.btnProfile?.tag    = indexPath.row
            cell.btnProfile?.addTarget(self, action: #selector(self.ClkBtnProfile(sender:)), for: .touchUpInside)
            
            var strUploadedOn           = String()
            var strAsgDate              = String()
            var strSubmitDate           = String()
            
            strUploadedOn               = arrayAssignmentSubject[indexPath.row]["UploadedOn"] as! String
            strAsgDate                  = arrayAssignmentSubject[indexPath.row]["AsgDate"]    as! String
            strSubmitDate               = arrayAssignmentSubject[indexPath.row]["SubmitDate"] as! String
            
            let formatter               = ISO8601DateFormatter()
            formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
            
            let dateUploadedOn          = formatter.date(from: strUploadedOn)
            strUploadedOn               = formatter.string(from: dateUploadedOn!)
            
            let dateAsgDate          = formatter.date(from: strAsgDate)
            strAsgDate               = formatter.string(from: dateAsgDate!)
            
            let dateSubmitDate          = formatter.date(from: strSubmitDate)
            strSubmitDate               = formatter.string(from: dateSubmitDate!)
            
            
            let dateFormatterGet        = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "dd MMM, yyyy"
            
            let dateUploadedOn1: Date?  = dateFormatterGet.date(from: strUploadedOn)
            cell.lblDate?.text     = "Updated On: \(dateFormatter.string(from: dateUploadedOn1!))"
            
            let dateUploadedOn2: Date?  = dateFormatterGet.date(from: strAsgDate)
            cell.lblAssigned?.text    = "\(dateFormatter.string(from: dateUploadedOn2!))"
            
            let dateUploadedOn3: Date?  = dateFormatterGet.date(from: strSubmitDate)
            cell.lblSubmitted?.text     = "\(dateFormatter.string(from: dateUploadedOn3!))"
            
            var strPhotoURL = String()
            strPhotoURL     = arrayAssignmentSubject[indexPath.row]["Photo"]     as! String
            
            var strPrefix   = String()
            strPrefix       += strPrefix.imgPath1()
            let finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
            let urlPhoto    = URL(string: finalUrl)
            
            let imgViewUser = UIImageView()
            imgViewUser.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                cell.btnProfile?.setBackgroundImage(img1, for: .normal)
            }
           /* var inthasAttachment           = Int()
            inthasAttachment               = arrayAssignmentSubject[indexPath.row]["hasAttachment"] as! Int
            
            switch inthasAttachment
            {
            case 0:
                cell.accessoryType  = UITableViewCell.AccessoryType.none
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
            // cell.isUserInteractionEnabled = false
            case 1:
                cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
                cell.selectionStyle = UITableViewCell.SelectionStyle.gray
                //cell.isUserInteractionEnabled = true
            default:
                break
            }*/
            cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCell.SelectionStyle.gray

        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblAssignment?.deselectRow(at: indexPath, animated: true)
        
       // self.performSegue(withIdentifier: "segueAssignmentWebview", sender: self)
        
        if(self.intListChoice == 0)
        {
            if let strAssDTL1        = arrayAssignment[indexPath.row]["AsgData"] as? String{
                strAssDTL = strAssDTL1
            }
            if let strFilePath1        = arrayAssignment[indexPath.row]["AsgFile"] as? String{
                finalUrl   = strFilePath1
            }
           
        }
        if(self.intListChoice == 1)
        {
            if let strAssDTL1        = arrayAssignmentSubject[indexPath.row]["AsgData"] as? String{
                strAssDTL = strAssDTL1
            }
            if let strFilePath1        = arrayAssignmentSubject[indexPath.row]["AsgFile"] as? String{
                finalUrl   = strFilePath1
            }

           /* if let strFilePath1        = arrayAssignmentSubject[indexPath.row]["AsgFile"] as? String
            {
                var strPrefix   = String()
                
                strFilePath = strFilePath1
                
                strPrefix       += strPrefix.imgPath1()
                finalUrl    = "\(strPrefix)" +  "\(strFilePath)"
                urlPDF        = URL(string: finalUrl)
                //print("urlPDF= \(String(describing: urlPDF))")
                
                self.performSegue(withIdentifier: "segueAssignmentWebview", sender: self)
            }*/
        }
        self.performSegue(withIdentifier: "segueAssignmentWebview", sender: self)

    }
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //print("arrayAssignment.count = \(arrayAssignment.count)")
        return arrayAssignment.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssignCollection", for: indexPath) as! customAssignmentCollectionViewCell
        guard let themeColor = self.getCurrentSchool()?.themColor else { return UICollectionViewCell()}
        cell.lblSubjectName?.textColor = themeColor.hexStringToUIColor()
        cell.lblSubjectName?.text = arrayAssignment[indexPath.row]["Subject"] as? String
        cell.lblLine?.tag      = indexPath.row + 1
        
        
        if eventIndex == indexPath.row {
            cell.lblLine?.backgroundColor = themeColor.hexStringToUIColor()
            
        }else {
            cell.lblLine?.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //print("Did select row= \(indexPath.row)")
        
        if let array = arrayAssignment[indexPath.row]["Assignments"] as? [[String: Any]]
        {
            self.arrayAssignmentSubject = array
            
            self.imgViewNoAssignment?.isHidden = true
            self.lblNoAssignment?.isHidden     = true
            
            /*******p code*/
            collectionViewAssignment?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewAssignment?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            /*********/
            
            self.tblAssignment?.delegate   = self
            self.tblAssignment?.dataSource = self
            
            self.tblAssignment?.reloadData()
            self.tblAssignment?.isHidden = false
        }
        else{
            tblAssignment?.isHidden = true
            
            self.imgViewNoAssignment?.isHidden = false
            self.lblNoAssignment?.isHidden     = false
            
            /*******p code*/
            collectionViewAssignment?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewAssignment?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            /*********/
        }
        
    }

    override func viewDidLayoutSubviews() {
    }
    
    @objc func ClkBtnProfile(sender: UIButton)
    {
        //print("ClkBtnProfile = \(sender.tag)")
        
        let intTag = sender.tag
        if(self.intListChoice == 0)
        {
            intUserIDAssignment   = arrayAssignment[intTag]["UserID"]  as! Int
            intUserTypeAssignment = arrayAssignment[intTag]["UserType"]as! Int
        }
        else if(self.intListChoice == 1)
        {
            intUserIDAssignment   = arrayAssignmentSubject[intTag]["UserID"]  as! Int
            intUserTypeAssignment = arrayAssignmentSubject[intTag]["UserType"]as! Int
        }
        //self.performSegue(withIdentifier: "assignProfile", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID      = intUserIDAssignment
        shortProfileVc.intUserType    = intUserTypeAssignment
        self.pushVC(shortProfileVc)
    }
    
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        self.hideMenu()
        if segue.identifier == "segueAssignmentWebview"
        {
            let webviewVC                = segue.destination as! WebviewCircularViewController
            webviewVC.strfinalUrl        = finalUrl
            webviewVC.intClassDistingues = 2
            webviewVC.strAssDTL          = strAssDTL
        }
//        if segue.identifier == "assignProfile"
//        {
//            let shortProfile = segue.destination as! ShortProfileViewController
//            shortProfile.intUserID      = intUserIDAssignment
//            shortProfile.intUserType    = intUserTypeAssignment
//            // shortProfile.intProfileType = 1
//        }
    }
}

