//  CusstomAssignmentTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class CusstomAssignmentTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle:     UILabel?
    @IBOutlet weak var lblDate:      UILabel?
    @IBOutlet weak var lblAssigned:  UILabel?
    @IBOutlet weak var lblSubmitted: UILabel?
    @IBOutlet weak var lblUpdatedBy: UILabel?
    @IBOutlet weak var btnProfile:   UIButton?
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
}
