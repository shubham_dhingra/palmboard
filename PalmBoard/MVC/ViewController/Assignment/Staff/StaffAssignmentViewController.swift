//  StaffAssignmentViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 31/03/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class StaffAssignmentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var heightCollection: NSLayoutConstraint?
    // @IBOutlet weak var viewSegment:   UIView!
    //@IBOutlet weak var segmentCustom: UISegmentedControl!
    @IBOutlet weak var tblAssignment:            UITableView?
    @IBOutlet weak var collectionViewAssignment: UICollectionView?
    @IBOutlet weak var imgViewNoAssignment: UIImageView?
    @IBOutlet weak var lblNoAssignment:     UILabel?
    
    var intUserID             = Int()
    var arrayAssignment        = [[String: Any]]()
    var arrayAssignmentSubject = [[String: Any]]()
    
    var strSchoolCodeSender   = String()
    var intListChoice         = Int()
    
    var strFilePath           = String()
    var strPrefix             = String()
    var urlPDF: URL!
    var finalUrl              = String()
    var intUserIDAssignment   = Int()
    var intUserTypeAssignment = Int()
    var eventIndex            = Int()
    var btnTitle              = UIButton()
    var strAssDTL: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgViewNoAssignment?.isHidden = true
        self.lblNoAssignment?.isHidden     = true
        
        self.title = "Assignment"
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Assignment", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        
        self.navigationItem.titleView = btnTitle
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        //segmentCustom.selectedSegmentIndex = 0
        heightCollection?.constant = 45
        self.eventIndex = 0
        
        self.collectionViewAssignment?.delegate   = nil
        self.collectionViewAssignment?.dataSource = nil
        
        self.tblAssignment?.delegate   = nil
        self.tblAssignment?.dataSource = nil
       
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            intUserID =  Int(userId)
        }
        
        
        
        self.webApiAssignment()
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.tblAssignment?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = true
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    
    func webApiAssignment()
    {
        Utility.shared.loader()
        // print("intListChoice = \(intListChoice)")
        
        self.tblAssignment?.isHidden = true
        self.collectionViewAssignment?.isHidden = true
        
        var urlString = String()
        //urlString = "http://webapi.palmboard.in/api/Academic/StaffAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)"
        urlString = APIConstants.basePath + "Academic/StaffAssignment?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)"

        print("Assignment urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                  Utility.shared.removeLoader()
            }
            if let result = results{
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        if let arrayAll = (result["MyClasses"] as? [[String: Any]]){
                            self.arrayAssignment = arrayAll
                            self.tblAssignment?.reloadData()
                            self.collectionViewAssignment?.reloadData()
                        }
                        if self.arrayAssignment.count == 0
                        {
                            self.imgViewNoAssignment?.isHidden = false
                            self.lblNoAssignment?.isHidden     = false
                        }
                        else
                        {
                            self.heightCollection?.constant = 45
                            
                            self.tblAssignment?.delegate   = nil
                            self.tblAssignment?.dataSource = nil
                            self.tblAssignment?.isHidden   = false
                            
                            self.collectionViewAssignment?.isHidden   = false
                            self.collectionViewAssignment?.delegate   = self
                            self.collectionViewAssignment?.dataSource = self
                            self.collectionViewAssignment?.reloadData()
                            
                            self.collectionViewAssignment?.performBatchUpdates({},completion: { (finished) in
                                
                                self.collectionViewAssignment?.collectionViewLayout.invalidateLayout()
                                
                                let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                                self.collectionViewAssignment?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                self.collectionView(self.collectionViewAssignment!, didSelectItemAt: IndexPath(item: 0, section: 0))
                            })
                        }
                    }
                }
            }
            if (error != nil){
                DispatchQueue.main.async
                    {
                        self.view.isUserInteractionEnabled = true
                        if let code = errorCode {
                            switch (code){
                            case Int(-1009):
                                Utility.shared.internetConnectionAlert()
                            default:
                                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                                print("errorcode = \(String(describing: code))")
                            }
                        }
                }
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //if(self.intListChoice == 0){
        print("arrayAssignment \n \n \(arrayAssignment.count)")
        return self.arrayAssignmentSubject.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellStaffAssignment", for: indexPath as IndexPath) as! CusstomStaffAssignmentTableViewCell
        guard let themeColor = self.getCurrentSchool()?.themColor else { return UITableViewCell() }
        
        var strTitleAssignment   = String()
        var strSubjectAssignment = String()
        strTitleAssignment   = arrayAssignmentSubject[indexPath.row]["Title"]   as! String
        if let subject = arrayAssignmentSubject[indexPath.row]["Subject"] as? String{
            strSubjectAssignment = " (\(subject))"
        }
        let firstAttr  =  [NSAttributedString.Key.foregroundColor : UIColor.flatLightBlack]
        let secondAttr =  [NSAttributedString.Key.foregroundColor : themeColor.hexStringToUIColor()]
        cell.lblTitle?.attributedText = Utility.shared.getAttributedString(firstStr: strTitleAssignment, firstAttr: firstAttr, secondStr: strSubjectAssignment, secondAttr: secondAttr)
        cell.selectionStyle = .none
        if let strUpdateName = arrayAssignmentSubject[indexPath.row]["UpdateBy"]   as? String{
            cell.lblUpdatedBy?.text = strUpdateName
        }
        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: indexPath.row == arrayAssignment.count - 1)
        }
        
        cell.btnProfile?.tag    = indexPath.row
        cell.btnProfile?.addTarget(self, action: #selector(self.ClkBtnProfile(sender:)), for: .touchUpInside)
        
        var strUploadedOn           = String()
        var strAsgDate              = String()
        var strSubmitDate           = String()
        
        if let strUploadedDate2 = arrayAssignmentSubject[indexPath.row]["UploadedOn"]   as? String{
            strUploadedOn     = strUploadedDate2
        }
        if let strAsgDate2 = arrayAssignmentSubject[indexPath.row]["AsgDate"]   as? String{
            strAsgDate                  = strAsgDate2
        }
        if let strSubmitDate2 = arrayAssignmentSubject[indexPath.row]["SubmitDate"]   as? String{
            strSubmitDate               = strSubmitDate2
        }
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        
        let dateUploadedOn          = formatter.date(from: strUploadedOn)
        strUploadedOn               = formatter.string(from: dateUploadedOn!)
        
        let dateAsgDate          = formatter.date(from: strAsgDate)
        strAsgDate               = formatter.string(from: dateAsgDate!)
        
        let dateSubmitDate          = formatter.date(from: strSubmitDate)
        strSubmitDate               = formatter.string(from: dateSubmitDate!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        let dateUploadedOn1: Date?  = dateFormatterGet.date(from: strUploadedOn)
        cell.lblDate?.text     = "Updated On: \(dateFormatter.string(from: dateUploadedOn1!))"
        
        let dateUploadedOn2: Date?  = dateFormatterGet.date(from: strAsgDate)
        cell.lblAssigned?.text    = "\(dateFormatter.string(from: dateUploadedOn2!))"
        
        let dateUploadedOn3: Date?  = dateFormatterGet.date(from: strSubmitDate)
        cell.lblSubmitted?.text     = "\(dateFormatter.string(from: dateUploadedOn3!))"
        
        var strPhotoURL = String()
        strPhotoURL     = arrayAssignmentSubject[indexPath.row]["Photo"]     as! String
        
        var strPrefix   = String()
        strPrefix       += strPrefix.imgPath1()
        let finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
        let urlPhoto    = URL(string: finalUrl)
        
        let imgViewUser = UIImageView()
        imgViewUser.af_setImage(withURL: urlPhoto!, placeholderImage:R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
            cell.btnProfile?.setBackgroundImage(img1, for: .normal)
        }
        /*
        var inthasAttachment           = Int()
        inthasAttachment               = arrayAssignmentSubject[indexPath.row]["hasAttachment"] as! Int
        
        switch inthasAttachment
        {
        case 0:
            cell.accessoryType  = UITableViewCell.AccessoryType.none
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
        //cell.isUserInteractionEnabled = false
        case 1:
            cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
            cell.selectionStyle = UITableViewCell.SelectionStyle.gray
            //cell.isUserInteractionEnabled = true
            
        default:
            break
        }
        */
        cell.accessoryType  = UITableViewCell.AccessoryType.disclosureIndicator
        cell.selectionStyle = UITableViewCell.SelectionStyle.gray

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblAssignment?.deselectRow(at: indexPath, animated: true)
        
        // if(self.intListChoice == 0)
        //{
        if let strAssDTL1        = arrayAssignmentSubject[indexPath.row]["AsgData"] as? String{
            strAssDTL = strAssDTL1
        }
        if let strFilePath1        = arrayAssignmentSubject[indexPath.row]["AsgFile"] as? String{
            finalUrl   = strFilePath1
        }
        self.performSegue(withIdentifier: "segueStaffAssignmentWebview", sender: self)

    }
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        return arrayAssignment.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAssignCollection", for: indexPath) as! customStaffAssignmentCollectionViewCell
        guard let themeColor = self.getCurrentSchool()?.themColor else { return UICollectionViewCell()}
        
        if let strClass =  arrayAssignment[indexPath.row]["ClassName"] as? String{
            cell.lblSubjectName?.text      = strClass
        }
       
        
        cell.lblLine?.tag              = indexPath.row + 1
        
        if eventIndex == indexPath.row {
            cell.lblLine?.backgroundColor = themeColor.hexStringToUIColor()
             cell.lblSubjectName?.textColor = themeColor.hexStringToUIColor()
        }else {
            cell.lblLine?.backgroundColor = UIColor.clear
            cell.lblSubjectName?.textColor = UIColor.flatBlack
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //print("arrayAssignment \n \n  \(arrayAssignment)")
        
        if let array = arrayAssignment[indexPath.row]["Assignment"] as? [[String: Any]]
        {
            self.arrayAssignmentSubject = array
            self.imgViewNoAssignment?.isHidden = true
            self.lblNoAssignment?.isHidden     = true
            
            /*******p code*/
            collectionViewAssignment?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewAssignment?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            /*********/
            
            self.tblAssignment?.delegate   = self
            self.tblAssignment?.dataSource = self
            
            self.tblAssignment?.reloadData()
            self.tblAssignment?.isHidden = false
        }
        else{
            tblAssignment?.isHidden = true
            
            self.imgViewNoAssignment?.isHidden = false
            self.lblNoAssignment?.isHidden     = false
            
            /*******p code*/
            collectionViewAssignment?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewAssignment?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            /*********/
        }
        
    }

    
    @objc func ClkBtnProfile(sender: UIButton)
    {
        //print("ClkBtnProfile = \(sender.tag)")
        
        let intTag = sender.tag
        // if(self.intListChoice == 0)
        // {
        if let userID = arrayAssignmentSubject[intTag]["UserID"]  as? Int  {
           intUserIDAssignment   = userID
        }
        if let userType = arrayAssignmentSubject[intTag]["UserType"] as? Int {
        intUserTypeAssignment = userType
        }
        //}
        //        else if(self.intListChoice == 1)
        //        {
        //            intUserIDAssignment   = arrayAssignmentSubject[intTag]["UserID"]  as! Int
        //            intUserTypeAssignment = arrayAssignmentSubject[intTag]["UserType"]as! Int
        //        }
       // self.performSegue(withIdentifier: "assignStaffrofile", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID      = intUserIDAssignment
        shortProfileVc.intUserType    = intUserTypeAssignment
        self.pushVC(shortProfileVc)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - prepare segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueStaffAssignmentWebview"
        {
            let webviewVC                = segue.destination as! WebviewCircularViewController
            webviewVC.strfinalUrl        = finalUrl
            webviewVC.intClassDistingues = 2
            webviewVC.strAssDTL          = strAssDTL

        }
    }
}
class customStaffAssignmentCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblSubjectName: UILabel?
    @IBOutlet weak var lblLine:        UILabel?
}
class CusstomStaffAssignmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:     UILabel?
    @IBOutlet weak var lblDate:      UILabel?
    @IBOutlet weak var lblAssigned:  UILabel?
    @IBOutlet weak var lblSubmitted: UILabel?
    @IBOutlet weak var lblUpdatedBy: UILabel?
    @IBOutlet weak var btnProfile:   UIButton?
    
    override func awakeFromNib(){
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)
    }
}
