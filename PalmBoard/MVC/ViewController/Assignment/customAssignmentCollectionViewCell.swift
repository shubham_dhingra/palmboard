//  customAssignmentCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 17/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class customAssignmentCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var lblSubjectName: UILabel?
    @IBOutlet weak var lblLine: UILabel?
}
