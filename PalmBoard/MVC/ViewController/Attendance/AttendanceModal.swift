//  AttendanceModal.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 05/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

//let sharedInstance = AttendanceModal()

class RPTModel{
    
    var date  :String
    var status:Int
    var isLate : Bool = false
    
    init(date:String,status:Int , isLate: Bool) {
        self.date   = date
        self.status = status
        self.isLate = isLate
    }
}
class AttendanceModal: NSObject {
    
    var UserID      :String
    var SchoolDays  :Int
    var TotalPresent:Int
    var TotalAbsent :Int
    var TotalLeave  :Int
    var TotalLateDays    : Int
    var WorkingDays :Int//String
    var PresentDays :Int
    var AbsentDays  :Int
    var LeaveDays   :Int
    var LateDays    : Int
    var Session     :String
    var StartDate   :String
    var EndDate     :String
    var YrID        :Int
    var isLateEnabled : Bool
    var rpt         = [RPTModel]()

    init(dic:[String:Any])
    {
        self.UserID       = dic["UserID"]      as? String ?? ""
        self.SchoolDays   = dic["SchoolDays"]  as? Int ?? 0
        self.TotalPresent = dic["TotalPresent"]as? Int ?? 0
        self.TotalAbsent  = dic["TotalAbsent"] as? Int ?? 0
        self.TotalLateDays     = dic["TotalLates"]   as? Int ?? 0
        self.TotalLeave   = dic["TotalLeave"]  as? Int ?? 0
        self.WorkingDays  = dic["WorkingDays"] as? Int ?? 0//as? String ?? ""
        self.PresentDays  = dic["PresentDays"] as? Int ?? 0
        self.AbsentDays   = dic["AbsentDays"]  as? Int ?? 0
        self.LeaveDays    = dic["LeaveDays"]   as? Int ?? 0
        self.LateDays    = dic["LateDays"]   as? Int ?? 0
       
        self.Session      = dic["Session"]     as? String ?? ""
        self.StartDate    = dic["StartDate"]   as? String ?? ""
        self.EndDate      = dic["EndDate"]     as? String ?? ""
        self.YrID         = dic["YrID"]        as? Int ?? 0
        self.isLateEnabled = dic["isLateEnabled"]        as? Bool ?? false
        
        if let rpt = dic["RPT"] as? [[String:Any]]{
            
            self.rpt.removeAll(keepingCapacity: false)
            for rec in rpt{
                
                let date   = rec["AttDate"] as? String ?? ""
                let status = rec["Status"]  as? Int ?? 0
                let isLate = rec["isLate"] as? Bool ?? false
                let model  = RPTModel(date: date, status: status ,isLate : isLate)
                self.rpt.append(model)
            }
        }
        //print("self.rpt = \(self.rpt.count)")
        //print("self.rpt = \(self.rpt)")
        //print("self.rpt = \(self.rpt[0].date)")

    }
}
