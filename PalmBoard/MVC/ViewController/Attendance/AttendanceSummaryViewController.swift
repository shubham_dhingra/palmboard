//
//  AttendanceSummaryViewController.swift
//  PalmBoard
//
//  Created by Shubham on 04/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
import Charts
import Foundation

class AttendanceSummaryViewController : BaseViewController {

    @IBOutlet weak var btnDate : UIButton?
    @IBOutlet weak var rightBarBtnItem:    UIBarButtonItem?
    @IBOutlet weak var leftBackBarBtnItem: UIBarButtonItem?
    @IBOutlet weak var lblPresentTotal : UILabel?
    @IBOutlet weak var lblPersentPercent : UILabel?
    @IBOutlet weak var lblAbsentTotal : UILabel?
    @IBOutlet weak var lblAbsentPercent : UILabel?
    @IBOutlet weak var lblLeaveTotal : UILabel?
    @IBOutlet weak var lblLeavePercent : UILabel?
    @IBOutlet weak var lblLatePercent : UILabel?
    @IBOutlet weak var lblLateTotal  : UILabel?
    @IBOutlet weak var lateView : UIView?
    @IBOutlet weak var pieChart_View:      PieChartView?
//    @IBOutlet weak var typesView: UIView!
    @IBOutlet weak var collectionViewClassHeight: NSLayoutConstraint!
    
    
    var currentDate                = Date()
    var selectDate   :            Date?
    var sessionStartDate        = String()
    var totalStrength : Int?
    var classesArr = [MyClasse]()
    var attendanceSummaryModel : AtttendanceSummaryModal?
    var isLateEnabled : Bool = false
    var isFirstTime : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        selectDate = currentDate
        getAttendanceData()
    }
    
    
    func onViewDidLoad() {
        self.navigationItem.title = "Attendance"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
         btnDate?.setTitle(currentDate.dateToString(formatType: "dd MMM, yyyy"), for: UIControl.State.normal)
        if let sessionStartDate = self.getCurrentUser()?.sessionStart {
            self.sessionStartDate = sessionStartDate
        }
        collectionView?.register(R.nib.classCell)
    }
    
    
    @IBAction func ClkSearch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let attendanceVc = storyboard.instantiateViewController(withIdentifier: "AttendanceReportSearchViewController") as? AttendanceReportSearchViewController else {return}
        self.pushVC(attendanceVc)
    }
    
    //MARK: - ClkBtnDate
    @IBAction func ClkBtnDate(_ sender: UIButton) {
        self.showDatePicker()
    }
    
    
    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}



extension AttendanceSummaryViewController {
    
    // MARK: - UIDatePickerMethods
    func showDatePicker(){
        
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: selectDate ?? Date() , minimumDate: self.sessionStartDate.convertStringIntoDate(getDateFormat : "yyyy-MM-dd"), maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate?.titleLabel?.text) { (date) in
            self.setDateIntoLabel(date: date , lastText :  self.btnDate?.titleLabel?.text)
        }
    }
    
    //Set the date into the label after getting response from the date picker
    func setDateIntoLabel(date : Date? ,lastText : String?) {
        if let date = date {
            selectDate = date
            let dateString = date.dateToString(formatType :"dd MMM, yyyy")
            btnDate?.setTitle(dateString, for: UIControl.State.normal)
            getAttendanceData()
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            btnDate?.setTitle(strLastText, for: UIControl.State.normal)
        }
    }
}


extension AttendanceSummaryViewController {
    
    
    func getAttendanceData() {
        APIManager.shared.request(with: HomeEndpoint.AttendanceSummary(attDate: selectDate?.dateToString(formatType: "yyyy-MM-dd"), UserID: self.userID)) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    
    func handle(response : Any?){
        if let modal = response as? AtttendanceSummaryModal , let classes = modal.myClassArr {
            self.attendanceSummaryModel = modal
            self.isLateEnabled = /self.attendanceSummaryModel?.isLateEnabled
            self.classesArr = classes
            self.collectionViewClassHeight?.constant = self.classesArr.count == 1 ?  (/self.collectionView?.frame.width / 2.0) : CGFloat(CGFloat(self.classesArr.count / 2) * (/self.collectionView?.frame.width / 2.0))
            self.view.layoutIfNeeded()
            updateUI()
        }
    }
    
     func updateUI() {
        setUpPieChart()
        reloadTable()
     }
    
    
    func setUpPieChart(){
        
        if let model = attendanceSummaryModel , let ttlPresent = attendanceSummaryModel?.totalPresent , let ttlAbsent = attendanceSummaryModel?.totalAbsent , let ttlLeave = attendanceSummaryModel?.totalLeave , let ttlLate =  attendanceSummaryModel?.totalLate , let lateEnabled = model.isLateEnabled{
            
            self.isLateEnabled = lateEnabled
            totalStrength = ttlPresent + ttlAbsent + ttlLeave
//            typesView?.isHidden = totalStrength == 0
            if totalStrength != 0 {
            lblPersentPercent?.text = "\((((/ttlPresent.toDouble) / (/totalStrength?.toDouble  * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
            lblAbsentPercent?.text = "\((((/ttlAbsent.toDouble) / (/totalStrength?.toDouble  * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
            lblLeavePercent?.text = "\((((/ttlLeave.toDouble) / (/totalStrength?.toDouble  * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
            }
            else {
            lblPersentPercent?.text = "0.0%"
            lblAbsentPercent?.text = "0.0%"
            lblLeavePercent?.text = "0.0%"
            lblLatePercent?.text = "0.0%"
            }
            
            lblPresentTotal?.text = "\(ttlPresent)"
            lblAbsentTotal?.text = "\(ttlAbsent)"
            lblLeaveTotal?.text = "\(ttlLeave)"
            lblLateTotal?.text = "\(ttlLate)"
            
            if self.isLateEnabled && totalStrength != 0{
                
                lblLatePercent?.text = "\((((/ttlLate.toDouble) / (/totalStrength?.toDouble  * 1.0)) * 100.0).rounded(toPlaces: 2).toString)%"
                lblLateTotal?.text = "\(ttlLate)"
            }
            lateView?.isHidden = !self.isLateEnabled
            
        
        pieChart_View?.highlightPerTapEnabled = false
        pieChart_View?.usePercentValuesEnabled = false
        pieChart_View?.drawEntryLabelsEnabled = false
        
        var pieChartEntryArr = [PieChartDataEntry]()
        var colors = [UIColor]()
            if ttlPresent != 0 {
               pieChartEntryArr.append(PieChartDataEntry(value: Double(ttlPresent - ttlLate), label: nil))
                colors.append(UIColor.markPresentColor)
            }
            if ttlAbsent != 0 {
                pieChartEntryArr.append(PieChartDataEntry(value:Double(ttlAbsent), label: nil))
                 colors.append( UIColor.markAbsentColor)
            }
            if ttlLeave != 0 {
                pieChartEntryArr.append(PieChartDataEntry(value:Double(ttlLeave), label: nil))
                 colors.append(UIColor.markLeaveColor)
            }
        
        
        
        if self.isLateEnabled {
            if ttlLate != 0 {
               pieChartEntryArr.append(PieChartDataEntry(value:Double(ttlLate), label: nil))
                colors.append(UIColor.piecChartLateColor)
            }
            
        }
        let dataSet = PieChartDataSet(values: pieChartEntryArr, label: "")
            dataSet.valueColors = colors
            dataSet.valueFont = R.font.ubuntuRegular(size: 1.0)!
        let data    = PieChartData(dataSet: dataSet)
        pieChart_View?.data = data
            
        let str1 = NSMutableAttributedString(string: "\(/totalStrength)\n", attributes: [NSAttributedString.Key.font:R.font.ubuntuBold(size: 20.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
        let str2 = NSMutableAttributedString(string: "Student(s)", attributes: [NSAttributedString.Key.font:R.font.ubuntuRegular(size: 18.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
        pieChart_View?.rotationEnabled        = true
        str1.append(str2)
//        pieChart_View?.centerAttributedText = str1
       pieChart_View?.centerText   = str1.string
        
        
//        pieChart_View?.drawEntryLabelsEnabled = false
        pieChart_View?.chartDescription?.text = ""
//        pieChart_View?.highlightPerTapEnabled = true
        pieChart_View?.legend.enabled         = false
       
        pieChart_View?.legend.drawInside = false
        dataSet.colors                        = colors
        pieChart_View?.rotationEnabled        = true
        //This must stay at end of function
        pieChart_View?.notifyDataSetChanged()
        }
    }
}



extension AttendanceSummaryViewController {
    
    func configureCollectionView() {
            collectionDataSource =  CollectionViewDataSource(items: classesArr, collectionView: collectionView, cellIdentifier: R.reuseIdentifier.classCell.identifier, headerIdentifier: nil, cellHeight: (/self.collectionView?.frame.width / 2.0), cellWidth: (/self.collectionView?.frame.width / 2.0))
            
            collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
                guard let cell         = cell as? ClassCell else {return}
                cell.isLateEnabled = self.isLateEnabled
                cell.modal             = item
            
            }
            collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
                //  self.didSelect(indexpath.row)
                self.didSelect(indexpath.row)
            }
        }
        
        func didSelect(_ index : Int){
            
            guard let attendanceSummaryVc = R.storyboard.reports.attendanceReportViewController() else {return}
            attendanceSummaryVc.intClassId = /self.classesArr[index].classID
            attendanceSummaryVc.selectClassName = /self.classesArr[index].className
            attendanceSummaryVc.arrayMyClass = self.classesArr
            attendanceSummaryVc.currentDate = self.selectDate ?? Date()
            self.pushVC(attendanceSummaryVc)
        }
    
    
    func reloadTable() {
        if isFirstTime {
            configureCollectionView()
            isFirstTime =  !isFirstTime
        }
        else {
            collectionDataSource?.items = classesArr
            collectionView?.reloadData()
        }
    }
}


