import UIKit
import EZSwiftExtensions


protocol HideBarDelegateAttendance: class{
    func hideBar(boolHide: Bool)
}

class AttendanceViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITableViewDataSource, UITableViewDelegate {
    
    var listData :  AttendanceModal!
    var btnTitle       =  UIButton()
    var strSender      = String()
    var intUserID      = Int()
    var intUserType    = Int()
    var monthIndex : Int?
    var actualWidth  = CGFloat()
    var months = [String]()
    
    var monthsNew     = [[String: Any]]()
    var eventIndex    = Int()
    var btnSession    = UIButton()
    var intscrollToItem = Int()
    
    var arrayHoliday = [String]()
    var arrayPresent = [String]()
    var arrayAbsent  = [String]()
    var arrayLate    = [String]()
    var arrayLeave   = [String]()
    var arrayLateDays =  [String]()
    var boolAPI      = Bool()
    var isLateEnabled = Bool()
    var currentDateString    = String()
    var todayAttType  : String?
    var boolCheckDataDate          = Bool()
    var a : String? , b : Int?
    var intIDBack      = Int()
    var intYearBack    = Int()
    var indexToScroll = Int()
    var intFilterData   = Int()
    var sessionYearMonthWise = [Int]()
    var noDataForaParticularMonth : Bool = true
    
    private var year        = Int()
    private var yearStarted = Int()
    
    lazy var sessionStartYear : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
    lazy var sessionNextYear : Int = {
        guard let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionEnd) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
     lazy var sessionStartMonth : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.month, from: Date())
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.month, from: Date())
        }
        return date.month
    }()
    
    lazy var noOfMonthsForParticularSession : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart , let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return 0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let sessionStartDate = dateFormatter.date(from: sessionStart) , let sessionStartEnd = dateFormatter.date(from: sessionEnd) else{
            return 0
        }
        
        return Calendar.current.dateComponents([.month], from: sessionStartDate, to: sessionStartEnd).month ?? 0
    }()
    
    @IBOutlet weak var tblViewAttendance: UITableView?
    @IBOutlet weak var collectionViewAttendance: UICollectionView?
    
    //constraint for dynamic layouts
    @IBOutlet weak var viewPercentage: UIView?
    @IBOutlet weak var heightPercentageView : NSLayoutConstraint?
    @IBOutlet weak var heightOuterPercentageView : NSLayoutConstraint?
    @IBOutlet weak var heightTableView:      NSLayoutConstraint?
    
    @IBOutlet weak var lblPresentCircle : UILabel?
    @IBOutlet weak var lblAbsentCircle  : UILabel?
    @IBOutlet weak var lblLeaveCircle   : UILabel?
    @IBOutlet weak var lblLateCircle    : UILabel?
    @IBOutlet weak var lblLateDaysValue : UILabel?
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.eventIndex      = 0
        boolCheckDataDate    = false
        
        setUpMonthView()
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        btnSession                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSession.frame           = CGRect(x : 0,y:  0,width : 84, height: 40)
        btnSession.addTarget(self, action: #selector(self.ClkBtnSession(sender:)), for: .touchUpInside)
        btnSession.titleLabel?.font =  R.font.ubuntuRegular(size: 17.0)
        let barButton = UIBarButtonItem.init(customView: btnSession)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.collectionViewAttendance?.delegate   = nil
        self.collectionViewAttendance?.dataSource = nil
        self.tblViewAttendance?.delegate          = nil
        self.tblViewAttendance?.dataSource        = nil
        
        //print("self.eventIndex = \(self.eventIndex)")
        
        boolAPI         = false
        btnTitle        =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame   = CGRect(x : 0,y:  0,width : 90, height: 40)
        btnTitle.setTitle("Attendance", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.titleLabel?.textAlignment = .left
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
       if intFilterData == 22
        {
            monthIndex = intIDBack
            
            intscrollToItem = indexToScroll
            eventIndex = indexToScroll
            yearStarted = intYearBack
            getAttendanceData(intMonth: monthIndex!, intYear: intYearBack)
        }
        else
        {
            
            let year     = Calendar.current.component(.year, from: Date())
            let month    = Calendar.current.component(.month, from: Date())
            monthIndex = month
            intscrollToItem = self.months.index(of: GetMonth.getMonthName(index: month)) ?? 0
            eventIndex = intscrollToItem
            yearStarted = year
            getAttendanceData(intMonth: monthIndex!, intYear: year)
        }
        print("intscrollToItem=  \(intscrollToItem)")
    }
    
    func setUpMonthView() {
        
        print("month = \(self.sessionStartMonth)")
        print("year = \(self.sessionStartYear)")
        print("No of months for a particular Session = \(self.noOfMonthsForParticularSession)")
        var startMonth = sessionStartMonth
        var index = 0
        sessionYearMonthWise = []
        var YearToAppend : Int = sessionStartYear
        while(index < 12){
            
            
            if startMonth > 12 {
                startMonth = 1
                YearToAppend = sessionNextYear
            }
            months.append(GetMonth.getMonthName(index : startMonth))
            startMonth = startMonth + 1
            sessionYearMonthWise.append(YearToAppend)
            index = index + 1
        }
        print("Session Month Wise : \(sessionYearMonthWise)")
    }
    
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        //        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        //
        //        parent.hideBar(boolHide: false)
        
        guard let themeColor = self.getCurrentSchool()?.themColor else { return}
        collectionViewAttendance?.backgroundColor = themeColor.hexStringToUIColor()
        
        let now                  = NSDate()
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        currentDateString    = dateFormatter.string(from: now as Date)
        print("currentDateString= \(currentDateString)")
        
    }
    
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.tblViewAttendance?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    
    
    // MARK: - getAttendanceData
    func getAttendanceData(intMonth: Int, intYear: Int)
    {
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Academic/Attendance?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&Month=\(intMonth)&Year=\(intYear)"
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
                if let result = results{
                    
                    if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                    {
                        if  let isLateEnabled = result["isLateEnabled"] as? Bool {
                            self.isLateEnabled = isLateEnabled
                        }
                        
                        
                        if (result["RPT"] as? [[String: Any]]) != nil{
                            self.noDataForaParticularMonth = false
                        }
                        else {
                            self.noDataForaParticularMonth = true
                        }
                        
                        self.tblViewAttendance?.isHidden = false
                        ez.runThisInMainThread {
                            self.actualWidth = (self.isLateEnabled) ? (((UIScreen.main.bounds.width - 50) / 4)) : (((UIScreen.main.bounds.width - 50) / 3))
                            
                            self.heightOuterPercentageView?.constant  = self.actualWidth + 54.0
                            self.heightPercentageView?.constant  = self.actualWidth
                            self.viewPercentage?.isHidden = false
                            self.tblViewAttendance?.isScrollEnabled = false
                            self.configure()
                        }
                        
                        self.loadListData(dataDict: result)
                        self.tblViewAttendance?.isHidden = false
                        self.tblViewAttendance?.setContentOffset(CGPoint(x:0,y:0), animated: true)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Something went wrong", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    // MARK: - loadListData
    func loadListData(dataDict : [String:Any])  {
        //dic:[String:Any]
        listData = AttendanceModal(dic: dataDict)//ListData(WithDict: dataDict)
        
        DispatchQueue.main.async(execute: {
            self.arrayPresent.removeAll()
            self.arrayLate.removeAll()
            self.arrayAbsent.removeAll()
            self.arrayLeave.removeAll()
            self.arrayHoliday.removeAll()
            self.arrayLateDays.removeAll()
            
            print("self.listData.rpt.count      = \(self.listData.rpt.count)")
            
            // if self.listData.rpt.count > 0 {
            for (index,_) in self.listData.rpt.enumerated()
            {
                if let intStatus = self.listData.rpt[index].status as? Int
                {
                    var strMonth = String()
                    strMonth     = self.listData.rpt[index].date.dateChangeFormat()
                    
                    switch intStatus
                    {
                    case 1:
                        self.arrayPresent.append(strMonth)
                        if  /self.listData.rpt[index].isLate {
                            self.arrayLate.append(strMonth)
                        }
                    case 2:
                        self.arrayAbsent.append(strMonth)
                    case 3:
                        self.arrayLeave.append(strMonth)
                    default:
                        break
                    }
                }
            }
            
            print("arrayPresent = \(self.arrayPresent)")
            print("arrayAbsent       = \(self.arrayAbsent)")
            print("arrayLeave        = \(self.arrayLeave)")
            print("arrayHoliday      = \(self.arrayHoliday)")
            print("arrayLate         = \(self.arrayLate)")
            
            sharedInstance.arrayPresentModal.removeAll()
            sharedInstance.arrayAbsentModal.removeAll()
            sharedInstance.arrayLeaveModal.removeAll()
            sharedInstance.arrayHolidayModal.removeAll()
            sharedInstance.arrayPresentModal = self.arrayPresent
            sharedInstance.arrayAbsentModal  = self.arrayAbsent
            sharedInstance.arrayLeaveModal   = self.arrayLeave
            sharedInstance.arrayHolidayModal = self.arrayHoliday
            sharedInstance.arrayLateModal = self.arrayLate
            
            self.todayAttType = nil
            
            if self.arrayPresent.count > 0
            {
                if self.arrayPresent.contains(self.currentDateString)
                {
                    print("Value has P")
                    self.todayAttType = "P"
                }
            }
            
            if self.arrayAbsent.count > 0 && self.todayAttType == nil {
                if self.arrayAbsent.contains(self.currentDateString)
                {
                    print("Value has A")
                    self.todayAttType = "A"
                }
            }
            
            if self.arrayLeave.count > 0 && self.todayAttType == nil {
                if self.arrayLeave.contains(self.currentDateString)
                {
                    print("Value has L")
                    self.todayAttType = "L"
                }
            }
            
            self.loadUI()
        })
    }
    // MARK: - loadUI
    func loadUI()  {
        
        DispatchQueue.main.async(execute: {
            
            
            self.btnSession.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
            self.btnSession.setTitle("\(self.listData.Session)",for: .normal)
            
            self.monthsNew.removeAll()
           
            for index in 0..<12
            {
                var monthsNewDict = [String: Any]()
                monthsNewDict["MonthName"] = self.months[index]
                monthsNewDict["Id"]        = GetMonth.getMonthIndex(monthName: self.months[index])
                monthsNewDict["year"]      = self.sessionYearMonthWise[index]
                self.monthsNew.append(monthsNewDict)
            }
            print("monthsNew = \(self.monthsNew)")
            self.collectionViewAttendance?.delegate   = self
            self.collectionViewAttendance?.dataSource = self
            self.tblViewAttendance?.delegate          = self
            self.tblViewAttendance?.dataSource        = self
            
            self.collectionViewAttendance?.reloadData()
            self.tblViewAttendance?.reloadData()
                       self.collectionViewAttendance?.performBatchUpdates({},
                                                               completion: { (finished) in
                                                                // print("collection-view finished reload done")
                                                                if !self.boolAPI{
                                                                    self.collectionViewAttendance?.collectionViewLayout.invalidateLayout()
                                                                    let indexPathForFirstRow = IndexPath(row: self.intscrollToItem, section: 0)
                                                                    // self.collectionViewAttendance?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                                                    self.collectionView(self.collectionViewAttendance!, didSelectItemAt: indexPathForFirstRow)
                                                                    
                                                                    self.collectionViewAttendance?.scrollToItem(at: indexPathForFirstRow, at: .centeredHorizontally, animated: true)
                                                                    
                                                                    if self.listData.rpt.count == 0{
                                                                        self.tblViewAttendance?.setContentOffset(CGPoint(x:0,y:0), animated: true)
                                                                    }}
            })
        })
        //lblHeader.text    = listData.Title
        //lblSubHeader.text = listData.EventDate
        
    }
    
    // MARK: - collectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return monthsNew.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellAttendance", for: indexPath) as! CustomAttendanceCollectionViewCell
        cell.lblMonthName?.text = monthsNew[indexPath.row]["MonthName"] as? String //months[indexPath.row]
        
        if eventIndex == indexPath.row {
            cell.lblMonthName?.font      =  R.font.ubuntuMedium(size: 17.0)
            cell.lblMonthName?.textColor = UIColor(white: 1, alpha: 1)
        }else {
            cell.lblMonthName?.font      =  R.font.ubuntuRegular(size: 17.0)
            cell.lblMonthName?.textColor = UIColor(white: 1, alpha: 0.42)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        self.tblViewAttendance?.setContentOffset(CGPoint(x:0,y:0), animated: true)
        self.todayAttType = nil
        
        
        collectionView.setContentOffset(CGPoint.zero, animated: true)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        let intID        =  monthsNew[indexPath.row]["Id"] as? Int
        let intYear      = monthsNew[indexPath.row]["year"] as? Int
        intscrollToItem  = intID!
        eventIndex       = indexPath.row
        monthIndex       = intID
        
        yearStarted     = intYear!
        
        if (boolAPI){
            getAttendanceData(intMonth: intID!, intYear: intYear!)
        }
        boolAPI = true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 3.0) , height: 40.0)
    }
    // MARK: - tableView Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "calendarCell", for: indexPath) as? CalendarCell else {return UITableViewCell()}
            cell.configure()
            
            let tempmonthTobeShown = monthIndex!
            var monthTobeShown     = tempmonthTobeShown
            
            if monthTobeShown > 12 {
                var tempMonth = monthTobeShown % 12
                if tempMonth == 0 {
                    tempMonth = 12
                }
                monthTobeShown = tempMonth
            }
            
            year = yearStarted
            let numberOfYears = tempmonthTobeShown / 12 // to get year with january
            if numberOfYears > 0 {
                for _ in 0...numberOfYears - 1 {
                    year = year + 1
                }
            }
            if monthTobeShown == 12 {
                sharedInstance.createCalendar(withMonth: monthTobeShown , andYear: year-1, view: cell.calView ?? UIView())
            } else {
                sharedInstance.createCalendar(withMonth: monthTobeShown , andYear: year, view: cell.calView ?? UIView())
            }
            //            if noDataForaParticularMonth {
            //                self.heightTableView?.constant = /self.tblViewAttendance?.contentSize.height
            //            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodayCell", for: indexPath) as! TodayCell
            
            
            let now                  = NSDate()
            let dateFormatter        = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM, yyyy"
            
            let currentDateString    = dateFormatter.string(from: now as Date)
            cell.lblDate?.text        = currentDateString
            
            if todayAttType == "P"
            {
                cell.todayStatusLbl?.text  = "Present Today"
                cell.lblStatus?.text = "P"
                cell.lblStatus?.textColor = UIColor(rgb: 0x78CC41)
                cell.backgroundColor = UIColor(rgb: 0x78CC41)
            }
            else if todayAttType == "A"{
                cell.todayStatusLbl?.text  = "Absent Today"
                cell.lblStatus?.text = "A"
                cell.backgroundColor = UIColor(rgb: 0xFB4E4E)
                cell.lblStatus?.textColor = UIColor(rgb: 0xFB4E4E)
            }
            else {
                cell.todayStatusLbl?.text  = "On Leave today"
                cell.lblStatus?.text = "L"
                cell.backgroundColor = UIColor(rgb: 0x44A8EB)
                cell.lblStatus?.textColor = UIColor(rgb: 0x44A8EB)
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
            
            cell.lblSchoolDays?.text = "School days\n\(listData.WorkingDays)"
            cell.lblPresent?.text    = "Present\n\(listData.PresentDays)"
            cell.lblAbsent?.text     = "Absent\n\(listData.AbsentDays)"
            cell.lblLeave?.text      = "Leave\n\(listData.LeaveDays)"
            cell.lblLateDays?.text      = self.isLateEnabled ? "Late Days\n\(listData.LateDays)" : nil
            cell.lblLateDays?.isHidden  = !self.isLateEnabled
            return cell
            
        case 3:
            let cell     = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! HeaderCell
            var strMonth = String()
            strMonth     = listData.StartDate.dateChangeFormatMonth()
            cell.lblDate?.text = "From \(strMonth) - Till Today"
            
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDetailCell", for: indexPath) as! ReportDetailCell
            
            cell.lblTotalSchoolDays?.text = "\(listData.SchoolDays)"
            cell.lblTotalPresents?.text   = "\(listData.TotalPresent)"
            cell.lblTotalAbsents?.text    = "\(listData.TotalAbsent)"
            cell.lblTotalLeaves?.text     = "\(listData.TotalLeave)"
            cell.lblTotalLateDays?.text     = self.isLateEnabled ? "\(listData.TotalLateDays)" : nil
            cell.lblTotalLateDays?.isHidden  = !self.isLateEnabled
            cell.lblLateDaysValue?.isHidden = !self.isLateEnabled
            self.heightTableView?.constant = /self.tblViewAttendance?.contentSize.height
            return cell
        default:
            fatalError()
        }
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            let screen        = UIScreen.main.bounds
            if screen.size.height > 568{
                return 390
            }
            else{
                return 300
            }
            
        case 1:
            if self.todayAttType == nil{
                return 0
            }
            else{
                return 73
            }
        case 2:
            return 73
        case 3:
            return 48
        case 4:
            if self.isLateEnabled  {return 220.0}  else { return 183.0}
        default:
            return 0
        }
    }
    
    // MARK: - ClkBtnSession
    @objc func ClkBtnSession(sender:UIButton){
        self.performSegue(withIdentifier: "attendanceSegue", sender: self)
    }
    
    
    // MARK: - prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "attendanceSegue" {
            let segueAtten        = segue.destination as! AttendanceYearlyViewController
            segueAtten.strYearID  = "\(self.listData.YrID)"//intYearID
            segueAtten.strSession = "\(self.listData.Session)"//intYearID
        }
    }
}
class CalendarCell : UITableViewCell {
    
    @IBOutlet weak var calView: UIView?
    override func awakeFromNib() {
    }
    
    func configure(){
        sharedInstance.weekDayNamesArray = sharedInstance.returnWeekDayArray()
        calView?.frame.size.width = screenWidth
    }
}
class TodayCell : UITableViewCell {
    
    @IBOutlet weak var lblStatus:      UILabel?
    @IBOutlet weak var todayStatusLbl: UILabel?
    @IBOutlet weak var lblDate:        UILabel?
    
    override func awakeFromNib() {
        lblStatus?.layer.cornerRadius = (lblStatus?.frame.width)! / 2
        lblStatus?.clipsToBounds      = true
    }
}
class HeaderCell : UITableViewCell {
    @IBOutlet weak var lblDate: UILabel?
}

class ReportCell : UITableViewCell {
    @IBOutlet weak var lblSchoolDays: UILabel?
    @IBOutlet weak var lblPresent:    UILabel?
    @IBOutlet weak var lblAbsent:     UILabel?
    @IBOutlet weak var lblLeave:      UILabel?
    @IBOutlet weak var lblLateDays : UILabel?
    
}

class ReportDetailCell : UITableViewCell {
    @IBOutlet weak var lblTotalSchoolDays: UILabel?
    @IBOutlet weak var lblTotalPresents:   UILabel?
    @IBOutlet weak var lblTotalAbsents:    UILabel?
    @IBOutlet weak var lblTotalLeaves:    UILabel?
    @IBOutlet weak var  lblTotalLateDays: UILabel?
    @IBOutlet weak var lblLateDaysValue : UILabel?
    
}

extension AttendanceViewController  {
    
    func configure() {
        
        var percentFloat =  Float()
        var AbsentFloat  =  Float()
        var leaveFloat   =  Float()
        var lateDaysFloat =  Float()
        percentFloat     = (Float((Float(listData.TotalPresent) / Float(listData.SchoolDays)) )) * 100
        AbsentFloat      = (Float((Float(listData.TotalAbsent) / Float(listData.SchoolDays))  )) * 100
        leaveFloat       = (Float((Float(listData.TotalLeave) / Float(listData.SchoolDays))  )) * 100
        lateDaysFloat    = (Float((Float(listData.TotalLateDays) / Float(listData.SchoolDays))  )) * 100
        
        
        if !(percentFloat.isNaN || AbsentFloat.isInfinite || leaveFloat.isNaN)
        {
            lblPresentCircle?.text  = "\( String(format: "%.2f",percentFloat))%"
            lblAbsentCircle?.text   = "\(String(format: "%.2f",AbsentFloat))%"
            lblLeaveCircle?.text    = "\(String(format: "%.2f",leaveFloat))%"
            lblLateCircle?.text     = "\(String(format: "%.2f",lateDaysFloat))%"
        }
        else{
            lblPresentCircle?.text  = "0%"
            lblAbsentCircle?.text   = "0%"
            lblLeaveCircle?.text    = "0%"
            lblLateCircle?.text     = "0%"
        }
        
        lblLateCircle?.isHidden              = !(/isLateEnabled)
        lblLateDaysValue?.isHidden           = !(/self.isLateEnabled)
        lblPresentCircle?.frame = CGRect(x:  /lblPresentCircle?.frame.origin.x, y: /lblPresentCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        lblAbsentCircle?.frame = CGRect(x:  /lblAbsentCircle?.frame.origin.x, y: /lblAbsentCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        lblLeaveCircle?.frame = CGRect(x:  /lblLeaveCircle?.frame.origin.x, y: /lblLeaveCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        
        
        lblPresentCircle?.layer.cornerRadius = (/lblPresentCircle?.frame.size.width) / 2
        lblAbsentCircle?.layer.cornerRadius  = (/lblAbsentCircle?.frame.size.width) / 2
        lblLeaveCircle?.layer.cornerRadius   = (/lblLeaveCircle?.frame.size.width) / 2
        lblLateCircle?.frame = CGRect(x:  /lblLateCircle?.frame.origin.x, y: /lblLateCircle?.frame.origin.y, width: /self.isLateEnabled ? actualWidth : 0, height: actualWidth)
        if /self.isLateEnabled {
            lblLateCircle?.frame = CGRect(x:  /lblLateCircle?.frame.origin.x, y: /lblLateCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
            lblLateCircle?.layer.cornerRadius  = (/lblLateCircle?.frame.size.width) / 2
        }
    }
}

