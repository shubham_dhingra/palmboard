import UIKit
import EZSwiftExtensions

class AttendanceYearlyViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    var months = [String]()
    var monthsNew = [[String: Any]]()
    var monthWiseCalendarArr = [UIView]()
    
    //  var firstMonth      = 4
    private  var currentYear     = Int()
    private var yearStarted     = Int()
    var strSender       = String()
    var intUserID       = Int()
    var intUserType     = Int()
    var arrayAttendance = [[String: Any]]()
    var allActionsDic   = [String:Any]()
    var allKeys         = [String]()
    var cellSize : CGSize?
    var strYearID      = String()
    var strSession     = String()
    var monthViewIndex = Int()
    
    @IBOutlet weak var collectionViewYearly: UICollectionView?
    
    var arrayApril     = [[String:Any]]()
    var arrayMay       = [[String:Any]]()
    var arrayJune      = [[String:Any]]()
    var arrayJuly      = [[String:Any]]()
    var arrayAugust    = [[String:Any]]()
    var arraySeptember = [[String:Any]]()
    var arrayOctober   = [[String:Any]]()
    var arrayNovember  = [[String:Any]]()
    var arrayDecember  = [[String:Any]]()
    var arrayJanuary   = [[String:Any]]()
    var arrayFebruary  = [[String:Any]]()
    var arrayMarch     = [[String:Any]]()
    
    //var arrayHolidayApril = [String]()
    var arrayPresentApril = [String]()
    var arrayAbsentApril  = [String]()
    var arrayLeaveApril   = [String]()
    
    // var arrayHolidayMay = [String]()
    var arrayPresentMay = [String]()
    var arrayAbsentMay  = [String]()
    var arrayLeaveMay   = [String]()
    
    //var arrayHolidayJune = [String]()
    var arrayPresentJune = [String]()
    var arrayAbsentJune  = [String]()
    var arrayLeaveJune   = [String]()
    
    // var arrayHolidayJuly = [String]()
    var arrayPresentJuly = [String]()
    var arrayAbsentJuly  = [String]()
    var arrayLeaveJuly   = [String]()
    
    //var arrayHolidayAugust = [String]()
    var arrayPresentAugust = [String]()
    var arrayAbsentAugust  = [String]()
    var arrayLeaveAugust   = [String]()
    
    // var arrayHolidaySeptember = [String]()
    var arrayPresentSeptember = [String]()
    var arrayAbsentSeptember  = [String]()
    var arrayLeaveSeptember   = [String]()
    
    // var arrayHolidayOctober = [String]()
    var arrayPresentOctober = [String]()
    var arrayAbsentOctober  = [String]()
    var arrayLeaveOctober   = [String]()
    
    //var arrayHolidayNovember = [String]()
    var arrayPresentNovember = [String]()
    var arrayAbsentNovember  = [String]()
    var arrayLeaveNovember   = [String]()
    
    //var arrayHolidayDecember = [String]()
    var arrayPresentDecember = [String]()
    var arrayAbsentDecember  = [String]()
    var arrayLeaveDecember   = [String]()
    
    //var arrayHolidayJanuary = [String]()
    var arrayPresentJanuary = [String]()
    var arrayAbsentJanuary  = [String]()
    var arrayLeaveJanuary   = [String]()
    
    // var arrayHolidayFebruary = [String]()
    var arrayPresentFebruary = [String]()
    var arrayAbsentFebruary  = [String]()
    var arrayLeaveFebruary   = [String]()
    
    // var arrayHolidayMarch    = [String]()
    var arrayPresentMarch    = [String]()
    var arrayAbsentMarch     = [String]()
    var arrayLeaveMarch      = [String]()
    var btnTitle             = UIButton()
    
    var intID                =  Int()
    var intYear              = Int()
    var sessionYearMonthWise = [Int]()
    
    lazy var sessionStartYear : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
    
    lazy var sessionNextYear : Int = {
        guard let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return Calendar.current.component(.year, from: Date())
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionEnd) else{
            return Calendar.current.component(.year, from: Date())
        }
        return date.year
    }()
    
    
    lazy var sessionStartMonth : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart else{
            return Calendar.current.component(.month, from: Date())
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: sessionStart) else{
            return Calendar.current.component(.month, from: Date())
        }
        return date.month
    }()
    
    
    lazy var noOfMonthsForParticularSession : Int = {
        guard let sessionStart = self.getCurrentUser()?.sessionStart , let sessionEnd = self.getCurrentUser()?.sessionEnd else{
            return 0
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let sessionStartDate = dateFormatter.date(from: sessionStart) , let sessionStartEnd = dateFormatter.date(from: sessionEnd) else{
            return 0
        }
        
        return Calendar.current.dateComponents([.month], from: sessionStartDate, to: sessionStartEnd).month ?? 0
    }()
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMonthView()
        
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        // guard let themeColor = self.getCurrentSchool()?.themColor else { return}
        
        
        let layout          = self.collectionViewYearly?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
        
        let stringDash = strSession.components(separatedBy: "-")
        self.yearStarted = Int(stringDash[0])!
        /********************************************************************/
        self.monthsNew.removeAll()
        // var intCurrentYear = Int()
        // var intNextYear    = Int()
        
        //let stringArray = strSession.components(separatedBy: "-")
        
        // intCurrentYear  = Int(stringArray[0])!
        //intNextYear     = Int(stringArray[1])!
        
        for index in 0..<12
        {
            var monthsNewDict = [String: Any]()
            monthsNewDict["MonthName"] = self.months[index]
            monthsNewDict["Id"]        = GetMonth.getMonthIndex(monthName: self.months[index])
            monthsNewDict["year"]      = self.sessionYearMonthWise[index]
            self.monthsNew.append(monthsNewDict)
            
        }
        ////print("monthsNew = \(self.monthsNew)")
        /********************************************************************/
        btnTitle                =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle(strSession, for: UIControl.State.normal)//"Attendance"
        //btnTitle.setTitle(str, for: UIControl.State.normal)
        // btnTitle.setAttributedTitle(str, for: .normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.titleLabel?.textAlignment = .left
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        btnTitle.titleLabel?.numberOfLines = 0
        //  btnTitle.backgroundColor = UIColor.red
        self.navigationItem.prompt = "Attendance"//"(2017 - 2018)"
        self.navigationItem.titleView = btnTitle
        
    }
    func setUpMonthView() {
        
        print("month = \(self.sessionStartMonth)")
        print("year = \(self.sessionStartYear)")
        print("No of months for a particular Session = \(self.noOfMonthsForParticularSession)")
        var startMonth = sessionStartMonth
        var index = 0
        sessionYearMonthWise = []
        var YearToAppend : Int = sessionStartYear
        while(index < 12){
            
            
            if startMonth > 12 {
                startMonth = 1
                YearToAppend = sessionNextYear
            }
            months.append(GetMonth.getMonthName(index : startMonth))
            startMonth = startMonth + 1
            sessionYearMonthWise.append(YearToAppend)
            index = index + 1
        }
        print("Session Month Wise  \(sessionYearMonthWise)")
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            intUserType =  Int(userType)
        }
        
        self.collectionViewYearly?.isHidden   = true
        self.collectionViewYearly?.dataSource = nil
        self.collectionViewYearly?.delegate   = nil
        
        self.webApiGridAttendance()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    
    // MARK: - webApiGridAttendance
    func webApiGridAttendance()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        
        urlString += "Academic/YearlyAttendance?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&YrID=\(strYearID)"//intUserID
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    // //print("Result == \(result)")
                    DispatchQueue.main.async {
                        // self.activityInd.stopAnimating()
                        // self.collectionViewAlbums.isHidden = false
                        
                        if let arrayAll = (result["RPT"] as? [[String: Any]]){
                            self.arrayAttendance = arrayAll
                        }
                        if self.arrayAttendance.count == 0{
                            ////print("self.arrayAttendance.count == \(self.arrayAttendance.count)")
                        }
                        else
                        {   ////print("arrayAttendance      = \(self.arrayAttendance)")
                            ////print("arrayAttendanceCount = \(self.arrayAttendance.count)")
                            self.allActionsDic.removeAll()
                            
                            for rec in self.arrayAttendance
                            {
                                var actionRecordDic = [String:Any]()
                                
                                let date   = rec["AttDate"] as? String ?? ""
                                let Status = rec["Status"]  as? Int ?? 0
                                let isLate = rec["isLate"] as? Bool ?? false
                                actionRecordDic["AttDate"] = date
                                actionRecordDic["Status"]  = Status
                                actionRecordDic["isLate"] = isLate
                                ////print("actionRecordDic = \(actionRecordDic)")
                                var found   = false
                                let strDueDate = rec["AttDate"] as? String ?? ""
                                
                                var sectionName = String()
                                //sectionName     = self.DateGetMonthName(serverDate: strDueDate)  //rec["AttDate"] as? String ?? ""
                                sectionName = strDueDate.DateGetMonthName()
                                
                                for (key, _) in self.allActionsDic
                                {
                                    if key == sectionName
                                    {
                                        var array = self.allActionsDic[sectionName] as! [[String : Any]]
                                        array.append(actionRecordDic)
                                        found     = true
                                        self.allActionsDic[sectionName] = array
                                        break
                                    }
                                }
                                if(found == false)
                                {
                                    let array = [actionRecordDic]//[[String:Any]]()
                                    //array.append(actionRecordDic)
                                    self.allActionsDic[sectionName] = array
                                    self.allKeys.append(sectionName)
                                }
                            }
                        }
                        //print("self.allKeys = \(self.allKeys)")
                        //print("self.allActionsDic = \(self.allActionsDic)")
                        ez.runThisInMainThread {
                            self.TraverseData(arrayKeys: self.allKeys, dictMonths: self.allActionsDic)
                        }
                    }
                }
                else{
                    // self.activityInd.stopAnimating()
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    // MARK: - TraverseData method
    func TraverseData(arrayKeys: [String], dictMonths: [String: Any])
    {
        if let April   = dictMonths["April"] as? [[String : Any]]{
            arrayApril = April
        }
        if let May   = dictMonths["May"]     as? [[String : Any]]{
            arrayMay = May
        }
        if let June   = dictMonths["June"]   as? [[String : Any]]{
            arrayJune = June
        }
        if let july   = dictMonths["July"]    as? [[String : Any]]{
            arrayJuly = july
        }
        if let August   = dictMonths["August"] as? [[String : Any]]{
            arrayAugust = August
        }
        if let September   = dictMonths["September"] as? [[String : Any]]{
            arraySeptember = September
        }
        if let October   = dictMonths["October"]   as? [[String : Any]]{
            arrayOctober = October
        }
        if let November   = dictMonths["November"] as? [[String : Any]]{
            arrayNovember = November
        }
        if let December   = dictMonths["December"] as? [[String : Any]]{
            arrayDecember = December
        }
        if let January    = dictMonths["January"]  as? [[String : Any]]{
            arrayJanuary  = January
        }
        if let February   = dictMonths["February"] as? [[String : Any]]{
            arrayFebruary = February
        }
        if let March   = dictMonths["March"] as? [[String : Any]]{
            arrayMarch = March
        }
        
        if arrayApril.count > 0{
            if let value =  self.returnArray(array: arrayApril)
            {
                self.arrayPresentApril = value.Present
                self.arrayAbsentApril  = value.Absent
                self.arrayLeaveApril   = value.Leave
            }
        }
        if arrayMay.count > 0{
            if let value =  self.returnArray(array: arrayMay)
            {
                self.arrayPresentMay = value.Present
                self.arrayAbsentMay  = value.Absent
                self.arrayLeaveMay   = value.Leave
            }
        }
        if arrayJune.count > 0{
            if let value =  self.returnArray(array: arrayJune)
            {
                self.arrayPresentJune = value.Present
                self.arrayAbsentJune  = value.Absent
                self.arrayLeaveJune   = value.Leave
            }
        }
        if arrayJuly.count > 0{
            if let value =  self.returnArray(array: arrayJuly)
            {
                self.arrayPresentJuly = value.Present
                self.arrayAbsentJuly  = value.Absent
                self.arrayLeaveJuly   = value.Leave
            }
        }
        if arrayAugust.count > 0{
            if let value =  self.returnArray(array: arrayAugust)
            {
                self.arrayPresentAugust = value.Present
                self.arrayAbsentAugust  = value.Absent
                self.arrayLeaveAugust   = value.Leave
            }
        }
        if arraySeptember.count > 0{
            if let value =  self.returnArray(array: arraySeptember)
            {
                self.arrayPresentSeptember = value.Present
                self.arrayAbsentSeptember  = value.Absent
                self.arrayLeaveSeptember   = value.Leave
            }
        }
        if arrayOctober.count > 0{
            if let value =  self.returnArray(array: arrayOctober)
            {
                self.arrayPresentOctober = value.Present
                self.arrayAbsentOctober  = value.Absent
                self.arrayLeaveOctober   = value.Leave
            }
        }
        if arrayNovember.count > 0{
            if let value =  self.returnArray(array: arrayNovember)
            {
                self.arrayPresentNovember = value.Present
                self.arrayAbsentNovember  = value.Absent
                self.arrayLeaveNovember   = value.Leave
            }
        }
        if arrayDecember.count > 0{
            if let value =  self.returnArray(array: arrayDecember)
            {
                self.arrayPresentDecember = value.Present
                self.arrayAbsentDecember  = value.Absent
                self.arrayLeaveDecember   = value.Leave
            }
        }
        if arrayJanuary.count > 0{
            if let value =  self.returnArray(array: arrayJanuary)
            {
                self.arrayPresentJanuary = value.Present
                self.arrayAbsentJanuary  = value.Absent
                self.arrayLeaveJanuary   = value.Leave
            }
        }
        if arrayFebruary.count > 0{
            if let value =  self.returnArray(array: arrayFebruary)
            {
                self.arrayPresentFebruary = value.Present
                self.arrayAbsentFebruary  = value.Absent
                self.arrayLeaveFebruary   = value.Leave
            }
        }
        if arrayMarch.count > 0{
            if let value =  self.returnArray(array: arrayMarch)
            {
                self.arrayPresentMarch = value.Present
                self.arrayAbsentMarch  = value.Absent
                self.arrayLeaveMarch   = value.Leave
            }
        }
        sharedInstanceYearly.arrayPresentAprilModal.removeAll()
        sharedInstanceYearly.arrayPresentMayModal.removeAll()
        sharedInstanceYearly.arrayPresentJuneModal.removeAll()
        sharedInstanceYearly.arrayPresentJulyModal.removeAll()
        sharedInstanceYearly.arrayPresentAugustModal.removeAll()
        sharedInstanceYearly.arrayPresentSeptemberModal.removeAll()
        sharedInstanceYearly.arrayPresentOctoberModal.removeAll()
        sharedInstanceYearly.arrayPresentNovemberModal.removeAll()
        sharedInstanceYearly.arrayPresentDecemberModal.removeAll()
        sharedInstanceYearly.arrayPresentJanuaryModal.removeAll()
        sharedInstanceYearly.arrayPresentFebruaryModal.removeAll()
        sharedInstanceYearly.arrayPresentMarchModal.removeAll()
        
        sharedInstanceYearly.arrayAbsentAprilModal.removeAll()
        sharedInstanceYearly.arrayAbsentMayModal.removeAll()
        sharedInstanceYearly.arrayAbsentJuneModal.removeAll()
        sharedInstanceYearly.arrayAbsentJulyModal.removeAll()
        sharedInstanceYearly.arrayAbsentAugustModal.removeAll()
        sharedInstanceYearly.arrayAbsentSeptemberModal.removeAll()
        sharedInstanceYearly.arrayAbsentOctoberModal.removeAll()
        sharedInstanceYearly.arrayAbsentNovemberModal.removeAll()
        sharedInstanceYearly.arrayAbsentDecemberModal.removeAll()
        sharedInstanceYearly.arrayAbsentJanuaryModal.removeAll()
        sharedInstanceYearly.arrayAbsentFebruaryModal.removeAll()
        sharedInstanceYearly.arrayAbsentMarchModal.removeAll()
        
        sharedInstanceYearly.arrayLeaveAprilModal.removeAll()
        sharedInstanceYearly.arrayLeaveMayModal.removeAll()
        sharedInstanceYearly.arrayLeaveJuneModal.removeAll()
        sharedInstanceYearly.arrayLeaveJulyModal.removeAll()
        sharedInstanceYearly.arrayLeaveAugustModal.removeAll()
        sharedInstanceYearly.arrayLeaveSeptemberModal.removeAll()
        sharedInstanceYearly.arrayLeaveOctoberModal.removeAll()
        sharedInstanceYearly.arrayLeaveNovemberModal.removeAll()
        sharedInstanceYearly.arrayLeaveDecemberModal.removeAll()
        sharedInstanceYearly.arrayLeaveJanuaryModal.removeAll()
        sharedInstanceYearly.arrayLeaveFebruaryModal.removeAll()
        sharedInstanceYearly.arrayLeaveMarchModal.removeAll()
        
        sharedInstanceYearly.arrayHolidayAprilModal.removeAll()
        sharedInstanceYearly.arrayHolidayMayModal.removeAll()
        sharedInstanceYearly.arrayHolidayJuneModal.removeAll()
        sharedInstanceYearly.arrayHolidayJulyModal.removeAll()
        sharedInstanceYearly.arrayHolidayAugustModal.removeAll()
        sharedInstanceYearly.arrayHolidaySeptemberModal.removeAll()
        sharedInstanceYearly.arrayHolidayOctoberModal.removeAll()
        sharedInstanceYearly.arrayHolidayNovemberModal.removeAll()
        sharedInstanceYearly.arrayHolidayDecemberModal.removeAll()
        sharedInstanceYearly.arrayHolidayJanuaryModal.removeAll()
        sharedInstanceYearly.arrayHolidayFebruaryModal.removeAll()
        sharedInstanceYearly.arrayHolidayMarchModal.removeAll()
        
        sharedInstanceYearly.arrayPresentAprilModal     = self.arrayPresentApril
        sharedInstanceYearly.arrayPresentMayModal       = self.arrayPresentMay
        sharedInstanceYearly.arrayPresentJuneModal      = self.arrayPresentJune
        sharedInstanceYearly.arrayPresentJulyModal      = self.arrayPresentJuly
        sharedInstanceYearly.arrayPresentAugustModal    = self.arrayPresentAugust
        sharedInstanceYearly.arrayPresentSeptemberModal = self.arrayPresentSeptember
        sharedInstanceYearly.arrayPresentOctoberModal  = self.arrayPresentOctober
        sharedInstanceYearly.arrayPresentNovemberModal = self.arrayPresentNovember
        sharedInstanceYearly.arrayPresentDecemberModal = self.arrayPresentDecember
        sharedInstanceYearly.arrayPresentJanuaryModal  = self.arrayPresentJanuary
        sharedInstanceYearly.arrayPresentFebruaryModal = self.arrayPresentFebruary
        sharedInstanceYearly.arrayPresentMarchModal    = self.arrayPresentMarch
        
        sharedInstanceYearly.arrayAbsentAprilModal     = self.arrayAbsentApril
        sharedInstanceYearly.arrayAbsentMayModal       = self.arrayAbsentMay
        sharedInstanceYearly.arrayAbsentJuneModal      = self.arrayAbsentJune
        sharedInstanceYearly.arrayAbsentJulyModal      = self.arrayAbsentJuly
        sharedInstanceYearly.arrayAbsentAugustModal    = self.arrayAbsentAugust
        sharedInstanceYearly.arrayAbsentSeptemberModal = self.arrayAbsentSeptember
        sharedInstanceYearly.arrayAbsentOctoberModal   = self.arrayAbsentOctober
        sharedInstanceYearly.arrayAbsentNovemberModal  = self.arrayAbsentNovember
        sharedInstanceYearly.arrayAbsentDecemberModal  = self.arrayAbsentDecember
        sharedInstanceYearly.arrayAbsentJanuaryModal   = self.arrayAbsentJanuary
        sharedInstanceYearly.arrayAbsentFebruaryModal  = self.arrayAbsentFebruary
        sharedInstanceYearly.arrayAbsentMarchModal     = self.arrayAbsentMarch
        
        
        sharedInstanceYearly.arrayLeaveAprilModal      = self.arrayLeaveApril
        sharedInstanceYearly.arrayLeaveMayModal        = self.arrayLeaveMay
        sharedInstanceYearly.arrayLeaveJuneModal       = self.arrayLeaveJune
        sharedInstanceYearly.arrayLeaveJulyModal       = self.arrayLeaveJuly
        sharedInstanceYearly.arrayLeaveAugustModal     = self.arrayLeaveAugust
        sharedInstanceYearly.arrayLeaveSeptemberModal  = self.arrayLeaveSeptember
        sharedInstanceYearly.arrayLeaveOctoberModal    = self.arrayLeaveOctober
        sharedInstanceYearly.arrayLeaveNovemberModal   = self.arrayLeaveNovember
        sharedInstanceYearly.arrayLeaveDecemberModal   = self.arrayLeaveDecember
        sharedInstanceYearly.arrayLeaveJanuaryModal    = self.arrayLeaveJanuary
        sharedInstanceYearly.arrayLeaveFebruaryModal   = self.arrayLeaveFebruary
        sharedInstanceYearly.arrayLeaveMarchModal      = self.arrayLeaveMarch
        
        self.collectionViewYearly?.isHidden   = false
        self.collectionViewYearly?.dataSource = self
        self.collectionViewYearly?.delegate   = self
        createCalendarMonthWise()
    }
    func returnArray(array: [[String: Any]]?)->(Present: [String],Absent: [String],Leave: [String])? {
        
        var arrayPresent = [String]()
        var arrayAbsent  = [String]()
        var arrayLeave   = [String]()
        
        for (index,_) in (array?.enumerated())!
        {
            if let intStatus = array?[index]["Status"] as? Int
            {
                var strMonth = String()
                strMonth     = array?[index]["AttDate"] as! String
                strMonth     = strMonth.dateChangeFormat()
                switch intStatus
                {
                case 1:
                    arrayPresent.append(strMonth)
                case 2:
                    arrayAbsent.append(strMonth)
                case 3:
                    arrayLeave.append(strMonth)
                default:
                    break
                }
            }
        }
        return (arrayPresent,arrayAbsent,arrayLeave)
    }
    
    func createCalendarMonthWise() {
        var index : Int = 0
        self.monthWiseCalendarArr = []
        var startMonth = sessionStartMonth
        
        while index < 12 {
            let monthView = UIView()
            let tempmonthTobeShown       = startMonth + index //firstMonth + index
            var monthTobeShown           = tempmonthTobeShown
            
            if monthTobeShown > 12 {
                var tempMonth = monthTobeShown % 12
                if tempMonth == 0 {
                    tempMonth = 12
                }
                monthTobeShown = tempMonth
            }
            currentYear = yearStarted
            
            let numberOfYears = tempmonthTobeShown / 12 //to get year with january
            if numberOfYears > 0 {
                for _ in 0...numberOfYears-1 {
                    currentYear = currentYear + 1
                }
            }
            //            cellCollection.viewCalendar?.frame.size = cellSize!
            monthView.frame.size =  CGSize(width: (/collectionViewYearly?.frame.size.width - 20) / 2, height: 252)
            sharedInstanceYearly.weekDayNamesArray = sharedInstanceYearly.returnWeekDayArray()
            if monthTobeShown == 12 {
                sharedInstanceYearly.createCalendarYearly(withMonth: monthTobeShown, andYear: self.currentYear-1, view: monthView, onVC: self)
            }
            else {
                sharedInstanceYearly.createCalendarYearly(withMonth: monthTobeShown, andYear: self.currentYear, view: monthView, onVC: self)
            }
            index = index + 1
        }
        self.collectionViewYearly?.reloadData()
        Utility.shared.removeLoader()
    }
    // MARK: - CollectionViewDelegate & DataSource Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return monthsNew.count   //12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        //        //print("Func: cellForItemAt = \(indexPath.row)")
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellYearlyAttendance", for: indexPath) as! YearCalendarCell
        cellCollection.lblMonth?.text = monthsNew[indexPath.row]["MonthName"] as? String
        cellCollection.viewCalendar?.removeSubviews()
        cellCollection.viewCalendar?.addSubview(monthWiseCalendarArr[indexPath.row])
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        intID        =  monthsNew[indexPath.row]["Id"] as! Int
        intYear      = monthsNew[indexPath.row]["year"] as! Int
        monthViewIndex = indexPath.row
        print("Month Index to Scroll : \(monthViewIndex) Month ID : \(intID)  Year : \(intYear)")
        self.performSegue(withIdentifier: "SeguebackView", sender: self)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        cellSize = CGSize(width: (collectionView.frame.size.width - 20)/2, height: 252)
        return CGSize(width: (cellSize?.width)!, height: 252)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewYearly?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SeguebackView"
        {
            let segueAtten        = segue.destination as! AttendanceViewController
            segueAtten.indexToScroll      = monthViewIndex
            segueAtten.intIDBack     = intID
            segueAtten.intYearBack   = intYear
            segueAtten.intFilterData = 22
        }
    }
}

