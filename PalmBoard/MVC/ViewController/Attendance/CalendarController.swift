//  CalendarController.swift
//  CalendarPage
//  Created by NewMacmini on 12/6/17.
//  Copyright © 2017 NewMacmini. All rights reserved.

import UIKit

let sharedInstance = CalendarController()

class CalendarController: NSObject {
    
    
    var arrayHolidayModal = [String]()
    var arrayPresentModal = [String]()
    var arrayAbsentModal  = [String]()
    var arrayLeaveModal   = [String]()
    var arrayLateModal    = [String]()
    var isYearCal         = false

       // let arrayHoliday = ["02/01/2017","12/01/2017","02/02/2017","02/10/2017","07/05/2017","22/08/2017","30/12/2017"]
        //let arrayPresent = ["02/01/2017","03/01/2017","04/01/2017","05/01/2017","06/01/2017","07/01/2017","09/01/2017","10/01/2017","11/01/2017","12/01/2017","13/01/2017","16/01/2017","18/01/2017","19/01/2017","20/01/2017","23/01/2017","24/01/2017","25/01/2017","30/01/2017","31/01/2017"]
        //let arrayAbsent = ["14/01/2017","17/01/2017","21/01/2017","26/01/2017","28/01/2017","27/01/2017"]
        
        
        var weekDayNamesArray: [String] = Array()
        var currentMonth: Int!
        var currentYear: Int!
        
        var isShowPreviousNextMonthDays:Bool = false
        
        var todayDateBackGroundColor:       UIColor?
        var normalDateColor:                UIColor?
        var selectedDateColor:              UIColor?
        var highlightedDateColor:           UIColor?
        var highlightedDateBackgroundColor: UIColor?
        var disableDateColor:               UIColor?
        var inset:                          CGFloat?
        
        
    func createCalendar(withMonth month:Int, andYear year: Int, view: UIView) {
        
        sharedInstance.currentMonth = month
        sharedInstance.currentYear  = year
        let date         = sharedInstance.returnDateFormatter().date(from:String(format: "1/%d/%d",sharedInstance.currentMonth, sharedInstance.currentYear))//"1/\\\(month)/\\\(year)"
        if let date = date {
            let numberOfDays = sharedInstance.getNumberOfDays(inMonth: month, withDate: date)
            var  weekDay     = sharedInstance.getFirstWeekDay(forDate: date)
            
            
            //Because Monday is the first Week Day!
            if (weekDay == 1)
            {weekDay = 7}
            else
            { weekDay = weekDay - 1}
            createMonthWith(numberofDays: numberOfDays, andStartingWeekDay: (weekDay), view: view)
        }
    }
    func createMonthWith(numberofDays days:Int, andStartingWeekDay weekday:Int , view:UIView)  {
        
        for vw in view.subviews {
            if vw.tag != 300 {
                vw.removeFromSuperview()
            }
        }
        var xPos = 00.0
        var yPos = 00.0
        var startIndex = weekday
        
        let width  = view.frame.size.width / 7//screenWidth / 7
        let height = width
        
        var weekCount = 1
        for weekDaynames in sharedInstance.weekDayNamesArray {
            let weekDayLable = UILabel.init(frame: CGRect(x: xPos, y: yPos, width: Double(width) + 5, height: 38.0))//Double(height)
            weekDayLable.text            = weekDaynames
            weekDayLable.textAlignment   = .center
            weekDayLable.contentMode     = .scaleAspectFit
            weekDayLable.backgroundColor = UIColor(rgb: 0xF3F3F3)
            weekDayLable.font      = R.font.ubuntuRegular(size: 17.0)
            weekDayLable.textColor = UIColor(rgb: 0xBFBFBF)

            view.addSubview(weekDayLable)
            xPos=xPos+Double(width)
            if(weekCount == 7){
                weekDayLable.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
                weekDayLable.font = R.font.ubuntuRegular(size: 17.0)

            }
            weekCount += 1
        }
        
        xPos = 00.0
        yPos = 40//Double(yPos) + Double(height);
        
        for i in 1...7 {
            
            if i == weekday {
                break
            }
            xPos = xPos + Double(width)
        }
        
        
        var lblWidth = 0.0
        var lblHeight = 0.0
        
        if(!isYearCal)
        {
            lblWidth = Double(width)-8
            lblHeight = Double(height) - 8
        }
        else
        {
            lblWidth = Double(width)-4
            lblHeight = Double(height)-4
        }
        
        for date in 1...days {
            
            let dateLable           = UILabel.init(frame: CGRect(x: xPos + 5, y: Double(yPos), width: lblWidth, height: lblHeight))
            dateLable.text          = String(describing: date)
            dateLable.textAlignment = .center
            dateLable.tag           = date
            if(isYearCal){
                dateLable.font = dateLable.font.withSize(14.0)
            }
            let date        = sharedInstance.returnDateFormatter().date(from:String(format: "%d/%d/%d",date, sharedInstance.currentMonth, sharedInstance.currentYear))
            let currentDate = sharedInstance.returnDateFormatter().date(from: sharedInstance.returnDateFormatter().string(from: Date()))
            
            if date?.compare(currentDate!) == .orderedSame {
                //current date
                //dateLable.backgroundColor    = UIColor.purple
               // dateLable.textColor          = UIColor.white
                dateLable.layoutIfNeeded()
                dateLable.clipsToBounds      = true
                dateLable.layer.cornerRadius = (dateLable.frame.size.width)/2
                
            }
            //Mark Sunday Blue
            sharedInstance.markSunday(date: date!, lbl: dateLable)
            ////Marking holidays
//            markHolidays(date: date!, lbl: dateLable)
            ////Marking Present
            sharedInstance.markPresent(date: date!, lbl: dateLable)
            ////Marking Absent
            sharedInstance.markAbsent(date: date!, lbl: dateLable)
            
            view.addSubview(dateLable)
            startIndex = startIndex + 1
            xPos       = xPos+Double(width) //+ 15
            if startIndex == 8 {
                xPos       = 00.0
                yPos       = Double(yPos)+Double(height);
                startIndex = 1;
            }
        }
    }
        func returnDateFormatter() -> DateFormatter {
            let formatter        = DateFormatter.init()
            formatter.dateFormat = "dd/MM/yyyy"
            return formatter
        }
        func returnMonthFormatter() -> DateFormatter {
            let formatter        = DateFormatter.init()
            formatter.dateFormat = "MMMM/yyyy"
            return formatter
        }
        func returnWeekDayArray() -> Array<String> {
            let firstWeekday = 2 // -> Monday
            let formatter    = DateFormatter.init()
            var symbols      = formatter.veryShortWeekdaySymbols
            symbols          = Array(symbols![firstWeekday-1..<(symbols?.count)!]) + symbols![0..<firstWeekday-1]
            //    print(symbols!)
            return symbols!
        }
        
        func getNumberOfDays(inMonth month:Int, withDate date:Date) -> Int {
            let cal              = Calendar.current
            var dateComponents   = DateComponents.init()
            dateComponents.month = month
            let range            = cal.range(of: .day, in: .month, for: date)
            return (range?.count)!
        }
        
        func getFirstWeekDay(forDate date:Date) -> Int {
            let cal = Calendar.current
            let dateComponents = cal.dateComponents([.weekday], from: date)
            return (dateComponents.weekday!)
        }
        func markSunday(date: Date , lbl : UILabel)
        {
            let calendar      = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
            let components    = calendar!.components([.weekday], from: date)
            if components.weekday == 1 {
                lbl.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
            }
        }
        
        func markPresent(date: Date , lbl : UILabel){
            for i in 0..<arrayPresentModal.count{
                let isPresent = returnDateFormatter().date(from: arrayPresentModal[i])
                let isLate =  arrayLateModal.contains(arrayPresentModal[i])
                if date.compare(isPresent!) == .orderedSame {
                    lbl.backgroundColor    = isLate ? UIColor(rgb: 0xffbb00) : UIColor(rgb: 0x78CC41) //UIColor.green
                    lbl.textColor          = UIColor.white
                    lbl.layoutIfNeeded()
                    lbl.clipsToBounds      = true
                    lbl.layer.cornerRadius = (lbl.frame.size.width)/2
                }
            }
        }
        func markAbsent(date: Date , lbl : UILabel){
            for i in 0..<arrayAbsentModal.count{
                let isAbsent = returnDateFormatter().date(from: arrayAbsentModal[i])
               // print("isAbsent")
                if date.compare(isAbsent!) == .orderedSame {
                    lbl.backgroundColor    = UIColor(rgb: 0xFB4E4E)//UIColor.red
                    lbl.textColor          = UIColor.white
                    lbl.layoutIfNeeded()
                    lbl.clipsToBounds      = true
                    lbl.layer.cornerRadius = (lbl.frame.size.width)/2            }
            }
            self.markMedicalLeave(date: date, lbl: lbl)
        }
        func markHolidays(date: Date , lbl : UILabel){
            for i in 0..<arrayHolidayModal.count{
                let isholiday = returnDateFormatter().date(from: arrayHolidayModal[i])
                if date.compare(isholiday!) == .orderedSame {
//                    lbl.backgroundColor    = UIColor(rgb: 0x44A8EB)//UIColor.blue
//                    lbl.textColor          = UIColor.white
//                    lbl.layoutIfNeeded()
//                    lbl.clipsToBounds      = true
//                    lbl.layer.cornerRadius = (lbl.frame.size.width)/2
                    
                    lbl.backgroundColor = UIColor(rgb: 0x44A8EB)//UIColor.red
                    lbl.textColor       = UIColor.white
                    lbl.numberOfLines   = 0
                    
                    let str = NSMutableAttributedString(string: "\(lbl.tag)\n")
                    let attributedString = NSAttributedString(string: "H", attributes: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 10.0) ])
                    str.addAttribute(NSAttributedString.Key.font, value: R.font.ubuntuRegular(size: 17.0) , range: NSRange(location: 0, length: 2))
                    
//                    if(isYearCal){
//                        let attributedString = NSAttributedString(string: "H", attributes: [NSAttributedStringKey.font: UIFont(name: "Ubuntu-Regular", size: 1.0) ])
//                        str.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Ubuntu-Regular", size: 9.0) , range: NSRange(location: 0, length: 2))
//                    }
                    str.append(attributedString)
                    lbl.attributedText     = str
                    lbl.layoutIfNeeded()
                    lbl.clipsToBounds      = true
                    lbl.layer.cornerRadius = (lbl.frame.size.width)/2

                }
            }
        }
    func markMedicalLeave(date : Date , lbl: UILabel){
        for i in 0..<arrayLeaveModal.count{
            let isMedicalLeave = returnDateFormatter().date(from: arrayLeaveModal[i])
            if date.compare(isMedicalLeave!) == .orderedSame {
                lbl.backgroundColor = UIColor(rgb: 0x44A8EB)//UIColor.red
                lbl.textColor       = UIColor.white
                lbl.numberOfLines   = 0
                
                let str = NSMutableAttributedString(string: "\(lbl.tag)\n")
                let attributedString = NSAttributedString(string: "L", attributes: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 10.0)])
                str.addAttribute(NSAttributedString.Key.font, value: R.font.ubuntuRegular(size: 17.0) , range: NSRange(location: 0, length: 2))
                
//                if(isYearCal){
//                    let attributedString = NSAttributedString(string: "ML", attributes: [NSAttributedStringKey.font: UIFont(name: "Ubuntu-Regular", size: 1.0) ])
//                    str.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Ubuntu-Regular", size: 9.0) , range: NSRange(location: 0, length: 2))
//                }
                str.append(attributedString)
                lbl.attributedText     = str
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
            }
    }
}
}
