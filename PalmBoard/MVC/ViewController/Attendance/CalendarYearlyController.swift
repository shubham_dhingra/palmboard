//  CalendarYearlyController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 12/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

let sharedInstanceYearly = CalendarYearlyController()

class CalendarYearlyController: NSObject {

    var arrayHolidayAprilModal = [String]()
    var arrayPresentAprilModal = [String]()
    var arrayAbsentAprilModal  = [String]()
    var arrayLeaveAprilModal   = [String]()
    
    var arrayHolidayMayModal = [String]()
    var arrayPresentMayModal = [String]()
    var arrayAbsentMayModal  = [String]()
    var arrayLeaveMayModal   = [String]()
    
    var arrayHolidayJuneModal = [String]()
    var arrayPresentJuneModal = [String]()
    var arrayAbsentJuneModal  = [String]()
    var arrayLeaveJuneModal   = [String]()
    
    var arrayHolidayJulyModal = [String]()
    var arrayPresentJulyModal = [String]()
    var arrayAbsentJulyModal  = [String]()
    var arrayLeaveJulyModal  = [String]()
    
    var arrayHolidayAugustModal = [String]()
    var arrayPresentAugustModal = [String]()
    var arrayAbsentAugustModal  = [String]()
    var arrayLeaveAugustModal   = [String]()
    
    var arrayHolidaySeptemberModal = [String]()
    var arrayPresentSeptemberModal = [String]()
    var arrayAbsentSeptemberModal  = [String]()
    var arrayLeaveSeptemberModal   = [String]()
    
    var arrayHolidayOctoberModal = [String]()
    var arrayPresentOctoberModal = [String]()
    var arrayAbsentOctoberModal  = [String]()
    var arrayLeaveOctoberModal   = [String]()
    
    var arrayHolidayNovemberModal = [String]()
    var arrayPresentNovemberModal = [String]()
    var arrayAbsentNovemberModal  = [String]()
    var arrayLeaveNovemberModal   = [String]()
    
    var arrayHolidayDecemberModal = [String]()
    var arrayPresentDecemberModal = [String]()
    var arrayAbsentDecemberModal  = [String]()
    var arrayLeaveDecemberModal   = [String]()
    
    var arrayHolidayJanuaryModal  = [String]()
    var arrayPresentJanuaryModal  = [String]()
    var arrayAbsentJanuaryModal   = [String]()
    var arrayLeaveJanuaryModal    = [String]()
    
    var arrayHolidayFebruaryModal = [String]()
    var arrayPresentFebruaryModal = [String]()
    var arrayAbsentFebruaryModal  = [String]()
    var arrayLeaveFebruaryModal   = [String]()
    
    var arrayHolidayMarchModal    = [String]()
    var arrayPresentMarchModal    = [String]()
    var arrayAbsentMarchModal     = [String]()
    var arrayLeaveMarchModal      = [String]()

    
    //var isYearCal         = false
    
    var weekDayNamesArray: [String] = Array()
    var currentMonth: Int!
    var currentYear: Int!
    
    var isShowPreviousNextMonthDays:Bool = false
    
    var todayDateBackGroundColor:       UIColor?
    var normalDateColor:                UIColor?
    var selectedDateColor:              UIColor?
    var highlightedDateColor:           UIColor?
    var highlightedDateBackgroundColor: UIColor?
    var disableDateColor:               UIColor?
    var inset:                          CGFloat?
    
    
    func createCalendarYearly(withMonth month:Int, andYear year: Int, view: UIView, onVC : UIViewController?) {
        
        sharedInstanceYearly.currentMonth = month
        sharedInstanceYearly.currentYear  = year
        let date         = sharedInstanceYearly.returnDateFormatter().date(from:String(format: "1/%d/%d",sharedInstanceYearly.currentMonth, sharedInstanceYearly.currentYear))//"1/\\\(month)/\\\(year)"
        let numberOfDays = sharedInstanceYearly.getNumberOfDays(inMonth: month, withDate: date!)
        var  weekDay     = sharedInstanceYearly.getFirstWeekDay(forDate: date!)
        //Because Monday is the first Week Day!
        if (weekDay == 1)
        {weekDay = 7}
        else
        { weekDay = weekDay - 1}
        createMonthWith(numberofDays: numberOfDays, andStartingWeekDay: (weekDay), view: view , onVC: onVC)
    }
    
    func createMonthWith(numberofDays days:Int, andStartingWeekDay weekday:Int , view : UIView , onVC : UIViewController?)  {
        
        for vw in view.subviews {
            if vw.tag != 300 {
                vw.removeFromSuperview()
            }
        }
        var xPos = 00.0
        var yPos = 00.0
        var startIndex = weekday
        
        let width  = view.frame.size.width / 7//screenWidth / 7
        let height = width
        
        var weekCount = 1
        for weekDaynames in sharedInstanceYearly.weekDayNamesArray {
            let weekDayLable = UILabel.init(frame: CGRect(x: xPos, y: yPos, width: Double(width) + 5, height: 38.0))//Double(height)
            weekDayLable.text            = weekDaynames
            weekDayLable.textAlignment   = .center
            weekDayLable.contentMode     = .scaleAspectFit
            weekDayLable.backgroundColor = UIColor(rgb: 0xF3F3F3)
            weekDayLable.font      = R.font.ubuntuRegular(size: 15.0)
            weekDayLable.textColor = UIColor(rgb: 0xBFBFBF)
            
            view.addSubview(weekDayLable)
            xPos=xPos+Double(width)
            if(weekCount == 7){
                weekDayLable.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
                weekDayLable.font = R.font.ubuntuRegular(size: 15.0)
                
            }
            weekCount += 1
        }
        
        xPos = 00.0
        yPos = 40//Double(yPos) + Double(height);
        
        for i in 1...7 {
            
            if i == weekday {
                break
            }
            xPos = xPos + Double(width)
        }
        
        
        var lblWidth = 0.0
        var lblHeight = 0.0
        
//        if(!isYearCal)
//        {
//            lblWidth = Double(width)-8
//            lblHeight = Double(height) - 8
//        }
//        else
        //{
            lblWidth = Double(width)-4
            lblHeight = Double(height)-4
        //}
        
        for date in 1...days {
            
            let dateLable           = UILabel.init(frame: CGRect(x: xPos + 5, y: Double(yPos), width: lblWidth, height: lblHeight))
            dateLable.text          = String(describing: date)
            dateLable.textAlignment = .center
            dateLable.tag           = date
            //if(isYearCal){
                dateLable.font = dateLable.font.withSize(12.0)
           // }
            let date        = sharedInstanceYearly.returnDateFormatter().date(from:String(format: "%d/%d/%d",date, sharedInstanceYearly.currentMonth, sharedInstanceYearly.currentYear))
            let currentDate = sharedInstanceYearly.returnDateFormatter().date(from: sharedInstanceYearly.returnDateFormatter().string(from: Date()))
            
            if date?.compare(currentDate!) == .orderedSame {
                //current date
                //dateLable.backgroundColor    = UIColor.purple
                // dateLable.textColor          = UIColor.white
                dateLable.layoutIfNeeded()
                dateLable.clipsToBounds      = true
                dateLable.layer.cornerRadius = (dateLable.frame.size.width)/2
                
            }
            //Mark Sunday Blue
            sharedInstanceYearly.markSunday(date: date!, lbl: dateLable)
            ////Marking holidays
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayAprilModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayMayModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayJuneModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayJulyModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayAugustModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidaySeptemberModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayOctoberModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayNovemberModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayDecemberModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayJanuaryModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayFebruaryModal)
            sharedInstanceYearly.markHolidays(date: date!, lbl: dateLable, array: arrayHolidayMarchModal)
            //////Marking Present
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable, array: arrayPresentAprilModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentMayModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentJuneModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentJulyModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentAugustModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentSeptemberModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentOctoberModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentNovemberModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentDecemberModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentJanuaryModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentFebruaryModal)
            sharedInstanceYearly.markPresent(date: date!, lbl: dateLable,array: arrayPresentMarchModal)
            ////Marking Absent
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable, array: arrayAbsentAprilModal,leaveArray: arrayLeaveAprilModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentMayModal,leaveArray: arrayLeaveMayModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentJuneModal,leaveArray: arrayLeaveJuneModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentJulyModal,leaveArray: arrayLeaveJulyModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentAugustModal,leaveArray: arrayLeaveAugustModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentSeptemberModal,leaveArray: arrayLeaveSeptemberModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentOctoberModal,leaveArray: arrayLeaveOctoberModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentNovemberModal,leaveArray: arrayLeaveNovemberModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentDecemberModal,leaveArray: arrayLeaveDecemberModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentJanuaryModal,leaveArray: arrayLeaveJanuaryModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentFebruaryModal,leaveArray: arrayLeaveFebruaryModal)
            sharedInstanceYearly.markAbsent(date: date!, lbl: dateLable,array: arrayAbsentMarchModal,leaveArray: arrayLeaveMarchModal)

            view.addSubview(dateLable)
            startIndex = startIndex + 1
            xPos       = xPos+Double(width) //+ 15
            if startIndex == 8 {
                xPos       = 00.0
                yPos       = Double(yPos)+Double(height);
                startIndex = 1;
            }
        }
        (onVC as? AttendanceYearlyViewController)?.monthWiseCalendarArr.append(view)
    }
    func returnDateFormatter() -> DateFormatter {
        let formatter        = DateFormatter.init()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }
    func returnMonthFormatter() -> DateFormatter {
        let formatter        = DateFormatter.init()
        formatter.dateFormat = "MMMM/yyyy"
        return formatter
    }
    func returnWeekDayArray() -> Array<String> {
        let firstWeekday = 2 // -> Monday
        let formatter    = DateFormatter.init()
        var symbols      = formatter.veryShortWeekdaySymbols
        symbols          = Array(symbols![firstWeekday-1..<(symbols?.count)!]) + symbols![0..<firstWeekday-1]
        //    print(symbols!)
        return symbols!
    }
    
    func getNumberOfDays(inMonth month:Int, withDate date:Date) -> Int {
        let cal              = Calendar.current
        var dateComponents   = DateComponents.init()
        dateComponents.month = month
        let range            = cal.range(of: .day, in: .month, for: date)
        return (range?.count)!
    }
    
    func getFirstWeekDay(forDate date:Date) -> Int {
        let cal = Calendar.current
        let dateComponents = cal.dateComponents([.weekday], from: date)
        return (dateComponents.weekday!)
    }
    func markSunday(date: Date , lbl : UILabel)
    {
        let calendar      = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let components    = calendar!.components([.weekday], from: date)
        if components.weekday == 1 {
            lbl.textColor = UIColor(rgb: 0x44A8EB)//UIColor.blue
        }
    }
    /*****************************************************************************************/
    func markPresent(date: Date , lbl : UILabel, array: [String]){
        for i in 0..<array.count{
            let isPresent = returnDateFormatter().date(from: array[i])
            if date.compare(isPresent!) == .orderedSame {
                lbl.backgroundColor    = UIColor(rgb: 0x78CC41)//UIColor.green
                lbl.textColor          = UIColor.white
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
            }
        }
    }
    func markAbsent(date: Date , lbl : UILabel, array: [String], leaveArray: [String]){
        for i in 0..<array.count{
            let isAbsent = returnDateFormatter().date(from: array[i])
            // print("isAbsent")
            if date.compare(isAbsent!) == .orderedSame {
                lbl.backgroundColor    = UIColor(rgb: 0xFB4E4E)//UIColor.red
                lbl.textColor          = UIColor.white
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2            }
        }
       self.markMedicalLeave(date: date, lbl: lbl, array: leaveArray)
    }
    func markHolidays(date: Date , lbl : UILabel,array: [String]){
        for i in 0..<array.count{
            let isholiday = returnDateFormatter().date(from: array[i])
            if date.compare(isholiday!) == .orderedSame {
//                lbl.backgroundColor    = UIColor(rgb: 0x44A8EB)//UIColor.blue
//                lbl.textColor          = UIColor.white
//                lbl.layoutIfNeeded()
//                lbl.clipsToBounds      = true
//                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
                
                lbl.backgroundColor = UIColor(rgb: 0x44A8EB)//UIColor.red
                lbl.textColor       = UIColor.white
                lbl.numberOfLines   = 0
                
                let str = NSMutableAttributedString(string: "\(lbl.tag)")
//
//                let attributedString = NSAttributedString(string: "H", attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Regular", size: 9.0) ])
//                str.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Ubuntu-Regular", size:10.0) , range: NSRange(location: 0, length: 2))
                
//                str.append(attributedString)
                lbl.attributedText     = str
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2

            }
        }
    }
    func markMedicalLeave(date : Date , lbl: UILabel,array: [String]){
        for i in 0..<array.count{
            let isMedicalLeave = returnDateFormatter().date(from: array[i])
            if date.compare(isMedicalLeave!) == .orderedSame {
                lbl.backgroundColor = UIColor(rgb: 0x44A8EB)//UIColor.red
                lbl.textColor       = UIColor.white
                lbl.numberOfLines   = 0
                
                let str = NSMutableAttributedString(string: "\(lbl.tag)")
//
//                let attributedString = NSAttributedString(string: "ML", attributes: [NSAttributedString.Key.font: UIFont(name: "Ubuntu-Regular", size: 9.0) ])
//                str.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Ubuntu-Regular", size:10.0) , range: NSRange(location: 0, length: 2))
//
//                str.append(attributedString)
                lbl.attributedText     = str
                lbl.layoutIfNeeded()
                lbl.clipsToBounds      = true
                lbl.layer.cornerRadius = (lbl.frame.size.width)/2
            }
        }
    }
}
