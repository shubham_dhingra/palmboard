//
//  GridAttendanceModal.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 09/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class RPTModelAttendance{
    
    var date  :String
    var status:Int
    
    init(date:String,status:Int) {
        self.date   = date
        self.status = status
    }
}

class GridAttendanceModal: NSObject {
    
//    var UserID      :String
//    var SchoolDays  :Int
//    var TotalPresent:Int
//    var TotalAbsent :Int
//    var TotalLeave  :Int
//    var WorkingDays :Int//String
//    var PresentDays :Int
//    var AbsentDays  :Int
//    var LeaveDays   :Int
//    var Session     :String
//    var StartDate   :String
//    var EndDate     :String
//    var YrID        :String
    var rpt         = [RPTModelAttendance]()
    
    init(dic:[String:Any])
    {
//        self.UserID       = dic["UserID"]      as? String ?? ""
//        self.SchoolDays   = dic["SchoolDays"]  as? Int ?? 0
//        self.TotalPresent = dic["TotalPresent"]as? Int ?? 0
//        self.TotalAbsent  = dic["TotalAbsent"] as? Int ?? 0
//        self.TotalLeave   = dic["TotalLeave"]  as? Int ?? 0
//        self.WorkingDays  = dic["WorkingDays"] as? Int ?? 0//as? String ?? ""
//        self.PresentDays  = dic["PresentDays"] as? Int ?? 0
//        self.AbsentDays   = dic["AbsentDays"]  as? Int ?? 0
//        self.LeaveDays    = dic["LeaveDays"]   as? Int ?? 0
//        self.Session      = dic["Session"]     as? String ?? ""
//        self.StartDate    = dic["StartDate"]   as? String ?? ""
//        self.EndDate      = dic["EndDate"]     as? String ?? ""
//        self.YrID         = dic["YrID"]        as? String ?? ""
        
        if let rpt = dic["RPT"] as? [[String:Any]]{
            
            self.rpt.removeAll(keepingCapacity: false)
            for rec in rpt{
                
                let date   = rec["AttDate"] as? String ?? ""
                let status = rec["Status"]  as? Int ?? 0
                let model  = RPTModelAttendance(date: date, status: status)
                self.rpt.append(model)
            }
        }
        print("self.rpt = \(self.rpt.count)")
        print("self.rpt = \(self.rpt)")
        print("self.rpt = \(self.rpt[0].date)")
        
    }
}

