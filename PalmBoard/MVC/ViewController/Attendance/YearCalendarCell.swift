//  YearCalendarCell.swift
//  CalendarPage
//  Created by NewMacmini on 12/8/17.
//  Copyright © 2017 NewMacmini. All rights reserved.
import UIKit

class YearCalendarCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBg:       UIView?
    @IBOutlet weak var viewCalendar: UIView?
    @IBOutlet weak var lblMonth:     UILabel?
    
    override func awakeFromNib()
    {
        viewBg?.layer.cornerRadius             = 15.0
        self.contentView.layer.cornerRadius    = 15
        viewBg?.layer.masksToBounds            = true
        self.contentView.layer.masksToBounds   = true
//        sharedInstanceYearly.weekDayNamesArray = sharedInstanceYearly.returnWeekDayArray()
    }
}
