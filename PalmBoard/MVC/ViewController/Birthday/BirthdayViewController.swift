
import UIKit

class BirthdayViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, iCarouselDelegate, iCarouselDataSource
{

    @IBOutlet weak var tblViewBirthDay:   UITableView?
    @IBOutlet weak var imgViewNoBirthday: UIImageView?
    @IBOutlet weak var lblNoBirthday:     UILabel?

    var arrayBirthdayToday          = [[String: Any]]()
    
    var strSchoolCodeSender         = String()
    var storedOffsets               = [Int: CGFloat]()
    var btnTitle                    =  UIButton()

    var arrayFilterMonth            = [[String: Any]]()
    var arrayTodayDateData          = [[String: Any]]()
    var boolCheck                   = Bool()
   
    var color11                     = UIColor()
    let color12                     = UIColor(rgb: 0xFFFFFF)
    let gradientLocations           = [0.1,0.6]
    let gradientLayer               = CAGradientLayer()


    var allKeysMonth                = [String]()
    var allActionsDic               = [String:Any]()
    var intUserTypeMain             = Int()
    var themeColor                  = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.title = "Birthday"
        boolCheck  = true
        themeColor = self.getCurrentSchool()?.themColor ?? ""
        color11 = themeColor.hexStringToUIColor()

        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Birthdays", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)

        self.navigationItem.titleView = btnTitle

        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserTypeMain =  Int(userType)
        }
       
      //  carousel.type = .coverFlow2

        allKeysMonth = strSchoolCodeSender.keyMonths()
        //print("allKeysMonth = \(allKeysMonth)")
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.tblViewBirthDay?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = true

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)

        self.imgViewNoBirthday?.isHidden = true
        self.lblNoBirthday?.isHidden     = true

        self.tblViewBirthDay?.delegate          = nil
        self.tblViewBirthDay?.dataSource        = nil
        self.tblViewBirthDay?.isHidden          = true
        
        tblViewBirthDay?.rowHeight              = UITableView.automaticDimension
        tblViewBirthDay?.estimatedRowHeight     = 2000
        
        tblViewBirthDay?.sectionHeaderHeight          = UITableView.automaticDimension
        tblViewBirthDay?.estimatedSectionHeaderHeight = 2000;
        
        tblViewBirthDay?.tableFooterView              = UIView(frame: CGRect.zero)
        self.webApiBirthday()
    }
    func webApiBirthday()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "birthday/lst?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())"//\(intUserID)
        
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                  DispatchQueue.main.async {
                    Utility.shared.removeLoader()
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                   // print("Result == \(result)")
                  
                    
                        
                        if let arrayAll = (result["MonthWise"] as? [[String: Any]])
                        {
                            self.arrayBirthdayToday = arrayAll
                        }
                        if self.arrayBirthdayToday.count == 0
                        {
                            self.imgViewNoBirthday?.isHidden = false
                            self.lblNoBirthday?.isHidden     = false
                        }
                        else
                        {
                            let now                  = NSDate()
                            let dateFormatter        = DateFormatter()
                            dateFormatter.dateFormat = "MMMM"
                            let currentMonth         = dateFormatter.string(from: now as Date)
                            dateFormatter.dateFormat = "dd MMM"
                            dateFormatter.timeZone   = NSTimeZone(abbreviation: "GMT+5:30")! as TimeZone//GMT+5:30
                            let currentDateString    = dateFormatter.string(from: now as Date)
                            
                            for (index,_) in self.arrayBirthdayToday.enumerated()
                            {
                                if let strMonth = self.arrayBirthdayToday[index]["Month"] as? String
                                {
                                    if let array =  (self.arrayBirthdayToday[index]["ThisMonth"] as? [[String:Any]]){
                                        self.allActionsDic[strMonth] = array
                                    }
                                    else{
                                        if let index = self.allKeysMonth.index(of:strMonth) {
                                            self.allKeysMonth.remove(at: index)
                                        }
                                        //self.allActionsDic[strMonth] = [[:]]
                                    }
                                }
                            }
                            print("**********")

                            print("self.allActionsDic = \(self.allActionsDic)")
                            print("**********")

                            for (index,_) in self.arrayBirthdayToday.enumerated()
                            {
                                if let strMonth = self.arrayBirthdayToday[index]["Month"] as? String
                                {
                                    if strMonth == currentMonth
                                    {
                                        if let arrayFilter = self.arrayBirthdayToday[index]["ThisMonth"] as? [[String:Any]] {
                                            self.arrayFilterMonth = arrayFilter
                                        }
                                        break
                                    }
                                }
                            }
                           // print("arrayFilterMonth == \(self.arrayFilterMonth)")
                          //  print("arrayFilterMonth count == \(self.arrayFilterMonth.count)")

                            for (index,_) in self.arrayFilterMonth.enumerated()
                            {
                                if let str = self.arrayFilterMonth[index]["DOB"] as? String
                                {
                                    var strDate             = String()
                                    strDate                 = str
                                    
                                     let strDateAPI: String      = strDate.dateChangeBirthdayUserToday()
                                    
                                    if currentDateString == strDateAPI{
                                        self.arrayTodayDateData.append(self.arrayFilterMonth[index])
                                    }
                                }
                            }
                            
                            print("arrayTodayDateData == \(self.arrayTodayDateData)")
                            print("arrayTodayDateData count == \(self.arrayTodayDateData.count)")

                            print("arrayFilterMonth == \(self.arrayFilterMonth)")
                            print("arrayFilterMonth count == \(self.arrayFilterMonth.count)")

                            self.tblViewBirthDay?.delegate   = self
                            self.tblViewBirthDay?.dataSource = self
                            self.tblViewBirthDay?.isHidden   = false
                            self.tblViewBirthDay?.reloadData()
                            
                            //self.tblViewBirthDay.beginUpdates()
                           // self.tblViewBirthDay.endUpdates()
                        }
                    
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
            if (error != nil){
                
                DispatchQueue.main.async
                    {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                }
            }
            }
        }
    }
    
    // MARK: - Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int{
        return allKeysMonth.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let strMonth = allKeysMonth[section]
        if (allActionsDic[strMonth] as? [[String:Any]]) != nil{
            return 1
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellBirthday", for: indexPath) as! customBirthdayTableViewCell
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? customBirthdayTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? customBirthdayTableViewCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
   /* func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.red
       // let headerView = view as! UITableViewHeaderFooterView
//        header.textLabel?.textColor = UIColor.white
        
        if section == 0
        {
            // let headerView = tableView.dequeueReusableCell(withIdentifier: "cardView") as! CardViewBirthdayTableViewCell
             let headerView = view as! CardViewBirthdayTableViewCell

            let gradientBackgroundColors = [color11.cgColor, color12.cgColor]
            //let gradientLocations        = [0.1,0.6]
            // let gradientLayer            = CAGradientLayer()
            gradientLayer.colors         = gradientBackgroundColors
            gradientLayer.locations      = gradientLocations as [NSNumber]
            gradientLayer.frame          = self.tblViewBirthDay.bounds
            let backgroundView           = UIView(frame: self.tblViewBirthDay.bounds)
            backgroundView.layer.insertSublayer(gradientLayer, at: 0)
            headerView.backgroundView    = backgroundView
        }
        else if section == 1
        {
            let headerView = view as! HeaderBirthdayTableViewCell

           // let headerView = tableView.dequeueReusableCell(withIdentifier: "HeaderBirthday") as! HeaderBirthdayTableViewCell

            headerView.backgroundColor = UIColor.white
        }
    }*/
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        print("Func: viewForHeaderInSection section = \(section))")
        if  section == 0
        {
            print("*******section0******")
            let headerView = tableView.dequeueReusableCell(withIdentifier: "cardView") as! CardViewBirthdayTableViewCell

            if arrayTodayDateData.count > 0{
                headerView.heightCarousel?.constant = 191
            }
            else {
                headerView.heightCarousel?.constant = 0
                
                let headerView1 = tableView.dequeueReusableCell(withIdentifier: "HeaderBirthday") as! HeaderBirthdayTableViewCell
                if let strMonth = allKeysMonth[section] as? String{
                    headerView1.lblDate?.text       = strMonth
                }
                
                return headerView1
            }
            if let strMonth = allKeysMonth[section] as? String{
                headerView.lblDate?.text       = strMonth
            }
            headerView.carousel?.type      = .coverFlow2
            return headerView
        }
        else
        {
            let headerView11 = tableView.dequeueReusableCell(withIdentifier: "HeaderBirthday") as! HeaderBirthdayTableViewCell
            if let strMonth = self.allKeysMonth[section] as? String{
                headerView11.lblDate?.text       = strMonth
            }
            let backgroundView11             = UIView(frame: (self.tblViewBirthDay?.bounds)!)
            backgroundView11.backgroundColor = UIColor.white
            headerView11.backgroundView      = backgroundView11
            return headerView11
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 15
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return arrayTodayDateData.count
    }
  
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view : CarouselReuseView = CarouselReuseView()
        view.frame.size          = CGSize(width: 204 , height: 170)
        view.backgroundColor     = UIColor.clear
        view.layer.cornerRadius  = 15.0
        view.layer.masksToBounds = true

        view.btnProfile?.tag       = index
        view.lblName?.tag          = index
        view.lblClass?.tag         = index
        view.btnHappyBirthday?.tag = index
        view.btnHappyBirthday?.backgroundColor  = themeColor.hexStringToUIColor()

        view.lblClear?.backgroundColor = UIColor.clear
        
        let shapePath1        = UIBezierPath.init(roundedRect: (view.viewInner?.bounds)!, byRoundingCorners: [UIRectCorner.topLeft,UIRectCorner.topRight], cornerRadii: CGSize(width: 15,height: 15))
        let newCornerLayer1   = CAShapeLayer()
        newCornerLayer1.frame = (view.viewInner?.bounds)!
        newCornerLayer1.path  = shapePath1.cgPath
        view.viewInner?.layer.mask = newCornerLayer1
        
        let shapePath        = UIBezierPath.init(roundedRect: (view.btnHappyBirthday?.bounds)!, byRoundingCorners: [UIRectCorner.bottomLeft,UIRectCorner.bottomRight], cornerRadii: CGSize(width: 15,height: 15))
        let newCornerLayer   = CAShapeLayer()
        newCornerLayer.frame = (view.btnHappyBirthday?.bounds)!
        newCornerLayer.path  = shapePath.cgPath
        view.btnHappyBirthday?.layer.mask = newCornerLayer
        
        var strPhoto     = String()
        if let strPhoto1 = arrayTodayDateData[index]["Photo"] as? String{
            strPhoto     = strPhoto1
        }
        var strPrefix   = String()
        strPrefix       += strPrefix.imgPath1()
        let finalUrl    = "\(strPrefix)" +  "\(strPhoto)"
        let urlPhoto    = URL(string: finalUrl)
        
        let imgView1 = UIImageView()
        imgView1.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            
            let img1 = imgView1.image?.af_imageRoundedIntoCircle()
            view.btnProfile?.setBackgroundImage(img1 as UIImage!, for: .normal)
        }
        
        if let strPhoto1    = arrayTodayDateData[index]["Name"] as? String{
            let stringArray = strPhoto1.components(separatedBy: "|")
            
            if let intType = arrayTodayDateData[index]["UserType"] as? Int
            {
                if intType == 1 ||  intType == 3
                {
                    if  stringArray[0].count > 0{
                        view.lblName?.text = stringArray[0]
                    }
                    if stringArray[1].count > 0{
                        view.lblClass?.text = "(\(stringArray[1]))"
                    }
                }
                else{
                    view.lblName?.text = strPhoto1
                    view.lblClass?.text = "Parent"
                }
            }
        }
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        print("index = \(index)")
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        // print("carousel= \(carousel)")
        // print("value= \(value)")
        // print("carousel= \(carousel)")
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
}
extension BirthdayViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        var arrayNew = [[String:Any]]()

            let strMonth = allKeysMonth[collectionView.tag]
            
            if let array = allActionsDic[strMonth] as? [[String:Any]]{
                arrayNew = array
                return arrayNew.count
            }
            else{
                return 0
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellBirthdayCal", for: indexPath) as! customBirthdayCollectionViewCell
        
        var intUserType = Int()
        var strDate     = String()
        var strPhoto    = String()
        var strName     = String()
        var arrayNew    = [[String:Any]]()
        
        let strMonth = allKeysMonth[collectionView.tag]
        
        if let array = allActionsDic[strMonth] as? [[String:Any]]
        {
            arrayNew = array
            
            strName     = arrayNew[indexPath.row]["Name"]     as! String
            strDate     = arrayNew[indexPath.row]["DOB"]      as! String
            strPhoto    = arrayNew[indexPath.row]["Photo"]    as! String
            intUserType = arrayNew[indexPath.row]["UserType"] as! Int
            if intUserType == 1 || intUserType == 3
            {
                let stringArray = strName.components(separatedBy: "|")
                cell.lblName?.text = "\(stringArray[0])(\(stringArray[1]))"
            }
            if intUserType == 2
            {
                strName += "(Parent)"//"(Teacher)"
                cell.lblName?.text = strName
            }
            cell.lblDate?.textColor = themeColor.hexStringToUIColor()
            cell.lblDate?.text = strDate.dateChangeBirthdayUser()
            var strPrefix   = String()
            strPrefix       += strPrefix.imgPath1()
            let finalUrl    = "\(strPrefix)" +  "\(strPhoto)"
            let urlPhoto    = URL(string: finalUrl)
            
            cell.imgView?.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = cell.imgView?.image?.af_imageRoundedIntoCircle()
                cell.imgView?.image = img1
            }
            //print("*** Row= \(indexPath.row) and section = \(indexPath.row)")
           // print("*** collectionView.tag = \(collectionView.tag)")
            return cell
        }
        else{
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
    
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
}
