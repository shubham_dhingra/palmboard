//  CardViewBirthdayTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class CardViewBirthdayTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDate:  UILabel?
    @IBOutlet weak var carousel: iCarousel?
    @IBOutlet weak var heightCarousel: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var color11                     = UIColor(rgb: 0x56B54B)
        let color12                     = UIColor(rgb: 0xFFFFFF)

        var themeColor = String()
        themeColor = DBManager.shared.getAllSchools().first?.themColor ?? ""
        color11        = themeColor.hexStringToUIColor()

        let gradientBackgroundColors = [color11.cgColor, color12.cgColor]
        let gradientLocations        = [0.1,0.6]
         let gradientLayer            = CAGradientLayer()
        gradientLayer.colors         = gradientBackgroundColors
        gradientLayer.locations      = gradientLocations as [NSNumber]
        
        let cgrect = CGRect(x:0, y:0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        gradientLayer.frame          = cgrect//self.bounds
        let backgroundView           = UIView(frame: self.bounds)
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        self.backgroundView    = backgroundView

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
