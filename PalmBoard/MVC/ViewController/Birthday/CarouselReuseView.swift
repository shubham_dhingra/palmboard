//  CarouselReuseView.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class CarouselReuseView: UIView
{
    @IBOutlet weak var viewInner: UIView?
    @IBOutlet weak var lblClear: UILabel?
    @IBOutlet var contentView: UIView?
    @IBOutlet weak var btnProfile:       UIButton?
    @IBOutlet weak var lblName:          UILabel?
    @IBOutlet weak var lblClass:         UILabel?
    @IBOutlet weak var btnHappyBirthday: UIButton?
    
    /*
     var viewBg:     UIView
     var btnProfile: UIButton
     var lblName:    UILabel
     var lblClass:   UILabel
     var btnHappyBirthday: UIButton
 */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()

    }
    private func commonInit()
    {
        Bundle.main.loadNibNamed("CarouselReuseView", owner: self, options: nil)
        addSubview(contentView!)
        contentView?.frame = self.bounds
        contentView?.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        self.backgroundColor    = UIColor.white
        self.layer.cornerRadius = 15.0

        // btnProfile.backgroundColor          = UIColor.blue
        btnProfile?.layer.cornerRadius       = (btnProfile?.frame.size.width)! / 2
        // btnProfile.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        // btnProfile.contentMode              = .scaleAspectFit
        
        
//        let shapePath = UIBezierPath.init(roundedRect: btnHappyBirthday.bounds, byRoundingCorners: [UIRectCorner.bottomLeft,UIRectCorner.bottomRight], cornerRadii: CGSize(width: 15,height: 15))
//        let newCornerLayer          = CAShapeLayer()
//        newCornerLayer.frame        = btnHappyBirthday.bounds
//        newCornerLayer.path         = shapePath.cgPath
//        btnHappyBirthday.layer.mask = newCornerLayer
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
