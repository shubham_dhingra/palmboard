//  HeaderBirthdayTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class HeaderBirthdayTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
