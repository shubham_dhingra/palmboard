//  customBirthdayCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 22/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class customBirthdayCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblDate: UILabel?
}
