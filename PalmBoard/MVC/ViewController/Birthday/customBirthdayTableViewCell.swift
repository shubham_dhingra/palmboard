//
//  customBirthdayTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 22/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class customBirthdayTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewBirthday: UICollectionView?
    
    /*
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 */

}

extension customBirthdayTableViewCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionViewBirthday?.delegate = dataSourceDelegate
        collectionViewBirthday?.dataSource = dataSourceDelegate
        collectionViewBirthday?.tag = row
        collectionViewBirthday?.setContentOffset((collectionViewBirthday?.contentOffset)!, animated:false) // Stops collection view if it was scrolling.
        collectionViewBirthday?.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionViewBirthday?.contentOffset.x = newValue }
        get { return (collectionViewBirthday?.contentOffset.x)! }
    }
}
