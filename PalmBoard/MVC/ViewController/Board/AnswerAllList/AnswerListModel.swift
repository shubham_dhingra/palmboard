//  AnswerListModel.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 29/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class QuestionModel{
    
    var QID         :Int
    var QType       :Int
    var Que         :String
    var QueImg      :String
    var UpdatedBy   :String
    var UpdatedOn   :String
    var Photo       :String
    var Likes       :String
    var isILike     :Int
    var TotalAnswer :String
    var isVerified  :Int
    var isReported  :Int
    var isAnswered  :Int
    var UserID      :Int
    var UserType    :Int

    var countTotalAnswer: Int
    
    init(QID:Int,QType:Int, Que: String, QueImg: String,UpdatedBy: String, UpdatedOn: String, Photo: String, Likes: String, isILike: Int, TotalAnswer: String, isVerified: Int, isReported: Int, isAnswered: Int, UserID: Int, UserType: Int,countTotalAnswer: Int) {
        
        self.QID         = QID
        self.QType       = QType
        self.Que         = Que
        self.QueImg      = QueImg
        self.UpdatedBy   = UpdatedBy
        self.UpdatedOn   = UpdatedOn
        self.Photo       = Photo
        self.Likes       = Likes
        self.isILike     = isILike
        self.TotalAnswer = TotalAnswer
        self.isVerified  = isVerified
        self.isReported  = isReported
        self.isAnswered  = isAnswered
        self.UserID      = UserID
        self.UserType    = UserType
        
        self.countTotalAnswer = countTotalAnswer
    }
}
class ListModel{
    
    var AnID        :Int
    var Answer      :String
    var AnsweredBy  :String
    var Photo       :String
    var AnsweredOn  :String
    var UserID      :Int
    var UserType    :Int
    var isVerified  :Int
    var isReported  :Int
    
    init(AnID:Int,Answer:String, AnsweredBy: String, Photo: String,AnsweredOn: String, UserID: Int, UserType: Int, isVerified: Int, isReported: Int) {
        
        self.AnID           = AnID
        self.Answer         = Answer
        self.AnsweredBy     = AnsweredBy
        self.Photo          = Photo
        self.AnsweredOn     = AnsweredOn
        self.UserID         = UserID
        self.UserType       = UserType
        self.isVerified     = isVerified
        self.isReported     = isReported
    }
}

class AnswerListModel: NSObject {
    var ErrorCode   :Int
    var Status      :String
    var Message     :String
    var List         = [ListModel]()
    var Question     = [QuestionModel]()

    init(dic:[String:Any])
    {
        self.ErrorCode  = dic["ErrorCode"] as? Int    ?? 0
        self.Status     = dic["Status"]    as? String ?? ""
        self.Message    = dic["Message"]   as? String ?? ""
        
        if let dtl = dic["List"] as? [[String:Any]]{
            
            self.List.removeAll(keepingCapacity: false)
            for rec in dtl{
                
                let AnID        = rec["AnID"]         as? Int    ?? 0
                let Answer      = rec["Answer"]       as? String ?? ""
                let AnsweredBy  = rec["AnsweredBy"]   as? String ?? ""
                var PhotoURL    = rec["Photo"]        as? String ?? ""
                PhotoURL        = PhotoURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

                var strPrefix   = String()
                strPrefix      += strPrefix.imgPath1()
                let Photo       = "\(strPrefix)" +  "\(PhotoURL)"

                
                let strFromDate = rec["AnsweredOn"]   as? String ?? ""
                let UserID      = rec["UserID"]       as? Int ?? 0
                let UserType    = rec["UserType"]     as? Int ?? 0
                let isVerified  = rec["isVerified"]   as? Int ?? 0
                let isReported  = rec["isReported"]   as? Int ?? 0
                
                let AnsweredOn    = strFromDate.dateChangeFormatQuestion()//dateChangeFormat()
                
                let model = ListModel(AnID: AnID, Answer: Answer, AnsweredBy: AnsweredBy, Photo: Photo, AnsweredOn: AnsweredOn, UserID: UserID, UserType: UserType, isVerified: isVerified, isReported: isReported)
                
                self.List.append(model)
            }
        }
        if let rec = dic["Question"] as? [String:Any]{
            
                self.Question.removeAll(keepingCapacity: false)
            
                let QID         = rec["QID"]         as? Int    ?? 0
                let QType       = rec["QType"]       as? Int    ?? 0
                let Que         = rec["Que"]         as? String ?? ""
                var PhotoURLQue = rec["QueImg"]      as? String ?? ""
                PhotoURLQue     = PhotoURLQue.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

                var strPrefix1   = String()
                strPrefix1       += strPrefix1.imgPath1()

                let QueImg       = "\(strPrefix1)" +  "\(PhotoURLQue)"

            
                let UpdatedBy   = rec["UpdatedBy"]   as? String ?? ""
                let strFromDate = rec["UpdatedOn"]   as? String ?? ""
                var PhotoURL    = rec["Photo"]       as? String ?? ""
                PhotoURL        = PhotoURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

                let isILike     = rec["isILike"]     as? Int ?? 0

                var strPrefix   = String()
                strPrefix      += strPrefix.imgPath1()
                let Photo       = "\(strPrefix)" +  "\(PhotoURL)"

                //let Likes       = rec["Likes"]       as? Int ?? 0
                //let TotalAnswer = rec["TotalAnswer"] as? Int ?? 0
            
                var Likes  =  String()
            
                let intLikes = rec["Likes"]       as? Int ?? 0
                if intLikes > 1{
                    Likes = "\(intLikes) Likes"
                }
                else  if intLikes == 1{
                    Likes = "\(intLikes) Like"
                }
                else{
                    Likes = ""
                }
            
                var TotalAnswer       =  String()
                var countTotalAnswer  =  Int()

                let AnswerAll = rec["TotalAnswer"] as? Int ?? 0
                if AnswerAll > 1{
                     TotalAnswer = "\(AnswerAll) Answers"
                    countTotalAnswer = 1
                }
                else  if AnswerAll == 1{
                    TotalAnswer = "\(AnswerAll) Answer"
                    countTotalAnswer = 1
                }
                else  if AnswerAll == 0{
                    TotalAnswer = ""
                    countTotalAnswer = 2
                }
            
                let isVerified  = rec["isVerified"]  as? Int ?? 0
                let isReported  = rec["isReported"]  as? Int ?? 0
                let isAnswered  = rec["isAnswered"]  as? Int ?? 0
                let UserID      = rec["UserID"]      as? Int ?? 0
                let UserType    = rec["UserType"]    as? Int ?? 0
                let UpdatedOn   = strFromDate.dateChangeFormatQuestion()//dateChangeFormat()
            
            let model = QuestionModel(QID: QID, QType: QType, Que: Que, QueImg: QueImg, UpdatedBy: UpdatedBy, UpdatedOn: UpdatedOn, Photo: Photo, Likes: Likes, isILike: isILike, TotalAnswer: TotalAnswer, isVerified: isVerified, isReported: isReported, isAnswered: isAnswered, UserID: UserID, UserType: UserType,countTotalAnswer: countTotalAnswer)
            
                self.Question.append(model)
        }
       // print("self.List.count = \(self.List.count)")
       // print("self.List = \(self.List)")
       // print("self.List = \(self.List[0].Answer)")
        //print("self.QuestionModel.count = \(self.Question.count)")
        //print("self.QuestionModel = \(self.Question)")
        //print("self.Question= \(self.Question[0].Que)")
        //self.listData.rpt[index].Title
    }
}
