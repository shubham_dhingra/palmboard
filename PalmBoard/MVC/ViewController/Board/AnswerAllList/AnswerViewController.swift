//  AnswerViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
import UIKit
let REPLY_SIZE: Float = 75
import AlamofireImage
import Alamofire
import IQKeyboardManager

class AnswerViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate, DeleteDelegate
{
    @IBOutlet weak var btnReply:       UIButton?
    @IBOutlet weak var txtViewReply:   UITextView?
    @IBOutlet weak var tblAnswer:      UITableView?
    @IBOutlet weak var bottomTxtView:  NSLayoutConstraint?
    @IBOutlet weak var heightTxtView:  NSLayoutConstraint?
    @IBOutlet weak var heightTblView:  NSLayoutConstraint?
    @IBOutlet weak var bottomBtnReply: NSLayoutConstraint?
    @IBOutlet weak var heightBtnReply: NSLayoutConstraint?
    var listData :  AnswerListModel!
    
    var strlblAnswer      = String()
    var strlblAnswerTime  = String()
    var strLikeAll        = String()
    var intAnswerID       = Int()
    var intIsAnswered     = Int()
    var intQID            = Int()
    var intMyAnswer       = Int()
    var btnReference      = UIButton()
    var keyboardHeight    = CGFloat()
    var deleteRowIndex    = IndexPath()
    let selectedColor     = UIColor(rgb: 0x56B54B)
    let unselectedColor   = UIColor(rgb: 0x5E5E5E)
    var strSchoolCode     = String()
    var intUserType       = Int()
    var intUserID         = Int()
    let imgViewCopy       = UIImageView()
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigation(vc: self, title: "Question")
        navigationItem.hidesBackButton        = false

        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        intMyAnswer             = 0
        tblAnswer?.delegate     = nil
        tblAnswer?.dataSource   = nil
        tblAnswer?.contentInset = UIEdgeInsets(top: -36, left: 0, bottom: 0, right: 0)
        let nib1 = UINib(nibName: "MyAnswer", bundle: nil)
        tblAnswer?.register(nib1, forCellReuseIdentifier: CellIdentifiers.CellMyAnswer.get)
        let nib2 = UINib(nibName: "AnswerList", bundle: nil)
        tblAnswer?.register(nib2, forCellReuseIdentifier: CellIdentifiers.CellAnswer.get)
        let nib3 = UINib(nibName: "AnswerHeader", bundle: nil)
        tblAnswer?.register(nib3, forCellReuseIdentifier: CellIdentifiers.CellHeader.get)

        self.btnReply?.setImage(R.image.sendGrey(), for: UIControl.State.normal)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {self.strSchoolCode  =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID    {self.intUserID      =  Int(userId)}
        if let userType   = self.getCurrentUser()?.userType  {self.intUserType    =  Int(userType)}
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissEditing(_:)))
        view.addGestureRecognizer(tap)
        
        tblAnswer?.isHidden    = true
        txtViewReply?.isHidden = true
        btnReply?.isHidden     = true
        
        txtViewReply?.layer.cornerRadius = 5
        txtViewReply?.clipsToBounds      = true
        // txtViewReply.contentInset       = UIEdgeInsetsMake(-36, 0, 0, 0)
        
        self.QAnslst(intQuesID: intQID)
    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let themeColor = self.getCurrentSchool()?.themColor else { return}
        txtViewReply?.tintColor = themeColor.hexStringToUIColor()
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    // MARK: - QAnslst method
    func QAnslst(intQuesID: Int)
    {
        // print("QAnslst")
        Utility.shared.loader()
        ////self.view.isUserInteractionEnabled = false
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "question/QAnslst?SchCode=\(strSchoolCode)&key=\(urlString.keyPath())&qid=\(intQuesID)&UserID=\(intUserID)&UserType=\(intUserType)"
          print("Func: urlStringAnswer= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if let dictQue = (result["Question"] as? [String: Any]){
                                
                                self.tblAnswer?.isHidden    = false
                                self.txtViewReply?.isHidden = false
                                self.btnReply?.isHidden     = false
                                
                               // print("dictQue= \(dictQue)")
                                if dictQue.keys.count > 0{
                                    DispatchQueue.main.async{
                                        self.loadListData(dataDict: result)
                                    }
                                }
                                else{
                                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noDataFound(), fromVC: self)
                                }
                            }
                        }
                        else{
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                        }
                    }
                    else{
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
            }
        }
    }
    // MARK: - loadListData
    func loadListData(dataDict : [String:Any])
    {
        listData = AnswerListModel(dic: dataDict)
        // print("self.listData.List.count = \(self.listData.List.count)")
        DispatchQueue.main.async
            {
                var intUserID1   = Int()
                var intUserType1 = Int()
                for (index,_) in self.listData.List.enumerated()
                {
                    intUserID1     = self.listData.List[index].UserID
                    intUserType1   = self.listData.List[index].UserType
                    if (self.intUserID == intUserID1 && self.intUserType == intUserType1)
                    {
                        self.intMyAnswer      = 1
                        self.strlblAnswer     = self.listData.List[index].Answer
                        self.strlblAnswerTime = self.listData.List[index].AnsweredOn
                        self.intAnswerID      = self.listData.List[index].AnID
                        self.listData.List.remove(at: index)
                        break
                    }
                }
                self.intIsAnswered                          = self.listData.Question[0].isAnswered
                self.tblAnswer?.rowHeight                    = UITableView.automaticDimension
                self.tblAnswer?.estimatedRowHeight           = 2000
                self.tblAnswer?.sectionHeaderHeight          = UITableView.automaticDimension
                self.tblAnswer?.estimatedSectionHeaderHeight = 2000;
                self.tblAnswer?.tableFooterView              = UIView(frame: CGRect.zero)
                self.tblAnswer?.delegate                     = self
                self.tblAnswer?.dataSource                   = self
                self.tblAnswer?.reloadData()
        }
        // print("After deletion self.listData.List.count = \(self.listData.List.count)")
    }
    // MARK: - Tableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? intMyAnswer : self.listData.List.count
        // return self.listData.List.count + intMyAnswer
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if intMyAnswer == 1
        {
            if indexPath.section == 0//if indexPath.row == 0
            {
                let cellAnswer = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CellMyAnswer.get, for: indexPath as IndexPath) as! MyAnswerTableViewCell
                cellAnswer.btnDelete?.addTarget(self, action: #selector(ClkBtnDelete(button:)), for: .touchUpInside)
                cellAnswer.btnDelete?.tag      = 0
                //cellAnswer.lblAnswer.text     = strlblAnswer.replace(target: "|", withString: ", ")
                var str1 = String()
                var str2 = String()
                let stringArray        = strlblAnswer.components(separatedBy: "|")
                str1                   = "\(stringArray[0])"
                if(stringArray.count > 1){
                    str1 += ", "
                    str2 = "\(stringArray[1])"
                }else{
                    if self.intUserType == 2 {
                        //  str1 += ", "
                        //     str2     = "Parent"
                    }
                    else {
                        str2     = ""
                    }
                }
                str1 += str2
                cellAnswer.lblAnswer?.text     = str1
                
                cellAnswer.lblAnswerTime?.text = strlblAnswerTime
                return cellAnswer
            }
            else
            {
                if indexPath.section == 1 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CellAnswer.get, for: indexPath as IndexPath) as! CustomAnswerTableViewCell
                    
                    var str1 = String()
                    var str2 = String()
                    
                    // cell.lblName.text   = self.listData.List[indexPath.row - 1].AnsweredBy.replace(target: "|", withString: ", ")
                    let stringArray        = self.listData.List[indexPath.row].AnsweredBy.components(separatedBy: "|")
                    str1                   = "\(stringArray[0])"
                    
                    if(stringArray.count > 1){
                        str1 += ", "
                        str2 = "\(stringArray[1])"
                    }else{
                        if self.intUserType == 2 {
                            // str1 += ", "
                            //  str2     = "Parent"
                        }
                        else {
                            str2     = ""
                        }
                    }
                    str1 += str2
                    cell.lblName?.text   = str1
                    
                    cell.lblAnswer?.text = self.listData.List[indexPath.row ].Answer
                    cell.lblTime?.text   = self.listData.List[indexPath.row ].AnsweredOn
                    cell.btnDot?.tag     = indexPath.row //- 1
                    //cell.btnDot.addTarget(self, action: #selector(ClkBtnReport(button:)), for: .touchUpInside)
                    cell.btnDot?.addTarget(self, action: #selector(ClkBtnDeleteOtherAnswer(button:)), for: .touchUpInside)
                    
                    if((intUserID == self.listData.Question[0].UserID) && (intUserType == self.listData.Question[0].UserType)){
                        cell.btnDot?.isHidden = false
                    }
                    else{
                        cell.btnDot?.isHidden = true
                    }
                    let urlPhoto        = URL(string: self.listData.List[indexPath.row].Photo)
                    
                    cell.btnImgCell?.tag = indexPath.row
                    cell.btnImgCell?.addTarget(self, action: #selector(ClkBtnViewProfile(button:)), for: .touchUpInside)
                    
                    imgViewCopy.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                        
                        let imgCopy = self.imgViewCopy.image?.af_imageRoundedIntoCircle()
                        cell.btnImgCell?.setBackgroundImage(imgCopy as UIImage?, for: .normal)
                    }
                    return cell
                }
                else{
                    let cellBlank = tableView.dequeueReusableCell(withIdentifier: "cellBlank", for: indexPath as IndexPath)
                    return cellBlank
                }
            }
        }
        else
        {
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswer", for: indexPath as IndexPath) as! CustomAnswerTableViewCell
                
                cell.lblName?.text   = self.listData.List[indexPath.row].AnsweredBy
                cell.lblAnswer?.text = self.listData.List[indexPath.row].Answer
                cell.lblTime?.text   = self.listData.List[indexPath.row].AnsweredOn
                cell.btnDot?.tag     = indexPath.row
                // cell.btnDot.addTarget(self, action: #selector(ClkBtnReport(button:)), for: .touchUpInside)
                cell.btnDot?.addTarget(self, action: #selector(ClkBtnDeleteOtherAnswer(button:)), for: .touchUpInside)
                
                if((intUserID == self.listData.Question[0].UserID) && (intUserType == self.listData.Question[0].UserType)){
                    cell.btnDot?.isHidden = false
                }
                else{
                    cell.btnDot?.isHidden = true
                }
                let urlPhoto    = URL(string: self.listData.List[indexPath.row].Photo)
                
                cell.btnImgCell?.tag = indexPath.row
                cell.btnImgCell?.addTarget(self, action: #selector(ClkBtnViewProfile(button:)), for: .touchUpInside)
                
                imgViewCopy.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    
                    let imgCopy = self.imgViewCopy.image?.af_imageRoundedIntoCircle()
                    cell.btnImgCell?.setBackgroundImage(imgCopy as UIImage?, for: .normal)
                }
                
                return cell
            }
            else {
                let cell1 = tableView.dequeueReusableCell(withIdentifier: "cellBlank", for: indexPath as IndexPath)
                // let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswer", for: indexPath as IndexPath) as! CustomAnswerTableViewCell
                return cell1
                
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //  print("section = \(section)")
        if section == 0 {
            let imgView1    = UIImageView()
            let headerView  = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.CellHeader.get) as! HeaderAnswerView
            // headerView.lblName.text     = self.listData.Question[0].UpdatedBy.replace(target: "|", withString: ", ")
            var str1 = String()
            var str2 = String()
            
            let stringArray = self.listData.Question[0].UpdatedBy.components(separatedBy: "|")
            str1            = "\(stringArray[0])"
            
            if(stringArray.count > 1){
                str1 += ", "
                str2 = "\(stringArray[1])"
            }else{
                if self.intUserType == 2 {
                    str1 += ", "
                    str2     = "Parent"
                }
                else {
                    str2     = ""
                }
            }
            str1 += str2
            headerView.intQuesID = intQID
            headerView.strLikeAll = strLikeAll
            headerView.lblName?.text     = str1
            headerView.lblTime?.text     = self.listData.Question[0].UpdatedOn
            headerView.lblQuestion?.text = self.listData.Question[0].Que
            // intIsAnswered             = self.listData.Question[0].isAnswered
            
            let urlPhoto                = URL(string: self.listData.Question[0].Photo)
            
            imgView1.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = imgView1.image?.af_imageRoundedIntoCircle()
                headerView.btnProfile?.setBackgroundImage(img1 as UIImage?, for: .normal)
                
                if imgView1.image != nil{
                    headerView.btnProfile?.setBackgroundImage(img1 as UIImage?, for: .normal)
                }
            }
            headerView.btnAnswer?.addTarget(self, action: #selector(ClkBtnAnswer(button:)), for: .touchUpInside)
            headerView.btnAllAnswer?.addTarget(self, action: #selector(ClkBtnAnswer(button:)), for: .touchUpInside)
            
            headerView.btnLike?.addTarget(self, action: #selector(ClkBtnLike(button:)), for: .touchUpInside)
            headerView.btnAllLikes?.addTarget(self, action: #selector(ClkBtnLikeAll(button:)), for: .touchUpInside)
            
            headerView.btnProfile?.addTarget(self, action: #selector(ClkProfile(button:)), for: .touchUpInside)
            headerView.btnAllLikes?.setTitle("\(self.listData.Question[0].Likes)",   for: .normal)
            headerView.btnAllAnswer?.setTitle("\(self.listData.Question[0].TotalAnswer)",for: .normal)
            
            btnReference          = headerView.btnAllLikes!
            //        if self.listData.Question[0].countTotalAnswer == 2{
            //            self.txtViewReply?.becomeFirstResponder()
            //        }
            switch self.listData.Question[0].isILike
            {
            case 0:
                headerView.btnLike?.setImage(R.image.like(), for: UIControl.State.normal)
                headerView.btnLike?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                headerView.btnLike?.setImage(R.image.liked(), for: UIControl.State.normal)
                headerView.btnLike?.setTitleColor(selectedColor, for: .normal)
            default:
                break
            }
            
            switch self.listData.Question[0].QType {
            case 1:
                // var imgFrame: CGRect = headerView.imgQuestion.frame
                //imgFrame.size.height = 0
                //headerView.imgQuestion.frame = imgFrame
                headerView.imgviewAnswerHeight.constant = 0
            case 2:
                
                let imgViewTemp = CustomImageView()
                let imgViewInitialFrame = headerView.imgQuestion?.frame
                imgViewTemp.frame = imgViewInitialFrame!
                
                imgViewTemp.imageFromServerURL(urlString: self.listData.Question[0].QueImg, tableView: nil, indexpath: nil)
                
                if imgViewTemp.image != nil{
                    headerView.imgQuestion?.image = imgViewTemp.image
                    headerView.imgviewAnswerHeight.constant = (imgViewTemp.image!.size.height)
                }else{
                    headerView.imgviewAnswerHeight.constant = 0
                }
            default:
                break
            }
            switch intIsAnswered
            {
            case 0:
                // print("Answer = \(intIsAnswered)")
                heightTxtView?.constant  = 40
                heightBtnReply?.constant = 40
                headerView.btnAnswer?.setImage(R.image.answer(), for: UIControl.State.normal)
            case 1:
                // NSLog("AnswerSelect = \(intIsAnswered)!")
                heightTxtView?.constant  = 0
                heightBtnReply?.constant = 0
                headerView.btnAnswer?.setImage(R.image.answerSelect(), for: UIControl.State.normal)
            default:
                break
            }
            return headerView
        }
        else {
            let headerViewBlank = tableView.dequeueReusableCell(withIdentifier: "cellBlank")
            return headerViewBlank
        }
    }
    // MARK: - textView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if((self.heightTxtView?.constant)! >= CGFloat(REPLY_SIZE)){
            self.heightTxtView?.constant = CGFloat(REPLY_SIZE)
        }
        else{
            heightTxtView?.constant = (txtViewReply?.contentSize.height)!
        }
        if (range.location == 0 && text == " "){
            return false
        }
        if text == "\n"{
            textView.resignFirstResponder()
            self.btnReply?.setImage(R.image.sendGrey(), for: UIControl.State.normal)
            return false
        }
        if text == "Please Type Answer"{
            textView.textColor = UIColor.lightGray
            textView.font      =  R.font.ubuntuRegular(size: 16.0)!
        }
        // self.btnReply?.setImage(UIImage(named: "Send Yellow"), for: UIControl.State.normal)
        ( textView.text.count + (text.count - range.length)) == 0 ? self.btnReply?.setImage(R.image.sendGrey(), for: UIControl.State.normal) : self.btnReply?.setImage(R.image.sendYellow(), for: UIControl.State.normal)
        // self.GapBodyReply.constant = (217 + 70)
        return textView.text.count + (text.count - range.length) <= 400
        // return  textView.text.length + (text.length - range.length) <= 400
    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Please Type Answer" {
            textView.text         = ""
            textView.textColor    = UIColor.black
            textView.font         =  R.font.ubuntuRegular(size: 16.0)!
        }
        self.bottomTxtView?.constant  = 0//100//216//Updated 3 Jan
        self.bottomBtnReply?.constant = 0//100//216//Updated 3 Jan
        self.txtViewReply?.becomeFirstResponder()
    }
    func textViewDidEndEditing(_ textView: UITextView)
    {
        self.txtViewReply?.resignFirstResponder()
        if textView.text == "" {
            textView.text      = "Please Type Answer"
            textView.textColor = UIColor.lightGray
            textView.font      =  R.font.ubuntuRegular(size: 16.0)!
        }
        self.bottomTxtView?.constant  = 5
        self.bottomBtnReply?.constant = 5
        
        if((self.txtViewReply?.contentSize.height)! >= CGFloat(REPLY_SIZE)){
            self.heightTxtView?.constant = CGFloat(REPLY_SIZE)
        }
        else{
            self.heightTxtView?.constant = (self.txtViewReply?.contentSize.height)!
        }
    }
    
    // MARK: - keyboardWillShow
    @objc func keyboardWillShow(_ notification: NSNotification){
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            self.bottomTxtView?.constant  = keyboardHeight  //226.0
            self.bottomBtnReply?.constant = keyboardHeight  //226.0
        }
    }
    // MARK: - keyboardWillHide
    @objc func keyboardWillHide(_ notification: NSNotification){
        self.bottomTxtView?.constant    = 5
        self.bottomBtnReply?.constant   = 5
        txtViewReply?.resignFirstResponder()
    }
    // MARK: - ClkBtnAnswer
    @objc func ClkBtnAnswer(button: UIButton){
        // NSLog("ClkBtnAnswer!")
        //txtViewReply.becomeFirstResponder()
    }
    //MARK::- handleQuestionLike
    func handleQuestionLike(response : Any?,sender: UIButton?){
        if let model = response as? ThoughtsLikeModal
        {
            if(model.status == "ok" && model.message == "Liked")
            {
                // print("result = \(result)")
                sender?.setImage(R.image.liked(), for: UIControl.State.normal)
                sender?.setTitleColor(self.selectedColor, for: .normal)
                self.strLikeAll = "\(/model.totalLikes) Likes"
                if /model.totalLikes > 1{
                    self.strLikeAll = "\(/model.totalLikes) Likes"
                }
                else  if /model.totalLikes == 1{
                    self.strLikeAll = "\(/model.totalLikes) Like"
                }
                else{
                    self.strLikeAll = ""
                }
                self.btnReference.setTitle("\(self.strLikeAll)",for: .normal)
            }
            else if(model.status == "ok" && model.message == "Unliked")
            {
                // print("result = \(result)")
                sender?.setImage(R.image.like(), for: UIControl.State.normal)
                sender?.setTitleColor(self.unselectedColor, for: .normal)
                self.strLikeAll = "\(/model.totalLikes) Likes"
                // print("Unliked  = \(self.strLikeAll)")
                if /model.totalLikes > 1{
                    self.strLikeAll = "\(/model.totalLikes) Likes"
                }
                else  if /model.totalLikes == 1{
                    self.strLikeAll = "\(/model.totalLikes) Like"
                }
                else{
                    self.strLikeAll = ""
                }
                self.btnReference.setTitle("\(self.strLikeAll)",for: .normal)
            }
            else{
                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
            }
        }
    }
    // MARK: - ClkBtnLike
    @objc func ClkBtnLike(button: UIButton){
        // NSLog("ClkBtnLike!")
        var intLikeDislike = Int()
        //self.view.isUserInteractionEnabled = false
        if button.hasImage(named: "like", for: .normal) {
            intLikeDislike = 1
        } else {
            intLikeDislike = 0
        }
        //isILike
        APIManager.shared.request(with: HomeEndpoint.LikeQuestion(UserID: self.userID, UserType: self.userType, QID: intQID.toString, isLike: intLikeDislike.toString), isLoader: false) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handleQuestionLike(response: success,sender: button)
            })
        }
    }
    // MARK: - ClkBtnLikeAll
    @objc func ClkBtnLikeAll(button: UIButton){
        //  self.performSegue(withIdentifier: "segueLikesAllUser", sender: self)
        //        print("self.strLikeAll = \(self.strLikeAll)")
        //        print("self.strLikeAll.count = \(self.strLikeAll.count)")
        //        print("Likes = \(self.listData.Question[0].Likes)")
        if "\(self.listData.Question[0].Likes)".count > 0 {
            guard let likesAllVc = R.storyboard.main.likesAllUserViewController() else {return}
            likesAllVc.strTotalLikes     = strLikeAll
            likesAllVc.intQuestionID     = intQID
            likesAllVc.intWordThoughtQuestion = 3
            self.pushVC(likesAllVc)
        }
    }
    //MARK: -ClkBtnReport
    @objc func ClkBtnReport(button: UIButton)
    {
        //        print("ClkBtnReport!=\(self.listData.List[button.tag].AnID) ")
        let indexPath     = IndexPath(row: button.tag, section: 0)
        deleteRowIndex    = indexPath
        guard let reportVc = R.storyboard.main.reportViewController() else {return}
        reportVc.intAnID            = self.listData.List[button.tag].AnID
        reportVc.intQAThoughtReport =  3
        reportVc.thoughtDelegate    = self
        reportVc.deleteReport       =  0//1
        self.presentVC(reportVc)
    }
    //MARK: -ClkBtnDelete //ClkBtnDeleteOtherAnswer
    @objc func ClkBtnDelete(button: UIButton)
    {
        deleteRowIndex    = IndexPath(row: button.tag, section: 0)
        // print("List count = \(self.listData.List.count)")
        // print("List = \(self.listData.List)")
        guard let reportVc          = R.storyboard.main.reportViewController() else {return}
        reportVc.intAnID            = self.intAnswerID
        reportVc.intQAThoughtReport =  3
        reportVc.thoughtDelegate    = self
        reportVc.deleteReport       =  0//1
        self.presentVC(reportVc)
    }
    //MARK: -ClkBtnDeleteOtherAnswer
    @objc func ClkBtnDeleteOtherAnswer(button: UIButton)
    {
        // print("ClkBtnDeleteOtherAnswer!=\(button.tag) ")
        deleteRowIndex    = IndexPath(row: button.tag, section: 1)
        // print("List count = \(self.listData.List.count)")
        // print("List = \(self.listData.List)")
        guard let reportVc = R.storyboard.main.reportViewController() else {return}
        reportVc.intAnID            = self.listData.List[button.tag].AnID
        reportVc.intQAThoughtReport =  3
        reportVc.thoughtDelegate    = self
        reportVc.deleteReport       =  0//1
        self.presentVC(reportVc)
    }
    //MARK::- handleDelete
    func handleDelete(response : Any?,sectionValue: Int,rowValue: Int){
        if let model = response as? ThoughtsLikeModal
        {
            if(model.status == "ok" && model.message == "Answer deleted successfully")
            {
                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.deleteSuccessfully(), fromVC: self)
                if sectionValue == 0{
                    self.intIsAnswered = 0
                    self.intMyAnswer   = 0
                }
                else if sectionValue == 1{
                }
                let indexPath1              = IndexPath(row: rowValue, section: sectionValue)
                self.txtViewReply?.textColor = UIColor.lightGray
                
                self.tblAnswer?.deleteRows(at: [indexPath1], with: .automatic)
                self.tblAnswer?.reloadData()
            }
            else if(model.status == "error" ){
                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
            }
        }
    }
    // MARK: - webAPIDeleteMyAnswer
    func webAPIDeleteMyAnswer(rowValue: Int, sectionValue: Int)
    {
        // print("rowValue = \(rowValue) and sectionValue= \(sectionValue)")
        ////self.view.isUserInteractionEnabled = false
        APIManager.shared.request(with: HomeEndpoint.QuestionDeleteAns(UserID: self.userID, UserType: self.userType, AnID: intAnswerID.toString)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handleDelete(response: success,sectionValue:sectionValue,rowValue:rowValue)
            })
        }
    }
    //MARK: -ClkProfile
    @objc func ClkProfile(button: UIButton)
    {
        guard let shortProfileVc     = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID     = listData.Question[0].UserID
        shortProfileVc.intUserType   = listData.Question[0].UserType
        self.pushVC(shortProfileVc)
    }
    //MARK::- handlePost
    func handlePost(response : Any?){
        if let model = response as? ThoughtsLikeModal
        {
            if(model.status == "ok" && model.message == "Answered successfully")
            {
                self.intMyAnswer   = 0
                self.intIsAnswered = 1
                self.heightTxtView?.constant  = 0
                self.heightBtnReply?.constant = 0
                self.listData.Question[0].countTotalAnswer = 1
                self.txtViewReply?.resignFirstResponder()
                self.txtViewReply?.text = "Please Type Answer"
                self.tblAnswer?.reloadData()
                self.QAnslst(intQuesID: self.intQID)
            }
            else if(model.status == "error" )
            {
                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
            }
        }
        else
        {
            //  print("AnswerError = \(error!)")
            DispatchQueue.main.async
                {
                    self.btnReply?.setImage(R.image.sendGrey(), for: UIControl.State.normal)
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
            }
        }
    }
    // MARK: - ClkBtnReply
    @IBAction func ClkBtnReply(_ sender: Any)
    {
        if txtViewReply?.text.count != 0 && txtViewReply?.text != "Please Type Answer"
        {
            self.btnReply?.setImage(R.image.sendYellow(), for: UIControl.State.normal)
            //print("Pwd: enterPwdAction")
            txtViewReply?.resignFirstResponder()
            APIManager.shared.request(with: HomeEndpoint.QuestionPostAnswer(UserID: self.userID, UserType: self.userType, QID: intQID.toString, Answer: txtViewReply?.text)) {[weak self] (response) in
                self?.handleResponse(response: response, responseBack: { (success) in
                    self?.handlePost(response: success)
                })
            }
        }
    }
    // MARK: - deleteRow
    func deleteRow() {
        // print("Func deleteRow Row \(deleteRowIndex.row) and section \(deleteRowIndex.section)deleted")
        if deleteRowIndex.section == 0{
            self.intIsAnswered = 0
            self.intMyAnswer   = 0
            self.listData.Question[0].TotalAnswer = "\(self.listData.List.count)"
        }
        else if deleteRowIndex.section == 1{
            self.listData.List.remove(at: deleteRowIndex.row)
        }
        self.tblAnswer?.deleteRows(at: [deleteRowIndex], with: .automatic)
        self.tblAnswer?.setContentOffset(.zero, animated: false)
        // print("self.listData.List.count = \(self.listData.List.count)")
        self.tblAnswer?.reloadData()
        let indexPath = IndexPath(row: deleteRowIndex.row, section : deleteRowIndex.section)
        
        if self.listData.List.count == 0 && self.intMyAnswer == 0{
            self.listData.Question[0].TotalAnswer = ""
            self.listData.Question[0].countTotalAnswer = 2
            
        }
        else  if self.intMyAnswer == 0 && self.listData.List.count != 0 {
            self.listData.Question[0].countTotalAnswer = 1
            
            if self.listData.List.count > 0{
                self.listData.Question[0].TotalAnswer = "\(self.listData.List.count) Answers"
            }
            else{
                self.listData.Question[0].TotalAnswer = "\(self.listData.List.count) Answers"
            }
        }
        else  if self.intMyAnswer == 1 && self.listData.List.count == 0 {
            self.listData.Question[0].TotalAnswer = "1 Answer"
            self.listData.Question[0].countTotalAnswer = 1
            
        }
        else if self.intMyAnswer == 1 && self.listData.List.count != 0 {
            var count  = Int()
            let count1 = self.listData.Question[0].TotalAnswer as? String
            // print("count1 = \(String(describing: count1))")
            count      = Int(count1!)! + 1
            self.listData.Question[0].TotalAnswer = "\(count) Answers"
        }
        self.txtViewReply?.textColor = UIColor.lightGray
    }
    // MARK: - ClkBtnLikeWord Method
    @objc func ClkBtnViewProfile(button: UIButton)
    {
        guard let shortProfileVc     = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID     = self.listData.List[button.tag].UserID
        shortProfileVc.intUserType   = self.listData.List[button.tag].UserType
        self.pushVC(shortProfileVc)
    }
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    // MARK: - dismissKeyboard
    @objc func dismissEditing(_ sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}
