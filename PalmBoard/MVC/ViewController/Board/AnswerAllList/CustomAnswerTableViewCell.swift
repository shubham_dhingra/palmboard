//  CustomAnswerTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
class CustomAnswerTableViewCell: UITableViewCell {

    @IBOutlet weak var btnImgCell: UIButton?
    @IBOutlet weak var imgViewCell: UIImageView?
    @IBOutlet weak var lblName:     UILabel?
    @IBOutlet weak var lblAnswer:   UILabel?
    @IBOutlet weak var lblTime:     UILabel?
    @IBOutlet weak var btnDot:      UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = UITableViewCell.SelectionStyle.none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
