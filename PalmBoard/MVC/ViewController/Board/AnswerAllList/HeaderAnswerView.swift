//  HeaderAnswerView.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 06/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class HeaderAnswerView: UITableViewCell
{
    @IBOutlet weak var btnProfile:      UIButton?
    @IBOutlet weak var lblName:         UILabel?
    @IBOutlet weak var lblTime:         UILabel?
    @IBOutlet weak var lblQuestion:     UILabel?
    @IBOutlet weak var imgQuestion:     UIImageView?
    @IBOutlet weak var btnAnswer:       UIButton?
    @IBOutlet weak var btnAllAnswer:    UIButton?
    @IBOutlet weak var btnLike:         UIButton?
    @IBOutlet weak var btnAllLikes:     UIButton?
    
    var intQuesID = Int()
    var strLikeAll = String()
    
    
    @IBOutlet weak var imgviewAnswerHeight: NSLayoutConstraint!
    //@IBOutlet weak var heightImg: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func btnLikeAllAct(_ sender : UIButton){
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        guard let likesAllVc = storyboard.instantiateViewController(withIdentifier: "LikesAllUserViewController") as? LikesAllUserViewController else {return}
//        likesAllVc.strTotalLikes     = strLikeAll
//        likesAllVc.intQuestionID     = intQuesID
//        likesAllVc.intWordThoughtQuestion = 3
//        ez.topMostVC?.presentVC(likesAllVc)
    }
}
