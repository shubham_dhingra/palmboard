//  MyAnswerTableViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 09/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class MyAnswerTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAnswer:       UILabel?
    @IBOutlet weak var lblAnswerTime:   UILabel?
    @IBOutlet weak var lblYourAnswered: UILabel?
    @IBOutlet weak var btnDelete: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
