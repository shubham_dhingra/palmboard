import UIKit
import Alamofire
import CoreData
import EZSwiftExtensions

class BoardViewController: UIViewController,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var tblBoard:               UITableView?
    @IBOutlet weak var staffModuleView :       UIView?
    @IBOutlet weak var staffModuleViewHeight : NSLayoutConstraint?
    @IBOutlet weak var lblNoUpdates :  UILabel?
    @IBOutlet weak var imgNoUpdates : UIImageView?
    @IBOutlet weak var imgNoUpdatesCenter: NSLayoutConstraint!
    
    var dictResult           = [String  : Any]()
    var dictSection_1        = [String  : Any]()
    var tempArray            = [[String : Any]]()
    var arrayStaffRPT        = [[String : Any]]()
    var arraySubReports      = [[String : Any]]()
    var strSubReportsTitle   = String()
    var isFirstTime : Bool   = true
    var strSchoolCodeSender  = String()
    var intUserType          = Int()
    var intUserID            = Int()
    var intCatID             = Int()
    var strUserName          = String()
    var strPassword          = String()
    var refreshControl = UIRefreshControl()
    var arrayRecentUpdate    :  [Update]?
    var intCount:Int         = 1
    var intAllMyCount        = Int()
    var strRoleName          = String()
    var storedOffsets        = [Int: CGFloat]()
    var themeColor           = String()
    var btnSearch            =  UIButton()
    var imgViewSchool        = UIImageView()
    var titleView            = UIView()
    var btnClicked : Bool    = false
    var awsURL : String?
    var pageNo : Int = 1
    var totalUpdates : Int = 0
    
    var tableDataSource : BoardTableDataSource?{
        didSet{
            tblBoard?.dataSource   = tableDataSource
            tblBoard?.delegate     = tableDataSource
            tblBoard?.reloadData()
        }
    }
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: NOTI.VERIFICATION_SUCCESS), object: nil, queue: nil) { (_) in
            self.getUserDetails(hitApi : true)
            self.pageNo = 1
            self.webAPIUpdateRecent(pg: self.pageNo)
        }
        refreshControl.tintColor = UIColor.flatGreen
        addRefereshControl()
        webAPIUpdateRecent(pg: pageNo)
        getUserDetails()
    }
    
    
    //MARK: - getUserDetails
    func getUserDetails(hitApi : Bool? = false) {
        
        if let user = self.getCurrentUser() {
            if let schoolCode = self.getCurrentSchool()?.schCode{
                strSchoolCodeSender =  schoolCode
            }
            intUserID =  Int(user.userID)
            intUserType =  Int(user.userType)
            intCatID =  Int(user.categoryId)
            
            if let UserName = user.username{
                strUserName =  UserName
            }
            if let Password = user.password{
                strPassword =  Password
            }
            if let RoleName = user.role{
                strRoleName =  RoleName
            }
        }
        if /hitApi {
            self.webAPIBoard()
        }
    }
    
    
    //MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        checkVersion()
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : true , "Index" : 0])
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        intAllMyCount = 1
        ez.runThisInMainThread {
            if let url    = self.getCurrentSchool()?.schLogo?.getImageUrl() {
                self.imgViewSchool.af_setImage(withURL: url, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    if let image = self.imgViewSchool.image {
                        let button    = UIButton.init(type: .custom)
                        button.frame  = CGRect.init(x: 0, y: 2, width: 44, height: 35)
                        button.setImage(image, for: UIControl.State.normal)
                        button.imageView?.contentMode   = UIView.ContentMode.scaleAspectFit
                        button.isUserInteractionEnabled = false
                        self.titleView.addSubview(button)
                        //self.view.bringSubview(toFront: self.titleView)
                        self.navigationItem.titleView = self.titleView
                    }
                }
            }
        }
        
        if let strSchoolName = self.getCurrentSchool()?.schName
        {
            titleView    = UIView(frame: CGRect(x : 0,y:  0,width : self.view.frame.size.width, height: 40))
            let label1      = UILabel(frame: CGRect(x : 50,y:  2,width : 290, height: 18))
            label1.text     = strSchoolName
            label1.font     = R.font.ubuntuRegular(size: 13.0)
            label1.textColor = UIColor(rgb: 0x545454)//UIColor.black
            let label2       = UILabel(frame: CGRect(x : 50,y: label1.frame.height + 2 ,width : 100, height: 14))
            if let strCity = self.getCurrentSchool()?.schCity{
                label2.text      = strCity
            }
            label2.textColor = UIColor(rgb: 0x878787)
            label2.font      = R.font.ubuntuLight(size: 10.0)
            
            titleView.addSubview(label1)
            titleView.addSubview(label2)
            navigationItem.titleView = titleView
            
            print("strRoleName = \(/self.getCurrentUser()?.role)")
            print("self.getCurrentUser()? = \(/self.getCurrentUser()?.searchEnabled)")
            
            if (self.getCurrentUser()?.searchEnabled)!  {
                btnSearch                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
                btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
                btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
                btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
                
                let barButton = UIBarButtonItem.init(customView: btnSearch)
                self.navigationItem.rightBarButtonItem = barButton
            }
        }
        self.webAPIBoard()
        
        intCount  = 1
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        themeColor = self.getCurrentSchool()?.themColor ?? ""
    }
    
    @objc func ClkBtnSearch(sender:UIButton){
        self.performSegue(withIdentifier: "SegueMainSearch", sender: nil)
    }
    
    
    //MARK: webAPIBoard Method
    func webAPIBoard()
    {
        print("Func: webAPIBoard")
        Utility.shared.loader()
        self.view.isUserInteractionEnabled = false
        
        var strKey    = String()
        strKey       += strKey.keyPath()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "user/Validate"
        
        let parameters: Parameters = [
            "SchoolCode": strSchoolCodeSender,
            "key": strKey,
            "Username": strUserName,
            "Password": strPassword
        ]
        print("parameters = \(parameters)")
        print("urlString = \(urlString)")
        
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: {[weak self] (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        //print("result = \(result)")
                        if(result["Status"] as? String == "ok" && result["isAuthenticate"] as? Bool == false)
                        {
                            //TODO:-
                            self?.verifyUser()
                        }
                        
                        if(result["Status"] as? String == "ok" && result["Message"] as? String == "success")
                        {
                            self?.view.isUserInteractionEnabled = true
                            
                            var dictResultAPI      = [String : Any]()
                            dictResultAPI          = result
                            
                            if let arrayStaffRPT1   = (dictResultAPI["StaffRPT"] as? [[String : Any]])
                            {
                                self?.arrayStaffRPT.removeAll()
                                self?.arrayStaffRPT  = arrayStaffRPT1
                                self?.tableDataSource?.arrayStaffRPT = self?.arrayStaffRPT ?? [[:]]
                            }
                            
                            if let arrayMenu = (dictResultAPI["Menu"] as? [[String : Any]]) {
                                
                                //                                if let parent = self?.parent as? CustomTabbarViewController {
                                //                                    parent.arrayMenu = arrayMenu
                                //                                    parent.tabCollectionView?.reloadData()
                                //                                }
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTI.UPDATE_MODULE_LIST), object: nil, userInfo: ["menu" : arrayMenu])
                            }
                            //                            self?.webAPIUpdateRecent(pg: /self?.pageNo)
                        }
                        else if(result["Status"] as? String == "error" )  {
                            self?.view.isUserInteractionEnabled = true
                        }
                        ez.runThisInMainThread {
                            self?.reloadTable()
                        }
                        
                    }
                    else{
                        Utility.shared.removeLoader()
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                    }
                    if (error != nil){
                        
                        print("PwdVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                self?.view.isUserInteractionEnabled = true
                                self?.webAPIUpdateRecent(pg: /self?.pageNo)
                                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                        }
                    }
            }
        })
    }
    
}


extension BoardViewController
{
    //MARK: webAPIUpdateRecent Method
    func webAPIUpdateRecent(pg : Int) {
        
        guard let user = self.getCurrentUser() else {return}
        
        APIManager.shared.request(with: HomeEndpoint.RecentUpdate(userID: Int(user.userID).toString ,userType: Int(user.userType).toString, roleName: user.role, pg: pg.toString), isLoader : isFirstTime) { (response) in
            self.refreshControl.endRefreshing()
            self.handleRecentUpdateResponse(response: response)
        }
    }
    
    func handleRecentUpdateResponse(response: Response) {
        
        switch response {
        case .success(let resVal):
            self.handle(response : resVal)
        case .failure(_):
            break
        }
    }
    
    //MARK::- handle
    func handle(response : Any?){
        
        if let modal = response as? RecentUpdate {
            
            if modal.errorCode == 0 {
                
                totalUpdates = /modal.total
                tblBoard?.isScrollEnabled = totalUpdates != 0
                lblNoUpdates?.isHidden = totalUpdates != 0
                imgNoUpdates?.isHidden = totalUpdates != 0
                self.awsURL = modal.aws_path
                imgNoUpdates?.translatesAutoresizingMaskIntoConstraints = false
                if let anchor = tblBoard?.centerYAnchor {
                    imgNoUpdates?.centerYAnchor.constraint(equalTo: anchor, constant: self.intUserType == 3 ? 1.4 : 1.0).isActive = self.intUserType != 3
                }

                if let  recentUpdatesArr = modal.updates {
                    if pageNo == 1 {
                        self.arrayRecentUpdate = []
                    }
                    if totalUpdates == self.arrayRecentUpdate?.count {
                        print("All recent update already loaded")
                        return
                    }
                    if pageNo == 1 {
                        self.arrayRecentUpdate = recentUpdatesArr
                    }
                    else {
                        if recentUpdatesArr.count != 0 {
                            _ =  recentUpdatesArr.map{(self.arrayRecentUpdate?.append($0))}
                        }
                    }
                    reloadTable()
                }
                else {
                    if pageNo == 1 {
                        Messages.shared.show(alert: .oops, message: "No Recent Updates" , type: .warning)
                        self.arrayRecentUpdate = []
                        reloadTable()
                    }
                }
            }
        }
    }
    
}
extension BoardViewController {
    
    func configureTableView() {
        tableDataSource  = BoardTableDataSource(items: arrayRecentUpdate, height: UITableView.automaticDimension, tableView: tblBoard, cellIdentifier: CellIdentifiers.recentUpdateCell.get , intUserType : self.intUserType , arrayStaffRPT : self.arrayStaffRPT , arraySubReports : self.arraySubReports, fromVc : self , awsURL : self.awsURL)
        tableDataSource?.viewforHeaderInSection = {(section) in
            
            if self.intUserType == 3 {
                if section == 1 {
                    let recentView =   RecentUpdatesHeader.instanceFromNib() ?? UIView()
                    return recentView
                }
                else {
                    return nil
                }
            }
            else {
                let recentView =   RecentUpdatesHeader.instanceFromNib() ?? UIView()
                return recentView
            }
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            if self.intUserType == 3 &&  indexpath.section == 0 {
               return
            }
            self.didSelect(indexpath.row)
        }
        
        tableDataSource?.scrollViewEndDeaccerlate = {(scrollView) in
            self.reloadMoreData(scrollView)
        }
    }
    func didSelect(_ index : Int){
        if index >= /self.arrayRecentUpdate?.count {
            return
        }
        else {
            guard let recentModule = self.arrayRecentUpdate?[index] else {return}
            navigateToController(recentModule: recentModule)
        }
    }
    
    func navigateToController(recentModule : Update){
        
        guard let modID = recentModule.mdlID else {return}
        
        guard let school = self.getCurrentSchool() , let user = self.getCurrentUser() else {return}
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.hideMenu()
        switch modID {
        //notice
        case 1:
            
            guard let vc = storyboard.instantiateViewController(withIdentifier: "NoticeDetailsViewController") as? NoticeDetailsViewController else {return}
            vc.intNtID = /recentModule.iD
            vc.strSenderDetails = /school.schCode
            vc.intUserIDDetails = /Int(user.userID)
            vc.intUserTypeDetails = /Int(user.userType)
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 3 :
            guard let vc = storyboard.instantiateViewController(withIdentifier: "CircularDetailsViewController") as? CircularDetailsViewController else {return}
            vc.intCirID = /recentModule.iD
            vc.strSenderDetails = /school.schCode
            vc.intUserIDDetails = /Int(user.userID)
            vc.intUserTypeDetails = /Int(user.userType)
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 2 :
            guard let vc = storyboard.instantiateViewController(withIdentifier: "DetailsPhotoAlbumsViewController") as? DetailsPhotoAlbumsViewController else {return}
            
            vc.intAlId = /recentModule.iD
            vc.intAPIType = /recentModule.galleryUpdate?.sMdlID
            vc.strTitle   = recentModule.galleryUpdate?.sMdlID == 1 ? "Photos" : "Videos"
            vc.fromBoardScreen = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 14 :
            
            NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
            guard let vc = storyboard.instantiateViewController(withIdentifier: "LeaveDetailsViewController") as? LeaveDetailsViewController else {return}
            vc.fromBoardScreen = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case 16:
            
            let composeStoryboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
            guard let vc = composeStoryboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else {return}
            
            guard let msgDTLs = recentModule.msgDTLs , let senderDTLs = msgDTLs.senderDTL else {
                return
            }
            
            vc.sMessage = SMessages.init(abbreviation_: nil, iD_: recentModule.iD?.toString, msgID_: recentModule.iD, msgType_: msgDTLs.msgType, recipients_: nil, sentOn_: recentModule.updtedOn, subject_: recentModule.caption, isReplyMsg_: 0, hasRead_: 0, photo_: msgDTLs.filePath)
            vc.name = senderDTLs.name
            vc.designation = senderDTLs.designation
            vc.imgUser = nil
            vc.senderPicUrl = senderDTLs.photo
            vc.senderID = senderDTLs.senderID
            vc.senderType = senderDTLs.senderType
            vc.msgType = .Inbox
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break
            
        }
    }
    
    func reloadTable() {
        print("AWS URL FETCHED : \(self.awsURL)")
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.awsURL = self.awsURL
            tableDataSource?.items = self.arrayRecentUpdate
            tblBoard?.reloadData()
        }
    }
    
}
extension BoardViewController {
    
    func checkVersion() {
        APIManager.shared.request(with: HomeEndpoint.appVersion()) { (response) in
            switch response {
            case .success(let response):
                self.handleVersionResponse(response)
            case .failure(_):
                break
            }
        }
    }
    
    func handleVersionResponse(_ response : Any?) {
        if let modal = response as? VersionData {
            guard let currentVersion = ez.appVersion , let normalVersion = modal.normalVersion  else {return}
            let normalVersionNumber = normalVersion.replace(target: ".", withString: "")
            let currentVersionNumber = currentVersion.replace(target: ".", withString: "")
            print("normal version : \(/normalVersionNumber.toInt())")
            print("current version : \(/currentVersionNumber.toInt())")
            if /normalVersionNumber.toInt() > /currentVersionNumber.toInt() {
                showAlert(title : /modal.title ,description : /modal.descriptionField , isForceUpdate : false)
            }
        }
//        updateSchoolData()
    }
    
    
    
    func showAlert(title : String , description : String , isForceUpdate : Bool){
        
        if isForceUpdate {
            AlertsClass.shared.showAlertController(withTitle: title, message: description, buttonTitles: [AlertConstants.Update.rawValue]) { (value) in
                let type = value as AlertTag
                switch type {
                case .yes:
                    self.startUpdate()
                default : break
                }
            }
        }
        else {
            AlertsClass.shared.showAlertController(withTitle: title, message: description, buttonTitles: [AlertConstants.Cancel.rawValue, AlertConstants.Update.rawValue]) { (value) in
                let type = value as AlertTag
                switch type {
                case .yes:
                    self.cancelUpdate()
                default :
                    self.startUpdate()
                }
            }
        }
    }
    
    func updateSchoolData() {
        guard let schCode = self.getCurrentSchool()?.schCode else {
            return
        }
        print(schCode)
        var strSchoolCodeverifyURL = String()
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.webAPIDomainNmae()
        strSchoolCodeverifyURL += "school/DTL?SchCode="
        strSchoolCodeverifyURL += schCode.uppercased()
        strSchoolCodeverifyURL += "&key="
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.keyPath()
        
        WebserviceManager.getJsonData(withParameter: strSchoolCodeverifyURL) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["ErrorCode"] as? Int == 0 && result["Status"] as? String == "ok")
                        {
                            DBManager.shared.SaveDataIntoDB(strSchoolCode: schCode, result: result , fromStarting : false , forActiveSchool: true)
                        }
                    }
            }
        }
    }
    
    func startUpdate(){
        if self.btnClicked == false {
            self.checkVersion()
        }
        guard let urlStr = URL(string: APIConstants.eCareProAppLink) else {return}
        UIApplication.shared.open(urlStr)
    }
    
    func cancelUpdate() {
        self.btnClicked = true
        return
    }
    
}
//MARK::- verifyUser()
extension BoardViewController {
    
    func verifyUser() {
        self.view.endEditing(true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc   = storyboard.instantiateViewController(withIdentifier: "VerificationViewController") as? VerificationViewController else {return}
        if let topVc = self.navigationController?.parent as? CustomTabbarViewController {
            topVc.presentVC(vc)
        }
    }
    
}


//MARK::- PAGINATION METHODS
extension BoardViewController {
    
    func addRefereshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tblBoard?.addSubview(refreshControl)
    }
    
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        pageNo  = 1
        totalUpdates = 0
        self.webAPIUpdateRecent(pg: pageNo)
    }
    
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView) {
        if self.totalUpdates == /self.tableDataSource?.items?.count {
            //            Messages.shared.show(alert: .oops, message: "No More Updates", type: .warning , style:  .bottom)
            return
        }
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h  {
            
            if self.totalUpdates != /self.tableDataSource?.items?.count {
                pageNo = pageNo + 1
                self.webAPIUpdateRecent(pg: pageNo)
            }
        }
    }
    
}

