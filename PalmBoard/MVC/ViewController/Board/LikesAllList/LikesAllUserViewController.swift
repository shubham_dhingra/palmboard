import UIKit
import EZSwiftExtensions

class LikesAllUserViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var viewBgAbove: UIView?
    @IBOutlet weak var imgLike:  UIImageView?
    @IBOutlet weak var lblLikes: UILabel?
    @IBOutlet weak var tblLikesAllUser: UITableView?

    var arrayLikesUserAll = [[String: Any]]()
    var strTotalLikes     = String()
    var intQuestionID     = Int()
    var intWordID         = Int()
    var intThid           = Int()
    var intPhid           = Int()
    var intVId            = Int()
    var intWordThoughtQuestion = Int()//intWordThoughtQuestion 1- word, 2-Thought and 3- Question/Answer
    var intUserType            = Int()

    // MARK: - viewDidLoad method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        guard let themeColor = self.getCurrentSchool()?.themColor else { return}

        viewBgAbove?.backgroundColor = themeColor.hexStringToUIColor()

        tblLikesAllUser?.rowHeight          = UITableView.automaticDimension
        tblLikesAllUser?.estimatedRowHeight = 2000
        tblLikesAllUser?.tableFooterView    = UIView(frame: CGRect.zero)

        //lblLikes.text = "\(strTotalLikes)"
        self.likeAllList(intQuesID: intQuestionID)
        setUpForIphoneX()
       // let parent =  self.presentingViewController?.parent as! CustomTabbarViewController
       // parent.hideBar(boolHide: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = false
    }
    // MARK: - likeAllList method
    func likeAllList(intQuesID: Int)
    {
        //print("likeAllList")

        Utility.shared.loader()
        var strSchoolCode = String()
        var urlString     = String()

        if let schoolCode = self.getCurrentSchool()?.schCode {strSchoolCode =  schoolCode}
        if let userType   = self.getCurrentUser()?.userType  {intUserType   =  Int(userType)}
        
        urlString += urlString.webAPIDomainNmae()
        //intWordThoughtQuestion 1- word, 2-Thought and 3- Question/Answer 4- PhotoGallery
        switch intWordThoughtQuestion
        {
        case 1:
            urlString += "like/WordLikedBy?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&Wid="
            urlString += "\(intWordID)"
        case 2:
            urlString += "like/ThoughtLikedBy?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&thid="
            urlString += "\(intThid)"
        case 3:
            urlString += "like/QueLikedBy?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&qid="
            urlString += "\(intQuesID)"
        case 4:
            urlString += "like/PhotoLikedBy?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&Phid="
            urlString += "\(intPhid)"
        case 5:
            urlString += "like/VideoLikedBy?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&VId="
            urlString += "\(intVId)"
        default:
            break
        }

       // print("Func: urlString= \(urlString)")

            WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
                DispatchQueue.main.async
                    {
                        Utility.shared.removeLoader()
                        if let result = results
                        {
                          
                            if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                            {
                                if let arrayAll =  (result["LikeBy"]) as? [[String: Any]]{
                                    self.arrayLikesUserAll =  arrayAll
                                }
                                if self.arrayLikesUserAll.count == 0
                                {
                                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noDataFound(), fromVC: self)
                                }
                                else{
                                    
                                        if  self.arrayLikesUserAll.count > 1{
                                            self.lblLikes?.text = "\(self.arrayLikesUserAll.count) Likes"
                                        }
                                        else {
                                            self.lblLikes?.text = "\(self.arrayLikesUserAll.count) Like"
                                        }

                                    self.tblLikesAllUser?.delegate   = self
                                    self.tblLikesAllUser?.dataSource = self
                                    self.tblLikesAllUser?.reloadData()
                                }
                            }
                            else if(result["Status"] as? String == "ok" && result["Message"] as? String == "No record found")
                            {
                                print("else conditionBottom: nextUsernameAction")
                            }
                        }
                        if (error != nil)
                        {
                            print("LikesAll = \(error!)")

                            DispatchQueue.main.async
                                {
                                    self.view.isUserInteractionEnabled = true
                                    switch (errorcode!){
                                    case Int(-1009):
                                        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    default:
                                        print("errorcode = \(String(describing: errorcode))")
                                    }
                            }
                        }
                }
            }
    }
    // MARK: - tableView Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       return self.arrayLikesUserAll.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellLikesAllUser", for: indexPath as IndexPath) as! CusomLikesAllUserViewController
        
        if let strName             = self.arrayLikesUserAll[indexPath.row]["Name"] as? String{
            
            let stringArray        = strName.components(separatedBy: "|")
            cell.lblNameCell?.text  = "\(stringArray[0])"
            
            if(stringArray.count > 1){
                cell.lblClass?.text   = "\(stringArray[1])"
            }else{
                cell.lblClass?.text  = intUserType == 2 ? "Parent" : ""
            }
        }
        if let strPhotoURL = self.arrayLikesUserAll[indexPath.row]["Photo"] as? String{
            
            var strPrefix   = String()
            strPrefix      += strPrefix.imgPath1()
            var finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
            finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let urlPhoto    = URL(string: finalUrl)
            
            cell.imgViewCell?.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = cell.imgViewCell?.image?.af_imageRoundedIntoCircle()
                cell.imgViewCell?.image = img1
                
            }
        }else{
            cell.imgViewCell?.image = R.image.noProfile_Big()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblLikesAllUser?.deselectRow(at: indexPath, animated: true)
        guard let shortProfileVc   = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID   = self.arrayLikesUserAll[indexPath.row]["UserID"]   as! Int
        shortProfileVc.intUserType = self.arrayLikesUserAll[indexPath.row]["UserType"] as! Int
        self.pushVC(shortProfileVc)
    }
    // MARK: - ClkBtnCross1 method
    
}
