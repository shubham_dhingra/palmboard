import UIKit
import Alamofire

protocol DeleteDelegate: class{
    func deleteRow()
   // func deleteThoughtBoard()
}
protocol thoughtBoardDelegate: class{
     func deleteThoughtBoard()
}
class ReportViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var reasonsView:   UIView?
    @IBOutlet weak var reportButton:  UIButton?
    @IBOutlet var      reasonsBtn:    [UIButton]?
    @IBOutlet weak var txtViewReason: UITextView?
    @IBOutlet weak var lblTitle:      UILabel?
    @IBOutlet weak var lblSubtitle:   UILabel?
    @IBOutlet weak var btnRight:      UIButton?
    
    weak var thoughtDelegate: DeleteDelegate?
    weak var BoardDelegate: thoughtBoardDelegate?

    var screenNumber  = 1
    var strSchoolCode = String()
    var thoughtId     = Int()
    var userId        = Int()
    var userType      = Int()
    var deleteReport  = Int()
    var reportId      = Int()
    var strReason     = String()
    var intAnID       = Int()
    var intQID        = Int()
    var intQAThoughtReport = Int()
    var intThoughtBoard = Int()
    
     var strSender   = String()
     var intUserType = Int()
     var intUserID   = Int()

   // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        btnRight?.isUserInteractionEnabled = false
        reportButton?.isHidden             = false
        reasonsView?.isHidden              = true
        screenNumber                       = 1
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        switch deleteReport{
        case 0:
            self.reportButton?.setImage(R.image.delete(), for: .normal)
            self.reportButton?.setTitle("Delete" , for: .normal)
        case 1:
            reportButton?.setImage(R.image.report(), for: .normal)
            reportButton?.titleLabel?.text = "Report"
        default:
            break
        }
        reportButton?.addTarget(self, action: #selector(self.selectReason(sender:)), for: .touchUpInside)
    }
      // MARK: - selectReason
      @objc func selectReason(sender:UIButton)
      {
        if(deleteReport == 0)
        {
         //delete selected
            var  parameters = Parameters()
            var strPrefix   = String()
            var urlString   = String()
            strPrefix      += strPrefix.webAPIDomainNmae()
            urlString      += strPrefix

            if intQAThoughtReport == 1
            {
                parameters =
                    [   "SchoolCode": strSender,
                        "key": urlString.keyPath(),
                        "UserType": "\(intUserType)",
                        "UserID": "\(intUserID)",
                        "ThID": self.thoughtId,
                ]
                urlString          += "Thoughts/Delete"
            }
            else if intQAThoughtReport == 2
            {
                parameters =
                    [   "SchoolCode": strSender,
                        "key": urlString.keyPath(),
                        "UserType": "\(intUserType)",
                        "UserID": "\(intUserID)",
                        "QID": intQID,
                ]
                urlString          += "Question/DeleteQue"
            }
            else if intQAThoughtReport == 3
            {
                parameters = [
                    "SchoolCode": strSender,
                    "key": urlString.keyPath(),
                    "UserType": "\(intUserType)",
                    "UserID": "\(intUserID)",
                    "AnID": "\(intAnID)"
                ]
                urlString    += "Question/DeleteAns"
            }

            print("urlString= \n \(urlString)")
            print("parameters= \n \(parameters)")
   
        WebserviceManager.post(urlString: urlString , param: parameters) { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                                if let result = results
                                {
                                    self.reportButton?.isHidden   = true

                                    //print("Delete result \n \(result)")
                                    if(result["Status"] as? String == "ok")
                                    {
                                        if self.intQAThoughtReport == 1
                                        {
                                     WebserviceManager.showAlert(message: "Deleted successfully", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock:
                                        {
                                            if self.intThoughtBoard == 101{
                                                self.BoardDelegate?.deleteThoughtBoard()
                                            }
                                            else {
                                                self.thoughtDelegate?.deleteRow()
                                            }
                                            self.dismiss(animated: true, completion: nil)
                                        }, secondBtnBlock: {
                                            self.dismiss(animated: true, completion: nil)
                                        })
                                        }
                                        else if self.intQAThoughtReport == 2
                                        {
                                            WebserviceManager.showAlert(message: "Deleted successfully", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                                self.thoughtDelegate?.deleteRow()
                                                self.dismiss(animated: true, completion: nil)
                                            }, secondBtnBlock: {
                                                self.dismiss(animated: true, completion: nil)
                                            })
                                        }
                                        else if self.intQAThoughtReport == 3
                                        {
                                            WebserviceManager.showAlert(message: "Deleted successfully", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                                self.thoughtDelegate?.deleteRow()
                                                self.dismiss(animated: true, completion: nil)
                                            }, secondBtnBlock: {
                                                self.dismiss(animated: true, completion: nil)
                                            })
                                        }
                                    }
                                    }
                                }
                      }
            }
        else
        {
       //report selected
        self.reportButton?.isHidden  = true
        self.reasonsView?.isHidden   = false
        self.txtViewReason?.isHidden = true
        self.screenNumber           = 2
        }
    }
      // MARK: - reasonSelected1
    @IBAction func reasonSelected1(_ sender: UIButton) {
        for button in reasonsBtn! {
            // Set all the other buttons as normal state
                 button.isSelected = false
                //print("\(button.tag) deselected")
                button.setImage(nil, for: .normal)
                button.titleEdgeInsets = UIEdgeInsets(top: 0,left:15,bottom: 0,right: 0)
        }
        //print("reasonSelected")
        sender.isSelected      = true
        sender.setImage(R.image.rightSelected(), for: .normal)
        sender.imageEdgeInsets = UIEdgeInsets(top: 0,left: (self.reasonsView?.frame.width)! - 30,bottom: 0,right: 0)
        sender.titleEdgeInsets = UIEdgeInsets(top: 0,left: -10,bottom: 0,right: 0)
        reportId               = sender.tag + 1
        self.strReason         = (sender.titleLabel?.text)!
        btnRight?.setImage(R.image.greenRight(), for: .normal)
        btnRight?.isUserInteractionEnabled = true
        print("reportId = \(reportId)")
    }
    // MARK: - crossPressed
    @IBAction func crossPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
      // MARK: - tickPressed
    @IBAction func tickPressed(_ sender: UIButton) {
        
        switch screenNumber
        {
        case 2:
            (self.reasonsBtn as! NSArray).setValue(NSNumber(value: true), forKey: "hidden")
            self.txtViewReason?.isHidden       = false
            self.lblTitle?.text                = "Give Reason"
            self.lblSubtitle?.text             = self.strReason
            btnRight?.setImage(R.image.greyRight(), for: .normal)
            btnRight?.isUserInteractionEnabled = false
            screenNumber                      = 3
        case 3:
            self.txtViewReason?.isHidden = false
            self.reportThought()
        default:
            break
        }
        // print("userinteraction = \(self.btnRight?.isUserInteractionEnabled)")
    }
    // MARK: - reportThought
    func reportThought()
    {
        reasonsView?.isHidden              = true
        var  parameters = Parameters()
        var strPrefix   = String()
        var urlString   = String()
        strPrefix      += strPrefix.webAPIDomainNmae()
        urlString      += strPrefix

        if intQAThoughtReport == 1
        {
            parameters =
                [   "SchoolCode": strSender,
                    "key": APP_CONSTANTS.KEY,
                    "UserType": "\(intUserType)",
                    "UserID": "\(intUserID)",
                    "ThID": thoughtId,
                    "RpID": reportId,
                    "Reason": txtViewReason?.text
                ]
            urlString          += "Thoughts/Report"
        }
        else if intQAThoughtReport == 2
        {
            parameters =
                [   "SchoolCode": strSender,
                    "key": APP_CONSTANTS.KEY,
                    "UserType": "\(intUserType)",
                    "UserID": "\(intUserID)",
                    "QID": intQID,
                    "RpID": reportId,
                    "Reason": txtViewReason?.text
                ]
            urlString          += "Question/Report"
        }
        else if intQAThoughtReport == 3
        {
            parameters =
                [   "SchoolCode": strSender,
                    "key": APP_CONSTANTS.KEY,
                    "UserType": "\(intUserType)",
                    "UserID": "\(intUserID)",
                    "AnID": intAnID,
                    "RpID": reportId,
                    "Reason": txtViewReason?.text
                ]
            urlString          += "Question/ReportAns"
        }
        print("urlString= \n \(urlString)")
        print("parameters= \n \(parameters)")

        //"\(urlString)api/Thoughts/Report"
        WebserviceManager.post(urlString:urlString , param: parameters) { (results, error, errorCode) in
            
            DispatchQueue.main.async
                {
                if let result = results
                  {
                   print("report result \n \(result)")
                     if(result["Status"] as? String == "ok")
                     {
                        if self.intQAThoughtReport == 1
                        {
                            WebserviceManager.showAlert(message: "We are sorry that you have this experience. We will review this photo and if it violates our community standards, we will remove it permanently.\n (Till the time the post is hidden)", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                            
                                if self.intThoughtBoard == 101{
                                    self.BoardDelegate?.deleteThoughtBoard()
                                }
                                else {
                                    self.thoughtDelegate?.deleteRow()
                                }
                            self.dismiss(animated: true, completion: nil)
                            
                            }, secondBtnBlock: {})
                       }
                        else if self.intQAThoughtReport == 2
                        {
                            WebserviceManager.showAlert(message: "We are sorry that you have this experience. We will review this photo and if it violates our community standards, we will remove it permanently.\n (Till the time the post is hidden)", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                
                                self.thoughtDelegate?.deleteRow()
                                self.dismiss(animated: true, completion: nil)
                            }, secondBtnBlock: {})
                        }
                        else if self.intQAThoughtReport == 3
                        {
                            self.thoughtDelegate?.deleteRow()
                            self.dismiss(animated: true, completion: nil)
                            /*
                            WebserviceManager.showAlert(message: "Answer reported successfully", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock:
                                {
                                self.dismiss(animated: true, completion: nil)
                                self.thoughtDelegate?.deleteRow()
                            }, secondBtnBlock: {})*/
                        }
                        }
                    }
               }
        }
    }
    func animateTxtView(txtView: UITextView, up: Bool){
        let movementDistance = -40
        let movementDuration = 0.3
        let movement         = (up ? movementDistance : -movementDistance)
        
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
      // MARK: - TextField Delegate Method
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if(textView.text == "Type Reason"){
            textView.selectedRange = NSMakeRange(0, 0)
            textView.text = ""
        }
        animateTxtView(txtView: textView , up: true)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        animateTxtView(txtView: textView , up: false)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         textView.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
 
        if(textView.text.count  > 3 ){
           self.btnRight?.isUserInteractionEnabled = true
           btnRight?.setImage(R.image.greenRight(), for: .normal)
        }else{
            print("isUserInteractionEnabled disabled")
            self.btnRight?.isUserInteractionEnabled = false
            btnRight?.setImage(R.image.greyRight(), for: .normal)
        }
        return true
    }
      // MARK: - touchesBegan
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(self.reportButton?.isHidden)!{}
        else{
           dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
