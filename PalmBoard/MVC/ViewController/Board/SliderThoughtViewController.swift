//  SliderThoughtViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 31/10/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import AlamofireImage
import Alamofire

//protocol resumeTimerDelegate {
//    func ClkbtnThoughtLikePressed(btnRef: UIButton,btnRefAllLikes: UIButton,intThoughtID: Int,intIsLikeStatus: Int)
//    func ClkbtnThoughtReportPressed(btnRef: UIButton,intUserIDThought: Int,intThoughtID: Int)
//
//}
class SliderThoughtViewController: UIViewController
{
    @IBOutlet weak var btnName:     UIButton?
    @IBOutlet weak var btnProfile:  UIButton?
    @IBOutlet weak var lblThought:  UILabel?
    @IBOutlet weak var lblAuthor:   UILabel?
    @IBOutlet weak var lblUpdated:  UILabel?
    @IBOutlet weak var btnLike:     UIButton?
    @IBOutlet weak var btnAllLikes: UIButton?
    @IBOutlet weak var btnReport:   UIButton?
    @IBOutlet weak var bgView:      UIView?
    
   // @IBOutlet weak var lblModule:      UILabel!
    //@IBOutlet weak var lblDescription: UILabel!
    
    var photoIndex = 0
    var strImgURL:        String!
    var strThought:       String!
    var strThoughtAuthor: String!
    var strName:          String!
    var strLikesAll:      String!
    var strLikes:         String!
    var strAuth:          String!

    var intThoughtLikes    = Int()
    var intThoughtIsLikes  = Int()
    var isReported         = Int()
    var intThoughtUserID   = Int()
    var intThoughtUserType = Int()
    var intThoughtisVerified = Int()
    var intThID              = Int()
    var itemIndex            = Int()

    var intUserType          = Int()
    //var imgUrl :        String!
    let selectedColor   = UIColor(rgb: 0x56B54B)
    let unselectedColor = UIColor(rgb: 0x5E5E5E)
    
   // var timerDelegate: resumeTimerDelegate?

    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        print("Func: viewWillAppear (SliderThoughtViewController)")
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async
        {
            print("UserID     = \(self.intThoughtUserID)")
            print("UserType   = \(self.intThoughtUserType)")
            print("isVerified = \(self.intThoughtisVerified)")
            print("ThID       = \(self.intThID)")
            
            print("Thought = \(/self.strThought)")
            print("Author = \(/self.strThoughtAuthor)")
            print("Likes = \(self.intThoughtLikes)")
            
            var nsString = String()
             nsString = self.strThought
            print("count = \(nsString.count)")

            if nsString.count >= 140
            {
                self.lblThought?.text = "\"\(String(nsString[nsString.startIndex..<nsString.index(nsString.startIndex, offsetBy: 140)]))\""
            }
            else{
                self.lblThought?.text = "\"\(self.strThought!)\""

              //  self.lblThought.text = self.strThought// "Don’t mask or call special attention to key display features. Don’t attempt to hide the device’s rounded corners, sensor housing, or arty hh"//
            }
            if (self.strThoughtAuthor) != nil
            {
                self.strAuth  = "~\(self.strThoughtAuthor!)"
            }
            else{
                 self.strAuth         = "~anonymous"

            }
       // self.strAuth         = "~\(self.strThoughtAuthor!)"
        // self.strAuth        += self.strThoughtAuthor
        self.lblAuthor?.text  = self.strAuth

        self.btnLike?.tag     = self.itemIndex
        self.btnAllLikes?.tag = self.itemIndex
        self.btnReport?.tag   = self.itemIndex
        self.btnProfile?.tag  = self.itemIndex
        self.btnName?.tag     = self.itemIndex
           
            
            if let userType = self.getCurrentUser()?.userType{
                self.intUserType =  Int(userType)
            }
            


       // self.btnAllLikes.isHidden = false
      //  self.btnName.setTitle(self.strName.replace(target: "|", withString: ", "),for: .normal)
            var str1 = String()
            var str2 = String()

            let stringArray        = self.strName.components(separatedBy: "|")
           // cell.lblNameCell.text  = "\(stringArray[0])"
            str1 = "\(stringArray[0])"
            
            if(stringArray.count > 1){
                str2 = "\(stringArray[1])"
            }else{
                print()
                if self.intUserType == 2 {
                    str2     = "Parent"
                }
                else {
                   str2     = ""
                }
            }
            str1 += ", "
            str1 += str2
             self.btnName?.setTitle((str1),for: .normal)

            switch self.intThoughtisVerified
            {
            case 0:
                self.btnReport?.isHidden = false
            case 1:
                self.btnReport?.isHidden = true
            default:
                break
            }
            
            if self.intThoughtLikes > 1{
                self.btnAllLikes?.setTitle("\(self.intThoughtLikes) Likes",for: .normal)
                //self.btnAllLikes.setTitle("50 Likes",for: .normal)
            }
            if self.intThoughtLikes == 1{
                self.btnAllLikes?.setTitle("\(self.intThoughtLikes) Like",for: .normal)
               // self.btnAllLikes.setTitle("500 Likes",for: .normal)
            }
            else if self.intThoughtLikes == 0
            {
              //  self.btnAllLikes.isHidden = true
                self.btnAllLikes?.setTitle("",for: .normal)
                //self.btnAllLikes.setTitle("5000 Likes",for: .normal)
            }
            if self.intThoughtIsLikes == 0{
                self.btnLike?.setImage(R.image.like(), for: UIControl.State.normal)
                self.btnLike?.setTitleColor(self.unselectedColor, for: .normal)
            }
            else if self.intThoughtIsLikes == 1{
                self.btnLike?.setImage(R.image.liked(), for: UIControl.State.normal)
                self.btnLike?.setTitleColor(self.selectedColor, for: .normal)
            }
            print("strImgURL            = \(self.strImgURL!)")

            var strPrefix   = String()
            strPrefix       += strPrefix.imgPath1()
            var finalUrl    = "\(strPrefix)" +  "\(self.strImgURL!)"
            
            print("finalUrl            = \(finalUrl)")
           // print("self.strModuleName  = \(self.strModuleName)")
            //print("self.strDescription = \(self.strDescription)")
            finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let url = URL(string: finalUrl)
            
            let imgView = UIImageView()
            imgView.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                
                let img1 = imgView.image?.af_imageRoundedIntoCircle()
                self.btnProfile?.setBackgroundImage(img1 as UIImage?, for: .normal)
            }
        }
    }
    
    @IBAction func ClkBtnProfileThought(_ sender: UIButton){
        print("Func: ClkBtnProfileThought = \(sender.tag)")
       // self.performSegue(withIdentifier: "seguePageProfile", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID     = intThoughtUserID
        shortProfileVc.intUserType   = intThoughtUserType
        self.pushVC(shortProfileVc)
    }

}
