//  SubReportsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 09/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class SubReportsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK::- OUTLETS
    @IBOutlet weak var collectionViewReport: UICollectionView?
    
    //MARK::- VARIABLES
    var arraySubReports = [[String: Any]]()
    var strTitle        = String()
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = strTitle
        print("arraySubReports=\(arraySubReports)")
        self.collectionViewReport?.delegate = self
        self.collectionViewReport?.dataSource = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return self.arraySubReports.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellRports = collectionView.dequeueReusableCell(withReuseIdentifier: "cellSubReports", for: indexPath) as! SubReports
        var intReport = Int()
        if let intReport1 = self.arraySubReports[indexPath.row]["SRptID"] as? Int{
            intReport = intReport1
        }
        switch intReport {
        case 1:
            cellRports.imgView?.image =  R.image.attendance_report()
        case 2,9:
            cellRports.imgView?.image =  R.image.leave_report()
        case 3:
            cellRports.imgView?.image =  R.image.reportcard_report()
        case 4:
            cellRports.imgView?.image =  R.image.statisticalReports()
        case 5:
            cellRports.imgView?.image =  R.image.appUser_report()
        case 6:
            cellRports.imgView?.image =  R.image.calsslist_report()
        case 7:
            cellRports.imgView?.image =  R.image.teacherlist_report()
        case 10:
            cellRports.imgView?.image =  R.image.attendance_report()
        default:
            break
        }
        if let strTitle = self.arraySubReports[indexPath.row]["Title"] as? String{
            cellRports.lblName?.text = strTitle
        }
        return cellRports
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Did select row= \(indexPath.row)")
        var intReport = Int()
        if let intReport1 = self.arraySubReports[indexPath.row]["SRptID"] as? Int{
            intReport = intReport1
        }
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        switch intReport {
        case 1:
            guard let attendanceSummaryVc = R.storyboard.reports.attendanceSummaryViewController() else {
                return
            }
            self.pushVC(attendanceSummaryVc)
//            guard let StatisticalVc = storyboard.instantiateViewController(withIdentifier: "AttendanceReportViewController") as? AttendanceReportViewController else {return}
//            self.pushVC(StatisticalVc)
            
        case 2 , 9:
            guard let leavesReportVc = storyboard.instantiateViewController(withIdentifier: "LeavesReportViewController") as? LeavesReportViewController else {return}
            leavesReportVc.leaveUserType = intReport == 2 ? .Student : .Staff
            self.pushVC(leavesReportVc)
        case 3:
            guard let reportCardVc = storyboard.instantiateViewController(withIdentifier: "StaffReportCardViewController") as? StaffReportCardViewController else {return}
            self.pushVC(reportCardVc)
        case 4:
            guard let StatisticalVc = storyboard.instantiateViewController(withIdentifier: "StatisticalReportViewController") as? StatisticalReportViewController else {return}
            self.pushVC(StatisticalVc)
        case 5:
            guard let appUserVC = storyboard.instantiateViewController(withIdentifier: "AppUserReportViewController") as? AppUserReportViewController else {return}
            self.pushVC(appUserVC)
        case 6:
            guard let appUserVC = storyboard.instantiateViewController(withIdentifier: "ClassListReportViewController") as? ClassListReportViewController else {return}
            self.pushVC(appUserVC)
        case 7:
            guard let appUserVC = storyboard.instantiateViewController(withIdentifier: "TeacherListReportViewController") as? TeacherListReportViewController else {return}
            self.pushVC(appUserVC)
        case 10:
            guard let executiveVc = storyboard.instantiateViewController(withIdentifier: "ExecutiveAttendanceReportViewController") as? ExecutiveAttendanceReportViewController else {return}
            executiveVc.intVCselection = 1
            self.pushVC(executiveVc)
        default:
            break
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionViewReport!.frame.width/2 - 5, height: 132)//self.view.frame.width/2,170
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}
}
class SubReports: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var lblName: UILabel?
}
