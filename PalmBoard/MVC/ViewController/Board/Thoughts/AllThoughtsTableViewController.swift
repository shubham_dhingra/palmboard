import UIKit
import Alamofire

class AllThoughtsTableViewController: BaseViewController,DeleteDelegate,UIScrollViewDelegate
{
    @IBOutlet weak var segmentControl:    UISegmentedControl?
    @IBOutlet weak var segmentBgView:     UIView?
    @IBOutlet weak var imgViewNoThoughts: UIImageView?
    @IBOutlet weak var lblNoThoughts:     UILabel?
    
    var refreshControl     = UIRefreshControl()
    var spinner            : UIActivityIndicatorView?
    var btnTitle           : UIButton?
    var arrayThoughts      : [List]?
    var tempArray          : [List]?
    var isLiked              = false
    var isFirstTime : Bool   = true
    var isPull2Refresh       = Bool()
    var schoolCode           = String()
    var userPhotoUrl         = String()
    var intTotalLikes        = Int()
    var deleteRowIndex       = IndexPath()
    var likeTag              = 0
    var deleteOrReport       = 0
    var reportThoughtId      = 0
    var reportUserId         = 0
    var intSegment           = 1
    var page                 = 1
    var noMoreData : Bool = false
    
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        //  self.navigationController?.setNavigationBarHidden(false, animated: true)
        let nib = UINib(nibName: "Thoughts", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: CellIdentifiers.cellThoughts.get)
        tableView?.tableFooterView    = UIView(frame: CGRect.zero)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        guard let themeColor            = self.getCurrentSchool()?.themColor else { return}
        segmentControl?.backgroundColor = themeColor.hexStringToUIColor()
        segmentBgView?.backgroundColor  = themeColor.hexStringToUIColor()
        
        spinner                  = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner?.color            = UIColor(rgb: 0x56B54B)
        spinner?.hidesWhenStopped = true
        spinner?.frame            = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        tableView?.tableFooterView = spinner
        spinner?.stopAnimating()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
        
        self.isPull2Refresh  = false
        
        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle?.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle?.setTitle("Thoughts", for: UIControl.State.normal)
        btnTitle?.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle?.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle?.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        segmentControl?.selectedSegmentIndex = 0
        intSegment    =  1
    }
    
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        imgViewNoThoughts?.isHidden = true
        lblNoThoughts?.isHidden     = true
        
        isLiked     = false
        if let strPhotoCheck = self.getCurrentUser()?.photoUrl  {userPhotoUrl    =  strPhotoCheck}
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        self.arrayThoughts = []
        webAPIThoughts(intSegment: intSegment, intPage: 1)
    }
    
    
    // MARK: - webAPICall
    func webAPIThoughts(intSegment: Int?,intPage : Int?)
    {
        // self.tableView?.isHidden = true
        self.imgViewNoThoughts?.isHidden = true
        self.lblNoThoughts?.isHidden     = true
        if(self.page == 1 && !self.isPull2Refresh){
            self.segmentControl?.isUserInteractionEnabled = false
        }
        APIManager.shared.request(with: HomeEndpoint.Thoughts(UserID: self.userID, UserType: self.userType, all: intSegment?.toString, dir: 1.toString, pg: intPage?.toString), isLoader: isFirstTime) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK::- handle
    func handle(response : Any?){
        if let model = response as? ThoughtsModal
        {
            tempArray                = model.list
            self.tableView?.isHidden = false
            self.segmentControl?.isUserInteractionEnabled = true
            self.spinner?.stopAnimating()
            self.refreshControl.endRefreshing()
            self.noMoreData = tempArray?.count == 0
            if /tempArray?.count > 0 {
                if(self.page == 1){
                    self.arrayThoughts   = tempArray
                    self.isPull2Refresh  = false
                }
                else{
                    if let tempArrayA = self.tempArray {
                        self.arrayThoughts?.append(contentsOf: tempArrayA)
                    }
                }
                reloadTable()
            }
            else{
                if(self.arrayThoughts?.count == 0 && self.page == 1){
                    self.imgViewNoThoughts?.isHidden = false
                    self.lblNoThoughts?.isHidden     = false
                    self.tableView?.isHidden = true
                }
                else
                {
                    if self.page != 1{
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noMoreThoughts(), fromVC: self)
                    }
                }
            }
        }
    }
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        //print("refresh pulled")
        self.isPull2Refresh = true
        noMoreData = false
        page = 1
        webAPIThoughts(intSegment: self.intSegment, intPage: page)
    }
    // MARK: - deleteRow
    func deleteRow()
    {
        self.arrayThoughts?.remove(at: deleteRowIndex.row)
        //  self.tableView?.deleteRows(at: [deleteRowIndex], with: .automatic)
        self.tableView?.setContentOffset(.zero, animated: false)
        // self.tblView?.reloadData()
        self.imgViewNoThoughts?.isHidden = self.arrayThoughts?.count != 0
        self.lblNoThoughts?.isHidden     = self.arrayThoughts?.count != 0
        
        reloadTable()
    }
    @IBAction func addThoughtPressed(_ sender: UIBarButtonItem) {
        guard let vc   = R.storyboard.main.updateThoughtViewController() else {return}
        vc.userPhotoUrl = self.userPhotoUrl
        vc.schoolCode   =  self.schoolCode
        //print("Before segue count \(arrayThoughts.count)")
        self.pushVC(vc)
    }
    
    
    // MARK: - segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        if(segmentControl?.selectedSegmentIndex == 0){
            intSegment = 1
        }
        else if(segmentControl?.selectedSegmentIndex == 1){
            intSegment = 0
        }
        self.arrayThoughts?.removeAll()
        self.noMoreData = false
        page = 1
        webAPIThoughts(intSegment: self.intSegment, intPage: page)
    }
    
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.tableView?.scrollToRow(at: [0,0], at: .top, animated: true)
    }
}
extension AllThoughtsTableViewController:ThoughtsDelegate {
    // MARK: - ClkBtnProfile
    func ClkBtnProfile(tag: Int) {
        guard let shortProfileVc     = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID     = /self.arrayThoughts?[tag].userID
        shortProfileVc.intUserType   = /self.arrayThoughts?[tag].userType
        self.pushVC(shortProfileVc)
    }
    
    // MARK: - ClkLikesAllList
    func ClkLikesAllList(tag: Int) {
        intTotalLikes = /self.arrayThoughts?[tag].likes
        guard let likesAllVc              = R.storyboard.main.likesAllUserViewController() else {return}
        likesAllVc.strTotalLikes          = intTotalLikes > 1 ? "\(intTotalLikes) Likes" : "\(intTotalLikes) Like"
        likesAllVc.intThid                = /self.arrayThoughts?[tag].thID
        likesAllVc.intWordThoughtQuestion = 2
        self.pushVC(likesAllVc)
    }
    // MARK: - ClkLikePressed
    func ClkLikePressed(btn:UIButton) {
        btn.isUserInteractionEnabled = false
        let indexPath = IndexPath(row: btn.tag, section: 0)
        //print("like row = \(indexPath.row)")
        if let intThoughtId = self.arrayThoughts?[btn.tag].thID{
            if let boolIsLiked = self.arrayThoughts?[btn.tag].isILike {
                self.isLiked = boolIsLiked == 1 ? false : true
                self.likeTag = boolIsLiked == 1 ? 0 : 1
            }
            //isILike
            APIManager.shared.request(with: HomeEndpoint.LikeThought(UserID: self.userID, UserType: self.userType, ThID: intThoughtId.toString, isLike: self.likeTag.toString), isLoader: false) {[weak self] (response) in
                self?.handleResponse(response: response, responseBack: { (success) in
                    self?.handleThoughtLike(response: success,indexPath: indexPath,sender: btn)
                })
            }
        }
    }
    //MARK::- handleThoughtLike
    func handleThoughtLike(response : Any?,indexPath: IndexPath,sender: UIButton){
        if let model = response as? ThoughtsLikeModal
        {
            self.arrayThoughts?[indexPath.row].likes   = model.totalLikes
            self.arrayThoughts?[indexPath.row].isILike = self.isLiked == true ? 1 : 0
            self.tableView?.reloadRows(at: [indexPath], with: .automatic)
            sender.isUserInteractionEnabled = true
        }
    }
    // MARK: - ClkDotsPressed
    func ClkDotsPressed(tag: Int) {
        let indexPath   = IndexPath(row: tag, section: 0)
        deleteRowIndex  = indexPath
        
        if let intThoughtId      = self.arrayThoughts?[tag].thID{
            self.reportThoughtId = intThoughtId
        }
        if let intUserId = self.arrayThoughts?[tag].userID{
            self.reportUserId = intUserId
        }
        if(segmentControl?.selectedSegmentIndex == 1 || (segmentControl?.selectedSegmentIndex == 0 && /self.userID?.toInt() == self.reportUserId))
        {
            //print("Delete thought")
            deleteOrReport = 0
        }
        else{
            //report
            deleteOrReport = 1
        }
        guard let reportVc          = R.storyboard.main.reportViewController() else {return}
        reportVc.deleteReport       = self.deleteOrReport
        reportVc.strSchoolCode      = self.schoolCode
        reportVc.userId             = self.reportUserId
        reportVc.userType           = /self.userType?.toInt()
        reportVc.thoughtId          = self.reportThoughtId
        reportVc.thoughtDelegate    = self
        reportVc.intQAThoughtReport =  1
        self.presentVC(reportVc)
    }
    
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        let y      = offset.y + bounds.size.height - inset.bottom
        let h      = size.height
        
        if noMoreData {
            return
        }
        
        if y > h && /arrayThoughts?.count > 0 {
            spinner?.startAnimating()
            if !isPull2Refresh {
                page += 1
                webAPIThoughts(intSegment: self.intSegment, intPage: page)
            }
        }
    }
}
extension AllThoughtsTableViewController
{
    func configureTableView() {
        tableDataSource = TableViewDataSource(items: self.arrayThoughts, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.cellThoughts.get)
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? ThoughtsTableViewCell)?.delegate       = self
            (cell as? ThoughtsTableViewCell)?.index          = indexpath?.row
            (cell as? ThoughtsTableViewCell)?.segmentControl = self.segmentControl
            (cell as? ThoughtsTableViewCell)?.modal          = item
        }
        tableDataSource?.scrollViewDidEndDragging = {(scrollView) in
            self.reloadMoreData(scrollView)
        }
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = self.arrayThoughts
            tableView?.reloadData()
        }
    }
}

