import UIKit
import Alamofire

class UpdateThoughtViewController: BaseViewController, UITextViewDelegate, UITextFieldDelegate
{
    @IBOutlet weak var imgStudent:     UIImageView?
    @IBOutlet weak var lblStudentInfo: UILabel?
    @IBOutlet weak var txtViewThought: UITextView?
    @IBOutlet weak var txtFldAuthot:   UITextField?
    @IBOutlet weak var txtViewHeight:  NSLayoutConstraint?
    
    var schoolCode         = String()
    var intUserId          = Int()
    var intUserType        = Int()
    var userPhotoUrl       = String()
    var textViewHeightTemp = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let strName      = self.getCurrentUser()?.name      else { return}
        guard var strClass     = self.getCurrentUser()?.userClass else { return}
        guard let userType     = self.getCurrentUser()?.userType  else { return}
        
        if userType == 2 {
            strClass = "Parent"
        }
        lblStudentInfo?.text      = "\(strName), \(strClass)"
        textViewHeightTemp        = (txtViewHeight?.constant)!
        txtViewThought?.text      = "Enter Thought"
        txtViewThought?.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
        txtFldAuthot?.useUnderline()
        // txtViewThought?.textColor = UIColor.lightGray
        txtViewThought?.font      = R.font.ubuntuLight(size: 17) 
        guard let themeColor      = self.getCurrentSchool()?.themColor else { return}
        txtViewThought?.tintColor = themeColor.hexStringToUIColor()
        txtFldAuthot?.tintColor   = themeColor.hexStringToUIColor()
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 168/255, green: 168/255, blue: 168/255, alpha:1)
        
        let url  = userPhotoUrl.getImageUrl()
        self.imgStudent?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: R.image.noProfile_Big())
        self.imgStudent?.layer.cornerRadius =  (self.imgStudent?.frame.height)! / 2
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    // MARK: - donePressed - Upload API
    @IBAction func donePressed(_ sender: UIBarButtonItem) {
        txtFldAuthot?.resignFirstResponder()
        let authotString = self.txtFldAuthot?.text?.count == 0 ? "Anonymous" : self.txtFldAuthot?.text
        APIManager.shared.request(with: HomeEndpoint.ThoughtsCreate(Quotation: /self.txtViewThought?.text!, Author: authotString!, UserID: self.intUserId.toString, UserType: self.intUserType.toString)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK::- handle
    func handle(response : Any?){
        if let model = response as? ThoughtsLikeModal
        {
            if model.status == "ok" {
                self.view.endEditing(true)
                WebserviceManager.showAlert(message: /model.message, title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    //self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                }, secondBtnBlock: {})
            }
        }
    }
    // MARK: - textview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView){
        //print("textViewDidBeginEditing")
        if(textView.text == "Enter Thought"){
            textView.text = ""
        }
        if(txtFldAuthot?.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            txtFldAuthot?.placeholder  = "Enter Author Name"
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //print("textViewDidEndEditing \n textView = \(textView.text)")
        if(textView.text.trimmingCharacters(in: .whitespacesAndNewlines) == "")
        {
            //print("txtViewHeight.constant = \(txtViewHeight.constant)")
            txtViewHeight?.constant = 35
            textView.textColor      = UIColor.lightGray
            textView.font           = R.font.ubuntuLight(size: 17)
            textView.text           = "Enter Thought"
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 168/255, green: 168/255, blue: 168/255, alpha:1)
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //print("shouldChangeTextIn")
        textView.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
        textView.font = R.font.ubuntuLight(size: 20)
        
        if(textView.text.count > 3 ){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 255/255, green: 187/255, blue: 0/255, alpha:1)
        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 168/255, green: 168/255, blue: 168/255, alpha:1)
        }
        
        let contentSize       = self.txtViewThought?.sizeThatFits((self.txtViewThought?.bounds.size)!)
        let numLinesDecimal   = (textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom) / (textView.font?.lineHeight)!
        let numLines          = floor(numLinesDecimal)
        var textViewtempframe = textView.frame
        
        if text.count == 0 && range.length > 0 {
            
            if(numLines > 1){
                textViewtempframe.size.height = self.textViewHeightTemp - 20
                txtViewHeight?.constant = textViewtempframe.size.height
            }
            else if(numLines == 1){
                //print("numLines = 1 txtViewHeight.constant = \(txtViewHeight.constant)")
                self.textViewHeightTemp = (txtViewHeight?.constant)!
            }
        }else{
            if(numLines < 5){
                self.txtViewHeight?.constant = (contentSize?.height)!
                self.textViewHeightTemp = (self.txtViewHeight?.constant)!
            }
        }
        let newText       = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 140
    }
    // MARK: - textfield Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField){
        textField.placeholder = nil
        animateTxtFld(txtField: textField , up: true)
        if(txtViewThought?.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            txtViewThought?.text = "Enter Thought"
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        animateTxtFld(txtField: textField, up: false)
        if(textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            textField.placeholder  = "Enter Author Name"
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.font = R.font.ubuntuRegular(size: 15)
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let numberOfChars = newText.count
        //print(" textField numberOfChars =\(numberOfChars)")
        return numberOfChars <= 50
    }
    // MARK: - animateTxtFld
    func animateTxtFld(txtField: UITextField, up: Bool){
        let movementDistance  = -75
        let movementDuration  = 0.3
        let movement          = (up ? movementDistance : -movementDistance)
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
}
