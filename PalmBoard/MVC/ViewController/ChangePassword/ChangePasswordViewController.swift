//  ChangePasswordViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 31/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class ChangePasswordViewController: BaseViewController {
    //MARK::- OUTLETS
    @IBOutlet weak var txtCurrentPassword : UITextField?
    @IBOutlet weak var lblConditions      : UILabel?
    @IBOutlet weak var txtNewPassword     : UITextField?
    @IBOutlet weak var txtConfirmPassword : UITextField?
    @IBOutlet weak var btnDone            : UIButton?
    
    //MARK::- VARIABLES
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = true
    }
    //MARK::- BUTTON ACTIONS
    @IBAction func btnDoneAct(_ sender : UIButton){
        print("Old Password :\(/txtCurrentPassword?.text)")
        print("New Password :\(/txtNewPassword?.text)")
        print("Confirm Password :\(/txtConfirmPassword?.text)")
        
        if "".validateChangePassword(old: txtCurrentPassword?.text, new: txtNewPassword?.text, confirm: txtConfirmPassword?.text){
            changePasswordAPI()
        }
    }
    
    
    func onViewDidLoad() {
    
        lblConditions?.text = "1. Include at least one number & symbol.\n2. Include both lower and upper case.\n3. Should be at least 8 characters long.\n4. Do not enter current password."
    }
    
    //MARK::- changePasswordAPI
    func changePasswordAPI(){
        APIManager.shared.request(with: HomeEndpoint.ChangePassword(UserID: userID, UserType: userType, NewPassword: txtNewPassword?.text)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                
                guard let password = self?.txtNewPassword?.text else {return}
                DBManager.shared.updateUserDetailsIntoLocalDB(newValue:password , entity: .password)
                

                Messages.shared.show(alert: .success, message: AlertMsg.changePasswordSuccess.get , type: .success)
                self?.dismissVC(completion: nil)
            })
        }
    }
}


extension ChangePasswordViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtCurrentPassword {
            txtNewPassword?.becomeFirstResponder()
            return false
        }
        else if textField == txtNewPassword {
            txtConfirmPassword?.becomeFirstResponder()
            return false
        }
        else if textField == txtConfirmPassword {
            textField.resignFirstResponder()
            return true
        }
        return true
    }
}
