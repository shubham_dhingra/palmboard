//
//  CircularDetailsViewController.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit
class CircularDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet weak var tblCircularDetails: UITableView?
    
    var dictCircularDetails =  [String: Any]()
    var intCirID            = Int()
    var strSenderDetails   = String()
    var intUserIDDetails   = Int()
    var intUserTypeDetails = Int()
    
    var strFilePath        = String()
    var strPrefix          = String()
    var urlPDF: URL!
    var finalUrl = String()
    var strSize   = String()
    var strDouble = Double()
    var btnTitle       =  UIButton()
    var isRead:  Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblCircularDetails?.rowHeight          = UITableView.automaticDimension
        tblCircularDetails?.estimatedRowHeight = 2000
        tblCircularDetails?.tableFooterView    = UIView(frame: CGRect.zero)
        
        tblCircularDetails?.delegate   = nil
        tblCircularDetails?.dataSource = nil
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)

        self.webAPICircularDetails()

    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
       // self.title = "Circular"
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Circular", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnTitle(sender:)), for: .touchUpInside)

        self.navigationItem.titleView = btnTitle

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        

    }
    @objc func ClkBtnTitle(sender:UIButton) {
        self.tblCircularDetails?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }

    //MARK:- webAPICircularDetails
    func webAPICircularDetails()
    {
        print("webAPICircularDetails")
        tblCircularDetails?.isHidden    = true
       
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()

        
        urlString += "Circular/Dtl?SchCode=\(strSenderDetails)&key=\(urlString.keyPath())&UserID=\(intUserIDDetails)&UserType=\(intUserTypeDetails)&CirID=\(intCirID)"
        
        print("Func: urlString= \(urlString)")
        self.view.isUserInteractionEnabled = false
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        print("*** result = \(result)")
                        
                       
                        
                        self.view.isUserInteractionEnabled = true
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            self.dictCircularDetails = results?["Circuler"] as! [String : Any]
                            print("dictCircularDetails count = \(self.dictCircularDetails.count)")
                            
                            if self.dictCircularDetails.count == 0
                            {
                                self.tblCircularDetails?.isHidden    = true
                                
                                let alert = UIAlertController(title: "", message: "Circular details is not found", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {

                                self.tblCircularDetails?.isHidden   = false
                                self.tblCircularDetails?.delegate   = self
                                self.tblCircularDetails?.dataSource = self
                                self.tblCircularDetails?.reloadData()
                            }
                        }
                    }
                    if (error != nil){
                        
                        print("userVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                self.tblCircularDetails?.isHidden = false
                                
                                self.view.isUserInteractionEnabled = true
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //return 2
        var intHasAttachment = Int()
        if let intHasAttachment1 =  self.dictCircularDetails["hasAttachment"] as? Int
        {
            intHasAttachment = intHasAttachment1
        }
        if  intHasAttachment == 1{
            return 2
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cellCircularDetails", for: indexPath as IndexPath) as! CustomCircularDetailsTableViewCell
            
            cell.lblHeading?.text     = self.dictCircularDetails["Title"] as? String
            cell.txtViewDetails?.text = self.dictCircularDetails["Message"] as? String
            cell.txtViewDetails?.dataDetectorTypes = UIDataDetectorTypes.link // Update UITextView content textVw.text = yourstring // Update hyperlink text colour.
            cell.txtViewDetails?.linkTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.blue, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
            
            var strDate             = String()
            strDate                 = self.dictCircularDetails["UpdatedOn"] as! String
            
            let formatter           = ISO8601DateFormatter()
            formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
            let date1               = formatter.date(from: strDate)
            strDate                 = formatter.string(from: date1!)
            
            let dateFormatterGet        = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "dd MMM, yyyy"
            
            let date: Date? = dateFormatterGet.date(from: strDate)
            
            if let updatedDate =  self.dictCircularDetails["UpdatedOn"] as? String {
                if dateFormatter.string(from: date!).count != 0  && updatedDate.getTimeIn12HourFormat().count != 0  {
                    cell.lblDate?.text    =  dateFormatter.string(from: date!) + " at " + updatedDate.getTimeIn12HourFormat()
                }
                else if dateFormatter.string(from: date!).count != 0  {
                    cell.lblDate?.text    =  dateFormatter.string(from: date!)
                }
                else {
                    cell.lblDate?.text    =  nil
                }
            }
            
            cell.imgViewClock?.image = (isRead == true ? R.image.clock() : R.image.clock_grey())

            return cell
        }
        else
        {
            let cellPDF =  tableView.dequeueReusableCell(withIdentifier: "cellCircularPDF", for: indexPath as IndexPath) as! CustomCircularDetailsPDFTableViewCell
            
            strDouble =  self.dictCircularDetails["FileSize"] as! Double
            
            let roundedValue2 = strDouble.roundToDecimal(2)
            print(roundedValue2)
            
            if roundedValue2 == 0.0 {
                strSize = "\(strDouble.rounded()) kb"
            }
            else {
                strSize = "\(roundedValue2) mb"
            }
           // if roundedValue2 == 0.0
           // {
              //  cellPDF.btnView?.isUserInteractionEnabled = false
           // }
           
            cellPDF.lblPDFSize?.text = strSize
            cellPDF.btnView?.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
            return cellPDF
        }
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton)
    {
        print("ClkBtnView")
        if let strFilePath1        = self.dictCircularDetails["FilePath"] as? String
        {
            strFilePath = strFilePath1
            strPrefix   = strPrefix.imgPath1()
            finalUrl    = "\(strPrefix)" +  "\(strFilePath)"
            urlPDF      = URL(string: finalUrl)
            print("urlPDF= \(String(describing: urlPDF))")
            self.performSegue(withIdentifier: "segueCircularWebview", sender: self)
        }
        else
        {
            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.fileIsNotCorrect(), fromVC: self)
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblCircularDetails?.deselectRow(at: indexPath, animated: true)
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueCircularWebview"
        {
            let webviewVC                = segue.destination as! WebviewCircularViewController
            webviewVC.strfinalUrl        = finalUrl
            webviewVC.intClassDistingues = 1
        }
    }
    
}


