import UIKit

protocol HideBarDelegateCircular: class{
    
    func hideBar(boolHide: Bool)
}

class CircularViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var tblCircular:   UITableView?
    
    @IBOutlet weak var imgViewNoCircular: UIImageView?
    @IBOutlet weak var lblNoCircular: UILabel?
    
    var boolRead: Bool?
    
    var arrayCircular  = [[String: Any]]()
    var strSender      = String()
    var intUserID      = Int()
    var intUserType    = Int()
    var intPage        = 1
    var intCirID1      = Int()
    var spinner        : UIActivityIndicatorView?
    var refreshControl = UIRefreshControl()
    var btnTitle       = UIButton()
    var totalCircular = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "All Circulars"
        
        spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner?.color        =  UIColor(rgb: 0x56B54B)
        spinner?.stopAnimating()
        spinner?.hidesWhenStopped = true
        
        spinner?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        tblCircular?.rowHeight          = UITableView.automaticDimension
        tblCircular?.estimatedRowHeight = 2000
        tblCircular?.tableFooterView    = UIView(frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tblCircular?.addSubview(refreshControl)
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 170, height: 40)
        btnTitle.setTitle("All Circulars", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        
        self.navigationItem.titleView = btnTitle
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.tblCircular?.setContentOffset(CGPoint(x:0,y:10), animated: true)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        navigationItem.hidesBackButton = true
        
        self.imgViewNoCircular?.isHidden = true
        self.lblNoCircular?.isHidden     = true
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        self.webAPICircular(intPagination: 1)
    }
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        
        // print("refresh pulled")
        intPage     = 1
        self.webAPICircular(intPagination: 1)
    }
    //MARK:- webAPICircular
    func webAPICircular(intPagination: Int)
    {
        //print("webAPICircular")
        tblCircular?.isHidden    = true
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Circular/All?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"
        
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        // print("*** result = \(result)")
                        
                        
                        
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            
                            if let totalCircular = result["TotalCirculer"] as? Int {
                                self.totalCircular = totalCircular
                            }
                            
                            self.arrayCircular = results?["Circulars"] as! [[String : Any]]
                            //print("arrayCircular count = \(self.arrayCircular.count)")
                            
                            if self.arrayCircular.count == 0
                            {
                                self.tblCircular?.isHidden       = true
                                self.imgViewNoCircular?.isHidden = false
                                self.lblNoCircular?.isHidden     = false
                            }
                            else
                            {
                                var intTotalNotice  = Int()
                                var intUnreadNotice = Int()
                                
                                intTotalNotice      = results?["TotalCirculer"] as! Int
                                intUnreadNotice     = results?["UnreadCirculer"] as! Int
                                self.title = "All Circulars (\("\(intUnreadNotice)/\(intTotalNotice)"))"
                                self.btnTitle.setTitle(self.title,for: .normal)
                                
                                self.tblCircular?.isHidden   = false
                                self.tblCircular?.delegate   = self
                                self.tblCircular?.dataSource = self
                                self.tblCircular?.reloadData()
                                
                                self.refreshControl.endRefreshing()
                                
                            }
                            self.spinner?.stopAnimating()
                        }
                        else{
                            self.lblNoCircular?.isHidden     = true
                            self.lblNoCircular?.text         = "Something went wrong"
                        }
                    }
                    if (error != nil){
                        Utility.shared.removeLoader()
                        
                        //print("userVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                self.tblCircular?.isHidden = false
                                
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    Utility.shared.internetConnectionAlert()
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            if self.arrayCircular.count == self.totalCircular {
                print("All data already loaded")
                return
            }
            
            print("fetch more data -- scrollViewDidEndDragging")
            spinner?.startAnimating()
            
            intPage = intPage + 1
            //self.webAPIQuestions(intAll: intAllMyCount, intPage: intCount)
            self.loadChunkDataAPICircular(intPagination: intPage)
        }
    }
    
    //MARK:- loadChunkDataAPICircular
    func loadChunkDataAPICircular(intPagination: Int)
    {
        //print("loadChunkDataAPICircular")
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Circular/All?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"
        
        print("Func:loadingPastData urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        self.spinner?.stopAnimating()
                        
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            let tempArray = results?["Circulars"] as! [[String : Any]]
                            if tempArray.count == 0
                            {
                                //                                let alert = UIAlertController(title: "", message: "No more Circular", preferredStyle: UIAlertControllerStyle.alert)
                                //                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                //                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                var intTotalNotice  = Int()
                                var intUnreadNotice = Int()
                                
                                intTotalNotice      = results?["TotalCirculer"] as! Int
                                intUnreadNotice     = results?["UnreadCirculer"] as! Int
                                self.title = "All Circulars (\("\(intUnreadNotice)/\(intTotalNotice)"))"
                                
                                self.btnTitle.setTitle(self.title,for: .normal)
                                
                                //print("Before arrayCircular count = \(self.arrayCircular.count)")
                                self.arrayCircular.append(contentsOf: results?["Circulars"] as! [[String : Any]])
                                //print("after arrayCircular count = \(self.arrayCircular.count)")
                                self.tblCircular?.reloadData()
                            }
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async
                            {
                                self.spinner?.stopAnimating()
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayCircular.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellCircular", for: indexPath as IndexPath) as! CustomCircularTableViewCell
        
        cell.lblHeading?.text = self.arrayCircular[indexPath.row]["Title"] as? String
        
        // cell.lblDate.text    = self.arrayCircular[indexPath.row]["UpdatedOn"] as? String
        
        var intCheckNew          = Int()
        var intColorCell         = Int()
        var intHasAttachment     = Int()
        var strDetail            = String()
        var strDate              = String()
        strDate                 = self.arrayCircular[indexPath.row]["UpdatedOn"] as! String
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: strDate)
        strDate                 = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        
        if let updatedDate =  self.arrayCircular[indexPath.row]["UpdatedOn"] as? String {
            if dateFormatter.string(from: date!).count != 0  && updatedDate.getTimeIn12HourFormat().count != 0  {
                cell.lblDate?.text    =  dateFormatter.string(from: date!) + " at " + updatedDate.getTimeIn12HourFormat()
            }
            else if dateFormatter.string(from: date!).count != 0  {
                cell.lblDate?.text    =  dateFormatter.string(from: date!)
            }
            else {
                cell.lblDate?.text    =  nil
            }
        }
        strDetail = self.arrayCircular[indexPath.row]["Message"] as! String
        
        //        if strDetail.count == 0{
        //            cell.widthImgViewTxt?.constant = 0
        //        }
        //        else{
        //            cell.widthImgViewTxt?.constant = 0
        //        }
        
        intCheckNew      = self.arrayCircular[indexPath.row]["isNew"]         as! Int
        intColorCell     = self.arrayCircular[indexPath.row]["isRead"]        as! Int
        intHasAttachment = self.arrayCircular[indexPath.row]["hasAttachment"] as! Int
        
        switch intCheckNew {
        case 0:
            cell.lblNew?.isHidden = true
        case 1:
            cell.lblNew?.isHidden = false
        default:
            break
        }
        
        switch intColorCell {
        case 0:
            cell.backgroundColor = UIColor(rgb: 0xEEEEEE)
            cell.imgViewClock?.image = R.image.clock()
        case 1:
            cell.backgroundColor = UIColor.white
            cell.imgViewClock?.image = R.image.clock_grey()
        default:
            break
        }
        switch intHasAttachment {
        case 0:
            cell.widthImgViewPDF?.constant = 0
            cell.imgViewPDF?.isHidden = true
        case 1:
            cell.widthImgViewPDF?.constant = 18
            cell.imgViewPDF?.isHidden = false
        default:
            break
        }
        boolRead = intColorCell == 1 ? true : false
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblCircular?.deselectRow(at: indexPath, animated: true)
        intCirID1 = self.arrayCircular[indexPath.row]["CirID"] as! Int
        performSegue(withIdentifier: "SegueCircularDetails", sender: self)
    }
    
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SegueCircularDetails"
        {
            let circularDetails                = segue.destination as! CircularDetailsViewController
            circularDetails.intCirID            = intCirID1
            circularDetails.strSenderDetails   = strSender
            circularDetails.intUserIDDetails   = intUserID
            circularDetails.intUserTypeDetails = intUserType
            circularDetails.isRead             =  boolRead
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        // SegueNoticDetails
        
    }
}


