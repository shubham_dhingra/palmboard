//
//  CustomCircularDetailsPDFTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomCircularDetailsPDFTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPDFSize: UILabel?
    @IBOutlet weak var btnView: UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

