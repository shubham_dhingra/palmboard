//
//  CustomCircularDetailsTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomCircularDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblHeading: UILabel?
    @IBOutlet weak var lblDate: UILabel?
    @IBOutlet weak var lblDetails: UILabel?
    @IBOutlet weak var txtViewDetails: UITextView?
    @IBOutlet weak var imgViewClock: UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}


