//
//  CustomCircularTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomCircularTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblHeading: UILabel?
    // @IBOutlet weak var imgViewNew: UIImageView!
    @IBOutlet weak var lblNew: UILabel?
    
    @IBOutlet weak var lblDate:      UILabel?
    @IBOutlet weak var imgViewPDF:   UIImageView?
    @IBOutlet weak var imgViewText:  UIImageView?
    @IBOutlet weak var imgViewClock: UIImageView?

    @IBOutlet weak var widthImgViewPDF: NSLayoutConstraint?
    @IBOutlet weak var widthImgViewTxt: NSLayoutConstraint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}


