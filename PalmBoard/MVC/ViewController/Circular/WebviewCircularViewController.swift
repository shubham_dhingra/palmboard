//  WebviewCircularViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class WebviewCircularViewController : UIViewController,UIWebViewDelegate
{
    @IBOutlet var webView: UIWebView?
    @IBOutlet var txtAssignmentDTL: UITextView?
    @IBOutlet weak var txtAssignmentHeightConst: NSLayoutConstraint!
    
    var strfinalUrl        =  String()
    var intClassDistingues =  Int()
    var btnTitle           =  UIButton()
    var moduleName : String?
    var strAssDTL: String?
    var urlPath: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)

        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
       // btnTitle.setTitle("Circular", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        webView?.scalesPageToFit = true
        //  btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        if strfinalUrl.count > 0{
            strfinalUrl    =  "\(strfinalUrl)"
             print("strfinalUrl= \(strfinalUrl)")
        }
    }
    
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        txtAssignmentDTL?.text     = strAssDTL
        if strAssDTL?.trimmed().count == 0 {
            txtAssignmentHeightConst?.constant = 0.0
        }
        txtAssignmentDTL?.isEditable = false
        ez.runThisInMainThread {
             self.updateTextView()
        }
       
        
       // lblAssignmentDTL?.isHidden = true
        if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
        
       btnTitle.setTitle(intClassDistingues == 2 ? "Assignment" : "Circular", for: UIControl.State.normal)
        webView?.delegate        = self
        webView?.backgroundColor = UIColor.clear
        if strfinalUrl.trimmed().count > 0 {
            if intClassDistingues == 2 {
                 strfinalUrl = APIConstants.imgbasePath + "\(strfinalUrl)"
            }
            else if intClassDistingues == 1 {
                let urlPDF        = URL(string: strfinalUrl)
                print("Circular urlPDF = \(/urlPDF?.absoluteString)")
            }
        }
       
        if let url = URL(string: self.strfinalUrl){
            let request = URLRequest(url: url)
            webView?.loadRequest(request)
        }
//        else {
//            AlertsClass.shared.showNativeAlert(withTitle: AlertConstants.Attention.get, message: R.string.localize.notFoundFile(/btnTitle.currentTitle), fromVC: self)
//        }
   
    }

    
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
        savePdf(urlString: strfinalUrl, fileName: "circular")

    }
    
    
    func updateTextView() {
        
        if URL(string: strfinalUrl) != nil {
            if Double(txtAssignmentDTL?.contentSize.height ?? 0.0) > 150.0 {
                txtAssignmentDTL?.isScrollEnabled = true
                txtAssignmentHeightConst?.constant = 150.0
            }
            else{
                txtAssignmentDTL?.isScrollEnabled = false
                txtAssignmentHeightConst?.constant = txtAssignmentDTL?.contentSize.height ??  0.0
            }
        }
        else {
            webView?.isHidden = true
            txtAssignmentHeightConst?.constant = (txtAssignmentDTL?.contentSize.height ?? 0.0) + (webView?.frame.size.height  ?? 0.0)
        }
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        Utility.shared.removeLoader()
        AlertsClass.shared.showNativeAlert(withTitle: "", message: "Can't Connect. Please check your internet Connection", fromVC: self)
    }
    // MARK: - savePdf
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "Circular-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                self.urlPath = actualPath
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
    // MARK: - sharePdf
    func sharePdf(path:URL) {
        
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: path.path) {
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            print("document was not found")
            let alertController = UIAlertController(title: "Error", message: "Document was not found!", preferredStyle: .alert)
            let defaultAction = UIAlertAction.init(title: "ok", style: UIAlertAction.Style.default, handler: nil)
            alertController.addAction(defaultAction)
            //  UIViewController.hk_currentViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    // MARK: - ClkShare
    @IBAction func ClkShare(_ sender: Any) {
        sharePdf(path: urlPath!)
    }
}
