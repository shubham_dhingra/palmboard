//
//  ClassmateTeacherCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class ClassmateTeacherCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnImage: UIButton?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblSubject: UILabel?
    @IBOutlet weak var btnDiscuss: UIButton?
    @IBOutlet weak var lblSubjects: UILabel?
    @IBOutlet weak var lblSubjectHeight: NSLayoutConstraint?
}
