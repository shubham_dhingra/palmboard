
import UIKit
protocol HideBarDelegate2: class{
    
    func hideBar(boolHide: Bool)
}
class ClassmatesTeachersViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var segmentControl: UISegmentedControl?
    @IBOutlet weak var segmentBgView: UIView?
    @IBOutlet weak var segmentHeight: NSLayoutConstraint?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var btnTitle: UIButton?
    @IBOutlet weak var btnClassMates: UIBarButtonItem!
    
    @IBOutlet weak var imgViewNoClassmateOrTeacher: UIImageView?
    @IBOutlet weak var lblNoClassmateOrTeacher: UILabel?
    
    var classId             = Int()
    var userId              = Int()
    var strSchoolCodeSender = String()
    var userType            = Int()
    var intListChoice       = Int()
    var arrayDataList       = [[String: Any]]()
    
    var otherUserId      = Int()
    var otherUserType    = Int()
    var otherUserPhoto   = String()
    var otherUserSubject = String()
    var urlString        = String()
    var themeColor       = String()
    var subModule       : [[String : Any]]?                  // 0 means both are active
    var action : Action?
    var heading : String?
    var users : Users?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        if users != .Cordinator {
            self.navigationItem.rightBarButtonItem = nil
            imgViewNoClassmateOrTeacher?.image = R.image.noTeachers()
            lblNoClassmateOrTeacher?.text      = "No Teachers Available"
            if (self.intListChoice == 0 && (subModule == nil || subModule?.count == 2 || subModule?.count == 0)){
                btnTitle?.setTitle(heading ?? "Teachers", for: .normal)
            }
            else if (self.intListChoice == 0 || subModule?.count == 1) {
                if let subModuleActiveName = subModule?[0]["SubModule"] as? String {
                    btnTitle?.setTitle(subModuleActiveName, for: .normal)
                }
                else {
                    btnTitle?.setTitle("Teachers", for: .normal)
                }
                if let moduleId = subModule?[0]["S_MdlID"] as? Int {
                    self.segmentControl?.selectedSegmentIndex = moduleId == 9 ? 1 : 0
                }
            }
            else{
                btnTitle?.setTitle("Classmates", for: .normal)
                imgViewNoClassmateOrTeacher?.image = R.image.noClassmate()
                lblNoClassmateOrTeacher?.text      = "No Classmates Available"
                self.segmentBgView?.isHidden  = true
                self.segmentControl?.isHidden = true
                segmentHeight?.constant       = 0
            }
        }
        else{
            imgViewNoClassmateOrTeacher?.image = R.image.no_weekly_plan()
            lblNoClassmateOrTeacher?.text      = "No Weekly Plan"
            self.navigationItem.prompt = "Weekly Plan"
            btnTitle?.setTitle("(Teacher wise)", for: .normal)
        }
        segmentControl?.isHidden = (users == .Cordinator || subModule?.count == 1)
        if subModule?.count == 1 {
            segmentHeight?.constant = 0
        }
        imgViewNoClassmateOrTeacher?.isHidden = true
        lblNoClassmateOrTeacher?.isHidden     = true
    }
    @IBAction func btnClassMatesAct(_ sender: UIBarButtonItem) {
        let storyboard = Storyboard.Module.getBoard()
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "ClasswiseWeeklyPlanViewController") as? ClasswiseWeeklyPlanViewController {
            self.navigationController?.pushViewController(selectClassVc, animated: false)
        }
    }
    // MARK: -  viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.collectionView?.isHidden = true
        navigationItem.hidesBackButton = true
        
        themeColor =  /self.getCurrentSchool()?.themColor
        segmentControl?.backgroundColor = themeColor.hexStringToUIColor()
        segmentBgView?.backgroundColor  = themeColor.hexStringToUIColor()
        segmentControl?.selectedSegmentIndex = 0
        arrayDataList.removeAll()
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let userId   = self.getCurrentUser()?.userID {self.userId =  Int(userId)}
        if let userType = self.getCurrentUser()?.userType{self.userType =  Int(userType)}
        if let classID  = self.getCurrentUser()?.classID{classId = Int(classID)}
        
        if users != .Cordinator {
            if(intListChoice == 0){
                self.navigationItem.title = "Teachers"
                //urlString                 = "http://webapi.palmboard.in/api/user/Teachers?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(classId)"
                urlString                 = APIConstants.basePath + "user/Teachers?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(classId)"
            }else
            {
                self.navigationItem.title    = "Classmates"
                segmentHeight?.constant       = 0
                self.segmentControl?.isHidden = true
                //  urlString = "http://webapi.palmboard.in/api/user/Classmates?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(classId)&UserID=\(userId)"
                urlString = APIConstants.basePath + "user/Classmates?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(classId)&UserID=\(userId)"
            }
        }
        else {
            segmentHeight?.constant       = 0
            self.segmentControl?.isHidden = true
            urlString = APIConstants.basePath + "Teacher/StaffList?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(userId)"
        }
        print("urlString \n \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("List result \n \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        if(self.intListChoice == 0){
                            
                            if self.users == .Cordinator {
                                if let allTeacher = (result["AllTeacher"] as? [[String: Any]]){
                                    self.arrayDataList = allTeacher
                                    self.collectionView?.isHidden = false
                                }
                                else{
                                    self.imgViewNoClassmateOrTeacher?.isHidden = false
                                    self.lblNoClassmateOrTeacher?.isHidden     = false
                                    self.collectionView?.isHidden = true
                                }
                            }
                            else {
                                
                                if self.segmentControl?.selectedSegmentIndex == 0 {
                                    if let allTeacher = (result["SubTeacher"] as? [[String: Any]]){
                                        self.arrayDataList = allTeacher
                                        self.collectionView?.isHidden = false
                                    }
                                    else{
                                        self.arrayDataList =  []
                                        self.imgViewNoClassmateOrTeacher?.isHidden = false
                                        self.lblNoClassmateOrTeacher?.isHidden     = false
                                        self.collectionView?.isHidden = true
                                    }
                                }
                                else {
                                    if let allTeacher = (result["AllTeacher"] as? [[String: Any]]){
                                        self.arrayDataList = allTeacher
                                        self.collectionView?.isHidden = false
                                    }
                                    else{
                                        self.arrayDataList =  []
                                        self.imgViewNoClassmateOrTeacher?.isHidden = false
                                        self.lblNoClassmateOrTeacher?.isHidden     = false
                                        self.collectionView?.isHidden = true
                                    }
                                }
                               }
                        } else{
                            
                            if let classmates = (result["Classmates"] as? [[String: Any]]){
                                self.arrayDataList = classmates
                                self.collectionView?.isHidden = false
                                
                            }
                            else{
                                self.imgViewNoClassmateOrTeacher?.isHidden = false
                                self.lblNoClassmateOrTeacher?.isHidden     = false
                                self.collectionView?.isHidden = true
                            }
                        }
                        //print("self.arrayDataList \n \(self.arrayDataList)")
                        self.collectionView?.reloadData()
                    }
                }
                else{
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        self.lblNoClassmateOrTeacher?.text     = "Something went wrong."
                        self.lblNoClassmateOrTeacher?.isHidden = false
                        self.lblNoClassmateOrTeacher?.center   = self.view.center
                        self.segmentControl?.isUserInteractionEnabled = true
                        self.collectionView?.isHidden = true
                    }
                }
            }
        }
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    override func viewDidLayoutSubviews() {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    @IBAction func titlePressed(_ sender: UIButton) {
        self.collectionView?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: -  segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl)
    {
        imgViewNoClassmateOrTeacher?.isHidden = true
        lblNoClassmateOrTeacher?.isHidden     = true
        
        self.arrayDataList.removeAll()
        var urlString = String()
        
        self.segmentControl?.isUserInteractionEnabled = false
        // collectionView.isHidden = true
        
        urlString                 = APIConstants.basePath + "user/Teachers?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(classId)"
        print("urlString \n \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString)//\(classId)
        { (results, error, errorCode) in
            
            
            if let result = results{
                
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("List result \n \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        self.segmentControl?.isUserInteractionEnabled = true
                        //self.collectionView.isHidden = false
                        
                        if(self.intListChoice == 0){
                            
                            if(sender.selectedSegmentIndex == 1){
                                if let allTeacher = (result["AllTeacher"] as? [[String: Any]]){
                                    self.arrayDataList = allTeacher
                                }
                            }else{
                                if let subTeacher = (result["SubTeacher"] as? [[String: Any]]){
                                    self.arrayDataList = subTeacher
                                }
                            }
                            self.collectionView?.setContentOffset(CGPoint(x:0,y:0), animated: true)
                            self.collectionView?.reloadData()
                            self.collectionView?.isHidden = self.arrayDataList.count == 0
                            self.imgViewNoClassmateOrTeacher?.isHidden = !(/self.collectionView?.isHidden)
                            self.lblNoClassmateOrTeacher?.isHidden = !(/self.collectionView?.isHidden)
                        }
                    }
                }
                else{
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        self.lblNoClassmateOrTeacher?.text     = "Something went wrong."
                        self.lblNoClassmateOrTeacher?.isHidden = false
                        self.lblNoClassmateOrTeacher?.center   = self.view.center
                        self.segmentControl?.isUserInteractionEnabled = true
                        self.collectionView?.isHidden = true
                        
                    }
                }
            }
        }
    }
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayDataList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // print("self.arrayDataList = \(self.arrayDataList)")
        if intListChoice == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as! ClassmateTeacherCollectionViewCell
            cell.btnImage?.setBackgroundImage(R.image.noProfile_Big(), for: .normal)
            cell.btnImage?.layer.cornerRadius = (cell.btnImage?.frame.width)! / 2
            cell.btnImage?.layer.masksToBounds = true
            cell.btnImage?.tag = indexPath.row
            
            cell.btnDiscuss?.tag = indexPath.row
            cell.btnDiscuss?.backgroundColor = themeColor.hexStringToUIColor()
            cell.btnDiscuss?.layer.cornerRadius = 5
            
            if users == .Cordinator {
                cell.btnImage?.isUserInteractionEnabled = false
            }
            
            if(self.arrayDataList.count == 0){}
            else{
                if let imgUrl = self.arrayDataList[indexPath.row]["Photo"] as? String
                {
                    var strPrefix      = String()
                    strPrefix          += strPrefix.imgPath()
                    var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                    finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url            = URL(string: finalUrlString )
                    // print("imgUrl \n \(url!)")
                    let imgViewUser = UIImageView()
                    imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                        
                        let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                        cell.btnImage?.setBackgroundImage(img1, for: .normal)
                    }
                }
                if let name = self.arrayDataList[indexPath.row]["Name"] as? String{
                    cell.lblName?.text = name
                }
                
                if let subject = self.arrayDataList[indexPath.row]["Subject"] as? String{
                    cell.lblSubjects?.text = "(\(subject))"
                }
                
                if(intListChoice == 0 && self.segmentControl?.selectedSegmentIndex == 0){
                    // cell.lblSubjectHeight.constant = 40
                    if let subject = self.arrayDataList[indexPath.row]["Subject"] as? String{
                        cell.lblSubject?.text = "(\(subject))"
                    }
                }
                else {
                    // cell.lblSubjectHeight.constant = 0
                    cell.lblSubject?.frame.size.height = 0
                    cell.lblSubject?.frame             = CGRect(x: 0 ,y: 0, width: 0, height: 0)
                }
                cell.lblSubjects?.isHidden = segmentControl?.selectedSegmentIndex == 1
            }
            return cell
            
        }
        else  {
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnlyClasslistlistCell", for: indexPath) as! OnlyClassmateCollectionViewCell
            
            cell.btnImage?.setBackgroundImage(R.image.noProfile_Big(), for: .normal)
            cell.btnImage?.layer.cornerRadius = (cell.btnImage?.frame.width)! / 2
            cell.btnImage?.layer.masksToBounds = true
            cell.btnImage?.tag = indexPath.row
            
            cell.btnDiscuss?.tag = indexPath.row
            cell.btnDiscuss?.backgroundColor = themeColor.hexStringToUIColor()
            cell.btnDiscuss?.layer.cornerRadius = 5
            
            if users == .Cordinator {
                cell.btnImage?.isUserInteractionEnabled = false
            }
            
            if(self.arrayDataList.count == 0){}
            else{
                if let imgUrl = self.arrayDataList[indexPath.row]["Photo"] as? String
                {
                    var strPrefix      = String()
                    strPrefix          += strPrefix.imgPath()
                    var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                    finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                    let url            = URL(string: finalUrlString )
                    // print("imgUrl \n \(url!)")
                    let imgViewUser = UIImageView()
                    imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                        
                        let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                        cell.btnImage?.setBackgroundImage(img1, for: .normal)
                    }
                }
                if let name = self.arrayDataList[indexPath.row]["Name"] as? String{
                    cell.lblName?.text = name
                }
                
            }
            return cell
        }
        //return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if users == .Cordinator {
            if let userId1 = self.arrayDataList[indexPath.row]["UserID"] as? Int
            {
                self.otherUserId      = userId1
                
            }else{
                self.otherUserId      = 0
            }
            let moduleStoryboard = Storyboard.Module.getBoard()
            if let vc = moduleStoryboard.instantiateViewController(withIdentifier: "PlanListViewController") as? PlanListViewController {
                vc.userRole = users
                vc.teacherID = self.otherUserId.toString
                vc.viewType = 1
                vc.action = .Review
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(intListChoice == 0){
            return CGSize(width: self.view.frame.width/2, height: 219)//219
        }else{
            return CGSize(width: self.view.frame.width/2, height: 186)//
        }
    }
    // MARK: -  imagePressed
    @IBAction func imagePressed(_ sender: UIButton) {
        
        let row      = sender.tag
        // let indexPath = IndexPath(row: row, section: 0)
        
        print("row--------\(row)")
        if let userId1 = self.arrayDataList[row]["UserID"] as? Int
        {
            self.otherUserId      = userId1
            
        }else{
            self.otherUserId      = 0
        }
        
        if let userType1 = self.arrayDataList[row]["UserType"] as? Int
        {
            self.otherUserType    = userType1
            
        }else{
            self.otherUserType    = 0
        }
        if let otherUserSubject1 = self.arrayDataList[row]["Subject"] as? String
        {
            // print("otherUserSubject1 = \(otherUserSubject1)")
            self.otherUserSubject = otherUserSubject1
            
        }else{
            self.otherUserSubject = ""
        }
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        //self.performSegue(withIdentifier: "showProfileTeacher", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID         = self.otherUserId
        shortProfileVc.intUserType       = self.otherUserType
        shortProfileVc.strteacherSubject = self.otherUserSubject
        self.pushVC(shortProfileVc)
        
    }
    @IBAction func imagePressedClassMate(_ sender: UIButton) {
        
        let row      = sender.tag
        // let indexPath = IndexPath(row: row, section: 0)
        //print("row--------\(row)")
        if let userId1 = self.arrayDataList[row]["UserID"] as? Int
        {
            self.otherUserId      = userId1
            
        }else{
            self.otherUserId      = 0
        }
        
        if let userType1 = self.arrayDataList[row]["UserType"] as? Int
        {
            self.otherUserType    = userType1
            
        }else{
            self.otherUserType    = 0
        }
        if let otherUserSubject1 = self.arrayDataList[row]["Subject"] as? String
        {
            // print("otherUserSubject1 = \(otherUserSubject1)")
            self.otherUserSubject = otherUserSubject1
            
        }else{
            self.otherUserSubject = ""
        }
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        //  self.performSegue(withIdentifier: "showProfileTeacher", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID         = self.otherUserId
        shortProfileVc.intUserType       = self.otherUserType
        shortProfileVc.strteacherSubject = self.otherUserSubject
        self.pushVC(shortProfileVc)
        
    }
    
    // MARK: -  discussPressed
    @IBAction func discussPressed(_ sender: UIButton) {
        
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: 0)
        
        if let userId1 = self.arrayDataList[indexPath.row]["UserID"] as? Int
        {
            self.otherUserId    = userId1
            if let userType1 = self.arrayDataList[indexPath.row]["UserType"] as? Int
            {
                self.otherUserType  = userType1
                if let photoUrl1 = self.arrayDataList[indexPath.row]["Photo"] as? String
                {
                    self.otherUserPhoto = photoUrl1
                }
            }
        }
        //  print("\(self.otherUserId)\n \(self.otherUserType)\n \(self.otherUserPhoto)")
    }
    @IBAction func discussPressedClassMate(_ sender: UIButton) {
        
        let row = sender.tag
        let indexPath = IndexPath(row: row, section: 0)
        
        if let userId1 = self.arrayDataList[indexPath.row]["UserID"] as? Int
        {
            self.otherUserId    = userId1
            if let userType1 = self.arrayDataList[indexPath.row]["UserType"] as? Int
            {
                self.otherUserType  = userType1
                if let photoUrl1 = self.arrayDataList[indexPath.row]["Photo"] as? String
                {
                    self.otherUserPhoto = photoUrl1
                }
            }
        }
        //  print("\(self.otherUserId)\n \(self.otherUserType)\n \(self.otherUserPhoto)")
    }
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
class OnlyClassmateCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnImage:   UIButton?
    @IBOutlet weak var lblName:    UILabel?
    @IBOutlet weak var btnDiscuss: UIButton?
}
