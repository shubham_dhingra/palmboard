//
//  BaseViewController.swift
//  ComposeMessageModule
//
//  Created by NupurMac on 16/05/18.
//  Copyright © 2018 ShubhamMac. All rights reserved.
//

import UIKit
import EZSwiftExtensions
import Foundation
import EZSwiftExtensions

typealias successBlock = (_ result: AnyObject? ) -> ()
class BaseViewController: UIViewController {
    
    //MARK::- OUTLETS
    // @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var collectionView : UICollectionView?
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet weak var btnContact1 : UIButton?
    @IBOutlet weak var btnContact2 : UIButton?
    @IBOutlet weak var btnOthers : UIButton?
    @IBOutlet weak var stackView : UIStackView?
    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var lblError : UILabel?
    @IBOutlet weak var lblNoData : UILabel?
    @IBOutlet weak var imgNoData : UIImageView?
    @IBOutlet weak var topHeaderConstraint: NSLayoutConstraint?

    //MARK::- VARIABLES
    var userType : String?
    var userID : String?
    var selectSchool : SchoolCodeTable?
    var sender : String?
    
    var tableDataSource : TableViewDataSource?{
        didSet{
            
            tableView?.dataSource = tableDataSource
            tableView?.delegate = tableDataSource
            tableView?.reloadData()
        }
    }
    
    var collectionDataSource: CollectionViewDataSource?{
        didSet{
            collectionView?.dataSource = collectionDataSource
            collectionView?.delegate = collectionDataSource
            collectionView?.reloadData()
        }
    }
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetails()
    }
    
    
    
    func getDetails() {
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            sender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.userID =  String(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.userType =  String(userType)
        }
        
    }
    
    
    func setNavigation(vc : UIViewController? , title : String?){
        vc?.title = title
        navigationItem.hidesBackButton        = true
        vc?.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func setUpForIphoneX() {
        
        if ez.topMostVC is HelpViewController || ez.topMostVC is SearchLeaveViewController || ez.topMostVC?.children.last?.children.last is EnterCodeViewController || ez.topMostVC is  LikesAllUserViewController || ez.topMostVC?.children.last?.children.last is LikesAllUserViewController || ez.topMostVC?.children.last?.children.last is ViewController {
            topHeaderConstraint?.constant = isiPhoneX ? 20.0 : 0.0
        }
        else if ez.topMostVC is FindSchoolCodeViewController ||  ez.topMostVC is RecipientListViewController  {
            topHeaderConstraint?.constant = isiPhoneX ? 24.0 : 0.0
        }
        else if  ez.topMostVC is ViewController {
             topHeaderConstraint?.constant = isiPhoneX ? 20.0 : 0.0
        }
        else {
           topHeaderConstraint?.constant = isiPhoneX ? 36.0 : 20.0
        }
    }
   
    func checkNoData(arr : [Any]?) {
        imgNoData?.isHidden =  arr?.count != 0 || arr == nil
        lblNoData?.isHidden = arr?.count != 0  || arr == nil
    }
    
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnBackAct(_ sender: UIButton) {
        self.navigationController == nil ? dismissVC(completion: nil) : popVC()
    }
    
    
    @IBAction func btnHelpAct(_ sender : UIButton){
        openHelpVc()
    }
    
    func openHelpVc() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let helpController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController else {return}
        guard let lastVc = self.navigationController?.viewControllers.last else {return }
        if lastVc is AddUserViewController || lastVc is AddPasswordViewController {
            if let currentSchool = self.selectSchool , let schName = currentSchool.schName{
                helpController.strEmail   = /currentSchool.supportEmail
                helpController.strPhoneNo = /currentSchool.supportPhone
                helpController.strLogoURL = "\(APIConstants.imgbasePath)" +  "\(/currentSchool.schLogo)"
                helpController.strCity    = /currentSchool.schCity
                helpController.strSchoolName = schName
                self.presentVC(helpController)
            }
            else {
                openHelpVcForCurrentSchool()
            }
        }
        else {
            openHelpVcForCurrentSchool()
        }
    }
    
    func openHelpVcForCurrentSchool(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let helpController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController else {return}
        
        if let currentSchool = self.getCurrentSchool() , let schName = currentSchool.schName{
            helpController.strEmail   = /currentSchool.supportEmail
            helpController.strPhoneNo = /currentSchool.supportPhone
            helpController.strLogoURL = "\(APIConstants.imgbasePath)" +  "\(/currentSchool.schLogo)"
            helpController.strCity    = /currentSchool.schCity
            helpController.strSchoolName = schName
            presentVC(helpController)
        }
    }
    
    //MARK::- HANDLE API RESPONSE
    func handleResponse(isErrorShown : Bool? = true , response : Response, responseBack : successBlock) {
        switch response{
            
        case .success(let responseValue):
            responseBack(responseValue)
            
        case .failure(_):
            if /isErrorShown == false{
                Messages.shared.show(alert: .oops, message: "Something went wrong", type: .warning)
            }
        }
    }
    
    //custom function to hide navigation bar
    func hideNavigationBar(controlller : UIViewController){
        
        if topMostVC is LeavesReportViewController {
            controlller.isNavBarHidden = true
        }
        else {
            controlller.isNavBarHidden = false
        }
    }
    
    var topMostVC : UIViewController?{
        return (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers.last
    }
    
    
    private var isiPhoneX: Bool {
        return (UIScreen.main.nativeBounds.size.height == 2436) ||  (UIScreen.main.nativeBounds.size.height == 2688) || (UIScreen.main.nativeBounds.size.height == 1792)
    }
    
}

//MARK::- Update Contact List Methods
extension BaseViewController {
    
    func animated(){
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}




//MARK::- Send text Message API
extension BaseViewController {
    
    func sendMessage(subjectLine : String? , body : String? , msgType : String? , byteArray : String? ,selectUsersArr : [Any]?,  recipientType : String? , MessageType : MessageType , responseBack: @escaping successBlock) {
        var recipentArr = [NSMutableDictionary]()
        var classIDs = [String]()
        if selectUsersArr?.count == 0 {
            Messages.shared.show(alert: .oops, message: AlertMsg.atLeastOneRecipient.get, type: .warning, style: .bottom)
            return
        }
        
        print("***** Message send to :\(/selectUsersArr?.count) People *******")
        var classIDss : String?
        
        for (_,value) in (selectUsersArr ?? []).enumerated() {
            
            if let contacts = value as? Contacts{
                let dict = NSMutableDictionary()
                dict["ReceiverID"] = contacts.receiverID
                dict["ReceiverType"] = contacts.receiverType
                 dict["Mobile"] = contacts.mobile
                recipentArr.append(dict)
            }
            
            if let classs = value as? MyClasses {
                classIDs.append(/classs.classID?.toString)
            }
        }
        classIDss = classIDs.joined(separator: ",")
        print("No of classes select to send \(classIDs.count)")
        if let userId = self.getCurrentUser()?.userID {
            self.userID =  String(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.userType =  String(userType)
        }
        
        //        let recipentString = Utility.shared.converyToJSONString(array: recipentArr)
        
        if MessageType != .sms {
        APIManager.shared.request(with: HomeEndpoint.sendMessage(MsgType : msgType , UserID: userID , UserType: userType , Subject: subjectLine, Body: body, Device: "2", Attachment: byteArray, Recipient: recipentArr, ClassID: classIDss, RecipientType: /recipientType)) { (response) in
            
            self.handleResponse(response: response, responseBack: { (response) in
                responseBack(response)
            })
        }
        }
        else {
            var sms_prov_id : Int = 0
            if let prov_id = self.getCurrentSchool()?.sMS_Prvd_ID {
                sms_prov_id = Int(prov_id)
            }
            
            APIManager.shared.request(with: HomeEndpoint.AppSMS(UserID:  self.userID, UserType:  self.userType, SMSType: subjectLine, SMS: body, RecipientType: recipientType, Recipient: recipentArr, ClassID: classIDss, sMS_Prvd_ID: sms_prov_id)) { (response) in
                self.handleResponse(response: response, responseBack: { (response) in
                    responseBack(response)
                })
            }
        }
    }
    
    @IBAction func btnOpenSelectContact(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        let composeMsgVC = storyboard
            .instantiateViewController(withIdentifier: "ComposeDetailsViewController")
        
        (composeMsgVC as? ComposeDetailsViewController)?.isUdpateSelectList = true
        //        (composeMsgVC as? ComposeDetailsViewController)?.updateContactDelegate = self
        self.presentVC(composeMsgVC)
        
    }
}


//MARK::- Shake Text Field
extension BaseViewController {
    
    func shakeTextField(textField : Any? , errorMessage : String?) {
        (textField as? UITextField)?.textColor = UIColor.red
        (textField as? UITextField)?.layer.borderColor = UIColor.red.cgColor
        (textField as? UITextField)?.layer.borderWidth = 1.0
        (textField as? UITextField)?.txtShakeAnimation()
        lblError?.isHidden = /errorMessage?.count == 0
        lblError?.text = errorMessage
    }
    
    
    func openController(storyBoardName : String , Identifier : String) -> UIViewController {
        let storyboardRef = UIStoryboard(name: storyBoardName, bundle: nil)
        let vc = storyboardRef.instantiateViewController(withIdentifier: Identifier)
        return vc
    }
}

//MARK::- Varify Mobile No While Registering( in escort and appointment)
extension BaseViewController {
    
    //MARK::- Show Success Alert on register Escort
    func showAlertOnError(title : String , msg : String) {
        AlertsClass.shared.showAlertController(withTitle: title, message: msg, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
            return
        }
    }
}



