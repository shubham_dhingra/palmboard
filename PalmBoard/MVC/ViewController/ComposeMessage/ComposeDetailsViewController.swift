//  ComposeDetailsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 30/04/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import ObjectMapper
import Foundation
import EZSwiftExtensions
import Alamofire
import SJSegmentedScrollView

class ComposeDetailsViewController: BaseViewController , UIGestureRecognizerDelegate,  DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    //MARK ::- OUTLETS
    @IBOutlet weak var usertypeCollecViewHeight: NSLayoutConstraint?
    @IBOutlet weak var tblViewCompose: UITableView!
    @IBOutlet weak var btnCancel:      UIButton!
    @IBOutlet weak var btnOkay:        UIButton!
    @IBOutlet weak var tokenView: NWSTokenView!
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    
    let tokenViewMinHeight: CGFloat = 40.0
    let tokenViewMaxHeight: CGFloat = 150.0
    let tokenBackgroundColor = UIColor(red: 98.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    
    //MARK::- VARIABLES
    
    var selectedIndex : Int = 0
    var boolSectionCheck     = Bool()
   
    var strSchoolCodeSender  = String()
    var intUserType          = Int()
    var intUserID            = Int()
    var arrayContactCommon   : [Contacts]?
    var arrayModules         : [MyClasses]?
     var assignIndex : Int = 2
    var intTypeRadio         = Int()
    var intCellType          = Int()
    var wholeClassSelect : Bool = false
    var isUdpateSelectList  : Bool?
    var intLayer : Int = 1
    var selectUsersArr       = [Any]()
    var backUpContactArr  : [Contacts]?
    var backUpClassesArr   : [MyClasses]?
    var isSearching : Bool    = false
    var users : [Users] = [.Parent , .Student , .Staff]
    
    
    var btnReference = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let usertype = self.getCurrentUser()?.userType {
            self.intUserType =  Int(usertype)
        }
        self.isNavBarHidden = true
        tblViewCompose.rowHeight          = UITableView.automaticDimension
        tblViewCompose.estimatedRowHeight = 2000
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        usertypeCollecViewHeight?.constant = self.intUserType != 3 ? 0.0 : 49.0
        tokenView.layoutIfNeeded()
        tokenView.dataSource = self
        tokenView.delegate = self
        tokenView.reloadData()
        configureCollectionView()
     
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        intTypeRadio = selectedIndex + 2
       
        if intUserType != 3 {
            intTypeRadio = 3
        }
        
        intUserType == 3 ? self.webAPIContact(senderType: 1) : self.webAPIContact(senderType: 3)
       
        self.boolSectionCheck = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.isNavBarHidden = true
    }
    
    
    
    @IBAction func ClkBtnCancel(_ sender: Any) {
        
        if intCellType == 1 {
            
            //for class
            var tempArr = [MyClasses]()
            for value in (arrayModules ?? []){
                value.isSelect = false
                tempArr.append(value)
            }
            arrayModules = tempArr
            dismiss(animated: true, completion: nil)
        }
            
            //for (contact and staff contact)
        else {
            
            if intLayer == 2 {
                intCellType = 1
                tblViewCompose.reloadData()
                intLayer = 1
                btnCancel.setTitle(self.intLayer == 2  ? "Back" : "Cancel" , for: .normal)
            }
            else {
            var tempArr = [Contacts]()
            for value in (arrayContactCommon ?? []){
                value.isSelect = /isUdpateSelectList
                tempArr.append(value)
            }
                arrayContactCommon = tempArr
                backToHome()
            }
        }
    }
    
    
    @IBAction func ClkBtnOkay(_ sender: Any) {
         method_CheckCount() == 0 ? showAlert(title: /AlertConstants.Attention.get, desc : AlertMsg.atLeastOneRecipient.get) : backToHome()
    }
    
    func backToHome() {
        NotificationCenter.default.post(name: .RECIPEINTS_UPDATE, object: nil, userInfo : ["Recipients" :selectUsersArr , "Type" : assignIndex])
        dismiss(animated: true, completion: nil)
        
    }
    
    func getSelectContact() -> [Contacts]? {
        return (arrayContactCommon?.filter({$0.isSelect == true}))
    }
    
    
    func showAlert(title : String? , desc : String?) {
        AlertsClass.shared.showAlertController(withTitle: /title, message: /desc, buttonTitles: [AlertConstants.Ok.get]) { (value) in
            let type = value as AlertTag
            switch type {
            default:
                return
            }
        }
    }
}
    


//MARK::- *** NWSToken ***  Delegate and Datasource
extension ComposeDetailsViewController : NWSTokenDelegate , NWSTokenDataSource {
   
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
        
    }
    
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int {
        return selectUsersArr.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets? {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String?{
        return "To:"
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String? {
        return "Search "
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView? {
        
        if let items = self.selectUsersArr[index] as? MyClasses {
            var colorAssign : UIColor?
            
            if let color = items.colorAssign {
                colorAssign = color
            }
            else {
                colorAssign = UIColor.random()
                (self.selectUsersArr[index] as? MyClasses)?.colorAssign = colorAssign
            }
            
            if let token = NWSImageToken.initWithTitle(/items.ClassName, image: nil , /items.ClassName, index, colorAssign){
                token.delegate = self
                token.object = self.selectUsersArr[index]
                return token
            }
        }
        
        if let items = self.selectUsersArr[index] as? Contacts {
            if let token = NWSImageToken.initWithTitle(/items.name, image: /items.photo ,nil, index) {
                token.delegate = self
                token.object = self.selectUsersArr[index]
                return token
            }
        }
        return nil
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int, token: NWSToken?) {
        ez.runThisInMainThread {
            self.deleteTokenAtIndex(index , true)
        }
        self.updateModal(object: token?.object)
    }
    
    
    // MARK: NWSTokenDelegate
    @objc(tokenView:didDeselectTokenAtIndex:)
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int){
    
    }
    
    //MARK::- TODO
    func deleteTokenAtIndex(_ index : Int, _ fromCursor : Bool? = true){
        if index < self.selectUsersArr.count{
            self.selectUsersArr.remove(at: index)
            ez.runThisInMainThread {
                self.tokenView.reloadData()
            }
         }
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView)  {

    }

    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String){
    
    }

    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String){
        if text == ""
        {
            return
        }
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize){
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
         self.view.layoutIfNeeded()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int) {
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int) {
        
    }
    
    
}


//MARK::- Extension TableView Delegate and DataSource
extension ComposeDetailsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.intCellType == 1 {
            return /arrayModules?.count
            
        }
        else {
            return /arrayContactCommon?.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if intCellType == 1{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellComposeClass", for: indexPath) as? ComposeClass else {return UITableViewCell()}
            cell.lblClass?.text = arrayModules?[indexPath.row].ClassName
            cell.btnSelectClass?.addTarget(self, action: #selector(dialog_checkButtonTapped(button:)), for: .touchUpInside)
            cell.btnSelectClass?.tag = indexPath.row
            cell.btnSelectClass?.isUserInteractionEnabled = false
            cell.assignIndex = self.assignIndex
            cell.setUpText()
            cell.btnViewParent?.tag = indexPath.row
            cell.delegate = self
            cell.btnSelectClass?.setImage(/arrayModules?[indexPath.row].isSelect ? UIImage(named: "Check") : UIImage(named: "Uncheck"), for: UIControl.State.normal)
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellComposeContact", for: indexPath) as? ComposeContact else {return UITableViewCell()}
            
            cell.btnSelectContact?.isUserInteractionEnabled = false
            cell.btnSelectContact?.setImage(/arrayContactCommon?[indexPath.row].isSelect ?  UIImage(named: "Check") : UIImage(named: "Uncheck"), for: UIControl.State.normal)
            let strName = arrayContactCommon?[indexPath.row].name
            let strChildName  = arrayContactCommon?[indexPath.row].childName
            let designation = arrayContactCommon?[indexPath.row].designation
             let strClassName  = arrayContactCommon?[indexPath.row].ClassName
            if intTypeRadio == 1
            {
                cell.lblName?.text  = /strName
                cell.lblClass?.text = strChildName
            }
            else if intTypeRadio == 2 {
                cell.lblName?.text  = "\(/strName)"
                cell.lblClass?.text = "P/O \(/strChildName) (\(/strClassName))"
            }
            else if intTypeRadio == 3 {
                cell.lblName?.text  = /strName
                cell.lblClass?.text = /designation
            }
            
            if let imgUrl = arrayContactCommon?[indexPath.row].photo{
                let url = imgUrl.getImageUrl()
                 cell.imgView?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            }
            else {
                cell.imgView?.image = R.image.noProfile_Big()
            }
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellComposeHeader") as! ComposeHeader
        
        if intCellType == 1{
            cell.lblSelect?.text = "Select All Class"
            boolSectionCheck = self.method_CheckCount() == /arrayModules?.count
            
            if self.method_CheckCount() == 0 {
                boolSectionCheck = false
            }
        }
        else if intCellType == 2{
            cell.lblSelect?.text = "Select All Contact"
            boolSectionCheck = self.method_CheckCount() == /arrayContactCommon?.count
            
            if self.method_CheckCount() == 0 {
                boolSectionCheck = false
            }
        }
        cell.btnSelectAllContact?.addTarget(self, action: #selector(ClkBtnHeader(button:)), for: .touchUpInside)
        cell.btnSelectAllContact?.setImage(boolSectionCheck ? UIImage(named: "Check") : UIImage(named: "Uncheck"), for: .normal)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 45
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.didSelect(indexPath)
    }
    
    func didSelect(_ indexPath  : IndexPath){
        
        if intCellType == 1{
            let isChecked = !(/arrayModules?[indexPath.row].isSelect)
            arrayModules?[indexPath.row].isSelect = isChecked
            updateToken(value : arrayModules?[indexPath.row] ?? "" , isChecked: isChecked)
        }
            
        else {
            let isChecked = !(/arrayContactCommon?[indexPath.row].isSelect)
            arrayContactCommon?[indexPath.row].isSelect = isChecked
            updateToken(value : arrayContactCommon?[indexPath.row] ?? "" , isChecked: isChecked)
        }
         updateTokens()
//        self.tblViewCompose.reloadRows(at: [IndexPath(row: indexPath.row, section: 0)], with: .none)
    }
    
    
    func updateTokens(){
        self.tblViewCompose.reloadData()
        self.tokenView.delegate = self
        self.tokenView.dataSource = self
        self.tokenView.reloadData()
    }
    
}


//MARK::- Update Token

extension ComposeDetailsViewController {
    
    func updateToken(value : Any , isChecked : Bool) {
        
        if let contact = value as? Contacts {
            addRemoveContact(contact , isChecked)
        }
        
        if let classs = value as? MyClasses {
            addRemoveClasses(classs, isChecked)
        }
    }
    
    //Simply Users
    func addRemoveContact(_ contact : Contacts , _ isChecked : Bool) {
        var found : Bool = false
        for (index,selectVal) in selectUsersArr.enumerated() {
            if let value = selectVal as? Contacts {
                if value.receiverID == contact.receiverID  && value.receiverType == contact.receiverType {
                    found = true
                    if  !isChecked {
                        selectUsersArr.remove(at: index)
                    }
                    break
                }
            }
        }
        if !found {
            selectUsersArr.append(contact)
        }
    }
    
    
    func addRemoveClasses(_ classs : MyClasses , _ isChecked : Bool) {
        var found : Bool = false
        for (index,selectVal) in selectUsersArr.enumerated() {
            if let value = selectVal as? MyClasses {
                if value.classID == classs.classID  && value.ClassName == classs.ClassName {
                    found = true
                    if  !isChecked {
                        selectUsersArr.remove(at: index)
                    }
                    break
                }
            }
        }
        if !found {
            selectUsersArr.append(classs)
        }
    }
}




// TableView Functions
/*
 1) ClkBtnHeader
 2) method_CheckCount
 3) dialog_checkButtonTapped
 4) refreshTableview
 
 */
extension ComposeDetailsViewController {
    
    
    @objc func ClkBtnHeader(button: UIButton) {
       button.setBackgroundImage(boolSectionCheck ? UIImage(named: "Check") : UIImage(named: "Uncheck"), for: UIControl.State.normal)
        boolSectionCheck = !boolSectionCheck
        self.refreshTableview(isChecked: boolSectionCheck)
    }
    
    @objc func method_CheckCount() -> Int {
        if self.intCellType == 1 {
            let totalSelectModule = arrayModules?.filter({$0.isSelect == true})
            //            print("Total Selected Class Count = \(/totalSelectModule?.count)")
            return /totalSelectModule?.count
            
        } else {
            let totalSelectModule = arrayContactCommon?.filter({$0.isSelect == true})
            //            print("Total Selected Contact Count = \(/totalSelectModule?.count)")
            return /totalSelectModule?.count
        }
    }
    
    
    //  MARK: - dialog_checkButtonTapped
    @objc func dialog_checkButtonTapped(button: UIButton) {
        
        let point = tblViewCompose.convert(CGPoint.zero, from: button)
        if let indexPath = tblViewCompose.indexPathForRow(at: point){
            self.didSelect(indexPath)
        }
    }
    
    //  MARK: - refreshTableview
    @objc func refreshTableview(isChecked : Bool){
       
        //for clases
        if intCellType == 1{
            arrayModules = arrayModules?.map({ (myClass) -> MyClasses in
                myClass.isSelect = isChecked
                return myClass
            })
            selectUsersArr = isChecked ? (arrayModules ?? []) : []
        }
            
            // for contact & staff
        else {
            arrayContactCommon = arrayContactCommon?.map({ (myContact) -> Contacts in
                myContact.isSelect = isChecked
                return myContact
            })
            selectUsersArr =  isChecked ? (arrayContactCommon ?? []) : []
        }
        tblViewCompose.reloadData()
        tblViewCompose.beginUpdates()
        tblViewCompose.endUpdates()
        tokenView.reloadData()
    }
}

extension ComposeDetailsViewController : DeleteTokenFromCrossDelegate {
    
    
    func deleteToken(_ index: Int, _ object : Any?) {
        ez.runThisInMainThread {
            self.deleteTokenAtIndex(index ,false)
            self.updateModal(object : object)
        }
    }
    
    func updateModal(object : Any?){
        var isUpdate : Bool = false
//        var changeIndex : Int = 0
        if let classs = object as? MyClasses {
            for (index , value) in ((self.arrayModules ?? []).enumerated()){
                if (value.classID == classs.classID) {
                    value.isSelect = false
                    self.arrayModules?[index].isSelect = false
                    self.backUpClassesArr?[index].isSelect = false
//                    changeIndex = index
                    isUpdate = true
                    break
                }
            }
//            for (index , value) in ((self.backUpClassesArr ?? []).enumerated()){
//                if (value.classID == classs.classID) {
//                    value.isSelect = false
//                    self.backUpClassesArr?[index].isSelect = false
//                     changeIndex = index
//                    isUpdate = true
//                    break
//                }
//            }
        }
        
        if let contact = object as? Contacts {
            for (index , value) in ((self.arrayContactCommon ?? []).enumerated()){
                if (value.receiverID == contact.receiverID) && (value.receiverType == contact.receiverType) {
                    value.isSelect = false
                    self.arrayContactCommon?[index].isSelect = false
                    self.backUpContactArr?[index].isSelect = false
//                     changeIndex = index
                    isUpdate = true
                    break
                }
            }
        }
        if isUpdate {
            tblViewCompose.reloadData()
//            tblViewCompose.reloadSections(IndexSet(integer : 0), with: .top)
//            tblViewCompose.reloadRows(at: [IndexPath(row: changeIndex, section: 0)], with: .none)
        }
    }
}


//MARK: - Configure CollectionView
extension ComposeDetailsViewController {
    
    func configureCollectionView() {
        
        collectionDataSource =  CollectionViewDataSource(items: users, collectionView: collectionView, cellIdentifier: CellIdentifiers.UsersTypeCell.get, headerIdentifier: nil, cellHeight: /self.collectionView?.bounds.height , cellWidth: /self.collectionView?.bounds.width / 3.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell = cell as? UsersTypeCell ,let index = indexpath?.row else {return}
            cell.selectIndex = self.selectedIndex
            cell.currentIndex = index
            cell.model = item
        }
        
        collectionDataSource?.aRowSelectedListener = {(indexpath, cell) in
            if self.selectedIndex == /indexpath.row {
                return
            }
            self.selectedIndex = /indexpath.row
            self.collectionView?.reloadData()
           
            if self.selectedIndex == 0 {
                self.assignIndex = 2
            }
            else if self.selectedIndex == 1 {
                 self.assignIndex = 1
            }
            else if self.selectedIndex == 2{
                 self.assignIndex = 3
            }
            else {
                self.assignIndex = 1
            }
            self.intTypeRadio =  self.assignIndex
            self.selectUsersArr = []
            self.tokenView.reloadData()
            self.arrayModules = []
            self.isSearching = false
            self.arrayContactCommon = []
            self.tblViewCompose.reloadData()
            self.webAPIContact(senderType: self.assignIndex)
        }
    }
}
extension ComposeDetailsViewController {
    
    func webAPIContact(senderType: Int)
    {
        self.view.endEditing(true)
        print("Func: webAPIContact")
        Utility.shared.loader()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        if (senderType == 1 || senderType == 2){
            urlString    += "Teacher/MyClass?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&UserID=\(intUserID)"
            intCellType = 1
        }
        else if senderType == 3 {
            urlString    += "Message/StaffContact?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&UserID=\(intUserID)&UserType=\(intUserType)"
            webAPIStaffContact(urlString: urlString)
            intCellType = 2
            
        }
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                      Utility.shared.removeLoader()
                }
               switch response.result {
                case .success(let data):
                    
                    self.tblViewCompose.isHidden = false
                    let object = Mapper<ContactsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.myClasses{
                            self.arrayModules = boardArray
                            self.backUpClassesArr = self.arrayModules
                            
                            self.tblViewCompose.reloadData()
                        }
//                        }else{
//                            self.arrayModules = []
//                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    //Contact API
    func webAPIContactStudentParentContact(_ classID : String)
    {
        self.view.endEditing(true)
        Utility.shared.loader()
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        let selectClass = classID
        urlString    += "Message/StudentParentContact?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&ofUserType=\(intTypeRadio)&ClassIDs=\(selectClass)"
        //1- Student, 2- Parent
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    let object = Mapper<ContactsModel>().map(JSONObject: data)
                    guard let parsedObject1 = object else {return}
                    self.tblViewCompose.isHidden = false
                    if parsedObject1.errorCode == 0 &&  parsedObject1.status == "ok" {
                        self.intCellType = 2
                        self.intLayer = 2
                        self.btnCancel.setTitle(self.intLayer == 2  ? "Back" : "Cancel" , for: .normal)
                        if let StudentParentContactArray = parsedObject1.contacts{
                            self.arrayContactCommon   = StudentParentContactArray
                            self.backUpContactArr =  self.arrayContactCommon
                            self.tblViewCompose.reloadData()
                        }
                    }
                    else{
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(let jsonError):
                    print("jsonError \n \(jsonError)")
                    break
                }
        }
    }
    func webAPIStaffContact(urlString: String)
    {
        self.view.endEditing(true)
        print("Func: webAPIStaffContact = \(urlString)")
       Utility.shared.loader()
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblViewCompose.isHidden = false
                    let object = Mapper<ContactsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok"
                    {
                        self.intCellType = 2
                        if let StudentParentContactArray = parsedObject.contacts{
                            self.arrayContactCommon      = StudentParentContactArray
                            self.backUpContactArr =  self.arrayContactCommon
                            self.tblViewCompose.reloadData()
                        }
//                        else{
//                            self.arrayModules = []
//                        }
                    }
                    else{
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(let error):
                    print("ERROR: \(error.localizedDescription)")
                    break
                }
        }
    }
}
extension ComposeDetailsViewController : ViewParentDelegate {
    func viewParent(_ index: Int) {
        if index < /self.arrayModules?.count  {
            if let classs =  self.arrayModules?[index]  , let classID = classs.classID?.toString {
                webAPIContactStudentParentContact(classID)
            }
        }
    }
}
