//
//  TextViewController.swift
//  ComposeMessageModule
//
//  Created by NupurMac on 15/05/18.
//  Copyright © 2018 ShubhamMac. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class TextViewController: BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var txtSubjLine : UITextField?
    @IBOutlet weak var txtMessage : UITextView?
    @IBOutlet weak var btnSend : CustomButtom?
    
    //MARK::- VARIABLES
     var placeholderLabel : UILabel!
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        placeholderLabel = Utility.shared.setUpTextViewPlaceholder(txtMessage!)
        txtMessage?.addSubview(placeholderLabel)
        placeholderLabel.isHidden = !(/txtMessage?.text.isEmpty)
    }
    
    func onViewDidLoad(){
        txtMessage?.delegate = self
        txtSubjLine?.delegate = self
        txtSubjLine?.attributedPlaceholder = Utility.shared.setUpPlaceholderFont(placeholderText: MsgConstants.subjLine.get)
    }
    
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnSendAct(_ sender: UIButton) {
        
//        sendMessage(subjectLine: txtSubjLine?.text, body: txtMessage?.text,msgType: "1", byteArray: nil) { (response) in
//            if let str = response as? String {
//               Messages.shared.show(alert: .success , message: str, type: .success)
//           }
//        }
    }
}

//MARK ::- CheckSendButtonState
extension TextViewController {
    
    func checkSendButtonState() {
        if !(/txtMessage?.text.trimmed().isEmpty) && !(/txtSubjLine?.text?.trimmed().isEmpty){
           changeButtonState(isEnabled : true)
        }
        else {
            changeButtonState(isEnabled : false)
        }
    }
    
    func changeButtonState(isEnabled: Bool) {
        btnSend?.borderColor = isEnabled ? UIColor.flatLightBlack : UIColor.flatGray
        btnSend?.isUserInteractionEnabled = isEnabled
        btnSend?.setTitleColor(isEnabled ? UIColor.flatLightBlack : UIColor.flatGray, for: .normal)
    }
}


//MARK::- TextViewDelegate
extension TextViewController : UITextViewDelegate {
   
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        checkSendButtonState()
    }
    
    
}

//MARK::- TextFieldDelegate
extension TextViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         changeButtonState(isEnabled : /txtSubjLine?.text?.count == 0)
//        btnSend?.isHidden = (/txtSubjLine?.text?.count == 0)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
          checkSendButtonState()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        checkSendButtonState()
        return true
    }
}

