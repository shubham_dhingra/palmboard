//
//  ViewController.swift
//  ComposeMessageModule
//
//  Created by NupurMac on 15/05/18.
//  Copyright © 2018 ShubhamMac. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import EZSwiftExtensions
import IQKeyboardManager
import Lottie

class ComposeMsgViewController : BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var msgStack: UIStackView?
    @IBOutlet weak var txtSubject : UITextField?
    @IBOutlet weak var txtMessage : UITextView?
    @IBOutlet weak var viewReply : UIView?
    @IBOutlet weak var viewMedia : UIView?
    @IBOutlet weak var btnRecipent : UIButton?
    @IBOutlet weak var btnSend : UIButton?
    @IBOutlet weak var msgStackBottomConstraint : NSLayoutConstraint?
    @IBOutlet weak var lblHeading : UILabel?
    @IBOutlet weak var tblDropDown : UITableView?
    @IBOutlet weak var smsTypeAction: UIButton?
    @IBOutlet weak var dropDownButtonHeight : NSLayoutConstraint?
    @IBOutlet weak var tokenView: NWSTokenView!
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var bluredView : UIView?
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView?
    
    //MARK::- VARIABLES
    var placeholderLabel : UILabel!
    var messageType : MessageType!
    var smsTypeList : [SMSTypeList]?
    var uploadImage : UIImage?
    var attachment : String?
    var containerView : UIView!
    var selectUsersArr = [Any]()
    var isFirstTime : Bool = true
    var msgSendType : Int = 1
    var recipientType : String?
    var lottieView = LOTAnimationView()
    var msgTypeID : Int?
    
    let tokenViewMinHeight: CGFloat  = 40.0
    let tokenViewMaxHeight: CGFloat = 150.0
    let tokenBackgroundColor = UIColor(red: 98.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    
    private var isiPhoneX: Bool {
        return (UIScreen.main.nativeBounds.size.height == 2436) ||  (UIScreen.main.nativeBounds.size.height == 2688) || (UIScreen.main.nativeBounds.size.height == 1792)
    }
    
    var tableDropDownDataSource : TableViewDataSource?{
        didSet{
            
            tblDropDown?.dataSource = tableDropDownDataSource
            tblDropDown?.delegate = tableDropDownDataSource
            tblDropDown?.reloadData()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bluredView?.isHidden = true
        IQKeyboardManager.shared().isEnabled = false
        //   activityIndicator?.stopAnimating()
        
        onViewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = true
    }
    
    func onViewWillAppear() {
        if messageType == MessageType.sms {
            getSMSType()
            self.viewMedia?.isHidden = true
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.isNavBarHidden = true
    }
    
    func getSMSType() {
        APIManager.shared.request(with: HomeEndpoint.smsType(userType: userType), isLoader: false) {[weak self](response) in
            self?.handleResponse(isErrorShown: false, response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    
    func handle(response : Any?){
        if let modal = response as? MessageMainModal {
            smsTypeList = modal.smsTypeList
            
        }
    }
    
    func onViewDidLoad() {
        //
//        NotificationCenter.default.addObserver(self, selector: #selector(ComposeMsgViewController.keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(ComposeMsgViewController.keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateRecipients(_:)), name: .RECIPEINTS_UPDATE, object: nil)
        contentView.frame.size.height = self.view.frame.height - 72.0
        placeholderLabel = Utility.shared.setUpTextViewPlaceholder(txtMessage!)
        txtMessage?.addSubview(placeholderLabel)
        placeholderLabel.isHidden = !(/txtMessage?.text.isEmpty)
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().isEnabled = false
        hideTabBar()
        hideMenu()
        let tapGesture =  UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(onTouchTableView(_:)))
        contentView.addGestureRecognizer(tapGesture)
        
        txtSubject?.placeholder = messageType.placeholder
        lblHeading?.text = messageType.id
        if userType != "3" {
            viewMedia?.isHidden = true
        }
        else {
            viewMedia?.isHidden = messageType == MessageType.sms
        }
        
        
        dropDownButtonHeight?.constant = messageType == MessageType.sms ? 40.0 : 0.0
        smsTypeAction?.isUserInteractionEnabled = messageType == MessageType.sms
        txtSubject?.isUserInteractionEnabled = !(/smsTypeAction?.isUserInteractionEnabled)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        tokenView.layoutIfNeeded()
        tokenView.dataSource = self
        tokenView.delegate = self
        tokenView.reloadData()
        self.isNavBarHidden = true
        
    }
    
    @objc func onTouchTableView(_ sender : UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func btnMediaAct(_ sender : UIButton){
        self.view.endEditing(true)
        mediaAction(sender)
    }
    
    func mediaAction(_ sender : UIButton){
        
        switch sender.tag { 
            
        //camera
        case 0:
            openMedia(mediaType : AlertConstants.Camera.get)
            break
        // gallery
        case 1:
            openMedia(mediaType : AlertConstants.Gallery.get)
            break
            
        //audio
        case 2:
            break
        default:
            break
        }
    }
    
    func openMedia(mediaType : String?) {
        CameraGalleryPickerBlock.shared.pickerImage(isActionSheetOpen: false, openMediaType: mediaType, pickedListner: { (image, fileName) in
            self.uploadImage = image
            self.msgSendType = 2
            
//            if  self.getCurrentUser()?.role?.lowercased() == "teacher" {
//                self.bluredView?.isHidden = false
//                self.activityIndicator?.startAnimating()
//                self.bluredView?.bringSubviewToFront(self.view)
//                self.matchmultifacesComposeMessage()
//            }
//            else {
                self.sendAppOrComposeMessage()
            //}
        }) {
        }
    }
    //Notification is there any updae in recipients list
    @objc func updateRecipients(_ notification : NSNotification){
        if let recipentsArr = notification.userInfo?["Recipients"] as? [Any] , let type = notification.userInfo?["Type"] as? Int {
            ez.runThisInMainThread {
                self.selectUsersArr = recipentsArr
                self.recipientType = type.toString
                self.tokenView.reloadData()
            }
        }
    }
    
    @IBAction func btnAddRecipientAct(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        let composeMsgVC = storyboard
            .instantiateViewController(withIdentifier: "ComposeDetailsViewController")
        (composeMsgVC as? ComposeDetailsViewController)?.isUdpateSelectList = false
        self.presentVC(composeMsgVC)
    }
    
    
    @IBAction func btnSendAct(_ sender: UIButton) {
        sendAppOrComposeMessage()
    }
    
    func matchmultifacesComposeMessage() {
        self.view.endEditing(true)
        
        
        if let image = self.uploadImage  {
            attachment = image.convertImageToBase64()
        }
        APIManager.shared.request(withImage: HomeEndpoint.Matchmultifaces(), image: self.uploadImage) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handleMatchmultifaces(response : success)
            })
        }
    }
    //MARK::- HANDLE API RESPONSE
    func handleMatchmultifaces(response : Any?){
        if let modal = response as? MatchmultifacesModal {
            
            bluredView?.isHidden = true
            activityIndicator?.stopAnimating()
            
            if let usersArr = modal.dataprofileList {
                for (_ ,user) in usersArr.enumerated() {
                    let contact = Contacts.init(receiverId: user.userID, receiverType: 2 , name: user.name, photo: user.photo)
                    self.selectUsersArr.append(contact)
                }
            }
            self.tokenView.reloadData()
            self.sendAppOrComposeMessage()
        }
    }
    
    func sendAppOrComposeMessage() {
        self.view.endEditing(true)
        if let image = self.uploadImage  {
            attachment = image.convertImageToBase64()
        }
        
        if "".validateSendMessage(txtSubject?.text, txtMessage?.text , msgSendType.toString) {
            sendMessage(subjectLine: messageType == .app ? txtSubject?.text : msgTypeID?.toString, body: txtMessage?.text, msgType: msgSendType.toString , byteArray: attachment, selectUsersArr: selectUsersArr, recipientType: self.recipientType , MessageType : messageType) { (response) in
                if let str = response as? String {
                    // Messages.shared.show(alert: .success , message: str, type: .success)
                    self.startLottie(str : str)
                }
            }
        }
    }
    
    func startLottie(str : String) {
        lottieView =  LOTAnimationView(name: LottieAnimation.CheckMarkSuccess.rawValue)
        lottieView.frame = CGRect(x: self.view.center.x - 100.0  , y: self.view.center.y  - 100.0 , w: 200.0, h: 200.0)
        self.view.addSubview(lottieView)
        self.view.bringSubviewToFront(lottieView)
        lottieView.play { (status) in
            self.resetScreen()
            self.lottieView.removeFromSuperview()
            //             Messages.shared.show(alert: .success , message: str, type: .success)
            self.popVC()
        }
    }
    
    func resetScreen() {
        
        self.txtSubject?.text = nil
        self.txtMessage?.text = nil
        attachment = nil
        selectUsersArr = []
        tokenView.reloadData()
    }
    
    
    @IBAction func openSmsListAct(_ sender: UIButton) {
        openDropDownTable()
    }
    
    // MARK: - keyboardWillShow
     func keyboardWillShow(){
       msgStackBottomConstraint?.constant = isiPhoneX ? 243.0 : 226.0
        viewMedia?.isHidden = true
    }
    
    // MARK: - keyboardWillHide
     func keyboardWillHide(){
        if userType != "3" {
            viewMedia?.isHidden = true
        }
        else {
            viewMedia?.isHidden = messageType == MessageType.sms
        }
         msgStackBottomConstraint?.constant = 0
        txtMessage?.resignFirstResponder()
    }
    
}

extension ComposeMsgViewController : UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        self.view.endEditing(true)
        return true
    }
}

//// MARK: - TextView Delegate Method

extension ComposeMsgViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        if textView == txtMessage {
            keyboardWillHide()
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtMessage {
            keyboardWillShow()
        }
    }
    
    
    
    
}

//MARK::- DROP DOWN
extension ComposeMsgViewController {
    
    func openDropDownTable(){
        self.view.endEditing(true)
        let dropDownFrame = CGRect(x: /smsTypeAction?.frame.origin.x + ((/smsTypeAction?.frame.width) / 2)  ,  y: /smsTypeAction?.frame.origin.y + /smsTypeAction?.frame.height + 10, width: /smsTypeAction?.frame.width / 2, height: CGFloat(/smsTypeList?.count * 45))
        UIView.animate(withDuration: 0.2) {
            self.setUpTableView(dropDownFrame)
        }
    }
    
    
    func setUpTableView(_ dropDownFrame : CGRect){
        
        containerView = UIView(frame:dropDownFrame)
        tblDropDown?.frame = containerView.bounds
        containerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        containerView.layer.shadowOpacity = 4.0
        
        self.view.addSubview(containerView)
        
        containerView.addSubview(self.tblDropDown!)
        tblDropDown?.isHidden = false
        tblDropDown?.layer.masksToBounds = true
        tblDropDown?.isScrollEnabled = false
        reloadTable()
        
    }
    
    func reloadTable() {
        if isFirstTime {
            configureDropDown()
            isFirstTime = false
        }
        else {
            tableDropDownDataSource?.items = smsTypeList
            tblDropDown?.reloadData()
        }
    }
    
    //MARK: - ConfigureTableView
    func configureDropDown() {
        
        tableDropDownDataSource = TableViewDataSource(items: smsTypeList , height: 45.0, tableView: tblDropDown, cellIdentifier: CellIdentifiers.DropDownCell.get)
        
        tableDropDownDataSource?.configureCellBlock = {(cell, item, indexpath) in
            
            (cell as? DropDownCell)?.model = item
        }
        tableDropDownDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
        
    }
    
    
    func didSelect(_ index : Int) {
        txtSubject?.text = smsTypeList?[index].subject
        msgTypeID = smsTypeList?[index].TypeID
        txtSubject?.textColor = UIColor.flatLightBlack
        UIView.animate(withDuration: 0.2) {
            self.containerView.removeFromSuperview()
            self.tblDropDown?.isHidden = true
        }
    }
}


//MARK::- ZFTokenField Delegate and Datasource
extension ComposeMsgViewController : NWSTokenDelegate , NWSTokenDataSource {
    
    
    // MARK: NWSTokenDataSource
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String) {
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String) {
        
    }
    
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int
    {
        return selectUsersArr.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets?
    {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String?
    {
        if selectUsersArr.count != 0 {
            return "To:"
        }
        return ""
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String?
    {
        return "Search "
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView? {
        
        if let items = self.selectUsersArr[index] as? MyClasses {
            var colorAssign : UIColor?
            
            if let color = items.colorAssign {
                colorAssign = color
            }
            else {
                colorAssign = UIColor.random()
                (self.selectUsersArr[index] as? MyClasses)?.colorAssign = colorAssign
            }
            
            if let token = NWSImageToken.initWithTitle(/items.ClassName, image: nil , /items.ClassName, index, colorAssign){
                token.delegate = self
                token.object = self.selectUsersArr[index]
                return token
            }
        }
        
        if let items = self.selectUsersArr[index] as? Contacts {
            if let token = NWSImageToken.initWithTitle(/items.name, image: /items.photo ,nil, index) {
                token.delegate = self
                token.object = self.selectUsersArr[index]
                return token
            }
        }
        return nil
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int, token: NWSToken?) {
        deleteTokenAtIndex(index , true)
        //        updateModal(object: token?.object)
    }
    
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int)  {
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int) {
    }
    
    //
    //MARK::- TODO
    func deleteTokenAtIndex(_ index : Int, _ fromCursor : Bool? = true){
        if index < self.selectUsersArr.count {
            self.selectUsersArr.remove(at: index)
            
            tokenView.reloadData()
            tokenView.layoutIfNeeded()
            tokenView.textView.becomeFirstResponder()
            
            // Check if search text exists, if so, reload table (i.e. user deleted a selected token by pressing an alphanumeric key)
            if /fromCursor {
                if tokenView.textView.text != ""
                {
                    //   self.searchContacts(tokenView.textView.text)
                }
            }
        }
    }
    
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView)  {
        if tokenView.textView.isFirstResponder && tokenView.textView.text != ""{
            
        }
    }
    
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
        
    }
    
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize)  {
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
        self.view.layoutIfNeeded()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int) {
        
    }
}

extension ComposeMsgViewController : DeleteTokenFromCrossDelegate {
    func deleteToken(_ index: Int, _ object : Any?) {
        deleteTokenAtIndex(index ,false)
    }
}


extension ComposeMsgViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first?.view == contentView {
            self.view.endEditing(true)
        }
    }
}


