//
//  HeaderViewController.swift
//  ComposeMessageModule
//
//  Created by NupurMac on 15/05/18.
//  Copyright © 2018 ShubhamMac. All rights reserved.
//

import UIKit
import Foundation
import EZSwiftExtensions

protocol  ContactListDelegate {
    func getContactList(contactsArr : [Contacts]?)
}

class HeaderViewController: UIViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnBack: UIButton?
    @IBOutlet weak var btnContact: UIButton?
    
//    //MARK::- VARIABLES
//    var contactNameList = [String]()
//    var contactListDelegte : ContactListDelegate?
//    
//    //MARK::- LIFECYCLES
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//    
//    //MARK::- BUTTON ACTIONS
//    @IBAction func btnBackAct(_ sender: UIButton) {
//        dismissVC(completion: nil)
//    }
//    
//    @IBAction func btnContactAct(_ sender: UIButton) {
//        
//        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
//        let composeMsgVC = storyboard
//            .instantiateViewController(withIdentifier: "ComposeDetailsViewController")
//        (composeMsgVC as? ComposeDetailsViewController)?.isUdpateSelectList = false
//        self.presentVC(composeMsgVC)
//    }
//}
//extension HeaderViewController : SelectContactDelegate {
//    
//    func getContactNames(contactsArr: [Contacts]?) {
//        print("Contact List Count :\(/contactsArr?.count)")
//        contactListDelegte?.getContactList(contactsArr: contactsArr)
//    }
}
