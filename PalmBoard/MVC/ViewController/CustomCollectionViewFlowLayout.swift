
import UIKit

class CustomCollectionViewFlowLayout: UICollectionViewFlowLayout {

    required init(coder: NSCoder) {
        super.init(coder: coder)!
        
        self.minimumLineSpacing = 10
      //  self.sectionInset.top = 0
        self.estimatedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 60)
    }
}
