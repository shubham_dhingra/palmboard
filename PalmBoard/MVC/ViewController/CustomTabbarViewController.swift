import UIKit
import EZSwiftExtensions

class CustomTabbarViewController: UIViewController,UITabBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, HideBarDelegateLibrary,HideBarDelegatePayment,HideBarDelegateLeave
{
    
    @IBOutlet weak var mainTabbar:          UITabBar?
    @IBOutlet weak var imgview:             UIImageView?
    @IBOutlet weak var mainTabBarHeight: NSLayoutConstraint?
    @IBOutlet weak var tabCollectionView:   UICollectionView?
    @IBOutlet weak var customContainerView: UIView?
    @IBOutlet weak var containerViewBottom: NSLayoutConstraint?
    @IBOutlet weak var barViewHeight: NSLayoutConstraint?
    
    var dictResult1          = [String : Any]()
    var strName              = String()
    var strPhoto             = String()
    var strSender            = String()
    var containerHeight      = CGFloat()
    var intListChoice        = Int()
    var arrayMenu            = [[String: Any]]()
    var  storyboard1         = UIStoryboard()
    var segueId              = String()
    var childControllers     = [UIViewController]()
    var currentCollectnY     = CGFloat()
    var tempCollectnY        = CGFloat()
    var selectedIndex        = Int ()
    
    private var isiPhoneX: Bool {
        return (UIScreen.main.nativeBounds.size.height == 2436) ||  (UIScreen.main.nativeBounds.size.height == 2688) || (UIScreen.main.nativeBounds.size.height == 1792)
    }
    
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        // self.isNavBarHidden = true
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateModuleList(_:)), name: NSNotification.Name(rawValue: NOTI.UPDATE_MODULE_LIST), object: nil)
        
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: NOTI.VERIFICATION_SUCCESS), object: nil, queue: nil) { (_) in
//            self.getMenu()
        }
        
        if let items = mainTabbar?.items?[0] {
          mainTabbar?.selectedItem = items
        }
        
        selectedIndex  = -1
        if !isiPhoneX {
             barViewHeight?.constant = 0.0
        }
        
       // if the device is Iphone X and the Green Menu is hidden than view height of 26.0 else normal 20.0
        else {
            barViewHeight?.constant = self.containerViewBottom?.constant == 0 ? 26.0 : 20.0
        }
       
//        guard let themeColor = self.getCurrentSchool()?.themColor else { return}
        self.tabCollectionView?.backgroundColor = UIColor.flatGreen
        
    }
    
    //Func call on notificaiton Name UPDATE_GREEN_MENU on come back to home after verification
    @objc func updateModuleList(_ notification : NSNotification){
        if let moduleMenu = notification.userInfo?["menu"] as? [[String : Any]] {
            ez.runThisInMainThread {
                self.arrayMenu = moduleMenu
                self.selectedIndex = -1
                self.tabCollectionView?.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storyboard1       = UIStoryboard(name: "Main", bundle: Bundle.main)
        currentCollectnY  = (self.mainTabbar?.frame.origin.y)!
        tempCollectnY     =  (self.tabCollectionView?.frame.origin.y)!
         mainTabBarHeight?.constant = 49.0
        containerHeight = (self.customContainerView?.frame.height)!
        UserDefaults.standard.set(3, forKey: "logged_in")
        UserDefaults.standard.synchronize()
        self.view.layoutIfNeeded()
        
         NotificationCenter.default.addObserver(self, selector: #selector(selectUnselectTabBar(_:)), name: .SELECT_UNSELECT_TABBAR, object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(selectUnselectMenuItem(_:)), name: .SELECT_UNSELECT_MENUITEM, object: nil)
        
        if let schoolCode = self.getCurrentSchool()?.schCode{
            strSender =  schoolCode
        }
        
        if let strPhotoCheck = self.getCurrentUser()?.photoUrl{
            strPhoto = strPhotoCheck
        }
        strName     = /self.getCurrentUser()?.firstName
        getMenu()
        setupMiddleButton()
        mainTabbar?.delegate     =  self
        self.tabBar(mainTabbar!, didSelect:(mainTabbar?.items![0])!)
        
        let url  = strPhoto.getImageUrl()
        imgview?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: nil)
        
        let selectedColor   = UIColor(rgb: 0x545454)
        let unselectedColor = UIColor(rgb: 0x545454)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
        
        if let downcastStrings = mainTabbar?.items
        {
            let fullName    = strName.trimmed()
            let fullNameArr = fullName.components(separatedBy: " ")
            if fullNameArr.count != 0 {
                let name       = fullNameArr[0]
                downcastStrings[2].title = name
            }
            else {
               downcastStrings[2].title = strName
            }
        }
        
    }
    
   @objc func selectUnselectTabBar(_ notification : NSNotification){
        if let selection = notification.userInfo?["Selection"] as? Bool , let index = notification.userInfo?["Index"] as? Int {
            ez.runThisInMainThread {
                if index != -1 && selection{
                    if let items = self.mainTabbar?.items?[index]{
                     self.mainTabbar?.selectedItem  = items
                    }
                }
                else {
                   self.mainTabbar?.selectedItem = nil
                }
            }
        }
    }
    
    @objc func selectUnselectMenuItem(_ notification : NSNotification){
        if let selection = notification.userInfo?["Selection"] as? Bool , let index = notification.userInfo?["Index"] as? Int {
            ez.runThisInMainThread {
                if index != -1 && selection{
                   self.getIndexBasedOnModuleID(moduleID: index , changeController: false)
                }
            }
        }
    }
    @objc func onAppActive(_ notification : NSNotification) {
       // getMenu()
    }
    // MARK: - setupMiddleButton
    func setupMiddleButton()
    {
        imgview?.layer.cornerRadius = (imgview?.frame.size.height)! / 2
        imgview?.image              = R.image.noProfile_Big()
        imgview?.layer.borderColor  = UIColor(rgb: 0xF3F3F3).cgColor//UIColor.white.cgColor
        imgview?.layer.borderWidth  = 3.0
    }
    
    func hideSetUp(hide : Bool){
        tabCollectionView?.isHidden = hide
        mainTabBarHeight?.constant = hide ? 0 : 49.0
        imgview?.isHidden = hide
        
    }
    
    func addVC (controller : UIViewController!){
        
        controller!.view!.frame = CGRect(x:0 , y:0 , width: (self.customContainerView?.frame.width)!, height: (self.customContainerView?.frame.height)!)
        //print("controller!.view!.frame = \(controller!.view!.frame) \n containerView.frame = \(customContainerView.frame) ")
        customContainerView?.insertSubview(controller!.view!, belowSubview: self.mainTabbar!)
        controller?.didMove(toParent: self)
        childControllers.append(controller)
        
        //print("Insert customContainerView = \(customContainerView.frame)")
    }
    
    func removeVC (controller : UIViewController!){
        
        controller?.willMove(toParent: nil)// 1
        controller?.view.removeFromSuperview()           // 2
        controller?.removeFromParent()
        childControllers.removeAll()
    }
    // MARK: - addNewController
    func addNewController(navigationController : UINavigationController, classType : UIViewController.Type){
        if(childControllers.count > 0){
            if let childClassAdded = childControllers[0] as? UINavigationController{
                
                if(type(of: childClassAdded.viewControllers[0]) == classType && (childClassAdded.viewControllers.count == 1)){
                    
                }else{
                    
                    if(childControllers.count > 0){
                        
                        self.removeVC(controller: childControllers[0])
                    }
                    self.addChild(navigationController)
                    self.addVC(controller: navigationController)
                }
            }
        }else{
            
            self.addChild(navigationController)
            self.addVC(controller: navigationController)
        }
    }
    // MARK: - tabBar
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        var viewController = UINavigationController()
        var classType     = UIViewController.self
        
        
        switch item.tag {
            
        case 0:
            viewController = storyboard1.instantiateViewController(withIdentifier: "TabViewController1") as! UINavigationController
            classType = BoardViewController.self
            
        case 1:
            viewController = storyboard1.instantiateViewController(withIdentifier: "TabViewController2") as! UINavigationController
            classType = WhatsNewViewController.self
            
        case 2:
            viewController  = storyboard1.instantiateViewController(withIdentifier: "TabViewController3") as! UINavigationController
            classType = MainProfileViewController.self
            
        case 3:
            let composeMsgStoryBoard = UIStoryboard(name: "ComposeMessage", bundle: nil)
            viewController  = composeMsgStoryBoard.instantiateViewController(withIdentifier: "TabViewController4") as! UINavigationController
            classType = MessageViewController.self
        case 4:
            viewController  = storyboard1.instantiateViewController(withIdentifier: "TabViewController5") as! UINavigationController
            classType = PushSettingViewController.self
        default:
            break
        }
        selectedIndex = -1
        self.tabCollectionView?.reloadData()
        
        self.addNewController(navigationController: viewController, classType: classType)
    }
    // MARK: - collectionview delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMenu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMenu", for: indexPath) as! MenuCollectionViewCell
        cell.menuLbl?.text = self.arrayMenu[indexPath.row]["Module"] as? String
        
        var intMdlID = Int()
        if let intMdlID1 = self.arrayMenu[indexPath.row]["MdlID"] as? Int{
            intMdlID = intMdlID1
        }
        if selectedIndex == indexPath.row{
            cell.contentView.alpha = 1.0
        }
        else{
            cell.contentView.alpha = 0.7
        }
        switch intMdlID
        {
        case 1:
            cell.menuImgView?.image = R.image.notice()
            
        case 2:
            cell.menuImgView?.image = R.image.photos()
            
        case 3:
            cell.menuImgView?.image = R.image.circulars()
            
        case 4:
            cell.menuImgView?.image = R.image.syllabus()
            
        case 5 , 21:
            cell.menuImgView?.image = R.image.attendance()
            
        case 6:
            cell.menuImgView?.image = UIImage(named: "Time table")
            
        case 7:
            cell.menuImgView?.image = R.image.libraryMenu()
            
        case 8:
            cell.menuImgView?.image = R.image.activityCalendar()
            
        case 9:
            cell.menuImgView?.image = R.image.calssMate()
            
        case 10:
            cell.menuImgView?.image = R.image.teachers1()
            
        case 11:
            cell.menuImgView?.image = R.image.reportCard()
            
        case 12:
            cell.menuImgView?.image = R.image.assignments()
        case 13:
            //print("***13")
            cell.menuImgView?.image = R.image.birthday()
        case 14:
            //print("***14")
            cell.menuImgView?.image = R.image.leaveParent()
        case 15:
            cell.menuImgView?.image = R.image.payment()
        case 16:
            cell.menuImgView?.image = UIImage(named: "SMS")
        case 17 :
            cell.menuImgView?.image = R.image.eSmartGuardMenuLogo()
        case 18 :
            cell.menuImgView?.image = R.image.addSms()
        case 19:
            cell.menuImgView?.image = R.image.website()
        case 22:
            cell.menuImgView?.image = R.image.thoughts()
        case 23:
            cell.menuImgView?.image = R.image.questionary()
        case 25:
            cell.menuImgView?.image = R.image.weekly_plan()
        case 26:
            cell.menuImgView?.image = UIImage(named: "marks_entry")
        case 27:
            cell.menuImgView?.image = R.image.bus_tracker_new()
        case 28:
            cell.menuImgView?.image = R.image.survey_icon()
        case 29:
            cell.menuImgView?.image = R.image.payslip()

        default:
            break
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !APIManager.shared.isConnectedToInternet() {
            Utility.shared.internetConnectionAlert()
            return
        }
        if selectedIndex != indexPath.row {
            changeMenuToController(indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85.0, height: 65.0)
    }
    
    func changeMenuToController(indexPath: IndexPath , changeController : Bool? = true) {
        mainTabbar?.selectedItem = nil
        selectedIndex           = indexPath.row
        
        self.tabCollectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        if /changeController {
        if let intMdlID1 = self.arrayMenu[indexPath.row]["MdlID"] as? Int {
            if(childControllers.count > 0){
                if let myNav = childControllers[0] as? UINavigationController{
                  myNav.moveToItemPage(itemSelected: intMdlID1 , module : self.arrayMenu[indexPath.row])
                }
            }
        }
        }
        
        self.tabCollectionView?.reloadData()
    }
    
    
    func getIndexBasedOnModuleID(moduleID : Int , changeController : Bool? = true) {
        if self.arrayMenu.count == 0 {
            return
        }
        else {
            for (index,value) in arrayMenu.enumerated() {
                if let fetchModuleID =  value["MdlID"] as? Int {
                    if fetchModuleID == moduleID {
                     changeMenuToController(indexPath: IndexPath(row: index, section: 0) , changeController: changeController)
                     break
                    }
                }
            }
        }
    }
    
    // MARK: - hideBar(boolHide: Bool)
    func hideBar(boolHide: Bool)
    {
        if(boolHide){
            self.containerViewBottom?.constant = 0
            self.barViewHeight?.constant = isiPhoneX ? 26.0 : 0.0
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseOut, animations: {
                self.tabCollectionView?.transform = CGAffineTransform(translationX: 0, y: (self.currentCollectnY - self.tempCollectnY))
            }, completion: { (Bool) in
            })
        }else
        {
            self.barViewHeight?.constant = isiPhoneX ? 26.0 : 0.0
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseOut, animations: {
                self.tabCollectionView?.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: { (Bool) in
                self.containerViewBottom?.constant = (self.tabCollectionView?.frame.height)!
            })
        }
        self.view.layoutIfNeeded()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.mainTabbar?.invalidateIntrinsicContentSize()
    }
}
//MENU UDPATE ON APP BECOME ACTIVE
extension CustomTabbarViewController {
    
    func getMenu() {
        APIManager.shared.request(with: HomeEndpoint.validate(schoolCode : self.getCurrentSchool()?.schCode , username: self.getCurrentUser()?.username, password: self.getCurrentUser()?.password), isLoader : false) {[weak self](response) in
            switch response{
            case .success(let responseValue):
                self?.handle(response : responseValue)
                
            case .failure(let responseValue):
                self?.handle(response : responseValue)
            }
        }
    }
    func handle(response : Any?){
        
        if let response = response as? [String : Any] {
            guard let menu = response["Menu"] as? [[String : Any]] else {return}
            if self.arrayMenu.count == 0 || self.arrayMenu.count != menu.count {
                self.arrayMenu = menu
                tabCollectionView?.reloadData()
            }
        }
    }
}
