
import UIKit

class FwdTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgView:      UIImageView?
    @IBOutlet weak var imgViewRight: UIImageView?
    @IBOutlet weak var lblCell:      UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

