import UIKit
import Alamofire
import AlamofireImage

class FwdViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    
    @IBOutlet weak var firstView:      UIView?
    @IBOutlet weak var viewPhoneEmail: UIView?
    @IBOutlet weak var lastView:       UIView?
    @IBOutlet weak var txtField:       UITextField?
    @IBOutlet weak var stackView:      UIStackView?
    @IBOutlet weak var lblCode:        UILabel?
    @IBOutlet weak var lblError:       UILabel?
    @IBOutlet weak var lblLine:        UILabel?
    @IBOutlet weak var lblLastMessage: UILabel?
    @IBOutlet weak var widthLblCodeConstraint: NSLayoutConstraint?
    @IBOutlet weak var tblView:           UITableView?
    @IBOutlet weak var lblTitle:          UILabel?
    @IBOutlet weak var btnCross:          UIButton?
    @IBOutlet weak var btnOK:             UIButton?
    @IBOutlet weak var popUpViewHeight:   NSLayoutConstraint?
    @IBOutlet weak var tblViewHeight:     NSLayoutConstraint?
    
    var arrayPanels             = [String]()
    var arrayPanelsImg          = [String]()
    var arrayRegistered         = [String]()
    var arrayRegisteredImg      = [String]()
    var arrayPanelsImgHover     = [String]()
    var arrayRegisteredImgHover = [String]()
    var screenNumber            = 0
    var selectedRow             = 0
    var intMobileEmail          = Int()
    var contactOn               = String()
    var forgotLabel             = String()
    var username                = String()
    var arrayMultipleUsers      = [[String: Any]]()
    
    var intUserType             = Int()
    var intCountryCode          = Int()
    var intSMSPvdID             = Int()
    var strSchoolCode           = String()
    
    // MARK: - viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        intMobileEmail = 11
        
        tblView?.isScrollEnabled     = false
        stackView?.layer.borderColor = UIColor.red.cgColor
        stackView?.layer.borderWidth = 2.0
        
        viewPhoneEmail?.isHidden = true
        lastView?.isHidden       = true
        lblCode?.text            = String(intCountryCode)//"+91"
        
        guard let sMSService = self.getCurrentSchool()?.smsService else {return}
        print("sMSService = \(sMSService)")

        arrayPanels              = ["Parent Login ID", "Student Login ID", "Staff Login ID"]
        arrayPanelsImg           = ["Parent icon","Student", "Staff"]
        arrayPanelsImgHover      = ["Parent Hover","STUDENT HOVER", "Staff hover"]
        
        
        arrayRegistered          = sMSService == 0 ? ["e-Mail ID"]   : ["Mobile Number", "e-Mail ID"]
        arrayRegisteredImg       = sMSService == 0 ? ["email icon"]  : ["Mobile icon",   "email icon"]
        arrayRegisteredImgHover  = sMSService == 0 ? ["email hover"] : ["Mobile hover",  "email hover"]
        
       // screenNumber = sMSService == 0 ? 2 : screenNumber
            
        tblView?.rowHeight          = UITableView.automaticDimension
        tblView?.estimatedRowHeight = 2000
        lblTitle?.text              = forgotLabel == "password" ?  "Receive Details On " : "Username of"
        lblError?.isHidden          = true
        
        if(forgotLabel == "username"){
            screenNumber = 1
        }else{
            screenNumber = 2
        }
    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblView?.reloadData()
    }
    // MARK: - viewDidLayoutSubviews
    override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        tblView?.layoutIfNeeded()
        let height = min(popUpViewHeight?.constant ?? 0.0, (tblView?.contentSize.height)!)
        tblViewHeight?.constant = height
        popUpViewHeight?.constant = 25 + (tblView?.frame.height)! + (btnOK?.frame.height)! + 20
        self.view.layoutIfNeeded()
    }
    // MARK: - tableview Delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch screenNumber {
        case 1:
            return arrayPanels.count
        case 2:
            return arrayRegistered.count
        case 5:
            return arrayMultipleUsers.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFwd",for: indexPath as IndexPath)as! FwdTableViewCell
        
        switch screenNumber
        {
        case 1:
            cell.lblCell?.text       = arrayPanels[indexPath.row]
            cell.lblCell?.textColor = UIColor(rgb: 0x545454)
            cell.imgView?.image      = UIImage(named: arrayPanelsImg[indexPath.row] )
            cell.imgViewRight?.image = nil
        case 2:
            cell.lblCell?.text       = arrayRegistered[indexPath.row]
            cell.lblCell?.textColor = UIColor(rgb: 0x545454)
            cell.imgView?.image      = UIImage(named: arrayRegisteredImg[indexPath.row] )
            cell.imgViewRight?.image = nil
        case 5:
            cell.imgView?.layer.cornerRadius = (cell.imgView?.frame.height)! / 2
            cell.imgView?.clipsToBounds     = true
            cell.lblCell?.textColor         = UIColor(rgb: 0x545454)
            
            if let strName = arrayMultipleUsers[indexPath.row]["Name"] as? String{
                
                let substr = strName.components(separatedBy: "|").first
                cell.lblCell?.text      = substr
            }
            
            if let photoUrl = arrayMultipleUsers[indexPath.row]["Photo"] as? String {
              //  let finalUrlString = "http://webapi.palmboard.in\(photoUrl)"
                let finalUrlString = APIConstants.imgbasePath + photoUrl

                //print("finalUrlString \n \(finalUrlString)")
                let url = URL(string: finalUrlString )
                if let data = try? Data(contentsOf: url!){
                    
                    cell.imgView?.image  = UIImage(data: data )
                }
            }
            cell.imgViewRight?.image = nil
            
        default:
            return cell
        }
        let height = min(popUpViewHeight?.constant ?? 0.0, (tblView?.contentSize.height)!)
        tblViewHeight?.constant = height
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblView?.deselectRow(at: indexPath, animated: true)
        
        let currentCell = tableView.cellForRow(at: indexPath) as! FwdTableViewCell
        
        switch screenNumber
        {
        case 1:
            for i in 0..<self.arrayPanelsImgHover.count
            {
                let indexPath   = IndexPath(row: i, section: 0)
                let currentCell1 = tableView.cellForRow(at: indexPath) as! FwdTableViewCell
                currentCell1.imgViewRight?.image  = UIImage(named:"")
                currentCell1.imgView?.image       = UIImage(named: arrayPanelsImg[indexPath.row] )
                currentCell1.lblCell?.textColor   = UIColor(rgb: 0x545454)
            }
            currentCell.imgView?.image      = UIImage(named: arrayPanelsImgHover[(indexPath.row)] )
            currentCell.imgViewRight?.image  = UIImage(named: "Right Selected" )
            currentCell.lblCell?.textColor   = UIColor(rgb: 0xFAC100)
            
            //Green right
            btnOK?.setImage(UIImage(named:"Green right"), for: .normal)
            // Grey right
            if(indexPath.row == 0){
                selectedRow = indexPath.row + 2
            }else if (indexPath.row == 1){
                selectedRow = indexPath.row + 1
            }else{
                selectedRow = indexPath.row + 1
            }
            break
            
        case 2:
            guard let sMSService = self.getCurrentSchool()?.smsService else {return}
            intMobileEmail = sMSService == 0 ? 1 : indexPath.row
            
            for i in 0..<self.arrayRegisteredImg.count
            {
                let indexPath   = IndexPath(row: i, section: 0)
                let currentCell1 = tableView.cellForRow(at: indexPath) as! FwdTableViewCell
                currentCell1.imgViewRight?.image  = UIImage(named:"")
                currentCell1.imgView?.image       = UIImage(named: arrayRegisteredImg[indexPath.row] )
                currentCell1.lblCell?.textColor   = UIColor(rgb: 0x545454)
            }
            currentCell.imgView?.image      = UIImage(named: arrayRegisteredImgHover[(indexPath.row)] )
            currentCell.imgViewRight?.image  = R.image.rightSelected()
            currentCell.lblCell?.textColor   = UIColor(rgb: 0xFAC100)
            btnOK?.setImage(R.image.greenRight(), for: .normal)
            
            
        case 5:
            for i in 0..<self.arrayMultipleUsers.count
            {
                let indexPath   = IndexPath(row: i, section: 0)
                let currentCell1 = tableView.cellForRow(at: indexPath) as! FwdTableViewCell
                currentCell1.imgViewRight?.image  = UIImage(named:"")
                currentCell1.lblCell?.textColor   = UIColor(rgb: 0x545454)
            }
            currentCell.imgViewRight?.image  = R.image.rightSelected()
            currentCell.lblCell?.textColor   = UIColor(rgb: 0xFAC100)
            
            //Green right
            btnOK?.setImage(R.image.greenRight(), for: .normal)
            // Grey right
            selectedRow = indexPath.row + 1
            
            break
            
        default: break
        }
    }
    // MARK: - ClkOk
    @IBAction func ClkOk(_ sender: Any)
    {
        switch screenNumber
        {
        case 1:
            //select panels
            screenNumber = 2
            lblTitle?.text = "Receive Details On"
            btnOK?.setImage(R.image.greyRight(), for: .normal)
            tblView?.reloadData()
            break
        case 2:
            //email. phone choose
            
            if(forgotLabel == "password"){
                if intMobileEmail == 0
                {
                    contactOn                        = "mob"
                }
                else if intMobileEmail == 1
                {
                    contactOn                        = "email"
                }
                self.getContactDetails(intMobileEmail: intMobileEmail)
                
            }else{
                screenNumber = 3
                if intMobileEmail == 0
                {
                    lblTitle?.text                    = "Enter Registered phone number"
                    widthLblCodeConstraint?.constant  = 35
                    lblLine?.isHidden                 = false
                    txtField?.keyboardType            = UIKeyboardType.phonePad
                    contactOn                        = "mob"
                }
                else if intMobileEmail == 1
                {
                    lblTitle?.text                    = "Enter Registered email Id"
                    lblError?.isHidden                = true
                    widthLblCodeConstraint?.constant  = 0
                    lblLine?.isHidden                 = true
                    txtField?.keyboardType            = UIKeyboardType.emailAddress
                    contactOn                        = "email"
                    
                }
                btnOK?.setImage(R.image.greyRight(), for: .normal)
                viewPhoneEmail?.isHidden = false
                tblView?.isHidden        = true
            }
            
        case 3:
            //enter contact info
            if(intMobileEmail == 0 ){
                
                self.getContactDetails(intMobileEmail: intMobileEmail)
            }else{
                
                let isEmailAddressValid = isValidEmail(emailId: (txtField?.text)!)
                if isEmailAddressValid
                {
                    self.getContactDetails(intMobileEmail: intMobileEmail)
                    
                }else{
                    //invalid id entered
                    lblError?.text               = "Sorry, This email Id is not valid"
                    lblError?.isHidden           = false
                }
            }
            self.view.endEditing(true)
            
        case 4:
            //thanks screen
            dismiss(animated: true, completion: nil)
            break
            
        case 5:
            //multiple registered users
            if (forgotLabel == "username"){
                
                Utility.shared.loader()
                
                guard let intId = arrayMultipleUsers[selectedRow - 1]["UserID"] as? Int else{
                    return
                }
                guard let intType = arrayMultipleUsers[selectedRow - 1]["UserType"] as? Int else{
                    return
                }
                
                let urlString = APIConstants.basePath + "User/GetUserByUID?SchCode=\(strSchoolCode)&key=\(APP_CONSTANTS.KEY)&UserID=\(intId)&UserType=\(intType)&RcvOn=\(contactOn)&prvd_ID=\(intSMSPvdID)&CountryCode=\(intCountryCode)"

                
                print("urlString \n \(urlString)")
                
                WebserviceManager.getJsonData(withParameter: urlString, completion: { (results, _ error: Error?, _ errorcode: NSInteger?) in
                    
                    DispatchQueue.main.async
                        {
                            Utility.shared.removeLoader()
                            
                            if let result = results
                            {
                                if(result["Status"] as? String == "ok" )
                                {
                                    self.screenNumber = 4
                                    self.lastView?.isHidden  = false
                                    self.viewPhoneEmail?.isHidden = true
                                    self.firstView?.isHidden = true
                                    self.lblError?.isHidden  = true
                                    self.tblView?.isHidden   = true
                                }
                            }
                            if (error != nil){
                                DispatchQueue.main.async
                                    {
                                        switch (errorcode!){
                                        case Int(-1009):
                                            
                                            let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                            
                                        default:
                                            print("errorcode = \(String(describing: errorcode))")
                                        }
                                }
                            }
                    }
                })
                
            }
            
        default: break
        }
    }
    
    func getContactDetails(intMobileEmail : Int)
    {
        var phoneInfo = String()
        var emailInfo = String()
        
        if(intMobileEmail == 0 ){
            phoneInfo                       = (txtField?.text)!
            emailInfo                       = ""
        }else{
            phoneInfo                       = ""
            emailInfo                       = (txtField?.text)!
        }
        var parameters = Parameters( )
        var urlString = String()
        
        if (forgotLabel == "username"){
            parameters =
                [
                    "SchoolCode": strSchoolCode,
                    "key": APP_CONSTANTS.KEY,
                    "UserType": selectedRow,
                    "RcvOn": contactOn,
                    "CountryCode": intCountryCode,
                    "prvd_ID": intSMSPvdID,
                    "Mobile":phoneInfo,
                    "Email":emailInfo
            ]
            urlString = APIConstants.basePath + "user/GetUsername"
        }
        else{//Only Password
            parameters =
                [
                    "SchoolCode" : strSchoolCode,
                    "key"        : APP_CONSTANTS.KEY,
                    "UserType"   : intUserType,
                    "RcvOn"      : contactOn,
                    "CountryCode": intCountryCode,
                    "prvd_ID"    : intSMSPvdID,
                    "Username"   : self.username,
            ]
            urlString = APIConstants.basePath + "user/GetPassword"
        }
        self.getUsernamePwd(parameters: parameters, urlString: urlString)
    }
    // MARK: - getUsernamePwd
    func getUsernamePwd( parameters: Parameters, urlString: String){
        
        Utility.shared.loader()
        
        if(intMobileEmail == 0 ){
            self.lblError?.text         =  "Sorry, This number is not registered"
        }else{
            self.lblError?.text         =  "Sorry, This email Id is not registered"
        }
        print("\(urlString)")
        print("\(parameters)")
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (response, _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()

                    if let response = response
                    {
                        print("parameters \n \(parameters)")
                        print("response \n \(response)")
                        if(response["Status"] as? String == "ok" )
                        {
                            if (response["ErrorCode"] as! Int == 2)
                            {
                                self.screenNumber = 5
                                self.firstView?.isHidden = false
                                self.tblView?.isHidden   = false
                                self.viewPhoneEmail?.isHidden = true
                                self.lastView?.isHidden  = true
                                self.btnOK?.setImage(R.image.greyRight(), for: .normal)
                                if (self.forgotLabel == "username"){
                                    self.lblTitle?.text = "User Name Of"
                                }else{
                                    self.lblTitle?.text = "Password Of"
                                }
                                self.arrayMultipleUsers = response["Users"] as! [[String : Any]]
                                self.tblView?.reloadData()
                            }
                            else if (response["ErrorCode"] as! Int == 1)
                            {
                                //status not ok
                                self.lblError?.isHidden = false
                                self.lastView?.isHidden = true
                            }
                            else
                            {
                                
                                self.screenNumber = 4
                                self.lastView?.isHidden  = false
                                self.viewPhoneEmail?.isHidden = true
                                self.firstView?.isHidden = true
                                self.lblError?.isHidden  = true
                                self.tblView?.isHidden   = true
                                
                                if(self.intMobileEmail == 0 ){
                                    self.lblLastMessage?.text = R.string.localize.smsSentMobile(self.forgotLabel == "username" ? "Username" : "Password")
                                }else{
                                    self.lblLastMessage?.text = R.string.localize.smsSentEmail(self.forgotLabel == "username" ? "Username" : "Password")
                                }
                            }
                        }
                        else{
                            //status not ok
                            self.lblError?.isHidden = false
                            self.lastView?.isHidden = true
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        })
    }
    
    
    func isValidEmail(emailId: String) -> Bool{
        
        var returnValue = true
        let emailRegEx  = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailId as NSString
            let results = regex.matches(in: emailId, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    // MARK: - animateTxtFld
    func animateTxtFld(txtFld: UITextField, up: Bool){
        
        let movementDistance = -120
        let movementDuration  = 0.3
        
        let movement = (up ? movementDistance : -movementDistance)
        
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
    @IBAction func ClkCross(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    //MARK: textField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        animateTxtFld(txtFld: textField, up: true)
        // print("Fwd: textFieldDidBeginEditing")
        if  ((textField.text?.count) != nil){
            print("Fwd: value<0")
        }
        else{
            //  print("*******Fwd: value<0")
        }
    }
    // MARK: - textfield delegate methods
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        //print("Fwd: textFieldDidEndEditing")
        animateTxtFld(txtFld: textField, up: false)
        if  ((textField.text?.count) != nil)
        {
            print("Fwd: value>0")
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        txtField?.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length > 0
        {
            if (!(lblError?.isHidden)!){
                // txtField.text = ""
                self.lblError?.isHidden = true
                // btnOK.setImage(UIImage(named:"Grey right"), for: .normal)
            }
            btnOK?.setImage(R.image.greenRight(), for: .normal)
            
        }else{
            self.lblError?.isHidden = true
            btnOK?.setImage(R.image.greyRight(), for: .normal)
        }
        return true
    }
    // MARK: - touchesBegan()
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
