//  DetailsCollectionReusableView.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 04/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class DetailsCollectionReusableView: UICollectionReusableView {
   
    @IBOutlet weak var lblAlbumName:     UILabel?
    @IBOutlet weak var lblDateAndPhotos: UILabel?
    @IBOutlet weak var lblAlbumDetails:  UILabel?
    @IBOutlet weak var btnMore:     UIButton?
    @IBOutlet weak var heightBtnMore: NSLayoutConstraint?
}
