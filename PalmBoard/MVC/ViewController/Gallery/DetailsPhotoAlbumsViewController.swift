//  DetailsPhotoAlbumsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class DetailsPhotoAlbumsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,CALayerDelegate{
    
    @IBOutlet weak var collectionViewDetails: UICollectionView?
    
    private var gradient: CAGradientLayer?
    
    var strVideoURL        = String()
    var btnTitle           =  UIButton()
    //var arrayDetailsPhotos = [[String: Any]]()
    var intPage            = Int()
    var strSchoolCodeSender = String()
    var arrayAlbumDetails  = [[String: Any]]()
    var intUserID          = Int()
    var intUserType        = Int()
    var intAlId            = Int()
    var intIndexNumber     = Int()
    var passContentOffSet  = IndexPath()
    
    var strTitle           = String()
    var intAPIType         = Int()
    
    var strAlbumTitle        = String()
    var strAlbumDescription  = String()
    var strAlbumEventDate    = String()
    var btnRef  = UIButton()
    var lblRef = UILabel()
    var heightBtnMoreRef: NSLayoutConstraint!
    var boolCheck =  Bool()
    var fromBoardScreen  : Bool = false
    
    let footerViewReuseIdentifier = "RefreshFooterView"
    var footerView:CustomFooterView?
    var isLoading:Bool     = false
    
    var viewNavigationBar  = UIView()
    var lblTitle           = UILabel()
    var currentOffset     = CGFloat()
    var lblDescriptionHeight = CGFloat()
    var awsURL : String?
    
    //  private var _templateHeader : DetailsCollectionReusableView
    //  @IBOutlet weak var heightBtnMore: NSLayoutConstraint!
    
    
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewDetails?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        boolCheck = true
        
        viewNavigationBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44)
        viewNavigationBar.backgroundColor = UIColor.clear
        
        lblTitle             = UILabel(frame: CGRect( x: 0, y: 0, width:viewNavigationBar.frame.width, height: viewNavigationBar.frame.height))
        lblTitle.numberOfLines = 0
        lblTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblTitle.font         = R.font.ubuntuMedium(size: 17.0)
        lblTitle.textColor    = UIColor(rgb: 0x545454)//UIColor()/545454
        lblTitle.text         = ""
        lblTitle.textAlignment = NSTextAlignment.center
        //lblTitle.sizeToFit()
        //lblTitle.backgroundColor = UIColor.green
        //navigationController?.navigationBar.insertSubview(viewNavigationBar, at: 0)
        if !fromBoardScreen {
        self.view.addSubview(viewNavigationBar)
        viewNavigationBar.addSubview(lblTitle)
        }
        
        btnTitle                = UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle(strTitle, for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        self.collectionViewDetails?.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // print("viewDidLayoutSubviews")
        //updateGradientFrame()
    }
    
    // MARK: - CALayerDelegate
    func action(for layer: CALayer, forKey event: String) -> CAAction? {
        return NSNull()
    }
    
    // MARK: - Convenience
    private func updateGradientFrame() {
        
        gradient           = CAGradientLayer()
        gradient?.colors    = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient?.locations = [0, 0.1, 0.9, 1]
        gradient?.delegate  = self
        collectionViewDetails?.layer.mask = gradient
        
        gradient?.frame = CGRect(
            x: 0,
            y: (collectionViewDetails?.contentOffset.y)!,//scrollView.contentOffset.y,
            width: (collectionViewDetails?.bounds.width)!,//scrollView.bounds.width,
            height: (collectionViewDetails?.bounds.height)!//scrollView.bounds.height
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        intPage = 1
       
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            intUserType =  Int(userType)
        }
        
        collectionViewDetails?.isHidden = true
        
        self.collectionViewDetails?.delegate   = nil
        self.collectionViewDetails?.dataSource = nil
         NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        self.webApiGallery()
    }
    
    
    func webApiGallery()
    {
        Utility.shared.loader()
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        switch intAPIType
        {
        case 1:
            urlString += "gallery/PhotoDTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&AlID=\(intAlId)&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"
        case 2:
            urlString += "gallery/VideoDTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&VAID=\(intAlId)&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"
        case 3:
            print("My Favorite)")
        default :
            break
        }
        
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("Result == \(result)")
                    DispatchQueue.main.async {
                      Utility.shared.removeLoader()
                        self.collectionViewDetails?.isHidden = false
                        if let awsURL = (result["AWS_Path"] as? String){
                            self.awsURL = awsURL
                        }
                        switch self.intAPIType
                        {
                        case 1:
                            if let arrayAll = (result["Photos"] as? [[String: Any]]){
                                self.arrayAlbumDetails = arrayAll
                            }
                        case 2:
                            if let arrayAll = (result["Videos"] as? [[String: Any]]){
                                self.arrayAlbumDetails = arrayAll
                            }
                        default :
                            break
                        }
                        if self.arrayAlbumDetails.count == 0
                        {
                            WebserviceManager.showAlert(message: "", title: self.intAPIType == 2 ? "No Videos Available" : "No Photos Available", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                self.navigationController?.popViewController(animated: true)
                            }, secondBtnBlock: {})
                        }
                        else
                        {
                            if let strAlbumName = result["Title"] as? String{
                                self.strAlbumTitle = strAlbumName
                                self.lblTitle.text    = strAlbumName
                            }
                            if let strDescription = result["Description"] as? String
                            {
                                self.strAlbumDescription = strDescription
                            }
                            if let strPhotos = result["EventDate"] as? String
                            {
                                /****************/
                                var strDate         = String()
                                var intTotalPhotos  = Int()
                                
                                switch self.intAPIType
                                {
                                case 1:
                                    if let  intTotalPhotos1  = result["TotalPhotos"] as? Int{
                                        intTotalPhotos = intTotalPhotos1
                                    }
                                case 2:
                                    if let  intTotalPhotos1  = result["TotalVideos"] as? Int{
                                        intTotalPhotos = intTotalPhotos1
                                    }
                                case 3:
                                    print("My Favorite)")
                                default :
                                    break
                                }
                                
                                strDate   = strPhotos
                                
                                let formatter           = ISO8601DateFormatter()
                                formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
                                let date1               = formatter.date(from: strDate)
                                strDate                 = formatter.string(from: date1!)
                                
                                var strDay = String()
                                strDay = self.daySuffix(from: date1!)
                                //print("strDay = \(strDay)")
                                
                                let dateFormatterGet        = DateFormatter()
                                dateFormatterGet.dateFormat = "yyyy-MM-dd"
                                let dateFormatter           = DateFormatter()
                                dateFormatter.dateFormat    = "MMM, yyyy"
                                let date: Date? = dateFormatterGet.date(from: strDate)
                                //cellCollection.lblTotalDatePhotos.text = dateFormatter.string(from: date!)
                                switch self.intAPIType
                                {
                                case 1:
                                    self.strAlbumEventDate = intTotalPhotos == 1 ? "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) Photo" : "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) Photos"
                                case 2:
                                    self.strAlbumEventDate = intTotalPhotos == 1 ? "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) Video" : "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) Videos"
                                case 3:
                                    print("My Favorite)")
                                default :
                                    break
                                }
                                /****************/
                            }
                            // print("arrayGallery = \(self.arrayAlbumDetails)")
                            //print("arrayGallery count = \(self.arrayAlbumDetails.count)")
                            
                            self.collectionViewDetails?.delegate   = self
                            self.collectionViewDetails?.dataSource = self
                            self.collectionViewDetails?.isHidden   = false
                            self.collectionViewDetails?.reloadData()
                        }
                    }
                }
                else{
                    Utility.shared.removeLoader()
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
       
        if kind == UICollectionView.elementKindSectionFooter {
            
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
            
        } else
        {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderViewDetails", for: indexPath) as! DetailsCollectionReusableView
            headerView.lblAlbumName?.text    = self.strAlbumTitle.trimmed()
            headerView.lblAlbumDetails?.text = self.strAlbumDescription.trimmed()

            if boolCheck
            {
               
                if (headerView.lblAlbumDetails?.countLabelLines())! > 3{
                    headerView.btnMore?.isHidden              = false
                    headerView.lblAlbumDetails?.numberOfLines = 2
                }
                else{
                    headerView.btnMore?.isHidden              = true
                    headerView.lblAlbumDetails?.numberOfLines = 0
                    
                }
                headerView.lblDateAndPhotos?.text = self.strAlbumEventDate
            }
            else{
                headerView.btnMore?.isHidden              = true
                headerView.lblAlbumDetails?.numberOfLines = 0
                headerView.lblAlbumDetails?.lineBreakMode = .byWordWrapping
                headerView.lblDateAndPhotos?.text = self.strAlbumEventDate
            }
            
            
            btnRef           = headerView.btnMore!
            lblRef           = headerView.lblAlbumDetails!
            heightBtnMoreRef = headerView.heightBtnMore
            
            lblTitle.text    = headerView.lblAlbumName?.text
            return headerView
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath)
    {
        //print("Func: willDisplaySupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        // print("Func: didEndDisplayingSupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    // MARK: - UIScrollViewDelegate
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // print("scrollViewDidScroll")
        
        var scrollPos = CGFloat()
        scrollPos = (self.collectionViewDetails?.contentOffset.y)!
        
        if(scrollPos > currentOffset )
        {
            //print("scrollPos > currentOffset with scrollPos \n \(scrollPos)")
            
            if(currentOffset < 0){
                navigationController?.setNavigationBarHidden(false, animated: true)
                collectionViewDetails?.layer.mask = nil
            }else{
                //Fully hide your toolbar
                updateGradientFrame()
                UIView.animate(withDuration: 0.5, delay: 0.3, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                    self.navigationController?.setNavigationBarHidden(true, animated: true)
                }, completion: nil)
                
            }
        } else {
            //Slide it up incrementally, etc.
            
            if(scrollPos <= 0){
                
                navigationController?.setNavigationBarHidden(false, animated: true)
                collectionViewDetails?.layer.mask = nil
            }else{
                
                updateGradientFrame()
                navigationController?.setNavigationBarHidden(true, animated: true)
            }
            
        }
        
        let threshold         = 100.0 ;
        let contentOffset     = scrollView.contentOffset.y;
        let contentHeight     = scrollView.contentSize.height;
        let diffHeight        = contentHeight - contentOffset;
        let frameHeight       = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold      =  min(triggerThreshold, 0.0)
        let pullRatio         = min(fabs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.footerView?.animateFinal()
        }
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        //print("pullHeight:\(pullHeight)");
        if pullHeight == 0.0
        {
            // if (self.footerView?.isAnimatingFinal)! {
            
            self.isLoading = true
            self.footerView?.startAnimate()
            
            intPage = intPage + 1
            self.loadChunkDataAPIAlbum(intPagination: intPage)
            //}
        }
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        // scrollView    = self.collectionViewDetails;
        currentOffset = (self.collectionViewDetails?.contentOffset.y)!
        
        //print("currentOffset = \(currentOffset)")
        
    }
    //MARK:- loadChunkDataAPINotice
    func loadChunkDataAPIAlbum(intPagination: Int)
    {
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        var alertMessage = String()
        switch intAPIType
        {
        case 1:
            urlString += "gallery/PhotoDTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&AlID=\(intAlId)&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"
            alertMessage = "No more Photos"
        case 2:
            urlString += "gallery/VideoDTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&VAID=\(intAlId)&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"
            alertMessage = "No more Videos"
        case 3:
            //print("My Favorite)")
            alertMessage = "No more Favourites"
        //urlString += "gallery/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPage)"
        default :
            break
        }
        print(urlString)
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            var tempArray = [[String : Any]]()
                            switch self.intAPIType
                            {
                            case 1:
                                if let tempArray1 = (result["Photos"] as? [[String: Any]]){
                                    tempArray = tempArray1
                                }
                            case 2:
                                if let tempArray1 = (result["Videos"] as? [[String: Any]]){
                                    tempArray = tempArray1
                                }
                            default :
                                break
                            }
                            
                            if tempArray.count == 0
                            {
                                self.isLoading = false
                                self.footerView?.stopAnimate()
                                self.footerView?.isHidden = true
                                
                                let alert = UIAlertController(title: "", message: "\(alertMessage)", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                switch self.intAPIType
                                {
                                case 1:
                                    self.arrayAlbumDetails.append(contentsOf: results?["Photos"] as! [[String : Any]])
                                    
                                case 2:
                                    self.arrayAlbumDetails.append(contentsOf: results?["Videos"] as! [[String : Any]])
                                    
                                default :
                                    break
                                }
                                
                                self.collectionViewDetails?.reloadData()
                                self.isLoading = false
                            }
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async
                            {
                                // self.spinner.stopAnimating()
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        
        // that -16 is because I have 8px for left and right spacing constraints for the label.
        let lblAlbumName:UILabel   = UILabel(frame: CGRect( x: 0, y: 0, width:collectionView.frame.width - 20, height: CGFloat.greatestFiniteMagnitude))
        lblAlbumName.numberOfLines = 0
        lblAlbumName.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblAlbumName.font          = R.font.ubuntuMedium(size: 17.0)
        lblAlbumName.text          = self.strAlbumTitle
        lblAlbumName.sizeToFit()
        
        let lblDateAndPhotos:UILabel   = UILabel(frame: CGRect( x: 0, y: 0, width:collectionView.frame.width - 20, height: CGFloat.greatestFiniteMagnitude))
        lblDateAndPhotos.numberOfLines = 0
        lblDateAndPhotos.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDateAndPhotos.font          = R.font.ubuntuLight(size: 12.0)
        lblDateAndPhotos.text          = self.strAlbumEventDate
        lblDateAndPhotos.sizeToFit()
        
        let lblAlbumDetails:UILabel   = UILabel(frame: CGRect( x: 0, y: 0, width:collectionView.frame.width - 20, height: CGFloat.greatestFiniteMagnitude))
        lblAlbumDetails.numberOfLines = 0
        lblAlbumDetails.lineBreakMode = NSLineBreakMode.byTruncatingTail
        lblAlbumDetails.font          = R.font.ubuntuRegular(size: 14.0)
        lblAlbumDetails.text          = self.strAlbumDescription
        lblAlbumDetails.sizeToFit()
        
       // print("lblAlbumDetails.text  :",lblAlbumDetails.text)
        // lblAlbumDetails.text = self.strAlbumEventDate
        var height = CGFloat()
        
        if(lblAlbumDetails.countLabelLines() == 3){
            height = 40
        }else{
            height = CGFloat(lblAlbumDetails.countLabelLines() * 16) + 10;
        }
         print("referenceSizeForHeaderInSection lblAlbumDetails.countLabelLines() = \(lblAlbumDetails.countLabelLines())")
        lblDescriptionHeight = height
        print(" lbl height = \(lblAlbumDetails.frame.height)")
        print("height = \(height)")
        if boolCheck == true
        {
            if lblAlbumDetails.countLabelLines() > 2{
                lblAlbumDetails.numberOfLines = 2
            }
            else{
                lblAlbumDetails.numberOfLines = 0
            }
            //Labl descr height corrected 29/1/18
            return CGSize(width: collectionView.frame.width, height: lblAlbumName.frame.height + lblDateAndPhotos.frame.height + 48 + 50)
        }
        else
        {
            return CGSize(width: collectionView.frame.width, height: lblAlbumName.frame.height + lblDateAndPhotos.frame.height + lblAlbumDetails.frame.height + CGFloat(height))
            
        }
        //This value should be the sum of the vertical spacing you set in the autolayout constraints for the label. + 16 worked for me as I have 8px for top and bottom constraints.
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "\(dayOfMonth)st"
        case 2, 22:     return "\(dayOfMonth)nd"
        case 3, 23:     return "\(dayOfMonth)rd"
        default:        return "\(dayOfMonth)th"
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0{
            if arrayAlbumDetails.count > 0{
                return arrayAlbumDetails.count
            }
            else{
                return 0
            }
        }
        else  if section == 1{
            return 0
        }
        else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // print("Func: cellForItemAt = ")
        
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAlbumDetails", for: indexPath) as! customDetailsGalleryCollectionViewCell
        
        var strPhotoURL     = String()
        
        switch self.intAPIType
        {
        case 1:
            cellCollection.imgThumb?.isHidden = true
            if self.awsURL?.count == 0 {
                strPhotoURL   += strPhotoURL.imgPath1()
            }
            else {
                strPhotoURL = /awsURL
            }
            strPhotoURL   += "/SchImg/"
            strPhotoURL   += self.strSchoolCodeSender
            strPhotoURL   += "/PhotoAlbum/Thumb/"
            if let  strPhotoURL1  = arrayAlbumDetails[indexPath.row]["FileName"] as? String{
                strPhotoURL   += strPhotoURL1
            }
        case 2:
            cellCollection.imgThumb?.isHidden = false
            
            if let  strPhotoURL1  = arrayAlbumDetails[indexPath.row]["URL"] as? String
            {
                var strVideoID = String()
                if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                      strVideoID = value
                      strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
                }
              
            }
        case 3:
            print("My Favorite)")
        default :
            break
        }
        
        if strPhotoURL.count != 0 {
        let finalUrl    = "\(strPhotoURL)"
        let urlPhoto    = URL(string: finalUrl)
        //print("urlPhoto = \(String(describing: urlPhoto))")
            if let urlPhoto = urlPhoto {
                cellCollection.imgView?.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                }
            }
         }
        else {
          cellCollection.imgView?.image = R.image.photogalleryPlaceholder()
        }
        return cellCollection
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // print("Func: didSelectItemAt = \(indexPath.row)")
        intIndexNumber = indexPath.row
        passContentOffSet = indexPath
        navigateToMediaSlider()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 4 - 1
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    @IBAction func ClkMore(_ sender: UIButton){
        
        if boolCheck{
            lblRef.numberOfLines      = 0
            btnRef.isHidden           = true
            sender.isHidden           = true
            heightBtnMoreRef.constant = 0
            boolCheck                 = false
            
            let indexSet = IndexSet(integer: 0)
           self.collectionViewDetails?.reloadSections(indexSet)
        }
    }
    func navigateToMediaSlider() {
        
        switch self.intAPIType {
        case 1:
            guard let vc = R.storyboard.main.photoSliderViewController() else {
            return
            }
            vc.imgArray            = arrayAlbumDetails
            vc.awsURL              = self.awsURL
            vc.passedContextOffset = passContentOffSet
            vc.intAPIType          = intAPIType
            self.pushVC(vc)
            
        case 2:
            guard let vc = R.storyboard.main.videoSliderViewController() else {
            return
            }
            vc.imgArray            = arrayAlbumDetails
            vc.passedContextOffset = passContentOffSet
            vc.intAPIType          = intAPIType
            self.pushVC(vc)
            
        case 3 :
            guard let vc = R.storyboard.main.videoSliderViewController() else {
                return
            }
            vc.imgArray            = arrayAlbumDetails
            vc.passedContextOffset = passContentOffSet
            vc.intAPIType          = intAPIType
            self.pushVC(vc)
        default:
            break
        }
    }
}
