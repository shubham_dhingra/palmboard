//  FavoriteGalleryViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 27/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.


import UIKit


class FavoriteGalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewAlbums: UICollectionView?
    
    var arrayPhotosGallery = [[String: Any]]()
    var btnTitle           =  UIButton()
    var intAlId            = Int()
    var intPage            = Int()
    var intAPIType         = Int()
    var strSchoolCodeSender = String()
    var strVideoURL        = String()
    var isLoading:Bool     = false
    var footerView:CustomFooterView?
    var intUserID          = Int()
    var intUserType        = Int()
    var passContentOffSet  = IndexPath()
    var awsURL : String?

    let footerViewReuseIdentifier = "RefreshFooterView"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionViewAlbums?.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 90, height: 40)
        btnTitle.setTitle("My Favourites", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)

        self.navigationItem.titleView = btnTitle
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewAlbums?.setContentOffset(CGPoint(x:0,y:0), animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        
        intPage = 1
      
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        collectionViewAlbums?.isHidden = true
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        self.webApiGallery()
        
    }
    func webApiGallery()
    {
        Utility.shared.loader()
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        
        urlString += "gallery/MyFavorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"//

        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    if let awsURL = (result["AWS_Path"] as? String){
                        self.awsURL = awsURL
                    }
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()

                        self.collectionViewAlbums?.isHidden = false
                        
                        if let arrayAll = (result["List"] as? [[String: Any]])
                        {
                            self.arrayPhotosGallery = arrayAll
                        }
                        
                        if self.arrayPhotosGallery.count == 0
                        {
                            WebserviceManager.showAlert(message: "", title: "Album not found", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                self.navigationController?.popViewController(animated: true)
                            }, secondBtnBlock: {})
                        }
                        else
                        {
                            print("arrayGallery = \(self.arrayPhotosGallery)")
                            self.collectionViewAlbums?.delegate   = self
                            self.collectionViewAlbums?.dataSource = self
                            self.collectionViewAlbums?.isHidden   = false
                            self.collectionViewAlbums?.reloadData()
                        }
                    }
                }
                else{
                      DispatchQueue.main.async {
                    Utility.shared.removeLoader()
                    }
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if arrayPhotosGallery.count > 0{
            return arrayPhotosGallery.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print("Func: cellForItemAt = ")
        var strPhotoURL = String()
        var intS_MdlID  = Int()
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFavoriteAlbum", for: indexPath) as! CustomFavoriteCollectionViewCell
        
        if let  intS_MdlID1  = arrayPhotosGallery[indexPath.row]["S_MdlID"] as? Int
        {
            intS_MdlID = intS_MdlID1
        }
        
        if self.awsURL?.count == 0 {
            strPhotoURL   += strPhotoURL.imgPath1()
        }
        else {
            strPhotoURL = /self.awsURL
        }
        strPhotoURL   += "/SchImg/"
        strPhotoURL   += self.strSchoolCodeSender
        switch intS_MdlID
        {
        case 1:
            strPhotoURL   += "/PhotoAlbum/Thumb/"
            cellCollection.imgThumb?.isHidden = true
            if let  strPhotoURL1  = arrayPhotosGallery[indexPath.row]["FileName"] as? String{
                strPhotoURL   += strPhotoURL1
            }

        case 2:
            cellCollection.imgThumb?.isHidden = false
            
            if let  strPhotoURL1  = arrayPhotosGallery[indexPath.row]["FileName"] as? String
            {
                var strVideoID = String()
                if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                    strVideoID = value
                    strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
                }
            }
        default :
            break
        }
        if strPhotoURL.count != 0 {
        let finalUrl    = "\(strPhotoURL)"
        let urlPhoto    = URL(string: finalUrl)
      //  print("urlPhoto = \(urlPhoto)")
        
        if let  urlPhoto    = URL(string: finalUrl) {
            cellCollection.imgView?.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
        }
        else {
            cellCollection.imgView?.image = R.image.photogalleryPlaceholder()
        }
        }
        else {
            cellCollection.imgView?.image = R.image.photogalleryPlaceholder()
        }
        return cellCollection
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Func: didSelectItemAt = \(indexPath.row)")
        passContentOffSet = indexPath
        navigateTofavoriteSlider()
//        self.performSegue(withIdentifier: "segueFevoriteSlider", sender: self)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 4 - 1
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    /********************************************************************************************/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath)
    {
        print("Func: willDisplaySupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        print("Func: didEndDisplayingSupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold         = 100.0 ;
        let contentOffset     = scrollView.contentOffset.y;
        let contentHeight     = scrollView.contentSize.height;
        let diffHeight        = contentHeight - contentOffset;
        let frameHeight       = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold      =  min(triggerThreshold, 0.0)
        let pullRatio         = min(fabs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.footerView?.animateFinal()
        }
        print("pullRation:\(pullRatio)")
    }
    
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        print("pullHeight:\(pullHeight)");
        if pullHeight == 0.0
        {
            //if (self.footerView?.isAnimatingFinal)! {
                print("load more trigger")
                self.isLoading = true
                self.footerView?.startAnimate()
                
                intPage = intPage + 1
                self.loadChunkDataAPIAlbum(intPagination: intPage)
           // }
        }
    }
    
    //MARK:- loadChunkDataAPIAlbum
    func loadChunkDataAPIAlbum(intPagination: Int)
    {
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
       // urlString += "gallery/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPagination)"
        urlString += "gallery/MyFavorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"

        print("Func:loadingPastData urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            var tempArray = [[String : Any]]()
                            if let tempArray1 = results?["List"] as? [[String : Any]]
                            {
                                tempArray = tempArray1
                            }
                            if tempArray.count == 0
                            {
                                self.isLoading = false
                                self.footerView?.stopAnimate()
                                self.footerView?.isHidden = true

//                                let alert = UIAlertController(title: "", message: "No more Favourites", preferredStyle: UIAlertControllerStyle.alert)
//                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                
                                print("Before arrayPhotosGallery count = \(self.arrayPhotosGallery.count)")
                                self.arrayPhotosGallery.append(contentsOf: results?["List"] as! [[String : Any]])
                                print("after arrayPhotosGallery count = \(self.arrayPhotosGallery.count)")
                                self.collectionViewAlbums?.reloadData()
                                self.isLoading = false
                            }
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async
                            {
                                // self.spinner.stopAnimating()
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {

        
        if segue.identifier == "segueFevoriteSlider"{
            let seguePotos                  = segue.destination as! FavoriteSliderViewController
            seguePotos.imgArray            = arrayPhotosGallery
            seguePotos.passedContextOffset = passContentOffSet
            seguePotos.intAPIType          = intAPIType
        }


    }
    
    func navigateTofavoriteSlider() {
        guard let vc = R.storyboard.main.favoriteSliderViewController() else {
            return
        }
        vc.imgArray            = arrayPhotosGallery
        vc.passedContextOffset = passContentOffSet
        vc.intAPIType          = intAPIType
        self.pushVC(vc)
    }
}

