//  FavoriteSliderViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 29/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class FavoriteSliderViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var myCollectionView: UICollectionView?
    var imgArray            = [[String:Any]]()
    var passedContextOffset = IndexPath()
    var intAPIType          = Int()
    var strSchoolCodeSender = String()
    // var arrayPhoto  = [[String: Any]]()
    var strVideoURL        = String()
    var strLikesPhotoVideo = String()
    var intPhIdVId         = Int()
    var intUserID          = Int()
    var intUserType        = Int()
    var intGlobalS_MdlID   = Int()
    
    @IBOutlet weak var viewBg:          UIView?
    @IBOutlet weak var btnAddFavourite: UIButton?
    @IBOutlet weak var btnLike:         UIButton?
    @IBOutlet weak var btnAllLikes:     UIButton?
    @IBOutlet weak var btnShare:        UIButton?
    
    let selectedColor   = UIColor(rgb: 0x56B54B)
    let unselectedColor = UIColor(rgb: 0x5E5E5E)
    
    @IBAction func ClkBack(_ sender: Any){
         NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        self.navigationController == nil ? dismissVC(completion: nil) : popVC()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Func: viewDidLoad: FavoriteSliderViewController")
        self.title = "Favorites"
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
       
        
        
        
        print("imgArray = \(imgArray)")
        print("passedContextOffset = \(passedContextOffset)")
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = false
        self.showTabBar()
        
    }
    
    override func viewWillAppear(_ animated: Bool)  {
        
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        self.isNavBarHidden = true
        self.hideTabBar()
        print("Func: viewWillAppear")
        myCollectionView?.delegate   = self
        myCollectionView?.dataSource = self
        myCollectionView?.register(FavoritePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        myCollectionView?.scrollToItem(at: passedContextOffset, at: .left, animated: true)
        self.myCollectionView?.performBatchUpdates({},
                                                   completion: { (finished) in
                                                    print("collection-view finished reload done")
                                                    self.myCollectionView?.scrollToItem(at: self.passedContextOffset, at: .centeredHorizontally, animated: false)
        })
        
        if passedContextOffset == [0,0] {
            updateLikes()
        }
        
    }
    //hide Status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if imgArray.count > 0{
            return imgArray.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print("Func: cellForItemAt = ")
        print("indexPath = \(indexPath)")
        
        let cell        = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! FavoritePreviewFullViewCell
        var strPhotoURL = String()
        
        if let  intS_MdlID  = imgArray[indexPath.row]["S_MdlID"] as? Int
        {
            if intS_MdlID == 1
            {
                cell.scrollImg.isUserInteractionEnabled = true
                
                cell.imgViewPlay.isHidden = true
                strPhotoURL   += strPhotoURL.imgPath1()
                strPhotoURL   += "/SchImg/"
                strPhotoURL   += self.strSchoolCodeSender
                strPhotoURL   += "/PhotoAlbum/Full/"
                if let  strPhotoURL1  = imgArray[indexPath.row]["FileName"] as? String{
                    strPhotoURL      += strPhotoURL1
                }
            }
            else if intS_MdlID == 2
            {
                cell.scrollImg.isUserInteractionEnabled = false
                cell.imgViewPlay.isHidden = false
                
                if let strPhotoURL1  = imgArray[indexPath.row]["FileName"] as? String
                {    var strVideoID = String()
                    if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                        strVideoID = value
                        strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
                    }
                }
            }
        }
        
        if strPhotoURL.count != 0 {
        let finalUrl    = "\(strPhotoURL)"
        let urlPhoto    = URL(string: finalUrl)
        print("urlPhoto = \(String(describing: urlPhoto))")
        
        if let  urlPhoto    = URL(string: finalUrl) {
            cell.imgView.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
        }
        else {
            cell.imgView?.image = R.image.photogalleryPlaceholder()
        }
       }
        else {
            cell.imgView?.image = R.image.photogalleryPlaceholder()
        }
        return cell
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        guard let flowLayout = myCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {return  }
        flowLayout.itemSize  = (myCollectionView?.frame.size)!
        flowLayout.invalidateLayout()
        myCollectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        print("Func: viewWillTransition")
        
        let offSet    = myCollectionView?.contentOffset
        let width     = myCollectionView?.bounds.size.width
        let index     = round((offSet?.x)! / width!)
        let newOffset = CGPoint(x: index * size.width, y: (offSet?.y)!)
        myCollectionView?.setContentOffset(newOffset, animated: true)
        coordinator.animate(alongsideTransition: {(context) in
            self.myCollectionView?.reloadData()
            self.myCollectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Func: didSelectItemAt = \(indexPath.row)")
        if let  intS_MdlID  = imgArray[indexPath.row]["S_MdlID"] as? Int
        {
            if intS_MdlID == 2
            {
                if let  strVideoURL1  = imgArray[indexPath.row]["FileName"] as? String{
                    strVideoURL = strVideoURL1
                    self.performSegue(withIdentifier: "segueFevoritePlaySlider", sender: self)
                }
            }
        }
    }
    @IBAction func ClkBtnFavourite(_ sender: UIButton)
    {
        print("Func: ClkBtnFavourite)")
        var inttag        = Int()
        var intID        = Int()
        var intIsFavorite = Int()
        var intS_MdlID    = Int()
        var urlString     = String()
        urlString        += urlString.webAPIDomainNmae()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intS_MdlID1  = imgArray[inttag]["S_MdlID"] as? Int
        {
            intS_MdlID = intS_MdlID1
            print("intS_MdlID = \(intS_MdlID)")
        }
        if let  intID1  = imgArray[inttag]["ID"] as? Int{
            intID = intID1
            print("intID = \(intID)")
        }
        if btn.hasImage(named: "FavoriteBlank", for: .normal) {
            print("YES")
            intIsFavorite = 1
            
        } else {
            print("NO")
            intIsFavorite = 0
        }
        urlString    += "gallery/Favorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&S_MdlID=\(intS_MdlID)&ID=\(intID)&UserID=\(intUserID)&UserType=\(intUserType)&isAdd=\(intIsFavorite)"
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Added to favorite successfully"
                            {
                                btn.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)//FavoriteFilled
                            }
                            else  if result["Message"] as? String == "Removed from favorite list"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            /************************/
                            self.imgArray[inttag].updateValue(intIsFavorite, forKey: "isFavourite")
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    @IBAction func ClkBtnShare(_ sender: UIButton)
    {
        print("Func: ClkBtnShare)")
        
        var inttag         = Int()
        var strFileName    = String()
        var intS_MdlID     = Int()
        var photoThumbnail = UIImage()
        
        inttag     = sender.tag
        print("inttag = \(inttag)")
        
        if let  intS_MdlID1  = imgArray[inttag]["S_MdlID"] as? Int
        {
            intS_MdlID = intS_MdlID1
            print("intS_MdlID = \(intS_MdlID)")
            
            if intS_MdlID == 1
            {
                var visibleRect    = CGRect()
                visibleRect.origin = (myCollectionView?.contentOffset)!
                visibleRect.size   = (myCollectionView?.bounds.size)!
                let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                let indexPath: IndexPath = (myCollectionView?.indexPathForItem(at: visiblePoint)!)!
                
                let selectedCell = myCollectionView?.cellForItem(at: indexPath) as! FavoritePreviewFullViewCell
                photoThumbnail   = selectedCell.imgView.image!
                print("photoThumbnail= \(photoThumbnail)")
                
                let shareItems:Array = [photoThumbnail] as [Any]//img,strShareCopyThought
                let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                self.present(activityViewController, animated: true, completion: nil)
            }
            else if intS_MdlID == 2
            {
                if let  strShareVideo1  = imgArray[inttag]["FileName"] as? String{
                    strFileName = strShareVideo1
                }
                print("strFileName= \(strFileName)")
                let shareItems:Array = [strFileName] as [Any]//img,strShareCopyThought
                let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    @IBAction func ClkBtnLike(_ sender: UIButton)
    {
        print("Func: ClkBtnLike)")
        
        var inttag        = Int()
        var intID        = Int()
        var intIsLike     = Int()
        var intLikes      = Int()
        var urlString     = String()
        urlString         += urlString.webAPIDomainNmae()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intID1  = imgArray[inttag]["ID"] as? Int{
            intID = intID1
            print("intID = \(intID)")
        }
        
        if btn.hasImage(named: "like", for: .normal) {
            print("YES")
            intIsLike = 1
            
        } else {
            print("NO")
            intIsLike = 0
        }
        if let intS_MdlID1Likes1 = imgArray[inttag]["S_MdlID"] as? Int{
            if intS_MdlID1Likes1 == 1
            {
                urlString    += "like/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&Phid=\(intID)&isLike=\(intIsLike)"
            }
            else if intS_MdlID1Likes1 == 2
            {
                urlString    += "like/Video?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&Vid=\(intID)&isLike=\(intIsLike)"
            }
        }
        
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Liked"
                            {
                                btn.setImage(R.image.liked(), for: UIControl.State.normal)
                                btn.setTitleColor(self.selectedColor, for: .normal)
                            }
                            else  if result["Message"] as? String == "Unliked"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.like(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            
                            intLikes = (results!["TotalLikes"] as? Int)!
                            print("intLikes = \(intLikes)")
                            self.btnAllLikes?.isHidden = false
                            if (intLikes > 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
                            }
                            else if (intLikes == 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
                            }
                            else  {
                                self.btnAllLikes?.setTitle("Like",for: .normal)

                                // self.btnTotalLikes.setTitle("Like",for: .normal)
                                //  self.btnAllLikes?.isHidden = true
                            }
                            /************************/
                            self.imgArray[inttag].updateValue(intIsLike, forKey: "islLike")
                            self.imgArray[inttag].updateValue(intLikes, forKey: "TotalLike")
                            
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    
    func updateLikes(){
        
        var visibleRect    = CGRect()
        visibleRect.origin = (myCollectionView?.contentOffset)!
        visibleRect.size   = (myCollectionView?.bounds.size)!
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        var indexPath = IndexPath()
        if  let indexPath1: IndexPath = myCollectionView?.indexPathForItem(at: visiblePoint) {
            indexPath =  indexPath1
        }
        else
        {
            indexPath = [0,0]
            print("else part \(indexPath)")
        }
        
        
        btnAddFavourite?.tag = indexPath.row
        btnLike?.tag         = indexPath.row
        btnAllLikes?.tag     = indexPath.row
        btnShare?.tag        = indexPath.row
        
        print("indexPath.row= \(indexPath.row)")
        
        var intLikes       = Int()
        var intIsLike      = Int()
        var intIsFavourite = Int()
        
        if let intLikes1 = imgArray[indexPath.row]["TotalLike"] as? Int
        {
            intLikes = intLikes1
            
            btnAllLikes?.isHidden = false
            if intLikes > 1{
                btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
            }
            else  if intLikes == 1{
                btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
            }
            else  if intLikes == 0{
                //btnAllLikes.setTitle("\(intLikes) Answer",for: .normal)
                 btnAllLikes?.setTitle("Like",for: .normal)
//                btnAllLikes?.isHidden = true
            }
        }
        if let intisLike1 = imgArray[indexPath.row]["islLike"] as? Int
        {
            intIsLike = intisLike1
            switch intIsLike
            {
            case 0:
                btnLike?.setImage(R.image.like(), for: UIControl.State.normal)
                btnLike?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnLike?.setImage(R.image.liked(), for: UIControl.State.normal)
                btnLike?.setTitleColor(selectedColor, for: .normal)
            default:
                break
            }
            
        }
        if let intisFavourite1 = imgArray[indexPath.row]["isFavourite"] as? Int
        {
            intIsFavourite = intisFavourite1
            print("intIsFavourite = \(intIsFavourite)")
            switch intIsFavourite
            {
            case 0:
                btnAddFavourite?.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnAddFavourite?.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            default:
                break
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        self.updateLikes()
    }
    @IBAction func ClkBtnAllLikes(_ sender: UIButton)
    {
        var inttag = Int()
        inttag = sender.tag
        
        if let intPhid1 = imgArray[inttag]["ID"] as? Int{
            intPhIdVId = intPhid1
        }
        if let strLikesPhoto1 = imgArray[inttag]["TotalLike"] as? Int{
            strLikesPhotoVideo = "\(strLikesPhoto1)"
        }
        if let intGlobalS_MdlID1 = imgArray[inttag]["S_MdlID"] as? Int{
            intGlobalS_MdlID = intGlobalS_MdlID1
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let likesAllVc = storyboard.instantiateViewController(withIdentifier: "LikesAllUserViewController") as? LikesAllUserViewController else {return}
        likesAllVc.strTotalLikes          = strLikesPhotoVideo
        if intGlobalS_MdlID == 1{
            likesAllVc.intPhid  = intPhIdVId
            intGlobalS_MdlID = 4
        }
        else{
            likesAllVc.intVId    = intPhIdVId
            intGlobalS_MdlID = 5
        }
        likesAllVc.intWordThoughtQuestion = intGlobalS_MdlID
         self.pushVC(likesAllVc)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "segueFevoritePlaySlider")
        {
            let seguePotos         = segue.destination as! VideoPlayerViewController
           // seguePotos.strVideoURL = strVideoURL
            if let videoID = strVideoURL.components(separatedBy: "/").last {
                seguePotos.strVideoURL = strVideoURL
                seguePotos.videoIdString = videoID
                
            }else{
                seguePotos.strVideoURL = ""
                seguePotos.videoIdString = ""
            }
        }
    }
}

class FavoritePreviewFullViewCell: UICollectionViewCell,UIScrollViewDelegate
{
    var scrollImg:   UIScrollView!
    var imgView:     UIImageView!
    var imgViewPlay: UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        print("Func: init")
        print("frame = \(frame)")
        
        scrollImg                              = UIScrollView()
        scrollImg.delegate                     = self
        scrollImg.alwaysBounceVertical         = false
        scrollImg.alwaysBounceHorizontal       = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 4.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollImg.addGestureRecognizer(doubleTapGest)
        
        self.addSubview(scrollImg)
        
        imgView                 = UIImageView()
        imgView.image           = UIImage(named: "user3")
        imgView.contentMode     = .scaleAspectFit
        
        imgViewPlay             = UIImageView()
        imgViewPlay.image       = UIImage(named: "playThumb")
        imgViewPlay.contentMode = .scaleToFill
        imgViewPlay.center      = imgView.center
        
        scrollImg.addSubview(imgView!)
        imgView.addSubview(imgViewPlay)
    }
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if scrollImg.zoomScale == 1 {
            scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollImg.setZoomScale(1, animated: true)
        }
    }
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect         = CGRect.zero
        zoomRect.size.height = imgView.frame.size.height / scale
        zoomRect.size.width  = imgView.frame.size.width  / scale
        let newCenter        = imgView.convert(center, from: scrollImg)
        zoomRect.origin.x    = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y    = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        print("Func: viewForZooming")
        return self.imgView
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        print("Func: layoutSubviews")
        
        scrollImg.frame = self.bounds
        imgView.frame   = self.bounds
        imgViewPlay.frame       = CGRect(x: imgView.frame.size.width / 2 - 15, y: imgView.frame.size.height / 2 - 15 , width: 30, height: 30)
        print("scrollImg.frame = \(scrollImg.frame)")
        print("imgView.frame = \(imgView.frame)")
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollImg.setZoomScale(1, animated: true)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

