//  GalleryViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class GalleryViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionViewGallery: UICollectionView?
    
    @IBOutlet weak var imgViewNoGallery: UIImageView?
    @IBOutlet weak var lblNoGallery: UILabel?
    
    var btnTitle     =  UIButton()
    var btnSearch    =  UIButton()
    
    var arrayGallery = [[String: Any]]()
    var strSchoolCodeSender     = String()    
    var intUserID               = Int()
    var intUserType             = Int()
    var intAPIType              = Int()
    var strTitle                = String()
    var strAWSURL               : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewGallery?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    @objc func ClkBtnSearch(sender:UIButton){
        guard let vc = R.storyboard.main.searchGalleryViewController() else {
            return
        }
        self.pushVC(vc)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        //let img = UIImage()
        //self.navigationController?.navigationBar.shadowImage = img
        //self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesBackButton = true
        
        print("Gallery viewwillapppear")
        imgViewNoGallery?.isHidden = true
        lblNoGallery?.isHidden     = true
        
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.tabCollectionView?.reloadData()
        parent.hideBar(boolHide: false)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        btnTitle                =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 60, height: 40)
        btnTitle.setTitle("Gallery", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        btnSearch                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
        btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
        btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        collectionViewGallery?.isHidden = true
        self.webApiGallery()
        
    }
    func webApiGallery()
    {
        Utility.shared.loader()
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Gallery/Dtl?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)"
        
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("Result == \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        self.collectionViewGallery?.isHidden = false
                        
                        if let arrayAll = (result["Modules"] as? [[String: Any]]){
                            self.arrayGallery = arrayAll
                        }
                        if let strAWS_Path = (result["AWS_Path"] as? String){
                            self.strAWSURL = strAWS_Path
                        }
                        if self.arrayGallery.count == 0
                        {
                            self.imgViewNoGallery?.isHidden = false
                            self.lblNoGallery?.isHidden     = false
                            //                            WebserviceManager.showAlert(message: "", title: "Gallery is not found", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                            //                                self.navigationController?.popViewController(animated: true)
                            //                            }, secondBtnBlock: {})
                        }
                        else
                        {
                            print("arrayGallery = \(self.arrayGallery)")
                            self.collectionViewGallery?.delegate   = self
                            self.collectionViewGallery?.dataSource = self
                            self.collectionViewGallery?.isHidden   = false
                            self.collectionViewGallery?.reloadData()
                        }
                    }
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrayGallery.count > 0 ? arrayGallery.count : 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print("Func: cellForItemAt = ")
        
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellGallery", for: indexPath) as! customGalleryCollectionViewCell
        
        var intItem      = Int()
        var intAlbum     = Int()
        var strSubModule = String()
        var strPhotoURL  = String()
        var strS_MdlID   = Int()
        var strCountMsg  = String()
        
        if let  S_MdlID  = arrayGallery[indexPath.row]["S_MdlID"] as? Int{
            strS_MdlID =  S_MdlID
        }
        
        if let  intItem1  = arrayGallery[indexPath.row]["Item"] as? Int{
            intItem =  intItem1
        }
        if let  intAlbum1  = arrayGallery[indexPath.row]["Album"] as? Int{
            intAlbum = intAlbum1
        }
        if let  strSubModule1  = arrayGallery[indexPath.row]["SubModule"] as? String{
            strSubModule = strSubModule1
            cellCollection.lblModuleName?.text = strSubModule1
        }
        if strS_MdlID == 2 || strS_MdlID == 1
        {
            cellCollection.lblTotalAlbum?.text       = "\(intAlbum) Albums  |  \(intItem) \(strSubModule)"
            cellCollection.favouriteWidth?.constant  = 0
            cellCollection.favouriteHeight?.constant = 0
        }
        else{
            if let  strCountMsg  = arrayGallery[indexPath.row]["CountMsg"] as? String{
                cellCollection.lblTotalAlbum?.text       = "\(strCountMsg)"
            }
            else{
                cellCollection.lblTotalAlbum?.text       = "\(strCountMsg)"
            }
            cellCollection.favouriteWidth?.constant  = 12.51
            cellCollection.favouriteHeight?.constant = 11.85
        }
        
        if let  strPhotoURL1 = arrayGallery[indexPath.row]["IconImg"] as? String{
            strPhotoURL      = strPhotoURL1
        }
        
        var strPrefix   = String()
        strPrefix       += strPrefix.imgPath1()
        let finalUrl    = strAWSURL?.count != 0 ? "\(/strAWSURL)\(strPhotoURL)" : ("\(strPrefix)" +  "\(strPhotoURL)")
        
        if let  urlPhoto    = URL(string: /finalUrl) {
            cellCollection.imgView?.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
        }
        return cellCollection
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let  intAPIType1  = arrayGallery[indexPath.row]["S_MdlID"] as? Int {
            intAPIType =  intAPIType1
            
            switch intAPIType{
            case 1,2:
                navigateToController(title : intAPIType1 == 1 ? R.string.localize.photoAlbums() : R.string.localize.videoAlbums() , intAPIType : intAPIType)
            case 3:
                navigateToController(title : nil , intAPIType : intAPIType)
            default :
                break
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2 - 10
        return CGSize(width: width, height: 210)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "seguePhotoAlbum"
        {
            let seguePotos        = segue.destination as! PhotoAlbumsViewController
            seguePotos.intAPIType = intAPIType
            seguePotos.strTitle   = strTitle
        }
    }
    func navigateToController(title : String? , intAPIType : Int) {
        
        if title == nil {
            guard let vc = R.storyboard.main.favoriteGalleryViewController() else {
                return
            }
            vc.intAPIType = intAPIType
            self.pushVC(vc)
        }
        else {
            guard let vc = R.storyboard.main.photoAlbumsViewController() else {
                return
            }
            vc.strTitle = /title
            vc.intAPIType = intAPIType
            self.pushVC(vc)
        }
    }
}
