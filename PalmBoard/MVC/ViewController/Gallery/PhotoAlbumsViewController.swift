//  PhotoAlbumsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class PhotoAlbumsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewAlbums: UICollectionView?
    var strTitle                = String()
    
    var arrayPhotosGallery = [[String: Any]]()
    var btnTitle           =  UIButton()
    var intAlId            = Int()
    var intPage            = Int()
    var intAPIType         = Int()
    var strSchoolCodeSender = String()
    var isLoading:Bool     = false
    var footerView:CustomFooterView?
    var intUserID          = Int()
    var intUserType        = Int()
    var alertMsg           =  String()
    var strAWSURL               : String?

    let footerViewReuseIdentifier = "RefreshFooterView"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        
        switch intAPIType
        {
        case 1:
            strTitle = "Photo Albums"
        case 2:
            strTitle = "Video Albums"
        case 3:
            strTitle = "My Favorites"
        default :
            break
        }
        
        self.collectionViewAlbums?.register(UINib(nibName: "CustomFooterView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerViewReuseIdentifier)
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle(strTitle, for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        intPage = 1
        
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
        collectionViewAlbums?.isHidden = true
        self.webApiGallery()
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewAlbums?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
//        parent.tabCollectionView?.reloadData()
      NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
    }
    
    func webApiGallery()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        switch intAPIType{
        case 1:
            urlString += "gallery/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPage)"
        case 2:
            urlString += "gallery/Video?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPage)"
        case 3:
            urlString += "gallery/MyFavorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPage)"
        default :
            break
        }
        
        print("PhotoAlbumsViewController urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
           
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("Result == \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        self.collectionViewAlbums?.isHidden = false
                        
                        if let arrayAll = (result["Album"] as? [[String: Any]]){
                            self.arrayPhotosGallery = arrayAll
                        }
                        if let strAWS_Path = (result["AWS_Path"] as? String){
                            self.strAWSURL = strAWS_Path
                        }
                        if self.arrayPhotosGallery.count == 0
                        {
                            WebserviceManager.showAlert(message: "", title: "Album not found", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                self.navigationController?.popViewController(animated: true)
                            }, secondBtnBlock: {})
                        }
                        else
                        {
                            self.collectionViewAlbums?.delegate   = self
                            self.collectionViewAlbums?.dataSource = self
                            self.collectionViewAlbums?.isHidden   = false
                            self.collectionViewAlbums?.reloadData()
                        }
                    }
                }
                else{
                     Utility.shared.removeLoader()
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        //print("numberOfItemsInSection \n arrayPhotosGallery.count = \(arrayPhotosGallery.count)")
        if arrayPhotosGallery.count > 0{
            return arrayPhotosGallery.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAlbum", for: indexPath) as! CustomAlbumCollectionViewCell
        
        var intTotalPhotos  = Int()
        var strPhotoURL     = String()
        
        if /strAWSURL?.count != 0 {
            strPhotoURL = /strAWSURL
        }
        else {
           strPhotoURL   += strPhotoURL.imgPath1()
        }

        strPhotoURL   += "/SchImg/"
        strPhotoURL   += self.strSchoolCodeSender
        switch intAPIType
        {
        case 1:
            strPhotoURL   += "/PhotoAlbum/Thumb/"
            alertMsg       = "No more Photo Albums"
        case 2:
            strPhotoURL   += "/videoImg/"
            alertMsg       = "No more Video Albums"
        case 3:
            print("My Favorite)")
            alertMsg       = "No more Photo Albums"
        default :
            break
        }
        
        if let  strSubModule1  = arrayPhotosGallery[indexPath.row]["Title"] as? String{
            
            cellCollection.lblAlbumName?.text = strSubModule1
        }
        
        if let  strPhotoURL1  = arrayPhotosGallery[indexPath.row]["FileName"] as? String{
            strPhotoURL   += strPhotoURL1
        }
        let finalUrl    = "\(strPhotoURL)"
        if let  urlPhoto    = URL(string: finalUrl) {
            cellCollection.imgView?.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
      }
        else {
            cellCollection.imgView?.image = R.image.photogalleryPlaceholder()
        }
        
        /****************/
        
        var strDate           = String()
        switch intAPIType
        {
        case 1:
            if let  intTotalPhotos1  = arrayPhotosGallery[indexPath.row]["TotalPhotos"] as? Int{
                intTotalPhotos = intTotalPhotos1
            }
        case 2:
            if let  intTotalPhotos1  = arrayPhotosGallery[indexPath.row]["TotalVideos"] as? Int{
                intTotalPhotos = intTotalPhotos1
            }
        case 3:
            print("My Favorite)")
        default :
            break
        }
        
        if let  strUpdatedOn  = arrayPhotosGallery[indexPath.row]["EventDate"] as? String{
            strDate   = strUpdatedOn
        }
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions  = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: strDate)
        strDate                 = formatter.string(from: date1!)
        
        var strDay = String()
        strDay = self.daySuffix(from: date1!)
        //print("strDay = \(strDay)")
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "MMM, yyyy"
        let date: Date? = dateFormatterGet.date(from: strDate)
        // cellCollection.lblTotalDatePhotos.text = dateFormatter.string(from: date!)
        switch intAPIType
        {
        case 1:
            let msg = intTotalPhotos == 1 ? "Photo" : "Photos"
            cellCollection.lblTotalDatePhotos?.text = "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) \(msg)"
        case 2:
              let msg = intTotalPhotos == 1 ? "Video" : "Videos"
            cellCollection.lblTotalDatePhotos?.text = "\(strDay) \(dateFormatter.string(from: date!)) | \(intTotalPhotos) \(msg)"
        default :
            break
        }
        
        /****************/
        return cellCollection
    }
    
    func daySuffix(from date: Date) -> String {
        
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "\(dayOfMonth)st"
        case 2, 22:     return "\(dayOfMonth)nd"
        case 3, 23:     return "\(dayOfMonth)rd"
        default:        return "\(dayOfMonth)th"
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        // print("Func: didSelectItemAt = \(indexPath.row)")
        
        switch intAPIType
        {
        case 1:
            if let  intAlId1  = arrayPhotosGallery[indexPath.row]["AlID"] as? Int
            {
                intAlId = intAlId1
            }
        case 2:
            if let  intAlId1  = arrayPhotosGallery[indexPath.row]["VAID"] as? Int
            {
                intAlId = intAlId1
            }
        case 3:
            print("My Favorite)")
        default :
            break
        }
        navigateToPhotoAlbumsDetails()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2 - 10
        return CGSize(width: width, height: 224)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    /********************************************************************************************/
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoading {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    
  
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath)
    {
        //print("Func: willDisplaySupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.prepareInitialAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        //print("Func: didEndDisplayingSupplementaryView")
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    //compute the scroll value and play witht the threshold to get desired effect
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold         = 100.0 ;
        let contentOffset     = scrollView.contentOffset.y;
        let contentHeight     = scrollView.contentSize.height;
        let diffHeight        = contentHeight - contentOffset;
        let frameHeight       = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold      =  min(triggerThreshold, 0.0)
        let pullRatio         = min(fabs(triggerThreshold),1.0);
        self.footerView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.footerView?.animateFinal()
        }
        
    }
    //compute the offset and call the load method
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = fabs(diffHeight - frameHeight);
        //print("pullHeight:\(pullHeight)");
        if pullHeight == 0.0
        {
            //  if (self.footerView?.isAnimatingFinal)! {
            
            self.isLoading = true
            self.footerView?.startAnimate()
            
            intPage = intPage + 1
            self.loadChunkDataAPIAlbum(intPagination: intPage)
            //  }
        }
    }
    //MARK:- loadChunkDataAPINotice
    func loadChunkDataAPIAlbum(intPagination: Int)
    {
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        switch intAPIType
        {
        case 1:
            urlString += "gallery/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPagination)"
        case 2:
            urlString += "gallery/Video?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&pg=\(intPagination)"
        case 3:
            urlString += "gallery/MyFavorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"
        default :
            break
        }
        
        //print("Func:loadingPastData urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            var tempArray = [[String : Any]]()
                            if let tempArray1 = results?["Album"] as? [[String : Any]]
                            {
                                tempArray = tempArray1
                            }
                            if tempArray.count == 0
                            {
                                self.isLoading = false
                                self.footerView?.stopAnimate()
                                self.footerView?.isHidden = true
                                
                                let alert = UIAlertController(title: "", message: "\(self.alertMsg)", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.arrayPhotosGallery.append(contentsOf: results?["Album"] as! [[String : Any]])
                                self.collectionViewAlbums?.reloadData()
                                self.isLoading = false
                            }
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async
                            {
                                // self.spinner.stopAnimating()
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    func navigateToPhotoAlbumsDetails() {
        guard let vc = R.storyboard.main.detailsPhotoAlbumsViewController() else {
            return
        }
        vc.intAPIType = self.intAPIType
        vc.strTitle   = self.intAPIType == 1 ? "Photos" : "Videos"
        vc.intAlId = intAlId
        vc.fromBoardScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

