//  PhotoSliderViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.


import UIKit
import EZSwiftExtensions

class PhotoSliderViewController:UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var myCollectionView: UICollectionView?
    
    var imgArray            = [[String:Any]]()
    var passedContextOffset = IndexPath()
    var intAPIType          = Int()
    var intUserID           = Int()
    var intUserType         = Int()
    var intPhid             = Int()
    var strSchoolCodeSender = String()
    var photoThumbnail      = UIImage()
    var strLikesPhoto       = String()
    var awsURL : String?
    
    @IBOutlet weak var viewBg:          UIView?
    @IBOutlet weak var btnAddFavourite: UIButton?
    @IBOutlet weak var btnLike:         UIButton?
    @IBOutlet weak var btnAllLikes:     UIButton?
    @IBOutlet weak var btnShare:        UIButton?
    
    let selectedColor   = UIColor(rgb: 0x56B54B)
    let unselectedColor = UIColor(rgb: 0x5E5E5E)
    
    @IBAction func ClkBack(_ sender: Any)
    {
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        self.navigationController == nil ? dismissVC(completion: nil) : popVC()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = true
        self.hideTabBar()
        
        myCollectionView?.delegate   = self
        myCollectionView?.dataSource = self
        myCollectionView?.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        print("Func: viewWillAppear")
        //myCollectionView.scrollToItem(at: passedContextOffset, at: .left, animated: true)
        print("self.imgArray[passedContextOffset.row] \n \(self.imgArray[passedContextOffset.row])")
        //For loading the like , dislike , favourites data in 1 photo
        
        self.myCollectionView?.performBatchUpdates({},
                                                   completion: { (finished) in
                                                    print("collection-view finished reload done")
                                                    self.myCollectionView?.scrollToItem(at: self.passedContextOffset, at: .centeredHorizontally, animated: false)
                                                    
        })
        if passedContextOffset == [0,0] {
            updateLikes()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.isNavBarHidden = false
        self.showTabBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Func: viewDidLoad")
        
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        self.title = "Photos"
        
        if let schoolCode = self.getCurrentSchool()?.schCode {strSchoolCodeSender =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID    {self.intUserID      =  Int(userId)}
        if let userType   = self.getCurrentUser()?.userType  {self.intUserType    =  Int(userType)}
       
    }
    //hide Status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if imgArray.count > 0{
            return imgArray.count
        }
        else{
            return 0
        }
    }
    // Called when the cell is displayed
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImagePreviewFullViewCell
        
        
        var strPhotoURL     = String()
        switch self.intAPIType
        {
        case 1:
            if self.awsURL?.count == 0 {
            strPhotoURL   += strPhotoURL.imgPath1()
            }
            else {
                strPhotoURL = /self.awsURL
            }
            strPhotoURL   += "/SchImg/"
            strPhotoURL   += self.strSchoolCodeSender
            strPhotoURL   += "/PhotoAlbum/Full/"
            if let  strPhotoURL1  = imgArray[indexPath.row]["FileName"] as? String{
                strPhotoURL   += strPhotoURL1
            }
            
        case 2:
            if let  strPhotoURL1  = imgArray[indexPath.row]["URL"] as? String{
                
                var strVideoID = String()
                if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                    strVideoID = value
                    strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
                }
            }
        default :
            break
        }
        if strPhotoURL.count != 0 {
        let finalUrl    = "\(strPhotoURL)"
        let urlPhoto    = URL(string: finalUrl)
//        let thumbUrl =   strPhotoURL.replace(target: "Full", withString: "Thumb")
        print("urlPhoto = \(String(describing: urlPhoto))")
            if let url = urlPhoto {
                cell.imgView.loadURL(imageUrl: url, placeholder: nil, placeholderImage:  R.image.photogalleryPlaceholder())
            }
        }
        else {
            cell.imgView.image = R.image.photogalleryPlaceholder()
        }
        return cell
    }
    
    
    @IBAction func ClkBtnFavourite(_ sender: UIButton){
        print("Func: ClkBtnFavourite)")
        var inttag        = Int()
        var intPhID       = Int()
        var intIsFavorite = Int()
        var urlString     = String()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intPhID1  = imgArray[inttag]["PhID"] as? Int{
            intPhID = intPhID1
        }
        if btn.hasImage(named: "FavoriteBlank", for: .normal) {
            print("YES")
            intIsFavorite = 1
            
        } else {
            print("NO")
            intIsFavorite = 0
        }
        
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "gallery/Favorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&S_MdlID=\(intAPIType)&ID=\(intPhID)&UserID=\(intUserID)&UserType=\(intUserType)&isAdd=\(intIsFavorite)"
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Added to favorite successfully"
                            {
                                btn.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)//FavoriteFilled
                            }
                            else  if result["Message"] as? String == "Removed from favorite list"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            /************************/
                            self.imgArray[inttag].updateValue(intIsFavorite, forKey: "isFavourite")
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    @IBAction func ClkBtnShare(_ sender: UIButton)
    {
        print("Func: ClkBtnShare)")
        
        var inttag = Int()
        inttag     = sender.tag
        print("inttag = \(inttag)")
        
        var visibleRect    = CGRect()
        visibleRect.origin = (myCollectionView?.contentOffset)!
        visibleRect.size   = (myCollectionView?.bounds.size)!
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath: IndexPath = (myCollectionView?.indexPathForItem(at: visiblePoint)!)!
        
        let selectedCell = myCollectionView?.cellForItem(at: indexPath) as! ImagePreviewFullViewCell
        photoThumbnail   = selectedCell.imgView.image!
        
        print("photoThumbnail= \(photoThumbnail)")
        
        let shareItems:Array = [photoThumbnail] as [Any]//img,strShareCopyThought
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func ClkBtnLike(_ sender: UIButton)
    {
        print("Func: ClkBtnLike)")
        
        var inttag        = Int()
        var intPhID       = Int()
        var intIsLike     = Int()
        var urlString     = String()
        var intLikes      = Int()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intPhID1  = imgArray[inttag]["PhID"] as? Int{
            intPhID = intPhID1
        }
        if btn.hasImage(named: "like", for: .normal) {
            intIsLike = 1
        } else {
            intIsLike = 0
        }
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "like/Photo?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&Phid=\(intPhID)&isLike=\(intIsLike)"
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Liked"
                            {
                                btn.setImage(R.image.liked(), for: UIControl.State.normal)
                                btn.setTitleColor(self.selectedColor, for: .normal)
                            }
                            else  if result["Message"] as? String == "Unliked"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.like(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            
                            intLikes = (results!["TotalLikes"] as? Int)!
                            print("intLikes = \(intLikes)")
                            self.btnAllLikes?.isHidden = false
                            if (intLikes > 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
                            }
                            else if (intLikes == 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
                            }
                            else  {
//                               self.btnTotalLikes.setTitle("Like",for: .normal)
                                self.btnAllLikes?.setTitle("Like",for: .normal)
                               // self.btnAllLikes?.isHidden = true
                            }
                            
                            /************************/
                            self.imgArray[inttag].updateValue(intIsLike, forKey: "isLike")
                            self.imgArray[inttag].updateValue(intLikes, forKey: "Likes")
                            
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        print("Func: viewWillLayoutSubviews")
        guard let flowLayout = myCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {return  }
        flowLayout.itemSize  = (myCollectionView?.frame.size)!
        flowLayout.invalidateLayout()
        myCollectionView?.collectionViewLayout.invalidateLayout()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        print("Func: viewWillTransition")
        
        let offSet    = myCollectionView?.contentOffset
        let width     = myCollectionView?.bounds.size.width
        let index     = round((offSet?.x)! / width!)
        let newOffset = CGPoint(x: index * size.width, y: (offSet?.y)!)
        myCollectionView?.setContentOffset(newOffset, animated: true)
        coordinator.animate(alongsideTransition: {(context) in
            self.myCollectionView?.reloadData()
            self.myCollectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateLikes()
    }
    //MARK:- Updating the Likes/Favourites
    func updateLikes(){
        var visibleRect    = CGRect()
        visibleRect.origin = (myCollectionView?.contentOffset)!
        visibleRect.size   = (myCollectionView?.bounds.size)!
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        var indexPath = IndexPath()
        if  let indexPath1: IndexPath = myCollectionView?.indexPathForItem(at: visiblePoint) {
            indexPath =  indexPath1
        }
        else
        {
            indexPath = [0,0]
            print("else part \(indexPath)")
        }
        btnAddFavourite?.tag = indexPath.row
        btnLike?.tag         = indexPath.row
        btnAllLikes?.tag     = indexPath.row
        btnShare?.tag        = indexPath.row
        
        var intLikes       = Int()
        var intIsLike      = Int()
        var intIsFavourite = Int()
        
        if let intLikes1 = imgArray[indexPath.row]["Likes"] as? Int
        {
            intLikes = intLikes1
            
            btnAllLikes?.isHidden = false
            if intLikes > 1{
                btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
            }
            else  if intLikes == 1{
                btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
            }
            else  if intLikes == 0{
                  btnAllLikes?.setTitle("Like",for: .normal)
//                btnAllLikes?.isHidden = true
            }
        }
        if let intisLike1 = imgArray[indexPath.row]["isLike"] as? Int
        {
            intIsLike = intisLike1
            switch intIsLike
            {
            case 0:
                btnLike?.setImage(R.image.like(), for: UIControl.State.normal)
                btnLike?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnLike?.setImage(R.image.liked(), for: UIControl.State.normal)
                btnLike?.setTitleColor(selectedColor, for: .normal)
            default:
                break
            }
            
        }
        if let intisFavourite1 = imgArray[indexPath.row]["isFavourite"] as? Int
        {
            intIsFavourite = intisFavourite1
          //  print("intIsFavourite = \(intIsFavourite)")
            switch intIsFavourite
            {
            case 0:
                btnAddFavourite?.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnAddFavourite?.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            default:
                break
            }
        }
    }
    @IBAction func ClkBtnAllLikes(_ sender: UIButton)
    {
//        var inttag = Int()
//        inttag     = sender.tag
        if let intPhid1       = imgArray[sender.tag]["PhID"] as? Int{intPhid = intPhid1}
        if let strLikesPhoto1 = imgArray[sender.tag]["Likes"] as? Int{strLikesPhoto = "\(strLikesPhoto1)"}
        //intWordThoughtQuestion 1- word, 2-Thought and 3- Question/Answer 4- PhotoGallery
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let likesAllVc = storyboard.instantiateViewController(withIdentifier: "LikesAllUserViewController") as? LikesAllUserViewController else {return}
        likesAllVc.strTotalLikes          = strLikesPhoto
        likesAllVc.intPhid                = intPhid
        likesAllVc.intWordThoughtQuestion = 4
        self.pushVC(likesAllVc)
    }
    
    // MARK: - prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SeguePhotoLikeAll"
        {
            let segveLikes = segue.destination as! LikesAllUserViewController
            segveLikes.strTotalLikes          = strLikesPhoto
            segveLikes.intPhid                = intPhid
            segveLikes.intWordThoughtQuestion = 4
            //intWordThoughtQuestion 1- word, 2-Thought and 3- Question/Answer 4- PhotoGallery
        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
class ImagePreviewFullViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    var scrollImg: UIScrollView!
    var imgView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        scrollImg                              = UIScrollView()
        scrollImg.delegate                     = self
        scrollImg.alwaysBounceVertical         = false
        scrollImg.alwaysBounceHorizontal       = false
        scrollImg.showsVerticalScrollIndicator = true
        scrollImg.flashScrollIndicators()
        
        scrollImg.minimumZoomScale = 1.0
        scrollImg.maximumZoomScale = 4.0
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        scrollImg.addGestureRecognizer(doubleTapGest)
        
        self.addSubview(scrollImg)
        
        imgView             = UIImageView()
        imgView.contentMode = .scaleAspectFit
        scrollImg.addSubview(imgView!)
    }
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        print("Func: handleDoubleTapScrollView")
        if scrollImg.zoomScale == 1 {
            scrollImg.zoom(to: zoomRectForScale(scale: scrollImg.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        }else{
            scrollImg.setZoomScale(1, animated: true)
        }
    }
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        print("Func: zoomRectForScale")
        
        var zoomRect         = CGRect.zero
        zoomRect.size.height = imgView.frame.size.height / scale
        zoomRect.size.width  = imgView.frame.size.width  / scale
        let newCenter        = imgView.convert(center, from: scrollImg)
        zoomRect.origin.x    = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y    = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        print("Func: viewForZooming")
        return self.imgView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {self.centerScrollViewContents()}
    func centerScrollViewContents()
    {
        let boundsSize    = scrollImg.bounds.size
        var contentsFrame = imgView.frame
        
        if (contentsFrame.size.width < boundsSize.width) {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2
        } else {
            contentsFrame.origin.x = 0
        }
        
        if (contentsFrame.size.height < boundsSize.height) {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2
        } else {
            contentsFrame.origin.y = 0
        }
        imgView.frame = contentsFrame
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        print("Func: layoutSubviews")
        
        scrollImg.frame = self.bounds
        imgView.frame  = self.bounds
        print("scrollImg.frame = \(scrollImg.frame)")
        print("imgView.frame   = \(imgView.frame)")
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        print("Func: prepareForReuse")
        scrollImg.setZoomScale(1, animated: true)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
