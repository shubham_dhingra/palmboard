//  RecentUpdateGridViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 02/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import EZSwiftExtensions

class RecentUpdateGridViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var myCollectionView: UICollectionView?
    var imgArray            = [String]()
    var passedContextOffset = IndexPath()
    var intAPIType          = Int()
    var strSchoolCodeSender = String()
    var strVideoURL        = String()
    var awsURL              : String?

    @IBAction func ClkBack(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("Func: viewWillAppear")
        //myCollectionView.scrollToItem(at: passedContextOffset, at: .left, animated: true)
        print("self.imgArray[passedContextOffset.row] \n \(self.imgArray[passedContextOffset.row])")

        myCollectionView?.delegate   = self
        myCollectionView?.dataSource = self
        if self.intAPIType == 1 {
        myCollectionView?.register(ImagePreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        }
        else{
            myCollectionView?.register(VideoPreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        }

        //For loading the like , dislike , favourites data in 1 photo
        self.myCollectionView?.performBatchUpdates({},
                                                   completion: { (finished) in
                                                    print("collection-view finished reload done")
                                                    self.myCollectionView?.scrollToItem(at: self.passedContextOffset, at: .centeredHorizontally, animated: false)
                                                    
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        self.title = "Photos"
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
    }
    //hide Status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if imgArray.count > 0{
            return imgArray.count
        }
        else{
            return 0
        }
    }
    // Called when the cell is displayed
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var strPhotoURL     = String()

        if self.intAPIType == 1 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ImagePreviewFullViewCell else {return UICollectionViewCell()}
            
            
            if self.awsURL?.count == 0   {
                strPhotoURL   += strPhotoURL.imgPath1()
            }
            else {
                strPhotoURL = /self.awsURL
            }
            strPhotoURL   += "/SchImg/"
            strPhotoURL   += self.strSchoolCodeSender
            strPhotoURL   += "/PhotoAlbum/Full/"
            let strPhotoURL1  = imgArray[indexPath.row]
            strPhotoURL      += strPhotoURL1
            
            let finalUrl    = "\(strPhotoURL)"
            let urlPhoto    = URL(string: finalUrl)
            // print("urlPhoto = \(String(describing: urlPhoto))")
            cell.imgView.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
            return cell
        }
        else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? VideoPreviewFullViewCell else {return UICollectionViewCell()}
            
            if let  strPhotoURL1  = imgArray[indexPath.row] as? String
            {
                var strVideoID = String()
                if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                    strVideoID = value
                    strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
                }
            }
            if strPhotoURL.count != 0 {
            let finalUrl    = "\(strPhotoURL)"
            let urlPhoto    = URL(string: finalUrl)
            // print("urlPhoto = \(String(describing: urlPhoto))")
            cell.imgView.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
            }
            else {
                cell.imgView?.image = R.image.photogalleryPlaceholder()
            }
            return cell
        }
       
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Func: didSelectItemAt = \(indexPath.row)")
        if self.intAPIType != 1 {

        if let  strVideoURL1  = imgArray[indexPath.row] as? String{
            strVideoURL = strVideoURL1
            }
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let vc = storyboard.instantiateViewController(withIdentifier: "VideoPlayerViewController") as? VideoPlayerViewController else {return}
            
            if let videoID = strVideoURL.components(separatedBy: "/").last {
                vc.strVideoURL   = strVideoURL
                vc.videoIdString = videoID
                
            }else{
                vc.strVideoURL = ""
                vc.videoIdString = ""
            }
            ez.topMostVC?.presentVC(vc)
    }
    }
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        print("Func: viewWillLayoutSubviews")
        guard let flowLayout = myCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {return  }
        flowLayout.itemSize  = (myCollectionView?.frame.size)!
        flowLayout.invalidateLayout()
        myCollectionView?.collectionViewLayout.invalidateLayout()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        print("Func: viewWillTransition")
        
        let offSet    = myCollectionView?.contentOffset
        let width     = myCollectionView?.bounds.size.width
        let index     = round((offSet?.x)! / width!)
        let newOffset = CGPoint(x: index * size.width, y: (offSet?.y)!)
        myCollectionView?.setContentOffset(newOffset, animated: true)
        coordinator.animate(alongsideTransition: {(context) in
            self.myCollectionView?.reloadData()
            self.myCollectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("Func: scrollViewDidScroll")
      //  updateLikes()
    }
}
