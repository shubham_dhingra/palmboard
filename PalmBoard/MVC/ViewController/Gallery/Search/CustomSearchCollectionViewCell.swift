//
//  CustomSearchCollectionViewCell.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 02/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class CustomSearchCollectionViewCell: UICollectionViewCell {
        @IBOutlet weak var viewBg:             UIView?
        @IBOutlet weak var imgView:            UIImageView?
        @IBOutlet weak var lblAlbumName:       UILabel?
        @IBOutlet weak var lblTotalDatePhotos: UILabel?
}
