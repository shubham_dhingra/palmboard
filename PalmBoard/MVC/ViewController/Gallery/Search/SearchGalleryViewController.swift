//  SearchGalleryViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 02/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class SearchGalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate {
    
    @IBOutlet weak var lblRecord: UILabel?
    @IBOutlet weak var collectionViewAlbums: UICollectionView?
    var strTitle                = String()
   // var arrayPhotosGallery = [[String: Any]]()
    
    var arrayPhAlbum = [[String: Any]]()
    var arrayVdAlbum = [[String: Any]]()

    var btnTitle           =  UIButton()
    var intAlId            = Int()
    var intAPIType         = Int()
    var strSchoolCodeSender = String()
    var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        
        strTitle = "Result"
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle(strTitle, for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17.0)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        searchBar                   = UISearchBar()
        searchBar.placeholder       = "Search"
        searchBar.showsCancelButton = false
        searchBar.barTintColor      = UIColor.white
        searchBar.searchBarStyle    = .minimal
        searchBar.returnKeyType     = .search
        searchBar.autocorrectionType = UITextAutocorrectionType.yes
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
        if let schoolCode = self.getCurrentSchool()?.schCode{
            strSchoolCodeSender =  schoolCode
        }
        collectionViewAlbums?.isHidden = true
        self.lblRecord?.isHidden       = true
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewAlbums?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print("searchBarSearchButtonClicked")
        //print("searchBar= \(String(describing: searchBar.text))")
        searchBar.resignFirstResponder()
        self.webApiSearchGallery(strQuery: searchBar.text!)
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 20
    }
    
    func webApiSearchGallery(strQuery: String){
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        
        urlString += "gallery/Search?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&query=\(strQuery)"
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
               DispatchQueue.main.async {
                   Utility.shared.removeLoader()
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    
                        self.collectionViewAlbums?.isHidden = false
                        
                        if let arrayAll = (result["PhAlbum"] as? [[String: Any]]){
                            self.arrayPhAlbum = arrayAll
                        }
                        if let arrayAll = (result["VdAlbum"] as? [[String: Any]]){
                            self.arrayVdAlbum = arrayAll
                        }
                        if self.arrayPhAlbum.count == 0 && self.arrayVdAlbum.count == 0{
                            self.collectionViewAlbums?.isHidden = true
                            self.lblRecord?.isHidden            = false
                        }
                        else
                        {
                            
                            print("arrayPhAlbum = \(self.arrayPhAlbum)")
                            print("arrayVdAlbum = \(self.arrayVdAlbum)")
                            self.lblRecord?.isHidden = true
                            self.searchBar.isHidden  = false
                            //self.searchBar.text     = ""
                            self.searchBar.placeholder            = "Search"
                            self.collectionViewAlbums?.delegate   = self
                            self.collectionViewAlbums?.dataSource = self
                            self.collectionViewAlbums?.isHidden   = false
                            self.collectionViewAlbums?.reloadData()
                        }
                    
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
            }
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0{
           return arrayPhAlbum.count
        }
        else if section == 1{
            return arrayVdAlbum.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print("Func: cellForItemAt = ")
        
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellSearch", for: indexPath) as! CustomSearchCollectionViewCell
        
      //var intItem       = Int()
       // var intTotalPhotos  = Int()
       // var strSubModule    = String()
        var strPhotoURL     = String()
        var strDate         = String()

        strPhotoURL   += strPhotoURL.imgPath1()
        strPhotoURL   += "/SchImg/"
        strPhotoURL   += self.strSchoolCodeSender
        
        if indexPath.section == 0
        {
            strPhotoURL   += "/PhotoAlbum/Thumb/"
            if let  strSubModule1  = arrayPhAlbum[indexPath.row]["Title"] as? String{
              //  strSubModule = strSubModule1
                cellCollection.lblAlbumName?.text = strSubModule1
            }
            if let  strPhotoURL1  = arrayPhAlbum[indexPath.row]["FileName"] as? String{
                strPhotoURL   += strPhotoURL1
            }
//            if let  intTotalPhotos1  = arrayPhAlbum[indexPath.row]["TotalPhotos"] as? Int{
//                intTotalPhotos = intTotalPhotos1
//            }
            if let  strUpdatedOn  = arrayPhAlbum[indexPath.row]["EventDate"] as? String{
                strDate   = strUpdatedOn
            }
        }
        else  if indexPath.section == 1
        {
            print("indexPath row = \(indexPath.row) and indexPath Section = \(indexPath.section)")
            strPhotoURL   += "/videoImg/"
            if let  strSubModule1  = arrayVdAlbum[indexPath.row]["Title"] as? String{
              //  strSubModule = strSubModule1
                cellCollection.lblAlbumName?.text = strSubModule1
            }
            if let  strPhotoURL1  = arrayVdAlbum[indexPath.row]["FileName"] as? String{
                strPhotoURL   += strPhotoURL1
            }
//            if let  intTotalPhotos1  = arrayVdAlbum[indexPath.row]["TotalVideos"] as? Int{
//                intTotalPhotos = intTotalPhotos1
//            }
            if let  strUpdatedOn  = arrayVdAlbum[indexPath.row]["EventDate"] as? String{
                strDate   = strUpdatedOn
            }
        }
        let finalUrl    = "\(strPhotoURL)"
        
        if let urlPhoto    = URL(string: finalUrl) {
            cellCollection.imgView?.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }
        }
        else {
            cellCollection.imgView?.image = R.image.photogalleryPlaceholder()
        }
        
        /****************/
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: strDate)
        strDate                 = formatter.string(from: date1!)
        
        var strDay = String()
        strDay = self.daySuffix(from: date1!)
        print("strDay = \(strDay)")
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "MMM, yyyy"
        let date: Date? = dateFormatterGet.date(from: strDate)
        // cellCollection.lblTotalDatePhotos.text = dateFormatter.string(from: date!)
       
        if indexPath.section == 0{
            cellCollection.lblTotalDatePhotos?.text = "\(strDay) \(dateFormatter.string(from: date!))"// | \(intTotalPhotos) Photos
            }
        else  if indexPath.section == 1{
                cellCollection.lblTotalDatePhotos?.text = "\(strDay) \(dateFormatter.string(from: date!))"// | \(intTotalPhotos) Videos
            }
        /****************/
        return cellCollection
    }
    func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "\(dayOfMonth)st"
        case 2, 22:     return "\(dayOfMonth)nd"
        case 3, 23:     return "\(dayOfMonth)rd"
        default:        return "\(dayOfMonth)th"
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       if indexPath.section == 0{
            intAPIType = 1
            if let  intAlId1  = arrayPhAlbum[indexPath.row]["AlID"] as? Int
            {
                intAlId = intAlId1
            }
        }
        else if indexPath.section == 1{
            intAPIType = 2
            if let  intAlId1  = arrayVdAlbum[indexPath.row]["VAID"] as? Int
            {
                intAlId = intAlId1
            }
        }
        navigateToPhotoAlbumsDetails()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width / 2 - 10
        return CGSize(width: width, height: 224)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func navigateToPhotoAlbumsDetails() {
        guard let vc = R.storyboard.main.detailsPhotoAlbumsViewController() else {
            return
        }
        vc.intAPIType = self.intAPIType
        vc.strTitle   = self.intAPIType == 1 ? "Photos" : "Videos"
        vc.intAlId = intAlId
        vc.fromBoardScreen = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
