import UIKit
import AVFoundation

class VideoPlayerViewController: UIViewController {
    //var webView     = UIWebView()
    var videoIdString = String()
    var strVideoURL   = String()
    
    @IBOutlet weak var webView: UIWebView?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver( forName: UIWindow.didBecomeVisibleNotification, object: self.view.window, queue: nil )
        {
            notification in
            print("Video is now fullscreen") }
        NotificationCenter.default.addObserver( forName: UIWindow.didBecomeHiddenNotification, object: self.view.window, queue: nil )
        { notification in
            self.dismiss(animated: true, completion: nil)
            print("**** Video stopped")
        }
        self.view.backgroundColor = UIColor.black
        
        webView?.backgroundColor = UIColor.clear
        webView?.isOpaque        = false
        
        self.view.addSubview(webView!)
        
        var uTubePrefix = String()//"\(strVideoURL)
        
        if videoIdString.range(of:"watch") != nil {
            videoIdString = videoIdString.replacingOccurrences(of: "watch?v=", with: "")
            uTubePrefix = "https://www.youtube.com/embed/\(videoIdString)"
        }else{
            uTubePrefix = "\(strVideoURL)"
        }
        if let videoUrl = URL(string: uTubePrefix){
            webView?.allowsInlineMediaPlayback = true
            webView?.loadRequest(URLRequest(url: videoUrl))//
        }
        else{
            let alert = UIAlertController(title: "", message: "Video not found", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }
    //hide Status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBAction func backClicked(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        // self.navigationController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
