//  VideoSliderViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 29/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class VideoSliderViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var myCollectionView: UICollectionView?
    var imgArray            = [[String:Any]]()
    var passedContextOffset = IndexPath()
    var intAPIType          = Int()
    var strSchoolCodeSender = String()
    // var arrayPhoto  = [[String: Any]]()
    var strVideoURL        = String()
    let selectedColor   = UIColor(rgb: 0x56B54B)
    let unselectedColor = UIColor(rgb: 0x5E5E5E)
    
    var intUserID           = Int()
    var intUserType         = Int()
    var intVId             = Int()
    var strLikesPhoto       = String()
    var photoThumbnail      = UIImage()
    
    
    @IBOutlet weak var viewBg:          UIView?
    @IBOutlet weak var btnAddFavourite: UIButton?
    @IBOutlet weak var btnLike:         UIButton?
    @IBOutlet weak var btnAllLikes:     UIButton?
    @IBOutlet weak var btnShare:        UIButton?
    
    @IBAction func ClkBack(_ sender: Any)
    {
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        self.navigationController == nil ? dismissVC(completion: nil) : popVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        print("Func: viewDidLoad")
        self.title = "Videos"        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        print("imgArray = \(imgArray)")
        print("passedContextOffset = \(passedContextOffset)")
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.isNavBarHidden = false
        self.showTabBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = true
        self.hideTabBar()
        myCollectionView?.delegate   = self
        myCollectionView?.dataSource = self
        myCollectionView?.register(VideoPreviewFullViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        //print("myCollectionView.frame = \(myCollectionView?.frame)")
        myCollectionView?.scrollToItem(at: passedContextOffset, at: .left, animated: true)
        self.myCollectionView?.performBatchUpdates({},
                                                   completion: { (finished) in
                                                    print("collection-view finished reload done")
                                                    self.myCollectionView?.scrollToItem(at: self.passedContextOffset, at: .centeredHorizontally, animated: false)
        })
        
        if passedContextOffset == [0,0] {
            updateLikes()
        }
    }
    //hide Status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if imgArray.count > 0{
            return imgArray.count
        }
        else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("Func: cellForItemAt = ")
        print("row = \(indexPath.row)")
        print("Section = \(indexPath.section)")
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! VideoPreviewFullViewCell
        var strPhotoURL     = String()
        
        if let  strPhotoURL1  = imgArray[indexPath.row]["URL"] as? String
        {
             var strVideoID = String()
            if let value = strPhotoURL1.extractYoutubeIdFromLink() {
                strVideoID = value
                strPhotoURL = "https://img.youtube.com/vi/\(strVideoID)/hqdefault.jpg"
            }
        }
        
        if strPhotoURL.count != 0 {
        let finalUrl    = "\(strPhotoURL)"
        if let urlPhoto    = URL(string: finalUrl) {
      //  print("urlPhoto = \(urlPhoto)")
        cell.imgView.af_setImage(withURL: urlPhoto, placeholderImage: R.image.photogalleryPlaceholder(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            //cell.imgView.image = imgArray[indexPath.row]
            }
        }
        else {
            cell.imgView.image = R.image.photogalleryPlaceholder()
            }
        }
        else {
          cell.imgView.image = R.image.photogalleryPlaceholder()
        }
        cell.imgViewPlay.tag = indexPath.row
        let tap = UITapGestureRecognizer()
        tap.view?.tag = indexPath.row
        tap.addTarget(self, action: #selector(self.playVideo(_:)))
         cell.imgViewPlay.addGestureRecognizer(tap)
        return cell
    }
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        guard let flowLayout = myCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {return  }
        flowLayout.itemSize  = (myCollectionView?.frame.size)!
        flowLayout.invalidateLayout()
        myCollectionView?.collectionViewLayout.invalidateLayout()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        print("Func: viewWillTransition")
        
        let offSet    = myCollectionView?.contentOffset
        let width     = myCollectionView?.bounds.size.width
        let index     = round((offSet?.x)! / width!)
        let newOffset = CGPoint(x: index * size.width, y: (offSet?.y)!)
        myCollectionView?.setContentOffset(newOffset, animated: true)
        coordinator.animate(alongsideTransition: {(context) in
            self.myCollectionView?.reloadData()
            self.myCollectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let  strVideoURL1  = imgArray[indexPath.row]["URL"] as? String{
//            strVideoURL = strVideoURL1
//            self.performSegue(withIdentifier: "segueVideoSlider", sender: self)
//        }
    }
    @IBAction func ClkBtnFavourite(_ sender: UIButton)
    {
        print("Func: ClkBtnFavourite)")
        var inttag        = Int()
        var intVId        = Int()
        var intIsFavorite = Int()
        var urlString     = String()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intPhID1  = imgArray[inttag]["VID"] as? Int{
            intVId = intPhID1
        }
        if btn.hasImage(named: "FavoriteBlank", for: .normal) {
            print("YES")
            intIsFavorite = 1
        } else {
            print("NO")
            intIsFavorite = 0
        }
        
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "gallery/Favorite?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&S_MdlID=\(intAPIType)&ID=\(intVId)&UserID=\(intUserID)&UserType=\(intUserType)&isAdd=\(intIsFavorite)"
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Added to favorite successfully"
                            {
                                btn.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)//FavoriteFilled
                            }
                            else  if result["Message"] as? String == "Removed from favorite list"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            /************************/
                            self.imgArray[inttag].updateValue(intIsFavorite, forKey: "isFavourite")
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    @IBAction func ClkBtnShare(_ sender: UIButton)
    {
        print("Func: ClkBtnShare)")
        var inttag        = Int()
        var strShareVideo = String()
        
        inttag     = sender.tag
        print("inttag = \(inttag)")
        
        if let  strShareVideo1  = imgArray[inttag]["URL"] as? String{
            strShareVideo = strShareVideo1
        }
        print("strShareVideo= \(strShareVideo)")
        
        let shareItems:Array = [strShareVideo] as [Any]//img,strShareCopyThought
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func ClkBtnLike(_ sender: UIButton)
    {
        print("Func: ClkBtnLike)")
        
        var inttag        = Int()
        var intVId       = Int()
        var intIsLike     = Int()
        var urlString     = String()
        var intLikes      = Int()
        
        inttag        = sender.tag
        print("inttag = \(inttag)")
        let btn = sender
        
        if let  intPhID1  = imgArray[inttag]["VID"] as? Int{
            intVId = intPhID1
        }
        if btn.hasImage(named: "like", for: .normal) {
            print("YES")
            intIsLike = 1
            
        } else {
            print("NO")
            intIsLike = 0
        }
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "like/Video?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&Vid=\(intVId)&isLike=\(intIsLike)"
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("*** result = \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if result["Message"] as? String == "Liked"
                            {
                                btn.setImage(R.image.liked(), for: UIControl.State.normal)
                                btn.setTitleColor(self.selectedColor, for: .normal)
                            }
                            else  if result["Message"] as? String == "Unliked"
                            {
                                //FavoriteBlank
                                btn.setImage(R.image.like(), for: UIControl.State.normal)
                                btn.setTitleColor(self.unselectedColor, for: .normal)
                            }
                            
                            intLikes = (results!["TotalLikes"] as? Int)!
                            print("intLikes = \(intLikes)")
                            self.btnAllLikes?.isHidden = false
                            if (intLikes > 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
                            }
                            else if (intLikes == 1) {
                                self.btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
                            }
                            else  {
                                 self.btnAllLikes?.setTitle("Like",for: .normal)
                                // self.btnTotalLikes.setTitle("Like",for: .normal)
                           //     self.btnAllLikes?.isHidden = true
                            }
                            
                            /************************/
                            self.imgArray[inttag].updateValue(intIsLike, forKey: "isLike")
                            self.imgArray[inttag].updateValue(intLikes, forKey: "Likes")
                            
                            /*************************/
                        }
                    }
                    if (error != nil)
                    {
                        print("PhotoSliderViewController = \(error!)")
                        DispatchQueue.main.async
                            {
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
        //normalDelegate?.ClkbtnReportNormalPressed(intTag: inttag)
    }
    func updateLikes(){
        
        var visibleRect    = CGRect()
        visibleRect.origin = (myCollectionView?.contentOffset)!
        visibleRect.size   = (myCollectionView?.bounds.size)!
        let visiblePoint   = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        var indexPath = IndexPath()
        if  let indexPath1: IndexPath = myCollectionView?.indexPathForItem(at: visiblePoint) {
            indexPath =  indexPath1
        }
        else
        {
            indexPath = [0,0]
            print("else part \(indexPath)")
        }
        btnAddFavourite?.tag = indexPath.row
        btnLike?.tag         = indexPath.row
        btnAllLikes?.tag     = indexPath.row
        btnShare?.tag        = indexPath.row
        
        print("indexPath.row= \(indexPath.row)")
        
        var intLikes       = Int()
        var intIsLike      = Int()
        var intIsFavourite = Int()
        
        if let intLikes1 = imgArray[indexPath.row]["Likes"] as? Int
        {
            intLikes = intLikes1
            
            btnAllLikes?.isHidden = false
            if intLikes > 1{
                btnAllLikes?.setTitle("\(intLikes) Likes",for: .normal)
            }
            else  if intLikes == 1{
                btnAllLikes?.setTitle("\(intLikes) Like",for: .normal)
            }
            else  if intLikes == 0{
                  btnAllLikes?.setTitle("Like",for: .normal)
                //btnAllLikes.setTitle("\(intLikes) Answer",for: .normal)
            }
        }
        if let intisLike1 = imgArray[indexPath.row]["isLike"] as? Int
        {
            intIsLike = intisLike1
            switch intIsLike
            {
            case 0:
                btnLike?.setImage(R.image.like(), for: UIControl.State.normal)
                btnLike?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnLike?.setImage(R.image.liked(), for: UIControl.State.normal)
                btnLike?.setTitleColor(selectedColor, for: .normal)
            default:
                break
            }
        }
        if let intisFavourite1 = imgArray[indexPath.row]["isFavourite"] as? Int
        {
            intIsFavourite = intisFavourite1
            print("intIsFavourite = \(intIsFavourite)")
            switch intIsFavourite
            {
            case 0:
                btnAddFavourite?.setImage(R.image.favoriteBlank(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            case 1:
                btnAddFavourite?.setImage(R.image.favoriteFilled(), for: UIControl.State.normal)
                btnAddFavourite?.setTitleColor(unselectedColor, for: .normal)
            default:
                break
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateLikes()
    }
    @IBAction func ClkBtnAllLikes(_ sender: UIButton)
    {
        var inttag = Int()
        inttag = sender.tag
        
        if let intPhid1 = imgArray[inttag]["VID"] as? Int{
            intVId = intPhid1
        }
        if let strLikesPhoto1 = imgArray[inttag]["Likes"] as? Int{
            strLikesPhoto = "\(strLikesPhoto1)"
        }
       // self.performSegue(withIdentifier: "SegueVideoLikeAll", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let likesAllVc = storyboard.instantiateViewController(withIdentifier: "LikesAllUserViewController") as? LikesAllUserViewController else {return}
        likesAllVc.strTotalLikes          = strLikesPhoto
        likesAllVc.intVId                = intVId
        likesAllVc.intWordThoughtQuestion = 5
        self.pushVC(likesAllVc)
    }
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segueVideoSlider"){
            let seguePotos         = segue.destination as! VideoPlayerViewController
           // seguePotos.strVideoURL = strVideoURL
            //Added Nupur code.
            if let videoID = strVideoURL.components(separatedBy: "/").last {
                seguePotos.strVideoURL = strVideoURL
                seguePotos.videoIdString = videoID
                
            }else{
                seguePotos.strVideoURL = ""
                seguePotos.videoIdString = ""
            }
        }
    }
    @objc func playVideo(_ sender : UITapGestureRecognizer){
        
        guard let tag = sender.view?.tag else {return}
        if let  strVideoURL1  = self.imgArray[tag]["URL"] as? String{
            strVideoURL = strVideoURL1
            self.performSegue(withIdentifier: "segueVideoSlider", sender: self)
        }
    }
}
class VideoPreviewFullViewCell: UICollectionViewCell,UIScrollViewDelegate
{
    var imgView:   UIImageView!
    var imgViewPlay:   UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        print("Func: init")
        print("frame = \(frame)")
        
        imgView             = UIImageView()
        imgView.contentMode = .scaleAspectFit
        
        imgViewPlay             = UIImageView()
        imgViewPlay.isUserInteractionEnabled = true
        imgViewPlay.image       = R.image.playThumb()
        imgViewPlay.contentMode = .scaleToFill
        imgViewPlay.center      = imgView.center
        self.addSubview(imgView)
        self.addSubview(imgViewPlay)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        print("Func: layoutSubviews")
        
        //scrollImg.frame = self.bounds
        imgView.frame   = self.bounds
        print("imgView.frame = \(imgView.frame)")
        imgViewPlay.frame       = CGRect(x: imgView.frame.size.width / 2 - 15, y: imgView.frame.size.height / 2 - 15 , width: 30, height: 30)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init (code:) hasn't been implement")
    }
}


