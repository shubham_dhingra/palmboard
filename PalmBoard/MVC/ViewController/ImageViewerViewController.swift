//  ImageViewerViewController.swift
//  e-Care Pro
//  Created by Shubham on 11/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class ImageViewerViewController: BaseViewController {

    @IBOutlet weak var imgView : UIImageView?
    @IBOutlet weak var btnCross : UIButton?
    
    var selectImage :  UIImage?
    var url : URL?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let image = selectImage {
            imgView?.image = image
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.85)
        }
        
        if let url = url {
            imgView?.loadURL(imageUrl: url, placeholder: nil, placeholderImage:UIImage(named: "Placeholder_image"))
        }
    }
}
