//  DiscardLeaveViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class DiscardLeaveViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    @IBAction func ClkCancel(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func ClkDiscard(_ sender: Any) {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
