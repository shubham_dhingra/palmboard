//  ExtensionLeaveDetailsViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 26/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import Foundation
import UIKit
//MARK::- Date Picker
extension LeaveDetailsViewController  {
    
    // open Date Picker
    func openDatePicker(tag : Int) {
        
        let lastText = tag == 0 ? lblFromDate?.text : lblToDate?.text
        let lastDateSelected = tag == 0 ? lastDatePrevSelected : lastDateFutureSelected
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: lastDateSelected , minimumDate: self.prevDate, maximumDate: self.futureDate, datePickerMode: .date , lastText: lastText) { (date) in
            self.setDateIntoLabel(date: date, tag: tag , lastText : lastText)
        }
    }
    
    //Set the date into the label after getting response from the date picker
    func setDateIntoLabel(date : Date? , tag : Int , lastText : String?) {
        
        if let date = date {
            if checkDateInterval(date , tag) {
                
                let dateString = date.dateToString(formatType :"dd MMM, yyyy")
                if tag == 0 {
                    lblFromDate?.text = dateString
                    lastDatePrevSelected  =  date
                }
                else {
                    lblToDate?.text = dateString
                    lastDateFutureSelected =  date
                }
                
                //Set Number Of Leave Days
                if userType != "3" {
                    let leaveDays = lastDateFutureSelected.convertedDate.daysInBetweenDate(lastDatePrevSelected.convertedDate).toInt
                    print("leaveDays : \(leaveDays)")
                    self.btnNoOfLeave?.setTitle(String(format: "%02d", /leaveDays  == 0  ? 1 : /leaveDays + 1), for: .normal)
                }
                else {
                    self.halfDayArr = []
                    self.selectHalfDayArr = []
                    self.collectionHeight?.constant = 0.0
                    self.reloadCollection()
                    var leaveDays = lastDateFutureSelected.convertedDate.daysInBetweenDate(lastDatePrevSelected.convertedDate)
                    leaveDays =  leaveDays == 0.0 ? 1.0 : (/leaveDays + 1.0)
                    self.lblNoOfLeave?.text = "\(leaveDays) Days"
                }
            }
            else {
                showAlert(title : AlertConstants.Attention.get  , desc : AlertMsg.invalidDurationSelect.get)
            }
        }
        else {
            if tag == 0 {
                lblFromDate?.text = /lastText?.isEmpty ? Date().toString() : lastText
            }
            else {
                lblToDate?.text = /lastText?.isEmpty ? Date().toString() : lastText
            }
        }
    }
    func showAlert(title : String? , desc : String?) {
        AlertsClass.shared.showAlertController(withTitle: /title, message: /desc, buttonTitles: [AlertConstants.Ok.get]) { (value) in
            let type = value as AlertTag
            switch type {
            default:
                return
            }
        }
    }
    func checkDateInterval(_ selectDate : Date , _ tag : Int) -> Bool{
        
        print(selectDate.compare(lastDateFutureSelected).rawValue)
        var isCorrect = tag == 0 ? (lastDateFutureSelected.compare(selectDate).rawValue) : selectDate.compare(lastDatePrevSelected).rawValue
        
        if isCorrect == 1{
            return true
        }
        //check of equal
        if isCorrect == -1{
            isCorrect = tag == 0 ? ((lastDateFutureSelected.hoursInBetweenDate(selectDate).rounded() < 24) ? 1 : -1) : ((lastDatePrevSelected.hoursInBetweenDate(selectDate).rounded() < 24) ? 1 : -1)
        }
        return isCorrect != -1
    }
}
//MARK::- Selection of Suggested Reason
extension LeaveDetailsViewController : SuggestReasonDelegate {
    //Select reason from the the suggested Reason
    func reasonSelect(_ index : Int? , _ state : States?){
        if let index = index , let state = state {
            setStates(selectedIndex : index , state : state)
        }
    }
    //1) Below function maintain the state that only suggested reason is selected
    //2) Also maintain the state of text field of enter the reason
    
    func setStates(selectedIndex : Int? , state : States){
        var noneSelected : Bool = true
        for (index,_) in self.reasonStatesArr.enumerated(){
           
            self.reasonStatesArr[index] = index == selectedIndex ? .yes : .no
            if index == selectedIndex && self.reasonStatesArr[index] == .yes {
                txtReason?.text = self.leaveReasonArr?[index]
                noneSelected = false
            }
        }
        if noneSelected == true {
            txtReason?.text = nil
        }
        updateDoneButtonUI()
        reloadTable()
    }
}


