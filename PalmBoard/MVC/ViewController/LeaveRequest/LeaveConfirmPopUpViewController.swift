//  LeaveConfirmPopUpViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 26/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
protocol ClearanceDelegate {
    func clearData()
}
class LeaveConfirmPopUpViewController: BaseViewController {
    
    //MARK::- VARIABLES
    @IBOutlet weak var lblFromDate :   UILabel?
    @IBOutlet weak var lblToDate :     UILabel?
    @IBOutlet weak var lblReason:      UILabel?
    @IBOutlet weak var lblNoOfLeaves : UILabel?
    
    
    //MARK::- VARIABLES
    var lastDatePrevSelected = Date()
    var lastDateFutureSelected = Date()
    var reason : String?
    var NoOfLeaves : String?
    var delegate : ClearanceDelegate?
    var halfDayArr : [HalfDayModal]?
    var leaveID : Int?
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setUpView() {
        
        
        let attributedString1 = NSMutableAttributedString(string:"Reason: ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0) ?? nil ])
        
        let attributedString2 = NSMutableAttributedString(string:/reason, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)])
        
        attributedString1.append(attributedString2)
        lblReason?.attributedText = attributedString1
        lblNoOfLeaves?.text = /NoOfLeaves
        lblFromDate?.text  = lastDatePrevSelected.dateToString(formatType :"dd MMM, yyyy")
        lblToDate?.text  = lastDateFutureSelected.dateToString(formatType :"dd MMM, yyyy")
    }
    
    //BUTTON ACTION SUBMIT
    @IBAction func btnSubmitAct(_ sender: UIButton){
        sendRequest()
    }
}


//MARK::- API -(SEND LEAVE REQUEST)
extension LeaveConfirmPopUpViewController {
    
    func sendRequest(){
        APIManager.shared.request(with: HomeEndpoint.leaveRequest(fromDate: lastDatePrevSelected.dateToString(formatType: "YYYY-MM-dd"), tillDate: lastDateFutureSelected.dateToString(formatType: "YYYY-MM-dd"), duration: lblNoOfLeaves?.text, Reason: /reason, StID: userID, UserID: userID , UserType: userType , halfDayDTL: self.userType == "3" ? self.halfDayArr?.toJSON() : nil , LeaveID : self.userType == "3" ? self.leaveID?.toString : nil)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { [weak self](response) in
                if let modal = response as? LeaveDetailsModel {
                    
                    if modal.errorCode == 0 {
                        Messages.shared.show(alert: .success , message: /modal.message, type: .success)
                    }
                    else {
                        Messages.shared.show(alert: .oops , message: /modal.message, type: .warning)
                    }
                    self?.delegate?.clearData()
                    self?.dismissVC(completion: nil)
                }
            })
        }
    }
}
