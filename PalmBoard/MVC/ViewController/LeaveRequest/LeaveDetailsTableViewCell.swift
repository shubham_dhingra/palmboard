//
//  LeaveDetailsTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 23/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class LeaveDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var btnProfile:           UIButton?
    @IBOutlet weak var lblName:              UILabel?
    @IBOutlet weak var lblAppliedDate:       UILabel?
    @IBOutlet weak var btnDelete:            UIButton?
    @IBOutlet weak var lblFromDate:          UILabel?
    @IBOutlet weak var lblToDate:            UILabel?
    @IBOutlet weak var lblTotalLeaveServer:  UILabel?
    @IBOutlet weak var lblReasonServer:      UILabel?
    @IBOutlet weak var lblApproved:          UILabel?
    @IBOutlet weak var lblApprovedDate:      UILabel?
    @IBOutlet weak var btnProfileApprovedBy: UIButton?
    @IBOutlet weak var lblApprovedBy:        UILabel?
    @IBOutlet weak var lblRejectedBy:        UILabel?

    @IBOutlet weak var heightBottomCell:  NSLayoutConstraint?
    @IBOutlet weak var heightLblRejected: NSLayoutConstraint?
    @IBOutlet weak var heightLblApproved: NSLayoutConstraint?

    override func awakeFromNib()
    {
        super.awakeFromNib()
        btnProfile?.layer.cornerRadius = (btnProfile?.frame.width)! / 2
        btnProfile?.clipsToBounds      = true
        
        btnProfileApprovedBy?.layer.cornerRadius = (btnProfileApprovedBy?.frame.width)! / 2
        btnProfileApprovedBy?.clipsToBounds      = true
        
        lblApproved?.layer.cornerRadius = 3.0
        lblApproved?.clipsToBounds      = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
