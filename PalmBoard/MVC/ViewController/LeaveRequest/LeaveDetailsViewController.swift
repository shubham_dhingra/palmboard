//  LeaveDetailsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions
import IQKeyboardManager

class LeaveDetailsViewController : BaseViewController {
    
    //MARk::- VARIABLES
    var isPrevDatesAllow : Int?
    var ForwardDays : Int?    
    var isFirstTime : Bool = true
    var BackwardDays : Int?
    var leaveReasonArr : [String]?
    var leaveDataArr : [LeaveReason]?
    var reasonStatesArr = [States]()
    var leaveTypeArr : [LeaveType]?
    var refreshControl = UIRefreshControl()
    var prevDate : Date?
    var futureDate : Date?
    var lastDateFutureSelected = Date()
    var lastDatePrevSelected = Date()
    var isTermSelected : Bool = false
    var leaveListArr : [LeaveList]?
    var pageNo : Int = 1
    var fromBoardScreen : Bool = false
    var termsAndCondition : TermCondition?
    var serverDate : String?
    var halfDayArr: [HalfDayModal]?
    var selectHalfDayArr : [HalfDayModal]?
    var isFirstTimeReloadCollection : Bool = true
    var leaveID : Int?
    var fromWhere : Bool = false
    
    
    //MARK::- OUTLETS
    @IBOutlet weak var segmentLeave:      UISegmentedControl!
    @IBOutlet weak var viewLeaveRequest:  UIView?
    @IBOutlet weak var tblLeaveDetails:   UITableView?
    @IBOutlet weak var viewSegment:       UIView?
    //LeaveRequest - Form
    @IBOutlet weak var lblMainHeading : UILabel?
    @IBOutlet weak var lblFromDate:       UILabel?
    @IBOutlet weak var lblToDate:         UILabel?
    @IBOutlet weak var btnNoOfLeave :     UIButton?
    @IBOutlet weak var txtReason :        UITextField?
    @IBOutlet weak var btnTermCondition:  UIButton?
    @IBOutlet weak var tblLeaveReason :   UITableView?
    @IBOutlet weak var btnDone:           UIButton!
    @IBOutlet weak var btnTerms:           UIButton?
    @IBOutlet weak var tblViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var btnLeaveApply    : UIButton?
    @IBOutlet weak var leaveTypeView    : UIView?
    @IBOutlet weak var lblLeaveName     : UILabel?
    @IBOutlet weak var lblNoOfLeave      : UILabel?
    @IBOutlet weak var lblSuggested     : UILabel?
    @IBOutlet weak var btnHalfDaySelection : UIButton?
    @IBOutlet weak var LeaveDaysView : UIView?
    @IBOutlet weak var collectionHeight : NSLayoutConstraint?
    
    var leaveReasontableDataSource : LeaveReasonTableDataSource?{
        didSet{
            tblLeaveReason?.dataSource = leaveReasontableDataSource
            tblLeaveReason?.delegate = leaveReasontableDataSource
            tblLeaveReason?.reloadData()
        }
    }
    
    var leaveDetailstableDataSource : LeaveDetailsTableDataSource?{
        didSet{
            tblLeaveDetails?.dataSource = leaveDetailstableDataSource
            tblLeaveDetails?.delegate = leaveDetailstableDataSource
            tblLeaveDetails?.reloadData()
        }
    }
    
    //MARK: - LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = fromBoardScreen
        btnDone?.isUserInteractionEnabled = fromBoardScreen
        viewSegment?.isHidden = self.userType == "3"
        leaveTypeView?.isHidden = true
        //dont change the order of below two lines
        segmentLeave.selectedSegmentIndex = self.userType == "3" ? 1 : 0
        collectionView?.isHidden =  self.userType == "3"
        lblSuggested?.isHidden = self.userType == "3"
        LeaveDaysView?.isHidden = /lblSuggested?.isHidden
        collectionView?.isHidden = self.selectHalfDayArr?.count == 0
        btnHalfDaySelection?.isHidden = self.userType != "3"
        tblLeaveReason?.isHidden = self.userType == "3"
        addRefereshControl()
        updateSegmentControl()
    
        self.collectionHeight?.constant = (self.selectHalfDayArr?.count == 0 || self.selectHalfDayArr == nil) ? 0.0 : 60.0
        if self.userType == "3" {
            tblViewHeightConst.constant = 0.0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onViewWillApppear()
    }
    
    @IBAction func btnTermsAndConditionsAct(_ sender: UIButton) {
        openTermsAndConditionsVC()
    }
    
    @IBAction func btnLeaveApply(_ sender : UIButton){
        fromWhere = true
        guard let vc = R.storyboard.main.teacherLeaveRequestViewController() else {
            return
        }
        vc.leaveTypeArr = leaveTypeArr
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func btnBackAct(_ sender: UIButton) {
        self.view.endEditing(true)
        if !(/txtReason?.text?.trimmed().isBlank) {
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Alert.get, message:AlertMsg.AlertDiscardMessage.get , buttonTitles: [AlertConstants.Discard.get , AlertConstants.Cancel.get]) { (tag) in
                switch tag {
                case .yes:
                    self.navigationController?.popViewController(animated: false)
                default:
                    return
                }
            }
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func openTermsAndConditionsVC(){
        if let termsVc = storyboard?.instantiateViewController(withIdentifier: "LeavePolicyViewController") as? LeavePolicyViewController {
            if let tC = termsAndCondition {
                termsVc.termsAndCondtion = tC
                if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                    parent.hideBar(boolHide: true)
                }
                self.presentVC(termsVc)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    
    func onViewWillApppear(){
        
        self.fromWhere = false
        self.isNavBarHidden = true
        segmentLeave?.selectedSegmentIndex = fromBoardScreen ? 1 : 0
        getLeaveData()
        reloadTable()
        updateSegmentControl()
        configureLeaveDataTable()
        if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
            parent.showMenu()
        }
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = /fromWhere
    }
    
    
    // MARK: - getLeaveData
    func getLeaveData() {
        APIManager.shared.request(with: HomeEndpoint.LeaveApply(UserID: self.userID , UserType:  self.userType)) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handle(response : response)
            })
        }
    }
    
    
    func handle(response : Any?){
        if let response = response as? LeaveDetailsModel, let serverDate = response.serverDate {
            self.serverDate = serverDate
            self.leaveTypeArr = response.leaveTypeArr
            //            btnLeaveApply?.isHidden = (self.userType != "3" && self.segmentLeave.selectedSegmentIndex == 1)
            self.leaveReasonArr = response.leaveReason?.reason
            self.lblFromDate?.text = serverDate.dateChangeFormat()
            self.lblToDate?.text = serverDate.dateChangeFormat()
            self.termsAndCondition = response.termCondition
            let date = serverDate.convertStringIntoDate()
            if self.userType != "3" {
                let noOfLeaveDays = String(format: "%02d", date.daysInBetweenDate(date).toInt == 0 ? 1 : date.daysInBetweenDate(date).toInt)
                self.btnNoOfLeave?.setTitle(noOfLeaveDays, for: .normal)
            }
            else {
                let noOfLeaveDays = date.daysInBetweenDate(date) == 0.0 ? 1.0 : date.daysInBetweenDate(date)
                lblNoOfLeave?.text = "\(noOfLeaveDays) Days"
            }
            
            
            if let leaveTerm = response.leaveTerms{
                self.ForwardDays = leaveTerm.forwardDays
                self.BackwardDays = leaveTerm.backwardDays
                self.isPrevDatesAllow = leaveTerm.isPrevDatesAllow
                self.setUpDatePickerCondition()
            }
            
            if self.userType != "3" {
                if self.leaveReasonArr?.count != 0 {
                    _ = self.leaveReasonArr?.map({ _ in
                        self.reasonStatesArr.append(.no)
                    })
                }
            }
            
            reloadTable()
        }
    }
    
    
    func setUpDatePickerCondition(){
        self.prevDate = BackwardDays?.getFutureAndPrevDate(isPreviousAllowed: 1).0
        self.futureDate = ForwardDays?.getFutureAndPrevDate(isPreviousAllowed: 0).0
    }
    
    
    
    // MARK: - BUTTON ACTIONS
    //Segment Control
    @IBAction func btnSegmentAct(_ sender: Any){
        updateSegmentControl()
    }
    
    @IBAction func btnTermsAct(_ sender: UIButton) {
        let image = sender.currentImage == UIImage(named: "Uncheck") ?  UIImage(named:  "Check") : UIImage(named: "Uncheck")
        isTermSelected = sender.currentImage == UIImage(named: "Uncheck")
        sender.setImage(image, for: .normal)
        updateDoneButtonUI()
    }
    
    @IBAction func btnDateAct(_ sender: UIButton) {
        openDatePicker(tag: sender.tag)
    }
    
    @IBAction func btnDoneAct(_ sender: Any){
        self.view.endEditing(true)
        openConfirmVC()
    }
    
    @IBAction func btnHalfDaySelectAct(_ sender : UIButton){
        
        if self.halfDayArr?.count == 0 || self.halfDayArr == nil {
            intializeHalfDayArr { (arr) in
                self.halfDayArr = arr
            }
        }
        
        print("No of half Days allow to be taken :\(/self.halfDayArr?.count)")
        
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "SelectSubjectViewController") as? SelectSubjectViewController {
            vc.leaveDelegate     = self
            vc.fromLeave = true
            vc.halfDayArr = self.halfDayArr
            vc.tag             = 3
            vc.subjectArr      = []
            self.presentVC(vc)
        }
    }
    
    func intializeHalfDayArr(halfDayArr : @escaping (_ arr : [HalfDayModal]?) -> ()) {
        
        self.halfDayArr = []
        var arr = [HalfDayModal]()
        var startIndex : Int = 0
        var startDate = lastDatePrevSelected
        let noOfDays = lastDateFutureSelected.convertedDate.daysInBetweenDate(lastDatePrevSelected.convertedDate).toInt == 0 ? 1 : (lastDateFutureSelected.convertedDate.daysInBetweenDate(lastDatePrevSelected.convertedDate).toInt + 1)
        while startIndex < noOfDays{
            let modal = HalfDayModal.init(leaveDate_: startDate)
            startDate = startDate.addingTimeInterval(86400)
            arr.append(modal)
            startIndex = startIndex + 1
        }
        halfDayArr(arr)
    }
    
    func openConfirmVC() {
        if let leaveConfirmVC = storyboard?.instantiateViewController(withIdentifier: "LeaveConfirmPopUpViewController") as? LeaveConfirmPopUpViewController {
            leaveConfirmVC.lastDateFutureSelected = lastDateFutureSelected
            leaveConfirmVC.lastDatePrevSelected = lastDatePrevSelected
            leaveConfirmVC.reason = txtReason?.text
            leaveConfirmVC.delegate = self
            if self.userType == "3" {
                leaveConfirmVC.NoOfLeaves = self.noOfLeaveTaken().toString
                leaveConfirmVC.halfDayArr = self.selectHalfDayArr
                leaveConfirmVC.leaveID = self.leaveID
            }
            else {
                leaveConfirmVC.NoOfLeaves = btnNoOfLeave?.currentTitle
                leaveConfirmVC.halfDayArr = []
                leaveConfirmVC.leaveID = nil
            }
            
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                parent.hideBar(boolHide: true)
            }
            self.presentVC(leaveConfirmVC)
        }
    }
}


// Configure TableView of Leave Reason ( ON SEGEMENT CONTROL = 0 )
extension LeaveDetailsViewController {
    
    //MARK: - ConfigureTableView
    func configureLeaveReasonTable() {
        
        leaveReasontableDataSource = LeaveReasonTableDataSource(items: leaveReasonArr, height: UITableView.automaticDimension, tableView: tblLeaveReason, cellIdentifier: CellIdentifiers.LeaveReasonCell.get)
        
        leaveReasontableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            if let index = indexpath?.row {
                (cell as? LeaveReasonCell)?.delegate = self
                (cell as? LeaveReasonCell)?.index = index
                (cell as? LeaveReasonCell)?.states = self.reasonStatesArr[index]
                (cell as? LeaveReasonCell)?.reason = item
                if  /self.tblLeaveReason?.frame.height <= /self.tblLeaveReason?.contentSize.height {
                    self.tblLeaveReason?.isScrollEnabled = true
                }
            }
        }
        leaveReasontableDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
    }
}



extension LeaveDetailsViewController {
    func reloadTable() {
        if segmentLeave.selectedSegmentIndex == 0 {
            
            if self.userID != "3" {
                if isFirstTime {
                    configureLeaveReasonTable()
                    isFirstTime = false
                }
                else {
                    leaveReasontableDataSource?.items = leaveReasonArr
                    tblLeaveReason?.reloadData()
                }
            }
        }
        else {
            
            lblNoData?.isHidden = !(leaveListArr?.count == 0 || leaveListArr?.count == nil)
            imgNoData?.isHidden = !(leaveListArr?.count == 0 || leaveListArr?.count == nil)
            if let btn = btnLeaveApply {
                self.view.bringSubviewToFront(btn)
            }
            
            if isFirstTime {
                configureLeaveDataTable()
                isFirstTime = false
            }
            else{
                leaveDetailstableDataSource?.items = leaveListArr
                tblLeaveDetails?.reloadData()
            }
        }
        
        if fromBoardScreen {
            configureLeaveReasonTable()
        }
    }
    
    
    func updateSegmentControl() {
        if segmentLeave.selectedSegmentIndex == 0 {
            lblNoData?.isHidden = true
            imgNoData?.isHidden = true
            //
        }
        btnDone?.isHidden = segmentLeave?.selectedSegmentIndex == 1
        tblLeaveDetails?.isHidden = segmentLeave.selectedSegmentIndex == 0
        viewLeaveRequest?.isHidden = segmentLeave.selectedSegmentIndex != 0
        self.view.bringSubviewToFront((segmentLeave.selectedSegmentIndex == 0 ? tblLeaveDetails : viewLeaveRequest) ?? UIView())
        if segmentLeave.selectedSegmentIndex == 1 {
            getLeaveList(pageNo : 1)
        }
        if self.userType == "3" {
            lblMainHeading?.text = self.segmentLeave.selectedSegmentIndex == 0 ? "Leave Request" : "Leave History"
        }
        else {
            lblMainHeading?.text = "Leave Request"
        }
        btnLeaveApply?.isHidden = !(self.userType == "3" && self.segmentLeave.selectedSegmentIndex == 1)
    }
}


//MARK::- Leave List -(Status API)
extension LeaveDetailsViewController {
    
    func getLeaveList(pageNo : Int) {
        APIManager.shared.request(with: HomeEndpoint.LeaveStatus(UserID: String(self.getCurrentUser()?.userID ?? 0) , UserType : String(self.getCurrentUser()?.userType ?? 0), pageNo : nil), isLoader : isFirstTime) {[weak self] (response) in
            self?.refreshControl.endRefreshing()
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handleLeaveListResponse(response: response, pageNo : pageNo)
            })
        }
    }
    
    func handleLeaveListResponse(response: Any? ,pageNo : Int){
        if let response = response as? LeaveDetailsModel , let leaveList = response.leaveList{
            //            print("Data from page No :\(pageNo)")
            self.leaveListArr = leaveList
            tblViewHeightConst.constant = CGFloat(/self.leaveReasonArr?.count * 32)
            reloadTable()
        }
    }
    
}

// Configure TableView of Leave Data ( ON SEGEMENT CONTROL = 1 )
extension LeaveDetailsViewController {
    
    //MARK: - ConfigureTableView
    func configureLeaveDataTable() {
        leaveDetailstableDataSource = LeaveDetailsTableDataSource(items: leaveListArr, height: UITableView.automaticDimension, tableView: tblLeaveDetails, cellIdentifier: CellIdentifiers.LeaveDetailCell.get)
        
        leaveDetailstableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? LeaveDetailCell)?.index  = indexpath?.row
            (cell as? LeaveDetailCell)?.model = item
            (cell as? LeaveDetailCell)?.delegate = self
            
        }
    }
}

extension LeaveDetailsViewController : DeleteLeaveDelegate {
    
    func deleteLeave(index : Int){
        if index <= /leaveListArr?.count {
            deleteAPI(leaveListArr?[index].lvID , index)
        }
    }
    
    func deleteAPI(_ leaveID : Int? ,_ index : Int?) {
        if let Id = leaveID ,let index = index {
            APIManager.shared.request(with: HomeEndpoint.LeaveDelete(LvID: Id.toString , UserType: self.userType)) {[weak self] (response) in
                self?.handleResponse(response: response, responseBack: { (response) in
                    if let str = response as? String {
                        self?.leaveListArr?.remove(at: index)
                        self?.reloadTable()
                        Messages.shared.show(alert: .success , message: str, type: .success)
                        
                    }
                })
            }
        }
    }
}

//MARK::- PAGINATION METHODS
extension LeaveDetailsViewController {
    
    func addRefereshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tblLeaveDetails?.addSubview(refreshControl)
    }
    
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        pageNo  = 1
        self.getLeaveList(pageNo : pageNo)
    }
    
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            pageNo = pageNo + 1
            self.getLeaveList(pageNo:pageNo)
        }
    }
    
}

//MARK::- TextFieldDelegate
extension LeaveDetailsViewController : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        btnDone.setTitleColor(UIColor.flatGray, for: .normal)
        btnDone.isUserInteractionEnabled = false
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
        updateDoneButtonUI()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func updateDoneButtonUI() {
//        self.view.endEditing(true)
        let state = ((/txtReason?.text?.count != 0) && isTermSelected)
        btnDone.setTitleColor(state ? UIColor.flatGreen : UIColor.flatGray, for: .normal)
        btnDone.isUserInteractionEnabled = state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtReason , let text = txtReason?.text{
            if text.lowercased() != string.lowercased(){
                self.reasonStatesArr = self.reasonStatesArr.map({ (state) -> States in
                    return .no
                })
                reloadTable()
            }
            if string == "" && text.count == 1 {
                textField.text = nil
            }
            updateDoneButtonUI()
            return true
        }
        return true
    }
}


extension LeaveDetailsViewController : ClearanceDelegate {
    func clearData() {
        
        if segmentLeave.selectedSegmentIndex == 0 {
            txtReason?.text = nil
            isTermSelected = false
            btnTermCondition?.setImage(UIImage(named: "Uncheck"), for: .normal)
            updateDoneButtonUI()
            self.lblFromDate?.text = self.serverDate?.dateChangeFormat()
            self.lblToDate?.text = self.serverDate?.dateChangeFormat()
            guard let serverdate = self.serverDate?.convertStringIntoDate() else {return}
            let noOfLeaveDays = String(format: "%02d", serverdate.daysInBetweenDate(serverdate).toInt == 0 ? 1 : serverdate.daysInBetweenDate(serverdate).toInt)
            self.btnNoOfLeave?.setTitle(noOfLeaveDays, for: .normal)
            if userType == "3" {
                self.lblNoOfLeave?.text = "\(/noOfLeaveDays.toDouble()) Days"
            }
            for (index,_) in self.reasonStatesArr.enumerated(){
                self.reasonStatesArr[index] = .no
            }
            self.segmentLeave.selectedSegmentIndex = userType == "3"  ? 1 : 0
            self.lastDatePrevSelected = Date()
            self.lastDateFutureSelected = Date()
            self.selectHalfDayArr = []
            self.halfDayArr = []
            self.collectionHeight?.constant = self.selectHalfDayArr?.count == 0 ? 0.0 : 60.0
            self.reloadCollection()
            if userType == "3" {
                self.updateSegmentControl()
            }
            self.leaveTypeView?.isHidden = true
            self.reloadTable()
            //            self.btnLeaveApply?.isHidden = userType != "3"
        }
    }
}


extension LeaveDetailsViewController : SelectLeaveApplyDelegate {
    
    func leaveDetails(_ leaveDetails: LeaveType?) {
        guard let leaveDTL = leaveDetails else {
            leaveTypeView?.isHidden = true
            return
        }
        self.leaveID = /leaveDTL.leaveId
        lblLeaveName?.text = /leaveDTL.name
        leaveTypeView?.isHidden = false
        self.selectHalfDayArr = self.getSelectHalfDay()
        self.collectionHeight?.constant = (self.selectHalfDayArr?.count == 0 || self.selectHalfDayArr == nil) ? 0.0 : 60.0
        self.view.bringSubviewToFront(leaveTypeView!)
        fromBoardScreen = false
        
        segmentLeave.selectedSegmentIndex = 0
        updateSegmentControl()
        
    }
}

extension LeaveDetailsViewController : SelectHalfDayDelegate{
    func passHalfDays(halfDayArr: [HalfDayModal]?) {
        self.halfDayArr = halfDayArr
        self.selectHalfDayArr = self.getSelectHalfDay()
        self.lblNoOfLeave?.text = "\(noOfLeaveTaken()) Days"
       
       
        print("@@@ \(/self.selectHalfDayArr?.count)")
        ez.runThisInMainThread {
        self.collectionHeight?.constant = 60.0
//       self.view.layoutSubviews()
//       self.view.layoutIfNeeded()
//             self.collectionHeight?.constant = (self.selectHalfDayArr?.count == 0 || self.selectHalfDayArr?.count == nil) ? 0.0 : 60.0
            self.reloadCollection()
        }
    }
    
    
    func noOfLeaveTaken() -> Double{
        if self.halfDayArr?.count != 0 && self.halfDayArr != nil {
            let totalDaysLeaveApply = /self.halfDayArr?.count.toDouble
            self.selectHalfDayArr = self.getSelectHalfDay()
            let selectHalfDay = self.selectHalfDayArr?.count.toDouble
            return totalDaysLeaveApply - (/selectHalfDay * 0.5)
        }
        else {
            let leaveDays = lastDateFutureSelected.convertedDate.daysInBetweenDate(lastDatePrevSelected.convertedDate)
            print("leaveDays : \(leaveDays)")
            if leaveDays  == 0.0  {
                return 1.0
            }
            else {
                return leaveDays + 1.0
            }
        }
    }
}


//UICollection View Delegate and data source
extension LeaveDetailsViewController {
    
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: self.selectHalfDayArr, collectionView: collectionView, cellIdentifier: CellIdentifiers.HalfDayCell.get, headerIdentifier: nil, cellHeight: 60.0 , cellWidth: 270.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell         = cell as? HalfDayCell , let index  = indexpath?.row else {return}
            cell.delegate           = self
            cell.btnCross?.tag = index
            cell.modal = item
        }
    }
    
    func reloadCollection() {
        if isFirstTimeReloadCollection {
            configureCollectionView()
//            isFirstTimeReloadCollection = isFirstTimeReloadCollection
        }
        else {
            collectionDataSource?.items = self.selectHalfDayArr
            collectionView?.reloadData()
        }
    }
    
    func getSelectHalfDay() -> [HalfDayModal]? {
        
        let arr = self.halfDayArr?.filter({ (halfDay) -> Bool in
            return halfDay.halfType != -1
        })
        print("Select halfDay : \(/arr?.count)")
//        self.collectionView?.isHidden = arr?.count == 0
        return arr
    }
}


extension LeaveDetailsViewController : DeleteHalfDayDelegate {
    
    func deleteHalfDay(index: Int?) {
        if self.selectHalfDayArr?.count != 0 {
            let leaveDate = self.selectHalfDayArr?[/index].leaveDate
            self.selectHalfDayArr?.remove(at: /index)
            self.halfDayArr = self.halfDayArr?.map({ (modal) -> (HalfDayModal) in
                if modal.leaveDate == leaveDate {
                    modal.halfType = -1
                }
                return modal
            })
            self.collectionHeight?.constant = (self.selectHalfDayArr?.count == 0 || self.selectHalfDayArr == nil) ? 0.0 : 60.0
            self.view.layoutIfNeeded()
            self.lblNoOfLeave?.text = "\(noOfLeaveTaken()) Days"
            self.reloadCollection()
        }
    }
}

