//  LeaveModal.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 23/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class DTLModel{

    var LvID            :Int
    var FromDate        :String
    var TillDate        :String
    var SubmittedOn     :String
    var Duration        :Float
    var Reason          :String
    var ApplicantName   :String
    var ApplicantPhoto  :String
    var StudentName     :String
    var StudentPhoto    :String
    var status          :String
    var ActionOn        :String
    var SID             :Int
    var TeacherName     :String
    var Designation     :String
    var Photo           :String

    init(LvID:Int,FromDate:String, TillDate: String, SubmittedOn: String,Duration: Float, Reason: String, ApplicantName: String, ApplicantPhoto: String, StudentName: String, StudentPhoto: String, status: String, ActionOn: String, SID: Int, TeacherName: String, Designation: String, Photo: String) {

        self.LvID            = LvID
        self.FromDate        = FromDate
        self.TillDate        = TillDate
        self.SubmittedOn     = SubmittedOn
        self.Duration        = Duration
        self.Reason          = Reason
        self.ApplicantName   = ApplicantName
        self.ApplicantPhoto  = ApplicantPhoto
        self.StudentName     = StudentName
        self.StudentPhoto    = StudentPhoto
        self.status          = status
        self.ActionOn        = ActionOn
        self.SID             = SID
        self.TeacherName     = TeacherName
        self.Designation     = Designation
        self.Photo           = Photo
    }
}
class LeaveModal: NSObject {

    var ErrorCode   :Int
    var Status      :String
    var Message     :String

    var dtl         = [DTLModel]()
    
    init(dic:[String:Any])
    {
        self.ErrorCode  = dic["ErrorCode"] as? Int    ?? 0
        self.Status     = dic["Status"]    as? String ?? ""
        self.Message    = dic["Message"]   as? String ?? ""

        if let dtl = dic["DTL"] as? [[String:Any]]{
            
            self.dtl.removeAll(keepingCapacity: false)
            for rec in dtl{
                
                let LvID           = rec["LvID"]          as? Int    ?? 0
                let strFromDate    = rec["FromDate"]      as? String ?? ""
                let strTillDate    = rec["TillDate"]      as? String ?? ""
                let strSubmittedOn = rec["SubmittedOn"]   as? String ?? ""
                let Duration       = rec["Duration"]      as? Float  ?? 0.0
                let Reason         = rec["Reason"]        as? String ?? ""
                let ApplicantName  = rec["ApplicantName"] as? String ?? ""
                let ApplicantPhoto = rec["ApplicantPhoto"]as? String ?? ""
                let StudentName    = rec["StudentName"]   as? String ?? ""
                let StudentPhoto   = rec["StudentPhoto"]  as? String ?? ""
                let status         = rec["status"]        as? String ?? ""
                let strActionOn    = rec["ActionOn"]      as? String ?? ""
                let SID            = rec["SID"]           as? Int    ?? 0
                let TeacherName    = rec["TeacherName"]   as? String ?? ""
                let Designation    = rec["Designation"]   as? String ?? ""
                let Photo          = rec["Photo"]         as? String ?? ""

                let FromDate    = strFromDate.dateChangeFormat()
                let TillDate    = strTillDate.dateChangeFormat()
                let SubmittedOn = strSubmittedOn.dateChangeFormat()
                let ActionOn    = strActionOn.dateChangeFormat()

                let model  = DTLModel(LvID: LvID,FromDate: FromDate,TillDate: TillDate,SubmittedOn: SubmittedOn,Duration: Duration,Reason: Reason,ApplicantName: ApplicantName,ApplicantPhoto: ApplicantPhoto,StudentName: StudentName, StudentPhoto: StudentPhoto, status: status, ActionOn: ActionOn, SID: SID, TeacherName: TeacherName,Designation: Designation,Photo: Photo)
                //dateChangeFormat
                self.dtl.append(model)
            }
        }
        print("self.rpt = \(self.dtl.count)")
        print("self.rpt = \(self.dtl)")
        //print("self.rpt = \(self.rpt[0].date)")
    }
}
