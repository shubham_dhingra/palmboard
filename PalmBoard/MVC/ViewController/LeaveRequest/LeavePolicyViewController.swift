//  LeavePolicyViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 27/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class LeavePolicyViewController: BaseViewController
{
    @IBOutlet weak var lblTermAConditions:        UILabel?
    @IBOutlet weak var lblDetailsTermAConditions: UILabel?
    @IBOutlet weak var lblImpNotes:               UILabel?
    @IBOutlet weak var lblDetailsImpNotes:        UILabel?
    @IBOutlet weak var lblRuleForLeaves:          UILabel?
    @IBOutlet weak var lblDetailsRuleForLeaves:   UILabel?
    @IBOutlet weak var btnTermCondition:          UIButton?
    @IBOutlet weak var btnAgree:                  UIButton?
    
    var termsAndCondtion : TermCondition?
    var forLeavePolicy : Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    //    setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onViewWillAppear()
    }
    
    func onViewWillAppear(){
        if let termsCondition = termsAndCondtion {
            lblDetailsImpNotes?.text        = termsCondition.notes
            lblDetailsRuleForLeaves?.text   = termsCondition.rules
            lblDetailsTermAConditions?.text = termsCondition.terms
            lblTermAConditions?.text = R.string.localize.termsAndConditions()
        }
        
        if !forLeavePolicy {
            lblTermAConditions?.text = R.string.localize.information()
            let string1 = Utility.shared.getAttributedString(firstStr: R.string.localize.forParents(), firstAttr: [NSAttributedString.Key.font: R.font.ubuntuBold(size: 16.0)!], secondStr: R.string.localize.switchAccDescPart1(), secondAttr: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 16.0)!])
            let string2 = Utility.shared.getAttributedString(firstStr: R.string.localize.teacherOrParent(), firstAttr: [NSAttributedString.Key.font: R.font.ubuntuBold(size: 16.0)!], secondStr: R.string.localize.switchAccDescPart2(), secondAttr: [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 16.0)!])
            
            let combination = NSMutableAttributedString()
            combination.append(string1)
            combination.append(string2)
            lblDetailsTermAConditions?.attributedText = combination
        }
        
        lblImpNotes?.isHidden = !forLeavePolicy
        lblDetailsImpNotes?.isHidden = !forLeavePolicy
        lblRuleForLeaves?.isHidden = !forLeavePolicy
        lblDetailsRuleForLeaves?.isHidden = !forLeavePolicy
        btnTermCondition?.isHidden = !forLeavePolicy
        btnAgree?.isHidden = !forLeavePolicy

        
    }
    
    func setUpView() {
        btnAgree?.layer.cornerRadius = 8.0
        btnAgree?.layer.borderWidth  = 1.0
        btnAgree?.layer.borderColor  = UIColor.white.cgColor
        btnAgree?.clipsToBounds      = true
    }

    @IBAction func ClkBtnCross(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func ClkBtnTerm(_ sender: UIButton) {
        if sender.hasImage(named: "checkbox-unfill", for: .normal) {
            print("YES")
            sender.setImage(R.image.checkboxFill(), for: UIControl.State.normal)

        } else {
            print("NO")
            sender.setImage(R.image.checkboxUnfill(), for: UIControl.State.normal)
        }
    }
    @IBAction func ClkBtnAgree(_ sender: Any){
        self.dismiss(animated: true, completion: nil)

    }
}
