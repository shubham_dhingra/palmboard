//  LeaveRequestViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 21/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
protocol HideBarDelegateLeave: class{
    func hideBar(boolHide: Bool)
}

class LeaveRequestViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    weak var hideBarDelegate: HideBarDelegateLeave?
    var btnTitle            = UIButton()
    var btnNext             = UIButton()

    @IBOutlet weak var segmentLeave: UISegmentedControl?
    @IBOutlet weak var viewSegment:  UIView?
    @IBOutlet weak var viewCalendar: UIView?
    @IBOutlet weak var tblLeave:     UITableView?
    var strSender      = String()
    var intUserID      = Int()
    var intUserType    = Int()
    var listData :  LeaveModal!

    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
       
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Leave Application", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        
        self.navigationItem.titleView = btnTitle
        
        btnNext                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnNext.frame            = CGRect(x : 0,y:  0,width : 60, height: 40)
        btnNext.setTitle("Next", for: UIControl.State.normal)
        btnNext.titleLabel?.font =  R.font.ubuntuRegular(size: 15)
        btnNext.setTitleColor(UIColor(rgb: 0xBFBFBF), for: .normal)
        btnNext.addTarget(self, action: #selector(self.ClkBtnNext(sender:)), for: .touchUpInside)

        let barButton = UIBarButtonItem.init(customView: btnNext)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.tblLeave?.delegate    = nil
        self.tblLeave?.dataSource  = nil

    }
    
    //
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // HideBarDelegateSyllabus?.hideBar(boolHide: false)
        hideBarDelegate?.hideBar(boolHide: false)
        segmentLeave?.selectedSegmentIndex = 0
        self.getLeaveData()
    }
   
    // MARK: - ClkSegment
    @IBAction func ClkSegment(_ sender: Any)
    {
        switch segmentLeave?.selectedSegmentIndex
        {
        case 0:
            tblLeave?.isHidden         = true
            viewCalendar?.isHidden     = true
        case 1:
            tblLeave?.isHidden         = false
            viewCalendar?.isHidden     = true
        default:
            break
        }
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.tblLeave?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnNext(sender:UIButton) {
        self.performSegue(withIdentifier: "leaveSegue", sender: nil)
    }
    // MARK: - getLeaveData
    func getLeaveData()
    {
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "leave/status?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)"//\(intUserID)
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                if let result = results{
                    
                    if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                    {
                        // print("Result Attendance== \(result)")
                        if let arrayAll = (result["DTL"] as? [[String: Any]]){
                            //self.collectionViewActivity.isHidden   = false
                            self.loadListData(dataDict: result)
                        }
                        else{
                            let alert = UIAlertController(title: "", message: "Leave data is not found", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        let alert = UIAlertController(title: "", message: "Something went wrong", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    // MARK: - loadListData
    func loadListData(dataDict : [String:Any])  {
        listData = LeaveModal(dic: dataDict)
        DispatchQueue.main.async(execute: {
            self.loadUI()
        })
    }
    // MARK: - loadUI
    func loadUI()  {
        
        DispatchQueue.main.async(execute: {
            self.tblLeave?.delegate    = self
            self.tblLeave?.dataSource  = self
            self.tblLeave?.reloadData()
        })
    }
    // MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.dtl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LeaveTableViewCell
        
        cell.lblName?.text             = listData.dtl[indexPath.row].ApplicantName
        cell.lblAppliedDate?.text      = listData.dtl[indexPath.row].SubmittedOn
        cell.lblFromDate?.text         = listData.dtl[indexPath.row].FromDate
        cell.lblToDate?.text           = listData.dtl[indexPath.row].TillDate
        cell.lblTotalLeaveServer?.text = "\(listData.dtl[indexPath.row].Duration)"
        cell.lblReasonServer?.text     = listData.dtl[indexPath.row].Reason
        cell.lblApproved?.text         = listData.dtl[indexPath.row].status
        cell.lblApprovedDate?.text     = listData.dtl[indexPath.row].ActionOn
        cell.lblApprovedBy?.text       = listData.dtl[indexPath.row].TeacherName
        //  cell.btnDelete                = listData.dtl[indexPath.row].
        // cell.btnProfile               = listData.dtl[indexPath.row].
        
        switch cell.lblApproved?.text {
        case "Pending"?:
            cell.lblApproved?.backgroundColor   = UIColor(rgb: 0x3982EF)
            cell.btnDelete?.isHidden            = false
            cell.btnProfileApprovedBy?.isHidden = true
            cell.lblRejectedBy?.isHidden        = true
            cell.lblApprovedBy?.isHidden        = true
            cell.heightBottomCell?.constant     = 0
            cell.heightLblRejected?.constant    = 0
            cell.heightLblApproved?.constant    = 0
            
        case "Approved"?:
            cell.lblApproved?.backgroundColor   = UIColor(rgb: 0x4BD964)
            cell.btnDelete?.isHidden            = true
            cell.btnProfileApprovedBy?.isHidden = false
            cell.lblApprovedBy?.isHidden        = false
            cell.lblRejectedBy?.isHidden        = false
            cell.lblRejectedBy?.text            = "Approved By"

            cell.heightBottomCell?.constant     = 14.5
            cell.heightLblRejected?.constant    = 10.5
            cell.heightLblApproved?.constant    = 15
            
        case "Rejected"?:
            cell.lblApproved?.backgroundColor   = UIColor(rgb: 0xFB4E4E)
            cell.btnDelete?.isHidden            = true
            cell.btnProfileApprovedBy?.isHidden = false
            cell.lblApprovedBy?.isHidden        = false
            cell.lblRejectedBy?.isHidden        = false
            cell.lblRejectedBy?.text            = "Rejected By"

            cell.heightBottomCell?.constant     = 14.5
            cell.heightLblRejected?.constant    = 10.5
            cell.heightLblApproved?.constant    = 15
            
        default:
            break
        }
        
        var strPrefix          = String()
        var strPrefixProfle    = String()
        
        var strPhotoURL        = String()
        strPhotoURL            = listData.dtl[indexPath.row].Photo
        var strPhotoURLProfile = String()
        strPhotoURLProfile     = listData.dtl[indexPath.row].ApplicantPhoto
        
        let imgViewProfileApprovedBy = UIImageView()
        let imgViewProfile           = UIImageView()
        
        strPrefixProfle += strPrefixProfle.imgPath1()
        strPrefix       += strPrefix.imgPath1()
        var finalUrl    = "\(strPrefix)" +  "\(strPhotoURL)"
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlPhoto    = URL(string: finalUrl)
        
        var finalUrlProfile    = "\(strPrefixProfle)" +  "\(strPhotoURLProfile)"
        finalUrlProfile = finalUrlProfile.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlPhotoProfile    = URL(string: finalUrlProfile)
        
        //print("urlPhotoProfile = \(urlPhotoProfile)")
        //print("urlPhoto = \(urlPhoto)")
        
        imgViewProfileApprovedBy.af_setImage(withURL: urlPhoto!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            
            let img1 = imgViewProfileApprovedBy.image?.af_imageRoundedIntoCircle()
            //self.imgView.image = img1
            cell.btnProfileApprovedBy?.setBackgroundImage(img1 as UIImage?, for: .normal)
        }
        
        imgViewProfile.af_setImage(withURL: urlPhotoProfile!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            
            let img1 = imgViewProfile.image?.af_imageRoundedIntoCircle()
            //self.imgView.image = img1
            cell.btnProfile?.setBackgroundImage(img1 as UIImage?, for: .normal)
        }
        // cell.btnProfileApprovedBy     = listData.dtl[indexPath.row].Photo
        return cell
    }
}
