//
//  LeaveSetting.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 28/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class LeaveReasonModel{
    var Reason          :[String]
    init( Reason: [String]) {
        self.Reason          = Reason
    }
}


class LeaveTermsModel{
    
    var DaysLimit        :Int
    var isPrevDatesAllow :Int
    var ForwardDays      :Int
    var BackwardDays     :Int
    var WeekOff          :[String]

    init(DaysLimit:Int,isPrevDatesAllow:Int, ForwardDays: Int, BackwardDays: Int, WeekOff: [String]) {
        
        self.DaysLimit        = DaysLimit
        self.isPrevDatesAllow = isPrevDatesAllow
        self.ForwardDays      = ForwardDays
        self.BackwardDays     = BackwardDays
        self.WeekOff          = WeekOff
    }
}
class LeaveSeetingModel{
    
    var Title      :String
    var FromDate   :String
    var TillDate   :String
    var Duration   :Float
    
    init(Title:String,FromDate:String, TillDate: String, Duration: Float) {
        
        self.Title        = Title
        self.FromDate     = FromDate
        self.TillDate     = TillDate
        self.Duration     = Duration
    }
}
class LeaveSetting: NSObject {
    var ErrorCode   :Int
    var Status      :String
    var Message     :String
   // var Reason      :[String]
    var ServerDate     :String

    var holiday         = [LeaveSeetingModel]()
    var LeaveTerms      = [LeaveTermsModel]()
    var LeaveReason     = [LeaveReasonModel]()

    init(dic:[String:Any])
    {
        self.ErrorCode  = dic["ErrorCode"] as? Int    ?? 0
        self.Status     = dic["Status"]    as? String ?? ""
        self.Message    = dic["Message"]   as? String ?? ""
        self.ServerDate = dic["ServerDate"]as? String ?? ""

        if let LeaveReason = dic["LeaveReason"] as? [String:Any]{
            self.LeaveReason.removeAll(keepingCapacity: false)

            var Reason      = [String]()
            if let Reason1  = LeaveReason["Reason"]          as? [String]{
                Reason      = Reason1
            }
            let model = LeaveReasonModel(Reason: Reason)
            self.LeaveReason.append(model)
        }

        if let holiday1 = dic["HolidayList"] as? [String:Any]{
            
            if let holiday = holiday1["Holiday"] as? [[String:Any]]
            {
                self.holiday.removeAll(keepingCapacity: false)
                for rec in holiday{
                    
                    let Title          = rec["Title"]     as? String    ?? ""
                    let strFromDate    = rec["FromDate"]  as? String ?? ""
                    let strTillDate    = rec["TillDate"]  as? String ?? ""
                    let Duration       = rec["Duration"]  as? Float  ?? 0.0
                    
                    let FromDate       = strFromDate.dateChangeFormat()
                    let TillDate       = strTillDate.dateChangeFormat()
                    
                    let model = LeaveSeetingModel(Title: Title, FromDate: FromDate, TillDate: TillDate, Duration: Duration)
                    self.holiday.append(model)
                }
            }
            else{
                print("No Holidays found in leave setting")
            }
        }
        if let LeaveTerms = dic["LeaveTerms"] as? [String:Any]{
            self.LeaveTerms.removeAll(keepingCapacity: false)

            let DaysLimit        = LeaveTerms["DaysLimit"]         as? Int  ?? 0
            let isPrevDatesAllow = LeaveTerms["isPrevDatesAllow"]  as? Int  ?? 0
            let ForwardDays      = LeaveTerms["ForwardDays"]       as? Int  ?? 0
            let BackwardDays     = LeaveTerms["BackwardDays"]      as? Int  ?? 0
            var WeekOff          = [String]()
            if let WeekOffArray  = LeaveTerms["WeekOff"]           as? [String]{
                WeekOff          = WeekOffArray
            }
            let model = LeaveTermsModel(DaysLimit: DaysLimit, isPrevDatesAllow: isPrevDatesAllow, ForwardDays: ForwardDays, BackwardDays: BackwardDays, WeekOff: WeekOff)
            self.LeaveTerms.append(model)
        }
        print("self.holiday.count = \(self.holiday.count)")
        print("self.holiday = \(self.holiday)")
        
        print("self.LeaveTerms.count = \(self.LeaveTerms.count)")
        print("self.LeaveTerms = \(self.LeaveTerms)")
        
        print("self.LeaveReason.count = \(self.LeaveReason.count)")
        print("self.LeaveReason = \(self.LeaveReason)")

        //print("self.rpt = \(self.rpt[0].date)")
    }
}

