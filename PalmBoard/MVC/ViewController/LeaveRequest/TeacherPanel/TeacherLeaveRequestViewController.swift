//
//  TeacherLeaveRequestViewController.swift
//  PalmBoard
//
//  Created by Shubham on 05/02/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit
protocol SelectLeaveApplyDelegate {
    func leaveDetails(_ leaveDetails : LeaveType?)
}
class TeacherLeaveRequestViewController: BaseViewController {
    
    @IBOutlet weak var txtLeaveType : UITextField?
    @IBOutlet weak var btnRequest : UIButton?
    
    var leaveNameArr = [String]()
    var pickerView : UIPickerView!
    var isFirstTime : Bool = true
    var leaveTypeArr : [LeaveType]?
    var selectLeave : LeaveType?
    var delegate   : SelectLeaveApplyDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnRequest?.isUserInteractionEnabled = false
        onviewDidLoad()
    }
    
    
    func onviewDidLoad() {
        leaveNameArr.append(R.string.localize.selectLeaveType())
        if leaveTypeArr?.count != 0 {
            for (_ , leaveType) in (leaveTypeArr ?? []).enumerated() {
                leaveNameArr.append(/leaveType.name)
            }
            reloadTable()
        }
    }
    
    @IBAction func btnRequestAct(_ sender : UIButton){
        
        if delegate != nil && selectLeave != nil {
            delegate?.leaveDetails(selectLeave)
            self.navigationController?.popViewController(animated: false)
        }
    }
}

//MARK::- TableView Delegate and Data Source
extension TeacherLeaveRequestViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: leaveTypeArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.LeaveTypeCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? LeaveTypeCell)?.model = item
        }
        
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(index : indexpath.row)
        }
        
    }
    
    func didSelect(index : Int){
        
        if index < /self.leaveTypeArr?.count && self.leaveTypeArr?.count != 0 {
            self.leaveTypeArr?[index].isExpand = !(/self.leaveTypeArr?[index].isExpand)
            self.reloadTable()
        }
    }
    
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = leaveTypeArr
            tableView?.reloadData()
        }
    }
}

extension TeacherLeaveRequestViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtLeaveType {
            self.pickUp(textField)
        }
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.backgroundColor = UIColor.flatGray
        textField.inputView = self.pickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.flatGreen
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: AlertConstants.Done.get, style: .plain, target: self, action: #selector(self.doneClick))
        doneButton.tintColor = UIColor.flatGreen
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        cancelButton.tintColor = UIColor.flatGreen
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
}


extension TeacherLeaveRequestViewController : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return /leaveNameArr.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return leaveNameArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.btnRequest?.isUserInteractionEnabled = true
        self.btnRequest?.setTitleColor(UIColor.flatGreen, for: .normal)
        self.selectLeave =  row == 0  ? nil : self.leaveTypeArr?[row - 1]
        self.txtLeaveType?.text =  leaveNameArr[row]
    }
    
    @objc func doneClick() {
        actionPerform()
    }
    
    @objc func cancelClick() {
        actionPerform()
    }
    
    func actionPerform() {
        self.txtLeaveType?.resignFirstResponder()
    }
}



