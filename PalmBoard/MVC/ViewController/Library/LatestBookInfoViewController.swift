//  LatestBookInfoViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 18/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class LatestBookInfoViewController: UIViewController {
    
    @IBOutlet weak var imgViewBookCover:   UIImageView!
    @IBOutlet weak var lblAccessionNo:     UILabel!
    @IBOutlet weak var lblBookTitle:       UILabel!
    @IBOutlet weak var lblAuthor:          UILabel!
    @IBOutlet weak var lblSubject:         UILabel!
    @IBOutlet weak var lblStorageHint:     UILabel!
    @IBOutlet weak var lblYearPublication: UILabel!
    @IBOutlet weak var lblBookNo:          UILabel!
    @IBOutlet weak var lblClassNo:         UILabel!
    @IBOutlet weak var lblIssuable:        UILabel!
    
    var arrayBookDetails   = [[String: Any]]()
    var intBookID          = Int()
    var strSchoolCodeSender      = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "e-Library"
        // HideBarDelegateSyllabus?.hideBar(boolHide: false)
        // hideBarDelegate?.hideBar(boolHide: false)
        self.hideMenu()
        self.webAPIBookDetails()
    }
    // MARK: -  webAPILibrary
    func webAPIBookDetails()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Library/BookDTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&BookID=\(intBookID)"
        
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()

                        if let arrayAll = (result["BookDTL"] as? [[String: Any]]){
                            self.arrayBookDetails = arrayAll
                            self.updateUI()
                        }
                        else
                        {
                            WebserviceManager.showAlert(message: "", title: "No book available", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                            }, secondBtnBlock: {})
                        }
                    }
                }
                else{
                    Utility.shared.removeLoader()
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    
    func updateUI()
    {
        if let AccessionNumber  = self.arrayBookDetails[0]["AccessionNumber"] as? String{
            lblAccessionNo.text = "Accession No: \(AccessionNumber)"
            let amountText = NSMutableAttributedString.init(string: lblAccessionNo.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 13))
            lblAccessionNo.attributedText = amountText
            
        }
        if let Title  = self.arrayBookDetails[0]["Title"] as? String{
            
            lblBookTitle.text = "Book Title: " + Title
            let amountText = NSMutableAttributedString.init(string: lblBookTitle.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 11))
            lblBookTitle.attributedText = amountText
        }
        if let Author  = self.arrayBookDetails[0]["Author"] as? String{
            lblAuthor.text = "Author: " + Author
            let amountText = NSMutableAttributedString.init(string: lblAuthor.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 7))
            lblAuthor.attributedText = amountText
        }
        if let Subject  = self.arrayBookDetails[0]["Subject"] as? String{
            lblSubject.text = "Subject: " + Subject
            let amountText = NSMutableAttributedString.init(string: lblSubject.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 7))
            lblSubject.attributedText = amountText
        }
        if let StorageHint  = self.arrayBookDetails[0]["StorageHint"] as? String{
            lblStorageHint.text = "Storage Hint: " + StorageHint
            let amountText = NSMutableAttributedString.init(string: lblStorageHint.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 13))
            lblStorageHint.attributedText = amountText
        }
        if let PublicationYear  = self.arrayBookDetails[0]["PublicationYear"] as? String{
            lblYearPublication.text = "Year of Publication: " + PublicationYear
            let amountText = NSMutableAttributedString.init(string: lblYearPublication.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 20))
            lblYearPublication.attributedText = amountText
        }
        if let BookNo  = self.arrayBookDetails[0]["BookNo"] as? String{
            lblBookNo.text = "Book No: " + BookNo
            let amountText = NSMutableAttributedString.init(string: lblBookNo.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 8))
            lblBookNo.attributedText = amountText
        }
        if let ClassNo  = self.arrayBookDetails[0]["ClassNo"] as? String{
            lblClassNo.text = "Class No: " + ClassNo
            let amountText = NSMutableAttributedString.init(string: lblClassNo.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 9))
            lblClassNo.attributedText = amountText
        }
        if let isIssuable  = self.arrayBookDetails[0]["isIssuable"] as? Int{
            let result = isIssuable == 1 ? "Yes" : "No"
            lblIssuable.text = "Issuable: " + "\(result)"
            let amountText = NSMutableAttributedString.init(string: lblIssuable.text!)
            let attributes = [NSAttributedString.Key.font: R.font.ubuntuRegular(size: 15.0)!,
                              NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)]
            amountText.setAttributes(attributes, range:  NSMakeRange(0, 9))
            lblIssuable.attributedText = amountText
        }
        if let imgUrl = self.arrayBookDetails[0]["CoverImg"] as? String
        {
            var strPrefix      = String()
            strPrefix          += strPrefix.imgPath()
            var finalUrlString = "\(imgUrl)"
            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url            = URL(string: finalUrlString )
            let imgViewUser = UIImageView()
            imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.libraryNoImage(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                
//                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                self.imgViewBookCover.image = imgViewUser.image
            }
        }
    }
}
