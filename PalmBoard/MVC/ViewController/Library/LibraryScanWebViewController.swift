//  LibraryScanWebViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 27/09/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class LibraryScanWebViewController: BaseViewController,UIWebViewDelegate,UIScrollViewDelegate {
    @IBOutlet weak var webViewWebsite: UIWebView?
    var strWebsite = String()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
            self.title = "e-Library"
            webViewWebsite?.scrollView.delegate = self
           // self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            print("strWebsite = \(strWebsite)")
            self.webAPIWebsite()
    }
    //MARK:- webAPIWebsite
    func webAPIWebsite()
    {
        self.webViewWebsite?.isHidden        = false
        self.webViewWebsite?.delegate        = self
        self.webViewWebsite?.backgroundColor = UIColor.clear
        if let url = URL(string: self.strWebsite){
            let request = URLRequest(url: url)
            self.webViewWebsite?.loadRequest(request)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            }));
            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()
        AlertsClass.shared.showNativeAlert(withTitle: "", message: "Can't Connect. Please check your internet Connection", fromVC: self)
    }
    
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
}
