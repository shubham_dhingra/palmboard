//  LibrarySearchViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 21/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class LibrarySearchViewController: UIViewController,UISearchBarDelegate {
    @IBOutlet weak var tblViewLibrarySearch: UITableView?
    @IBOutlet weak var lblNoRecord: UILabel?
    
    var searchBar: UISearchBar?
    
    var arrayLatestBook   = [[String: Any]]()
    var intBookID         = Int()
    
    var strSchoolCodeSender  = String()
    var intPage              = Int()
    var spinner              = UIActivityIndicatorView()
    var strSearch            = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        tblViewLibrarySearch?.tableFooterView    = UIView(frame: CGRect.zero)
        tblViewLibrarySearch?.estimatedRowHeight = 2000
        
        spinner              = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner.color        =  UIColor(rgb: 0x56B54B)
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        searchBar                   = UISearchBar()
        searchBar?.placeholder       = "Search by Title/Author's"
        searchBar?.showsCancelButton = false
        searchBar?.barTintColor      = UIColor.white
        searchBar?.searchBarStyle    = .minimal
        searchBar?.returnKeyType     = .search
        searchBar?.delegate = self
        self.navigationItem.titleView = searchBar
    }
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblNoRecord?.isHidden = true
        intPage = 1
    }
    // MARK: -  Search Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print("searchBarSearchButtonClicked")
        //print("searchBar= \(String(describing: searchBar.text))")
        searchBar.resignFirstResponder()
        strSearch = /searchBar.text?.trimmingCharacters(in: .whitespaces)
        
        strSearch.count == 0 ?  Messages.shared.show(alert: .oops, message: AlertMsg.PleaseEnterText.get, type: .warning) :
        self.webAPILibrary(strQuery: searchBar.text!)
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 70
    }
    
    // MARK: -  webAPILibrary
    func webAPILibrary(strQuery: String)
    {
        self.lblNoRecord?.isHidden = true
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Library/Search?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&query=\(strQuery)&pg=\(intPage)"
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let arrayAll = (result["BookDTL"] as? [[String: Any]]){
                            self.arrayLatestBook = arrayAll
                        }
                        
                        if self.arrayLatestBook.count == 0{
                            self.lblNoRecord?.isHidden = false
                        }
                        else
                        {
                            self.tblViewLibrarySearch?.isHidden   = false
                            self.tblViewLibrarySearch?.reloadData()
                        }
                       // print("arrayTimetable = \(self.arrayLatestBook)")
                    }
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            print("fetch more data -- scrollViewDidEndDragging")
            spinner.startAnimating()
            
            intPage = intPage + 1
            self.loadChunkDataSearchSentAPI(intPagination: intPage, strQuery: strSearch)
        }
    }
    func loadChunkDataSearchSentAPI(intPagination: Int,strQuery: String)
    {
        self.lblNoRecord?.isHidden = true
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Library/Search?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&query=\(strQuery)&pg=\(intPagination)"
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let tempArray = (result["BookDTL"] as? [[String: Any]]){
                            if tempArray.count == 0{
                            }
                            else
                            {
                                self.arrayLatestBook.append(contentsOf: results?["LatestBook"] as! [[String : Any]])
                                self.tblViewLibrarySearch?.reloadData()
                            }
                        }
                    }
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "searchLibrary")
        {
            let destination       = segue.destination as! LatestBookInfoViewController
            destination.intBookID = self.intBookID
        }
    }
}
extension LibrarySearchViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLatestBook.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellLibrarySearch", for: indexPath) as! LibrarySearchCell
        
        if let Title  = self.arrayLatestBook[indexPath.row]["Title"] as? String{
            cell.lblBook?.text = Title
        }
        if let Author  = self.arrayLatestBook[indexPath.row]["Author"] as? String{
            cell.lblAuthor?.text = "~" + Author
        }
        if let imgUrl = self.arrayLatestBook[indexPath.row]["CoverImg"] as? String
        {
//            var strPrefix      = String()
//            strPrefix          += strPrefix.imgPath1()
//            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
            var finalUrlString =  imgUrl

            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url            = URL(string: finalUrlString )
           // print("url = \(url)")
            let imgViewUser = UIImageView()
            imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                
                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                cell.imgViewCover?.image = img1
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblViewLibrarySearch?.deselectRow(at: indexPath, animated: true)
        if let BookID  = self.arrayLatestBook[indexPath.row]["BookID"] as? Int{
            intBookID = BookID
        }
        self.performSegue(withIdentifier: "searchLibrary", sender: self)
    }
}
class LibrarySearchCell: UITableViewCell {
    @IBOutlet weak var lblBook:         UILabel?
    @IBOutlet weak var lblAuthor:       UILabel?
    @IBOutlet weak var imgViewCover:    UIImageView?
}
