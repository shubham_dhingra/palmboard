//  LibraryViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import LocalAuthentication

protocol HideBarDelegateLibrary: class{
    func hideBar(boolHide: Bool)
}
class LibraryViewController: UIViewController,UIWebViewDelegate {
    weak var hideBarDelegate: HideBarDelegateLibrary?
    
    @IBOutlet weak var webViewLibrary:   UIWebView?
    @IBOutlet weak var tblViewLibrary:   UITableView?
    @IBOutlet weak var segmentControl:   UISegmentedControl?
    @IBOutlet weak var segmentBgView:    UIView?
    @IBOutlet weak var segmentHeight:    NSLayoutConstraint?
    @IBOutlet weak var imgViewNoLibrary: UIImageView?
    @IBOutlet weak var lblNoLibrary:     UILabel?
    @IBOutlet var btnMenu:               [UIButton]!
    
    var arrayLatestBook   = [[String: Any]]()
    var arrayMyAccount    = [[String: Any]]()
    var subModule       : [[String : Any]]?                  // 0 means both are active
    var strModuleLink: String?
    var isEnableBool: Bool?

    var context = LAContext()

    /// The current authentication state.
    var state = AuthenticationState.loggedout

    
    var intBtnType      = Int()
    var intSegment      = Int()
    var intBookID       = Int()
    var btnSearch       =  UIButton()
    var actionButton    : ActionButton!

    
    var intUserID            = Int()
    var intUserType          = Int()
    var strSchoolCodeSender  = String()
    var strMegaBookLink      = String()
    
    @objc func ClkBtnSearch(sender:UIButton) {
        self.performSegue(withIdentifier: "Seguelibrarysearch", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        intBtnType = 1
        btnSearch                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
        btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
        btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        webViewLibrary?.isHidden = true
        
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
       
        tblViewLibrary?.tableFooterView    = UIView(frame: CGRect.zero)
        tblViewLibrary?.estimatedRowHeight = 2000
        onViewDidLoad()
    
    }
    
    
    func onViewDidLoad() {
        _ = btnMenu.enumerated().map { (index , button) in
            button.isHidden = true
        }
        
        segmentControl?.isHidden = true
        segmentBgView?.isHidden = true
        
        if (subModule == nil || subModule?.count == 0) {
            segmentControl?.isHidden = true
            segmentBgView?.isHidden = true
            segmentControl?.selectedSegmentIndex = 1
            showMegaBookLibrary()
        }
            
        else {
            for (_ , subModule) in (subModule ?? []).enumerated() {
               if let moduleId = subModule["S_MdlID"] as? Int {
                if moduleId == 4 {
                    _ = btnMenu.enumerated().map { (index , button) in
                        button.isHidden = false
                    }
                    webAPILibrary()
                    segmentControl?.isHidden = false
                    segmentBgView?.isHidden = true
                    break
                }
               }
            }
        }
        
        if /segmentControl?.isHidden && subModule?.count != nil {
            showMegaBookLibrary()
        }
    }
    
    
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "e-Library"
        self.showMenu()
        imgViewNoLibrary?.isHidden = true
        lblNoLibrary?.isHidden     = true
        // HideBarDelegateSyllabus?.hideBar(boolHide: false)
        hideBarDelegate?.hideBar(boolHide: false)
    }
    
    // MARK: -  webAPILibrary
    func webAPILibrary()
    {
        
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Library/DTL?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)"
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let MegaBookLink = (result["MegaBookLink"] as? String){
                            self.strMegaBookLink = MegaBookLink
                        }
                        
                        if let arrayAll = (result["LatestBook"] as? [[String: Any]]){
                            self.arrayLatestBook = arrayAll
                        }
                        if let arrayAll = (result["MyAccount"] as? [[String: Any]]){
                            self.arrayMyAccount = arrayAll
                        }
                        if let dicAddBook = (result["AddBook"] as? [String: Any]){
                            self.isEnableBool  = dicAddBook["isEnable"]   as? Bool
                            
                            if self.isEnableBool! {
                                self.strModuleLink = dicAddBook["ModuleLink"] as? String
                                UIView.animate(withDuration: 2.0) {
                                    self.setUpActionLayer()
                                }
                            }
                        }

                        
                        if self.arrayLatestBook.count == 0{
                            self.imgViewNoLibrary?.isHidden = false
                            self.lblNoLibrary?.isHidden     = false
                        }
                        else{
                            self.tblViewLibrary?.isHidden   = false
                            self.tblViewLibrary?.reloadData()
                        }
                        print("arrayTimetable = \(self.arrayLatestBook)")
                    }
                }
                else{
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
            }
        }
    
    }
    
    // MARK: -  ClkBtnMenu
    @IBAction func ClkBtnMenu(_ sender: UIButton)
    {
        intBtnType = sender.tag
        sender.titleLabel?.font =  R.font.ubuntuMedium(size: 15)
        
        if sender.tag == 1 {
            for btn in btnMenu {
                if btn.tag == 2 {
                    btn.setImage(R.image.accountUnSelect(), for: UIControl.State.normal)
                    btn.titleLabel?.font =  R.font.ubuntuRegular(size: 15)
                }
            }
            sender.setImage(R.image.latestBookSelect(), for: UIControl.State.normal)
        }
        else if sender.tag == 2 {
            for btn in btnMenu {
                if btn.tag == 1 {
                    btn.setImage(R.image.latestBookUnselect(), for: UIControl.State.normal)
                    btn.titleLabel?.font =  R.font.ubuntuRegular(size: 15)
                }
            }
            sender.setImage(R.image.accountSelect(), for: UIControl.State.normal)
        }
        self.tblViewLibrary?.setContentOffset(CGPoint(x:0,y:0), animated: true)
        tblViewLibrary?.reloadData()
    }
    // MARK: -  segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl){
        imgViewNoLibrary?.isHidden = true
        lblNoLibrary?.isHidden     = true
        
        
        if(segmentControl?.selectedSegmentIndex == 0){
            showListofBooks()
        }
        else if(segmentControl?.selectedSegmentIndex == 1){
            showMegaBookLibrary()
        }
    }
    
    func showListofBooks() {
        lblNoLibrary?.isHidden = self.arrayLatestBook.count != 0
        imgViewNoLibrary?.isHidden = (/lblNoLibrary?.isHidden)
        
        intSegment = 0
        for btn in btnMenu {
            btn.isHidden = false
        }
        tblViewLibrary?.isHidden = false
        webViewLibrary?.isHidden = true
        tblViewLibrary?.reloadData()
    }
    
    func showMegaBookLibrary() {
        intSegment = 1
        for btn in btnMenu {
            btn.isHidden = true
        }
        tblViewLibrary?.isHidden = true
        webViewLibrary?.isHidden = false
        self.showWebView()
       
    }
    
    
    func showWebView()
    {
        Utility.shared.loader()
        self.webViewLibrary?.delegate       = self
        self.webViewLibrary?.backgroundColor = UIColor.clear
        if let url = URL(string: self.strMegaBookLink)
        {
            let request = URLRequest(url: url)
            self.webViewLibrary?.loadRequest(request)
        }
        else
        {
            Utility.shared.removeLoader()
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            }));
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()

    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SegueLibraryDetails")
        {
            let destination       = segue.destination as! LatestBookInfoViewController
            destination.intBookID = self.intBookID
        }
    }
}
extension LibraryViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if intBtnType == 1{
            
            imgViewNoLibrary?.isHidden = arrayLatestBook.count != 0
            lblNoLibrary?.text = "No e-Book Available"
            lblNoLibrary?.isHidden = arrayLatestBook.count != 0
            return arrayLatestBook.count
        }
        else {
            imgViewNoLibrary?.isHidden = arrayMyAccount.count != 0
            lblNoLibrary?.text = "No Transactions Available"
            lblNoLibrary?.isHidden = arrayMyAccount.count != 0
            return arrayMyAccount.count
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if intBtnType == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLibraryLatestBook", for: indexPath) as! LibraryLatestBookCell
            
            if let Title  = self.arrayLatestBook[indexPath.row]["Title"] as? String{
                cell.lblBook?.text = Title
            }
            if let Author  = self.arrayLatestBook[indexPath.row]["Author"] as? String{
                cell.lblAuthor?.text = "~" + Author
            }
            if let imgUrl = self.arrayLatestBook[indexPath.row]["CoverImg"] as? String
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath1()
                var finalUrlString = "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString)
//                print("url = \(url)")
                let imgViewUser = UIImageView()
                if let url = URL(string : /imgUrl) {
                    imgViewUser.clearImageFromCache(validUrl: url)
                }
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.libraryNoImage(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true) { (response) in
                    
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    cell.imgViewCover?.image = img1
                }
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLibraryMyAccount", for: indexPath) as! LibraryMyAccountCell
            
            if let Title  = self.arrayMyAccount[indexPath.row]["Title"] as? String{
                cell.lblBookName?.text = Title
            }
            cell.lblSerialNo?.text = "\(indexPath.row + 1) "
            
            if let AccessionNumber  = self.arrayMyAccount[indexPath.row]["AccessionNumber"] as? String{
                cell.lblSubAccession?.text = "\(AccessionNumber)"
            }
            if let Cost  = self.arrayMyAccount[indexPath.row]["Cost"] as? String{
                cell.lblSubCost?.text = "\(Cost)"
            }
            if let IssueDate  = self.arrayMyAccount[indexPath.row]["IssueDate"] as? String{
                cell.lblSubIssueDate?.text = IssueDate.dateChangeFormat()
            }
            if let ExpReturnDate  = self.arrayMyAccount[indexPath.row]["ExpReturnDate"] as? String{
                cell.lblSubExpReturnDate?.text = ExpReturnDate.dateChangeFormat()
            }
            if let FineAmount  = self.arrayMyAccount[indexPath.row]["FineAmount"] as? Float{
                cell.lblSubFineAmount?.text = "\(FineAmount)"
            }
            if let ReturnDate  = self.arrayMyAccount[indexPath.row]["ReturnDate"] as? String{
                cell.lblSubReturnDate?.text = ReturnDate.dateChangeFormat()
            }
            if let imgUrl = self.arrayMyAccount[indexPath.row]["CoverImg"] as? String
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                if let url = URL(string : /imgUrl) {
                    imgViewUser.clearImageFromCache(validUrl: url)
                }
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.libraryNoImage(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true) { (response) in
                    
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    cell.imgViewCoverPic?.image = img1
                }
            }
            return cell
        }
    }
    //    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 80
    //    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblViewLibrary?.deselectRow(at: indexPath, animated: true)
        
        if intBtnType == 1{
            if let BookID  = self.arrayLatestBook[indexPath.row]["BookID"] as? Int{
                intBookID = BookID
            }
        }
        else {
            if let BookID  = self.arrayMyAccount[indexPath.row]["BookID"] as? Int{
                intBookID = BookID
            }
        }
        self.performSegue(withIdentifier: "SegueLibraryDetails", sender: self)
    }
}
//MARK::- SetUp ActionLayer
extension LibraryViewController {
    
    func setUpActionLayer() {
        actionButton = ActionButton(attachedToView: self.view, items: nil)
        actionButton.backgroundImage = UIImage(named : "Add Book")!
        actionButton.action = { button in
            print("check authentication")
            self.openScanner()
            //            self.checkAuthentication()
        }
        
    }
    
    
    func checkAuthentication() {
        if state == .loggedin {
            state = .loggedout
            
        } else {
            
            context = LAContext()
            
            context.localizedCancelTitle = "Login to your Account"
            
            // First check if we have the needed hardware support.
            var error: NSError?
            if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                
                let reason = "Please login to continue"
                context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                    
                    if success {
                        DispatchQueue.main.async { [unowned self] in
                            self.state = .loggedin
                            self.openScanner()
                        }
                        
                    } else {
                        print(error?.localizedDescription ?? "Failed to authenticate")
                    }
                }
            } else {
                if error?.localizedDescription ==  "Passcode not set." {
                    
                }
                print(error?.localizedDescription ?? "Can't evaluate policy")
            }
        }
    }
    
    func openScanner() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ScannerViewController") as? ScannerViewController  else {return}
        vc.strLibraryURL = strModuleLink
        self.presentVC(vc)
    }
}

class LibraryLatestBookCell: UITableViewCell {
    @IBOutlet weak var lblBook:         UILabel?
    @IBOutlet weak var lblAuthor:       UILabel?
    @IBOutlet weak var imgViewCover:    UIImageView?
}
class LibraryMyAccountCell: UITableViewCell {
    @IBOutlet weak var lblBookName:         UILabel?
    @IBOutlet weak var lblSerialNo:         UILabel?
    @IBOutlet weak var lblSubAccession:     UILabel?
    @IBOutlet weak var lblSubCost:          UILabel?
    @IBOutlet weak var lblSubIssueDate:     UILabel?
    @IBOutlet weak var lblSubExpReturnDate: UILabel?
    @IBOutlet weak var lblSubFineAmount:    UILabel?
    @IBOutlet weak var lblSubReturnDate:    UILabel?
    @IBOutlet weak var imgViewCoverPic:     UIImageView?
}
