//  ScannerViewController.swift
//  PalmBoard
//  Created by Shubham on 24/09/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import AVFoundation
import EZSwiftExtensions
import IQKeyboardManager

class ScannerViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var topView       : UIView?
    @IBOutlet weak var bottomView    : UIView?
    @IBOutlet weak var scannerView   : UIView?
    @IBOutlet weak var headerView    : UIView?
    @IBOutlet weak var middleView    : UIView?
    @IBOutlet weak var enterISBNView : UIView?
    @IBOutlet weak var txtISBN       : UITextField?
    @IBOutlet weak var btnCross      : UIButton?
    @IBOutlet weak var btnSkip       : UIButton?
    @IBOutlet weak var mainView      : UIView?
    @IBOutlet weak var lblHeading    : UILabel?
    
    //MARK::- VARIABLES
    var captureSession    : AVCaptureSession?
    var videoPreviewLayer : AVCaptureVideoPreviewLayer?
    var activeField       : UITextField?
    var strLibraryURL     : String?
    var strISBNCode       : String?
    var strBarCode        : String?
    
    var qrCodeFrameView   = UIView()
    var contentW: CGFloat = 0.0
    var contentH: CGFloat = 0.0
    var isScanISBNCode    = Bool()
    
    @IBAction func btnCrossAct(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            ez.runThisInMainThread {
                self.view.endEditing(true)
                self.btnCross?.isHidden = true
            }
        }
    }
    @IBAction func btnSkipAct(_ sender: UIButton) {
        openLibraryWebView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        let screenSize = UIScreen.main.bounds.size
        contentW = screenSize.width
        contentH = screenSize.height
        addDoneButtonTo(textField: txtISBN!)
        //        enterISBNView?.roundCorners(corners: [.topLeft , .topRight], radius: 20.0)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:
            NSNotification.Name(rawValue: UIResponder.keyboardWillShowNotification.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:  NSNotification.Name(rawValue: UIResponder.keyboardDidHideNotification.rawValue), object: nil)
    }
    //MARK::- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        txtISBN?.text          = ""
        lblHeading?.text       = "Scan ISBN Bar code"
        txtISBN?.placeholder   = "Enter ISBN Number"
        isScanISBNCode         = false
        self.btnSkip?.isHidden = true
        
        setUpCamera()
    }
    //MARK::- FUNCTIONS
    func setUpCamera(){
        
        //Configure Capture Device
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do {
            guard let captureDevice = captureDevice else {return}
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
        } catch {
            print(error)
            return
        }
        setMetaDataOutput()
        
        //Configure videoPreviewlayer
        guard let capturesession = captureSession else {return}
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: capturesession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        
        //Set Up CustomView
        view.layer.addSublayer(videoPreviewLayer!)
        view.bringSubviewToFront(bottomView!)
        view.bringSubviewToFront(enterISBNView!)
        view.bringSubviewToFront(topView!)
        view.bringSubviewToFront(middleView!)
        view.bringSubviewToFront(scannerView!)
        view.bringSubviewToFront(headerView!)
        view.bringSubviewToFront(mainView!)
        self.view.layoutIfNeeded()
        captureSession?.startRunning()
    }
    //MARK::- setMetaDataOutput
    func setMetaDataOutput(){
        //Set the MetaDataOutput
        let captureMetadataOutput = AVCaptureMetadataOutput()
        var scanRect = CGRect(x: (contentW-(scannerView?.frame.size.width)!)/2.0, y: (contentH-(scannerView?.frame.size.height)!
            )/2.0, width: (scannerView?.frame.size.width)!, height: (scannerView?.frame.size.height)!)
        
        scanRect = CGRect(x: scanRect.origin.y/contentH, y: scanRect.origin.x/contentW, width: scanRect.size.height/contentH, height: scanRect.size.width/contentW)
        captureMetadataOutput.rectOfInterest = scanRect
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr,AVMetadataObject.ObjectType.upce,AVMetadataObject.ObjectType.code39,AVMetadataObject.ObjectType.code39Mod43,AVMetadataObject.ObjectType.ean13,AVMetadataObject.ObjectType.ean8,AVMetadataObject.ObjectType.code93,AVMetadataObject.ObjectType.code128,AVMetadataObject.ObjectType.pdf417,AVMetadataObject.ObjectType.aztec,AVMetadataObject.ObjectType.interleaved2of5,AVMetadataObject.ObjectType.itf14,AVMetadataObject.ObjectType.dataMatrix]
    }
}


//MARK::- QRCode Delegate Functions
extension ScannerViewController  : AVCaptureMetadataOutputObjectsDelegate{
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //When QRCode is not detected
        
        if metadataObjects.count == 0 {
            qrCodeFrameView.frame = CGRect.zero
            return
        }
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.ean8 || metadataObj.type == AVMetadataObject.ObjectType.ean13 || metadataObj.type == AVMetadataObject.ObjectType.pdf417 {
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil && isScanISBNCode == false {
                print(/metadataObj.stringValue)
                captureSession?.stopRunning()
                getQRCode(metadataObj.stringValue)
            }
            else {
                Messages.shared.show(alert: .oops, message: "Invalid Barcode", type: .warning)
            }
        }
        else{
            Messages.shared.show(alert: .oops, message: "Invalid Barcode", type: .warning)
        }
    }
    //MARK::- getQRCode
    func getQRCode(_ extractString : String?) {
        Messages.shared.show(alert: .success, message: "ISBN Code :\(/extractString)", type: .success)
        print("BARCODE String:",/extractString)
        
        if (self.btnSkip?.isHidden == true) {
            strISBNCode            = /extractString
            lblHeading?.text       = "Scan Bar code"
            txtISBN?.placeholder   = "Enter Bar Number"
            self.btnSkip?.isHidden = false
            ez.dispatchDelay(2) {
                self.captureSession?.startRunning()
            }
        }
        else{
            self.btnSkip?.isHidden = false
            checkBarCode(/extractString)
            
        }
    }
    
    
    func checkBarCode(_ extractString : String?){
        APIManager.shared.request(with:HomeEndpoint.verifyBarCode(BarCode: extractString) , isLoader: false) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response : success , extractString : extractString)
            })
        }
    }
    
    func handle(response : Any? , extractString : String?){
        
        guard let extractString = extractString else {
            captureSession?.startRunning()
            return
        }
        
        if let modal = response as? MainProfileModel {
            if !(/modal.isExists) {
                strBarCode = extractString
                captureSession?.stopRunning()
                openLibraryWebView()
            }
            else {
                captureSession?.startRunning()
                Messages.shared.show(alert: .oops, message: /modal.message, type: .warning)
            }
        }
    }
    
    
    //MARK::- openLibraryWebView
    func openLibraryWebView() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "LibraryScanWebViewController") as? LibraryScanWebViewController  else {return}
        let strURL    = strLibraryURL! + "?isbn=\(/strISBNCode)&barcode=\(/strBarCode)"
        // strLibraryURL = strLibraryURL! + "?isbn=\(/strISBNCode)&barcode=\(/strBarCode)"
        vc.strWebsite = strURL//strLibraryURL!
        self.presentVC(vc)
    }
    
}
//MARK::- Keyboard Handling
extension ScannerViewController{
    
    @objc func keyboardWillShow(notification: Notification) {
        ez.runThisInMainThread {
            self.keyboardControl(notification, isShowing: true)
        }
    }
    @objc func keyboardWillHide(notification: Notification) {
        ez.runThisInMainThread {
            self.keyboardControl(notification, isShowing: false)
        }
    }
    private func keyboardControl(_ notification: Notification, isShowing: Bool) {
        
        /* Handle the Keyboard property of Default*/
        var userInfo = notification.userInfo!
        let keyboardRect = (userInfo[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue
        let curve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey]! as AnyObject).uint32Value
        
        let options = UIView.AnimationOptions(rawValue: UInt(curve!) << 16 | UIView.AnimationOptions.beginFromCurrentState.rawValue)
        let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey]! as AnyObject).doubleValue
        
        btnCross?.isHidden = !isShowing
        
        if isShowing { /// Wite space of save area in iphonex ios 11
            if #available(iOS 11.0, *) {
            }
        }
        UIView.animate(
            withDuration: duration!,
            delay: 0,
            options: options,
            animations: {
                self.view.layoutIfNeeded()
        },
            completion: { bool in
                
        })
        
    }
}
extension ScannerViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first?.view != enterISBNView {
            self.view.endEditing(true)
        }
    }
}
extension ScannerViewController {
    
    private func addDoneButtonTo(textField: UITextField) {
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.didTapDone(_:)))
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    @objc func didTapDone(_ sender: AnyObject?) {
        txtISBN?.endEditing(true)
        if txtISBN?.text?.count == 0 {
            Messages.shared.show(alert: .oops, message: AlertMsg.enterISBNValue.get, type: .warning)
        }
        else {
            if (self.btnSkip?.isHidden == true) {
                strISBNCode            = txtISBN?.text
                lblHeading?.text       = "Scan Bar code"
                txtISBN?.text          = ""
                txtISBN?.placeholder   = "Enter Bar Number"
                self.btnSkip?.isHidden = false
                captureSession?.startRunning()
            }
            else{
                self.btnSkip?.isHidden = false
                strBarCode             = txtISBN?.text
                captureSession?.stopRunning()
                openLibraryWebView()
            }
        }
    }
}
