import UIKit
import CoreData

class EnterCodeViewController: BaseViewController,UITextFieldDelegate
{
    @IBOutlet weak var btnConvart:        CustomButtom?
    @IBOutlet weak var txtField1:                 UITextField?
    @IBOutlet weak var txtField2:                 UITextField?
    @IBOutlet weak var txtField3:                 UITextField?
    @IBOutlet weak var txtField4:                 UITextField?
    @IBOutlet weak var txtField5:                 UITextField?
    @IBOutlet weak var txtField6:                 UITextField?
    @IBOutlet weak var lblInvalid:                UILabel?
    @IBOutlet weak var btnFindCode:               UIButton?
    @IBOutlet weak var topInvalidSchoolCode:      NSLayoutConstraint?
    
    
    var strSchoolCode    = String()
    var strSchoolName    = String()
    var strSchoolImgLogo = String()
    var arraySlider      = [[String : Any]]()
    var arraySchoolList  = [[String : Any]]()
    var arrSearchData    = [[String : Any]]()
    var search:String    = ""
    var strSchCity       = String()
    var sMS_Prvd_ID      = Int()
    var countryCode      = Int()
    var supportPhone     = String()
    var themColor        = String()
    var webSite          = String()
    var supportEmail     = String()
    var textFieldArr  =   [UITextField]()
    
    var yourAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
        NSAttributedString.Key.foregroundColor : UIColor.white,
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    
    // MARK: - viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        textFieldArr =  [txtField1! ,txtField2! , txtField3! , txtField4! , txtField5! , txtField6!]
        cleanTextField()
        setUpForIphoneX()
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if strSchoolCode.count == 0 {
            cleanTextField()
            strSchoolCode  = ""
        }
        
        _ = textFieldArr.map { (field) -> UITextField in
            field.textColor = UIColor(rgb: 0x56B54B)
            field.layer.borderColor = UIColor.flatGreen.cgColor
            field.layer.borderWidth = 1.0
            field.layer.cornerRadius = 5.0
            return field
        }

        yourAttributes = [
            NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
            NSAttributedString.Key.foregroundColor : UIColor.flatGreen,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                        attributes: yourAttributes)
        btnFindCode?.setAttributedTitle(attributeString, for: .normal)
        
        topInvalidSchoolCode?.constant = 0
        lblInvalid?.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    //MARK: textField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.lblInvalid?.isHidden   = true
        yourAttributes = [
            NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
            NSAttributedString.Key.foregroundColor : UIColor.flatGreen,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                        attributes: yourAttributes)
        btnFindCode?.setAttributedTitle(attributeString, for: .normal)
        
        self.txtField1?.textColorYellow(text: (self.txtField1?.text)!)
        self.txtField2?.textColorYellow(text: (self.txtField2?.text)!)
        self.txtField3?.textColorYellow(text: (self.txtField3?.text)!)
        self.txtField4?.textColorYellow(text: (self.txtField4?.text)!)
        self.txtField5?.textColorYellow(text: (self.txtField5?.text)!)
        self.txtField6?.textColorYellow(text: (self.txtField6?.text)!)
        
        _ = textFieldArr.map { (field) -> UITextField in
            field.borderColorWithGreen()
            return field
        }

        /********************************************************/
        if textField.tag != 10
        {
            animateTxtFld(txtFld: textField , up: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //print("textFieldShouldReturn )")
        if textField.tag == 10
        {
            return true // We do not want UITextField to insert line-breaks.
        }
        else{
            let nextTag: NSInteger = textField.tag + 1
            let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder?
            if (nextResponder != nil)
            {
                // Found next responder, so set it.
                nextResponder?.becomeFirstResponder()
            } else {
                // Not found, so remove keyboard.
                textField.resignFirstResponder()
            }
            return false // We do not want UITextField to insert line-breaks.
        }
    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag == 10
        {
           return true
        }
        else
        {
            let currentString           = textField.text! as NSString
            let newString               = currentString.replacingCharacters(in: range, with: string)
            let compSepByCharInSet      = string.components(separatedBy: aSet)
            let numberFiltered          = compSepByCharInSet.joined(separator: "")
            
            if ((newString.count) <= maxLength && string == numberFiltered) == true
            {
                self.topInvalidSchoolCode?.constant = 0
                lblInvalid?.isHidden = true
                
                textField.addTarget(self, action: #selector(self.textFieldMoveNext(textfield: )), for: UIControl.Event.editingChanged)
            }
            return (newString.count) <= maxLength && string == numberFiltered
        }
    }
    @objc func textFieldMoveNext(textfield : UITextField)//Custom
    {
        //print("textFieldMoveNext")
        
        let newString12 = textfield.text!
        //print("newString12 in next method = \(newString12)")
        if (newString12.count == 0)
        {
            textFieldMoveBack(textfield: textfield)//Custom
        }
        else
        {   let nextTag = textfield.tag + 1
            let nextResponder = textfield.superview?.viewWithTag(nextTag) as UIResponder?
            if (nextResponder != nil)
            {
                nextResponder?.becomeFirstResponder()
            } else {
                textfield.resignFirstResponder()
            }
        }
    }
    
    func textFieldMoveBack(textfield : UITextField)//Custom
    {
        let nextTag = textfield.tag - 1
        let nextResponder = textfield.superview?.viewWithTag(nextTag) as UIResponder?
        if (nextResponder != nil)
        {
            nextResponder?.becomeFirstResponder()
            
        } else {
            textfield.resignFirstResponder()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool{
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        strSchoolCode = "\(/self.txtField1?.text)\(/self.txtField2?.text)\(/self.txtField3?.text)\(/self.txtField4?.text)\(/self.txtField5?.text)\(/self.txtField6?.text)"
        
        if textField.tag != 10
        {
            animateTxtFld(txtFld: textField, up: false)
        }
        if strSchoolCode.count < 6
        {
            //            btnContact?.isUserInteractionEnabled = false
            textField.layer.borderColor  = UIColor.flatGreen.cgColor
            if textField.tag != 10
            { textField.textColorYellow(text: textField.text!)
            }
        }
    }
    
    //MARK: - ClkBtnContinue
    @IBAction func ClkBtnContinue(_ sender: Any)
    {
        self.schoolVerifyCode(strSchoolCodeVerify: strSchoolCode)
    }
    
    @IBAction func btnBackActionAct(_ sender: UIButton) {
        self.popVC()
    }
    
    //MARK: - schoolVerifyCode
    func schoolVerifyCode(strSchoolCodeVerify: String)
    {
        //print("Func: schoolVerifyCode= \(strSchoolCodeVerify)")
        
        
        if strSchoolCodeVerify.count !=  6 {
            animateForError(msg: AlertMsg.incompleteSchoolCode.get)
            return
        }
        
        var strSchoolCodeverifyURL = String()
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.webAPIDomainNmae()
        strSchoolCodeverifyURL += "school/DTL?SchCode="
        strSchoolCodeverifyURL += strSchoolCodeVerify.uppercased()
        strSchoolCodeverifyURL += "&key="
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.keyPath()
        print("strSchoolCodeverifyURL \n \(strSchoolCodeverifyURL)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: strSchoolCodeverifyURL) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        //  print("result \n \(result)")
                        if(result["ErrorCode"] as? Int == 0 && result["Status"] as? String == "ok")
                        {
                          
                            self.strSchoolCode = strSchoolCodeVerify
                            self.btnFindCode?.isUserInteractionEnabled = true
                            //Insert Data into SchoolCodeTable
                            if let valueCity    = (result["City"] as? String){
                                self.strSchCity = valueCity
                            }
                            if let valueLogo    = (result["Logo"] as? String){
                                
                                let strImgURL: String  = valueLogo
                                var finalUrl = APIConstants.imgbasePath + strImgURL

                                //print("finalUrl = \(finalUrl)")
                                finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                                self.strSchoolImgLogo = finalUrl
                                
                            }
                            if let valueschoolName    = (result["SchoolName"] as? String){
                                self.strSchoolName  = valueschoolName
                            }
                            if let supportEmail11     = (result["SupportEmail"]   as? String)
                            {
                                self.supportEmail = supportEmail11
                            }
                            if let supportPhone11     = (result["SupportPhone"]   as? String)
                            {
                                self.supportPhone = supportPhone11
                            }
                            
                            self.openHelpVc()
                            
                        }
                        else if(result["Status"] as? String == "Invalid School Code" )
                        {
                            self.txtField1?.txtShakeAnimation()
                            self.txtField2?.txtShakeAnimation()
                            self.txtField3?.txtShakeAnimation()
                            self.txtField4?.txtShakeAnimation()
                            self.txtField5?.txtShakeAnimation()
                            self.txtField6?.txtShakeAnimation()
                            
                            self.txtField1?.borderWidthColor()
                            self.txtField2?.borderWidthColor()
                            self.txtField3?.borderWidthColor()
                            self.txtField4?.borderWidthColor()
                            self.txtField5?.borderWidthColor()
                            self.txtField6?.borderWidthColor()
                            
                            self.txtField1?.textColorError(text: (self.txtField1?.text)!)
                            self.txtField2?.textColorError(text: (self.txtField2?.text)!)
                            self.txtField3?.textColorError(text: (self.txtField3?.text)!)
                            self.txtField4?.textColorError(text: (self.txtField4?.text)!)
                            self.txtField5?.textColorError(text: (self.txtField5?.text)!)
                            self.txtField6?.textColorError(text: (self.txtField6?.text)!)
                            
                            self.lblInvalid?.isHidden                  = false
                            self.topInvalidSchoolCode?.constant        = 5
                            
                          
                            
                            self.yourAttributes = [
                                NSAttributedString.Key.font : R.font.ubuntuRegular(size: 14.0)! ,
                                NSAttributedString.Key.foregroundColor : UIColor.white,
                                NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
                            let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                                            attributes: self.yourAttributes)
                            self.btnFindCode?.setAttributedTitle(attributeString, for: .normal)
                        }
                    }
                    if (error != nil){
                        //print("errorLobby = \(error!)")
                        DispatchQueue.main.async
                            {
                                
                                self.btnFindCode?.isUserInteractionEnabled = true
                               
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    
    // MARK: - animateTxtFld
    func animateTxtFld(txtFld: UITextField, up: Bool){
        
        let movementDistance = -120
        let movementDuration  = 0.3
        let movement = (up ? movementDistance : -movementDistance)
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
    // MARK: - dismissKeyboard
    override func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK: - ClkCross
    @IBAction func ClkCross(_ sender: Any)
    {
        Utility.shared.removeLoader()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func ClkFAQ(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let faqVC = storyboard.instantiateViewController(withIdentifier: "HelpWebViewController") as? HelpWebViewController else {return}
        faqVC.intReturnType = 1
        self.pushVC(faqVC)
    }
    // MARK: - ClkBtnFindCode
    @IBAction func ClkBtnFindCode(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "FindSchoolCodeViewController") as? FindSchoolCodeViewController else {return}
        vc.fromHelpVc = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func ClkBack(_ sender: UIBarButtonItem) {
    }
   
    
    override func openHelpVc() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let helpController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController else {return}
        helpController.strEmail   = supportEmail
        helpController.strPhoneNo = supportPhone
        helpController.strLogoURL = strSchoolImgLogo
        helpController.strCity    = strSchCity
        helpController.strSchoolName = strSchoolName
        self.presentVC(helpController)
    }
    

    
    func animateForError(msg : String){
        
        
        self.cleanTextField()
        for value in self.textFieldArr {
            value.borderWidthColor()
            value.txtShakeAnimation()
            value.textColorError(text: (value.text!))
        }
        self.lblInvalid?.text = msg
        self.lblInvalid?.isHidden  = false
    }
    
    func cleanTextField(){
        for value in textFieldArr {
            value.borderColorWithGreen()
            value.text = nil
        }
    }
}

extension EnterCodeViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != txtField1 && touches.first != txtField2 && touches.first != txtField3 && touches.first != txtField4 && touches.first != txtField5 && touches.first != txtField6{
           self.view.endEditing(true)
        }
    }
}

extension EnterCodeViewController : FindSchoolDelegate{
    func sendSchoolCode(schoolCode: String?) {
        guard let schoolCode = schoolCode else {return}
        if schoolCode.count != 6 {
            return
        }
        if schoolCode.count == 6 {
            strSchoolCode = schoolCode
            self.txtField1?.text = schoolCode[0].toString
            self.txtField2?.text = schoolCode[1].toString
            self.txtField3?.text = schoolCode[2].toString
            self.txtField4?.text = schoolCode[3].toString
            self.txtField5?.text = schoolCode[4].toString
            self.txtField6?.text = schoolCode[5].toString
            _ = textFieldArr.map { (field) -> UITextField in
                field.borderColorWithGreen()
                return field
            }
        }
    }
}
