//  FindSchoolCodeViewController.swift
//  eSmartGuard
//  Created by Shubham on 11/09/18.
//  Copyright © 2018 Franciscan Solutions Pvt. Ltd. All rights reserved.

import UIKit
import EZSwiftExtensions

protocol FindSchoolDelegate {
    func sendSchoolCode(schoolCode : String?)
}
class FindSchoolCodeViewController: BaseViewController {
    
    @IBOutlet weak var outerview: UIView!
    @IBOutlet weak var txtSearch: UITextField?
     
    var schoolsArr = [SchoolList]()
    var schoolList : [SchoolList]?
    var isFirstTime : Bool = true
    var fromStarting : Bool = true
    var fromHelpVc : Bool = false
    var delegate : FindSchoolDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSchoolList()
        setUpForIphoneX()
        setUpView()
    }
    func setUpView() {
       outerview.addBorder(width: 1.0, color: UIColor.white)
        tableView?.addBorder(width: 1.0, color: UIColor.white)
        tableView?.isHidden = true
        txtSearch?.becomeFirstResponder()
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = schoolsArr
            tableView?.reloadData()
        }
    }
    func getSchoolList() {
        APIManager.shared.request(with: HomeEndpoint.getSchoolList(), isLoader: false) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response  : success)
            })
        }
    }
    func handle(response : AnyObject?){
        if let modal = response as? SchoolListDTL , let arr =  modal.list{
            schoolList = arr
//            reloadTable()
        }
    }
}

extension FindSchoolCodeViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: schoolsArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.FindSchoolCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            ez.runThisInMainThread {
               (cell as? FindSchoolCell)?.modal = item
            }
           
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ selectIndex: Int) {
        self.view.endEditing(true)
        if selectIndex < schoolsArr.count {
            guard let schoolCode = schoolsArr[selectIndex].schoolCode else {return}
            if fromHelpVc {
                self.navigationController?.popViewController(animated: false)
                delegate?.sendSchoolCode(schoolCode: schoolCode)
            }
            else {
                self.navigationController?.popViewController(animated: false)
                delegate?.sendSchoolCode(schoolCode: schoolCode)
                }
        }
    }
}

extension FindSchoolCodeViewController : UITextFieldDelegate {
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.trimmed() == "" && string.trimmed() == "")
        {
            tableView?.isHidden = true
        }
        else{
             tableView?.isHidden = false
            if string != "" {
            textField.text = /textField.text  + string
            }
            else {
           let afterTrimLasrCharamter  = String((textField.text?.dropLast())!)
           textField.text = /afterTrimLasrCharamter
            tableView?.isHidden = textField.text?.trimmed().count == 0
            }
            schoolsArr = []
            for (_ ,school) in (schoolList ?? []).enumerated() {
                if /(school.name?.lowercased().contains((/textField.text?.lowercased()), compareOption: NSString.CompareOptions.caseInsensitive)) ||  /(school.city?.lowercased().contains((/textField.text?.lowercased()), compareOption: NSString.CompareOptions.caseInsensitive)) || /(school.address?.lowercased().contains((/textField.text?.lowercased()), compareOption: NSString.CompareOptions.caseInsensitive)) || /(school.schoolCode?.lowercased().contains((/textField.text?.lowercased()), compareOption: NSString.CompareOptions.caseInsensitive)) || /(school.state?.lowercased().contains((/textField.text?.lowercased()), compareOption: NSString.CompareOptions.caseInsensitive)){
                   schoolsArr.append(school)
                }
            }
            tableView?.isHidden = schoolsArr.count == 0
            reloadTable()
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSearch?.resignFirstResponder()
        return true
    }
}
