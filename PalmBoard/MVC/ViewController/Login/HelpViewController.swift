import UIKit

class HelpViewController: BaseViewController {
    
    @IBOutlet weak var imgViewSchool: UIImageView?
    @IBOutlet weak var lblSchoolName: UILabel?
    @IBOutlet weak var lblSchoolCity: UILabel?
    @IBOutlet weak var btnPhoneValue: UIButton?
    @IBOutlet weak var btnEmailValue: UIButton?
    
    var strEmail   = String()
    var strPhoneNo = String()
    var strSchoolName  = String()
    var strCity    = String()
    var strLogoURL = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string :strLogoURL){
        imgViewSchool?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: R.image.noProfile_Big())
        }
        lblSchoolName?.text  = strSchoolName
        lblSchoolCity?.text  = strCity
        btnPhoneValue?.setTitle(strPhoneNo, for: .normal)
        btnEmailValue?.setTitle(strEmail, for: .normal)
        setUpForIphoneX()
    }
    
    @IBAction func btnBackActionAct(_ sender: UIButton) {
//        guard let vc = self.navigationController?.viewControllers else {return}
         self.dismissVC(completion: nil)
    }
    
    
    @IBAction func callPressed(_ sender: UIButton) {
        
        if let url = URL(string: "tel://\(strPhoneNo)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    @IBAction func emailPressed(_ sender: UIButton) {
        //EMAIL
        let email = strEmail
        let urlEMail = URL(string: "mailto:\(email)")
        
        if UIApplication.shared.canOpenURL(urlEMail!) {
            UIApplication.shared.openURL(urlEMail!)
        } else {
            Messages.shared.show(alert: .oops, message: "Something went wrong please try again", type: .warning)
        }
    }
    
    @IBAction func faqPressed(_ sender: UIButton) {
        
    }
}
