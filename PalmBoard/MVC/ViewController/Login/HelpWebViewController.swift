import UIKit

class HelpWebViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var webVeiw: UIWebView?
    var intReturnType: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForIphoneX()
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        webVeiw?.delegate = self
        webVeiw?.backgroundColor = UIColor.clear
        self.navigationItem.title = "FAQ"
        
        if let url = URL(string: "http://www.franciscansolutions.com/faq-v2.aspx")
        {
            let request = URLRequest(url: url)
            webVeiw?.loadRequest(request)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
                _ = self.navigationController?.popViewController(animated: true)
                
            }));
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
       Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
          Utility.shared.removeLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()
        let alert = UIAlertController(title: "", message: "Can't Connect. Please check your internet Connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }));
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnBackAction(_ sender : UIButton) {
        if intReturnType == 1{
            self.popVC()
        }else{
            self.dismissVC(completion: nil)}
    }
}
