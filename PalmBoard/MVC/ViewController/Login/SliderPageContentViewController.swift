import UIKit
import AlamofireImage
import Alamofire

class SliderPageContentViewController: UIViewController {

    @IBOutlet weak var imgViewModule:  UIImageView?
    @IBOutlet weak var lblModule:      UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    
    var photoIndex = 0
    var strImgURL:      String?
    var strModuleName:  String?
    var strDescription: String?
    var imgUrl :        String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
          
            self.lblModule?.text     = self.strModuleName
            self.lblDescription?.text = self.strDescription
            var strPrefix   = String()
            strPrefix       += strPrefix.imgPath1()
            var finalUrl    = "\(strPrefix)" +  "\(self.strImgURL!)"

            finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url = URL(string: finalUrl)
            
            self.imgViewModule?.af_setImage(withURL: url!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
            }

        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
