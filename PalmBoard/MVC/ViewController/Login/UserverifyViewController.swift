import UIKit
import CoreData
import Alamofire

class UserverifyViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UITextFieldDelegate
{
    @IBOutlet weak var lblInvalidPwd:              UILabel?
    @IBOutlet weak var lblErrorRegisteredUserName: UILabel?
    @IBOutlet weak var sliderView:                 UIView?
    @IBOutlet weak var bottomView:                 UIView?
    @IBOutlet weak var pwdView:                    UIView?
    
    @IBOutlet weak var imgViewSchool:    UIImageView?
    @IBOutlet weak var lblSchoolName:    UILabel?
    @IBOutlet weak var lblSchoolPwdName: UILabel?
    @IBOutlet weak var txtFieldUserName: UITextField?
    @IBOutlet weak var txtFieldPwd:      UITextField?
    @IBOutlet weak var lblDisplayName:   UILabel?
    @IBOutlet weak var imgViewUser:      UIImageView?
    
    @IBOutlet weak var btnNextUser:      CustomButtom?
    @IBOutlet weak var btnEnterPwd:      CustomButtom?
    
    let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."
    var dictResult           = [String : Any]()
    var pageViewController   = UIPageViewController()
    var arraySliderImageURLs = [[String : Any]]()
    var itemIndex            = 0
    var timer: Timer?
    var boolTimer            = Bool()
    var activeTxtfld         = UITextField()
    var isChecked            = Bool()
    var strSchoolCode        = String()
    var userOrPasswrd        = String()
    var strSchoolCodeSender  = String()
    
    var intUserType       = Int()
    var intCountryCode    = Int()
    var intSMSPvdID       = Int()
    var imgSchool         = UIImage()
    
    var arraySliderBlank  = [[String: Any]]()
    
    @IBOutlet weak var bottomViewBottom:            NSLayoutConstraint?
    @IBOutlet weak var sliderviewTop:               NSLayoutConstraint?
    @IBOutlet weak var bottomViewTop:               NSLayoutConstraint?
    @IBOutlet weak var sliderheightConstraint:      NSLayoutConstraint?
    @IBOutlet weak var lblregisteredUserNameHeight: NSLayoutConstraint?
    @IBOutlet weak var lblInvalidPwdHeight:         NSLayoutConstraint?
    
    var buttonRight: UIButton?

    //    let container =  CoreDataManager.sharedInstance.persistentContainer
    //    let context   =  CoreDataManager.sharedInstance.persistentContainer.viewContext
    //
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(2, forKey: "logged_in")
        UserDefaults.standard.synchronize()
        
        var themeColor = String()
        if let check = self.getCurrentSchool()?.themColor
        {
            themeColor =  check
            pwdView?.backgroundColor    = themeColor.hexStringToUIColor()
            bottomView?.backgroundColor = themeColor.hexStringToUIColor()
            txtFieldUserName?.tintColor = themeColor.hexStringToUIColor()
            txtFieldPwd?.tintColor      = themeColor.hexStringToUIColor()
        }
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        sliderheightConstraint?.constant = self.view.frame.size.height/2
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserverifyViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        lblregisteredUserNameHeight?.constant = 0
        lblInvalidPwdHeight?.constant         = 0
        
        buttonRight                  = UIButton(type: .custom)
        buttonRight?.setImage(R.image.hideEye(), for: .normal)//eyes
        buttonRight?.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        buttonRight?.frame           = CGRect(x: 0, y: 0, width: CGFloat(25), height: CGFloat(25))
        buttonRight?.addTarget(self, action: #selector(self.ClkShowPwd), for: .touchUpInside)
        txtFieldPwd?.rightView       = nil//buttonRight
        txtFieldPwd?.rightViewMode   = .always
        
        self.isChecked               = true
        self.FetchMethod()
    }

    // MARK: - Update Data
    @IBAction func ClkShowPwd(_ sender: Any)
    {
        let showImage = R.image.eyes()! as UIImage
        let hideImage = R.image.hideEye()! as UIImage
        
        if txtFieldPwd?.text != ""
        {
            if (self.isChecked == true)
            {
                (sender as AnyObject).setImage(showImage, for: UIControl.State.normal)
                self.isChecked = false;
                txtFieldPwd?.isSecureTextEntry = false
            }
            else
            {
                (sender as AnyObject).setImage(hideImage, for: UIControl.State.normal)
                self.isChecked = true;
                txtFieldPwd?.isSecureTextEntry = true
            }
        }
     }
    
    // MARK: - Update Data
    func FetchMethod()
    {
        print("*** FetchMethod")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context     = appDelegate.persistentContainer.viewContext
        
        if let schoolCode1 = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode1
        }
        
        print("*** strSchoolCodeSender = \(strSchoolCodeSender)")
        
        //
        let fetchRequest    = NSFetchRequest<NSFetchRequestResult>()
        let entityName      = NSEntityDescription.entity(forEntityName: "SchoolCodeTable", in: context)
        fetchRequest.entity = entityName
        fetchRequest.predicate = NSPredicate(format: "schCode = %@",strSchoolCodeSender)
        
        // fetchRequest.sortDescriptors = [NSSortDescriptor(key: "schCode",ascending: false)]
        do
        {
            let result =  try context.fetch(fetchRequest)
            print("Result=\(result)")
            print("Result count=\(result.count)")
            
            let arryResult =  result as! [SchoolCodeTable]
            //  print("arryResult and count=\(arryResult)\n \(arryResult.count)")
            
            for item in arryResult
            {
                //print("arryResult and count=\(arryResult)\n \(arryResult.count)")
                var strschoolName      = item.schName
                let strSchoolCity      = item.schCity
                strSchoolCode          = item.schCode!
                let strSchoolLogo      = item.schLogo
                
                UserDefaults.standard.set(strschoolName, forKey: "schoolName")
                UserDefaults.standard.synchronize()
                
                intCountryCode        = Int(item.countryCode)
                intSMSPvdID           = Int(item.sMS_Prvd_ID)
                
                strschoolName          = strschoolName! + ", "
                strschoolName          = strschoolName! + strSchoolCity!
                
                //arraySliderImageURLs   = strSchoolCodeArray as! [[String : Any]]
                lblSchoolName?.text     = strschoolName
                lblSchoolPwdName?.text  = strschoolName
                let strImgURL: String  = strSchoolLogo!
                
               // var finalUrl = "http://webapi.palmboard.in/\(strImgURL)"
                var finalUrl = APIConstants.imgbasePath + strImgURL

                print("finalUrl = \(finalUrl)")
                finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url = URL(string: finalUrl)
                
                imgViewSchool?.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    
                    self.imgSchool = (self.imgViewSchool?.image)!
                }
                
                if arraySliderImageURLs.count > 0
                {
                    self.createpages()
                }
                else{
                    self.getSliderData()
                }
                break
            }
        }
        catch let error as NSError
        {
            print("Error in fetch result= \(error)")
        }
    }
    // MARK: - dismissKeyboard
    @objc override func dismissKeyboard() {
        view.endEditing(true)
    }
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        boolTimer     = true
        activeTxtfld  = self.txtFieldUserName!
        pwdView?.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
    }
    //MARK: createpages Method
    func createpages()
    {
        pageViewController = self.storyboard!.instantiateViewController(withIdentifier: "PageView") as! UIPageViewController
        pageViewController.dataSource = self
        pageViewController.delegate   = self
        pageViewController.view.frame =  CGRect(x: 0, y: (sliderView?.frame.origin.y)! , width: (sliderView?.frame.width)! , height: (sliderView?.frame.height)! - ((imgViewSchool?.frame.height)! / 2 + 5) )
        
        pageViewController.view.backgroundColor = UIColor.clear
        self.addChild(pageViewController)
        sliderView?.addSubview(pageViewController.view)//self.view.addSubview(pageViewController.view)
        
        let firstController         = getItemController(itemIndex: 0)
        let startingViewControllers = [firstController]
        pageViewController.setViewControllers(startingViewControllers, direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        self.view.isMultipleTouchEnabled = true
        
        if arraySliderImageURLs.count > 1
        {
            if self.timer == nil
            {
                self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.animateTransition), userInfo: nil, repeats: true)
            }
        }
    }
    //MARK: animateTransition Method
    @objc func animateTransition()
    {
        //print("animateTransition")
        if boolTimer == true
        {
            itemIndex += 1
            if (itemIndex == arraySliderImageURLs.count)
            {
                itemIndex  = 0
            }
            // print("itemIndex = \(itemIndex)")
            let firstController = getItemController(itemIndex: itemIndex)
            let startingViewControllers = [firstController]
            pageViewController.setViewControllers(startingViewControllers, direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    // MARK: - UIPageViewController Method
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! SliderPageContentViewController
        
        var index = itemController.photoIndex
        itemIndex = index
        if ((index == 0) || (index == NSNotFound))
        {
            index = arraySliderImageURLs.count
        }
        index -= 1
        
        return getItemController(itemIndex: index)
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! SliderPageContentViewController
        
        var index = itemController.photoIndex
        itemIndex = index
        
        if (index == NSNotFound)
        {
            return nil
        }
        index += 1
        
        if (index == arraySliderImageURLs.count)
        {
            index = 0
        }
        return getItemController(itemIndex: index)
    }
    func presentationCount(for pageViewController: UIPageViewController) -> Int
    {
        setupPageControl()
        return arraySliderImageURLs.count
    }
    func presentationIndex(for pageViewController: UIPageViewController) -> Int
    {
        return itemIndex
    }
    // MARK: - getItemController
    private func getItemController(itemIndex: NSInteger) -> SliderPageContentViewController
    {
        // print("*** getItemController")
        let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "SliderView") as! SliderPageContentViewController
        
        pageItemController.photoIndex = itemIndex
        let moduleValues = self.arraySliderImageURLs[itemIndex] as [String: Any]
        
        if let moduleName = moduleValues["Module"] as? String{
            pageItemController.strModuleName = moduleName
        }
        if let description = moduleValues["Description"] as? String{
            pageItemController.strDescription = description
        }
        if let imgUrl = moduleValues["ImgPath"] as? String{
            pageItemController.strImgURL = imgUrl
        }
        
        return pageItemController
    }
    // MARK: - setupPageControl
    private func setupPageControl()
    {
        var themeColor = String()
        let appearance = UIPageControl.appearance()
        
        if let check = self.getCurrentSchool()?.themColor{
            themeColor =  check
            appearance.currentPageIndicatorTintColor = themeColor.hexStringToUIColor()
        }
        appearance.pageIndicatorTintColor        = UIColor(rgb: 0xEAEAEA)
        // appearance.currentPageIndicatorTintColor = themeColor.hexStringToUIColor()//UIColor(rgb: 0xFFC500)
        appearance.backgroundColor               = UIColor.white
    }
    
    
    override func viewDidLayoutSubviews() {
        let pageControll   = UIPageControl()
        pageControll.subviews.forEach {
            print("value has changed")
            $0.transform = CGAffineTransform(scaleX: 7, y: 7)
        }
    }
    
    // MARK: - nextUsernameAction
    @IBAction func nextUsernameAction(_ sender: Any)
    {
        if txtFieldUserName?.text == ""{
            self.txtFieldUserName?.txtShakeAnimation()
            self.txtFieldUserName?.borderWidthColor()
            self.txtFieldUserName?.textColorError(text: (self.txtFieldUserName?.text!)!)
        }
        else
        {
            print("Not blank txtfield")
            txtFieldUserName?.resignFirstResponder()
            Utility.shared.loader()
              activeTxtfld =  self.txtFieldPwd!
            
            self.view.isUserInteractionEnabled = false
            
            var urlString = String()
            urlString += urlString.webAPIDomainNmae()
            urlString += "user/verify?SchCode="
            urlString += strSchoolCode
            urlString += "&key="
            urlString += urlString.keyPath()
            urlString += "&username="
            urlString += (txtFieldUserName?.text)!
            
            print("Func: urlString= \(urlString)")
            
            WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
                DispatchQueue.main.async
                    {
                         Utility.shared.removeLoader()
                        if let result = results
                        {
                            if(result["Status"] as? String == "ok" && result["Message"] as? String == "success")
                            {
                                self.view.isUserInteractionEnabled = true
                                self.txtFieldUserName?.rightView    = nil
                                self.txtFieldUserName?.textColorNoError(text: (self.txtFieldUserName?.text)!)
                                self.intUserType =  (result["UserType"] as? Int)!
                                DBManager.shared.saveNewUser(result: result, schoolCode: self.strSchoolCode ,username : self.txtFieldUserName?.text)
                                if let strName = result["Name"] as? String{
                                    var strName1 = "Hi, "
                                    strName1    += strName
                                    self.lblDisplayName?.text = strName1
                                }
                                if let imgUrl = result["Photo"] as? String{
                                    
                                    var strPrefix      = String()
                                    strPrefix          += strPrefix.imgPath()
                                    var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                                    let url            = URL(string: finalUrlString )
                                    
                                    finalUrlString = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                                    self.imgViewUser?.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                                        
                                        self.imgViewUser?.layer.cornerRadius = (self.imgViewUser?.frame.size.height)! / 2
                                        self.imgViewUser?.clipsToBounds      = true
                                    }
                                }
                                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                                    self.pwdView?.transform = CGAffineTransform(translationX: 0, y: 0)
                                    self.txtFieldPwd?.becomeFirstResponder()
                                    
                                }, completion: nil)
                            }
                            else if(result["Status"] as? String == "ok" && result["Message"] as? String == "No record found")
                            {
                                print("else conditionBottom: nextUsernameAction")
                                self.lblregisteredUserNameHeight?.constant = 15
                                self.lblInvalidPwdHeight?.constant         = 0
                                
                                self.txtFieldUserName?.txtShakeAnimation()
                                self.txtFieldUserName?.borderWidthColor()
                                self.txtFieldUserName?.textColorError(text: (self.txtFieldUserName?.text)!)
                                self.view.isUserInteractionEnabled = true
                                self.txtFieldUserName?.rightView    = nil
                            }
                        }
                        if (error != nil){
                            
                            print("userVerifyAPI = \(error!)")
                            DispatchQueue.main.async
                                {
                                    self.view.isUserInteractionEnabled = true
                                    self.txtFieldUserName?.rightView    = nil
                                    
                                    switch (errorcode!){
                                    case Int(-1009):
                                        
                                        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    default:
                                        print("errorcode = \(String(describing: errorcode))")
                                    }
                            }
                        }
                }
            }
        }
    }
    
    
    // MARK: - forgotBtnPressed
    @IBAction func forgotBtnPressed(_ sender: UIButton) {
        
        if(sender.titleLabel?.text!.lowercased() == "forgot username?"){
            self.userOrPasswrd = "username"
        }else if(sender.titleLabel?.text!.lowercased() == "forgot password?"){
            self.userOrPasswrd = "password"
        }
        self.performSegue(withIdentifier: "fwdUpSegue", sender: self)
        self.view.endEditing(true)
        //animateTxtFld(txtFld: txtFieldUserName, up: false)
    }
    
    
    // MARK: - previousUsernamepressed
    @IBAction func previousUsernamepressed(_ sender: Any){
        print("Bottom: previousUsernamepressed")
        self.navigationController?.popViewController(animated: false)
    }
    
    
    // MARK: - helpUsernameAction
    @IBAction func helpUsernameAction(_ sender: Any){
        print("Bottom: helpUsernameAction")
        openHelpVc()
//           self.performSegue(withIdentifier: "helpAct", sender: nil)
    }
    
    
    func openHelpVc() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let helpController = storyboard.instantiateViewController(withIdentifier: "HelpViewController") as? HelpViewController else {return}
        if let currentSchool = self.getCurrentSchool() , let schName = currentSchool.schName{
            helpController.strEmail   = /currentSchool.supportEmail
            helpController.strPhoneNo = /currentSchool.supportPhone
            helpController.strLogoURL = "\(APIConstants.imgbasePath)" + /currentSchool.schLogo
            helpController.strCity    = /currentSchool.schCity
            helpController.strSchoolName = schName
        }
        self.presentVC(helpController)
    }
    
    // MARK: - previousPwdPressed
    @IBAction func previousPwdPressed(_ sender: Any)
    {
        print("Pwd: previousPwdPressed")
        // pwdView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
            
            self.pwdView?.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
            
        }, completion: nil)
        self.lblregisteredUserNameHeight?.constant = 0
    }
    // MARK: - enterPwdAction
    @IBAction func enterPwdAction(_ sender: Any)
    {
        print("Pwd: enterPwdAction")
        if txtFieldPwd?.text == ""
        {
            //print("blank txtfield")
            self.txtFieldPwd?.borderWidthColorGray()
            self.txtFieldPwd?.txtShakeAnimation()
        }
        else
        {
            txtFieldPwd?.resignFirstResponder()
            Utility.shared.loader()
            self.lblInvalidPwdHeight?.constant         = 0
            self.lblregisteredUserNameHeight?.constant = 0
            
            activeTxtfld =  self.txtFieldPwd!
            
            self.view.isUserInteractionEnabled = false
            
            var strKey = String()
            strKey += strKey.keyPath()
            
            var urlString = String()
            urlString    += urlString.webAPIDomainNmae()
            urlString    += "user/Validate"
            //  print("Login url = \n \(urlString)")
            
            let parameters: Parameters = [
                "SchoolCode": strSchoolCode,
                "key": strKey,
                "Username": txtFieldUserName?.text! ?? "",
                "Password": txtFieldPwd?.text! ?? ""
            ]
            WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
                DispatchQueue.main.async
                    {
                         Utility.shared.removeLoader()
                        if let result = results
                        {
                            if(result["Status"] as? String == "ok" && result["isAuthenticate"] as? Bool == true && result["ErrorCode"] as? Int == 0)
                            {
                                self.view.isUserInteractionEnabled = true
                                DBManager.shared.saveUserData(result: result, senderCode: self.strSchoolCode, username: self.txtFieldUserName?.text , password: self.txtFieldPwd?.text)
                                
                                self.dictResult   = result
                                print("dictResult = \(self.dictResult)")
                                self.performSegue(withIdentifier: "customTabSegve", sender: self)
                            }
                            else //if(result["Status"] as? String == "error" )
                            {
                                self.lblregisteredUserNameHeight?.constant = 0
                                self.lblInvalidPwdHeight?.constant         = 15
                                
                                self.txtFieldPwd?.txtShakeAnimation()
                                self.txtFieldPwd?.borderWidthColor()
                                self.txtFieldPwd?.textColorError(text: (self.txtFieldPwd?.text!)!)
                                self.view.isUserInteractionEnabled = true
                            }
                        }
                        if (error != nil){
                            
                            
                            print("PwdVerifyAPI = \(error!)")
                            DispatchQueue.main.async
                                {
                                    self.view.isUserInteractionEnabled = true
                                    
                                    switch (errorcode!){
                                    case Int(-1009):
                                        
                                        let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    default:
                                        print("errorcode = \(String(describing: errorcode))")
                                    }
                            }
                        }
                }
            })
        }
    }
    
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if self.timer != nil
        {
            self.timer?.invalidate()
            self.timer = nil
        }
        boolTimer = false
        txtFieldUserName?.resignFirstResponder()
        txtFieldPwd?.resignFirstResponder()
    }
    // MARK: - TextField Delegate Method
    func textFieldDidBeginEditing(_ textField: UITextField) {

        //Nupur 14/Nov
        textField.textColor = UIColor(red: 84/255, green: 84/255, blue: 84/255, alpha:1)
        textField.borderColorClean()
        self.lblregisteredUserNameHeight?.constant = 0
        self.lblInvalidPwdHeight?.constant         = 0
        animateTxtFld(txtFld: textField , up: true)
    }
    func textFieldDidEndEditing(_ textField: UITextField){
        animateTxtFld(txtFld: textField, up: false)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true // We do not want UITextField to insert line-breaks.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let maxLength = 26
        txtFieldPwd?.rightView       = nil

        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if newString.length == 0{
            buttonRight?.setImage(R.image.hideEye(), for: .normal)//eyes
        }
        if newString.length > 0
        {
            // btnOK.setImage(UIImage(named:"Green right"), for: .normal)
            if textField.tag == 10
            {
                // btnNextUser.isUserInteractionEnabled = true
                self.txtFieldUserName?.layer.borderColor  = UIColor.clear.cgColor
            }
            else  if textField.tag == 11
            {
                txtFieldPwd?.rightView       = buttonRight
                // btnEnterPwd.isUserInteractionEnabled = true
                self.txtFieldPwd?.layer.borderColor  = UIColor.clear.cgColor
                self.txtFieldPwd?.textColorNoError(text: (self.txtFieldPwd?.text!)!)
                self.lblInvalidPwdHeight?.constant = 0
            }
        }
        else{
            // btnOK.setImage(UIImage(named:"Grey right"), for: .normal)
            if textField.tag == 10
            {
                //btnNextUser.isUserInteractionEnabled     = false
                self.txtFieldUserName?.layer.borderColor  = UIColor.clear.cgColor
                self.txtFieldUserName?.textColorNoError(text: (self.txtFieldUserName?.text!)!)
                self.lblregisteredUserNameHeight?.constant = 0
            }
            else  if textField.tag == 11
            {
                // btnEnterPwd.isUserInteractionEnabled     = false
                self.txtFieldPwd?.layer.borderColor  = UIColor.clear.cgColor
                self.txtFieldPwd?.textColorNoError(text: (self.txtFieldPwd?.text!)!)
                self.lblInvalidPwdHeight?.constant = 0
            }
        }
        //Nupur 14/Nov
        let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
        let filtered = string.components(separatedBy: cs).joined(separator: "")
        
        if(textField == txtFieldUserName){
            return newString.length <= maxLength && (string == filtered)
        }else{
            return newString.length <= maxLength
        }
    }
    // MARK: - animateTxtFld
    func animateTxtFld(txtFld: UITextField, up: Bool){
        
        let movementDistance = -160
        let movementDuration  = 0.3
        let movement = (up ? movementDistance : -movementDistance)
        
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        boolTimer = false
        
        if segue.identifier == "fwdUpSegue" {
            let fwdController = segue.destination as! FwdViewController
            fwdController.forgotLabel    = userOrPasswrd
            fwdController.username       = (txtFieldUserName?.text)!
            fwdController.intSMSPvdID    = intSMSPvdID
            fwdController.intCountryCode = intCountryCode
            fwdController.strSchoolCode  = strSchoolCode
            fwdController.intUserType    = intUserType
        }
        else if segue.identifier == "customTabSegve"
        {
            //let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.dictResult)
           // let encodedDataimgSchool = NSKeyedArchiver.archivedData(withRootObject: imgSchool)
//            UDKeys.imgSchool.save(encodedDataimgSchool)
            UserDefaults.standard.synchronize()
        }
        else if segue.identifier == "helpAct" {
           
            if let currentSchool = self.getCurrentSchool() , let schName = currentSchool.schName{
                let vc = segue.destination as! HelpViewController
                vc.strEmail = /currentSchool.supportEmail
                vc.strPhoneNo = /currentSchool.supportPhone
                vc.strSchoolName = schName
                vc.strLogoURL = /currentSchool.schLogo
               
            }
        }
    }
}
extension UserverifyViewController {
    
    func getSliderData() {
        var strSchoolCodeverifyURL = String()
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.webAPIDomainNmae()
        strSchoolCodeverifyURL += "school/DTL?SchCode="
        strSchoolCodeverifyURL += /self.getCurrentSchool()?.schCode?.uppercased()
        strSchoolCodeverifyURL += "&key="
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.keyPath()
        
        WebserviceManager.getJsonData(withParameter: strSchoolCodeverifyURL) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["ErrorCode"] as? Int == 0 && result["Status"] as? String == "ok")
                        {
                            UserDefaults.standard.synchronize()
                            if(result["Active"] as? Int == 1){
                                self.arraySliderImageURLs = (result["Slider"] as? [[String: Any]])!
                                self.createpages()
                            }
                        }
                    }
            }
        }
    }
}
