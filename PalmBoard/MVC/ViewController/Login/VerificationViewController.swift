//  VerificationViewController.swift
//  e-Care Pro
//  Created by Shubham on 07/09/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import Alamofire

class VerificationViewController: UIViewController {
    
    @IBOutlet weak var txtUsername : UITextField?
    @IBOutlet weak var txtPassword : UITextField?
    @IBOutlet weak var btnNext     : UIButton?
    @IBOutlet weak var imgProfile  : UIImageView?
    @IBOutlet weak var lblName     : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onViewWillAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        txtUsername?.resignFirstResponder()
        txtPassword?.resignFirstResponder()
    }
    func onViewWillAppear() {
        guard let user = self.getCurrentUser() else {return}
        if let url = user.photoUrl?.getImageUrl() {
            imgProfile?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: R.image.noProfile_Big())
        }
        else {
            imgProfile?.image = R.image.noProfile_Big()
        }
        if let name = user.name {
            lblName?.text = "Hi, \(name)"
        }
    }
    func setUpView() {
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: NOTI.VERIFICATION_SUCCESS), object: nil, queue: nil) { (_) in
            self.dismissVC(completion: nil)
        }
    }
    @IBAction func btnNextAct(_ sender : UIButton) {
        self.view.endEditing(true)
        if "".validateUser(username: txtUsername?.text, password: txtPassword?.text) {
            varifyUserView()
        }
    }
    func varifyUserView() {
        var strKey = String()
        strKey += strKey.keyPath()
        guard let schoolCode = DBManager.shared.getAllSchools().first?.schCode else {return}
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "user/Validate"
        
        let parameters: Parameters = [
            "SchoolCode": schoolCode,
            "key": strKey,
            "Username": /txtUsername?.text,
            "Password": /txtPassword?.text
        ]
        Utility.shared.loader()
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["isAuthenticate"] as? Bool == true && result["ErrorCode"] as? Int == 0)
                        {
                            self.view.isUserInteractionEnabled = true
                            guard let user = result["UserDTL"] as? [String: Any] else {return}
                            
                            if DBManager.shared.getAuthenticatedUser().count > 0 {
                            
                           if let currentUser = self.getCurrentUser(){
                  
                            if let userID = user["UserID"] as? Int32 , let userType = user["UserType"] as? Int32
                            {
                                if currentUser.userID == userID && currentUser.userType == userType {
                                    DBManager.shared.saveUserData(result: result, senderCode: schoolCode, username: self.txtUsername?.text , password: self.txtPassword?.text, forVerification:  true)
                                }
                                else {
                                    Messages.shared.show(alert: .oops, message: "Invalid Username or Password", type: .warning)
                                }
                            }
                            else {
                                Messages.shared.show(alert: .oops, message: "Invalid Username or Password", type: .warning)
                            }
                        }
                        else {
                            Messages.shared.show(alert: .oops, message: "Invalid Username or Password", type: .warning)
                        }
                            }
                    }
                    if (error != nil){
                        
                        
                        print("PwdVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
            }
        }
        )
    }
}
extension VerificationViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtUsername {
            txtPassword?.becomeFirstResponder()
        }
        if textField == txtPassword {
            txtPassword?.resignFirstResponder()
        }
        return true
    }
}

