import UIKit

class AddSchoolTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewSchoolLogo: UIImageView?
    @IBOutlet weak var lblSchoolName:     UILabel?
    @IBOutlet weak var lblCity:           UILabel?
    @IBOutlet weak var lblAcademicYr:     UILabel?
    
    override func awakeFromNib() {super.awakeFromNib()}
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)
}
}
