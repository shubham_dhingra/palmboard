import UIKit
import Alamofire

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var scrollView:          UIScrollView?
    
    @IBOutlet weak var imgViewCover:        UIImageView?
    @IBOutlet weak var btnEditCover:        UIButton?
    @IBOutlet weak var btnEditProfile:      UIButton?
    
    @IBOutlet weak var btnProfilePic:       UIButton?
    @IBOutlet weak var lblPending:          UILabel?
    @IBOutlet weak var lblProfilePending:   UILabel?
    
    @IBOutlet weak var lblStName:           UILabel?
    @IBOutlet weak var lblStClass:          UILabel?
    @IBOutlet weak var lblUsername:         UILabel?
    @IBOutlet weak var lblContact:          UILabel?
    
    @IBOutlet weak var viewBgUsername:      UIView?
    @IBOutlet weak var txtFldUsername:      UITextField?
    @IBOutlet weak var btnEditUsername:     UIButton?
    @IBOutlet weak var viewBgContact:       UIView?
    @IBOutlet weak var viewChangePassword : UIView?
    @IBOutlet weak var txtFldContact:       UITextField?
    @IBOutlet weak var btnEditContact:      UIButton?
    
    @IBOutlet weak var viewBluredBg:        UIView?
    @IBOutlet weak var viewVerificationOTP: UIView?
    @IBOutlet weak var txtFldOTP1:          UITextField?
    @IBOutlet weak var txtFldOTP2:          UITextField?
    @IBOutlet weak var txtFldOTP3:          UITextField?
    @IBOutlet weak var txtFldOTP4:          UITextField?
    @IBOutlet weak var btnRightOTP:         UIButton?
    @IBOutlet weak var btnResendOTP:        UIButton?
    
    @IBOutlet weak var btnBackNavigation: UIBarButtonItem?
    
    @IBOutlet weak var btnFB:            UIButton?
    @IBOutlet weak var btnTwitter:       UIButton?
    @IBOutlet weak var btnLinkedIn:      UIButton?
    
    @IBOutlet weak var viewBgFB:         UIView?
    @IBOutlet weak var txtFieldFB:       UITextField?
    @IBOutlet weak var btnEditFB:        UIButton?
    
    @IBOutlet weak var viewBgTwitter:    UIView?
    @IBOutlet weak var txtFieldTwitter:  UITextField?
    @IBOutlet weak var btnEditTwitter:   UIButton?
    
    @IBOutlet weak var viewBgLinkedIn :  UIView?
    @IBOutlet weak var txtFieldLinkedIn: UITextField?
    @IBOutlet weak var btnEditLinkedIn:  UIButton?
    
    let ACCEPTABLE_CHARACTERS  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    let ACCEPTABLE_CHARACTERS2 = "0123456789"
    var imagePicker         : UIImagePickerController?
    var stName              : String?
    var stClass             : String?
    var stUsername          : String?
    var stContactNo         : String?
    var strFBURL            : String?
    var strTwitterURL       : String?
    var strLinkedInURL      : String?
    
    var stProfilePicUrl     : URL?
    var stCoverPicUrl       : URL?
    var activeTextField     : UITextField?
    var strSchoolCodeSender : String?
    var intUserType         : Int?
    var intUserID           : Int?
    var intSMSPrvdId        : Int?
    var imageData           : Data?
    
    var ifTxtUsernameUp    : Bool?
    var ifNumberVerified   : Bool?
    var ifContactEditing   : Bool?
    var ifUsernameEditing  : Bool?
    var strEditImageName   : String?
    var strOTPGenerated    : String?
    var intResendCount     : Int?
    var intCoverImg        : Int?
    var intProfileImg      : Int?
    
    //var modal : MainProfileModel?
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Utility.shared.removeLoader()
        self.viewBluredBg?.isHidden     = true
        
        self.strEditImageName           = "edit pencil2"
        viewBluredBg?.isHidden          = true
        viewVerificationOTP?.isHidden   = true
        
        txtFldUsername?.returnKeyType   = .done
        txtFldContact?.returnKeyType    = .done
        
        txtFieldFB?.returnKeyType       = .done
        txtFieldTwitter?.returnKeyType  = .done
        txtFieldLinkedIn?.returnKeyType = .done
        
        ifTxtUsernameUp   = false
        ifNumberVerified  = false
        ifContactEditing  = false
        ifUsernameEditing = false
        
        if let schoolCode = self.getCurrentSchool()?.schCode    {strSchoolCodeSender =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID       {self.intUserID      =  Int(userId)}
        if let userType   = self.getCurrentUser()?.userType     {self.intUserType    =  Int(userType)}
        if let check      = self.getCurrentSchool()?.sMS_Prvd_ID{intSMSPrvdId        =  Int(check)}
        
        btnFB?.isHidden           = self.intUserType == 3  || self.intUserType == 1
        btnTwitter?.isHidden      = self.intUserType == 3  || self.intUserType == 1
        btnLinkedIn?.isHidden     = self.intUserType == 3  || self.intUserType == 1
        viewBgFB?.isHidden        = self.intUserType == 3  || self.intUserType == 1
        viewBgTwitter?.isHidden   = self.intUserType == 3  || self.intUserType == 1
        viewBgLinkedIn?.isHidden  = self.intUserType == 3  || self.intUserType == 1
        viewBgContact?.isHidden   = true //self.intUserType == 3  || self.intUserType == 2
        
        btnEditCover?.isHidden   = self.intCoverImg == 1
        lblPending?.isHidden     = self.intCoverImg == 0
        
        btnEditProfile?.isHidden     = self.intProfileImg == 1
        lblProfilePending?.isHidden  = self.intProfileImg == 0
        
        
        switch self.intCoverImg {
        case 2:
            lblPending?.backgroundColor = UIColor(rgb: 0xF3565D)
            lblPending?.text = "Rejected"
        default:
            lblPending?.backgroundColor = UIColor(rgb: 0x4EAD4A)
            lblPending?.text = "Pending"
        }
        switch self.intProfileImg {
        case 2:
            lblProfilePending?.backgroundColor = UIColor(rgb: 0xF3565D)
            lblProfilePending?.text = "Rejected"
        default:
            lblProfilePending?.backgroundColor = UIColor(rgb: 0x4EAD4A)
            lblProfilePending?.text = "Pending"
        }
        btnEditProfile?.isUserInteractionEnabled = true
        
        if (self.intUserType == 1) || (self.intUserType == 3)
        {
            btnEditProfile?.isHidden     = true
            lblProfilePending?.isHidden  = true
        }
        
        lblStName?.text         = "\(/stName)"
        lblStClass?.text        = "\(/stClass)"
        lblUsername?.text       = "\(/stUsername)"
        lblContact?.text        = "\(/stContactNo)"
        
        txtFldUsername?.text    = "\(/stUsername)"
        txtFldContact?.text     = "\(/stContactNo)"
        txtFieldFB?.text        = "\(/strFBURL)"
        txtFieldTwitter?.text   = "\(/strTwitterURL)"
        txtFieldLinkedIn?.text  = "\(/strLinkedInURL)"
        
        viewBgUsername?.layer.cornerRadius      = /viewBgUsername?.frame.height / 2
        viewBgUsername?.layer.masksToBounds     = true
        viewBgContact?.layer.cornerRadius       = /viewBgContact?.frame.height / 2
        viewBgContact?.layer.masksToBounds      = true
        viewChangePassword?.layer.cornerRadius  = /viewChangePassword?.frame.height / 2
        viewChangePassword?.layer.masksToBounds = true
        viewBgFB?.layer.cornerRadius            = /viewBgFB?.frame.height / 2
        viewBgFB?.layer.masksToBounds           = true
        viewBgTwitter?.layer.cornerRadius       = /viewBgTwitter?.frame.height / 2
        viewBgTwitter?.layer.masksToBounds      = true
        viewBgLinkedIn?.layer.cornerRadius      = /viewBgLinkedIn?.frame.height / 2
        viewBgLinkedIn?.layer.masksToBounds     = true
        
        
        
        self.imgViewCover?.af_setImage(withURL: stCoverPicUrl!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
        }
        
        let imageView =  UIImageView()
        if stProfilePicUrl != nil{
            imageView.af_setImage(withURL: stProfilePicUrl!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                let img1 = imageView.image?.af_imageRoundedIntoCircle()
                
                self.btnProfilePic?.setBackgroundImage(img1, for: .normal)
                self.btnProfilePic?.layer.masksToBounds = true
                self.btnProfilePic?.backgroundColor     = .clear
                self.btnProfilePic?.layer.cornerRadius  = /self.btnProfilePic?.frame.height / 2
                self.btnProfilePic?.layer.borderWidth   = 5
                self.btnProfilePic?.layer.borderColor   = UIColor.white.cgColor
                if(self.intUserType == 1){
                    self.btnProfilePic?.isUserInteractionEnabled = false
                }
            }
        }
        else{
            self.btnProfilePic?.setBackgroundImage(R.image.noProfile_Big(), for: .normal)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    func openChangePasswordVc(){
        if let changePasswordVc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController {
            self.navigationController?.presentVC(changePasswordVc)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : true , "Index" : 2])

        self.isNavBarHidden = false
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
    }
    // MARK: - willEnterForeground
    @objc func willEnterForeground() {
        if(/ifTxtUsernameUp){
            ifTxtUsernameUp = false
            // self.animateTxtFld(txtFld: self.activeTextField, up: true)
            self.activeTextField?.resignFirstResponder()
        }
    }
    
    @IBAction func btnChangePasswordAct(_ sender: UIButton) {
        openChangePasswordVc()
    }
    // MARK: - backBtnPressed
    @IBAction func backBtnPressed(_ sender: UIBarButtonItem) {
        
        if(self.strEditImageName == "Right Selected" || /ifNumberVerified)
        {
            WebserviceManager.showAlert(message: "Changes you have made are not saved. Tap on the tick to save changes.", title: "", firstBtnText: "Discard", secondBtnText: "Cancel", firstBtnBlock:
                {
                    self.navigationController?.popViewController(animated: true)
            }, secondBtnBlock: {})
            
        }else{
            // self.delegate?.updateProfile()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - textfield Delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.btnBackNavigation?.isEnabled = false
        self.scrollView?.isScrollEnabled  = false
        activeTextField                   = textField
        
        if(ifTxtUsernameUp == false){
            self.animateTxtFld(txtFld: textField, up: true)
        }else
        {
            if(textField == self.txtFldContact){
                self.txtFldUsername?.textColor = UIColor(red: 64/255 ,green: 138/255 , blue: 243/255,alpha: 1.0)
                self.btnEditUsername?.setBackgroundImage(R.image.editPencil2(), for: .normal)
                self.strEditImageName = "edit pencil2"
                
            }else{
                self.txtFldContact?.textColor = UIColor(red: 64/255 ,green: 138/255 , blue: 243/255,alpha: 1.0)
                self.btnEditContact?.setBackgroundImage(R.image.editPencil2(), for: .normal)
            }
        }
        if(textField == self.txtFldUsername){
            self.strEditImageName = "Cancel editing"
            ifContactEditing      = false
            self.txtFldContact?.resignFirstResponder()
            self.btnEditUsername?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            textField.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
        }
        if(textField == self.txtFieldFB){
            self.strEditImageName = "Cancel editing"
            ifContactEditing      = false
            self.txtFldContact?.resignFirstResponder()
            self.txtFldUsername?.resignFirstResponder()
            self.txtFieldTwitter?.resignFirstResponder()
            self.txtFieldLinkedIn?.resignFirstResponder()
            
            self.btnEditFB?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            textField.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
        }
        if(textField == self.txtFieldTwitter){
            self.strEditImageName = "Cancel editing"
            ifContactEditing      = false
            self.txtFldContact?.resignFirstResponder()
            self.txtFldUsername?.resignFirstResponder()
            self.txtFieldFB?.resignFirstResponder()
            self.txtFieldLinkedIn?.resignFirstResponder()
            
            self.txtFldContact?.resignFirstResponder()
            self.btnEditTwitter?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            textField.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
        }
        if(textField == self.txtFieldLinkedIn){
            self.strEditImageName = "Cancel editing"
            ifContactEditing      = false
            self.txtFldContact?.resignFirstResponder()
            self.txtFldUsername?.resignFirstResponder()
            self.txtFieldFB?.resignFirstResponder()
            self.txtFieldTwitter?.resignFirstResponder()
            self.btnEditLinkedIn?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            textField.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
        }
        else if(textField == self.txtFldContact)
        {
            ifContactEditing    = true
            self.txtFldUsername?.resignFirstResponder()
            self.btnEditContact?.setBackgroundImage(R.image.cancelEditing(), for: .normal)
            textField.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
        }
        self.viewBgContact?.backgroundColor  = UIColor(red:243/255,green:243/255,blue: 243/255,alpha:1.0)
        self.viewBgUsername?.backgroundColor = UIColor(red:243/255,green:243/255,blue: 243/255,alpha:1.0)
        self.viewBgFB?.backgroundColor       = UIColor(red:243/255,green:243/255,blue: 243/255,alpha:1.0)
        self.viewBgTwitter?.backgroundColor  = UIColor(red:243/255,green:243/255,blue: 243/255,alpha: 1.0)
        self.viewBgLinkedIn?.backgroundColor = UIColor(red:243/255,green:243/255,blue: 243/255,alpha: 1.0)
        
        self.txtFldOTP1?.textColorYellow(text: /self.txtFldOTP1?.text)
        self.txtFldOTP2?.textColorYellow(text: /self.txtFldOTP2?.text)
        self.txtFldOTP3?.textColorYellow(text: /self.txtFldOTP3?.text)
        self.txtFldOTP4?.textColorYellow(text: /self.txtFldOTP4?.text)
        
        self.txtFldOTP1?.borderColorClean()
        self.txtFldOTP2?.borderColorClean()
        self.txtFldOTP3?.borderColorClean()
        self.txtFldOTP4?.borderColorClean()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.scrollView?.isScrollEnabled  = true
        self.btnBackNavigation?.isEnabled = true
        
        if (textField == self.txtFieldFB)
        {
            self.strEditImageName = "edit pencil2"
            textField.resignFirstResponder()
            self.scrollView?.isScrollEnabled  = true
            self.btnBackNavigation?.isEnabled = true
            
            self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
            self.btnEditFB?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            self.UpdateFBURL()
        }
        else if (textField == self.txtFieldTwitter)
        {
            self.strEditImageName = "edit pencil2"
            textField.resignFirstResponder()
            self.scrollView?.isScrollEnabled  = true
            self.btnBackNavigation?.isEnabled = true
            
            self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
            self.btnEditTwitter?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            self.UpdateTwitterURL()
        }
        else if (textField == self.txtFieldLinkedIn)
        {
            self.strEditImageName = "edit pencil2"
            textField.resignFirstResponder()
            self.scrollView?.isScrollEnabled  = true
            self.btnBackNavigation?.isEnabled = true
            
            self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
            self.btnEditLinkedIn?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
            self.UpdateLinkedInURL()
        }
        else if (textField == self.txtFldUsername)
        {
            self.strEditImageName = "edit pencil2"
            if((textField.text?.count)! < 6){
                WebserviceManager.showAlert(message: "Minimum 6 characters required.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
            }
            else if (textField.text == self.stUsername){
                textField.resignFirstResponder()
                self.scrollView?.isScrollEnabled  = true
                self.btnBackNavigation?.isEnabled = true
                
                self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
                if (/ifUsernameEditing){
                    WebserviceManager.showAlert(message: "You are entering the old username.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                }
                self.btnEditUsername?.setBackgroundImage(UIImage(named:/self.strEditImageName), for: .normal)
            }else
            {
                textField.resignFirstResponder()
                self.scrollView?.isScrollEnabled  = true
                self.btnBackNavigation?.isEnabled = true
                
                self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
                self.btnEditUsername?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
                self.usernameVerify()
            }
        }
        else if(textField == self.txtFldContact){
            
            ifContactEditing = false
            
            if(self.txtFldContact?.text?.trimmingCharacters(in: .whitespaces) == ""){
                self.txtFldContact?.text = "\(/stContactNo)"
                self.btnEditContact?.setBackgroundImage(R.image.editPencil2(), for: .normal)
                self.txtFldContact?.textColor = UIColor(red: 64/255 , green: 138/255 , blue: 243/255,alpha: 1.0)
                
            }else if (textField.text?.count != 10){
                WebserviceManager.showAlert(message: "Phone number should be 10 digits long.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
            }else{
                if(textField.text == self.stContactNo){
                    self.txtFldContact?.text = "\(/stContactNo)"
                    self.btnEditContact?.setBackgroundImage(R.image.editPencil2(), for: .normal)
                    self.txtFldContact?.textColor = UIColor(red: 64/255 , green: 138/255 , blue: 243/255,alpha: 1.0)
                }else{
                    if let phoneNumberValidator = textField.text?.isPhoneNumber{
                        if (phoneNumberValidator)
                        {
                            ifNumberVerified = true
                            self.btnEditContact?.setBackgroundImage(R.image.rightSelected(), for: .normal)
                            self.txtFldContact?.textColor = UIColor(red: 64/255 , green: 138/255 , blue: 243/255,alpha: 1.0)
                        }else
                        {
                            ifNumberVerified = false
                            self.btnEditContact?.setBackgroundImage(R.image.editPencil2(), for: .normal)
                            self.txtFldContact?.textColor = UIColor(red: 64/255 , green: 138/255 , blue: 243/255,alpha: 1.0)
                        }
                    }
                }
            }
            textField.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
        }
        self.txtFldUsername?.textColor = UIColor(red: 64/255 ,green: 138/255 , blue: 243/255,alpha: 1.0)
        return false
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == self.txtFldUsername)
        {
            ifUsernameEditing = true
            let newText       = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            let cs            = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered      = string.components(separatedBy: cs).joined(separator: "")
            return numberOfChars <= 20 && (string == filtered)
        }
        else if (textField == self.txtFldContact)
        {
            ifUsernameEditing = false
            let cs2       = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS2).inverted
            let filtered2 = string.components(separatedBy: cs2).joined(separator: "")
            return string == filtered2
        }
            
        else if (textField == self.txtFldOTP1 || textField == self.txtFldOTP2 || textField == self.txtFldOTP3 || textField == self.txtFldOTP4 )
        {
            ifUsernameEditing  = false
            let currentString           = textField.text! as NSString
            let newString               = currentString.replacingCharacters(in: range, with: string)
            let compSepByCharInSet      = string.components(separatedBy: aSet)
            let numberFiltered          = compSepByCharInSet.joined(separator: "")
            
            if ((newString.count) <= 1 && string == numberFiltered) == true{
                textField.addTarget(self, action: #selector(self.textFieldMoveNext(textfield: )), for: UIControl.Event.editingChanged)
            }
            return (newString.count) <= maxLength && string == numberFiltered
        }
        return true
    }
    // MARK: - textFieldMoveNext
    @objc func textFieldMoveNext(textfield : UITextField)//Custom
    {
        let newString12 = textfield.text!
        //print("newString12 in next method = \(newString12)")
        if (newString12.count == 0){
            textFieldMoveBack(textfield: textfield)//Custom
        }
        else{
            let nextTag       = textfield.tag + 1
            let nextResponder = textfield.superview?.viewWithTag(nextTag) as UIResponder?
            if (nextResponder != nil)
            {
                nextResponder?.becomeFirstResponder()
            } else {
                
                textfield.resignFirstResponder()
                self.scrollView?.isScrollEnabled = true
                self.btnBackNavigation?.isEnabled = true
                self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
                btnRightOTP?.isUserInteractionEnabled = true
                btnRightOTP?.setImage(R.image.greenRight(), for: .normal)
            }
        }
    }
    // MARK: - textFieldMoveBack
    func textFieldMoveBack(textfield : UITextField)//Custom
    {
        self.btnRightOTP?.isUserInteractionEnabled = false
        self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
        let nextTag = textfield.tag - 1
        let nextResponder = textfield.superview?.viewWithTag(nextTag) as UIResponder?
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        }
        else{
            textfield.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.txtFldContact!, up: false)
        }
    }
    // MARK: - animateTxtFld
    func animateTxtFld(txtFld: UITextField, up: Bool){
        let movementDistance = -180
        let movementDuration  = 0.3
        if(up == true){
            ifTxtUsernameUp = true
        }else{
            ifTxtUsernameUp = false
        }
        let movement = (up ? movementDistance : -movementDistance)
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
            print("self.view.frame y \n \(self.view.frame.origin.y)")
        }
    }
   
    // MARK: - sendOTP
    func sendOTP(){
        
        Utility.shared.loader()
        self.viewBluredBg?.isHidden = false
        
        self.btnResendOTP?.isUserInteractionEnabled = false
        self.btnResendOTP?.setTitleColor(UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha:1), for: .normal)
        let OTP = arc4random_uniform(8999) + 1000
        var SMS = "Your e-Care Pro phone number verification code is \(OTP)"
        strOTPGenerated = "\(OTP)"
        SMS = SMS.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        var urlString = String()
        urlString += APIConstants.basePath + "SMSService/SingleSMS?SchCode=\(/self.strSchoolCodeSender)&key=\(urlString.keyPath())&mobile=\(/self.txtFldContact?.text)&Msg=\(SMS)&prvd_ID=\(/self.intSMSPrvdId)&CountryCode=91"
        //print("urlString \n \(urlString)")
        
        let utilityQueue = DispatchQueue.global(qos: .utility)
        var url = urlString
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60
        
        manager.request(url).responseJSON(queue: utilityQueue)
        {
            response in
            DispatchQueue.main.async
                {
                    switch response.result
                    {
                    case .success(let JSON):
                        let jsonData = JSON as! [String : Any]
                        
                        Utility.shared.removeLoader()

                        self.viewBluredBg?.isHidden = true
                        self.txtFldOTP1?.text = ""
                        self.txtFldOTP2?.text = ""
                        self.txtFldOTP3?.text = ""
                        self.txtFldOTP4?.text = ""
                        
                        if(jsonData["Status"] as? String == "ok" && jsonData["ErrorCode"] as? Int == 0)
                        {
                            //print("OTP result \n \(result)")
                            self.navigationController?.navigationBar.isHidden = true
                            self.viewBluredBg?.isHidden = false
                            self.viewVerificationOTP?.isHidden = false
                            self.btnRightOTP?.isUserInteractionEnabled = false
                            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
                            
                        }else
                        {
                            self.navigationController?.navigationBar.isHidden = false
                            WebserviceManager.showAlert(message: "Error in sending verification code. Please check if you have entered a valid phone number", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                        break
                        
                    case .failure(let error):
                        let err = error as NSError
                        if(err.code == -1009){
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                            self.cancelOTPview()
                        } else if error._code == NSURLErrorTimedOut {
                            //HANDLE TIMEOUT HERE
                            //print("Time OUT 60 SEC")
                            self.btnResendOTP?.isUserInteractionEnabled = true
                            self.btnResendOTP?.setTitleColor(UIColor(red: 255/255, green: 187/255, blue: 0/255, alpha:1), for: .normal)
                        }
                        break
                    }
            }
        }
    }
    // MARK: - usernameUpdate
    func usernameUpdate()
    {
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        var urlString = String()
        urlString += APIConstants.basePath + "user/ChangeUsername"
        
        //let range = stUsername?.index(after: stUsername?.startIndex)..<stUsername?.endIndex
        //stUsername[range]  // "ello, playground"
        
        let parameters: Parameters = [
            "SchoolCode" : "\(/self.strSchoolCodeSender)",
            "key"        : "\(urlString.keyPath())",
            "OldUsername": "\(stUsername?.dropFirst() ?? "")",
            "NewUsername": "\(self.txtFldUsername?.text!.dropFirst() ?? "")"
        ]
        print("URLString = \(urlString)")
        print("parameters = \(parameters)")
        
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        print("Update result \n \(result)")
                        Utility.shared.removeLoader()

                        self.viewBluredBg?.isHidden = true
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            self.strEditImageName = "Re edit"
                            self.txtFldUsername?.textColor = UIColor.white
                            self.btnEditUsername?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
                            self.viewBgUsername?.backgroundColor = UIColor(red: 75/255 ,green: 217/255 , blue: 100/255,alpha: 1.0)
                            self.stUsername =   self.txtFldUsername?.text!
                            // UserDefaults.standard.set(self.txtFldUsername?.text! ,forKey: "username")
                            
                            let username =  "\(self.txtFldUsername?.text!.dropFirst() ?? "")"
                            DBManager.shared.updateUserDetailsIntoLocalDB(newValue:username.lowercased() , entity: .username)
                            
                        }else if(result["Status"] as? String == "error" )
                        {
                            //error
                        }
                    }
            }
        })
    }
    // MARK: - usernameVerify
    func usernameVerify()
    {
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        var urlString = String()
        urlString = APIConstants.basePath + "user/verify?SchCode=\(/self.strSchoolCodeSender)&key=\(urlString.keyPath())&username=\(/self.txtFldUsername?.text!)"
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()

                    self.viewBluredBg?.isHidden = true
                    if let result = results
                    {
                        // print("result \n \(result)")
                        if(result["isVerified"] as? Bool == true){
                            WebserviceManager.showAlert(message: "Username already exists", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                self.txtFldUsername?.text = "\(/self.stUsername)"
                                self.strEditImageName = "edit pencil2"
                                self.btnEditUsername?.setBackgroundImage(UIImage(named: /self.strEditImageName), for: .normal)
                                
                            }, secondBtnBlock: {})
                            
                        }else{
                            self.strEditImageName = "Right Selected"
                            self.btnEditUsername?.setBackgroundImage(R.image.rightSelected(), for: .normal)
                        }
                        if (error != nil){
                            switch (errorCode!){
                            case Int(-1009):
                                WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                            default:
                                print("Error")
                            }
                        }
                    }
            }
        }
        
    }
    // MARK: - contactUpdate
    func contactUpdate(){
        if(ifTxtUsernameUp == true){
            self.activeTextField?.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.activeTextField!, up: false)
        }
        self.navigationController?.navigationBar.isHidden = false
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        var urlString = String()
        
        urlString += APIConstants.basePath + "user/ChangeStudentMobile"
        
        let parameters: Parameters = [
            "SchoolCode": "\(/self.strSchoolCodeSender)",
            "key"       : "\(urlString.keyPath())",
            "Mobile"    : "\(/self.txtFldContact?.text)",
            "UserID"    : "\(/self.intUserID)"
        ]
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        //print("Number update result \n \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            
                            self.stContactNo =  self.txtFldContact?.text
                            self.viewBluredBg?.isHidden = true
                            self.viewVerificationOTP?.isHidden = true
                            self.btnRightOTP?.isUserInteractionEnabled = false
                            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
                            
                            self.txtFldContact?.textColor = UIColor.white
                            self.btnEditContact?.setBackgroundImage(R.image.reEdit(), for: .normal)
                            self.viewBgContact?.backgroundColor = UIColor(red: 75/255 ,green: 217/255 , blue: 100/255,alpha: 1.0)
                        }
                    }
                    if(error != nil){
                        Utility.shared.removeLoader()
                        self.viewBluredBg?.isHidden = true
                        if(errorcode == -1009){
                            
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }else{
                            
                            WebserviceManager.showAlert(message: "Failed to update the username. Please try again", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                    }
            }
        })
    }
    // MARK: - editBtnPressed
    @IBAction func editBtnPressed(_ sender: UIButton) {
        if(sender == btnEditProfile){
            CameraGalleryPickerBlock.shared.pickerImage(pickedListner: {[weak self](image, filename) in
                guard let scaledImage = image.scalingAndCropping(for:CGSize(width: 432, height: 506)) else {return}
                guard let data = scaledImage.jpegData(compressionQuality: 0.5) else {return}
                self?.imageData = data
                self?.uploadProfilePic(completionBlock: { (success) in
                    //print("success = \(success!)")
                    if(success)!{
                        self?.btnProfilePic?.setBackgroundImage(image.af_imageRoundedIntoCircle(), for: .normal)
                        self?.btnProfilePic?.layer.masksToBounds = true
                        self?.btnProfilePic?.contentMode         = .scaleAspectFill
                        self?.btnProfilePic?.clipsToBounds       = true
                        
                        self?.btnProfilePic?.layer.masksToBounds = true
                        self?.btnProfilePic?.backgroundColor     = .clear
                        self?.btnProfilePic?.layer.cornerRadius  = /self?.btnProfilePic?.frame.height / 2
                        self?.btnProfilePic?.layer.borderWidth   = 5
                        self?.btnProfilePic?.layer.borderColor   = UIColor.white.cgColor

                        self?.btnEditProfile?.isHidden           = true
                        self?.lblProfilePending?.text            = "Pending"
                        self?.lblProfilePending?.isHidden        = false
                        self?.lblProfilePending?.backgroundColor = UIColor(rgb: 0x4EAD4A)
                    }
                    
                })
                
            }) {
                
            }
        }
        else if(sender == btnEditCover){
            CameraGalleryPickerBlock.shared.pickerImage(pickedListner: {[weak self](image, filename) in
                // self?.imgViewCover?.image = image
                 guard let data = image.jpegData(compressionQuality: 0.5) else {return}
                self?.imageData = data
                self?.imgViewCover?.contentMode   = .scaleAspectFill
                self?.imgViewCover?.clipsToBounds = true
                self?.uploadCoverPic(completionBlock: { (success) in
                    //print("success = \(success!)")
                    if(success)!{
                        // print("uploaded image = \(image)")
                        self?.imgViewCover?.image    = image
                        self?.btnEditCover?.isHidden = true
                        self?.lblPending?.text       = "Pending"
                        self?.lblPending?.isHidden   = false
                        self?.lblPending?.backgroundColor = UIColor(rgb: 0x4EAD4A)

                    }
                })
                
            }) {
                
            }
        }
        else if(sender == btnEditUsername)
        {
            if(self.strEditImageName == "Cancel editing"){
                self.txtFldUsername?.text = ""
                
            }else if(self.strEditImageName == "edit pencil2" || self.strEditImageName == "Re edit" )
            {
                self.txtFldUsername?.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
                self.viewBgUsername?.backgroundColor = UIColor(red: 243/255 ,green: 243/255 , blue: 243/255,alpha: 1.0)
                self.txtFldUsername?.becomeFirstResponder()
                self.txtFldContact?.resignFirstResponder()
                
            }else if(self.strEditImageName == "Right Selected")
            {
                self.usernameUpdate()
            }
        }
        else if (sender == btnEditContact){
            if(/ifNumberVerified){
                WebserviceManager.showAlert(message: "Do you want to send a verification code on \(/self.txtFldContact?.text) ?", title: "", firstBtnText: "Confirm", secondBtnText: "Cancel", firstBtnBlock: {
                    self.sendOTP()
                }, secondBtnBlock: {})
                
            }else
            {
                if(/ifContactEditing){
                    self.txtFldContact?.text = ""
                    ifContactEditing = false
                }else{
                    self.txtFldContact?.becomeFirstResponder()
                    self.txtFldUsername?.resignFirstResponder()
                }
            }
        }
        else if(sender == btnEditFB)
        {
            if(self.strEditImageName == "Cancel editing"){
                self.txtFieldFB?.text = ""
                
            }else if(self.strEditImageName == "edit pencil2" || self.strEditImageName == "Re edit" )
            {
                self.txtFieldFB?.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
                self.viewBgFB?.backgroundColor = UIColor(red: 243/255 ,green: 243/255 , blue: 243/255,alpha: 1.0)
                self.txtFieldFB?.becomeFirstResponder()
                self.txtFldContact?.resignFirstResponder()
                self.txtFldUsername?.resignFirstResponder()
            }
            else if(self.strEditImageName == "Right Selected")
            {
                self.UpdateFBURL()
            }
        }
        else if(sender == btnEditTwitter)
        {
            if(self.strEditImageName == "Cancel editing"){
                self.txtFieldTwitter?.text = ""
                
            }else if(self.strEditImageName == "edit pencil2" || self.strEditImageName == "Re edit" )
            {
                self.txtFieldTwitter?.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
                self.viewBgTwitter?.backgroundColor = UIColor(red: 243/255 ,green: 243/255 , blue: 243/255,alpha: 1.0)
                self.txtFieldTwitter?.becomeFirstResponder()
                self.txtFldContact?.resignFirstResponder()
                self.txtFldUsername?.resignFirstResponder()
                self.txtFieldFB?.resignFirstResponder()
                
            }else if(self.strEditImageName == "Right Selected"){
                self.UpdateTwitterURL()
            }
        }
        else if(sender == btnEditLinkedIn)
        {
            if(self.strEditImageName == "Cancel editing"){
                self.txtFieldLinkedIn?.text = ""
                
            }else if(self.strEditImageName == "edit pencil2" || self.strEditImageName == "Re edit" )
            {
                self.txtFieldLinkedIn?.textColor = UIColor(red: 84/255 , green: 84/255 , blue: 84/255,alpha: 1.0)
                self.viewBgUsername?.backgroundColor = UIColor(red: 243/255 ,green: 243/255 , blue: 243/255,alpha: 1.0)
                self.txtFieldFB?.becomeFirstResponder()
                self.txtFldContact?.resignFirstResponder()
                self.txtFldUsername?.resignFirstResponder()
                self.txtFieldFB?.resignFirstResponder()
                self.txtFieldTwitter?.resignFirstResponder()
            }
            else if(self.strEditImageName == "Right Selected"){
                self.UpdateLinkedInURL()
            }
        }
    }
    // MARK: - UpdateFBURL
    func UpdateFBURL(){
        if(ifTxtUsernameUp == true){
            self.activeTextField?.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.activeTextField!, up: false)
        }
        self.navigationController?.navigationBar.isHidden = false
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        var urlString = String()
        urlString += APIConstants.basePath + "user/UpdateSocialMedia"
        
        let parameters: Parameters = [
            "SchoolCode": "\(/self.strSchoolCodeSender)",
            "key"       : "\(urlString.keyPath())",
            "UserID"    : "\(/self.intUserID)",
            "facebook"  : "\(/self.txtFieldFB?.text)"
        ]
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        //print("Number update result \n \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            
                            self.stContactNo                           = self.txtFldContact?.text
                            self.viewVerificationOTP?.isHidden         = true
                            self.btnRightOTP?.isUserInteractionEnabled = false
                            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
                            
                            self.txtFieldFB?.textColor = UIColor.white
                            self.btnEditFB?.setBackgroundImage(R.image.reEdit(), for: .normal)
                            self.viewBgFB?.backgroundColor = UIColor(red: 75/255 ,green: 217/255 , blue: 100/255,alpha: 1.0)
                        }
                    }
                    if(error != nil){
                        Utility.shared.removeLoader()

                        self.viewBluredBg?.isHidden = true
                        if(errorcode == -1009){
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }else{
                            WebserviceManager.showAlert(message: "Failed to update the Facebook URL. Please try again", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                    }
            }
        })
    }
    // MARK: - UpdateTwitterURL
    func UpdateTwitterURL(){
        if(ifTxtUsernameUp == true){
            self.activeTextField?.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.activeTextField!, up: false)
        }
        self.navigationController?.navigationBar.isHidden = false
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        var urlString = String()
        urlString += APIConstants.basePath + "user/UpdateSocialMedia"
        
        let parameters: Parameters = [
            "SchoolCode": "\(/self.strSchoolCodeSender)",
            "key"       : "\(urlString.keyPath())",
            "UserID"    : "\(/self.intUserID)",
            "Twitter"  : "\(/self.txtFieldTwitter?.text)"
        ]
        
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        //print("Number update result \n \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            
                            self.stContactNo                           = self.txtFldContact?.text
                            self.viewVerificationOTP?.isHidden         = true
                            self.btnRightOTP?.isUserInteractionEnabled = false
                            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
                            
                            self.txtFieldTwitter?.textColor = UIColor.white
                            self.btnEditTwitter?.setBackgroundImage(R.image.reEdit(), for: .normal)
                            self.viewBgTwitter?.backgroundColor = UIColor(red: 75/255 ,green: 217/255 , blue: 100/255,alpha: 1.0)
                        }
                    }
                    if(error != nil){
                        Utility.shared.removeLoader()

                        self.viewBluredBg?.isHidden = true
                        if(errorcode == -1009){
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }else{
                            WebserviceManager.showAlert(message: "Failed to update the Twitter URL. Please try again", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                    }
            }
        })
    }
    // MARK: - UpdateLinkedInURL
    func UpdateLinkedInURL(){
        if(ifTxtUsernameUp == true){
            self.activeTextField?.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.activeTextField!, up: false)
        }
        self.navigationController?.navigationBar.isHidden = false
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        var urlString = String()
        urlString += APIConstants.basePath + "user/UpdateSocialMedia"
        
        let parameters: Parameters = [
            "SchoolCode": "\(/self.strSchoolCodeSender)",
            "key"       : "\(urlString.keyPath())",
            "UserID"    : "\(/self.intUserID)",
            "LinkedIn"  : "\(/self.txtFieldLinkedIn?.text)"
        ]
        
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        //print("Number update result \n \(result)")
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            
                            self.stContactNo                           = self.txtFldContact?.text
                            self.viewVerificationOTP?.isHidden         = true
                            self.btnRightOTP?.isUserInteractionEnabled = false
                            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
                            
                            self.txtFieldLinkedIn?.textColor = UIColor.white
                            self.btnEditLinkedIn?.setBackgroundImage(R.image.reEdit(), for: .normal)
                            self.viewBgLinkedIn?.backgroundColor = UIColor(red: 75/255 ,green: 217/255 , blue: 100/255,alpha: 1.0)
                        }
                    }
                    if(error != nil){
                        Utility.shared.removeLoader()

                        self.viewBluredBg?.isHidden = true
                        if(errorcode == -1009){
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }else{
                            WebserviceManager.showAlert(message: "Failed to update the LinkedIn URL. Please try again", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                    }
            }
        })
    }
    func uploadProfilePic(completionBlock : @escaping (_ success: Bool?)->()){
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        if let imgData = self.imageData {
            let urlProfilePicPost = "http://ecare.franciscansolutions.com/api/Upload/ProfileImg?SchCode=\(/self.strSchoolCodeSender)&UserType=\(/intUserType)&UserID=\(/intUserID)&key=\(APP_CONSTANTS.KEY)"
            
            let parameters = [
                "SchoolCode": /self.strSchoolCodeSender,
                "UserType": /intUserType,
                "UserID": /self.intUserID,
                "key": "\(urlProfilePicPost.keyPath())"
                
                ] as [String : Any]
            
            // print("parameters = \(parameters)")
            print("urlProfilePicPost \n \(urlProfilePicPost)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                // print("Multipartformdata")
                for (key, value) in parameters {
                    if let valueString = value as? String{
                        multipartFormData.append((valueString).data(using: .utf8)!, withName: key)
                    }else{
                        if let valueInt = value as? Int{
                            multipartFormData.append(("\(valueInt)").data(using: .utf8)!, withName: key)
                        }
                    }
                }
                // print("self.imageData = \(imgData)")
                multipartFormData.append(imgData, withName: "image" ,fileName: "\(/self.intUserID).jpeg" , mimeType: "image/jpeg")
            }, to:urlProfilePicPost)
            { (result) in
                print("result \n \(result)")
                switch result{
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseJSON { response in
                        if  let statusCode = response.response?.statusCode{
                            
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            // print("jSON response \n \(response)")
                            print("statusCode \(statusCode)")
                            
                            if(statusCode == 201){
                                completionBlock(true)
                            }else{
                                completionBlock(false)
                            }
                        }else{
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden      =  true
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                            completionBlock(false)
                        }
                    }
                    
                case .failure(let encodingError):
                    Utility.shared.removeLoader()

                    self.viewBluredBg?.isHidden = true
                    completionBlock(false)
                    print(encodingError)
                }
            }
        }
    }
    
    func uploadCoverPic(completionBlock : @escaping (_ success: Bool?)->()){
        
        Utility.shared.loader()

        self.viewBluredBg?.isHidden = false
        
        if let imgData = self.imageData {
            
            let parameters = [
                "SchoolCode": /self.strSchoolCodeSender,
                "UserType": /intUserType
                ] as [String : Any]
            
            // print("parameters = \(parameters)")
            let urlCoverPicPost = "http://ecare.franciscansolutions.com/api/Upload/CoverImg?SchCode=\(/self.strSchoolCodeSender)&UserType=\(/intUserType)&key=\(APP_CONSTANTS.KEY)"
            print("urlCoverPicPost \n \(urlCoverPicPost)")
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                // print("Multipartformdata")
                for (key, value) in parameters {
                    if let valueString = value as? String{
                        multipartFormData.append((valueString).data(using: .utf8)!, withName: key)
                    }else{
                        if let valueInt = value as? Int{
                            multipartFormData.append(("\(valueInt)").data(using: .utf8)!, withName: key)
                        }
                    }
                }
                // print("self.imageData = \(imgData)")
                multipartFormData.append(imgData, withName: "image" ,fileName: "\(/self.intUserID).jpeg" , mimeType: "image/jpeg")
            }, to:urlCoverPicPost)
            { (result) in
                print("result \n \(result)")
                switch result{
                    
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseJSON { response in
                        if  let statusCode = response.response?.statusCode{
                            
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden = true
                            // print("jSON response \n \(response)")
                            print("statusCode \(statusCode)")
                            
                            if(statusCode == 201){
                                completionBlock(true)
                            }else{
                                completionBlock(false)
                            }
                        }else{
                            Utility.shared.removeLoader()

                            self.viewBluredBg?.isHidden      =  true
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                            completionBlock(false)
                        }
                    }
                    
                case .failure(let encodingError):
                    Utility.shared.removeLoader()

                    self.viewBluredBg?.isHidden = true
                    completionBlock(false)
                    print(encodingError)
                }
            }
        }
    }
    
    // MARK: - cancelOTPview
    func cancelOTPview()
    {
        if(ifTxtUsernameUp == true){
            self.activeTextField?.resignFirstResponder()
            self.scrollView?.isScrollEnabled = true
            self.btnBackNavigation?.isEnabled = true
            self.animateTxtFld(txtFld: self.activeTextField!, up: false)
        }
        self.btnEditContact?.setBackgroundImage(R.image.rightSelected(), for: .normal)
        viewBluredBg?.isHidden = true
        viewVerificationOTP?.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
    }
    // MARK: - resendCodePressed
    @IBAction func resendCodePressed(_ sender: UIButton) {
        
        intResendCount? += 1
        // print("intResendCount = \(intResendCount)")
        if(intResendCount == 3){
            sender.isUserInteractionEnabled = false
            sender.setTitleColor(UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha:1), for: .normal)
        }else{
            self.btnRightOTP?.isUserInteractionEnabled = false
            self.btnRightOTP?.setImage(R.image.greyRight(), for: .normal)
            self.txtFldOTP1?.text = ""
            self.txtFldOTP2?.text = ""
            self.txtFldOTP3?.text = ""
            self.txtFldOTP4?.text = ""
            
            self.txtFldOTP1?.textColorYellow(text: /self.txtFldOTP1?.text)
            self.txtFldOTP2?.textColorYellow(text: /self.txtFldOTP2?.text)
            self.txtFldOTP3?.textColorYellow(text: /self.txtFldOTP3?.text)
            self.txtFldOTP4?.textColorYellow(text: /self.txtFldOTP4?.text)
            
            self.txtFldOTP1?.borderColorClean()
            self.txtFldOTP2?.borderColorClean()
            self.txtFldOTP3?.borderColorClean()
            self.txtFldOTP4?.borderColorClean()
            self.sendOTP()
        }
    }
    // MARK: - crossPressed
    @IBAction func crossPressed(_ sender: UIButton) {
        self.cancelOTPview()
    }
    // MARK: - rightPressed
    @IBAction func rightPressed(_ sender: UIButton)
    {
        print("strOTPGenerated = \(/strOTPGenerated)")
        ifNumberVerified = false
        let strEnteredOTP = "\(/txtFldOTP1?.text)\(/txtFldOTP2?.text)\(/txtFldOTP3?.text)\(/txtFldOTP4?.text)"
        print("strEnteredOTP = \(strEnteredOTP)")
        if (strEnteredOTP == strOTPGenerated){
            self.contactUpdate()
        }else
        {
            sender.isUserInteractionEnabled = false
            sender.setImage(R.image.greyRight(), for: .normal)
            self.btnResendOTP?.isUserInteractionEnabled = true
            self.btnResendOTP?.setTitleColor(UIColor(red: 255/255, green: 187/255, blue: 0/255, alpha:1), for: .normal)
            
            self.txtFldOTP1?.txtShakeAnimation()
            self.txtFldOTP2?.txtShakeAnimation()
            self.txtFldOTP3?.txtShakeAnimation()
            self.txtFldOTP4?.txtShakeAnimation()
            
            self.txtFldOTP1?.borderWidthColor()
            self.txtFldOTP2?.borderWidthColor()
            self.txtFldOTP3?.borderWidthColor()
            self.txtFldOTP4?.borderWidthColor()
            
            self.txtFldOTP1?.textColorError(text: /self.txtFldOTP1?.text)
            self.txtFldOTP2?.textColorError(text: /self.txtFldOTP2?.text)
            self.txtFldOTP3?.textColorError(text: /self.txtFldOTP3?.text)
            self.txtFldOTP4?.textColorError(text: /self.txtFldOTP4?.text)
        }
    }
    @IBAction func termsConditnsPressed(_ sender: UIButton) {}
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
extension UIImage {
    
    func crop(to:CGSize) -> UIImage {
        
        guard let cgimage         = self.cgImage else { return self }
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        let contextSize: CGSize   = contextImage.size
        
        //Set to square
        var posX: CGFloat       = 0.0
        var posY: CGFloat       = 0.0
        let cropAspect: CGFloat = to.width / to.height
        var cropWidth: CGFloat  = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth  = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY       = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth  = contextSize.height * cropAspect
            posX       = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth  = contextSize.height * cropAspect
                posX       = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth  = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY       = (contextSize.height - cropHeight) / 2
            }
        }
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        UIGraphicsBeginImageContextWithOptions(to, true, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resized!
    }
}
