import UIKit
import EZSwiftExtensions
import IQKeyboardManager
import Fabric
import Crashlytics

class MainProfileViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var scrollView:           UIScrollView?
    @IBOutlet weak var btnEdit:              UIButton?
    @IBOutlet weak var imgCover:             UIImageView?
    @IBOutlet weak var imgUser:              UIImageView?
    @IBOutlet weak var lblName:              UILabel?
    @IBOutlet weak var lblClass:             UILabel?
    @IBOutlet weak var lblUsername:          UILabel?
    @IBOutlet weak var lblAdmissionNo:       UILabel?
    @IBOutlet weak var lblFatherName:        UILabel?
    @IBOutlet weak var lblFatherNameTitle:   UILabel?
    
    
    @IBOutlet weak var lblMotherName:        UILabel?
    @IBOutlet weak var lblAddress:           UILabel?
    @IBOutlet weak var lblDOA:               UILabel?
    @IBOutlet weak var lblDOB:               UILabel?
    @IBOutlet weak var lblContactNo:         UILabel?
    @IBOutlet weak var lblMainContact:       UILabel?
    @IBOutlet weak var schoolTableView:      UITableView?
    @IBOutlet weak var tblViewSchoolsHeight: NSLayoutConstraint?
    @IBOutlet weak var tblViewUsersHeight:   NSLayoutConstraint?
    @IBOutlet weak var viewAdmissionDOA:     UIView?
    @IBOutlet weak var lblEmailId:           UILabel?
    @IBOutlet weak var viewEmailId:          UIView?
    @IBOutlet weak var viewFatherName:       UIView?
    @IBOutlet weak var ViewMotherName:       UIView?
    @IBOutlet weak var lblDOJ:               UILabel?
    @IBOutlet weak var viewDOJ:              UIView?
    @IBOutlet weak var viewPermanentAddress: UIView?
    @IBOutlet weak var lblPermanentAdress:   UILabel?
    
    @IBOutlet weak var lblGender:            UILabel?
    @IBOutlet weak var viewGender:           UIView?
    @IBOutlet weak var viewMaritalStatus:    UIView?
    @IBOutlet weak var lblMaritalStatus:     UILabel?
    
    @IBOutlet weak var viewDOA:              UIView?
    @IBOutlet weak var viewDOB:              UIView?

    @IBOutlet weak var lblDateAnniversary:   UILabel?
    
    @IBOutlet weak var viewFB:               UIView?
    @IBOutlet weak var viewTwitter:          UIView?
    @IBOutlet weak var viewLinkedIn:         UIView?
    @IBOutlet weak var viewGray:             UIView?
    @IBOutlet weak var viewGrayBelowMyQuestion: UIView?
    
    @IBOutlet weak var viewReligion:         UIView?
    @IBOutlet weak var lblReligion:          UILabel?

    @IBOutlet weak var viewNationality:      UIView?
    @IBOutlet weak var lblNationality:       UILabel?

    
    @IBOutlet weak var lblFB:                UILabel?
    @IBOutlet weak var lblTwitter:           UILabel?
    @IBOutlet weak var lblLinkedIn:          UILabel?
    
    var stringName          : String?
    var stringClass         : String?
    var stringUsername      : String?
    var stringContactNo     : String?
    var stringFBUrl         : String?
    var stringTwitterUrl    : String?
    var stringLinkedInUrl   : String?
    
    var stringCoverPicUrl   : URL?
    var stringProfilePicUrl : URL?
    var removeUser :  UserTable?
    var intCoverImg: Int?
    var intProfileImg: Int?
    
    
    //MARK::- VARIABLES
    var usersArr       : [UserTable]?
    var isFirstTime    : Bool = true
    var intCountryCode : Int?
    var isLogoutAll : Bool = false
    
    //MARK: - LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isNavBarHidden = false
        self.title   = "Profile"
        Fabric.with([Crashlytics.self])
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        self.imgUser?.layer.masksToBounds = true
        self.imgUser?.backgroundColor     = .clear
        self.imgUser?.layer.cornerRadius  = /self.imgUser?.frame.height / 2
        self.imgUser?.layer.borderWidth   = 5
        self.imgUser?.layer.borderColor   = UIColor.white.cgColor
        
        if let check = self.getCurrentSchool()?.countryCode {
            intCountryCode =  Int(check)
        }
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        // relaod the table when add a new users
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: NOTI.ADD_USER), object: nil, queue: nil) { (_) in
            self.getAddUsers()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = false
        tblViewUsersHeight?.constant   = 0
        btnEdit?.isHidden = true
        getProfileData()
        getAddUsers()
        ez.runThisInMainThread {
            self.showTabBar()
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                parent.hideBar(boolHide: true)
            }
        }
        updateUI()
    }
    
    //MARK:-  getAddUsers()
    func getAddUsers() {
        
        let data =  DBManager.shared.getUsersTableAssociateWithSchools()
        print("No of Authenticated Users associated with School:" , data.count)
        
        usersArr = data
        
        let parentUsers = usersArr?.filter({ (user) -> Bool in
            return user.userType == 2
        })
        
        let parentUserHeight = CGFloat((/parentUsers?.count) * 100)
        let otherUserHeight = CGFloat((/usersArr?.count - /parentUsers?.count) * 95)
        tblViewUsersHeight?.constant = parentUserHeight + otherUserHeight
        
        tableView?.isScrollEnabled = false
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = usersArr
            tableView?.reloadData()
        }
    }
    func getProfileData() {
        APIManager.shared.request(with: HomeEndpoint.myProfile(UserID: userID, UserType: userType)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handle(response : response)
            })
        }
    }
    
    
    func handle(response : Any?){
        
        if let modal = response as? MainProfileModel , let profile = modal.myProfile {
            // self.modal = modal
            if let url = profile.coverImg?.getImageUrl() {
                print("URl= \(url)")
                //                imgCover?.image = nil
                imgCover?.clearImageFromCache(validUrl: url)
                
                imgCover?.loadURL(imageUrl: url, placeholder: nil, placeholderImage:  R.image.coverImage())
            }
            btnEdit?.isHidden = false
            if let url = profile.photo?.getImageUrl() {
                imgUser?.loadURL(imageUrl: url, placeholder: nil, placeholderImage:  R.image.noProfile_Big())
            }
            lblName?.text        = /profile.name
            
            if let className     = profile.className {
                lblClass?.text   =  "Class - \(className)"
            }
            if userType?.toInt() == 3 {
                lblClass?.text   =  profile.designation
                //lblClass?.isHidden = true
            }
            if  userType?.toInt() == 2 {
                lblClass?.text   =  "Parent"
            }
            lblUsername?.text    = "@\(/profile.Username)".uppercased()
            lblAdmissionNo?.text = profile.admissionNo
            lblDOA?.text         = profile.admissionDate?.dateChangeFormat()
            lblDOB?.text         = profile.dOB?.dateChangeFormat()
            lblAddress?.text     = profile.address
            if profile.stContact?.trimmed().count != 0 {
                lblContactNo?.text   = /intCountryCode?.toString + "-" + /profile.stContact
            }
            else {
                lblContactNo?.text = ""
            }
            lblEmailId?.text      = profile.contactEmailID
            lblReligion?.text     = profile.religion    == "" ? "N/A" :  profile.religion
           // lblNationality?.text  = profile.nationality == "" ? "N/A" :  profile.nationality
            if let Nationality = profile.nationality {
                lblNationality?.text   = Nationality
            }
            else {
                lblNationality?.text = "N/A"
            }

            if userType?.toInt() == 3 {
                lblContactNo?.text     = /intCountryCode?.toString + "-" + /profile.mobile
                lblEmailId?.text       = profile.emailID
                lblMainContact?.text   = /profile.mobile + "\n\(/profile.fatherHusbandMob)" + "\n\(/profile.emergencyContactNo)"
                
                lblGender?.text          = /profile.gender
                lblMaritalStatus?.text   = /profile.maritalStatus
                viewDOA?.isHidden = /profile.maritalStatus == "Unmarried"
                lblPermanentAdress?.text = /profile.p_Address
                lblDateAnniversary?.text = /profile.dOAnniversary?.dateChangeFormat()
                //father Details
                lblFatherName?.text     = profile.fatherHusbandName
                lblDOJ?.text            = profile.dOJ?.dateChangeFormat()
                if stringContactNo?.trimmed().count != 0 {
                   stringContactNo         =  /intCountryCode?.toString + "-" + /profile.mobile
                }
                else {
                   stringContactNo         =  nil
                }
              
            }
            else{
                lblMainContact?.text = profile.stContact! + "\n\(/profile.frContact)\n\(/profile.mrContact)"
                if userType?.toInt() == 2 {
                    lblMainContact?.text = profile.contactMob
                }
                //father Details
                lblFatherName?.text = profile.fatherName
                lblMotherName?.text = profile.motherName
                lblFB?.text         = profile.socialMedia?.facebook == "" ? "N/A" :  profile.socialMedia?.facebook
                lblTwitter?.text    = profile.socialMedia?.twitter  == "" ? "N/A" :  profile.socialMedia?.twitter
                lblLinkedIn?.text   = profile.socialMedia?.linkedIn == "" ? "N/A" :  profile.socialMedia?.linkedIn
                
                stringFBUrl         = profile.socialMedia?.facebook
                stringTwitterUrl    = profile.socialMedia?.twitter
                stringLinkedInUrl   = profile.socialMedia?.linkedIn
                if stringContactNo?.trimmed().count != 0 {
                    stringContactNo         =  /intCountryCode?.toString + "-" + /profile.stContact
                }
                else {
                    stringContactNo         =  nil
                }
            }
            
            intCoverImg         = profile.userImgReq?.coverImg
            intProfileImg       = profile.userImgReq?.profileImg
            
            stringName          = /profile.name
            stringClass         = lblClass?.text
            stringUsername      = "@\(/profile.Username)".uppercased()
            if profile.stContact?.trimmed().count != 0 {
                stringContactNo     =  /intCountryCode?.toString + "-" + /profile.stContact
            }
            else {
                stringContactNo = nil
            }
            
            if let url = profile.coverImg?.getImageUrl() {
                stringCoverPicUrl   = url
            }
            else{
                stringCoverPicUrl = URL(string: "")
            }
            
            if let url = profile.photo?.getImageUrl() {
                stringProfilePicUrl = url
            }
            else{
                stringProfilePicUrl = URL(string: "")
            }
        }
    }
    //Edit Profile Button
    @IBAction func editPressed(_ sender: UIBarButtonItem) {
        // self.performSegue(withIdentifier: "editSegue", sender: self)
        guard let editProfileVc = storyboard?.instantiateViewController(withIdentifier: "editProfileViewController") as? EditProfileViewController else {return}
        
        editProfileVc.stName          = stringName
        editProfileVc.stClass         = stringClass
        editProfileVc.stUsername      = stringUsername
        editProfileVc.stContactNo     = stringContactNo
        editProfileVc.stCoverPicUrl   = stringCoverPicUrl
        editProfileVc.stProfilePicUrl = stringProfilePicUrl
        editProfileVc.strFBURL        = stringFBUrl
        editProfileVc.strTwitterURL   = stringTwitterUrl
        editProfileVc.strLinkedInURL  = stringLinkedInUrl
        editProfileVc.intCoverImg     = intCoverImg
        editProfileVc.intProfileImg   = intProfileImg
        
        self.pushVC(editProfileVc)
    }
    //Add Account
    @IBAction func btnAddAccountAct(_ sender: UIButton) {
        guard let adduserVC = storyboard?.instantiateViewController(withIdentifier: "AddUserViewController") as? AddUserViewController else {return}
        if usersArr?.count != 0 {
            adduserVC.schoolData = self.usersArr?[0].schCodes
            self.pushVC(adduserVC)
        }
    }
    
    @IBAction func myQuestionAct(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let myQuestionsVc = storyboard.instantiateViewController(withIdentifier: "MyQuestionsStoryboardID") as? MyQuestionsViewController else {return}
        self.pushVC(myQuestionsVc)
        
    }
    @IBAction func btnLogout(_ sender: UIButton) {
        isLogoutAll = true
        logoutPressed()
    }
    
    @IBAction func btnInformationAct(_ sender: UIButton) {
        guard let vc = R.storyboard.main.leavePolicyViewController() else {
            return
        }
        vc.forLeavePolicy = false
        self.presentVC(vc)
    }
    
    // MARK: - Edit Profile
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "editSegue"){
        }
    }
}

extension MainProfileViewController {
    
    func updateUI() {
        viewFB?.isHidden                  = userType?.toInt() == 3  || userType?.toInt() == 1
        viewTwitter?.isHidden             = userType?.toInt() == 3  || userType?.toInt() == 1
        viewLinkedIn?.isHidden            = userType?.toInt() == 3  || userType?.toInt() == 1
        viewEmailId?.isHidden             = userType?.toInt() == 2  || userType?.toInt() == 1
        viewGray?.isHidden                = userType?.toInt() == 3  || userType?.toInt() == 1
        viewAdmissionDOA?.isHidden        = userType?.toInt() == 3  || userType?.toInt() == 2
        ViewMotherName?.isHidden          = userType?.toInt() == 3  || userType?.toInt() == 2
        viewDOJ?.isHidden                 = userType?.toInt() == 2  || userType?.toInt() == 1
        viewDOA?.isHidden                 = userType?.toInt() == 2  || userType?.toInt() == 1
        viewGender?.isHidden              = userType?.toInt() == 2  || userType?.toInt() == 1
        viewMaritalStatus?.isHidden       = userType?.toInt() == 2  || userType?.toInt() == 1
        viewPermanentAddress?.isHidden    = userType?.toInt() == 2  || userType?.toInt() == 1
        viewFatherName?.isHidden          = userType?.toInt() == 2
        viewDOB?.isHidden                 = userType?.toInt() == 2
        viewGrayBelowMyQuestion?.isHidden = userType?.toInt() == 3  || userType?.toInt() == 1
        
        viewReligion?.isHidden            = userType?.toInt() == 2  || userType?.toInt() == 1
        viewNationality?.isHidden         = userType?.toInt() == 2  || userType?.toInt() == 1
        
        if userType?.toInt() == 3 {
            lblFatherNameTitle?.text   = "Father's/Spouse's Name"
        }
    }
    
}
// Configure TableView of Leave Data ( ON SEGEMENT CONTROL = 1 )
extension MainProfileViewController {
    
    //MARK: - ConfigureTableView
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: usersArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.AddAccountCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? AddAccountCell)?.index  = indexpath?.row
            (cell as? AddAccountCell)?.modal = item
            (cell as? AddAccountCell)?.delegate = self
            (cell as? AddAccountCell)?.isExclusiveTouch = true
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ selectIndex: Int) {
        if selectIndex == 0 {
            return
        }
        
        if selectIndex < /usersArr?.count && /usersArr?.count >= 2  {
            self.view.isUserInteractionEnabled = false
            if let user = usersArr?[selectIndex] , let currentUser = usersArr?[0] {
                refreshHomeTab(user , currentUser)
            }
        }
    }
    
    func refreshHomeTab(_ user : UserTable , _ currentUser : UserTable){
        
        APIManager.shared.request(with: HomeEndpoint.validateForChangeUser(schoolCode: user.schCode, username: user.username, password: user.password), isLoader: false) {[weak self](response) in
            self?.handleResponse(isErrorShown: true, response: response, responseBack: { (data) in
                self?.handleValidateResponse(data,user,currentUser)
            })
        }
    }
    
    func handleValidateResponse(_ response : Any? , _ user : UserTable , _ currentUser : UserTable ){
        if let response = response as? [String : Any] {
            
            DBManager.shared.saveUserData(result: response , senderCode: user.schCode, username: user.username , password: user.password , forVerification:  true)
            
            DBManager.shared.updateCurrentUserIntoDatabase(schoolCode:currentUser.schCode , username: currentUser.username, isActive:0, confirm:{(_) in
            })
            DBManager.shared.updateCurrentUserIntoDatabase(schoolCode:user.schCode , username: user.username, isActive: 1, confirm:{(_) in
            })
            getAddUsers()
            // Defaults.shared.saveUserDetails(dictResult: response, schoolCode: /user.schCode)
            UIView.animate(withDuration: 0.2) {
                self.openMainTab()
            }
        }
    }
    
    func openMainTab() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.checkUserExists()
        }
    }
    
    func removeUserFromServer(_ currentServer : UserTable){
        APIManager.shared.request(with: HomeEndpoint.logout(schoolCode: currentServer.schCode, userID: currentServer.userID, userType: currentServer.userType)) { (response) in
            print("User Logout succesfully from Server")
        }
    }
}


extension MainProfileViewController : SelectAccountDelegate {
    
    func selectAccount(_ selectIndex: Int) {
        if selectIndex == 0 {
            return
        }
        if selectIndex < /usersArr?.count , let user = usersArr?[selectIndex] ,let name = user.name {
            removeUser = user
            isLogoutAll = false
            let msg = AlertMsg.removeUser.get + name + AlertMsg.fromAccountList.get
            if APIManager.shared.isConnectedToInternet() {
                showAlert(title: AlertConstants.remove.get ,message: msg , buttons: [AlertConstants.Ok.get , AlertConstants.Cancel.get])
            }
            else {
                Utility.shared.internetConnectionAlert()
            }
        }
    }
}

extension MainProfileViewController {
    
    //logout for all users
    func logoutPressed(){
        if APIManager.shared.isConnectedToInternet() {
            showAlert(title: AlertConstants.logout.get ,message: AlertMsg.LogoutSure.get , buttons: [AlertConstants.Ok.get , AlertConstants.Cancel.get])
        }
        else {
            Utility.shared.internetConnectionAlert()
        }
    }
    
    
    func showAlert(title : String? , message : String? , buttons : [String]) {
        AlertsClass.shared.showAlertController(withTitle: /title, message: /message, buttonTitles: buttons) { (value) in
            let type = value as AlertTag
            switch type {
            case .yes:
                self.isLogoutAll ? self.logout() : self.removeSelectUser()
            default : return
            }
        }
    }
    
    func removeSelectUser(){
        if let removeUser = removeUser {
            self.removeUserFromServer(removeUser)
            DBManager.shared.updateCurrentUserIntoDatabase(schoolCode: /removeUser.schCode , username: /removeUser.username, isActive: Int(removeUser.isActive) , forDelete:  true, confirm: {(_) in
            })
            UIView.animate(withDuration: 0.5) {
                self.getAddUsers()
            }
        }
    }
    
    func logout(){
        
        DBManager.shared.DeleteAllData()
        guard let intialVc = storyboard?.instantiateViewController(withIdentifier: "ViewController") as? UINavigationController else {return}
        
        guard  let mainVc =
            storyboard?.instantiateViewController(withIdentifier: "SchoolCodeViewController") as? ViewController else {return}
        intialVc.viewControllers = [mainVc]
        
        UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: .transitionFlipFromLeft , animations: { () -> Void in
            UIApplication.shared.keyWindow?.rootViewController = intialVc
        }) { (completed) -> Void in
        }
    }
}
