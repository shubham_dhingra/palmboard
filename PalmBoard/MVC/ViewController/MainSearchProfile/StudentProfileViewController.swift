//  StudentProfileViewController.swift
//  e-Care Pro
//  Created by ShubhamMac on 01/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class StudentProfileViewController: BaseViewController {
    
    @IBOutlet weak var imgBg       : UIImageView?
    @IBOutlet weak var imgStu      : UIImageView?
    @IBOutlet weak var viewProfile : UIView?
    @IBOutlet weak var scrollView  : UIScrollView?
    //Self Details
    @IBOutlet weak var lblName        : UILabel?
    @IBOutlet weak var lblClass       : UILabel?
    @IBOutlet weak var lblMobile      : UILabel?
    @IBOutlet weak var lblUserName    : UILabel?
    @IBOutlet weak var lblAdmissionNo : UILabel?
    @IBOutlet weak var lblDateOfAdmission : UILabel?
    @IBOutlet weak var lblDOB         : UILabel?
    @IBOutlet weak var lblGender      : UILabel?
    @IBOutlet weak var lblNationality : UILabel?
    @IBOutlet weak var lblReligion  : UILabel?
    @IBOutlet weak var lblTransport : UILabel?
    @IBOutlet weak var lblAddress   : UILabel?
    //father Details
    @IBOutlet weak var lblFatherName      : UILabel?
    @IBOutlet weak var lblFatherDob       : UILabel?
    @IBOutlet weak var lblFProfesion      : UILabel?
    @IBOutlet weak var lblFDesignation    : UILabel?
    @IBOutlet weak var lblFResidentalAddr : UILabel?
    @IBOutlet weak var lblOfficeAddress   : UILabel?
    @IBOutlet weak var lblFEmailId        : UILabel?
    //  @IBOutlet weak var lblFContactNo      : UILabel?
    @IBOutlet weak var txtViewFContactNo        : UITextView?
    
    //mother Details
    @IBOutlet weak var lblMotherName     : UILabel?
    @IBOutlet weak var lblMotherDob      : UILabel?
    @IBOutlet weak var lblMProfesion     : UILabel?
    @IBOutlet weak var lblMResidentalAdd : UILabel?
    @IBOutlet weak var lblMContactNo     : UILabel?
    //Anniversary
    @IBOutlet weak var lblAnniversary   : UILabel?
    @IBOutlet weak var lblContactNumber : UILabel?
    @IBOutlet weak var lblContactPerson : UILabel?
    //School
    @IBOutlet weak var imgSchool      : UIImageView?
    @IBOutlet weak var lblSchName     : UILabel?
    @IBOutlet weak var lblSchLocation : UILabel?
    @IBOutlet weak var lblSession     : UILabel?
    @IBOutlet weak var imgViewNationality: UIImageView?
    @IBOutlet weak var lblNationalityStaff: UILabel?
    @IBOutlet weak var lblNationalityStaffValue: UILabel?
    @IBOutlet weak var imgViewReligion: UIImageView?
    @IBOutlet weak var lblReligionStaff: UILabel?
    @IBOutlet weak var lblReligionStaffValue: UILabel?
    @IBOutlet weak var admissionView: UIView?
    @IBOutlet weak var imgViewAdmissionNo: UIImageView?
    @IBOutlet weak var lblAdmission: UILabel?
    @IBOutlet weak var lblAdmissionValue: UILabel?
    @IBOutlet weak var lblResidentalAddress: UILabel?
    @IBOutlet weak var viewSpouse: UIView?
    @IBOutlet weak var fatherDetailsView: UIView!
    @IBOutlet weak var fatherNameView: UIView!
    @IBOutlet weak var fatherDob: UIView!
    @IBOutlet weak var fatherProfessionView: UIView!
    @IBOutlet weak var fatherDesView: UIView!
    @IBOutlet weak var transportView: UIView!
    @IBOutlet weak var motherDetailsView: UIView!
    @IBOutlet weak var officeAddView: UIView!
    @IBOutlet weak var motherNameView: UIView!
    @IBOutlet weak var motherDob: UIView!
    @IBOutlet weak var motherProfessionView: UIView!
    @IBOutlet weak var motherResAddView: UIView!
    @IBOutlet weak var motherContact : UIView!
    @IBOutlet weak var parentAnniDetailView : UIView!
    @IBOutlet weak var parentAnni: UIView!
    @IBOutlet weak var parentAnniContactView: UIView!
    @IBOutlet weak var parentAnniContactNoView: UIView!
    @IBOutlet weak var lblFatherSpouseName : UILabel?
    @IBOutlet weak var viewEmergencyNo : UIView?
    @IBOutlet weak var txtViewEmergencyContactNo : UITextView?
    
    
    var strUserType: String?
    var strUserID: String?
    var selectedIndex : Int = 0
    
    //variables
    var userArr : [UserTable]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        
        setUpView()
        self.imgStu?.layer.masksToBounds = true
        self.imgStu?.backgroundColor     = .clear
        self.imgStu?.layer.cornerRadius  = /self.imgStu?.frame.height / 2
        self.imgStu?.layer.borderWidth   = 5
        self.imgStu?.layer.borderColor   = UIColor.white.cgColor
        
        getProfile()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewProfile?.roundView()
        imgStu?.roundImageView()
        viewSpouse?.isHidden = selectedIndex == 0
    }
    private func setUpView() {
        admissionView?.isHidden = selectedIndex != 0
        lblAdmission?.text = selectedIndex != 0 ? "Date of joining" : "Admission No"
        lblNationalityStaff?.text  = selectedIndex != 0 ? "Maritial Status" : "Nationality"
        lblResidentalAddress?.text = selectedIndex != 0 ?
            "Permanent Address" : "Residental Address"
        lblReligionStaff?.text = selectedIndex != 0 ?
            "Date of Anniversary" : "Religion"
        //        imgViewNationality?.image =
        fatherDetailsView.isHidden = selectedIndex != 0
        fatherNameView.isHidden = selectedIndex != 0
        fatherDob.isHidden = selectedIndex != 0
        fatherProfessionView.isHidden = selectedIndex != 0
        fatherDesView.isHidden = selectedIndex != 0
        motherDetailsView.isHidden = selectedIndex != 0
        officeAddView.isHidden = selectedIndex != 0
        motherNameView.isHidden = selectedIndex != 0
        motherDob.isHidden = selectedIndex != 0
        motherProfessionView.isHidden = selectedIndex != 0
        motherResAddView.isHidden = selectedIndex != 0
        motherContact.isHidden = selectedIndex != 0
        parentAnniDetailView.isHidden = selectedIndex != 0
        parentAnni.isHidden = selectedIndex != 0
        parentAnniContactView.isHidden = selectedIndex != 0
        parentAnniContactNoView.isHidden = selectedIndex != 0
        transportView.isHidden = selectedIndex != 0
//        viewEmergencyNo?.isHidden = self.userType != "3"
    }
}
extension StudentProfileViewController {
    
    func getProfile() {
        APIManager.shared.request(with: HomeEndpoint.userDetails(UserID: strUserID, UserType: strUserType), isLoader: true) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handle(response : response)
            })
        }
    }
    func handle(response : Any?){
        if let response = response as? MainProfileModel {
            let userModal  =  selectedIndex != 0 ?  response.myProfile : response.details
            guard let modal = userModal else {return }
            if let url = modal.photo?.getImageUrl() {
                imgStu?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: nil)
            }
            if let url = modal.coverImg?.getImageUrl() {
                imgBg?.loadURL(imageUrl:url , placeholder: nil, placeholderImage: nil)
            }
            
            //Self Details
            lblName?.text            = modal.name
            lblClass?.text           = "Class- " + (/modal.className?.unwrapValue())
            lblClass?.isHidden = selectedIndex != 0
            
            lblMobile?.text          = modal.contactMobile?.unwrapValue()
            lblUserName?.text        = "@" + (/modal.Username).unwrapValue().uppercased()
            lblUserName?.text        = ""
            lblAdmissionNo?.text     = selectedIndex == 0 ? modal.admissionNo?.unwrapValue() : modal.dOJ?.dateChangeFormat()
            lblDateOfAdmission?.text =  modal.admissionDate?.dateChangeFormat()
            if selectedIndex == 1 {
                lblFatherSpouseName?.text = modal.fatherHusbandName?.unwrapValue()
            }
            lblDOB?.text             = modal.dOB?.dateChangeFormat()
            lblGender?.text          = modal.gender?.unwrapValue()
            lblNationalityStaffValue?.text     = selectedIndex == 0 ? modal.nationality?.unwrapValue() : modal.maritalStatus?.unwrapValue()
            lblReligion?.text        = selectedIndex == 0 ? modal.religion?.unwrapValue() : modal.dOAnniversary?.dateChangeFormat().unwrapValue()
            lblTransport?.text       = modal.transport?.unwrapValue()
            lblAddress?.text         = modal.address?.unwrapValue()
            
            //father Details
            lblFatherName?.text      = modal.fatherName
            lblFatherDob?.text       = modal.fatherDOB?.dateChangeFormat().unwrapValue()
            lblFProfesion?.text = modal.fatherProfession?.unwrapValue()
            
            lblFDesignation?.text    = modal.fatherDesignation?.unwrapValue()
            lblFResidentalAddr?.text = selectedIndex == 0 ? modal.fatherResidentialAddress?.unwrapValue() : modal.p_Address?.unwrapValue()
            
            lblOfficeAddress?.text   = modal.fatherOfficeAddress?.unwrapValue()
            
            lblFEmailId?.text        = selectedIndex == 0 ?modal.fatherEmail1?.unwrapValue() : modal.emailID?.unwrapValue()
            //            lblFContactNo?.text      =  selectedIndex == 0 ? modal.fatherMob1?.unwrapValue() : (/modal.mobile?.unwrapValue() + "\n" +  (/modal.emergencyContactNo?.unwrapValue() == "N/A" ? "" : /modal.emergencyContactNo?.unwrapValue()) + "\n" + (/modal.alternateMobile?.unwrapValue() == "N/A" ? "" : /modal.alternateMobile?.unwrapValue()))
            if selectedIndex == 0 {
                txtViewFContactNo?.text      =  modal.fatherMob1?.unwrapValue()
            }
            else {
                var contacts : String?
                contacts = modal.mobile?.unwrapValue()
                
                if modal.alternateMobile?.unwrapValue() != "N/A" {
                    contacts = /contacts + "\n" + /modal.alternateMobile
                }
               txtViewFContactNo?.text  = contacts
            }
            
            
            self.adjustUITextViewHeight(arg: txtViewFContactNo!)
            //mother Details
            lblMotherName?.text     = modal.motherName?.unwrapValue()
            lblMotherDob?.text      = modal.motherDOB?.dateChangeFormat()
            lblMProfesion?.text     = modal.motherProfession?.unwrapValue()
            lblMResidentalAdd?.text = modal.motherResidentialAddress?.unwrapValue()
            lblMContactNo?.text     = modal.motherMob1?.unwrapValue()
            //            let strCont1            = /modal.motherMob1
            //            let strCont2            = /modal.emergencyContactNo
            //            lblMContactNo?.text     = (strCont1 + strCont2).unwrapValue()
            
            //Anniversary
            lblAnniversary?.text   = modal.parentAnniversaryDate?.dateChangeFormat().unwrapValue()
            lblContactPerson?.text = modal.contactPerson?.unwrapValue()
            if selectedIndex == 0 {
                lblContactNumber?.text = modal.contactMobile?.unwrapValue()
            }
            else {
                lblContactNumber?.text = modal.designation?.unwrapValue()
            }
            
            txtViewEmergencyContactNo?.text = modal.emergencyContactNo?.unwrapValue()
           
        }
    }
    
}
extension StudentProfileViewController
{
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
}
