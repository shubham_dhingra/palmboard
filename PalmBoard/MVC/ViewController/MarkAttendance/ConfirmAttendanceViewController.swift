//
//  ConfirmAttendanceViewController.swift
//  PalmBoard
//
//  Created by Shubham on 05/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import Foundation
import EZSwiftExtensions

class ConfirmAttendanceViewController: BaseViewController {
    
    @IBOutlet weak var lblPresent : UILabel?
    @IBOutlet weak var lblAbsent : UILabel?
    @IBOutlet weak var lblLeave : UILabel?
    @IBOutlet weak var viewPresent : UIView?
    @IBOutlet weak var viewAbsent : UIView?
    @IBOutlet weak var viewLeave : UIView?
    @IBOutlet weak var imgStatus: UIImageView?
    @IBOutlet weak var lblMainHeading: UILabel?
    @IBOutlet weak var lblSubHeading: UILabel?
    @IBOutlet weak var btnConfirm: UIButton?
    @IBOutlet weak var btnCancel: UIButton?
    @IBOutlet weak var progressView : UIView?
    @IBOutlet weak var outerView : UIView?
    @IBOutlet weak var lblAlertHeading: UILabel?
    
    var confirmationDone : Bool = false
    var count: CGFloat = 0
    var progressRing: CircularProgressBar!
    var timer: Timer!
    var studentArr : [Student]?
    var classID = Int()
    var smsType = Int()
    var smsTemplate = String()
    var copyTemplate = String()
    var subID : Int?
    var present : Int = 0 , absent : Int = 0 , leave : Int = 0
    
    override func viewDidLoad() {
        progressView?.isHidden = true
        
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    @IBAction func btnConfirmAct(_ sender : UIButton){
        if !confirmationDone {
            savedAttendance()
        }
        else {
            sentSMS()
        }
    }
    
    
    @IBAction func btnCancelAct(_ sender : UIButton){
        if confirmationDone {
           NotificationCenter.default.post(name: .UPDATE_STUDENT_LIST, object: nil)
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    
    func onViewDidLoad(){
        copyTemplate = smsTemplate
        getStudentCountStatusWise()
        updateUI()
        
    }
    
    func sentSMS() {
        guard let school = self.getCurrentSchool() ,let user = self.getCurrentUser() else {return}
        
        let absenteeStudent = self.studentArr?.filter({return $0.status == 2})
        guard let absentees = absenteeStudent else {return}
        
        var studentsDict = [NSMutableDictionary]()
        for (_,student) in absentees.enumerated() {
            if student.status == 2 && student.contactMob?.trimmed().count != 0{
                let dict = NSMutableDictionary()
                dict["UserID"] = student.stID
                dict["UserType"] = "2"
                dict["Mobile"] = student.contactMob
                dict["SMS"] =  student.templateForAbsentee
                studentsDict.append(dict)
            }
        }
        
        if studentsDict.count == 0 {
            self.dismissVC(completion: nil)
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: AlertMsg.ContactMobRequiredDetails.get, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                switch tag {
                default:
                    NotificationCenter.default.post(name: .UPDATE_STUDENT_LIST, object: nil)
                    return
                }
            }
        }
        else {
        let param = Parameterss.sendBulkSms.map(values:  [school.schCode , APP_CONSTANTS.KEY , smsType.toString ,"\(user.userID)", "\(user.userType)" , "2" , studentsDict , "0" ,"\(school.sMS_Prvd_ID)"  ,"\(school.countryCode)", ""])
        
        APIManager.shared.request(with: HomeEndpoint.sendBulkSmS(body: param)) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                if let str = success as? String {
                    Messages.shared.show(alert: .success, message: /str, type: .success)
                }
               NotificationCenter.default.post(name: .UPDATE_STUDENT_LIST, object: nil)
               self?.dismissVC(completion: nil)
            })
        }
        }
    }
    
    func savedAttendance() {
        
        var studentsDict = [NSMutableDictionary]()
        guard let school = self.getCurrentSchool()?.schCode , let studentArr = self.studentArr else {return}
        
        for (_,value) in studentArr.enumerated() {
            if value.constant == 0 {
                let dict = NSMutableDictionary()
                dict["StID"] = value.stID
                dict["Status"] = value.status
                studentsDict.append(dict)
            }
        }
        let param = Parameterss.savedAttendanceModal.map(values: [school , APP_CONSTANTS.KEY , classID.toString ,studentsDict , subID?.toString])
        APIManager.shared.request(with: HomeEndpoint.savedAttendance(body: param)) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                
                //When there is no absentee in the class && the template is not defiend then there is no sms facilities.
                if self?.absent == 0 || self?.smsTemplate.trimmed().count == 0{
                    self?.dismissVC(completion: nil)
                    NotificationCenter.default.post(name: .UPDATE_STUDENT_LIST, object: nil)
                    Messages.shared.show(alert: .success, message: AlertMsg.attendanceSavedSuccess.get, type: .success)
                    return
                }
                self?.confirmationDone = true
                self?.updateUI()
            })
        }
    }
    
    
    func getStudentCountStatusWise(){
        if studentArr?.count != 0 {
            guard let arr = studentArr else {return}
            
            for (index,value) in arr.enumerated() {
                if value.status == 1 {
                    present = present + 1
                }
                else if value.status == 2 {
                    arr[index].templateForAbsentee = /smsTemplate.replace(target: "S____", withString:  /value.stName)
                    absent = absent + 1
                    smsTemplate = copyTemplate
                }
                else if value.status == 3 {
                    leave = leave + 1
                }
            }
            
            lblPresent?.text = "Present : \(present)"
            lblAbsent?.text = "Absent : \(absent)"
            lblLeave?.text = "Leave : \(leave)"
        }
    }
    
    
    func updateUI() {
        imgStatus?.image = confirmationDone ? UIImage(named : "check_iOS") : UIImage(named : "exclamation_iOS")
        viewPresent?.isHidden = confirmationDone
        viewAbsent?.isHidden = confirmationDone
        viewLeave?.isHidden = confirmationDone
        btnConfirm?.setTitle( confirmationDone ? "YES" : "CONFIRM", for: .normal)
        btnCancel?.setTitle(confirmationDone ? "NO" : "CANCEL" , for: .normal)
        lblMainHeading?.text = confirmationDone ? "Saved Successfully" : "Please confirm Students' Attendance"
        lblAlertHeading?.text = confirmationDone ? "Alert" : "Attendance Confirmation"
        lblSubHeading?.textAlignment = confirmationDone ? .center : .left
        if confirmationDone {
            lblSubHeading?.text = "Do you also want to send absentee notification (SMS/ Push notification)?"
        }
        else {
            lblSubHeading?.attributedText = Utility.shared.getAttributedString(firstStr: "As you have marked :", firstAttr: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], secondStr: nil, secondAttr: nil)
        }
    }
    
    
}


extension ConfirmAttendanceViewController {
    
    func startProgressView() {
        let xPosition = /progressView?.frame.width / 2
        let yPosition = /progressView?.frame.height / 2
        let position = CGPoint(x: xPosition, y: yPosition)
        //        progressView?.backgroundColor = UIColor.orange
        progressRing = CircularProgressBar(radius: 50, position: position, innerTrackColor: .defaultInnerColor, outerTrackColor: .defaultOuterColor, lineWidth: 6)
        progressView?.layer.addSublayer(progressRing)
        timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(incrementCount), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    // Note only works when time has not been invalidated yet
    @objc func resetProgressCount() {
        count = 0
        timer.fire()
    }
    
    @objc func incrementCount() {
        count += 1
        if count == 50 {
            count = 100
        }
        progressRing.progress = count
        
        if count >= 100.0 {
            ez.runThisAfterDelay(seconds: 1) {
                self.dismissVC(completion: nil)
            }
            timer.invalidate()
        }
    }
    
}



