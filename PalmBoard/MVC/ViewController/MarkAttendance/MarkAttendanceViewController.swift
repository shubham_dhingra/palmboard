//
//  MarkAttendanceViewController.swift
//  PalmBoard
//
//  Created by Shubham on 04/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class MarkAttendanceViewController: BaseViewController {
    
    var isFirstTime : Bool = true
    var classArr : [MyClasses]?
    var studentArr : [Student]?
    var subjectArr : [Subjects]?
    var selectSubject : Subjects?
    var selectClass : MyClasses?
    var subModuleId : Int?
    var smsType : Int?
    var attendanceMarkedStatus : Bool = false
    var smsTemplate : String?
    var AttType : AttendanceType?
    
    var isAlertShown : Bool = false
    
    @IBOutlet weak var lblClassSelect: UILabel?
    @IBOutlet weak var lblSubjectSelect : UILabel?
    @IBOutlet weak var btnSave : UIButton?
    @IBOutlet weak var classView: UIView?
    @IBOutlet weak var lblHeading : UILabel?
    @IBOutlet weak var subjectView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadTable()
        // self.hideMenu()
        // self.isNavBarHidden = true
        btnSave?.isHidden = true
        NotificationCenter.default.addObserver(forName: .UPDATE_STUDENT_LIST, object: nil, queue: nil) { (_) in
            
            //for subjectWise both iD need to be passed
            if let classID = self.selectClass?.classID , let subjectId = self.selectSubject?.SubID {
                self.getStudentList(classId: classID, subId: subjectId)
            }
            
            //for classWise subjectId pass to be nil
            else if let classID =  self.selectClass?.classID {
                self.getStudentList(classId: classID, subId: nil)
            }
        }
//        getClassList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ez.runThisInMainThread {
            self.isNavBarHidden = true
        }
        subjectView?.isHidden = AttType == .Class
        lblHeading?.text = AttType == .Class ? AlertMsg.classWiseAttendance.get : AlertMsg.subjectWiseAttendance.get
    }
    
    @IBAction func btnSelectClassAct(_ sender: UIButton) {
        
        if sender.tag == 1 {
            if classArr == nil || classArr?.count == 0 {
                getClassList()
            }
            else {
                openSelectClassVc(tag : sender.tag)
            }
        }
        else {
            guard let classs = self.selectClass else {
                Messages.shared.show(alert: .oops, message: AlertMsg.SelectClass.get, type: .warning)
                return
            }
            if lblClassSelect?.text != IOSMessages.SelectClass.get {
                
                if subjectArr?.count == 0 || subjectArr == nil {
                     getSubjectList(classID: /classs.classID)
                }
                else {
                    openSelectClassVc(tag : sender.tag)
                }
            }
        }
    }
    
    //MARK: - openSelectClassVc
    
    func openSelectClassVc(tag : Int){
        let storyboard = UIStoryboard(name :"Reports" , bundle: nil)
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "SelectClassViewController") as? SelectClassViewController {
            selectClassVc.classArr = classArr
            selectClassVc.delegate = self
            selectClassVc.subDelegate = self
            selectClassVc.selectedClass = selectClass
            selectClassVc.tag = tag
            selectClassVc.subjectArr = self.subjectArr
            selectClassVc.selectedSubject = self.selectSubject
            if !(ez.topMostVC is SelectClassViewController) {
               self.navigationController?.pushViewController(selectClassVc, animated: false)
            }
         }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = studentArr
            tableView?.reloadData()
        }
    }
    
    @IBAction func btnSaveAttendanceAct(_ sender: UIButton) {
        if self.studentArr?.count != 0 {
            self.hideMenu()
            let moduleStoryboard = UIStoryboard(name: "Module", bundle: nil)
            let vc = moduleStoryboard.instantiateViewController(withIdentifier: "ConfirmAttendanceViewController") as! ConfirmAttendanceViewController
            vc.studentArr = studentArr
            vc.smsType = /self.smsType
            vc.classID = /self.selectClass?.classID
            vc.subID = /self.selectSubject?.SubID
            vc.smsTemplate = /self.smsTemplate
            self.navigationController?.view.window?.rootViewController?.present(vc, animated: false , completion: nil)
           
        }
    }
}

extension MarkAttendanceViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: studentArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.StudentAttendanceCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? StudentAttendanceCell)?.attendanceMarkedStatus = /self.attendanceMarkedStatus
            (cell as? StudentAttendanceCell)?.index = /indexpath?.row
            (cell as? StudentAttendanceCell)?.modal = item
        }
    }
}


extension MarkAttendanceViewController : SearchByClassDelegate , SelectSubjectDelegate {
    
    func getSelectClass(selectClass: MyClasses?, classArr: [MyClasses]?) {
        if let classs = selectClass {
            self.selectSubject = nil
            self.lblSubjectSelect?.text = IOSMessages.SelectSubject.get
            lblClassSelect?.text = classs.ClassName
            self.selectClass = classs
            self.subjectArr = []
            if AttType == .Class {
                self.getStudentList(classId : /selectClass?.classID, subId: nil)
            }
            else {
                self.getSubjectList(classID: /selectClass?.classID)
            }
        }
    }
    
    func getSelectSubject(selectSubject : Subjects?, subjectArr : [Subjects]?) {
        if let subject = selectSubject {
            lblSubjectSelect?.text = subject.ShortName
            self.selectSubject = subject
            self.getStudentList(classId : /selectClass?.classID , subId : self.selectSubject?.SubID)
        }
    }
    
}

//MARK::- API ZONE
extension MarkAttendanceViewController {
    
    func getStudentList(classId : Int , subId : Int?) {
        APIManager.shared.request(with: HomeEndpoint.getStudentListClassWise(ClassID: classId.toString , SubID : subId?.toString), isLoader: true) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    
    
    func getClassList() {
        APIManager.shared.request(with: HomeEndpoint.getClassListForAttendance(UserID: /self.userID , isSubject: AttType == .Class ? 0 : 1), isLoader: false) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    
    
    func getSubjectList(classID : Int?) {
        guard let user = self.getCurrentUser() , let classID = classID else {return}
        APIManager.shared.request(with: HomeEndpoint.SubjectClassWise(UserID: Int(user.userID).toString, classID: classID.toString), isLoader: false) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    
    func handle(response : Any?) {
        if let modal = response as? ContactsModel , let arr = modal.myClasses {
            self.classArr = arr
            if /self.classArr?.count != 0 {
                openSelectClassVc(tag : 1)
            }
        }
        
        if self.classArr?.count == 0 || self.classArr == nil {
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: AlertMsg.NoClassAssigned.get, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                switch tag {
                default:
                    return
                }
            }
        }
        
        
        
        //get subject List
        if let modal = response as? StudentListModal , let subjects = modal.subjects {
            self.subjectArr = subjects
            if /self.subjectArr?.count != 0 {
                openSelectClassVc(tag : 2)
            }
         }
        
        if let modal = response as? StudentListModal , let hasMarked = modal.hasMarked , let arr = modal.studentList {
            tableView?.isHidden = arr.count == 0
            btnSave?.isHidden = hasMarked
            smsType = modal.sMSType
            attendanceMarkedStatus = hasMarked
            smsTemplate = modal.sMSTemp
            studentArr = arr
            reloadTable()
            if hasMarked && !isAlertShown{
                isAlertShown = true
                AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: "\(AlertMsg.attendanceLockedMsg1.get) (\(/selectClass?.ClassName)) \(AlertMsg.attendanceLockedMsg2.get)" , buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                    switch tag {
                    default:
                        self.isAlertShown = false
                        return
                    }
                }
            }
        }
    }
}
