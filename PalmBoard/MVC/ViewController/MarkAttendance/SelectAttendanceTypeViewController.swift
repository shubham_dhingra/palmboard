//  SelectClassTypeViewController.swift
//  PalmBoard
//  Created by Shubham on 23/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

protocol SelectAttendanceType  {
    func selectAttendanceType(_ type : AttendanceType , subModuleId : Int)
}
class SelectAttendanceTypeViewController: BaseViewController {
    
    //MARK::- Outlets
    @IBOutlet weak var btnSubjAtte  : UIButton?
    @IBOutlet weak var btnClassAtte : UIButton?
    @IBOutlet weak var lblType1     : UILabel?
    @IBOutlet weak var lblType2     : UILabel?
    @IBOutlet weak var imgType1     : UIImageView?
    @IBOutlet weak var imgType2     : UIImageView?
    @IBOutlet weak var classAttendanceView : UIView?
    @IBOutlet weak var subjAttendanceView : UIView?
    
    var subModule : [[String : Any]]?
    var delegate  : SelectAttendanceType?
    var module    : Module?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.window?.rootViewController?.dismiss(animated: false, completion: nil)
        if module == .Attendance || module == .GradeBook{
            if subModule?.count == 2 {
                classAttendanceView?.isHidden = false
                subjAttendanceView?.isHidden = false
                if let type1 =  subModule?[0]["SubModule"] as? String {
                    lblType1?.text = type1
                }
                if let type2 =  subModule?[1]["SubModule"] as? String {
                    lblType2?.text = type2
                }
            }
            if  subModule?.count == 1 {
                if let subModuleId =  subModule?[0]["S_MdlID"] as? Int {
                    
                    if module == .Attendance {
                    classAttendanceView?.isHidden  = !(subModuleId == 6)
                    subjAttendanceView?.isHidden  = !(subModuleId == 7)
                    }
                    if module == .GradeBook {
                        classAttendanceView?.isHidden  = !(subModuleId == 10)
                        subjAttendanceView?.isHidden  = !(subModuleId == 11)
                    }
                    
                    if subModuleId == 6 || subModuleId == 10 {
                        if let type1 =  subModule?[0]["SubModule"] as? String {
                            lblType1?.text = type1
                        }
                    }
                    else {
                        if let type1 =  subModule?[0]["SubModule"] as? String {
                            lblType2?.text = type1
                        }
                    }
                }
            }
        }
        else if module == .WeeklyPlan{
            classAttendanceView?.isHidden = false
            
            lblType1?.text = AlertMsg.CreatePlan.get
            lblType2?.text = AlertMsg.ExisitingPlan.get
        }
        
        //set image of modules
        ez.runThisInMainThread {
            if let selModule = self.module {
            switch selModule {
                
            case .Attendance :
                self.imgType1?.image = UIImage(named : "Class Att")
                self.imgType2?.image = UIImage(named :"Student Att")
            case .WeeklyPlan :
                self.imgType1?.image = UIImage(named :"create_plan")
                self.imgType2?.image = UIImage(named :"view_plan")
            case .GradeBook :
                self.imgType1?.image = UIImage(named :"ongoing_assessment_marks")
                self.imgType2?.image = UIImage(named :"report_card")
            default :
                break
            }
//            self.imgType1?.image = UIImage(named:  self.module  == .WeeklyPlan ? "create_plan" : "Class Att")
//            self.imgType2?.image = UIImage(named : self.module == .WeeklyPlan  ? "view_plan"   : "Student Att")
        }
            self.isNavBarHidden = true
        }
        self.hideMenu()
    }
    
    //MARK::- Button Actions
    @IBAction func btnSelectAttType(_ sender : UIButton){
        
//        self.lblType1?.textColor = UIColor.flatGreen
//        self.lblType2?.textColor = UIColor.flatGreen
        let storyboard = Storyboard.Module.getBoard()
        guard let selModule = module else {
            print("module value is nil")
            return
        }
        switch selModule {
        case .Attendance:
            if let vc = storyboard.instantiateViewController(withIdentifier: "MarkAttendanceViewController") as? MarkAttendanceViewController {
                if /self.subModule?.count == 2 {
                    vc.AttType = sender.tag == 1 ? .Class : .Subject
                    vc.subModuleId = sender.tag == 1 ? (self.subModule?[0]["S_MdlID"] as? Int) : (self.subModule?[1]["S_MdlID"] as? Int)
                }
                else {
                    vc.AttType = sender.tag == 1 ? .Class : .Subject
                    vc.subModuleId = (self.subModule?[0]["S_MdlID"] as? Int)
                }
                self.navigationController?.pushViewController(vc, animated: false)
            }
        case .WeeklyPlan:
            if sender.tag == 1 {
                if let vc = storyboard.instantiateViewController(withIdentifier: "NewPlanViewController") as? NewPlanViewController {
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
                // view existing plan
            else if sender.tag == 2 {
                if let vc = storyboard.instantiateViewController(withIdentifier: "PlanListViewController") as? PlanListViewController {
                    vc.userRole = .Staff
                    vc.action = .View
                    vc.viewType = 1
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        case .GradeBook:
            let mainStoryBoard = Storyboard.Main.getBoard()
            if sender.tag == 1 {
                
                if let vc = mainStoryBoard.instantiateViewController(withIdentifier: "WebviewReportCardViewController") as? WebviewReportCardViewController {
                    vc.module = .OnlineAssessmentMarks
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
                // view existing plan
            else if sender.tag == 2 {
                
                if let vc = mainStoryBoard.instantiateViewController(withIdentifier: "reportStoryboardID") as?
                    ReportCardViewController {
                    if let userId = userID  {
                        vc.intUserID = userId.toInt() ?? 0
                    }
                  self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            
            break
        default:
        break
        }
    }
}
