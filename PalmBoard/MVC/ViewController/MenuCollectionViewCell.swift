
import UIKit


class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuImgView: UIImageView?
    @IBOutlet weak var menuLbl:     UILabel?
}
