//  ReplyMessageViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/04/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import AVFoundation
import AlamofireImage
import Alamofire
import IQKeyboardManager

class ChatViewController : BaseViewController {
    //MARK::- OUTLETS
    @IBOutlet weak var imgProfile    : UIImageView?
    @IBOutlet weak var lblName       : UILabel?
    @IBOutlet weak var lblDesignation: UILabel?
    @IBOutlet weak var lblSubject    : UILabel?
    @IBOutlet weak var msgStack      : UIStackView?
    @IBOutlet weak var viewReply     : UIView?
    @IBOutlet weak var viewMedia     : UIView?
    @IBOutlet weak var btnRecipent   : UIButton?
    @IBOutlet weak var txtMessage    : UITextView?
    @IBOutlet weak var btnSend       : UIButton?
    @IBOutlet weak var msgStackBottomConstraint : NSLayoutConstraint?
    
    //MARK::- VARIABLES
    var isFirstTime  : Bool = true
    var chatList     : [ChatMsgs]?
    var name         : String?
    var designation  : String?
    var imgUser      : UIImage?
    var senderID     : Int?
    var senderType   : Int?
    var msgSendType  : Int = 1
    var sMessage     : SMessages?
    var attachment   : String?
    var senderPicUrl : String?
    var msgType      : MessageType?
    var placeholderLabel : UILabel!
    var uploadImage      : UIImage?
    var isStopAll   : Bool = false
    
    var chatDataSource  : ChatDataSource?{
        didSet{
            tableView?.dataSource = chatDataSource
            tableView?.delegate   = chatDataSource
            tableView?.reloadData()
        }
    }
    
    private var isiPhoneX: Bool {
        return (UIScreen.main.nativeBounds.size.height == 2436) ||  (UIScreen.main.nativeBounds.size.height == 2688) || (UIScreen.main.nativeBounds.size.height == 1792)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    
    func onViewDidLoad(){
        
        placeholderLabel = Utility.shared.setUpTextViewPlaceholder(txtMessage!)
        txtMessage?.addSubview(placeholderLabel)
        
        placeholderLabel.isHidden = !(/txtMessage?.text.isEmpty)
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        IQKeyboardManager.shared().isEnabled = false
        self.hideTabBar()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        txtMessage?.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name:UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        isStopAll = false
        self.isNavBarHidden = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : true , "Index" :3])
        onViewWillAppear()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
        onViewWillDisappear()
    }
    
    func onViewWillDisappear() {
        
        isStopAll = true
        guard let viewControllers = self.navigationController?.viewControllers else {return}
        
        if viewControllers.first is BoardViewController {
            self.isNavBarHidden = false
            showTabBar()
            showMenu()
        }
        
        guard let chatMsgs = chatList else {return}
        
        let tempArr =  chatMsgs.map({ (chatMsg) -> ChatMsgs in
            chatMsg.isPlaying = true
            return chatMsg
        })
        
        chatList = tempArr
        reloadTable()
        }
//        var tempArr = chatMsgs
//        for (index , value) in tempArr.enumerated() {
//            if value.jukeBoxRef != nil {
//                   value.isPlaying = true
//                }
//            }
       
    
    func openMedia(mediaType : String?) {
        CameraGalleryPickerBlock.shared.pickerImage(isActionSheetOpen: false, openMediaType: mediaType, pickedListner: { (image, fileName) in
            self.uploadImage = image
            self.msgSendType = 2
            self.replyMessage()
        }) {
        }
    }
//    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//        if self.view.frame.origin.y == 0{
//            self.view.frame.origin.y -= keyboardSize.height
//        }
//    }
    // MARK: - keyboardWillShow
//    @objc func keyboardWillShow(_ notification : Notification){
//          viewMedia?.isHidden = true
//          scrollToBottom()
//
//        msgStackBottomConstraint?.constant = 333.0
//      }
//
//
//    // MARK: - keyboardWillHide
//    @objc func keyboardWillHide(_ notification : Notification){
//        msgStackBottomConstraint?.constant = 0.0
//        viewMedia?.isHidden   = msgType != .Inbox
//        if msgType == .Inbox {
//            viewMedia?.isHidden = userType != "3"
//        }
//        if self.view.frame.origin.y != 0 {
//            self.view.frame.origin.y = 0
//        }
//        txtMessage?.resignFirstResponder()
//    }
    
    
    
    // MARK: - keyboardWillShow
     func keyboardWillShow(){
         msgStackBottomConstraint?.constant = isiPhoneX ? 243.0 : 226.0
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
        
        
        
          viewMedia?.isHidden = true
          scrollToBottom()

          self.view.layoutIfNeeded()

    }
//
//
    // MARK: - keyboardWillHide
     func keyboardWillHide(){
        msgStackBottomConstraint?.constant = 0
        viewMedia?.isHidden   = msgType != .Inbox
        if msgType == .Inbox {
            viewMedia?.isHidden = userType != "3"
        }
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0 {
//                self.view.frame.origin.y = 0
//            }
//        }
        txtMessage?.resignFirstResponder()
    }
    
    
    
    @IBAction func btnRecipientAct(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RecipientListViewController") as? RecipientListViewController else {return}
        vc.recipientArr = sMessage?.recipients
        self.presentVC(vc)
    }
    
    
    @IBAction func btnSendAct(_ sender : Any){
        if txtMessage?.text.trimmed().count != 0 {
            replyMessage()
        }
        else {
            Messages.shared.show(alert: .oops, message: AlertMsg.MessageEmpty.get , type: .warning)
        }
    }
    @IBAction func btnMediaAct(_ sender : UIButton){
        self.view.endEditing(true)
        mediaAction(sender)
    }
    func mediaAction(_ sender : UIButton){
        switch sender.tag {
        //camera
        case 0:
            openMedia(mediaType : AlertConstants.Camera.get)
            break
        // gallery
        case 1:
            openMedia(mediaType : AlertConstants.Gallery.get)
            break
        //audio
        case 2:
            break
        default:
            break
        }
    }
    
}
//MARK::- MESSAGE FROM API -> (GET ALL MESSAGES)
extension ChatViewController {
    
    func getChatList() {
        guard let msgID = sMessage?.msgID , let senderID = senderID , let senderType = senderType  else {return}
        APIManager.shared.request(with: HomeEndpoint.getMessageDTL(msgID: msgID, senderID: senderID, senderType: senderType, userID: userID, userType: userType), isLoader : isFirstTime) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    func getSentMessageDTL() {
        guard let msgID = sMessage?.msgID else {return}
        APIManager.shared.request(with: HomeEndpoint.sentMessageDTL(msgID: msgID, userID: userID, userType: userType)) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    func handle(response : AnyObject?){
        if let modal = response as? MessageDTLModal {
            if let msgArr = modal.msgDTL {
                chatList = msgArr
            }
            else {
                chatList = []
            }
            reloadTable()
        }
    }
    
    func onViewWillAppear() {
         IQKeyboardManager.shared().isEnabled = false
        if let image = imgUser {
            imgProfile?.image = image
        }
        else {
            if let profilePic = senderPicUrl {
                imgProfile?.loadURL(imageUrl: profilePic.getImageUrl(), placeholder: nil, placeholderImage: R.image.noProfile_Big())
            }
            else {
                imgProfile?.image = R.image.noProfile_Big()
            }
        }
        if let subject = sMessage?.subject {
            lblSubject?.text = "Sub : \(subject)"
        }
        
        btnRecipent?.isHidden = msgType  == .Inbox
        viewMedia?.isHidden   = msgType != .Inbox
        viewReply?.isHidden   = msgType    != .Inbox
        if msgType == .Inbox {
            viewMedia?.isHidden = userType != "3"
        }
        if msgType == .Inbox {
            
            lblName?.text = /name
            lblDesignation?.text = /designation
            
            getChatList()
        }
        else {
            if let sMessage = self.sMessage , let recipient = sMessage.recipients {
                
                if recipient.count != 0 {
                    let names = recipient.map({ (recipient) -> String in
                        /recipient.name
                    })
                    let firstName = names[0]
                    let count = Int(/names.count - 1)
                    
                    lblName?.text = /firstName
                    lblDesignation?.text = count != 0 ? "and \(count) more" : nil
                    getSentMessageDTL()
                }
            }
        }
    }
}

extension ChatViewController {
    //MARK::- replyMessage
    func replyMessage() {
        self.view.endEditing(true)
        
        if let image = self.uploadImage  {
            attachment = image.convertImageToBase64()
        }
        if let sMessages = sMessage {
            APIManager.shared.request(with: HomeEndpoint.replyMessage(msgID: /sMessages.msgID, msgType: msgSendType, userID: userID, userType: userType, Body: txtMessage?.text, Attachment: attachment, ReceiverID: /senderID, ReceiverType: /senderType), isLoader: false) {[weak self](response) in
                self?.handleResponse(response: response, responseBack: { (success) in
                    self?.handleReplyResponse(response: success)
                })
            }
        }
    }
    func handleReplyResponse(response : Any?){
        if (response as? String) != nil {
            getChatList()
            txtMessage?.text = nil
            attachment = nil
            uploadImage = nil
            msgSendType = 1
            placeholderLabel.isHidden = !(/txtMessage?.text.isEmpty)
        }
    }
}
//MARK: - ConfigureTableView
extension ChatViewController {
    func configureTableView() {
        chatDataSource  = ChatDataSource(items: chatList, height: UITableView.automaticDimension, tableView: tableView , userID : userID?.toInt() , userType : userType?.toInt() , isStopAll: isStopAll)
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            chatDataSource?.isStopAll = isStopAll
            chatDataSource?.items = chatList
            tableView?.reloadData()
        }
        scrollToBottom()
    }
    
    func scrollToBottom(){
        if /chatList?.count != 0 {
            tableView?.scrollToRow(at: IndexPath(row: /chatList?.count - 1 , section: 0), at: UITableView.ScrollPosition.bottom , animated: false)
        }
    }
}
// MARK: - TextField Delegate Method
extension ChatViewController : UITextViewDelegate {
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
      keyboardWillHide()
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       keyboardWillShow()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            self.view.endEditing(true)
        }
        return true
    }
}
