//  MessageDetailsSearchViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 10/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class MessageDetailsSearchViewController: BaseViewController,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lblNoRecord: UILabel?
    
    var pgNo        = 1
    var isFirstTime : Bool = true
    var messageArr  : [SMessages]?
    var name        : String?
    var designation : String?
    var imgUser     : UIImage?
    var msgModal    : IMessages?
    var msgType     : MessageType?
    
    //MARK::- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
    }
    //MARK::- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabBar()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target: nil, action: nil)
        self.isNavBarHidden = true
    }
    //MARK::- Search Bar Delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        getAllMessages(pgNo: pgNo, searchText: searchBar.text!)
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 20
    }
}
//MARK::- MESSAGE FROM API -> (GET ALL MESSAGES)
extension MessageDetailsSearchViewController {
    func getAllMessages(pgNo : Int,  searchText: String) {
        guard let senderID = msgModal?.senderID , let senderType = msgModal?.senderType else {return}
        APIManager.shared.request(with: HomeEndpoint.getMessageSearch(senderID: senderID, senderType: senderType, userID: userID, userType: userType, pg: pgNo, query: searchText )) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    func handle(response : AnyObject?){
        print("handle  All Messages")
        if let modal = response as? MessageMainModal {
            if let msgArr = modal.sentMessageArr {
                messageArr = msgArr
                lblNoRecord?.isHidden = true
                reloadTable()
            }
            else {
                messageArr = []
                 lblNoRecord?.isHidden = false
                reloadTable()
            }
        }
    }
}
//MARK: - ConfigureTableView
extension MessageDetailsSearchViewController {
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: messageArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.MessageCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? MessageCell)?.index  = indexpath?.row
            (cell as? MessageCell)?.modal = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    func didSelect(_ index : Int){
        navigateToChatVc(index)
    }
    func navigateToChatVc(_ index : Int) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else {return}
        vc.sMessage    = messageArr?[index]
        vc.name        = name
        vc.designation = designation
        vc.imgUser     = imgUser
        vc.senderID    =  msgModal?.senderID
        vc.senderType  = msgModal?.senderType
        vc.msgType     = msgType
        pushVC(vc)
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = messageArr
            tableView?.reloadData()
        }
    }
}
