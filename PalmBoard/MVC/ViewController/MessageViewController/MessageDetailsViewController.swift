//  MessageDetailsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/04/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class MessageDetailsViewController: BaseViewController {
    
    @IBOutlet weak var imgProfile    : UIImageView?
    @IBOutlet weak var lblName       : UILabel?
    @IBOutlet weak var lblDesignation: UILabel?
    
    var pgNo       = 1
    var isFirstTime : Bool = true
    var messageArr  : [SMessages]?
    var name        : String?
    var designation : String?
    var imgUser     : UIImage?
    var msgModal    : IMessages?
    var msgType     : MessageType?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
   }
    override func viewWillAppear(_ animated: Bool) {
      
        super.viewWillAppear(animated)
        showTabBar()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target: nil, action: nil)
        self.isNavBarHidden = true
        onViewWillAppear()
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)

    }
    @IBAction func btnSearchAct(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "MessageDetailsSearchViewController") as? MessageDetailsSearchViewController else {return}
        vc.isNavBarHidden = true
        vc.msgModal    = msgModal
        vc.msgType     = .Inbox
        vc.name        = name
        vc.designation = designation
        vc.msgType     = .Inbox
        vc.imgUser     = imgUser
        pushVC(vc)
    }
    func onViewWillAppear() {
       if let image = imgUser {
            imgProfile?.image = image
        }
        lblName?.text = /name
        lblDesignation?.text = /designation
         getAllMessages(pgNo : pgNo)
    }
}
//MARK::- MESSAGE FROM API -> (GET ALL MESSAGES)
extension MessageDetailsViewController {
   
    func getAllMessages(pgNo : Int) {
        guard let senderID = msgModal?.senderID , let senderType = msgModal?.senderType else {return}
        APIManager.shared.request(with: HomeEndpoint.getAllMessages(senderID: senderID, senderType: senderType, userID: userID, userType: userType, pg: pgNo)) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    func handle(response : AnyObject?){
        print("handle  All Messages")
        if let modal = response as? MessageMainModal {
            if let msgArr = modal.sentMessageArr {
                messageArr = msgArr
                reloadTable()
            }
            else {
                messageArr = []
                reloadTable()
            }
        }
    }
}
//MARK: - ConfigureTableView
extension MessageDetailsViewController {
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: messageArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.MessageCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? MessageCell)?.index  = indexpath?.row
            (cell as? MessageCell)?.modal = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ index : Int){
        navigateToChatVc(index)
    }
    
    func navigateToChatVc(_ index : Int) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else {return}
        vc.sMessage = messageArr?[index]
        vc.name = name
        vc.designation = designation
        vc.imgUser = imgUser
        vc.senderID =  msgModal?.senderID
        vc.senderType = msgModal?.senderType
        vc.msgType = msgType
        pushVC(vc)
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = messageArr
            tableView?.reloadData()
        }
    }
}
