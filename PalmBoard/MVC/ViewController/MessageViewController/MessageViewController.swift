
//  MessageViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/04/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class MessageViewController: BaseViewController {
    
    //MARK::- VARIABLES
    var pageNo             = 1
    var intSelfSenderID    : Int?
    var intSelfSenderType  : Int?
    var actionButton       : ActionButton!
    var messageArr         : [IMessages]?
    var sentMessageArr     : [SMessages]?
    var refreshControl     = UIRefreshControl()
    var isFirstTime        : Bool = true
    var segmentIndex       : Int = 0
    var totalMessage       : Int = 0
    
    //MARK::- OUTLETS
    @IBOutlet weak var viewSegment: UIView?
    @IBOutlet weak var segmentView: UISegmentedControl?
    @IBOutlet weak var segmentHeight : NSLayoutConstraint?
    @IBOutlet weak var imgViewNoMessage: UIImageView?
    @IBOutlet weak var lblNoMessage    : UILabel?

    //MARK::- LIFECYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
//        reloadTable()
        
        addRefereshControl()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        ModuleSetter.shared.messageSetting(userID: self.userID, userType: self.userType) { (msgSettingModal) in
            self.segmentView?.isHidden = !(/msgSettingModal.compose)
            self.segmentHeight?.constant = /msgSettingModal.compose ? 50 : 0
            UIView.animate(withDuration: 2.0) {
                if /msgSettingModal.compose {
                     self.setUpActionLayer(msgSettingModal)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showTabBar()
        showMenu()
        pageNo = 1
        totalMessage = 0
        getMessages(pageNo)
        self.isNavBarHidden = true
        if segmentIndex == 0 && messageArr?.count != 0 {
            reloadTable()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 2.0) {
//            self.setUpActionLayer()
//        }
          NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : true , "Index" : 3])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let viewControllers = self.navigationController?.viewControllers else {return}
            
        if viewControllers.count != 0 {
            for vc in viewControllers {
                if vc is MessageDetailsViewController || vc is ChatViewController {
                    self.isNavBarHidden = true
                }
            }
            isNavBarHidden = false
        }
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func ClkSegment(_ sender: Any) {
        segmentIndex = /segmentView?.selectedSegmentIndex
        pageNo = 1
        totalMessage = 0
        if segmentIndex == 0 {
           messageArr = []
        }
        else {
            sentMessageArr = []
        }
        getMessages(pageNo)
    }
}


//MARK::- PAGINATION METHODS
extension MessageViewController {
    
    func addRefereshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
    }
    
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        pageNo  = 1
        totalMessage = 0
        self.getMessages(pageNo)
    }
    
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView) {
        if self.totalMessage == /self.tableDataSource?.items?.count {
             print("All message already loaded")
            return
        }
        
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            print("fetch more data -- scrollViewDidEndDragging")
            
            if self.totalMessage != /self.tableDataSource?.items?.count {
                pageNo = pageNo + 1
                self.getMessages(pageNo)
            }
            
        }
    }
    
}

//MARK:: - ******** INBOX AND SENT API AND RESPONSE HANDLING **********
extension MessageViewController {
    
    func getMessages(_ pageNo : Int){
        let route = HomeEndpoint.getMessages(userID: userID, userType: userType, pg: pageNo, segmentIndex:  segmentIndex)
        APIManager.shared.request(with: route, isLoader: true) {[weak self](response) in
            self?.refreshControl.endRefreshing()
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    func handle(response : AnyObject?){
        if let modal = response as? MessageMainModal {
            if segmentIndex == 0 {
               handleInboxMessage(modal)
            }
            else {
                handleSentMessage(modal)
            }
        }
    }
    func handleInboxMessage(_ modal : MessageMainModal){
        if let msgArr = modal.messageArr {
            totalMessage = /modal.total
            if pageNo == 1{
                self.messageArr = []
            }
            if totalMessage == self.messageArr?.count {
                print("All inbox message already loaded")
                return
            }
            if pageNo == 1 {
                self.messageArr = msgArr
            }
            else {
                if msgArr.count != 0 {
                    _ =  msgArr.map{(self.messageArr?.append($0))}
                }
            }
            reloadTable()
        }
        else {
            if pageNo == 1 {
            Messages.shared.show(alert: .oops, message: segmentView?.selectedSegmentIndex == 0 ? AlertMsg.InboxEmpty.get : AlertMsg.SentEmpty.get , type: .warning)
            messageArr = []
            }
            reloadTable()
        }
        self.imgViewNoMessage?.isHidden = /(messageArr?.count) != 0
        self.lblNoMessage?.isHidden     = /(messageArr?.count) != 0
    }
    func handleSentMessage(_ modal : MessageMainModal){
        if let sentMsgArr = modal.sentMessageArr {
            totalMessage = /modal.total
            if pageNo == 1{
                 self.sentMessageArr = []
            }
            if totalMessage == self.sentMessageArr?.count {
                print("All sent message already loaded")
                return
            }
            
            if pageNo == 1 {
                
                self.sentMessageArr = sentMsgArr
            }
            else {
                if sentMsgArr.count != 0 {
                    _ =  sentMsgArr.map{(self.sentMessageArr?.append($0))}
                }
            }
            reloadTable()
        }
        else {
              if pageNo == 1 {
                 Messages.shared.show(alert: .oops, message: segmentView?.selectedSegmentIndex == 0 ? AlertMsg.InboxEmpty.get : AlertMsg.SentEmpty.get , type: .warning)
             sentMessageArr = []
            }
            reloadTable()
        }
        self.imgViewNoMessage?.isHidden = /(sentMessageArr?.count) != 0
        self.lblNoMessage?.isHidden     = /(sentMessageArr?.count) != 0
    }
}

//MARK: - ConfigureTableView
extension MessageViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: messageArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.MessageCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? MessageCell)?.index  = indexpath?.row
            (cell as? MessageCell)?.modal = item
        }
        
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            guard let cell = self.tableView?.cellForRow(at: indexpath) as? MessageCell  else {return}
            self.didSelect(cell , indexpath.row)
        }
        tableDataSource?.scrollViewEndDeaccerlate = {(scrollView) in
            self.reloadMoreData(scrollView)
        }
    }
    
    func didSelect(_ cell : MessageCell ,_ index : Int){
        segmentView?.selectedSegmentIndex == 0 ? naviagteToMessageDetails(cell, index) : navigateToChatVc(cell, index)
    }
    
    
    func naviagteToMessageDetails(_ cell: MessageCell , _ index : Int){
        
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "MessageDetailsViewController") as? MessageDetailsViewController else {return}
        vc.msgModal = messageArr?[index]
        vc.name = cell.lblName?.text
        vc.designation = cell.lblDesignation?.text
        vc.msgType = .Inbox
        vc.imgUser = cell.imgUser?.image
        vc.isNavBarHidden = true
        pushVC(vc)
        
    }
    
    
    func navigateToChatVc(_ cell: MessageCell, _ index : Int) {
        
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else {return}
        vc.sMessage = sentMessageArr?[index]
        vc.name = cell.lblName?.text
        vc.designation =  cell.lblDesignation?.text
        vc.imgUser = cell.imgUser?.image
        vc.isNavBarHidden = true
        vc.msgType = .Sent
        pushVC(vc)
    
    }
    
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = segmentIndex == 0 ? messageArr : sentMessageArr
            tableView?.reloadData()
            if pageNo != 1 {
                if /tableDataSource?.items?.count != 0 {
                    tableView?.scrollToRow(at: IndexPath(row: /tableDataSource?.items?.count - 1 , section: 0), at: .bottom, animated: false)
                }
            }
        }
    }
}


//MARK::- SetUp ActionLayer
extension MessageViewController {
    
    func setUpActionLayer(_ msgSettingModal : MessageMainModal) {
       
        actionButton = nil
        var actionButtonArr = [ActionButtonItem]()
        if /msgSettingModal.msgWithSMS {
        let smsutton    = ActionButtonItem(title: "SMS & App Message", image: #imageLiteral(resourceName: "SMS & App Message"))
        smsutton.action = { item in
            self.navigateToComposeMessage(.sms)
            self.actionButton.toggle()
        }
        actionButtonArr.append(smsutton)
        }
        
        if /msgSettingModal.OnlyMsg {
        let appButton = ActionButtonItem(title: "Only App Message", image : #imageLiteral(resourceName: "only App message"))
        appButton.action = { item in
            self.navigateToComposeMessage(.app)
            self.actionButton.toggle()
        }
        actionButtonArr.append(appButton)
        }
        if actionButtonArr.count != 0 || /msgSettingModal.compose{
             actionButton = ActionButton(attachedToView: self.view, items: actionButtonArr)
        }
        actionButton.action = { button in
            (self.userType == "1" ||  self.userType == "2") ?
                self.navigateToComposeMessage(.app) :  button.toggleMenu()
        }
    }
    
    func navigateToComposeMessage(_ messageType : MessageType) {
        let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "ComposeMsgViewController") as? ComposeMsgViewController else {return}
        vc.messageType = messageType
        self.pushVC(vc)
    }
}
