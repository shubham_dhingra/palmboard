//
//  RecipientListViewController.swift
//  e-Care Pro
//
//  Created by Shubham on 13/07/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class RecipientListViewController: BaseViewController {
    //MARK::- Outlets
    @IBOutlet weak var lblHeading : UILabel?
    //MARK::- Variables
    var isFirstTime  : Bool = true
    var recipientArr : [Recipients]?
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForIphoneX()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        onViewWillAppear()
        reloadTable()
        configureTableView()
        
    }
    func onViewWillAppear() {
        if let count = recipientArr?.count {
            
            lblHeading?.text = count == 1 ? "\(count) Recipient" : "\(count) Recipients"
        }
        else {
            lblHeading?.text = nil
        }
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = recipientArr
            tableView?.reloadData()
        }
    }
}
extension RecipientListViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: recipientArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.RecipientCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? RecipientCell)?.modal = item
        }
    }
}


//MARK ::-  Recipient Cell and Cell Configuration
class RecipientCell : TableParentCell {
    
    var modal : Any? {
        didSet{
            configure()
        }
    }
    
    func configure() {
        if let object = modal as? Recipients {
          lblName?.text = Utility.shared.getFullIdentity(object: object)
            if let photoUrl = object.photo {
                let url = photoUrl.getImageUrl()
                    imgUser?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: nil)
             }
            else {
                imgUser?.image = R.image.noProfile_Big()
            }
        }
    }
}
