//  MyQuestionsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 19/06/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import Alamofire

class MyQuestionsViewController: BaseViewController,DeleteDelegate {
    
    @IBOutlet weak var segmentControl:     UISegmentedControl?
    @IBOutlet weak var segmentBgView:      UIView?
    @IBOutlet weak var imgViewNoQuestions: UIImageView?
    @IBOutlet weak var lblNoQuestions:     UILabel?
    
    var isFirstTime          : Bool   = true
    var tempArray            : [ListQuestion]?
    var arrayQuestions       : [ListQuestion]?
    var isPull2Refresh       = Bool()
    var deleteOrReport       = 0
    var deleteRowIndex       = IndexPath()
    var strSchoolCodeSender: String?
    var intUserType:         Int?
    var intUserID:           Int?
    var intCatID:            Int?
    var intCount:            Int?
    var intAll               :Int?
    var intQID:              Int?
    let selectedColor        = UIColor(rgb: 0x56B54B)
    let unselectedColor      = UIColor(rgb: 0x5E5E5E)
    var spinner             : UIActivityIndicatorView?
    var themeColor          : String?
    var btnTitle            : UIButton?
    var questionDataSource : QuestionTableDataSource?{
        didSet{
            tableView?.dataSource = questionDataSource
            tableView?.delegate = questionDataSource
            tableView?.reloadData()
        }
    }
    // var refreshControl   = UIRefreshControl()
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.tableView?.scrollToRow(at: [0,0], at: .top, animated: true)
    }
    @IBAction func addQuestionPressed(_ sender: UIBarButtonItem) {
        guard let updateThoughtVc = R.storyboard.main.uploadQuestionViewController() else {return}
        self.pushVC(updateThoughtVc)
    }
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        intAll = 1
        self.isPull2Refresh  = false
        navigationItem.hidesBackButton = true
        btnTitle                     = UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle?.frame              = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle?.setTitle("Questionnaire", for: UIControl.State.normal)
        btnTitle?.titleLabel?.font   = R.font.ubuntuMedium(size: 17)
        btnTitle?.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle?.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        if let schoolCode = self.getCurrentSchool()?.schCode  {strSchoolCodeSender =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID     {intUserID           =  Int(userId)}
        if let userId     = self.getCurrentUser()?.userType   {intUserType         =  Int(userId)}
        if let userId     = self.getCurrentUser()?.categoryId {intCatID            =  Int(userId)}
        
        spinner                  = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner?.color            = UIColor(rgb: 0x56B54B)
        spinner?.hidesWhenStopped = true
        spinner?.frame            = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        tableView?.tableFooterView = spinner
        
        spinner?.stopAnimating()
        Utility.shared.loader()
        tableView?.isHidden            = true
        tableView?.backgroundView      = nil
        tableView?.backgroundColor     = UIColor.clear
        tableView?.estimatedRowHeight = 250
        tableView?.rowHeight          = UITableView.automaticDimension
        // refreshControl = UIRefreshControl()
        // refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        // tblBoard?.addSubview(refreshControl)
        let nib = UINib(nibName: "QuestionNormal", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: CellIdentifiers.CellQuestionNormal.get)
        tableView?.tableFooterView    = UIView(frame: CGRect.zero)

        let nibImage = UINib(nibName: "QuestionImage", bundle: nil)
        tableView?.register(nibImage, forCellReuseIdentifier: CellIdentifiers.CellMyQuestionImage.get)
        tableView?.tableFooterView    = UIView(frame: CGRect.zero)

    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.imgViewNoQuestions?.isHidden = true
        self.lblNoQuestions?.isHidden     = true
        // self.webAPIQuestions(intPage: 1, intAll: 1)
        self.webAPIQuestions(intPage: 1, intAll:segmentControl?.selectedSegmentIndex == 0 ? 1 : 0)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        intCount  = 1
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    // MARK: -  segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl){
        self.webAPIQuestions(intPage: 1, intAll:segmentControl?.selectedSegmentIndex == 0 ? 1 : 0)
        intAll = segmentControl?.selectedSegmentIndex == 0 ? 1 : 0
        intCount  = 1
        reloadTable()
    }
    //MARK: webAPIQuestions Method
    func webAPIQuestions(intPage: Int, intAll: Int)
    {
        self.imgViewNoQuestions?.isHidden = true
        self.lblNoQuestions?.isHidden     = true

        // All - 1 // My- 0
        if(intPage == 1 && !self.isPull2Refresh){
            self.view?.isUserInteractionEnabled = false
        }
        APIManager.shared.request(with: HomeEndpoint.QuestionLst(UserID: self.userID, UserType: self.userType, CatID: intCatID?.toString, pg: intPage.toString, dir: 1.toString, all: intAll.toString), isLoader: isFirstTime) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success,intPage:intPage)
            })
        }
    }
    //MARK::- handle
    func handle(response : Any?,intPage: Int){
        if let model = response as? QuestionModal
        {
            self.view.isUserInteractionEnabled = true
            self.spinner?.stopAnimating()
            //
            tempArray                = model.list
            self.tableView?.isHidden = false
            if /tempArray?.count > 0 {
                if(intPage == 1){
                    self.arrayQuestions = tempArray
                    self.isPull2Refresh  = false
                }
                else{
                    // print("when new data loaded")
                    //when new data loaded
                    if let tempArrayA = self.tempArray {
                        self.arrayQuestions?.append(contentsOf: tempArrayA)
                    }
                }
                //print("self.arrayThoughts.count after loading \n \(self.arrayThoughts.count)")
                reloadTable()
//                if(intPage == 1){
//                    self.tableView?.scrollToRow(at: [0,0], at: .top, animated: true)
//                }
            }
            else{
                if(intPage == 1){
                    self.arrayQuestions = tempArray
                }
                //print("list empty")
                if(self.arrayQuestions?.count == 0 && intPage == 1){
                    self.imgViewNoQuestions?.isHidden = false
                    self.lblNoQuestions?.isHidden     = false//uploadquestion
                    self.lblNoQuestions?.text         = intAll == 1 ? "No Questionnaire Available" : R.string.localize.uploadquestion()//uploadquestion

                    self.tableView?.isHidden = true
                }
                else
                {
                    if(!self.isPull2Refresh){
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: (intPage == 1 ? R.string.localize.questionIsNotFound() : R.string.localize.noMoreQuestions()), fromVC: self)
                    }
                }
            }
        }
        ////
        /* //
         self.tempArray = model.list
         if self.tempArray?.count == 0
         {
         AlertsClass.shared.showNativeAlert(withTitle: "", message: (intPage == 1 ? R.string.localize.questionIsNotFound() : R.string.localize.noMoreQuestions()), fromVC: self)
         }
         else
         {
         if let tempArrayA = self.tempArray {
         self.arrayQuestions?.append(contentsOf: tempArrayA)
         }
         //self.reloadTable()
         }
         //
         if intPage == 1 {
         self.arrayQuestions = model.list
         }
         if self.arrayQuestions?.count == 0 && intPage > 1 {
         // self.arrayQuestions = [[:]]
         self.arrayQuestions?.removeAll()
         }
         else {
         self.arrayQuestions = model.list
         }
         self.imgViewNoQuestions?.isHidden = self.arrayQuestions?.count != 0
         self.lblNoQuestions?.isHidden     = self.arrayQuestions?.count != 0
         reloadTable()
         }
         else {
         self.spinner?.stopAnimating()
         DispatchQueue.main.async
         {
         self.view.isUserInteractionEnabled = true
         self.reloadTable()
         WebserviceManager.showAlert(message: "No internet connection available", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
         }, secondBtnBlock: {})
         }
         }*/
    }
    //MARK::- handleQuestionLike
    func handleQuestionLike(response : Any?,indexPath: IndexPath,sender: UIButton?,QuestionType: Int?){
        if let model = response as? ThoughtsLikeModal
        {
            if model.status == "ok" && model.message == "Liked"
            {
                if QuestionType == 1
                {
                    let cell = self.tableView?.cellForRow(at: indexPath) as! MyQuestionNormalTableViewCell
                    if /model.totalLikes > 1{
                        cell.btnLikeAllQuestion?.setTitle("\(/model.totalLikes) Likes",for: .normal)
                    }
                    else if (/model.totalLikes == 1 ) {
                        cell.btnLikeAllQuestion?.setTitle("\(/model.totalLikes) Like",for: .normal)
                    }
                    else  if /model.totalLikes == 0 {
                        cell.btnLikeAllQuestion?.setTitle("",for: .normal)
                    }
                    
                    cell.btnLikeQuestion?.setImage(R.image.liked(), for: UIControl.State.normal)
                    cell.btnLikeQuestion?.setTitleColor(self.selectedColor, for: .normal)
                    
                    self.arrayQuestions?[indexPath.row].isILike = 1
                    self.arrayQuestions?[indexPath.row].likes   = /model.totalLikes
                }
                else if QuestionType == 2
                {
                    let cell         = self.tableView?.cellForRow(at: indexPath) as! MyQuestionImageTableViewCell
                    
                    let myIntCheck = model.totalLikes
                    if /model.totalLikes > 1{
                        cell.btnLikeAllQuestionImage?.setTitle("\(/model.totalLikes) Likes",for: .normal)
                    }
                    else if (/model.totalLikes == 1 ) {
                        cell.btnLikeAllQuestionImage?.setTitle("\(/model.totalLikes) Like",for: .normal)
                    }
                    else  if /model.totalLikes == 0 {
                        cell.btnLikeAllQuestionImage?.setTitle("",for: .normal)
                    }
                    cell.btnLikeQuestionImage?.setImage(R.image.liked(), for: UIControl.State.normal)
                    cell.btnLikeQuestionImage?.setTitleColor(self.selectedColor, for: .normal)
                    
                    self.arrayQuestions?[indexPath.row].isILike = 1
                    self.arrayQuestions?[indexPath.row].likes   = myIntCheck
                }
            }
            else if model.status == "ok" && model.message == "Unliked"
            {
                // print("result = \(result)")
                // print("indexPath = \(indexPath)")
                if QuestionType == 1
                {
                    let cell       = self.tableView?.cellForRow(at: indexPath) as! MyQuestionNormalTableViewCell
                    if /model.totalLikes > 1{
                        cell.btnLikeAllQuestion?.setTitle("\(/model.totalLikes) Likes",for: .normal)
                    }
                    else if (model.totalLikes == 1 ) {
                        cell.btnLikeAllQuestion?.setTitle("\(/model.totalLikes) Like",for: .normal)
                    }
                    else  if model.totalLikes == 0 {
                        cell.btnLikeAllQuestion?.setTitle("",for: .normal)
                    }
                    
                    cell.btnLikeQuestion?.setImage(R.image.like(), for: UIControl.State.normal)
                    cell.btnLikeQuestion?.setTitleColor(self.unselectedColor, for: .normal)
                    
                    self.arrayQuestions?[indexPath.row].isILike = 0
                    self.arrayQuestions?[indexPath.row].likes   = model.totalLikes
                    
                    //print("cell.btnLikeAllQuestion.titleLabel?.text = \(cell.btnLikeAllQuestion.titleLabel?.text)")
                }
                else if QuestionType == 2
                {
                    let cell       = self.tableView?.cellForRow(at: indexPath) as! MyQuestionImageTableViewCell
                    if /model.totalLikes > 1{
                        cell.btnLikeAllQuestionImage?.setTitle("\(/model.totalLikes) Like",for: .normal)
                    }
                    else if (model.totalLikes == 1 ) {
                        cell.btnLikeAllQuestionImage?.setTitle("\(/model.totalLikes) Like",for: .normal)
                    }
                    else  if model.totalLikes == 0 {
                        cell.btnLikeAllQuestionImage?.setTitle("",for: .normal)
                    }
                    self.arrayQuestions?[indexPath.row].isILike = 0
                    self.arrayQuestions?[indexPath.row].likes   = model.totalLikes
                    
                    cell.btnLikeQuestionImage?.setImage(R.image.like(), for: UIControl.State.normal)
                    cell.btnLikeQuestionImage?.setTitleColor(self.unselectedColor, for: .normal)
                }
            }
            //            else
            //            {
            //                WebserviceManager.showAlert(message: "Something went wrong", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
            //                }, secondBtnBlock: {})
            //            }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView) {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        let y      = offset.y + bounds.size.height - inset.bottom
        let h      = size.height
        // let reloadDistance = CGFloat(30.0)
        if y > h && /arrayQuestions?.count > 0 //+ reloadDistance
        {
            spinner?.startAnimating()
            intCount = /intCount + 1
            //  self.loadingPastData(intPage: /intCount)
            webAPIQuestions(intPage: /intCount, intAll: /intAll)
        }
    }
    // MARK: - deleteRow
    func deleteRow()
    {
        self.arrayQuestions?.remove(at: deleteRowIndex.row)
        //  self.tableView?.deleteRows(at: [deleteRowIndex], with: .automatic)
        self.tableView?.setContentOffset(.zero, animated: false)
        // self.tblBoard?.reloadData()
        reloadTable()
    }
}
extension MyQuestionsViewController
{
    func configureTableView() {
        questionDataSource = QuestionTableDataSource(items: self.arrayQuestions, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: "")
        
        questionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath)
        }
        questionDataSource?.scrollViewEndDeaccerlate = {(scrollView) in
            self.reloadMoreData(scrollView)
        }
    }
    
    func didSelect(_ index : IndexPath){
        print("didSelect")
        tableView?.deselectRow(at: index, animated: true)
        intQID = self.arrayQuestions?[index.row].qID
        let intIsVerified   = self.arrayQuestions?[index.row].isVerified
        if intIsVerified != 0 && intIsVerified != 2 {
            guard let answerVc              = R.storyboard.main.answerViewController() else {return}
            answerVc.intQID       = /intQID
            self.pushVC(answerVc)
            self.hidesBottomBarWhenPushed = true
        }
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            questionDataSource?.items = self.arrayQuestions
            tableView?.reloadData()
        }
    }
}
extension MyQuestionsViewController: QuestionNormalDelegate,QuestionImageDelegate
{
    func ClkBtnLikeImageQuestion(btn: UIButton) {
        self.ClkLike(btn: btn)
    }
    func ClkBtnLikeQuestion(btn: UIButton) {
        self.ClkLike(btn: btn)
    }
    func ClkLike(btn: UIButton)
    {
        let intQType  = self.arrayQuestions?[btn.tag].qType
        let indexPath = IndexPath(row: btn.tag, section: 0)
        
        var intLikeDislike = Int()
        intQID             = self.arrayQuestions?[btn.tag].qID
        
        if btn.hasImage(named: "like", for: .normal) {
            intLikeDislike = 1
        } else {
            intLikeDislike = 0
        }
        //isILike
        APIManager.shared.request(with: HomeEndpoint.LikeQuestion(UserID: self.userID, UserType: self.userType, QID: intQID?.toString, isLike: intLikeDislike.toString), isLoader: false) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handleQuestionLike(response: success,indexPath: indexPath,sender: btn,QuestionType: intQType)
            })
        }
    }
    // MARK: - ClkbtnReportImagePressed
    func ClkbtnReportImagePressed(intTag: Int){
        
        intQID         = self.arrayQuestions?[intTag].qID
        deleteRowIndex = IndexPath(row: intTag, section: 0)
        
        var reportUserId         = Int()
        var reportUserType       = Int()
        if let reportUserId1 = self.arrayQuestions?[intTag].userID{
            reportUserId = reportUserId1
        }
        if let reportUserType1 = self.arrayQuestions?[intTag].userType{
            reportUserType = reportUserType1
        }
        if((intUserID == reportUserId) && (intUserType == reportUserType)){
            deleteOrReport = 0
        }
        else{
            deleteOrReport = 1
        }
        guard let reportVc       = R.storyboard.main.reportViewController() else {return}
        reportVc.deleteReport    = self.deleteOrReport
        reportVc.thoughtDelegate = self
        //reportViewController.BoardDelegate   = self
        reportVc.intQID          = /intQID
        reportVc.intQAThoughtReport = 2
        self.presentVC(reportVc)
    }
    // MARK: - ClkbtnProfileImagePressed
    func ClkbtnProfileImagePressed(intTag: Int){
        guard let shortProfileVc    = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID    = /self.arrayQuestions?[intTag].userID
        shortProfileVc.intUserType  = /self.arrayQuestions?[intTag].userType
        self.pushVC(shortProfileVc)
    }
    // MARK: - ClkbtnReportNormalPressed
    func ClkbtnReportNormalPressed(intTag: Int){
        intQID         = self.arrayQuestions?[intTag].qID
        deleteRowIndex = IndexPath(row: intTag, section: 0)
        
        var reportUserId         = Int()
        var reportUserType       = Int()
        
        if let reportUserId1 = self.arrayQuestions?[intTag].userID{
            reportUserId = reportUserId1
        }
        if let reportUserType1 = self.arrayQuestions?[intTag].userType{
            reportUserType = reportUserType1
        }
        if((intUserID == reportUserId) && (intUserType == reportUserType)){
            //delete
            print("Delete Question")
            deleteOrReport = 0
        }
        else{
            //report
            deleteOrReport = 1
        }
        guard let reportVc       = R.storyboard.main.reportViewController() else {return}
        reportVc.deleteReport    = self.deleteOrReport
        reportVc.thoughtDelegate = self
        //reportViewController.BoardDelegate   = self
        reportVc.intQID          = /intQID
        reportVc.intQAThoughtReport = 2
        presentVC(reportVc)
    }
    // MARK: - ClkbtnProfileNormalPressed
    func ClkbtnProfileNormalPressed(intTag: Int){
        guard let shortProfileVc   = R.storyboard.main.shortProfileViewController() else {return}
        shortProfileVc.intUserID   = /self.arrayQuestions?[intTag].userID
        shortProfileVc.intUserType = /self.arrayQuestions?[intTag].userType
        self.pushVC(shortProfileVc)
    }
}
// MARK: - refresh
// @objc func refresh(sender:AnyObject) {
//print("refresh pulled")
/// self.isPull2Refresh = true
///  self.isTopReload    = true
//intAll = segmentControl?.selectedSegmentIndex == 0 ? 1 : 0
//Comment 29 April 2019
// intCount  = 1
// self.webAPIQuestions(intPage: 1, intAll: /self.intAll)//segmentControl?.selectedSegmentIndex == 0 ? 1 : 0)
// }
