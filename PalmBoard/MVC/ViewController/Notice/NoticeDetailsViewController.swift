//  NoticeDetailsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class NoticeDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var tblNoticeDetails: UITableView?
    
    var dictNoticeDetails =  [String: Any]()
    
    var intNtID            = Int()
    var strSenderDetails   = String()
    var intUserIDDetails   = Int()
    var intUserTypeDetails = Int()
    
    var strFilePath        = String()
    var strPrefix          = String()
    var awsURL : String?
    var urlPDF: URL!
    var finalUrl = String()
    var strSize   = String()
    var strDouble = Double()
    var btnTitle       =  UIButton()
    var isRead:  Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblNoticeDetails?.rowHeight          = UITableView.automaticDimension
        tblNoticeDetails?.estimatedRowHeight = 2000
        tblNoticeDetails?.tableFooterView    = UIView(frame: CGRect.zero)
        
        tblNoticeDetails?.delegate   = nil
        tblNoticeDetails?.dataSource = nil
        
        self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
        // cellNoticeDetails
        // cellNoticePDF
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Notice", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnTitle(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        self.webAPINoticeDetails()
    }
    @objc func ClkBtnTitle(sender:UIButton) {
        self.tblNoticeDetails?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
       // self.title = "Notice"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    //MARK:- webAPINoticeDetails
    func webAPINoticeDetails()
    {
        print("webAPINotice")
        tblNoticeDetails?.isHidden    = true
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Notice/Dtl?SchCode=\(strSenderDetails)&key=\(urlString.keyPath())&UserID=\(intUserIDDetails)&UserType=\(intUserTypeDetails)&NtID=\(intNtID)"
        
        print("Func: urlString= \(urlString)")
        self.view.isUserInteractionEnabled = false
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        print("*** result = \(result)")
                        
                        
                        self.view.isUserInteractionEnabled = true
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            self.dictNoticeDetails = results?["Notice"] as! [String : Any]
                            print("dictNoticeDetails count = \(self.dictNoticeDetails.count)")
                            
                            if self.dictNoticeDetails.count == 0
                            {
                                self.tblNoticeDetails?.isHidden    = true
                                
                                let alert = UIAlertController(title: "", message: "Notice details is not found", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.tblNoticeDetails?.isHidden   = false
                                self.tblNoticeDetails?.delegate   = self
                                self.tblNoticeDetails?.dataSource = self
                                self.tblNoticeDetails?.reloadData()
                            }
                        }
                        
                        if let aws = result["AWS_Path"] as? String {
                            self.awsURL = aws
                        }
                    }
                    if (error != nil){
                        
                        print("userVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                self.tblNoticeDetails?.isHidden = false
                                
                                self.view.isUserInteractionEnabled = true
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var intHasAttachment = Int()
        if let intHasAttachment1 =  self.dictNoticeDetails["hasAttachment"] as? Int
        {
            intHasAttachment = intHasAttachment1
        }
        if  intHasAttachment == 1{
            return 2
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cellNoticeDetails", for: indexPath as IndexPath) as! CustomNoticeDetailsTableViewCell
            
            cell.lblHeading?.text = self.dictNoticeDetails["Heading"] as? String
            //cell.lblDate.text    = self.arrayNoticeDetails[0]["UpdatedOn"] as? String
//            cell.lblDetails.text = self.dictNoticeDetails["Detail"] as? String
            cell.txtViewDetails?.text = self.dictNoticeDetails["Detail"] as? String
            cell.txtViewDetails?.dataDetectorTypes = UIDataDetectorTypes.link // Update UITextView content textVw.text = yourstring // Update hyperlink text colour.
            cell.txtViewDetails?.linkTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.blue, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]

            var strDate              = String()
            strDate                 = self.dictNoticeDetails["NoticeDate"] as! String
            
            let formatter           = ISO8601DateFormatter()
            formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
            let date1               = formatter.date(from: strDate)
            strDate                 = formatter.string(from: date1!)
            
            let dateFormatterGet        = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "dd MMM, yyyy"
            
            let date: Date? = dateFormatterGet.date(from: strDate)
            
            if let updatedDate =  self.dictNoticeDetails["NoticeDate"] as? String {
                if dateFormatter.string(from: date!).count != 0  && updatedDate.getTimeIn12HourFormat().count != 0  {
                    cell.lblDate?.text    =  dateFormatter.string(from: date!) + " at " + updatedDate.getTimeIn12HourFormat()
                }
                else if dateFormatter.string(from: date!).count != 0  {
                    cell.lblDate?.text    =  dateFormatter.string(from: date!)
                }
                else {
                    cell.lblDate?.text    =  nil
                }
            }
            cell.imgViewClock?.image = (isRead == true ? R.image.clock() : R.image.clock_grey())
            return cell
        }
        else
        {
            let cellPDF =  tableView.dequeueReusableCell(withIdentifier: "cellNoticePDF", for: indexPath as IndexPath) as! CustomNoticeDetailsPDFTableViewCell
            
            strDouble =  self.dictNoticeDetails["FileSize"] as! Double
            
            let roundedValue2 = strDouble.roundToDecimal(2)
            print(roundedValue2)
            
            if roundedValue2 == 0.0 {
                strSize = "\(strDouble.rounded(toPlaces: 6)) kb"
            }
            else {
               strSize = "\(roundedValue2) mb"
            }
            cellPDF.lblPDFSize?.text = strSize
            cellPDF.btnView?.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
            
            return cellPDF
        }
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton)
    {
       // print("ClkBtnView")
        if let strFilePath1        = self.dictNoticeDetails["FilePath"] as? String
        {
            strFilePath = strFilePath1
            
            if self.awsURL?.trimmed().count == 0 {
                  strPrefix       = strPrefix.imgPath1()
            }
            else {
                strPrefix = /awsURL
            }
          
            finalUrl    = "\(strPrefix)" +  "\(strFilePath)"
            finalUrl = /finalUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            urlPDF        = URL(string: finalUrl)
            print("urlPDF= \(String(describing: urlPDF))")
            
            self.performSegue(withIdentifier: "segueNoticeWebview", sender: self)
        }
        else
        {
            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.fileIsNotCorrect(), fromVC: self)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblNoticeDetails?.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "segueNoticeWebview"
        {
            let webviewVC = segue.destination as! WebviewNoticeViewController
            webviewVC.strfinalUrl = finalUrl
            webviewVC.strTitle = "Notice"
        }
    }
    
}

