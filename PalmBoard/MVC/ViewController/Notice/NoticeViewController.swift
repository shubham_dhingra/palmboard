//  NoticeViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit

class NoticeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var imgViewNoNotice: UIImageView?
    @IBOutlet weak var lblNoNotice: UILabel?
    
    @IBOutlet weak var tblNotice:   UITableView?
    
    var arrayNotice = [[String: Any]]()
    var strSender   = String()
    var intUserID   = Int()
    var intUserType = Int()
    var intPage     = 1
    var intNtID1    = Int()
    var awsURL      : String?
    
    var spinner        : UIActivityIndicatorView?
    var refreshControl =  UIRefreshControl()
    var totalNotice = Int()
    var btnTitle       =  UIButton()
    var boolRead: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner?.color        =  UIColor(rgb: 0x56B54B)
        spinner?.stopAnimating()
        spinner?.hidesWhenStopped = true
        
        spinner?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        tblNotice?.rowHeight          = UITableView.automaticDimension
        tblNotice?.estimatedRowHeight = 2000
        tblNotice?.tableFooterView    = UIView(frame: CGRect.zero)
        
        refreshControl = UIRefreshControl()
        // refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tblNotice?.addSubview(refreshControl)
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    @objc func ClkBtnView(sender:UIButton) {
        self.tblNotice?.setContentOffset(CGPoint(x:0,y:10), animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        
        navigationItem.hidesBackButton = true
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.tblNotice?.setContentOffset(CGPoint(x:0,y:10), animated: true)
        
        imgViewNoNotice?.isHidden = true
        lblNoNotice?.isHidden     = true
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("All Notices", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        
        self.navigationItem.titleView = btnTitle
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        self.webAPINotice(intPagination: 1)
    }
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        
        print("refresh pulled")
        intPage     = 1
        self.webAPINotice(intPagination: 1)
    }
    
    //MARK:- webAPINotice
    func webAPINotice(intPagination: Int)
    {
        print("webAPINotice")
        tblNotice?.isHidden    = true
        Utility.shared.loader()
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Notice/All?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"
        
        print("Func: urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        //print("*** result = \(result)")
                        
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if let awsURL = result["AWS_Path"] as? String {
                                self.awsURL = awsURL
                            }
                            if let totalNotice = result["TotalNotice"] as? Int {
                                self.totalNotice = totalNotice
                            }
                            self.arrayNotice = results?["Notices"] as! [[String : Any]]
                            print("arrayNotice count = \(self.arrayNotice.count)")
                            
                            if self.arrayNotice.count == 0
                            {
                                self.tblNotice?.isHidden       = true
                                self.imgViewNoNotice?.isHidden = false
                                self.lblNoNotice?.isHidden     = false
                            }
                            else
                            {
                                var intTotalNotice  = Int()
                                var intUnreadNotice = Int()
                                
                                intTotalNotice      = results?["TotalNotice"] as! Int
                                intUnreadNotice     = results?["UnreadNotice"] as! Int
                                
                                // self.title = "All Notices (\("\(intTotalNotice)/\(intUnreadNotice)"))"
                                self.title = "All Notices (\("\(intUnreadNotice)/\(intTotalNotice)"))"
                                
                                self.btnTitle.setTitle(self.title,for: .normal)
                                
                                self.tblNotice?.isHidden   = false
                                self.tblNotice?.delegate   = self
                                self.tblNotice?.dataSource = self
                                self.tblNotice?.reloadData()
                                
                                self.refreshControl.endRefreshing()
                            }
                            self.spinner?.stopAnimating()
                        }
                    }
                    if (error != nil){
                        
                        print("userVerifyAPI = \(error!)")
                        DispatchQueue.main.async
                            {
                                self.tblNotice?.isHidden = false
                                
                                switch (errorcode!){
                                case Int(-1009):
                                    
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            if self.arrayNotice.count == self.totalNotice {
                print("All data already loaded")
                return
            }
            
            print("fetch more data -- scrollViewDidEndDragging")
            spinner?.startAnimating()
            
            intPage = intPage + 1
            //self.webAPIQuestions(intAll: intAllMyCount, intPage: intCount)
            self.loadChunkDataAPINotice(intPagination: intPage)
        }
    }
    
    //MARK:- loadChunkDataAPINotice
    func loadChunkDataAPINotice(intPagination: Int)
    {
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Notice/All?SchCode=\(strSender)&key=\(urlString.keyPath())&UserID=\(intUserID)&UserType=\(intUserType)&pg=\(intPagination)"
        
        print("Func:loadingPastData urlString= \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        self.spinner?.stopAnimating()
                        
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            let tempArray = results?["Notices"] as! [[String : Any]]
                            if tempArray.count == 0
                            {
                                //                                let alert = UIAlertController(title: "", message: "No more Notice", preferredStyle: UIAlertControllerStyle.alert)
                                //                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                //                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                var intTotalNotice  = Int()
                                var intUnreadNotice = Int()
                                
                                intTotalNotice      = results?["TotalNotice"] as! Int
                                intUnreadNotice     = results?["UnreadNotice"] as! Int
                                
                                //self.title = "All Notices (\("\(intTotalNotice)/\(intUnreadNotice)"))"
                                self.title = "All Notices (\("\(intUnreadNotice)/\(intTotalNotice)"))"
                                
                                self.btnTitle.setTitle(self.title,for: .normal)
                                
                                
                                print("Before arrayNotice count = \(self.arrayNotice.count)")
                                self.arrayNotice.append(contentsOf: results?["Notices"] as! [[String : Any]])
                                print("after arrayNotice count = \(self.arrayNotice.count)")
                                self.tblNotice?.reloadData()
                            }
                        }
                    }
                    if (error != nil){
                        DispatchQueue.main.async{
                            self.spinner?.stopAnimating()
                            switch (errorcode!){
                            case Int(-1009):
                                Utility.shared.internetConnectionAlert()
                            default:
                                print("errorcode = \(String(describing: errorcode))")
                            }
                        }
                    }
            }
        }
    }
    //MARK:- TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrayNotice.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellNotice", for: indexPath as IndexPath) as! CustomNoticeTableViewCell
        cell.lblHeading?.text = self.arrayNotice[indexPath.row]["Heading"] as? String
        // cell.lblDate.text    = self.arrayNotice[indexPath.row]["UpdatedOn"] as? String
        
        var intCheckNew          = Int()
        var intColorCell         = Int()
        var intHasAttachment     = Int()
        // var strDetail            = String()
        var strDate              = String()
        strDate                 = self.arrayNotice[indexPath.row]["NoticeDate"] as! String
        
        let formatter           = ISO8601DateFormatter()
        formatter.formatOptions = [.withFullDate,.withDashSeparatorInDate]
        let date1               = formatter.date(from: strDate)
        strDate                 = formatter.string(from: date1!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        let date: Date? = dateFormatterGet.date(from: strDate)
        if let updatedDate =  self.arrayNotice[indexPath.row]["NoticeDate"] as? String {
            if dateFormatter.string(from: date!).count != 0  && updatedDate.getTimeIn12HourFormat().count != 0  {
                cell.lblDate?.text    =  dateFormatter.string(from: date!) + " at " + updatedDate.getTimeIn12HourFormat()
            }
            else if dateFormatter.string(from: date!).count != 0  {
                cell.lblDate?.text    =  dateFormatter.string(from: date!)
            }
            else {
                cell.lblDate?.text    =  nil
            }
        }
        intCheckNew      = self.arrayNotice[indexPath.row]["isNew"]     as! Int
        intColorCell     = self.arrayNotice[indexPath.row]["isRead"]        as! Int
        intHasAttachment = self.arrayNotice[indexPath.row]["hasAttachment"] as! Int
        
        switch intCheckNew {
        case 0:
            cell.lblNew?.isHidden = true
        case 1:
            cell.lblNew?.isHidden = false
        default:
            break
        }
        switch intColorCell {
        case 0:
            cell.backgroundColor = UIColor(rgb: 0xEEEEEE)
            cell.imgViewClock?.image = R.image.clock()
        case 1:
            cell.backgroundColor = UIColor.white
            cell.imgViewClock?.image = R.image.clock_grey()
            
        default:
            break
        }
        switch intHasAttachment {
        case 0:
            cell.widthImgViewPDF?.constant = 0
            cell.imgViewPDF?.isHidden      = true
        case 1:
            cell.widthImgViewPDF?.constant = 18
            cell.imgViewPDF?.isHidden     = false
        default:
            break
        }
        boolRead = intColorCell == 1 ? true : false
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblNotice?.deselectRow(at: indexPath, animated: true)
        intNtID1 = self.arrayNotice[indexPath.row]["NtID"] as! Int
        performSegue(withIdentifier: "SegueNoticDetails", sender: self)
    }
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {}
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {}
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "SegueNoticDetails"
        {
            let noticeDetails                = segue.destination as! NoticeDetailsViewController
            noticeDetails.intNtID            = intNtID1
            noticeDetails.awsURL             = awsURL
            noticeDetails.strSenderDetails   = strSender
            noticeDetails.intUserIDDetails   = intUserID
            noticeDetails.intUserTypeDetails = intUserType
            noticeDetails.isRead             = boolRead
            
        }
    }
}

