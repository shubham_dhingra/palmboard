//  WebviewNoticeViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 15/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
import UIKit
class WebviewNoticeViewController: UIViewController,UIWebViewDelegate
{
    var strfinalUrl =  String()
    var btnTitle    =  UIButton()
    @IBOutlet var webView: UIWebView?
    var urlPath: URL?
    var strTitle: String? = nil

    // MARK: - viewDidLoad
    override func viewDidLoad(){
        super.viewDidLoad()
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle(strTitle, for: UIControl.State.normal)
        btnTitle.titleLabel?.font = R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        webView?.scalesPageToFit = true
        self.navigationItem.titleView = btnTitle
    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        webView?.delegate = self
        webView?.backgroundColor = UIColor.clear
        if let url = URL(string: self.strfinalUrl){
            let request = URLRequest(url: url)
            webView?.loadRequest(request)
        }
        else
        {
            // print ("Data is not here")
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            // alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
                //let fields = alert.textFields!;
                //print("Yes we can: ")
                
                _ = self.navigationController?.popViewController(animated: true)
                
            }));
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
        savePdf(urlString: strfinalUrl, fileName: "notice")
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()
        let alert = UIAlertController(title: "", message: "Can't Connect. Please check your internet Connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            
            _ = self.navigationController?.popViewController(animated: true)
            
        }));
        self.present(alert, animated: true, completion: nil)
    }
    // MARK: - savePdf
    func savePdf(urlString:String, fileName:String) {
        DispatchQueue.main.async {
            let url = URL(string: urlString)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "Notice-\(fileName).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                self.urlPath = actualPath
                try pdfData?.write(to: actualPath, options: .atomic)
                print("pdf successfully saved!")
            } catch {
                print("Pdf could not be saved")
            }
        }
    }
     // MARK: - sharePdf
    func sharePdf(path:URL) {
        
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: path.path) {
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [path], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            print("document was not found")
            let alertController = UIAlertController(title: "Error", message: "Document was not found!", preferredStyle: .alert)
            let defaultAction = UIAlertAction.init(title: "ok", style: UIAlertAction.Style.default, handler: nil)
            alertController.addAction(defaultAction)
          //  UIViewController.hk_currentViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    // MARK: - ClkShare
    @IBAction func ClkShare(_ sender: Any) {
        sharePdf(path: urlPath!)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
