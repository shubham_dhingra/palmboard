//  PaySlipViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 03/04/19.
//  Copyright © 2019 Franciscan. All rights reserved.
import UIKit
import EZSwiftExtensions
import WebKit

protocol HideBarDelegatePaySlip: class{
    func hideBar(boolHide: Bool)
}

class PaySlipViewController: BaseViewController {
    
    @IBOutlet weak var webKit: WKWebView!
    @IBOutlet weak var txtYear :  UITextField?
    @IBOutlet weak var txtMonth : UITextField?
    @IBOutlet weak var imgViewNo: UIImageView?
    @IBOutlet weak var lblData: UILabel?
    weak var hideBarDelegate: HideBarDelegatePaySlip?
    
    var intTag  : Int?
    var arrayMonth   :[String]?
    var arrayYear     :[String]?
    var arrayFilePath: [String]?
    var paySlipYear : [Year]?
    var selectYearIndex : Int = -1
    var selectMonthIndex : Int = -1
    var pickerView : UIPickerView!
    var strFile      : String?
    var arrayMonthlyPaySlip  = [MonthlyPaySlip]()
    var currentTextField : UITextField?
    
    // MARK: -  viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation(vc: self , title : "Pay Slip")
        getYearAPI()
    }
    
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideBarDelegate?.hideBar(boolHide: false)
        lblData?.isHidden = true
        imgViewNo?.isHidden = true
    }
    
    //MARK:- getYearAPI
    func getYearAPI()
    {
        APIManager.shared.request(with: HomeEndpoint.Payslip(UserID: self.userID), isLoader: true) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response : success)
            })
        }
    }
    
    
    // MARK: -  handle
    func handle(response : Any?){
        if let modal = response as? PaySlipModal, let years = modal.years {
            
            self.paySlipYear = years
            if years.count > 0 {
                //1)
                arrayYear = years.map({ (value) -> String in
                    return (/value.year).toString
                })
                //2)
                arrayYear?.insert(R.string.localize.selectYear(), at: 0)
                txtYear?.text = /arrayYear?.first
                txtMonth?.text = R.string.localize.selectMonth()
            }
            webKit?.isHidden = years.count != 0
        }
    }
}

extension PaySlipViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtYear {
            self.currentTextField = txtYear
            self.pickUp(textField)
        }
        
        if textField == txtMonth {
            if txtYear?.text == R.string.localize.selectYear() {
                self.txtMonth?.resignFirstResponder()
                self.arrayMonth = []
                self.arrayFilePath = []
                Messages.shared.show(alert: .oops, message: "Please select year", type: .warning)
                return
            }
            else {
                //first index of Select Year
                if selectYearIndex != -1 {
                    self.arrayMonth = self.paySlipYear?[selectYearIndex].monthlyPaySlip?.map({ (monthPaySlip) -> String in
                        return /monthPaySlip.month
                    })
                    self.arrayFilePath = self.paySlipYear?[selectYearIndex].monthlyPaySlip?.map({ (monthPaySlip) -> String in
                        return /monthPaySlip.filePath
                    })
                   self.arrayMonth?.insert(R.string.localize.selectMonth(), at: 0)
                   self.arrayFilePath?.insert("", at: 0)
                    if /self.arrayMonth?.count != 0 {
                        self.currentTextField = txtMonth
                        self.pickUp(textField)
                    }
                }
            }
        }
    }
    
    
    
    
}

extension PaySlipViewController : UIPickerViewDelegate , UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if currentTextField == txtYear {
            return /arrayYear?.count
        }
        if currentTextField == txtMonth {
            return /arrayMonth?.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if currentTextField == txtYear {
            return /arrayYear?[row]
        }
        if currentTextField == txtMonth {
            return /arrayMonth?[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if currentTextField == txtYear {
            selectYearIndex = row - 1
            txtYear?.text = self.arrayYear?[row]
            if row == 0 {
                txtMonth?.text = R.string.localize.selectMonth()
                selectMonthIndex = -1
            }
            
        }
        if currentTextField == txtMonth {
            txtMonth?.text = self.arrayMonth?[row]
            selectMonthIndex = row - 1
            if row != 0 {
                 self.strFile = self.arrayFilePath?[row]
            }
        }
    }
    
    @objc func doneClick() {
        webAPIPaySlip()
        actionPerform()
    }
    
    @objc func cancelClick() {
        actionPerform()
    }
    
    func actionPerform() {
        self.currentTextField?.resignFirstResponder()
    }
}

//MARK::- PickerView Creation
extension PaySlipViewController {
   
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.pickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.selectRow(currentTextField == txtYear  ? (selectYearIndex + 1) : (selectMonthIndex + 1) , inComponent: 0, animated: false)
        self.pickerView.backgroundColor = UIColor.flatGray
        textField.inputView = self.pickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.flatGreen
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: AlertConstants.Done.get, style: .plain, target: self, action: #selector(self.doneClick))
        doneButton.tintColor = UIColor.flatGreen
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        cancelButton.tintColor = UIColor.flatGreen
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
}


extension PaySlipViewController: WKNavigationDelegate
{
    //: UIWebViewDelegate{
    func webAPIPaySlip() {
        imgViewNo?.isHidden                = true
        lblData?.isHidden                  = true
        
        if txtYear?.text == R.string.localize.selectYear() || txtMonth?.text == R.string.localize.selectMonth(){
            imgViewNo?.isHidden = false
            lblData?.isHidden = false
            webKit.isHidden = true
            strFile = ""
            return
        }
        
        if let url = URL(string: /strFile){
            webKit.navigationDelegate = self
            Utility.shared.loader()
            webKit.load(URLRequest(url: url))
            self.webKit.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
        }
        else
        {
            imgViewNo?.isHidden                = false
            lblData?.isHidden                  = false
        }
    }
    
    // MARK: - webView method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "loading" {
            if webKit.isLoading {
                Utility.shared.loader()
            } else {
                Utility.shared.removeLoader()
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utility.shared.removeLoader()
    }
    
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Utility.shared.removeLoader()
        webKit.isHidden = true
        imgViewNo?.isHidden = false
        lblData?.isHidden = false
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        // get the statuscode
        guard let statusCode = (navigationResponse.response as? HTTPURLResponse)?.statusCode
            else {
                decisionHandler(.allow)
                return
        }
        // respond or pass-through however you like
        switch statusCode {
        case 200:
            webKit.isHidden = false
        default:
            self.imgViewNo?.isHidden      = false
            self.lblData?.isHidden      = false
            self.webKit?.isHidden         = true
        }
        decisionHandler(.allow)
    }
}


