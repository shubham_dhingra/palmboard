//  PaymentViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 21/12/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions


protocol HideBarDelegatePayment: class{
    func hideBar(boolHide: Bool)
}


class PaymentViewController: BaseViewController,UIWebViewDelegate,UIScrollViewDelegate {
    weak var hideBarDelegate: HideBarDelegatePayment?
    
    @IBOutlet weak var webViewWebsite: UIWebView?
    var strWebsite = String()
    var strToken: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        self.title = "Fee"
        webViewWebsite?.scrollView.delegate = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
//        if let baseUrl = self.getCurrentSchool()?.paymentUrl , let user = self.getCurrentUser(), let username = user.username , let password = user.password, let schCode = user.schCode{
//            strWebsite =  baseUrl + "?usr=\(username)&psw=\(password)&schCode=\(schCode)"
//        }
        
       // print("strWebsite = \(strWebsite)")
        //self.webAPIWebsite()
        getTokenAPI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // HideBarDelegateSyllabus?.hideBar(boolHide: false)
        hideBarDelegate?.hideBar(boolHide: false)
    }
    //MARK:- getTokenAPI
    func getTokenAPI()
    {
        
        APIManager.shared.request(with: HomeEndpoint.GenerateToken(schoolCode: self.getCurrentSchool()?.schCode, username: self.getCurrentUser()?.username, password: self.getCurrentUser()?.password), isLoader: true) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response : success)
            })
        }
    }
    func handle(response : Any?){
        if let modal = response as? SettingModal {
            strToken = modal.tokenKey
            
            if let baseUrl = self.getCurrentSchool()?.paymentUrl {
                strWebsite =  baseUrl + "?token=\(/strToken)"
               // http://payment.franciscanepay.com/mlogin.aspx?token=
                 print("strWebsite = \(strWebsite)")
                self.webAPIWebsite()
            }
    }
    }
    //MARK:- webAPIWebsite
    func webAPIWebsite()
    {
        self.webViewWebsite?.isHidden        = false
        self.webViewWebsite?.delegate        = self
        self.webViewWebsite?.backgroundColor = UIColor.clear
        if let url = URL(string: self.strWebsite){
            let request = URLRequest(url: url)
            self.webViewWebsite?.loadRequest(request)
        }
        else
        {
            let alert = UIAlertController(title: "", message: "URL is not correct", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            }));
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        Utility.shared.removeLoader()
        print("strWebsite = \(strWebsite)")

        let alert = UIAlertController(title: "", message: "Can't Connect. Please check your internet Connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
        }));
        self.present(alert, animated: true, completion: nil)
    }
    /*
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
            parent.hideBar(boolHide: true)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: false)
            }
        }
        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
    }
    */
}
