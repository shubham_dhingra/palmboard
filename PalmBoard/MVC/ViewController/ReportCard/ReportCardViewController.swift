//  ReportCardViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
import UIKit

class ReportCardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UIActionSheetDelegate,UIScrollViewDelegate
{
    @IBOutlet weak var scrollViewReportCard:     UIScrollView?
    @IBOutlet weak var collectionViewReportCard: UICollectionView?
    @IBOutlet weak var tblviewReportCard:        UITableView?
    @IBOutlet weak var imgViewNoReportCard:      UIImageView?
    @IBOutlet weak var lblNoReportCard:          UILabel?
    
    @IBOutlet weak var barBtnView:               UIView?
    @IBOutlet weak var btnView:                  UIButton?
    @IBOutlet weak var leftBackBarBtnItem:       UIBarButtonItem?
    @IBOutlet weak var btnTitle:                 UIButton?

    var strSchoolCodeSender     = String()
    var strYears                = String()
    var strYearsBracket         = String()
    var strURLPdf               = String()
    
    var intUserID               = Int()
    var intCheckStaff           = Int()
    var USERTYPE                = Int()
    var strClass                = String()
    var strName                 = String()
    var strPhoto                = String()
    
    var arrayReportCard       = [[String: Any]]()
    var arrayReportCardAll    = [[String: Any]]()
    // var btnTitle                    =  UIButton()
    var themeColor              = String()
    var imageView               = UIImageView()
    var eventIndex   : Int = 0
    

    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isNavBarHidden = false
        self.view.isUserInteractionEnabled = true

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if let themeColor = self.getCurrentSchool()?.themColor {
            self.themeColor = themeColor
        }
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        self.tblviewReportCard?.delegate               = nil
        self.tblviewReportCard?.dataSource           = nil
        self.collectionViewReportCard?.delegate     = nil
        self.collectionViewReportCard?.dataSource = nil
        self.tblviewReportCard?.tableFooterView    = UIView(frame: CGRect.zero)
        self.webApiReportCard()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        setUpBarBtn()
        //navigationItem.hidesBackButton = true
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        if strPhoto.trimmed().count != 0 {
            
            imageView.af_setImage(withURL: strPhoto.getImageUrl(), placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                
                let img1 = self.imageView.image?.af_imageRoundedIntoCircle()
                self.btnView?.setBackgroundImage(img1, for: .normal)
            }
            btnView?.layer.cornerRadius = 16
        }
        
        if intCheckStaff == 11{
            btnTitle?.setTitle(strName+", "+strClass, for: UIControl.State.normal)
            btnTitle?.contentHorizontalAlignment = .left
            navigationItem.hidesBackButton = false
        }
        else{
            if let schoolCode = self.getCurrentSchool()?.schCode {
                self.strSchoolCodeSender =  schoolCode
            }
            btnTitle?.setTitle("Report Card", for: UIControl.State.normal)
            btnTitle?.contentHorizontalAlignment = .right

            navigationItem.hidesBackButton = true
        }
        self.imgViewNoReportCard?.isHidden = true
        self.lblNoReportCard?.isHidden     = true
    }
    
    
    func setUpBarBtn(){
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.normalTap))
        tap.numberOfTapsRequired  = 1
        self.btnView?.isUserInteractionEnabled = true
        self.btnView?.addGestureRecognizer(tap)
        if let view = self.btnView {
            self.view.bringSubviewToFront(view)
        }
    }
    
    @objc func normalTap(){
       
        guard let VCProfile = R.storyboard.main.studentProfileViewController() else {
            return
        }
        VCProfile.strUserType      = USERTYPE.toString
        VCProfile.strUserID        = intUserID.toString
        VCProfile.selectedIndex    =  0
        self.pushVC(VCProfile)
    }
    
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let subViews =   self.navigationController?.navigationBar.subviews else {return}
        for views in subViews {
            if views.tag == 3 || views.tag == 4{
                views.removeFromSuperview()
            }
        }
    }
    
    func webApiReportCard()
    {
        self.tblviewReportCard?.isHidden        = true
        self.collectionViewReportCard?.isHidden = true
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "ReportCard/Dtl?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(intUserID)"
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        Utility.shared.removeLoader()
                        
                        
                        if let arrayAll = (result["Classes"] as? [[String: Any]])  {
                            
                            self.arrayReportCard = arrayAll
                            
                            if self.arrayReportCard.count != 0 {
                                
                                if (self.arrayReportCard[0]["ReportCards"] as? [[String : Any]]) == nil{
                                    self.imgViewNoReportCard?.isHidden = false
                                    self.lblNoReportCard?.isHidden     = false
                                    self.collectionViewReportCard?.isHidden = false
                                }
                            }
                            print("arrayReportCard == \(self.arrayReportCard.count)")
                        }
                        
                        if self.arrayReportCard.count == 0
                        {
                            self.imgViewNoReportCard?.isHidden = false
                            self.lblNoReportCard?.isHidden     = false
                            self.collectionViewReportCard?.isHidden = false
                        }
                        else
                        {
                            self.collectionViewReportCard?.delegate   = self
                            self.collectionViewReportCard?.dataSource = self
                            self.collectionViewReportCard?.reloadData()
                            self.collectionViewReportCard?.isHidden = false
                            
                            self.collectionViewReportCard?.performBatchUpdates({},
                                                                               completion: { (finished) in
                                                                                print("collection-view finished reload done")
                                                                                
                                                                                self.tblviewReportCard?.delegate   = self
                                                                                self.tblviewReportCard?.dataSource = self
                                                                                self.tblviewReportCard?.isHidden = false
                                                                                self.collectionViewReportCard?.collectionViewLayout.invalidateLayout()
                                                                                
                                                                                let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                                                                                self.collectionViewReportCard?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                                                                //  if let theLabel = self.view.viewWithTag( 1) as? UILabel {
                                                                                //theLabel.backgroundColor = UIColor(rgb: 0xFFC026)
                                                                                //  }
                                                                                self.collectionView(self.collectionViewReportCard!, didSelectItemAt: IndexPath(item: 0, section: 0))
                                                                                //self.heightCollection.constant = 45
                            })
                            
                            
                        }
                        print("arrayReportCard = \(self.arrayReportCard)")
                    }
                    
                }
            }
            if (error != nil){
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                    if let code = errorCode {
                        switch (code){
                        case Int(-1009):
                            Utility.shared.internetConnectionAlert()
                        default:
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                            print("errorcode = \(String(describing: code))")
                        }
                    }
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayReportCard.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellReportCard", for: indexPath) as! customReportCardCollectionViewCell
        
        cellCollection.lblClassName?.text = arrayReportCard[indexPath.row]["ClassName"] as? String
        cellCollection.lblClassYear?.text = arrayReportCard[indexPath.row]["AcademicYear"] as? String
        
        if eventIndex == indexPath.row {
            cellCollection.lblBottomLine?.backgroundColor = themeColor.hexStringToUIColor()
            cellCollection.lblClassName?.textColor  = themeColor.hexStringToUIColor()
            cellCollection.lblClassYear?.textColor  = themeColor.hexStringToUIColor()
        }else {
            cellCollection.lblBottomLine?.backgroundColor = UIColor.clear
            cellCollection.lblClassName?.textColor  = UIColor(rgb: 0xD5D5D5)
            cellCollection.lblClassYear?.textColor  = UIColor(rgb: 0xD5D5D5)
        }
        
        return cellCollection
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Did select row= \(indexPath.row)")
        
        
        if let array = arrayReportCard[indexPath.row]["ReportCards"] as? [[String: Any]]
        {
            self.arrayReportCardAll = array
            if let strYears1 = arrayReportCard[indexPath.row]["AcademicYear"] as? String
            {
                strYears         = strYears1
                strYearsBracket  = "("
                strYearsBracket += strYears1
                strYearsBracket += ")"
            }
            /*******p code*/
            
            collectionViewReportCard?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewReportCard?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            
            /*********/
            self.tblviewReportCard?.delegate   = self
            self.tblviewReportCard?.dataSource = self
            
            self.tblviewReportCard?.reloadData()
            self.tblviewReportCard?.isHidden = false
        }
        else{
            collectionViewReportCard?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewReportCard?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            tblviewReportCard?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayReportCardAll.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellReportCard", for: indexPath as IndexPath) as! customReportCardTableViewCell
        
        cell.lblReportCardName?.text = arrayReportCardAll[indexPath.row]["ExamName"] as? String
        cell.lblReportYear?.text     = strYearsBracket//strYears
        // cell.lblDate.text           = arrayReportCardAll[indexPath.row]["UpdatedOn"] as? String
        
        var strUploadedOn           = String()
        
        strUploadedOn               = arrayReportCardAll[indexPath.row]["UpdatedOn"] as! String
        
        let formatter               = ISO8601DateFormatter()
        formatter.formatOptions     = [.withFullDate,.withDashSeparatorInDate]
        
        let dateUploadedOn          = formatter.date(from: strUploadedOn)
        strUploadedOn               = formatter.string(from: dateUploadedOn!)
        
        let dateFormatterGet        = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd MMM, yyyy"
        
        let dateUploadedOn1: Date?  = dateFormatterGet.date(from: strUploadedOn)
        cell.lblDate?.text          = "\(dateFormatter.string(from: dateUploadedOn1!))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblviewReportCard?.deselectRow(at: indexPath, animated: true)
        
        var strFrontReportCard          = String()
        var strBackReportCard           = String()
        
        var intViewMode = Int()
        intViewMode     = arrayReportCardAll[indexPath.row]["ViewMode"] as! Int
        
        var strPrefix   = String()
        strPrefix       += strPrefix.imgPath1()
        strPrefix       += "SchImg/"
        strPrefix       += strSchoolCodeSender
        strPrefix       += "/ReportCard/"
        
        if intViewMode == 1
        {
            if let strFileName = arrayReportCardAll[indexPath.row]["FileName"] as? String{
                //print("***strFileName = \(strFileName.count) ")
                strPrefix += strFileName
                self.strURLPdf = strPrefix
                self.performSegue(withIdentifier: "pdfReportCard", sender: self)
            }
        }
        else if intViewMode == 2
        {
            if let strFrontReportCard1 = arrayReportCardAll[indexPath.row]["FrontFileName"] as? String{
                strFrontReportCard = strFrontReportCard1
            }
            if let strBackReportCard1 = arrayReportCardAll[indexPath.row]["BackFileName"] as? String{
                strBackReportCard = strBackReportCard1
            }
            //   print("strFrontReportCard = \(strFrontReportCard.count) and strBackReportCard = \(strBackReportCard.count)")
            
            let alert = UIAlertController(title: "Report Card", message: "Please Select an Option", preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Front Report Card", style: .default , handler:{ (UIAlertAction)in
                print("User click Front Report Card button")
                strPrefix += strFrontReportCard
                self.strURLPdf = strPrefix
                self.performSegue(withIdentifier: "pdfReportCard", sender: self)
            }))
            alert.addAction(UIAlertAction(title: "Back Report Card", style: .default , handler:{ (UIAlertAction)in
                print("User click Back Report Card button")
                strPrefix += strBackReportCard
                self.strURLPdf = strPrefix
                self.performSegue(withIdentifier: "pdfReportCard", sender: self)
            }))
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.cancel, handler:{ (UIAlertAction)in
                // print("User click Dismiss button")
            }))
            self.present(alert, animated: true, completion: nil
                //            self.present(alert, animated: true, completion: {
                //                print("completion block")
                //            }
            )
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pdfReportCard"
        {
            let webviewVC             = segue.destination as! WebviewReportCardViewController
            webviewVC.strfinalUrl     = self.strURLPdf
        }
    }
}

