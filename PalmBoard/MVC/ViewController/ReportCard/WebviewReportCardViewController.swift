
import UIKit

class WebviewReportCardViewController: UIViewController,UIWebViewDelegate
{
    @IBOutlet var webView: UIWebView?
    var btnTitle       =  UIButton()
    var strfinalUrl    =  String()
    var module : Module?
  
    override func viewDidLoad()  {
        super.viewDidLoad()
        
        if module != .OnlineAssessmentMarks {
            self.navigationController?.navigationBar.tintColor = UIColor(rgb: 0x545454)
            btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
            btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
            btnTitle.setTitle("Report Card", for: UIControl.State.normal)
            btnTitle.titleLabel?.font = R.font.ubuntuMedium(size: 17)
            btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
            self.navigationItem.titleView = btnTitle
        }
            
        
            
            
        else {
            self.isNavBarHidden = module == .OnlineAssessmentMarks
        }
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        Utility.shared.loader()
        webView?.delegate = self
        webView?.backgroundColor = UIColor.clear
        
        if module == .OnlineAssessmentMarks {
            if let baseUrl = self.getCurrentSchool()?.assessmentMarksURL , let userID = self.getCurrentUser()?.userID {
                strfinalUrl =  baseUrl + "?uid=\(userID)"
                print("strWebsite = \(strfinalUrl)")
                if let url = URL(string: /self.strfinalUrl.replaceSpaceWithPercent20()){
                    let request = URLRequest(url: url)
                    webView?.loadRequest(request)
                }
                else{
                    Utility.shared.removeLoader()
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: "Something went wrong", fromVC:  self)
                }
            }
        }
        else {
            if let url = URL(string: /self.strfinalUrl.replaceSpaceWithPercent20()){
                let request = URLRequest(url: url)
                webView?.loadRequest(request)
            }
            else{
                Utility.shared.removeLoader()
                AlertsClass.shared.showNativeAlert(withTitle: "", message: "Something went wrong", fromVC:  self)
            }
        }
    }
    
    
    // MARK: - webView method
    func webViewDidStartLoad(_ webView: UIWebView){
        Utility.shared.loader()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)  {
        Utility.shared.removeLoader()
        AlertsClass.shared.showNativeAlert(withTitle: "", message: "Something went wrong", fromVC:  self)
    }
}


