//
//  customReportCardCollectionViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 18/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class customReportCardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblClassName: UILabel?
    @IBOutlet weak var lblClassYear: UILabel?
    @IBOutlet weak var lblBottomLine: UILabel?
    
}
