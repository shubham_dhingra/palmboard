//
//  customReportCardTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 18/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class customReportCardTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel?
    @IBOutlet weak var lblReportYear: UILabel?
    @IBOutlet weak var lblReportCardName: UILabel?
    @IBOutlet weak var imgViewReportCard: UIImageView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
