//  AppUserReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 04/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import Charts

class AppUserReportViewController: UIViewController {

    @IBOutlet weak var pieChart_View: PieChartView?
    
    @IBOutlet weak var lblCircularAndroid: UILabel?
    @IBOutlet weak var lblCircularAniOS:   UILabel?
    
    @IBOutlet weak var lblAndroidPer: UILabel?
    @IBOutlet weak var lbliOSPer:     UILabel?

    @IBOutlet weak var lblAndroidCount: UILabel?
    @IBOutlet weak var lbliOSCount:     UILabel?

    var intAndroidUser = Int()
    var intiOSUser     = Int()
    var intTotalUser   = Int()

    var btnTitle      = UIButton()
    var strSender     = String()

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // guard let themeColor = self.getCurrentSchool()?.themColor else { return}

        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSender =  schoolCode
        }

        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("App User Report", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        self.navigationItem.titleView = btnTitle

        lblCircularAndroid?.layer.cornerRadius = (lblCircularAndroid?.frame.width)! / 2
        lblCircularAndroid?.clipsToBounds      = true
        lblCircularAniOS?.layer.cornerRadius   = (lblCircularAniOS?.frame.width)! / 2
        lblCircularAniOS?.clipsToBounds        = true
       //lblCircularWindow.layer.cornerRadius  = lblCircularWindow.frame.width / 2
       //lblCircularWindow.clipsToBounds       = true
        
        self.webApiAppUserReport()
    }
    func webApiAppUserReport()
    {

        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "report/AppUser?SchCode=\(strSender)&key=\(urlString.keyPath())"
        
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                 Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    // print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let intAndroid1 = (result["Android"] as? Int){
                            self.intAndroidUser = intAndroid1
                        }
                        if let intiOS1 = (result["iOS"] as? Int){
                            self.intiOSUser = intiOS1
                        }
                        self.lblAndroidCount?.text = "\(self.intAndroidUser)"
                        self.lbliOSCount?.text     = "\(self.intiOSUser)"
                        
                        self.intTotalUser = self.intAndroidUser + self.intiOSUser
                        if self.intTotalUser != 0 {
                        self.lblAndroidPer?.text = "\(Int((100 * Float(self.intAndroidUser) / Float(self.intTotalUser)).rounded()))%"
                        self.lbliOSPer?.text     = "\(Int((100 * Float(self.intiOSUser)     / Float(self.intTotalUser)).rounded()))%"
                        }
                        self.updateGraph(android_users: self.intAndroidUser, ios_users: self.intiOSUser ,totalUser: self.intTotalUser)
                    }
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "Something went wrong", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                         self.navigationController?.popViewController(animated: true)
                    }, secondBtnBlock: {})
                }
            }
        }
    }
    
    func updateGraph(android_users: Int,ios_users: Int,totalUser: Int)
    {
        //will update the graph
        let entry1  = PieChartDataEntry(value: Double(android_users))
        let entry2  = PieChartDataEntry(value: Double(ios_users))
        let dataSet = PieChartDataSet(values: [entry1, entry2], label: "")
        let data    = PieChartData(dataSet: dataSet)
        pieChart_View?.data = data
        
        let color1 = UIColor(rgb: 0x00E3FF)
        let color2 = UIColor(rgb: 0x363636)
        
        let str1 = NSMutableAttributedString(string: "\(totalUser)\n", attributes: [NSAttributedString.Key.font:R.font.ubuntuBold(size: 20.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x2B2B2B)])
        let str2 = NSMutableAttributedString(string: "Logins", attributes: [NSAttributedString.Key.font:R.font.ubuntuRegular(size: 20.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
        str1.append(str2)
        pieChart_View?.centerText   = str1.string
        pieChart_View?.drawEntryLabelsEnabled = false
        pieChart_View?.chartDescription?.text = ""
        pieChart_View?.highlightPerTapEnabled = true
        pieChart_View?.legend.enabled         = false
        dataSet.colors                       = [color1, color2]
        pieChart_View?.rotationEnabled        = true
        //This must stay at end of function
        pieChart_View?.notifyDataSetChanged()
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
