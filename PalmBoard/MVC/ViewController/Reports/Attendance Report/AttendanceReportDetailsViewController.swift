//  AttendanceReportDetailsViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 25/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import Alamofire
import ObjectMapper
import EZSwiftExtensions

class AttendanceReportDetailsViewController : UIViewController {
    
    @IBOutlet weak var lblNameHeader:  UILabel?
    @IBOutlet weak var imgViewHeader:  UIImageView?
    @IBOutlet weak var btnFrom:        UIButton?
    @IBOutlet weak var btnTo:          UIButton?
    @IBOutlet weak var tblViewDetails: UITableView?
    @IBOutlet weak var viewPercentage: UIView?
    
    @IBOutlet weak var heightPercentageView : NSLayoutConstraint?
    @IBOutlet weak var heightOuterPercentageView : NSLayoutConstraint?
    @IBOutlet weak var heightTableView:      NSLayoutConstraint?
    
    @IBOutlet weak var lblPresentCircle : UILabel?
    @IBOutlet weak var lblAbsentCircle  : UILabel?
    @IBOutlet weak var lblLeaveCircle   : UILabel?
    @IBOutlet weak var lblLateCircle    : UILabel?
    @IBOutlet weak var lblLateDaysValue : UILabel?
    
    
    var isLateEnabled : Bool?
    var indexPath1: IndexPath?
    
    var currentDate           = Date()
    let formatter             = DateFormatter()
    
    var strSchoolCodeSender  = String()
    var intUserID            = Int()
    var intBtnFromTo         = Int()
    var sessionStartDate     = String()
    var studentName     : String?
    var studentPhoto    : UIImage?
    var selectFromDate = Date()
    var selectToDate   = Date()
    var actualWidth  = CGFloat()
    var dictSummary     : AttSummary?
    var arrayAttendance : [Attendance]?
    //var isLateEnabled : Bool?
    var strFromDate  = String()
    var strToDate   = String()
    
    @IBOutlet weak var equalWidthConstraint: NSLayoutConstraint!
    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.dateFormat = "dd MMM, yyyy"
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        //        tblViewDetails?.tableFooterView   = UIView(frame: CGRect.zero)
        //        tblViewDetails?.isHidden = true
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideMenu()
        intBtnFromTo         = 1
        let monthStartDate   = currentDate.getStartDateOfMonth(format: "dd MMM, yyyy")
        btnFrom?.setTitle(monthStartDate.0, for: UIControl.State.normal)
        selectFromDate       = monthStartDate.1
        btnTo?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        formatter.dateFormat = "yyyy-MM-dd"
        selectToDate         = currentDate
        strToDate            = formatter.string(from: currentDate)
        if let fromDate = currentDate.getStartDateOfMonth(format: "yyyy-MM-dd").0 {
            strFromDate = fromDate
        }
        //set StudentImage and StudentPhoto
        if let name = studentName {
            lblNameHeader?.text = name
        }
        if let image = studentPhoto {
            imgViewHeader?.image = image
        }
        //tblViewDetails?.rowHeight = UITableView.automaticDimension
        // tblViewDetails?.estimatedRowHeight = 160;
        //tblViewDetails?.delegate   = self
        //tblViewDetails?.dataSource = self
        tblViewDetails?.isHidden   = true
        APIManager.shared.isConnectedToInternet() ?  self.webAttendanceDetailsReport() : Utility.shared.internetConnectionAlert()
        
        self.tblViewDetails?.layoutSubviews()
        
    }
    //MARK: - ClkBtnFrom
    @IBAction func ClkBtnFrom(_ sender: UIButton) {
        print("ClkBtnFrom")
        intBtnFromTo = 1
        self.showDatePicker(btnDate: sender)
    }
    //MARK: - ClkBtnTo
    @IBAction func ClkBtnTo(_ sender: UIButton) {
        print("ClkBtnTo")
        intBtnFromTo = 2
        self.showDatePicker(btnDate: sender)
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(btnDate: UIButton){
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: intBtnFromTo == 1 ? selectFromDate : selectToDate , minimumDate: self.sessionStartDate.convertStringIntoDate(getDateFormat : "yyyy-MM-dd"), maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate.titleLabel?.text) { (date) in
            self.setDateIntoLabel(dateVal : date , lastText :  btnDate.titleLabel?.text)
        }
    }
    // MARK: -Set the date into the label after getting response from the date picker
    func setDateIntoLabel(dateVal : Date? ,lastText : String?) {
        if let dateVal = dateVal {
            let dateString = dateVal.dateToString(formatType :"dd MMM, yyyy")
            formatter.dateFormat = "dd MMM, yyyy"
            //Validation for from date
            if intBtnFromTo == 1 {
                if (dateVal) <= formatter.date(from: (btnTo?.titleLabel?.text)!)!  {//(dateVal) < formatter.date(from: (lasttext))! {
                    selectFromDate = dateVal
                    btnFrom?.setTitle(dateString, for: UIControl.State.normal)
                    
                    formatter.dateFormat = "yyyy-MM-dd"
                    strFromDate  = formatter.string(from: dateVal)
                }
                else {
                    WebserviceManager.showAlert(message: "", title: "'From Date' can't be before 'To date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
                //Validatin for To date
            else {
                if (dateVal) >= formatter.date(from: (btnFrom?.titleLabel?.text)!)!  {
                    //(dateVal) > formatter.date(from: (lasttext))!{
                    selectToDate = dateVal
                    btnTo?.setTitle(dateString, for: UIControl.State.normal)
                    
                    formatter.dateFormat = "yyyy-MM-dd"
                    strToDate = formatter.string(from: dateVal)
                    
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "'To Date' can't be less then 'From date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
            APIManager.shared.isConnectedToInternet() ?  self.webAttendanceDetailsReport() : Utility.shared.internetConnectionAlert()
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            if intBtnFromTo == 1 {
                btnFrom?.setTitle(strLastText, for: UIControl.State.normal)
            }
            else {
                btnTo?.setTitle(strLastText, for: UIControl.State.normal)
            }
        }
    }
    //MARK: - webAttendanceDetailsReport
    func webAttendanceDetailsReport()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Academic/StudentAtt?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&UserID=\(intUserID)&fromdate=\(strFromDate)&tilldate=\(strToDate)"
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblViewDetails?.isHidden = false
                    let object = Mapper<AttendanceDetailsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        if let boardArray1 = parsedObject.attendance{
                            self.arrayAttendance = boardArray1
                            self.isLateEnabled = parsedObject.isLateEnabled
                            
                            print("arrayAttendance Count = \(/self.arrayAttendance?.count)")
                        }
                        if let boardArray1 = parsedObject.attSummary{
                            self.dictSummary = boardArray1
                        }
                        
                        if let isLateEnabled1 = parsedObject.isLateEnabled {
                            self.isLateEnabled = isLateEnabled1
                        }
                        ez.runThisInMainThread {
                            self.actualWidth = (self.isLateEnabled ?? false) ? (((UIScreen.main.bounds.width - 50) / 4)) : (((UIScreen.main.bounds.width - 50) / 3))
                            self.heightPercentageView?.constant  = self.actualWidth
                            self.heightOuterPercentageView?.constant  = self.actualWidth + 54.0
                            self.configure(modal: self.dictSummary!)
                        }
                        self.tblViewDetails?.isHidden   = false
                        self.tblViewDetails?.delegate   = self
                        self.tblViewDetails?.dataSource = self
                        self.tblViewDetails?.isScrollEnabled = false
                        self.tblViewDetails?.reloadData()
                        self.heightTableView?.constant = /self.tblViewDetails?.contentSize.height
                       
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
}
extension AttendanceReportDetailsViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4//5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
            return /self.arrayAttendance?.count
        }
        else  {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Attendance1.get, for: indexPath) as? AttendanceReportCell else {return UITableViewCell()}
            cell.isLateEnabled = self.isLateEnabled
            cell.modal = dictSummary
            return cell
        }
        else if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Attendance2.get, for: indexPath) as? AttendanceAllCell else {return UITableViewCell()}
            cell.modal = arrayAttendance?[indexPath.row]
            return cell
        }
        else if indexPath.section == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Attendance4.get, for: indexPath) as?
                AttendanceReportHeaderCell else {return UITableViewCell()}
            // cell.modal = arrayAttendance?[indexPath.section]
            return cell
        }
        else if indexPath.section == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.Attendance5.get, for: indexPath) as? AttendanceReportDetailCell else {return UITableViewCell()}
            cell.isLateEnabled = self.isLateEnabled
            cell.modal = dictSummary
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0 , 1:
            return 70.0
        case 2 :
            return 54.0
        case 3 :
            if /self.isLateEnabled  {return 260.0}  else { return 230.0}
        default :
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0 , 1:
            return 70.0
        case 2 :
            return 54.0
        case 3 :
            if /self.isLateEnabled  {return 230.0}  else { return 210.0}
        default :
            return UITableView.automaticDimension
        }
    }
    
}
extension AttendanceReportDetailsViewController  {
    
    //var isLateEnabled : Bool?
    
    func configure(modal: AttSummary) {
        
        if let modal = modal as? AttSummary {
            
            var percentFloat =  Float()
            var AbsentFloat  =  Float()
            var leaveFloat   =  Float()
            var lateFloat    =  Float()
            
            percentFloat     = (Float((Float(/modal.totalPrasent) / Float(/modal.totalSchoolDays)) )) * 100
            AbsentFloat      = (Float((Float(/modal.totalAbsent) / Float(/modal.totalSchoolDays)) )) * 100
            leaveFloat       = (Float((Float(/modal.totalLeave) / Float(/modal.totalSchoolDays)) )) * 100
            lateFloat        = (Float((Float(/modal.totalLateDays) / Float(/modal.totalSchoolDays)) )) * 100
            
            if !(percentFloat.isNaN || AbsentFloat.isInfinite || leaveFloat.isNaN)
            {
                lblPresentCircle?.text  = "\( String(format: "%.2f",percentFloat))%"
                lblAbsentCircle?.text   = "\(String(format: "%.2f",AbsentFloat))%"
                lblLeaveCircle?.text    = "\(String(format: "%.2f",leaveFloat))%"
                lblLateCircle?.text     = "\(String(format: "%.2f",lateFloat))%"
            }
            else{
                lblPresentCircle?.text  = "0%"
                lblAbsentCircle?.text   = "0%"
                lblLeaveCircle?.text    = "0%"
                lblLateCircle?.text     = "0%"
            }
        }
        //        let actualWidth = /self.isLateEnabled ? ((UIScreen.main.bounds.width / 4)) : ((UIScreen.main.bounds.width / 3 - 40))
        //        print("*********** Label frame : " , self.lblPresentCircle?.frame)
        lblLateCircle?.isHidden              = !(/isLateEnabled)
        lblLateDaysValue?.isHidden           = !(/self.isLateEnabled)
        lblPresentCircle?.frame = CGRect(x:  /lblPresentCircle?.frame.origin.x, y: /lblPresentCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        lblAbsentCircle?.frame = CGRect(x:  /lblAbsentCircle?.frame.origin.x, y: /lblAbsentCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        lblLeaveCircle?.frame = CGRect(x:  /lblLeaveCircle?.frame.origin.x, y: /lblLeaveCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
        
        
        lblPresentCircle?.layer.cornerRadius = (/lblPresentCircle?.frame.size.width) / 2
        lblAbsentCircle?.layer.cornerRadius  = (/lblAbsentCircle?.frame.size.width) / 2
        lblLeaveCircle?.layer.cornerRadius   = (/lblLeaveCircle?.frame.size.width) / 2
        lblLateCircle?.frame = CGRect(x:  /lblLateCircle?.frame.origin.x, y: /lblLateCircle?.frame.origin.y, width: /self.isLateEnabled ? actualWidth : 0, height: actualWidth)
        if /self.isLateEnabled {
            lblLateCircle?.frame = CGRect(x:  /lblLateCircle?.frame.origin.x, y: /lblLateCircle?.frame.origin.y, width: actualWidth, height: actualWidth)
            lblLateCircle?.layer.cornerRadius  = (/lblLateCircle?.frame.size.width) / 2
        }
    }
    
}


