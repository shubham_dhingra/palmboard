//  AttendanceReportSearchViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 25/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import Alamofire
import ObjectMapper

class AttendanceReportSearchViewController: UIViewController,UISearchBarDelegate {

    @IBOutlet weak var lblNoStudent: UILabel?
    @IBOutlet weak var collectionViewSearch: UICollectionView?
    
    var arrayResult: [Result]?
    var searchBar: UISearchBar!
    var strSchoolCodeSender = String()
    var intUserType = Int()
    var intUserIDAttendance = Int()
    var intUserID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionViewSearch?.isHidden = true
        lblNoStudent?.isHidden         = true

        searchBar                   = UISearchBar()
        searchBar.placeholder       = "Search by Name"
        searchBar.showsCancelButton = false
        searchBar.barTintColor      = UIColor.white
        searchBar.searchBarStyle    = .minimal
        searchBar.returnKeyType     = .search
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
       if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        
       
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //print("searchBar= \(String(describing: searchBar.text))")
        searchBar.resignFirstResponder()
        self.webAttendanceReportSearch(strSearch: searchBar.text!)
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 20
    }
    //MARK: - webAttendanceDetailsReport
    func webAttendanceReportSearch(strSearch: String)
    {
        print("Func: webAttendanceReportSearch")
        lblNoStudent?.isHidden = true
       Utility.shared.loader()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=1&UserType=1&Name=\(searchBar.text!)&StaffID=\(intUserID)"
        print("urlString= \(urlString.replace(target: " ", withString: "%20"))")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async {
                   Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.collectionViewSearch?.isHidden = false
                    let object = Mapper<AttendanceDetailsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray1 = parsedObject.result{
                            self.arrayResult = boardArray1
                            print("arrayResult Count = \(/self.arrayResult?.count)")
                            self.collectionViewSearch?.delegate   = self
                            self.collectionViewSearch?.dataSource = self
                            
                            self.collectionViewSearch?.reloadData()
                            self.collectionViewSearch?.isHidden = false
                            self.lblNoStudent?.isHidden = true

                        }
                        else{
                            self.lblNoStudent?.isHidden = false
                            self.arrayResult = []
                            self.collectionViewSearch?.reloadData()
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }

    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    // MARK: - prepare
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
//    {
//
//
//        if segue.identifier == "SegueAttendanceReportSearch"{
//            let segveLikes = segue.destination as! AttendanceReportDetailsViewController
//            segveLikes.intUserID     = intUserIDAttendance
//        }
//    }
    
}
extension AttendanceReportSearchViewController: UICollectionViewDelegate, UICollectionViewDataSource
{
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return /self.arrayResult?.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.searchCollectioncell.get, for: indexPath) as? AttendanceReportSearch else {return UICollectionViewCell()}
        cell.modal = arrayResult?[indexPath.row]
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        intUserIDAttendance = /self.arrayResult?[indexPath.row].userID
       // self.performSegue(withIdentifier: "SegueAttendanceReportSearch", sender: self)
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let attendanceVc = storyboard.instantiateViewController(withIdentifier: "AttendanceReportDetailsViewController") as? AttendanceReportDetailsViewController else {return}
        attendanceVc.intUserID     = intUserIDAttendance
        self.pushVC(attendanceVc)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width/2, height: 171)
    }
    
}
