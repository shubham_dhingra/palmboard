//  AttendanceReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 04/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire

class AttendanceReportViewController: UIViewController {
    
    @IBOutlet weak var btnDate:           UIButton?
    @IBOutlet weak var segmentAttendance: UISegmentedControl?
    @IBOutlet weak var tblAttType:        UITableView?
    @IBOutlet weak var tblAttendance:     UITableView?    
    @IBOutlet weak var lblNoStudent:        UILabel?
    @IBOutlet weak var lblClassType:       UILabel?
    @IBOutlet weak var barBtnView:         UIView?
    @IBOutlet weak var imgBarBtn:          UIImageView?
    @IBOutlet weak var rightBarBtnItem:    UIBarButtonItem?
    @IBOutlet weak var leftBackBarBtnItem: UIBarButtonItem?
    @IBOutlet weak var btnRightSearch: UIBarButtonItem!
    
    var selectDate : Date?
    var currentDate             = Date()
    let formatter               = DateFormatter()
    var strSchoolCodeSender     = String()
    var intClassId              = Int()
    var intSegmetType           = Int()
    var sessionStartDate        = String()
    var selectClassName : String?
    var arrayAttendanceReport: [AttReport]?
    var arrayCopyAttendanceReport: [AttReport]?
    var arrayMyClass:   [MyClasse]?
    
    var intUserId = Int()
    var intUserIDAttendance = Int()
    var strDate = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        intSegmetType = 0
        formatter.dateFormat = "yyyy-MM-dd"
        selectDate = currentDate//formatter.string(from: currentDate)
        // print("strDate = \(strDate)")
        segmentAttendance?.addUnderlineForSelectedSegment()
        
        if let sessionStartDate = self.getCurrentUser()?.sessionStart {
            self.sessionStartDate = sessionStartDate
            // print("Session Start Date", self.sessionStartDate)
        }
        formatter.dateFormat = "dd MMM, yyyy"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        btnDate?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        
        tblAttendance?.estimatedRowHeight = 300
        tblAttendance?.rowHeight          = UITableView.automaticDimension
        
        tblAttendance?.tableFooterView  = UIView(frame: CGRect.zero)
        tblAttType?.tableFooterView     = UIView(frame: CGRect.zero)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            self.intUserId =  Int(userId)
        }
        barBtnView?.isHidden = true
        setUpBarBtn()
        
        if arrayMyClass?.count != 0 {
          self.webAttendanceReport()
        }
        if let className = selectClassName {
           self.lblClassType?.text = "Class: \(className)"
        }
    }
    
    func setUpBarBtn(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.barBtnView?.addGestureRecognizer(tap)
    }
    @objc func handleTap() {
        if (self.tblAttType?.isHidden)! {
            self.tblAttType?.isHidden = false
            self.imgBarBtn?.image = R.image.smsUpArrow()
            self.lblNoStudent?.isHidden = true
        } else {
            self.tblAttType?.isHidden = true
            self.imgBarBtn?.image = R.image.smsDownArrow()
        }
    }
    //MARK: - ClkBtnDate
    @IBAction func ClkBtnDate(_ sender: UIButton) {
        self.showDatePicker()
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(){
        
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: selectDate ?? Date() , minimumDate: self.sessionStartDate.convertStringIntoDate(getDateFormat : "yyyy-MM-dd"), maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate?.titleLabel?.text) { (date) in
            self.setDateIntoLabel(date: date , lastText :  self.btnDate?.titleLabel?.text)
        }
    }
    //Set the date into the label after getting response from the date picker
    func setDateIntoLabel(date : Date? ,lastText : String?) {
        if let date = date {
            selectDate = date
            let dateString = date.dateToString(formatType :"dd MMM, yyyy")
            btnDate?.setTitle(dateString, for: UIControl.State.normal)
            webAttendanceReport()
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            btnDate?.setTitle(strLastText, for: UIControl.State.normal)
        }
    }
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.hideMenu()
        super.viewWillAppear(animated)
        //         barBtnView?.isHidden = true
    }
    //MARK: - ClkSegment
    @IBAction func ClkSegment(_ sender: Any) {
        segmentAttendance?.changeUnderlinePosition()
        intSegmetType = (segmentAttendance?.selectedSegmentIndex)!
        
        if self.arrayAttendanceReport?.count != 0 {
            switch segmentAttendance?.selectedSegmentIndex {
            case 0:
                // print("strDate = \(strDate)")
                self.arrayCopyAttendanceReport = self.arrayAttendanceReport
            case 1,2,3:
                self.arrayCopyAttendanceReport = self.arrayAttendanceReport?.filter({$0.status == segmentAttendance?.selectedSegmentIndex})
            default:
                break
            }
            if arrayCopyAttendanceReport == nil {
                self.lblNoStudent?.isHidden = false
            }
            else {
                self.lblNoStudent?.isHidden = arrayCopyAttendanceReport?.count != 0
            }
        }
        else {
            self.lblNoStudent?.isHidden = false
        }
        tblAttendance?.reloadData()
    }
    //MARK: - webAttendanceReport
    func webAttendanceReport()
    {
        self.view.endEditing(true)
        
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Academic/ClassAtt?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&ClassID=\(intClassId)&attdate=\(selectDate?.dateToString(formatType: "yyyy-MM-dd") ?? "")"
        print("urlString= \(urlString)")
        Utility.shared.loader()
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblAttendance?.isHidden = false
                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.AttendanceReport{
                            self.arrayAttendanceReport = boardArray
                            self.lblNoStudent?.isHidden = boardArray.count != 0
                            if self.intSegmetType == 0 {
                                self.arrayCopyAttendanceReport = boardArray
                                self.tblAttendance?.isHidden = false
                            }
                            else  {
                                self.arrayCopyAttendanceReport = self.arrayAttendanceReport?.filter({$0.status == self.intSegmetType})
                            }
                            // print("arraySMSReport Count = \(/self.arrayAttendanceReport?.count)")
                            self.tblAttendance?.reloadData()
                        }
                        else {
                            self.arrayCopyAttendanceReport = []
                            self.arrayAttendanceReport = []
                            self.tblAttendance?.reloadData()
                            self.lblNoStudent?.isHidden = false
                        }
                        self.barBtnView?.isHidden = false
                        
                    }
                case .failure(let error):
                    self.barBtnView?.isHidden = false
                    self.lblNoStudent?.isHidden = true
                    WebserviceManager.showAlert(message:error.localizedDescription, title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
        }
    }
//    //MARK: - webAttendanceTypeReport
//    func webAttendanceTypeReport()
//    {
//        self.view.endEditing(true)
//        Utility.shared.loader()
//        var urlString = String()
//        urlString    += urlString.webAPIDomainNmae()
//        urlString    += "Teacher/MyClass?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&UserID=\(intUserId)"
//        print("urlString= \(urlString)")
//        Alamofire.request(urlString, method: .get
//            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
//                DispatchQueue.main.async{
//                    Utility.shared.removeLoader()
//                }
//                switch response.result {
//                case .success(let data):
//                    self.tblAttendance?.isHidden = false
//                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
//                    guard let parsedObject = object else {return}
//                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
//
//                        if let boardArray1 = parsedObject.MyClasse{
//                            self.arrayMyClass = boardArray1
//                            print("arrayMyClass Count = \(/self.arrayMyClass?.count)")
//                            self.lblNoStudent?.isHidden = boardArray1.count != 0
//                            if self.arrayMyClass?.count != 0 {
//                                if self.arrayMyClass?[0].className?.trimmed().count != 0 {
//                                    self.lblClassType?.text = "Class: \(/self.arrayMyClass?[0].className)"
//                                }
//                                self.intClassId        = /self.arrayMyClass?[0].classID
//                                // print("intClassId  = \(self.intClassId)")
//                                self.tblAttType?.reloadData()
//                                self.webAttendanceReport()
//                            }
//                            else{
//                                self.segmentAttendance?.isHidden = true
//                                self.btnRightSearch = nil
//                                self.navigationItem.rightBarButtonItems = nil
//                                self.btnDate?.isHidden = true
//                                self.title = "Attendance"
//                                self.lblNoStudent?.text = "No class assigned to you."
//
//                            }
//                        }
//                    }
//                case .failure(let error):
//                    WebserviceManager.showAlert(message: error.localizedDescription, title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
//                    }, secondBtnBlock: {})
//                    break
//                }
//        }
//    }
    //MARK: - ClkSearch
    @IBAction func ClkSearch(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let attendanceVc = storyboard.instantiateViewController(withIdentifier: "AttendanceReportSearchViewController") as? AttendanceReportSearchViewController else {return}
        self.pushVC(attendanceVc)
    }
    //MARK: - ClkBack
    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- UITableViewDelegate and UITableViewDataSource
extension AttendanceReportViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tblAttType{
            return 1
        }
        else{
            //  print("Count = \(/arrayCopyAttendanceReport?.count)")
            return /arrayCopyAttendanceReport?.count
        }}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblAttType{
            return /arrayMyClass?.count
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tblAttendance{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.attendanceReport.get, for: indexPath) as? AttendanceReportTableViewCell else {return UITableViewCell()}
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.modal = arrayCopyAttendanceReport?[indexPath.section]
            
            return cell
        } else if tableView == self.tblAttType {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.attendanceType.get, for: indexPath)
            cell.textLabel?.text = arrayMyClass?[indexPath.row].className
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.textLabel?.font = R.font.ubuntuRegular(size: 17)
            cell.textLabel?.textColor = UIColor(rgb: 0x545454)
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblAttType{
            
            if self.arrayMyClass?[indexPath.row].className?.trimmed().count != 0 {
                self.lblClassType?.text = "Class:  \(/arrayMyClass?[indexPath.row].className)"
            }
            intClassId = /arrayMyClass?[indexPath.row].classID
            tableView.isHidden = true
            imgBarBtn?.image = R.image.smsDownArrow()
            self.webAttendanceReport()
        }
        else{
            guard let cell  = tableView.cellForRow(at: indexPath) as? AttendanceReportTableViewCell else{ return}
            intUserIDAttendance = /self.arrayCopyAttendanceReport?[indexPath.section].userID
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let attendanceVc = storyboard.instantiateViewController(withIdentifier: "AttendanceReportDetailsViewController") as? AttendanceReportDetailsViewController else {return}
            attendanceVc.sessionStartDate = self.sessionStartDate
            attendanceVc.studentName = cell.lblName?.text
            attendanceVc.studentPhoto = cell.imgStudent?.image
            attendanceVc.intUserID     = intUserIDAttendance
            self.pushVC(attendanceVc)
        }
    }
}
