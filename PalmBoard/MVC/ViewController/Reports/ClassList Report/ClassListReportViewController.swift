//  ClassListReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 04/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class ClassListReportViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UISearchControllerDelegate {
    
    @IBOutlet weak var collectionViewGrid:   UICollectionView?
    @IBOutlet weak var collectionViewMenu:   UICollectionView?
    @IBOutlet weak var heightCollection:     NSLayoutConstraint?
    
    @IBOutlet weak var imgViewTotal:      UIImageView?

    @IBOutlet weak var btnTotalStudent:      UIButton?
    @IBOutlet weak var lblLine:              UILabel?
    @IBOutlet weak var lblTotalBoys:         UILabel?
    @IBOutlet weak var lblTotalGirls:        UILabel?
    @IBOutlet weak var lblTotalStudentCount: UILabel?
    @IBOutlet weak var stackViewCategory:    UIStackView?
    
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgViewNoClassmateOrTeacher: UIImageView?
    @IBOutlet weak var lblNoClassmateOrTeacher:     UILabel?

    var userId                    = Int()
    var strSchoolCodeSender       = String()
    var userType                  = Int()
    var arrayDataList             = [[String: Any]]()
    var arrayAssignmentSubject    = [[String: Any]]()
    var arraySearchAssignmentList = [[String: Any]]()
    
    var otherUserId      = Int()
    var otherUserType    = Int()
    var otherUserPhoto   = String()
    var otherUserSubject = String()
    var btnTitle         = UIButton()
    var btnSearch        = UIButton()
    var intClassId       = Int()
    var eventIndex       = Int()
    
    var filtered:[String]   = []
    var searchActive : Bool = false
    let searchController    = UISearchController(searchResultsController: nil)
    var themeColor          = String()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.eventIndex = 0
        heightCollection?.constant = 45
        
        imgViewNoClassmateOrTeacher?.isHidden = true
        lblNoClassmateOrTeacher?.isHidden     = true
        
        lblTotalStudentCount?.layer.cornerRadius  = (lblTotalStudentCount?.frame.size.width)! / 2
        lblTotalStudentCount?.layer.masksToBounds = true
//        collectionViewTopConstraint.constant = 0.0
    }
    // MARK: -  viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        themeColor = self.getCurrentSchool()?.themColor ?? ""
        
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Class List", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        btnSearch                =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
        btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
        btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation     = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search By Name"
        searchController.searchBar.sizeToFit()
        self.searchController.searchBar.alpha = 0
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.userId =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.userType =  Int(userType)
        }
        
        if let classID = self.getCurrentUser()?.classID{
            self.intClassId =  Int(classID)
        }
       
        self.collectionViewGrid?.delegate   = nil
        self.collectionViewGrid?.dataSource = nil
        
        self.collectionViewMenu?.delegate   = nil
        self.collectionViewMenu?.dataSource = nil
        
        webApiClassList()
    }
    //MARK: searchBar Delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchActive = true
        let searchString = searchController.searchBar.text
        self.arraySearchAssignmentList = self.arrayAssignmentSubject.filter({ (item) -> Bool in
            
            let resultText: NSString = item["StudentName"] as! NSString
            
            return (resultText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        searchActive = self.arraySearchAssignmentList.count == 0 ?  false : true
 
        updateUI()
        
        collectionViewGrid?.reloadData()
    }
    
    func updateUI() {
        collectionViewTopConstraint.constant = searchActive ? 0.0 : 74.0
            btnTotalStudent?.isHidden   = searchActive
        imgViewTotal?.isHidden      = searchActive
            lblTotalBoys?.isHidden      = searchActive
            lblTotalGirls?.isHidden     = searchActive
            stackViewCategory?.isHidden = searchActive
            lblTotalStudentCount?.isHidden = searchActive
        
      }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchController.searchBar.alpha = 0
        //self.navigationItem.titleView = nil
        self.navigationItem.titleView = btnTitle
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        searchActive = false
        self.arraySearchAssignmentList.removeAll()
        self.dismiss(animated: true, completion: nil)
        collectionViewGrid?.reloadData()
        updateUI()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
         updateUI()
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 70
    }
    // MARK: - viewDidLayoutSubviews
    override func viewDidLayoutSubviews() {
        self.collectionViewGrid?.collectionViewLayout.invalidateLayout()
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewGrid?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - ClkBtnSearch
    @objc func ClkBtnSearch(sender:UIButton)
    {
        self.searchController.searchBar.alpha = 1
        self.searchController.searchBar.showsCancelButton = true
        searchController.searchBar.becomeFirstResponder()
        self.navigationItem.titleView = searchController.searchBar
        self.navigationItem.rightBarButtonItem = nil
    }
    
    // MARK: -  segmentChanged
    func webApiClassList()
    {
        imgViewNoClassmateOrTeacher?.isHidden = true
        lblNoClassmateOrTeacher?.isHidden     = true

        collectionViewGrid?.isHidden     = true
        collectionViewMenu?.isHidden     = true
        
        self.stackViewCategory?.isHidden = true
        btnTotalStudent?.isHidden        = true
        imgViewTotal?.isHidden = true
        lblLine?.isHidden                = true
        lblTotalStudentCount?.isHidden   = true
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "teacher/StudentInClass?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(userId)"
        
        print("urlString = \(urlString)")
        Utility.shared.loader()
        WebserviceManager.getJsonData(withParameter: urlString)
        { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    //print("List result \n \(result)")
                    DispatchQueue.main.async {
                        self.collectionViewGrid?.isHidden = false
                        self.collectionViewMenu?.isHidden = false
                        
                        if let allTeacher = (result["StudentInClass"] as? [[String: Any]]){
                            self.arrayDataList       = allTeacher
                        }
                        if self.arrayDataList.count == 0{
                            self.imgViewNoClassmateOrTeacher?.isHidden = false
                            self.lblNoClassmateOrTeacher?.isHidden     = false

//                            WebserviceManager.showAlert(message: "", title: "Class List not found", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
                        }
                        else{
                            self.stackViewCategory?.isHidden = false
                            self.btnTotalStudent?.isHidden   = false
                            self.imgViewTotal?.isHidden      = false
                            self.lblLine?.isHidden           = false
                            self.lblTotalStudentCount?.isHidden  = false
                            
                            self.lblTotalStudentCount?.text = "\(self.arrayDataList.count)"
                            // self.collectionViewGrid.setContentOffset(CGPoint(x:0,y:0), animated: true)
                            // self.collectionViewGrid.reloadData()
                            
                            self.heightCollection?.constant = 45
                            
                            self.collectionViewGrid?.delegate   = nil
                            self.collectionViewGrid?.dataSource = nil
                            self.collectionViewGrid?.isHidden   = false
                            
                            self.collectionViewMenu?.isHidden   = false
                            self.collectionViewMenu?.delegate   = self
                            self.collectionViewMenu?.dataSource = self
                            self.collectionViewMenu?.reloadData()
                            
                            self.collectionViewMenu?.performBatchUpdates({},
                                                                        completion: { (finished) in
                                                                            print("collection-view finished reload done")
                                                                            
                                                                            self.collectionViewMenu?.collectionViewLayout.invalidateLayout()
                                                                            
                                                                            let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                                                                            self.collectionViewMenu?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                                                            
                                                                            self.collectionView(self.collectionViewMenu!, didSelectItemAt: IndexPath(item: 0, section: 0))
                            })
                        }
                    }
                }
                else{
                    DispatchQueue.main.async{
                    self.heightCollection?.constant = 0
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                }
                }
            }
            else{
                DispatchQueue.main.async{
                self.heightCollection?.constant = 0
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
            }
            }
        }
    }
    
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1{
            return self.arrayDataList.count
        }
        else{
            if(searchActive){
                return self.arraySearchAssignmentList.count
            }
            else{
                if self.searchController.searchBar.text?.trimmed().count == 0 {
                    return self.arrayAssignmentSubject.count
                }
                else {
                    return self.arraySearchAssignmentList.count
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("collectionView.tag = \(collectionView.tag)")
        if collectionView.tag == 1
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellClassReport", for: indexPath) as! customClassReportCollectionViewCell
            
            cell.lblSubjectName?.text = arrayDataList[indexPath.row]["ClassName"] as? String
            cell.lblLine?.tag         = indexPath.row + 1
            // print("cell.lblSubjectName.text= \(String(describing: cell.lblSubjectName.text))")
            
            if eventIndex == indexPath.row {
                cell.lblLine?.backgroundColor  = themeColor.hexStringToUIColor()//UIColor(rgb: 0xFFC026)
                cell.lblSubjectName?.textColor = themeColor.hexStringToUIColor()//UIColor(rgb: 0xFFC026)
            }else {
                cell.lblLine?.backgroundColor  = UIColor.clear
                cell.lblSubjectName?.textColor = UIColor(rgb: 0xD5D5D5)
            }
            cell.lblLine?.isHidden = false
            return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as! ClassListReportCollectionViewCell
            
            cell.btnImage?.layer.cornerRadius = (cell.btnImage?.frame.width)! / 2
            cell.btnImage?.layer.masksToBounds = true
            cell.btnImage?.tag = indexPath.row
            
           // cell.btnDiscuss?.tag = indexPath.row
           // cell.btnDiscuss?.layer.cornerRadius = 5
            
            if (self.arrayDataList.count == 0){
                WebserviceManager.showAlert(message: "Class List is not found for this class", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                }, secondBtnBlock: {})
                
            }else{
                
                if self.arrayAssignmentSubject.count > 0 {
                    
                    if (searchActive){
                        
                        if let imgUrl = self.arraySearchAssignmentList[indexPath.row]["Photo"] as? String
                        {//filtered
                            var strPrefix      = String()
                            strPrefix          += strPrefix.imgPath()
                            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            let url            = URL(string: finalUrlString )
                            // print("imgUrl \n \(url!)")
                            let imgViewUser = UIImageView()
                            imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                                
                                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                                cell.btnImage?.setBackgroundImage(img1, for: .normal)
                            }
                        }
                        if let name = self.arraySearchAssignmentList[indexPath.row]["StudentName"] as? String
                        {
                            cell.lblName?.text = name
                        }
                    }else{
                        //not filtered
                        if let imgUrl = self.arrayAssignmentSubject[indexPath.row]["Photo"] as? String
                        {
                            var strPrefix      = String()
                            strPrefix          += strPrefix.imgPath()
                            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            let url            = URL(string: finalUrlString )
                            // print("imgUrl \n \(url!)")
                            let imgViewUser = UIImageView()
                            imgViewUser.af_setImage(withURL: url!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                                
                                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                                cell.btnImage?.setBackgroundImage(img1, for: .normal)
                            }
                        }
                        if let name = self.arrayAssignmentSubject[indexPath.row]["StudentName"] as? String
                        {
                            cell.lblName?.text = name
                        }
                    }
                }
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //print("Did select row= \(indexPath.row)")
        if collectionView.tag == 1
        {
        if let array = arrayDataList[indexPath.row]["Students"] as? [[String: Any]]
        {
            self.arrayAssignmentSubject    = array
            self.arraySearchAssignmentList = array
            
            self.lblTotalStudentCount?.text = "\(array.count)"
            if let Boys = arrayDataList[indexPath.row]["Boys"] as? Int{
                self.lblTotalBoys?.text  = "\(Boys) Boys"
            }
            if let Girls = arrayDataList[indexPath.row]["Girls"] as? Int{
                self.lblTotalGirls?.text  = "\(Girls) Girls"
            }
            collectionViewMenu?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewMenu?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            
            collectionView.reloadData()
            self.collectionViewGrid?.delegate   = self
            self.collectionViewGrid?.dataSource = self
            self.collectionViewGrid?.reloadData()
            self.collectionViewGrid?.isHidden   = false
            
            self.imgViewNoClassmateOrTeacher?.isHidden = true
            self.lblNoClassmateOrTeacher?.isHidden     = true
        }
        else{
            collectionViewGrid?.isHidden    = true
            self.lblTotalBoys?.text         = "0 Boys"
            self.lblTotalGirls?.text        = "0 Girls"
            self.lblTotalStudentCount?.text = "0"
            
            self.imgViewNoClassmateOrTeacher?.isHidden = false
            self.lblNoClassmateOrTeacher?.isHidden     = false

            collectionViewMenu?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewMenu?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            self.arrayAssignmentSubject    = []
            self.arraySearchAssignmentList = []
            collectionView.reloadData()
        }
    }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 2{
            return CGSize(width: self.view.frame.width/2, height: 190)
        }
        else {
            return CGSize(width: 100, height: 45)
        }
    }
    
    // MARK: -  imagePressed
    @IBAction func imagePressed(_ sender: UIButton) {
        
        let row      = sender.tag
        
        var arraySend = [[String:Any]]()
        if self.arraySearchAssignmentList.count > 0 {
            arraySend = self.arraySearchAssignmentList
        }
        else {
            arraySend = self.arrayAssignmentSubject
        }
        // let indexPath = IndexPath(row: row, section: 0)
        if let userId1 = arraySend[row]["UserID"] as? Int{
            self.otherUserId      = userId1
        }else{
            self.otherUserId      = 0
        }
        if let userType1 = arraySend[row]["UserType"] as? Int{
            self.otherUserType    = userType1
        }else{
            self.otherUserType    = 0
        }

        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        //self.performSegue(withIdentifier: "showProfileTeacher", sender: self)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "ShortProfileViewController") as? ShortProfileViewController else {return}
        shortProfileVc.intUserID         = self.otherUserId
        shortProfileVc.intUserType       = self.otherUserType
        shortProfileVc.strteacherSubject = self.otherUserSubject
        self.pushVC(shortProfileVc)
    }
}

class ClassListReportCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var btnImage:   UIButton?
    @IBOutlet weak var lblName:    UILabel?
   // @IBOutlet weak var btnDiscuss: UIButton?
}
class customClassReportCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var lblSubjectName: UILabel?
    @IBOutlet weak var lblLine:        UILabel?
}
