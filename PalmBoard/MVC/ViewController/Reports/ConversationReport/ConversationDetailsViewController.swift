//
//  ConversationDetailsViewController.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

class ConversationDetailsViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel?
    @IBOutlet weak var btnRecipt: UIButton?
    @IBOutlet weak var tblConvDetails: UITableView?
    
    var intUserId            = Int()
    var strSchoolCodeSender  = String()
    var strDate              = String()
    var arrayMsgDTL: [MsgDTL]?
    var mainModel: ConversationDetailsModel?
    
    var intMsgID              = Int()
    
    @IBAction func ClkRecipent(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let conversationListVc = storyboard.instantiateViewController(withIdentifier: "ConversationListViewController") as? ConversationListViewController else {return}
        conversationListVc.mainModel     = mainModel
        self.presentVC(conversationListVc)
        // self.performSegue(withIdentifier: "segueConversationList", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Conversation Report"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        tblConvDetails?.tableFooterView  = UIView(frame: CGRect.zero)
        
    
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserId =  Int(userId)
        }
        
        self.webConversationReport()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : false , "Index" : -1])
        //        let parent =  self.presentingViewController?.parent as! CustomTabbarViewController
        //        parent.hideBar(boolHide: true)
        
    }
    func webConversationReport()
    {
        print("Func: webConversationReport")
        Utility.shared.loader()
        self.tblConvDetails?.isHidden = true
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/CnvrMsgDTL?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&MsgID=\(intMsgID)"
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    
                    self.tblConvDetails?.isHidden = false
                    let object = Mapper<ConversationDetailsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.msgDTL{
                            self.arrayMsgDTL = boardArray
                            self.mainModel = parsedObject
                            print("arrayConversation Count = \(/self.arrayMsgDTL?.count)")
                            self.tblConvDetails?.reloadData()
                            self.tblConvDetails?.isHidden = false
                        }
                        self.lblTitle?.text = parsedObject.subject
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueConversationList"{
            let segveLikes = segue.destination as! ConversationListViewController
            segveLikes.mainModel     = mainModel
        }
    }
}
//MARK:- UITableViewDelegate and UITableViewDataSource
extension ConversationDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /arrayMsgDTL?.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.conversationDetailsCell.get, for: indexPath) as? ConversationDetailsTableViewCell else {return UITableViewCell()}
        
        cell.modal = arrayMsgDTL?[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // intUserIDAttendance = /self.arrayCopyAttendanceReport?[indexPath.section].userID
        //self.performSegue(withIdentifier: "SegueAttendanceReportDetails", sender: self)
    }
}
