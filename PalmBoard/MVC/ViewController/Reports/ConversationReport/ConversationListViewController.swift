//  ConversationListViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 30/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class ConversationListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
//    @IBOutlet weak var navBar:         UINavigationBar?
    @IBOutlet weak var tblViewList:    UITableView?
    @IBOutlet weak var leftBarBtnItem: UIBarButtonItem?
    @IBOutlet weak var btnRecipient: UIBarButtonItem?
    var mainModel: ConversationDetailsModel?
    
    var intUserType       = Int()
    var arrayRecipients   = [[String: Any]]()
    
    // MARK: - viewDidLoad method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        let themeColor =  self.getCurrentSchool()?.themColor
//        navBar?.backgroundColor = themeColor?.hexStringToUIColor()
        
        tblViewList?.rowHeight          = UITableView.automaticDimension
        tblViewList?.estimatedRowHeight = 2000
        tblViewList?.tableFooterView    = UIView(frame: CGRect.zero)
        if let count = mainModel?.recipients?.count {
            if count == 0 {
                return
            }
            btnRecipient?.title = count == 1 ? "\(count) Recipient" : "\(count) Recipients"
        }
      
        //        let parent =  self.presentingViewController?.parent as! CustomTabbarViewController
        //        parent.hideBar(boolHide: true)
    }
    // MARK: - tableView Delegate & DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return /mainModel?.recipients?.count//self.arrayRecipients.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.conversationListCell.get, for: indexPath) as? ConversationList else {return UITableViewCell()}
        cell.modal = mainModel?.recipients?[indexPath.row]//mainModel?[indexPath.row]
        return cell
    }
    // MARK: - ClkBtnCross1 method
    @IBAction func ClkBtnCross(_ sender: Any)
    {
        //        let parent =  self.presentingViewController?.parent as! CustomTabbarViewController
        //        parent.hideBar(boolHide: false)
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
class ConversationList: UITableViewCell {
    @IBOutlet weak var lblNameSent: UILabel!
    @IBOutlet weak var imgViewSent: UIImageView!
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? Recipient {
            lblNameSent.text    = modal.name
            
            if let SenderType = modal.receiverType
            {
                
                if SenderType == 1 || SenderType == 2
                {
                    var strChildName  = String()
                    var strClass      = String()
                    var strName       = String()
                    
                    if let ChildName = modal.childName{
                        strChildName = ChildName
                    }
                    if let ClassName = modal.className{
                        strClass = ClassName
                    }
                    if let name = modal.name{
                        strName = name
                    }
                    
                    if SenderType == 1{
                        lblNameSent.text = "\(strName), (\(strClass))"
                    }
                    else if SenderType == 2{
                        lblNameSent.text = "Parent of \(strChildName), (\(strClass))"
                    }
                }
                else if let name = modal.name{
                    lblNameSent.text = "\(name)"
                    if let ChildName = modal.designation{
                        lblNameSent.text = "\(name), \(ChildName)"
                    }
                    // cell.lblName.text = "\(name), \(strNameDesignation)"
                }
            }
            
            
            if let imgUrl = modal.photo {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgViewSent.image = img1
                }
            }
            else{
                self.imgViewSent.image = R.image.noProfile_Big()
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgViewSent.clipsToBounds       = true
        imgViewSent.layer.cornerRadius = imgViewSent.frame.size.width / 2
    }
}
