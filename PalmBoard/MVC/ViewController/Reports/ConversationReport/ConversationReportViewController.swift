//  ConversationReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 28/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire
import EZSwiftExtensions

class ConversationReportViewController: UIViewController {
    
    @IBOutlet weak var tblConversation: UITableView?
    @IBOutlet weak var imgViewNoConversation: UIImageView?
    @IBOutlet weak var lblNoConversation    : UILabel?

    var intUserId            = Int()
    var strSchoolCodeSender  = String()
    var strDate              = String()
    var strFromDate:String?
    
    var arrayConversation: [Conversation]?
    let formatter             = DateFormatter()
    var currentDate           = Date()
    var intPaggination        = Int()
    var spinner               = UIActivityIndicatorView()
    var intMsgID              = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Conversation Report"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        intPaggination = 1
        tblConversation?.tableFooterView  = UIView(frame: CGRect.zero)
       
        
        formatter.dateFormat = "yyyy-MM-dd"
        strDate = formatter.string(from: currentDate)
        
        if let fromDate = currentDate.getStartDateOfMonth1(format: "yyyy-MM-dd") {
            strFromDate = fromDate
        }        
        spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner.color        =  UIColor(rgb: 0x56B54B)
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
        
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
       
        
        if let schoolCode = self.getCurrentSchool()?.schCode {strSchoolCodeSender =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID    {self.intUserId =  Int(userId)}
        
        self.webConversationReport(intPagination: intPaggination)
    }
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            let parent =  self.navigationController?.parent as! CustomTabbarViewController
            parent.hideBar(boolHide: true)
    }
    func webConversationReport(intPagination: Int)
    {
        self.imgViewNoConversation?.isHidden = true
        self.lblNoConversation?.isHidden     = true

        print("Func: webConversationReport")
        Utility.shared.loader()
        self.tblConversation?.isHidden = true
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        print("strDate = \(strDate)")
        
        urlString    += "Message/Conversation?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=1&FromDate=\(/strFromDate)&TillDate=\(strDate)&pg=\(intPagination)"//strDate
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblConversation?.isHidden = false
                    let object = Mapper<ConversationModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.conversation{
                            self.arrayConversation = boardArray
                            print("arrayConversation Count = \(/self.arrayConversation?.count)")
                            self.tblConversation?.isHidden = false
                            self.tblConversation?.reloadData()
                        }
                        if self.arrayConversation?.count == 0 || self.arrayConversation?.count == nil  || parsedObject.conversation?.count == 0{
                            self.arrayConversation?.removeAll()
                            self.tblConversation?.reloadData()
                            self.imgViewNoConversation?.isHidden = /(self.arrayConversation?.count) != 0
                            self.lblNoConversation?.isHidden     = /(self.arrayConversation?.count) != 0

                            WebserviceManager.showAlert(message: "No data found", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                               self.navigationController?.popViewController(animated: true)
                            }, secondBtnBlock: {})
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            print("fetch more data -- scrollViewDidEndDragging")
            spinner.startAnimating()
            
            intPaggination = intPaggination + 1
            //self.webAPIQuestions(intAll: intAllMyCount, intPage: intCount)
            self.loadChunkSMSReport(intPagination: intPaggination)
        }
    }
    func loadChunkSMSReport(intPagination: Int)
    {
        print("Func: webSMSReport")
        spinner.startAnimating()
        
        print("strDate = \(strDate)")
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/Conversation?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=1&FromDate=\(/strFromDate)&TillDate=\(strDate)&pg=\(intPagination)"//strDate
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                
                switch response.result {
                case .success(let data):
                    self.tblConversation?.isHidden = false
                    let object = Mapper<ConversationModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.conversation{
                            for sms in boardArray {
                                self.arrayConversation?.append(sms)
                            }
                            self.tblConversation?.reloadData()
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
}
//MARK:- UITableViewDelegate and UITableViewDataSource
extension ConversationReportViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /arrayConversation?.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.conversationCell.get, for: indexPath) as? ConversationTableViewCell else {return UITableViewCell()}
        cell.selectionStyle = .none
        cell.modal = arrayConversation?[indexPath.row]
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        intMsgID = /self.arrayConversation?[indexPath.row].msgID
        // self.performSegue(withIdentifier: "SegueConversationDetails", sender: self)
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let conversationDetailsVc = storyboard.instantiateViewController(withIdentifier: "ConversationDetailsViewController") as? ConversationDetailsViewController else {return}
        conversationDetailsVc.intMsgID     = intMsgID
        self.pushVC(conversationDetailsVc)
    }
}
