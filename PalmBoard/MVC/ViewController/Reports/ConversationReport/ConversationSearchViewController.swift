//  ConversationSearchViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire

class ConversationSearchViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tblConvSearch:  UITableView?
    @IBOutlet weak var tblConvMenu:    UITableView?
    @IBOutlet weak var txtFieldSearch: UITextField?
    @IBOutlet weak var viewBg:         UIView?
    @IBOutlet weak var btnRightBar:    UIBarButtonItem?
    @IBOutlet weak var btnFrom:         UIButton?
    @IBOutlet weak var btnTo:           UIButton?
    @IBOutlet weak var heightTextField: NSLayoutConstraint?
    @IBOutlet weak var imgViewNoConversation: UIImageView?
    @IBOutlet weak var lblNoConversation    : UILabel?

    var intMsgID = Int()
    var intFilter = Int()
    
    var intUserId             = Int()
    var strSchoolCodeSender   = String()
    var strDate               = String()
    var arrayConversation: [Conversation]?
    let formatter             = DateFormatter()
    var currentDate           = Date()
    var intBtnFromTo          = Int()
    var thirtyDaysBeforeToday = Date()
    var strFromDate           = String()
    var strToDate             = String()
    var intPaggination        = Int()
    var spinner               = UIActivityIndicatorView()
    var arrayMenu             = [[String:Any]]()
    
    var selectToDate   = Date()
    var selectFromDate = Date()
    
    @IBAction func ClkFilter(_ sender: Any) {
        tblConvMenu?.isHidden = false
        tblConvMenu?.frame    = CGRect(x:self.view.frame.size.width - 168.89,y:50,width: 168.89, height: 214)
        tblConvMenu?.reloadData()
    }
    func setUpTableView(){
        
        let containerView : UIView = UIView(frame:CGRect(x:self.view.frame.size.width - 168.89,y:64,width: 168.89, height: 214))
        tblConvMenu?.frame = containerView.bounds
        containerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        containerView.layer.shadowOpacity = 4.0
        
        self.view.addSubview(containerView)
        containerView.addSubview(self.tblConvMenu!)
        
        tblConvMenu?.isHidden = false
        tblConvMenu?.layer.masksToBounds = true
        tblConvMenu?.isScrollEnabled = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Conversation Report"
        heightTextField?.constant = 0
        //        self.setUpTableView()
        formatter.dateFormat    = "dd MMM, yyyy"
        txtFieldSearch?.delegate = self
        
        arrayMenu = [["name": "All","id": 1],["name": "Student","id": 2],["name": "Parent","id": 3],["name": "Teacher","id": 4],["name": "Subject/Has word","id": 5]]
        
        intPaggination = 1
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        tblConvSearch?.tableFooterView  = UIView(frame: CGRect.zero)
        tblConvMenu?.tableFooterView    = UIView(frame: CGRect.zero)
        
        spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner.color        =  UIColor(rgb: 0x56B54B)
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            self.intUserId =  Int(userId)
        }
        
        intFilter = 1
        tblConvMenu?.isHidden = true
        heightTextField?.constant = 0
        //        let parent =  self.presentingViewController?.parent as! CustomTabbarViewController
        //        parent.hideBar(boolHide: true)
        
        intBtnFromTo = 1
        thirtyDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -30, to: currentDate)!
        btnFrom?.setTitle(formatter.string(from: thirtyDaysBeforeToday), for: UIControl.State.normal)
        btnTo?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        
        print("currentDate = \(currentDate)")
        print("thirtyDaysBeforeToday = \(thirtyDaysBeforeToday)")
        
        formatter.dateFormat = "yyyy-MM-dd"
        strFromDate = formatter.string(from: thirtyDaysBeforeToday)
        strToDate   = formatter.string(from: currentDate)
        
        // datePicker?.maximumDate = currentDate
        
        selectToDate   = currentDate
        selectFromDate = thirtyDaysBeforeToday
        
        webConversationReport(intPage: intPaggination)
        
        // webConversationReport(intPage: intPaggination)
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //datePicker.minimumDate = thirtyDaysBeforeToday
    }
    //MARK: - ClkBtnFrom
    @IBAction func ClkBtnFrom(_ sender: UIButton) {
        intBtnFromTo = 1
        self.showDatePicker(btnDate: sender)
    }
    //MARK: - ClkBtnTo
    @IBAction func ClkBtnTo(_ sender: UIButton) {
        intBtnFromTo = 2
        self.showDatePicker(btnDate: sender)
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(btnDate: UIButton){
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: intBtnFromTo == 1 ? selectFromDate : selectToDate , minimumDate: nil, maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate.titleLabel?.text) { (date) in
            self.setDateIntoLabel(dateVal : date , lastText :  btnDate.titleLabel?.text)
        }//minimumDate: thirtyDaysBeforeToday
    }
    // MARK: -Set the date into the label after getting response from the date picker
    func setDateIntoLabel(dateVal : Date? ,lastText : String?) {
        if let dateVal = dateVal {
            let dateString = dateVal.dateToString(formatType :"dd MMM, yyyy")
            
            //   formatter.dateFormat = "yyyy-MM-dd"//"dd MMM, yyyy"
            formatter.dateFormat = "dd MMM, yyyy"
            //Validation for from date
            if intBtnFromTo == 1 {
                if  dateVal.compare(selectToDate) == .orderedAscending || dateVal.compare(selectToDate) == .orderedSame {
                    selectFromDate = dateVal
                    btnFrom?.setTitle(dateString, for: UIControl.State.normal)
                    formatter.dateFormat = "yyyy-MM-dd"
                    strFromDate  = formatter.string(from: selectFromDate)
                    webConversationReport(intPage: intPaggination)
                }
                else {
                    WebserviceManager.showAlert(message: "", title: "'From Date' can't be greater than 'To date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
                
                //Validatin for To date
            else {
                if dateVal.compare(selectFromDate) == .orderedDescending || dateVal.compare(selectFromDate) == .orderedSame {
                    selectToDate = dateVal
                    btnTo?.setTitle(dateString, for: UIControl.State.normal)
                    formatter.dateFormat = "yyyy-MM-dd"
                    strToDate = formatter.string(from: selectToDate)
                    webConversationReport(intPage: intPaggination)
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "'To Date' can't be before 'From Date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            if intBtnFromTo == 1 {
                btnFrom?.setTitle(strLastText, for: UIControl.State.normal)
            }
            else {
                btnTo?.setTitle(strLastText, for: UIControl.State.normal)
            }
        }
        
    }
    /*
     //MARK: - DoneDatePicker
     @IBAction func DoneDatePicker(_ sender: Any) {
     
     if intBtnFromTo == 1{
     if (datePicker?.date)! < formatter.date(from: (btnTo?.titleLabel?.text)!)!  {
     btnFrom?.setTitle(formatter.string(from: (datePicker?.date)!), for: UIControl.State.normal)
     formatter.dateFormat = "yyyy-MM-dd"
     strFromDate  = formatter.string(from: (datePicker?.date)!)
     }
     else{
     WebserviceManager.showAlert(message: "", title: "'From Date' can't be before 'To date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
     }, secondBtnBlock: {})
     }
     }
     else if intBtnFromTo == 2{
     if (datePicker?.date)! > formatter.date(from: (btnFrom?.titleLabel?.text)!)!  {
     btnTo?.setTitle(formatter.string(from: (datePicker?.date)!), for: UIControl.State.normal)
     formatter.dateFormat = "yyyy-MM-dd"
     strToDate = formatter.string(from: (datePicker?.date)!)
     }
     else{
     WebserviceManager.showAlert(message: "", title: "'To Date' can't be before 'From Date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
     }, secondBtnBlock: {})
     }
     }
     webConversationReport(intPage: intPaggination)
     }
     */
    func webConversationReport(intPage: Int)
    {
        self.imgViewNoConversation?.isHidden = true
        self.lblNoConversation?.isHidden     = true

        print("Func: webConversationReport")
        Utility.shared.loader()
        // formatter.dateFormat = "dd MMM, yyyy"
        self.tblConvSearch?.isHidden = true
        
        
        print("strFromDate = \(strFromDate)")
        print("strToDate = \(strToDate)")
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        //// &SearchBy=5&FromDate=2018-04-01&TillDate=2018-05-08&query=the
        
        if intFilter == 5{
            urlString    += "Message/Conversation?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intFilter)&FromDate=\(strFromDate)&TillDate=\(strToDate)&pg=\(intPage)&query=\(/txtFieldSearch?.text)"//strDate
        }
        else{
            urlString    += "Message/Conversation?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intFilter)&FromDate=\(strFromDate)&TillDate=\(strToDate)&pg=\(intPage)"//strDate
            
        }
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async {
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblConvSearch?.isHidden = false
                    let object = Mapper<ConversationModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.conversation{
                            self.arrayConversation = boardArray
                            print("arrayConversation Count = \(/self.arrayConversation?.count)")
                            self.tblConvSearch?.reloadData()
                            self.tblConvSearch?.isHidden = false
                        }
                        if self.arrayConversation?.count == 0 || self.arrayConversation?.count == nil ||  parsedObject.conversation == nil || parsedObject.conversation?.count == 0 {

                            self.arrayConversation?.removeAll()
                            self.tblConvSearch?.reloadData()
                            self.imgViewNoConversation?.isHidden = /(self.arrayConversation?.count) != 0
                            self.lblNoConversation?.isHidden     = /(self.arrayConversation?.count) != 0
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    //MARK: - scrollview Delegate method
    /*   func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
     
     if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
     parent.hideBar(boolHide: true)
     }
     }
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
     
     let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
     if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
     parent.hideBar(boolHide: false)
     }
     }
     RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
     }
     */
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            print("fetch more data -- scrollViewDidEndDragging")
            spinner.startAnimating()
            
            intPaggination = intPaggination + 1
            //self.webAPIQuestions(intAll: intAllMyCount, intPage: intCount)
            self.loadChunkSMSReport(intPagination: intPaggination)
        }
    }
    func loadChunkSMSReport(intPagination: Int)
    {
        print("Func: webSMSReport")
        spinner.startAnimating()
        formatter.dateFormat = "dd MMM, yy"
        
        print("strFromDate = \(strFromDate)")
        print("strToDate = \(strToDate)")
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/Conversation?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intFilter)&FromDate=\(strFromDate)&TillDate=\(strToDate)&pg=\(intPagination)"
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                
                switch response.result {
                case .success(let data):
                    self.tblConvSearch?.isHidden = false
                    let object = Mapper<ConversationModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.conversation{
                            for sms in boardArray {
                                self.arrayConversation?.append(sms)
                            }
                            self.tblConvSearch?.reloadData()
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueserachConversationdetails"{
            let segveLikes = segue.destination as! ConversationDetailsViewController
            segveLikes.intMsgID     = intMsgID
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        webConversationReport(intPage: intPaggination)
        
        return true
    }
}
//MARK:- UITableViewDelegate and UITableViewDataSource
extension ConversationSearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblConvMenu {
            return 5
        }else {
            return /arrayConversation?.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblConvMenu {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.conversationMenuCell.get, for: indexPath) as? ConversationSearchMenuTableViewCell else {return UITableViewCell()}
            cell.lblMenu.text = (arrayMenu[indexPath.row]["name"] as! String)
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.conversationSearchCell.get, for: indexPath) as? ConversationSearchTableViewCell else {return UITableViewCell()}
            cell.modal = arrayConversation?[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tblConvMenu {
            intFilter = (arrayMenu[indexPath.row]["id"] as! Int)
            if intFilter == 5 {
                heightTextField?.constant = 30
            }
            else{
                heightTextField?.constant = 0
                webConversationReport(intPage: intPaggination)
                
            }
            self.tblConvMenu?.isHidden =  true
        }
        else{
            intMsgID = /self.arrayConversation?[indexPath.row].msgID
            //self.performSegue(withIdentifier: "segueserachConversationdetails", sender: self)
            let storyboard = UIStoryboard(name: "Reports", bundle: nil)
            guard let conversationDetailsVc = storyboard.instantiateViewController(withIdentifier: "ConversationDetailsViewController") as? ConversationDetailsViewController else {return}
            conversationDetailsVc.intMsgID     = intMsgID
            self.pushVC(conversationDetailsVc)
        }
    }
}
