//  ExecutiveAttendanceReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 19/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
class ExecutiveAttendanceReportViewController: BaseViewController {
    @IBOutlet weak var btnDate:            UIButton?
    @IBOutlet weak var btnShorting:        UIButton?
    @IBOutlet weak var lblShorting:        UILabel?
    @IBOutlet weak var segmentControl:     UISegmentedControl?
    @IBOutlet weak var segmentBgView:      UIView?
    @IBOutlet weak var lblNoRecord:        UILabel?
    @IBOutlet weak var searchHeight:       NSLayoutConstraint?
    @IBOutlet weak var searchBar:          UISearchBar?
    @IBOutlet weak var barBtnView:         UIView?
    @IBOutlet weak var rightBarBtnItem:    UIBarButtonItem?
    @IBOutlet weak var leftBackBarBtnItem: UIBarButtonItem?
    @IBOutlet weak var searchBarItem:      UIBarButtonItem?
    
    var strSchoolCodeSender   = String()
    var currentDate           = Date()
    let formatter             = DateFormatter()
    var thirtyDaysBeforeToday = Date()
    var intSegmentSelection   = Int()
    var selectDate            : Date?
    var intVCselection        = Int()
    var isFirstTime           : Bool = true
    var isSearch              : Bool = false
    var searchActive          : Bool = false
    var reverseBool           : Bool = true
    var arrayAttendance          : [DTL]?
    var arrayCopyAttendance      : [DTL]?
    var arrayCopySearchAttendance: [DTL]?
    var arrayStaffType           : [StaffType]?
    var strStaffID               : String?
    
    // MARK: -  segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl)
    {
        if intVCselection == 1
        {
            if(searchActive){
                if(segmentControl?.selectedSegmentIndex == 0){
                    self.arrayCopySearchAttendance = self.arrayAttendance
                }
                else if(segmentControl?.selectedSegmentIndex == 1){
                    self.arrayCopySearchAttendance = self.arrayAttendance?.filter{($0.isPrasent == true)}
                }
                else if(segmentControl?.selectedSegmentIndex == 2){
                    self.arrayCopySearchAttendance = self.arrayAttendance?.filter{($0.isPrasent == false)}
                }
            }else{
                if(segmentControl?.selectedSegmentIndex == 0){
                    self.arrayCopyAttendance = self.arrayAttendance
                }
                else if(segmentControl?.selectedSegmentIndex == 1){
                    self.arrayCopyAttendance = self.arrayAttendance?.filter{($0.isPrasent == true)}
                }
                else if(segmentControl?.selectedSegmentIndex == 2){
                    self.arrayCopyAttendance = self.arrayAttendance?.filter{($0.isPrasent == false)}
                }
            }
        }
        else {
            if(segmentControl?.selectedSegmentIndex == 0){
                self.arrayCopyAttendance = self.arrayAttendance
            }
            else if(segmentControl?.selectedSegmentIndex == 1){
                self.arrayCopyAttendance = self.arrayAttendance?.filter{($0.isPrasent == true)}
            }
            else if(segmentControl?.selectedSegmentIndex == 2){
                self.arrayCopyAttendance = self.arrayAttendance?.filter{($0.isPrasent == false)}
            }
        }
        intSegmentSelection = (segmentControl?.selectedSegmentIndex)!
        //tableView?.reloadData()
        self.reloadTable()
    }
    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ExecutiveAttendanceCell", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: CellIdentifiers.ExecutiveCell.get)
        
        self.title = intVCselection == 1 ? "Staff's Attendance" : "Executive Attendance"
        
        // searchBar?.showsCancelButton = false
        // searchBar.barTintColor      = UIColor.white
        //searchBar.searchBarStyle    = .minimal
        searchBar?.delegate = self
        self.definesPresentationContext = true
        searchHeight?.constant =  0
        searchBar?.isHidden    =  true
        barBtnView?.isHidden   = intVCselection == 0
        if intVCselection == 0 {
            leftBackBarBtnItem    = nil
            barBtnView?.isHidden  = true
            btnShorting?.isHidden = true
            lblShorting?.isHidden = true
            searchBarItem         = nil
            self.navigationItem.rightBarButtonItems = nil
            
            self.searchBarItem?.isEnabled = false
            self.searchBarItem?.image = UIImage(named: "")
        }
        formatter.dateFormat = "dd MMM, yyyy"
        thirtyDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -30, to: currentDate)!
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        formatter.dateFormat = "yyyy-MM-dd"
        selectDate = currentDate//formatter.string(from: currentDate)
        
        formatter.dateFormat = "dd MMM, yyyy"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        btnDate?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        webAPIStaffType()
    }
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblNoRecord?.isHidden = true
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        formatter.dateFormat = "dd MMM, yy"
        let Date1       = (formatter.date(from: (self.btnDate?.titleLabel?.text)!)!)
        formatter.dateFormat = "yyyy-MM-dd"
        let strFromDate = formatter.string(from: Date1)
        
        if intVCselection == 0 {
            webAPIExecutiveAttendance()
        }
        else { webAPITeacherAttendance(strStaffType: "") }
    }
    //MARK: - ClkBtnFrom
    @IBAction func ClkBtnShort(_ sender: UIButton) {
        
        lblShorting?.text = reverseBool ? "Z\nA" : "A\nZ"
        reverseBool       = !reverseBool
        
        if intVCselection == 1
        {
            if(searchActive){
                self.arrayCopySearchAttendance?.reverse()
            }else{
                self.arrayCopyAttendance?.reverse()
            }
        }
        else {
            self.arrayCopyAttendance?.reverse()
        }
        self.reloadTable()
    }
    //MARK: - ClkBtnFrom
    @IBAction func ClkBtnDate(_ sender: UIButton) {
        self.showDatePicker()
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(){
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: selectDate ?? Date() , minimumDate: thirtyDaysBeforeToday, maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate?.titleLabel?.text) { (date) in
            self.setDateIntoLabel(date: date , lastText :  self.btnDate?.titleLabel?.text)
        }
    }
    //Set the date into the label after getting response from the date picker
    func setDateIntoLabel(date : Date? ,lastText : String?) {
        if let date = date {
            selectDate = date
            let dateString = date.dateToString(formatType :"dd MMM, yyyy")
            btnDate?.setTitle(dateString, for: UIControl.State.normal)
            if intVCselection == 0 {
                webAPIExecutiveAttendance()
            }
            else { webAPITeacherAttendance(strStaffType: "") }
            // self.webAPIExecutiveAttendance()
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            btnDate?.setTitle(strLastText, for: UIControl.State.normal)
        }
    }
    // MARK: -  webAPIStaffType
    func webAPIStaffType() {
        
        guard let user = self.getCurrentUser() else {return}
        
        APIManager.shared.request(with: HomeEndpoint.StaffType(), isLoader : isFirstTime) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handleStaffType(response: success)
            })
        }
    }
    func handleStaffType(response: Any?) {
        
        if let model = response as? StaffTypeModal {
            self.arrayStaffType = model.staffType
        }
    }
    // MARK: -  webAPITeacherAttendance
    func webAPITeacherAttendance(strStaffType: String?) {
        lblNoRecord?.isHidden = true
        
        guard let user = self.getCurrentUser() else {return}
        
        APIManager.shared.request(with: HomeEndpoint.AttRpt(AttDate: selectDate?.dateToString(formatType: "yyyy-MM-dd") ?? "", UserId: Int(user.userID).toString, StaffType: strStaffType), isLoader : true) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response: success)
            })
        }
    }
    func handle(response: Any?) {
        
        if let model = response as? TeacherAtteReportModel {
            
            if /model.dTL?.count > 0 {
                self.arrayAttendance     = model.dTL
                self.arrayCopyAttendance = model.dTL //self.arrayAttendance
                
                if self.intSegmentSelection == 0{}
                else if self.intSegmentSelection == 1{
                    self.arrayCopyAttendance =  model.dTL?.filter{($0.isPrasent == true)}
                }
                else if self.intSegmentSelection == 2{
                    self.arrayCopyAttendance = model.dTL?.filter{($0.isPrasent == false)}
                }
                self.reloadTable()
            }
            else {
                self.arrayAttendance = []
                self.arrayCopyAttendance = model.dTL //self.arrayAttendance
                self.reloadTable()
            }
        }
        if self.arrayCopyAttendance?.count == 0{
            lblNoRecord?.isHidden = /self.arrayCopyAttendance?.count > 0
        }
        lblNoRecord?.isHidden = /self.arrayCopyAttendance?.count > 0
    }
    // MARK: -  webAPIExecutiveAttendance
    func webAPIExecutiveAttendance()
    {
        APIManager.shared.request(with: HomeEndpoint.ExecutiveAttendance(AttDate: selectDate?.dateToString(formatType: "yyyy-MM-dd") ?? ""), isLoader : isFirstTime) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handleExecutive(response: success)
            })        }
    }
    func handleExecutive(response: Any?) {
        
        if let model = response as? TeacherAtteReportModel {
            
            if /model.dTL?.count > 0 {
                self.arrayAttendance     = model.dTL
                self.arrayCopyAttendance = model.dTL //self.arrayAttendance
                
                if self.intSegmentSelection == 0{}
                else if self.intSegmentSelection == 1{
                    self.arrayCopyAttendance =  model.dTL?.filter{($0.isPrasent == true)}
                }
                else if self.intSegmentSelection == 2{
                    self.arrayCopyAttendance = model.dTL?.filter{($0.isPrasent == false)}
                }
                self.reloadTable()
            }
        }
        if self.arrayCopyAttendance?.count == 0{
            lblNoRecord?.isHidden = /self.arrayCopyAttendance?.count > 0
        }
        lblNoRecord?.isHidden = /self.arrayCopyAttendance?.count > 0
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
// Configure TableView of Leave Reason ( ON SEGEMENT CONTROL = 0 )
extension ExecutiveAttendanceReportViewController {
    
    func configureTableView() {
        
        if intVCselection == 1
        {
            if(searchActive){
                tableDataSource = TableViewDataSource(items: arrayCopySearchAttendance, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.ExecutiveCell.get)
            }else{
                tableDataSource = TableViewDataSource(items: arrayCopyAttendance, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.ExecutiveCell.get)
            }
        }
        else {
            tableDataSource = TableViewDataSource(items: arrayCopyAttendance, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.ExecutiveCell.get)
        }
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? ExecutiveAttendanceCell)?.intVCselection = self.intVCselection
            (cell as? ExecutiveAttendanceCell)?.modal          = item
        }
}
 
    func reloadTable() {
        
        
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            if intVCselection == 1
            {
                if(searchActive){
                    tableDataSource?.items = self.arrayCopySearchAttendance
                }else{
                    tableDataSource?.items = self.arrayCopyAttendance
                }
            }
            else {
                tableDataSource?.items = self.arrayCopyAttendance
            }
            //   tableDataSource?.items = self.arrayCopyAttendance
            tableView?.reloadData()
        }
    }
}
extension ExecutiveAttendanceReportViewController: SelectStaffTypeDelegate {
    func getStaffTypeDelegate(selectStaffType: StaffType?, staffTypeArr: [StaffType]?) {
        print("getStaffTypeDelegate")
        print("selectStaffType = \(selectStaffType)")
        print("staffTypeArr = \(staffTypeArr)")
        
        print("staffTypeArr?[index]. = \(staffTypeArr?[0].staffTypeID)")
        let arraySelect =  staffTypeArr?.filter({ (Xyz) -> Bool in
            return Xyz.isSelected
        })
        //            let arraySubName =  arraySelect?.map({ (subject) -> String in
        //                return /subject.staffType
        //            })
        let arraySubID =  arraySelect?.map({ (subject) -> String in
            return /subject.staffTypeID?.toString
        })
        
        // lblSubject?.text = (/arraySubName?.count == 1) ? arraySubName?[0] : (/arraySubName?.count >= 2 ? "\(/arraySubName?[0]) +\(/arraySubName?.count - 1)" : "Select Subject")
        
        strStaffID = /arraySubID?.count > 0 ? arraySubID?.joined(separator: ",") : ""
        
        print("strStaffID = \(/strStaffID)")
        
        if (/strStaffID?.count > 0) {
            // weeklyPlanForStudentAPI(strPg: "\(intPaggination)", strPlanDate:strSelectDate, strSubIDs: strSubID)
            webAPITeacherAttendance(strStaffType: strStaffID)
        }
    }
    //MARK: - openSelectSubjectVc
    func openSelectSubjectVc(){
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "SelectSubjectViewController") as? SelectSubjectViewController {
            vc.arrStaffType    = self.arrayStaffType
            vc.staffDelegate   = self
            vc.fromLeave       = false
            vc.fromAttendance  = true
            vc.tag             = 2
            vc.subjectArr      = []
            self.presentVC(vc)
        }
    }
}
extension ExecutiveAttendanceReportViewController:UISearchBarDelegate {
    @IBAction func ClkSearch(_ sender: Any) {
        searchBar?.resignFirstResponder()
        self.arrayCopySearchAttendance?.removeAll()
        
        if !isSearch {
            searchBar?.isHidden = false
            isSearch = true
            searchHeight?.constant =  44
        }
        else {
            searchBar?.isHidden = true
            isSearch = false
            searchHeight?.constant =  0
            
            searchActive = false
            self.arrayCopySearchAttendance?.removeAll()
            self.dismiss(animated: true, completion: nil)
            tableView?.reloadData()
            lblNoRecord?.isHidden = true
            tableView?.isHidden = false
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 20
    }
    //MARK: searchBar Delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("textDidChange")
        print("searchBar= \(String(describing: searchBar.text))")
        
        searchActive = true
        let searchString = searchBar.text!
        self.arrayCopySearchAttendance = self.arrayCopyAttendance?.filter({ (item) -> Bool in
            let resultText:NSString = /item.name as NSString
            return (resultText.range(of: searchString, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        print("self.arrayCopySearchAttendance \n \(self.arrayCopySearchAttendance)")
        
        if self.arrayCopySearchAttendance?.count == 0 && (searchString.count) > 0 {
            lblNoRecord?.isHidden = false
            tableView?.isHidden   = true
        }
        if self.arrayCopySearchAttendance?.count == 0 && (searchString.count) == 0 {
            lblNoRecord?.isHidden = true
            tableView?.isHidden   = false
            searchActive          = false
            self.reloadTable()
        }
        else{
            lblNoRecord?.isHidden = true
            tableView?.isHidden   = false
            self.reloadTable()
        }
    }
    //    //MARK: searchBar Delegate methods
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
        searchBar.text = nil
        searchActive   = false
        searchBar.resignFirstResponder()
        self.arrayCopySearchAttendance?.removeAll()
        self.dismiss(animated: true, completion: nil)
        lblNoRecord?.isHidden = true
        tableView?.isHidden   = false
        self.reloadTable()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        searchActive = true
        //collectionView.reloadData()
    }
    @IBAction func ClkFilter(_ sender: Any) {
        openSelectSubjectVc()
    }
    
}
