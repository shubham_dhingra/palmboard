//
//  LeavesReportViewController.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 04/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import Foundation

class LeavesReportViewController: BaseViewController {
    
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnFilter : UIButton?
    @IBOutlet weak var btnSearch : UIButton?
    @IBOutlet weak var segmentLeave: UISegmentedControl?
    
    
    //MARK::- VARIABLES
    var leaveListArr : [LeaveList]?
    var refreshControl = UIRefreshControl()
    var isFirstTime : Bool = true
    var leaveUserType : Users?
    
    //MARK::- LIFE CYCLCES
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func onViewDidLoad(){
        reloadTable()
        updateSegmentControl()
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnSearch(_ sender: UIButton) {
        openSearchViewController()
    }
    
    func openSearchViewController(){
        if let searchVC = storyboard?.instantiateViewController(withIdentifier: "SearchLeaveViewController") as? SearchLeaveViewController {
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                parent.hideBar(boolHide: true)
            }
            searchVC.leaveUserType = leaveUserType
            self.presentVC(searchVC)
        }
    }
    @IBAction func btnFilter(_ sender: UIButton) {
           sender.isSelected = !sender.isSelected
          getLeaveList(pageNo : 1 , order : sender.isSelected ?  1 : 2)
    }
    //Segment Control
    @IBAction func btnSegmentAct(_ sender: Any){
        updateSegmentControl()
        self.view.layoutIfNeeded()
    }
    
    func updateSegmentControl(){
        getLeaveList(pageNo: 1, order: 2)
    }
}
extension LeavesReportViewController {
    func reloadTable() {
        if leaveListArr?.count == nil  || leaveListArr?.count == 0 {
            configureLeaveDataTable()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = leaveListArr
            tableView?.reloadData()
        }
    }
}

// Configure TableView of Leave Data ( ON SEGEMENT CONTROL = 1 )
extension LeavesReportViewController {
    
    //MARK: - ConfigureTableView
    func configureLeaveDataTable() {
        tableDataSource = TableViewDataSource(items: leaveListArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.LeaveDetailCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? LeaveDetailCell)?.index  = indexpath?.row
            (cell as? LeaveDetailCell)?.model = item
            (cell as? LeaveDetailCell)?.leaveUserType = self.leaveUserType
            (cell as? LeaveDetailCell)?.delegate = self
         }
    }
}


extension LeavesReportViewController : DeleteLeaveDelegate{
    
    //MARK::- LeaveStatusUpdate
    func leaveStatusUpdate(leaveAction: String, index: Int) {
        //Approved Action
        if index < /leaveListArr?.count {
            let lvID = leaveListArr?[index].lvID?.toString
            leaveActions(LvID: /lvID, Action: leaveAction == LeaveStatus.Approved.get ? "1" : "2" ,index: index)
        }
    }
    
    
    func leaveActions(LvID : String , Action : String ,index : Int){
        
        APIManager.shared.request(with: HomeEndpoint.LeaveAction(UserID: userID, Action: Action, LvID: LvID , UserType: userType)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                if let modal = response as? LeaveDetailsModel {
                    if modal.errorCode == 0 {
                    Messages.shared.show(alert: .success, message: Action == "1" ?
                        AlertMsg.leaveAppproveSuccess.get : AlertMsg.leaveRejectSuccess.get , type: .success)
                    self?.leaveListArr?.remove(at: index)
                    self?.reloadTable()
                    }
                    else {
                        Messages.shared.show(alert: .oops, message: R.string.localize.somethingWentWrong(), type: .warning)
                    }
                    
                }
               
            })
        }
    }
}

//MARK::- Leave List -(Status API)
extension LeavesReportViewController {
    
    func getLeaveList(pageNo : Int ,order : Int) {
        if leaveUserType == nil {
            leaveUserType = .Student
        }
        APIManager.shared.request(with: HomeEndpoint.LeaveReport(UserID: userID, Status: self.segmentLeave?.selectedSegmentIndex.toString, ord: order.toString, UserType: leaveUserType == .Student ? "1" : "3" , pg: pageNo.toString),isLoader: isFirstTime) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handleLeaveListResponse(response: response, pageNo : pageNo)
            })
        }
    }
    
    func handleLeaveListResponse(response: Any? ,pageNo : Int){
        if let response = response as? LeaveDetailsModel {
            if let leaveList = response.leaveList {
            print("Data from page No :\(pageNo)")
            if pageNo == 1 {
                self.leaveListArr = leaveList
            }
            else {
                _ = leaveList.map{(self.leaveListArr?.append($0))}
            }
            }
            else {
             self.leaveListArr = []
            }
            self.lblNoData?.isHidden = self.leaveListArr?.count != 0
            reloadTable()
        }
    }
}
