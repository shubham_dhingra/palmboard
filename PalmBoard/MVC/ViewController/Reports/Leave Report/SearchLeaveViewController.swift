//
//  SearchLeaveViewController.swift
//  e-Care Pro
//
//  Created by ShubhamMac on 29/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
import Foundation
import IQKeyboardManager


class SearchLeaveViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var txtSearch : UITextField?
    @IBOutlet weak var btnFilter : UIButton?
    @IBOutlet weak var lblLeaveType : UILabel?
    @IBOutlet weak var tblDropDown :UITableView?
    @IBOutlet weak var headerView : UIView?
    @IBOutlet weak var btnOpenDatePicker : UIButton?
    @IBOutlet weak var lblNodata : UILabel?
    
    //MARK::- VARIABLES
    var leaveListArr : [LeaveList]?
    var refreshControl = UIRefreshControl()
    var ClassID : String?
    var searchText : String?
    var classArr : [MyClasses]?
    var selectDate : Date?
    var leaveUserType : Users?
    var containerView : UIView!
    var isFirstTime : Bool = true
    var isFilterPressed : Bool = false
    var selectedLeaveActionType : LeaveStatus = .Pending
    var selectedFilterType : FilterType = .Name
    lazy var dropDownArr : [LeaveStatus] =  [.Pending , .Approved , .Rejected]
    lazy var filterTypeArr : [FilterType] = [.Name , .Date , .Class]
    var tableDropDownDataSource : TableViewDataSource?{
        didSet{
            
            tblDropDown?.dataSource = tableDropDownDataSource
            tblDropDown?.delegate = tableDropDownDataSource
            tblDropDown?.reloadData()
        }
    }
    
    
    lazy var sessionStart : Date = {
        if let sessionStart = self.getCurrentUser()?.sessionStart {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            return dateFormatter.date(from: sessionStart) ?? Date()
        }
        return Date()
    }()
    
    lazy var sessionEnd : Date = {
        if let sessionEnd = self.getCurrentUser()?.sessionEnd {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            return dateFormatter.date(from: sessionEnd) ?? Date()
        }
        return Date()
    }()
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
        if leaveUserType == .Staff {
            //remove the class filter in case of teacher leave request
            filterTypeArr.removeLast()
        }
        setUpForIphoneX()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        onViewWillApppear()
    }
    
    func onViewWillApppear(){
        navigationController?.setNavigationBarHidden(true, animated: false)
        btnOpenDatePicker?.isUserInteractionEnabled = false
        Utility.shared.setUpTextFieldPlacholderUI(txtSearch , placeholderText : FilterType.Name.placholderText , placeholderFont : R.font.ubuntuItalic(size: 15.0)! , placeholderTextColor  : UIColor.flatGray)
        self.lblLeaveType?.text = self.selectedLeaveActionType.get
        
    }
    
    //MARK::- BUTTON ACTIONS
    @IBAction func btnOpenDatePickerAct(_ sender : UIButton){
        self.tblDropDown?.isHidden = true
        self.view.endEditing(true)
        if self.selectedFilterType == .Date{
            openDatePicker()
        }
        if self.selectedFilterType == .Class{
            if classArr != nil {
                openSelectClassVc()
            }
            else {
                getClass()
            }
        }
    }
    
    
    func getClass(){
        
        APIManager.shared.request(with: HomeEndpoint.myClasses(UserID: userID), isLoader : false) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                if let model = response as? ContactsModel ,let classArr = model.myClasses{
                    self?.classArr = classArr
                    self?.openSelectClassVc()
                }
            })
        }
    }
    
    
    
    func openSelectClassVc(){
        if let selectClassVc = self.storyboard?.instantiateViewController(withIdentifier: "SelectClassViewController") as? SelectClassViewController {
            selectClassVc.classArr = classArr
            selectClassVc.tempClassArr = classArr
            selectClassVc.delegate = self
            self.presentVC(selectClassVc)
        }
    }
    
    
    @IBAction func btnFilterOptionAct(_ sender : UIButton){
        isFilterPressed = true
        openDropDownTable(sender : sender)
        
    }
    
    @IBAction func btnLeaveActionTypeAct(_ sender : UIButton){
        isFilterPressed = false
        openDropDownTable(sender : sender)
    }
}

extension SearchLeaveViewController {
    
    func onViewDidLoad(){
        
        tblDropDown?.isHidden = true
        reloadTable()
    }
    
    //Relaod LeaveList Data table
    func reloadTable() {
        if leaveListArr?.count == nil  || leaveListArr?.count == 0 {
            configureLeaveDataTable()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = leaveListArr
            tableView?.reloadData()
        }
    }
}

// Configure TableView of Leave Data ( ON SEGEMENT CONTROL = 1 )
extension SearchLeaveViewController {
    
    //MARK: - ConfigureTableView
    func configureLeaveDataTable() {
        tableDataSource = TableViewDataSource(items: leaveListArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.LeaveDetailCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? LeaveDetailCell)?.index  = indexpath?.row
            (cell as? LeaveDetailCell)?.model = item
            (cell as? LeaveDetailCell)?.delegate = self
            
        }
    }
}

extension SearchLeaveViewController : DeleteLeaveDelegate{
    
    //MARK::- LeaveStatusUpdate
    func leaveStatusUpdate(leaveAction: String, index: Int) {
        //Approved Action
        if index < /leaveListArr?.count {
            let lvID = leaveListArr?[index].lvID?.toString
            leaveActions(LvID: /lvID, Action: leaveAction == LeaveStatus.Approved.get ? "1" : "2" ,index: index)
        }
    }
    
    
    func leaveActions(LvID : String , Action : String ,index : Int){
        
        APIManager.shared.request(with: HomeEndpoint.LeaveAction(UserID: userID, Action: Action, LvID: LvID , UserType: userType)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (response) in
                Messages.shared.show(alert: .success, message: Action == "1" ?
                    AlertMsg.leaveAppproveSuccess.get : AlertMsg.leaveRejectSuccess.get , type: .success)
                self?.leaveListArr?.remove(at: index)
                self?.reloadTable()
            })
        }
    }
}

//MARK::- DROP DOWN
extension SearchLeaveViewController {
    
    func openDropDownTable(sender : UIButton){
        txtSearch?.endEditing(true)
        self.view.endEditing(true)
        
        
        let dropDownFrame = isFilterPressed ?  CGRect(x: /headerView?.frame.width - 210.0, y: /headerView?.frame.height, width: 210.0, height: CGFloat(filterTypeArr.count * 45))
            :  CGRect(x: sender.frame.origin.x - 16.0, y: /headerView?.frame.height, width: 120.0, height: 135.0)
        UIView.animate(withDuration: 0.2) {
            self.setUpTableView(dropDownFrame)
        }
    }
    
    
    func setUpTableView(_ dropDownFrame : CGRect){
        
        containerView = UIView(frame:dropDownFrame)
        tblDropDown?.frame = containerView.bounds
        containerView.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        containerView.layer.shadowOpacity = 4.0
        
        self.view.addSubview(containerView)
        containerView.addSubview(self.tblDropDown!)
        
        tblDropDown?.isHidden = false
        tblDropDown?.layer.masksToBounds = true
        tblDropDown?.isScrollEnabled = false
        configureDropDown()
    }
    
    func configureDropDown() {
        
        //MARK: - ConfigureTableView
        tableDropDownDataSource = TableViewDataSource(items: isFilterPressed ? filterTypeArr : dropDownArr, height: 45.0, tableView: tblDropDown, cellIdentifier:  isFilterPressed ?  CellIdentifiers.FilterDropDownCell.get : CellIdentifiers.DropDownCell.get)
        
        tableDropDownDataSource?.configureCellBlock = {(cell, item, indexpath) in
            
            //not change the below order
            if self.isFilterPressed {
                (cell as? FilterDropDownCell)?.previousFilterType = self.selectedFilterType
                (cell as? FilterDropDownCell)?.model = item
            }
            else {
                (cell as? DropDownCell)?.previousLeaveType = self.selectedLeaveActionType
                (cell as? DropDownCell)?.model = item
            }
        }
        tableDropDownDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    
    func didSelect(_ index : Int) {
        if self.isFilterPressed {
            if self.selectedFilterType != self.filterTypeArr[index] {
                self.selectedFilterType = self.filterTypeArr[index]
                updateUI()
            }
        }
        else {
            if self.selectedLeaveActionType != self.dropDownArr[index] {
                self.selectedLeaveActionType = self.dropDownArr[index]
                self.lblLeaveType?.text = self.selectedLeaveActionType.get
                leaveListArr = []
                tableDataSource?.items = leaveListArr
                tableView?.reloadData()
            }
            if searchText?.trimmed().count != 0 && searchText != nil{
                 self.getLeaveList(pageNo : 1)
            }
            self.containerView.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.2) {
            self.tblDropDown?.isHidden = true
        }
    }
    
    func updateUI(){
        txtSearch?.text = nil
        if isFilterPressed {
            Utility.shared.setUpTextFieldPlacholderUI(txtSearch , placeholderText : selectedFilterType.placholderText , placeholderFont :  R.font.ubuntuItalic(size: 15.0)! , placeholderTextColor  : UIColor.flatGray)
            btnOpenDatePicker?.isUserInteractionEnabled = self.selectedFilterType == .Date || self.selectedFilterType == .Class
            txtSearch?.isUserInteractionEnabled = self.selectedFilterType == .Name
            txtSearch?.text = nil
            leaveListArr = []
            tableDataSource?.items = leaveListArr
            tableView?.reloadData()
        }
    }
}

//MARK::- Text Field Delegate
extension SearchLeaveViewController : UITextFieldDelegate {
    
    //    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    //
    //        if textField == txtSearch && /txtSearch?.text?.trimmed().count != 0{
    //            getLeaveList(pageNo: 1)
    //        }
    //        if txtSearch?.text?.trimmed().count == 0 {
    //            leaveListArr = []
    //            reloadTable()
    //        }
    //        return true
    //    }
    //
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        if textField == txtSearch && /txtSearch?.text?.trimmed().count != 0{
    //            getLeaveList(pageNo: 1)
    //        }
    //        if txtSearch?.text?.trimmed().count == 0 {
    //            leaveListArr = []
    //            reloadTable()
    //        }
    //        return true
    //    }
    //
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tblDropDown?.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if string.isEmpty{
            if let searchtext = searchText {
                searchText = String(searchtext.dropLast())
            }
        }
        else{
            searchText = textField.text! + string
        }
        print(/searchText)
        if searchText?.trimmed().count == 0 {
            leaveListArr = []
            reloadTable()
        }
        else {
            getLeaveList(pageNo: 1)
        }
        return true
    }
}

//MARK::- Leave List -(Status API)
extension SearchLeaveViewController {
    
    func getLeaveList(pageNo : Int) {
//        self.view.endEditing(true)
        tblDropDown?.isHidden = true
        APIManager.shared.request(with: HomeEndpoint.SearchLeave(UserID: userID, Status:selectedLeaveActionType.id , ord: "2", SearchBy: selectedFilterType.id, pg: pageNo.toString, StName: selectedFilterType == FilterType.Name ? txtSearch?.text?.trimmed() : nil, ClassID: selectedFilterType == FilterType.Class ? ClassID : nil, LeaveDate: selectedFilterType == FilterType.Date ?  selectDate?.dateToString(formatType: "yyyy-MM-dd") : nil, UserType: self.leaveUserType == .Student ? "1" : "3"), isLoader : false) { [weak self] (response) in
            
            self?.handleResponse(response: response, responseBack: { (response) in
                self?.handleLeaveListResponse(response: response, pageNo : pageNo)
            })
        }
    }
    
    func handleLeaveListResponse(response: Any? ,pageNo : Int){
        if let response = response as? LeaveDetailsModel , let leaveList = response.leaveList{
            print("Data from page No :\(pageNo)")
            if pageNo == 1 {
                self.leaveListArr = leaveList
            }
            else {
                _ = leaveList.map{(self.leaveListArr?.append($0))}
            }
            lblNodata?.isHidden = true
            reloadTable()
        }
        else {
            lblNodata?.isHidden = false
            self.leaveListArr = []
            tableDataSource?.items = self.leaveListArr
            reloadTable()
        }
    }
}


extension SearchLeaveViewController  {
    
    // open Date Picker
    func openDatePicker() {
        txtSearch?.endEditing(true)
        txtSearch?.isUserInteractionEnabled = false
        let lastText = txtSearch?.text
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: selectDate ?? Date() , minimumDate: sessionStart, maximumDate: sessionEnd, datePickerMode: .date , lastText: lastText) { (date) in
            self.setDateIntoLabel(date: date , lastText : lastText)
        }
    }
    
    //Set the date into the label after getting response from the date picker
    func setDateIntoLabel(date : Date? ,lastText : String?) {
        if let date = date {
            selectDate = date
            let dateString = date.dateToString(formatType :"dd MMM, yyyy")
            txtSearch?.text = dateString
        }
        else {
            txtSearch?.text = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
        }
        getLeaveList(pageNo: 1)
    }
}


extension SearchLeaveViewController : SearchByClassDelegate {
    
    
    
    func getSelectClass(selectClass: MyClasses?  , classArr : [MyClasses]?) {
        if let selectClass = selectClass {
            
            leaveListArr = []
            tableDataSource?.items = leaveListArr
            tableView?.reloadData()
            txtSearch?.text = selectClass.ClassName
            ClassID = selectClass.classID?.toString
            self.classArr = classArr
            getLeaveList(pageNo: 1)
        }
    }
}

extension SearchLeaveViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != tblDropDown {
            self.animated()
            tblDropDown?.isHidden = true
        }
    }
}
