
//
//  SelectClassViewController.swift
//  e-Care Pro
//
//  Created by ShubhamMac on 30/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit
protocol SearchByClassDelegate {
    func getSelectClass(selectClass : MyClasses? , classArr : [MyClasses]?)
}

protocol SelectSubjectDelegate {
    func getSelectSubject(selectSubject : Subjects? , subjectArr : [Subjects]?)
}


class SelectClassViewController: BaseViewController {
    
    @IBOutlet weak var tableViewHeight : NSLayoutConstraint?
    @IBOutlet weak var lblHeading : UILabel?
    
    //MARK::- VARIABLES
    var isFirstTime : Bool = true
    var delegate : SearchByClassDelegate?
    var subDelegate : SelectSubjectDelegate?
    var classArr : [MyClasses]?
    var tempClassArr : [MyClasses]?
    var selectedClass : MyClasses?
    var selectedSubject : Subjects?
    var subjectArr : [Subjects]?
    var tag : Int?
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        configureClassTable()
        adjustTableViewHeight()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isNavBarHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = false
    }
    
    func adjustTableViewHeight() {
        
        var itemsCount : Int?
        if tag == 2 {
            guard let subjectCount = self.subjectArr?.count else {return}
            itemsCount = subjectCount
        }
        else {
            guard let classCount = self.classArr?.count else {return}
            itemsCount = classCount
        }
        
        if /itemsCount != 0 && /itemsCount <= 5 {
            tableViewHeight?.constant = CGFloat(Double(/itemsCount) * 45.0)
            tableView?.isScrollEnabled = false
        }
        else {
            tableViewHeight?.constant = 225.0
            tableView?.isScrollEnabled = true
        }
        tableView?.reloadData()
        lblHeading?.text = tag == 2 ? IOSMessages.SelectSubject.get : IOSMessages.SelectClass.get
    }
    
    //BUTTON ACTION SUBMIT
    @IBAction func btnOkAct(_ sender: UIButton){
        
        //for subjects
        if /tag == 2 {
            if let selectSubject = selectedSubject {
                subDelegate?.getSelectSubject(selectSubject: selectSubject, subjectArr: subjectArr)
               if  self.navigationController == nil {
                dismissVC(completion: nil)
               } else{
                self.navigationController?.popViewController(animated: false)
                }
            }
        }
            
            //for class
        else {
            if let selectedClass = selectedClass {
                delegate?.getSelectClass(selectClass: selectedClass, classArr: classArr)
                if  self.navigationController == nil {
                    dismissVC(completion: nil)
                } else{
                    self.navigationController?.popViewController(animated: false)}
                
                
            }
        }
    }
    
    override func btnBackAct(_ sender: UIButton) {
        if  self.navigationController == nil {
           
            dismissVC(completion: nil)
        } else{
            self.navigationController?.popViewController(animated: false)
        }
    }
}

extension SelectClassViewController {
    func configureClassTable() {
        
        if tag == 2 && selectedSubject == nil{
            subjectArr?.forEach({ (subject) in
                subject.isSelected = false
            })
          
        }
        
        if tag == 1 && selectedClass == nil{
            classArr?.forEach({ (classArr) in
                classArr.isSelected = false
            })
        }
        
        //MARK: - ConfigureTableView
        tableDataSource = TableViewDataSource(items: /tag == 2 ? subjectArr : classArr, height: UITableView.automaticDimension,tableView: tableView, cellIdentifier: CellIdentifiers.DropDownCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? DropDownCell)?.model = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ selectedIndex : Int){
        
        if tag == 2 {
            for  (index,_) in (subjectArr ?? []).enumerated() {
                subjectArr?[index].isSelected = index == selectedIndex ? true : false
            }
            selectedSubject = subjectArr?[selectedIndex]
            tableDataSource?.items = subjectArr
        }
        else {    
            for  (index,_) in (classArr ?? []).enumerated() {
                classArr?[index].isSelected = index == selectedIndex ? true : false
            }
            selectedClass = classArr?[selectedIndex]
            tableDataSource?.items = classArr
        }
        tableView?.reloadData()
    }
}
