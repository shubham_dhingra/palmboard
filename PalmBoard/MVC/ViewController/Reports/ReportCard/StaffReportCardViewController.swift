//  StaffReportCardViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 05/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class StaffReportCardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UISearchControllerDelegate {
    
    @IBOutlet weak var collectionViewMenuReportCard: UICollectionView?
    @IBOutlet weak var collectionViewGridReportCard: UICollectionView?
    @IBOutlet weak var heightCollection:             NSLayoutConstraint?
    @IBOutlet weak var imgViewNoReportCard :         UIImageView?
    @IBOutlet weak var lblNoReportCard :             UILabel?
    @IBOutlet weak var lblNoStudents   : UILabel?
    
    var userId                     = Int()
    var strSchoolCodeSender        = String()
    var userType                   = Int()
    var arrayDataList              = [[String: Any]]()
    var arrayReportCard            = [[String: Any]]()
    var arraySearchAssignmentList  = [[String: Any]]()
    var otherUserId                = Int()
    var otherUserType              = Int()
    var otherUserPhoto             = String()
    var otherUserSubject           = String()
    var btnTitle                   = UIButton()
    var btnSearch                  = UIButton()
    var intClassId                 = Int()
    var eventIndex                 = Int()
    
    var strClassPass            = String()
    var strName                 = String()
    var strPhoto                = String()
    
    var filtered             :[String]   = []
    var searchActive         : Bool = false
    let searchController     = UISearchController(searchResultsController: nil)
    
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.eventIndex = 0
        heightCollection?.constant = 45
    }
    // MARK: -  viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Report Card", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        btnSearch                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
        btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
        btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation     = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search By Name"
        searchController.searchBar.sizeToFit()
        self.searchController.searchBar.alpha = 0
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.userId =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.userType =  Int(userType)
        }
        if let ClassId = self.getCurrentUser()?.classID{
            self.intClassId =  Int(ClassId)
        }
        self.collectionViewGridReportCard?.delegate   = nil
        self.collectionViewGridReportCard?.dataSource = nil
        
        self.collectionViewMenuReportCard?.delegate   = nil
        self.collectionViewMenuReportCard?.dataSource = nil
        webApiClassList()
    }
    //MARK: searchBar Delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchActive = true
        let searchString = searchController.searchBar.text//?.trimmed()
        self.arraySearchAssignmentList = self.arrayReportCard.filter({ (item) -> Bool in
            let resultText: NSString = item["StudentName"] as! NSString
            return (resultText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        searchActive = self.arraySearchAssignmentList.count == 0 ?  false : true

        collectionViewGridReportCard?.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchController.searchBar.alpha = 0
        //self.navigationItem.titleView = nil
        self.navigationItem.titleView = btnTitle
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        searchActive = false
        self.arraySearchAssignmentList.removeAll()
        self.dismiss(animated: true, completion: nil)
        collectionViewGridReportCard?.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    // MARK: - ClkBtnSearch
    @objc func ClkBtnSearch(sender:UIButton)
    {
        self.searchController.searchBar.alpha = 1
        self.searchController.searchBar.showsCancelButton = true
        searchController.searchBar.becomeFirstResponder()
        self.navigationItem.titleView = searchController.searchBar
        self.navigationItem.rightBarButtonItem = nil
    }
    // MARK: - viewDidLayoutSubviews
    override func viewDidLayoutSubviews() {
        // self.collectionViewGrid.collectionViewLayout.invalidateLayout()
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionViewGridReportCard?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: -  segmentChanged
    func webApiClassList()
    {
        Utility.shared.loader()
        collectionViewGridReportCard?.isHidden = true
        collectionViewMenuReportCard?.isHidden = true
        
        var urlString = String()
        var strUserID : String?
        if let userID = self.getCurrentUser()?.userID {
            strUserID = String(userID)
        }
        urlString += urlString.webAPIDomainNmae()
        urlString += "teacher/StudentInClass?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(/strUserID)"
        
        print("urlString = \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString)
        { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("List result \n \(result)")
                    DispatchQueue.main.async {
                        self.collectionViewGridReportCard?.isHidden = false
                        self.collectionViewMenuReportCard?.isHidden = false
                        
                        
                        if let allTeacher = (result["StudentInClass"] as? [[String: Any]]){
                            self.arrayDataList = allTeacher
                        }
                        if self.arrayDataList.count == 0 {
                            WebserviceManager.showAlert(message: "", title: "Class List is not found", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                                
                            }, secondBtnBlock: {})
                        }
                        else{
                            
                            
                            self.heightCollection?.constant = 45
                            
                            self.collectionViewGridReportCard?.delegate   = nil
                            self.collectionViewGridReportCard?.dataSource = nil
                            self.collectionViewGridReportCard?.isHidden   = false
                            
                            self.collectionViewMenuReportCard?.isHidden   = false
                            self.collectionViewMenuReportCard?.delegate   = self
                            self.collectionViewMenuReportCard?.dataSource = self
                            self.collectionViewMenuReportCard?.reloadData()
                            
                            //self.collectionViewMenu.backgroundColor = UIColor.red
                            // self.collectionViewGrid.backgroundColor = UIColor.yellow
                            
                            self.collectionViewMenuReportCard?.performBatchUpdates({},
                                                                                   completion: { (finished) in
                                                                                    print("collection-view finished reload done")
                                                                                    
                                                                                    self.collectionViewMenuReportCard?.collectionViewLayout.invalidateLayout()
                                                                                    let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                                                                                    self.collectionViewMenuReportCard?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                                                                                    self.collectionView(self.collectionViewMenuReportCard!, didSelectItemAt: IndexPath(item: 0, section: 0))
                            })
                        }
                    }
                }
            }
            if (error != nil){
                DispatchQueue.main.async{
                    self.collectionViewGridReportCard?.isHidden = error != nil
                    self.collectionViewMenuReportCard?.isHidden = error != nil
                    
                    self.view.isUserInteractionEnabled = true
                    if let code = errorCode {
                        switch (code){
                        case Int(-1009):
                            Utility.shared.internetConnectionAlert()
                        default:
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                            print("errorcode = \(String(describing: code))")
                        }
                    }
                }
            }
        }
    }
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1{
            print("self.arrayDataList.count= \(self.arrayDataList.count)")
            return self.arrayDataList.count
        }
        else{
            if(searchActive){
                self.lblNoReportCard?.isHidden = true
                self.lblNoStudents?.text = "No Student Found"
                self.lblNoStudents?.isHidden = self.arraySearchAssignmentList.count != 0
                return self.arraySearchAssignmentList.count
            }else{
                if self.searchController.searchBar.text?.trimmed().count == 0 {
                    return self.arrayReportCard.count
                }
                else {
                    return self.arraySearchAssignmentList.count
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 1
        {
            let cellMenu = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRCRMenu", for: indexPath) as! RCRMenu
            if let strClassName = arrayDataList[indexPath.row]["ClassName"] as? String
            {
                cellMenu.lblClassNameRCR?.text = strClassName
            }
            cellMenu.lblLineReportCard?.tag = indexPath.row + 1
            
            if eventIndex == indexPath.row {
                cellMenu.lblLineReportCard?.backgroundColor = UIColor.flatGreen
                cellMenu.lblClassNameRCR?.textColor         = UIColor.flatGreen
                
            }else {
                cellMenu.lblLineReportCard?.backgroundColor = UIColor.clear
                cellMenu.lblClassNameRCR?.textColor         = UIColor(rgb: 0xD5D5D5)
            }
            cellMenu.lblLineReportCard!.isHidden = false
            // print("cell.lblLineReportCard = \(cellMenu.lblLineReportCard?.frame)")
            // print("lblClassName = \(cellMenu.lblClassNameRCR?.frame)")
            
            return cellMenu
        }
        else{
            let cellGrid = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRCRGrid", for: indexPath) as! RCRGrid
            
            cellGrid.btnImage?.layer.cornerRadius  = (cellGrid.btnImage?.frame.width)! / 2
            cellGrid.btnImage?.layer.masksToBounds = true
            cellGrid.btnImage?.tag                 = indexPath.row
            if(self.arrayDataList.count == 0){
                WebserviceManager.showAlert(message: "", title: "Class List is not found for this class", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                }, secondBtnBlock: {})
                
            }else{
                
                if self.arrayReportCard.count > 0 {
                    if (searchActive){
                        
                        if let imgUrl = self.arraySearchAssignmentList[indexPath.row]["Photo"] as? String
                        {
                            var strPrefix      = String()
                            strPrefix          += strPrefix.imgPath()
                            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            let url            = URL(string: finalUrlString )
                            // print("imgUrl \n \(url!)")
                            let imgViewUser = UIImageView()
                            imgViewUser.af_setImage(withURL: url!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                                
                                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                                cellGrid.btnImage?.setBackgroundImage(img1, for: .normal)
                            }
                        }
                        if let name = self.arraySearchAssignmentList[indexPath.row]["StudentName"] as? String{
                            cellGrid.lblName?.text = name
                        }
                    }
                    else{
                        if let name = self.arrayReportCard[indexPath.row]["StudentName"] as? String{
                            cellGrid.lblName?.text = name
                        }
                        
                        if let imgUrl = self.arrayReportCard[indexPath.row]["Photo"] as? String
                        {
                            var strPrefix      = String()
                            strPrefix          += strPrefix.imgPath()
                            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                            let url            = URL(string: finalUrlString )
                            let imgViewUser = UIImageView()
                            imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                                
                                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                                cellGrid.btnImage?.setBackgroundImage(img1, for: .normal)
                            }
                        }
                        
                    }
                }
            }
            return cellGrid
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView.tag == 1
        {
            
            if let array = arrayDataList[indexPath.row]["Students"] as? [[String: Any]]
            {
                self.arrayReportCard = array
                self.arraySearchAssignmentList = array
                collectionViewMenuReportCard?.setContentOffset(CGPoint.zero, animated: true)
                collectionViewMenuReportCard?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                
                eventIndex = indexPath.row
                collectionView.reloadData()
                
                self.collectionViewGridReportCard?.delegate   = self
                self.collectionViewGridReportCard?.dataSource = self
                
                self.collectionViewGridReportCard?.reloadData()
                self.collectionViewGridReportCard?.isHidden = false
            }
            else{
                collectionViewGridReportCard?.isHidden = true
                collectionViewMenuReportCard?.setContentOffset(CGPoint.zero, animated: true)
                collectionViewMenuReportCard?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                
                eventIndex = indexPath.row
                collectionView.reloadData()
            }
        }
        else
        {
            cellTab(row: indexPath.row)
        }
        self.lblNoStudents?.isHidden = true
        self.lblNoReportCard?.isHidden = false
        self.lblNoReportCard?.text = "No Report Card Available"
        self.imgViewNoReportCard?.isHidden = !(collectionViewGridReportCard?.isHidden)!
        self.lblNoReportCard?.isHidden = !(collectionViewGridReportCard?.isHidden)!
        
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 2{
            return CGSize(width: self.view.frame.width/2, height: 170)
        }
        else {
            return CGSize(width: 100, height: 45)
        }
    }
    // MARK: -  imagePressed
    func cellTab(row: Int) {
       
        var arraySend = [[String: Any]]()
        if self.arraySearchAssignmentList.count > 0 {
            arraySend = self.arraySearchAssignmentList
        }
        else {
            arraySend = self.arrayReportCard
        }
        
        if let userId1 = arraySend[row]["UserID"] as? Int
        {
            self.otherUserId      = userId1
        }else{
            self.otherUserId      = 0
        }
        if let userType1 = arraySend[row]["UserType"] as? Int
        {
            self.otherUserType    = userType1
            
        }else{
            self.otherUserType    = 0
        }
        
        if let strClassPass1 = arraySend[row]["ClassName"] as? String
        {
            self.strClassPass      = strClassPass1
        }else{
            self.strClassPass      = ""
        }
        if let strName1 = arraySend[row]["StudentName"] as? String
        {
            self.strName      = strName1
        }else{
            self.strName      = ""
        }
        if let strPhoto1 = arraySend[row]["Photo"] as? String
        {
            self.strPhoto      = strPhoto1
        }else{
            self.strPhoto      = ""
        }
        openStaffReport()
    }
    func openStaffReport() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let reportCardvc = storyboard
            .instantiateViewController(withIdentifier: "reportStoryboardID") as? ReportCardViewController {
            reportCardvc.intUserID      = self.otherUserId
            reportCardvc.intCheckStaff  = 11
            
            reportCardvc.strClass      = self.strClassPass
            reportCardvc.strName       = self.strName
            reportCardvc.strPhoto      = self.strPhoto
            reportCardvc.USERTYPE      = self.otherUserType
            self.pushVC(reportCardvc)
        }
    }
}
extension StaffReportCardViewController {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
        return totalCharacters <= 70
    }
}



class RCRGrid: UICollectionViewCell {
    @IBOutlet weak var btnImage:   UIButton?
    @IBOutlet weak var lblName:    UILabel?
}
class RCRMenu: UICollectionViewCell
{
    @IBOutlet weak var lblClassNameRCR:   UILabel?
    @IBOutlet weak var lblLineReportCard: UILabel?
}
