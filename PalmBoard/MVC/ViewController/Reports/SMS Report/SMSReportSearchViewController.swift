//  SMSReportSearchViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 24/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire

class SMSReportSearchViewController: UIViewController,UISearchBarDelegate {
   
    @IBOutlet weak var tblSMSReportSearch: UITableView?
    @IBOutlet weak var lblNoRecord:        UILabel?

    var strSchoolCodeSender  = String()
    var intCountryCode       = Int()
    var intPage              = Int()
    var spinner              = UIActivityIndicatorView()
    
    var arraySMSReport: [SMS]?
    var searchBar: UISearchBar?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSMSReportSearch?.isHidden = true
        lblNoRecord?.isHidden        = true
      
        searchBar                     = UISearchBar()
        searchBar?.placeholder         = "Search by Mobile No."
        searchBar?.showsCancelButton   = true
        searchBar?.barTintColor        = UIColor.black
        searchBar?.searchBarStyle      = .minimal
        searchBar?.returnKeyType       = .search
        searchBar?.delegate            = self
        searchBar?.keyboardType        = UIKeyboardType.numbersAndPunctuation
//        searchBar?.keyboardType        = .numberPad
        self.navigationItem.titleView = searchBar

        
        spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner.color        =  UIColor(rgb: 0x56B54B)
        spinner.stopAnimating()
        spinner.hidesWhenStopped = true
        
        spinner.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        let nib = UINib(nibName: "StudentSMSCell", bundle: nil)
        tblSMSReportSearch?.register(nib, forCellReuseIdentifier: "studentCell")
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let check = self.getCurrentSchool()?.countryCode {
            intCountryCode =  Int(check)}

        print("intCountryCode = \(intCountryCode)")
    }
    
    
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.webApiSearchSMSReport(searchtext: searchBar.text!)
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let totalCharacters = (searchBar.text?.appending(text).count ?? 0) - range.length
//        return totalCharacters <= 10
        return true
    }
    
    func webApiSearchSMSReport(searchtext: String)
    {
        Utility.shared.loader()
        print("Func: webSMSReport")
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/SMSSearchMob?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&mobile=\(intCountryCode)\(searchtext)"
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async {
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.tblSMSReportSearch?.isHidden = false
                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.sMSs{
                            self.arraySMSReport = boardArray
                            print("arraySMSReport Count = \(/self.arraySMSReport?.count)")
                            self.tblSMSReportSearch?.reloadData()
                            self.tblSMSReportSearch?.isHidden = false
                            self.lblNoRecord?.isHidden = true

                        }
                        else{
                            self.tblSMSReportSearch?.isHidden = true
                            self.lblNoRecord?.isHidden = false
                        }
                        if self.arraySMSReport?.count == 0{
                            self.tblSMSReportSearch?.isHidden = true
                            self.lblNoRecord?.isHidden = false
                        }
                    }
                    else {
                        self.tblSMSReportSearch?.isHidden = true
                        self.lblNoRecord?.isHidden = false

                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)

                    break
                }
        }
    }
}


//MARK:- UITableViewDelegate and UITableViewDataSource
extension SMSReportSearchViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
            return /arraySMSReport?.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.smsReport.get, for: indexPath) as? StudentSMSCell else {return UITableViewCell()}
        
            cell.modal = arraySMSReport?[indexPath.section]
            return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
}
