//  SMSReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 16/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire

class SMSReportViewController: UIViewController {
    
    @IBOutlet weak var tableSMS:           UITableView?
    @IBOutlet weak var tableSMSType:       UITableView?
    @IBOutlet weak var lblSMSType:         UILabel?
    @IBOutlet weak var barBtnView:         UIView?
    @IBOutlet weak var imgBarBtn:          UIImageView?
    @IBOutlet weak var rightBarBtnItem:    UIBarButtonItem?
    @IBOutlet weak var leftBackBarBtnItem: UIBarButtonItem?
    
    @IBOutlet weak var btnFrom:            UIButton?
    @IBOutlet weak var btnTo:              UIButton?
    @IBOutlet weak var lblFrom:            UILabel?
    @IBOutlet weak var lblTo:              UILabel?
    
    var selectToDate   = Date()
    var selectFromDate = Date()
    var selectDate     : Date?
    
    var intBtnFromTo          = Int()
    var strSchoolCodeSender   = String()
    var intPage               = Int()
    
    var currentDate           = Date()
    var thirtyDaysBeforeToday = Date()
    let formatter             = DateFormatter()
    
    var intSMSTypeId          = Int()
    var spinner               = UIActivityIndicatorView()
    
    var arraySMSReport: [SMS]?
    var arraySMSType:   [SMSType]?
    
    @IBAction func ClkBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ClkRightBarBtnItem(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Reports", bundle: nil)
        guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "SMSReportSearchViewController") as? SMSReportSearchViewController else {return}
        self.pushVC(shortProfileVc)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        formatter.dateFormat         = "dd MMM, yy"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        intPage                      = 1
        spinner                      = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        spinner.color                = UIColor(rgb: 0x56B54B)
        spinner.stopAnimating()
        spinner.hidesWhenStopped     = true
        spinner.frame                = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 60)
        
        tableSMS?.estimatedRowHeight = 300
        tableSMS?.rowHeight          = UITableView.automaticDimension
        
        let nib = UINib(nibName: "StudentSMSCell", bundle: nil)
        tableSMS?.register(nib, forCellReuseIdentifier: "studentCell")
        setUpBarBtn()
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        self.webSMSTypeReport()
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        intBtnFromTo = 1
        thirtyDaysBeforeToday = Calendar.current.date(byAdding: .day, value: -30, to: currentDate)!
        
        btnFrom?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        btnTo?.setTitle(formatter.string(from: currentDate), for: UIControl.State.normal)
        
        print("currentDate = \(currentDate)")
        print("thirtyDaysBeforeToday = \(thirtyDaysBeforeToday)")
        
        selectDate     = currentDate//formatter.string(from: currentDate)
        selectFromDate = currentDate
        selectToDate   = currentDate
        
        //        datePickerSMSReport?.maximumDate = currentDate
        //        datePickerSMSReport?.minimumDate = thirtyDaysBeforeToday
    }
    func setUpBarBtn(){
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        self.barBtnView?.addGestureRecognizer(tap)
    }
    @objc func handleTap() {
        if (self.tableSMSType?.isHidden)! {
            self.tableSMSType?.isHidden = false
            self.imgBarBtn?.image = R.image.smsUpArrow()
        } else {
            self.tableSMSType?.isHidden = true
            self.imgBarBtn?.image = R.image.smsDownArrow()
        }
    }
    //MARK: - ClkBtnFrom
    @IBAction func ClkBtnFrom(_ sender: UIButton) {
        intBtnFromTo = 1
        self.showDatePicker(btnDate: sender)
    }
    //MARK: - ClkBtnTo
    @IBAction func ClkBtnTo(_ sender: UIButton) {
        intBtnFromTo = 2
        self.showDatePicker(btnDate: sender)
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(btnDate: UIButton){
        intPage = 1
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: intBtnFromTo == 1 ? selectFromDate : selectToDate , minimumDate: thirtyDaysBeforeToday, maximumDate: currentDate, datePickerMode: .date , lastText:  btnDate.titleLabel?.text) { (date) in
            self.setDateIntoLabel(dateVal : date , lastText :  btnDate.titleLabel?.text)
        }
    }
    // MARK: -Set the date into the label after getting response from the date picker
    func setDateIntoLabel(dateVal : Date? ,lastText : String?) {
        if let dateVal = dateVal {
            let dateString = dateVal.dateToString(formatType :"dd MMM, yyyy")
            guard let lasttext = lastText else {
                print("last text is empty")
                return
            }
            //   formatter.dateFormat = "yyyy-MM-dd"//"dd MMM, yyyy"
            formatter.dateFormat = "dd MMM, yyyy"
            //Validation for from date
            if intBtnFromTo == 1 {
                if  dateVal.compare(selectToDate) == .orderedAscending || dateVal.compare(selectToDate)  == .orderedSame{
                    selectFromDate = dateVal
                    
                    btnFrom?.setTitle(dateString, for: UIControl.State.normal)
                    // formatter.dateFormat = "yyyy-MM-dd"
                    //   strFromDate  = formatter.string(from: dateVal)
                    webSMSReport(intPaggination: intPage)
                }
                else {
                    WebserviceManager.showAlert(message: "", title: "'From Date' should not be greater than 'To Date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
                
                //Validatin for To date
            else {
                if dateVal.compare(selectFromDate) == .orderedDescending ||  dateVal.compare(selectFromDate)  == .orderedSame {
                    selectToDate = dateVal
                    btnTo?.setTitle(dateString, for: UIControl.State.normal)
                    webSMSReport(intPaggination: intPage)
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "'To Date' can't be less than 'From Date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
        }
        else {
            let strLastText = /lastText?.isEmpty ? Date().dateToString(formatType :"dd MMM, yyyy") : lastText
            if intBtnFromTo == 1 {
                btnFrom?.setTitle(strLastText, for: UIControl.State.normal)
            }
            else {
                btnTo?.setTitle(strLastText, for: UIControl.State.normal)
            }
        }
    }
    
    func webSMSReport(intPaggination: Int)
    {
        Utility.shared.loader()
        formatter.dateFormat = "dd MMM, yy"
        
        let Date1       = (formatter.date(from: (self.btnFrom?.currentTitle)!)!)
        let Date2       = (formatter.date(from: (self.btnTo?.currentTitle)!)!)
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        let strFromDate = formatter.string(from: Date1)
        let strToDate   = formatter.string(from: Date2)
        
        print("strFromDate = \(strFromDate)")
        print("strToDate = \(strToDate)")
        print("intSMSTypeId = \(intSMSTypeId)")
        
        var urlString = String()

        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/SMSRPT?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SenderID=0&SenderType=0&SMSType=\(intSMSTypeId)&FromDate=\(strFromDate)&TillDate=\(strToDate)&pg=\(intPaggination)"//\(thirtyDaysBefore)//\(currentDate)
        
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                
                switch response.result {
                case .success(let data):
                    self.tableSMS?.isHidden = false
                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.sMSs{
                            self.arraySMSReport = boardArray
                            print("arraySMSReport Count = \(/self.arraySMSReport?.count)")
                            self.tableSMS?.reloadData()
                        }
                        if parsedObject.sMSs == nil {
                            self.arraySMSReport?.removeAll()
                            self.tableSMS?.reloadData()
                        }
                        if self.arraySMSReport?.count == 0 || self.arraySMSReport?.count == nil {
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noDataFound(), fromVC: self)
                        }
                    }
                    else {
                    }
                case .failure(_):
                    AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    break
                }
        }
    }
    func webSMSTypeReport()
    {
        print("Func: webSMSTypeReport")
        
        Utility.shared.loader()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/SMSType?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)"
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async{
                    Utility.shared.removeLoader()
                }
                switch response.result {
                    
                case .success(let data):
                    self.tableSMS?.isHidden = false
                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray1 = parsedObject.SMSType{
                            self.arraySMSType = boardArray1
                            print("arraySMSReport Count = \(/self.arraySMSType?.count)")
                            
                            let smsType = SMSType.init(typeID_: 0, subject_: "All SMS")
                            self.arraySMSType?.insert(smsType, at: 0)
                            
                            self.tableSMSType?.reloadData()
                            self.intSMSTypeId = 0
                            self.webSMSReport(intPaggination: self.intPage)
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    // MARK: - scrollViewDidEndDragging
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        let y      = offset.y + bounds.size.height - inset.bottom
        let h      = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h //+ reloadDistance
        {
            print("fetch more data -- scrollViewDidEndDragging")
            spinner.startAnimating()
            
            intPage = intPage + 1
            //self.webAPIQuestions(intAll: intAllMyCount, intPage: intCount)
            self.loadChunkSMSReport(intPagination: intPage)
        }
    }
    func loadChunkSMSReport(intPagination: Int)
    {
        print("Func: webSMSReport")
        spinner.startAnimating()
        formatter.dateFormat = "dd MMM, yy"
        
        let Date1       = (formatter.date(from: (self.btnFrom?.titleLabel?.text)!)!)
        let Date2       = (formatter.date(from: (self.btnTo?.titleLabel?.text)!)!)
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        let strFromDate = formatter.string(from: Date1)
        let strToDate   = formatter.string(from: Date2)
        
        print("strFromDate = \(strFromDate)")
        print("strToDate = \(strToDate)")
        print("intSMSTypeId = \(intSMSTypeId)")
        
        //FromDate=2018-04-01&TillDate=2018-05-08
        
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        urlString    += "Message/SMSRPT?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SenderID=0&SenderType=0&SMSType=\(intSMSTypeId)&FromDate=\(strFromDate)&TillDate=\(strToDate)&pg=\(intPagination)"//\(thirtyDaysBefore)//\(currentDate)
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                
                switch response.result {
                case .success(let data):
                    self.tableSMS?.isHidden = false
                    let object = Mapper<SMSReportModal>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray = parsedObject.sMSs{
                            for sms in boardArray {
                                self.arraySMSReport?.append(sms)
                            }
                            self.tableSMS?.reloadData()
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
}
//MARK:- UITableViewDelegate and UITableViewDataSource
extension SMSReportViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableSMSType{
            return 1
        }
        else{
            print("Count = \(/arraySMSReport?.count)")
            return /arraySMSReport?.count
        }}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableSMSType{
            return /arraySMSType?.count
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableSMS{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.smsReport.get, for: indexPath) as? StudentSMSCell else {return UITableViewCell()}
            cell.modal = arraySMSReport?[indexPath.section]
            return cell
        } else if tableView == self.tableSMSType {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.smsType.get, for: indexPath)
            cell.textLabel?.text      = arraySMSType?[indexPath.row].Subject
            cell.textLabel?.font      = R.font.ubuntuRegular(size: 17)
            cell.textLabel?.textColor = UIColor(rgb: 0x545454)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableSMSType{
            intPage = 1
            self.lblSMSType?.text = arraySMSType?[indexPath.row].Subject
            intSMSTypeId          = /arraySMSType?[indexPath.row].TypeID
            tableView.isHidden    = true
            imgBarBtn?.image      = R.image.smsDownArrow()
            self.webSMSReport(intPaggination: intPage)
        }
    }
}
