//  StatisticalReportViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 04/01/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import Charts

class StatisticalReportViewController: UIViewController {
    
    @IBOutlet weak var pieChart_View:      PieChartView?
    @IBOutlet weak var lblCircularAndroid: UILabel?
    @IBOutlet weak var lblCircularAniOS:   UILabel?
    
    @IBOutlet weak var lblAndroidPer:      UILabel?
    @IBOutlet weak var lbliOSPer:          UILabel?
    
    @IBOutlet weak var lblAndroidCount:    UILabel?
    @IBOutlet weak var lbliOSCount:        UILabel?
    
    @IBOutlet weak var imgView1:           UIImageView?
    @IBOutlet weak var imgView2:           UIImageView?
    
    @IBOutlet weak var lblGender1:         UILabel?
    @IBOutlet weak var lblGender2:         UILabel?
    
    @IBOutlet weak var segmentCustom: UISegmentedControl?
    
    var color1 = UIColor()
    var color2 = UIColor()
    
    var dictStudent   = [String: Any]()
    var dictTeacher   = [String: Any]()
    var dictTransport = [String: Any]()
    var intTotalUser   = Int()
    
    let ios_users     = 6250
    let android_users = 16250
    var btnTitle      = UIButton()
    var strSender     = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let schoolCode = self.getCurrentSchool()?.schCode{
            strSender =  schoolCode
        }
        
        segmentCustom?.addUnderlineForSelectedSegment()
        
        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Statistical Report", for: UIControl.State.normal)
        btnTitle.titleLabel?.font = R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        self.navigationItem.titleView = btnTitle
        
        lblCircularAndroid?.layer.cornerRadius = (lblCircularAndroid?.frame.width)! / 2
        lblCircularAndroid?.clipsToBounds      = true
        lblCircularAniOS?.layer.cornerRadius   = (lblCircularAniOS?.frame.width)! / 2
        lblCircularAniOS?.clipsToBounds        = true
        //Students
        imgView1?.image = R.image.boys()
        imgView2?.image = R.image.girls()
        
        lblGender1?.text = "Boys"
        lblGender2?.text = "Girls"
        
        color1 = UIColor(rgb: 0x3E98F9)
        color2 = UIColor(rgb: 0xFF70B7)
        // let str1 = "25000"
        // let str2 = "Students"
        
        webApiStatisticalReport()
        
        // updateGraph(color1: color1, color2: color2, value1: android_users, value2: ios_users, str1: str1, str2: str2)
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    func webApiStatisticalReport()
    {
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "report/Statistical?SchCode=\(strSender)&key=\(urlString.keyPath())"
        
        print("urlString = \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async{
                Utility.shared.removeLoader()
            }
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let dict1 = (result["StudentStatistical"] as? [String: Any]){
                            self.dictStudent = dict1
                        }
                        if let dict2 = (result["TeacherStatistical"] as? [String: Any]){
                            self.dictTeacher = dict2
                        }
                        if let dict3 = (result["TransporStatistical"] as? [String: Any]){
                            self.dictTransport = dict3
                        }
                        
                        var Total = Int()
                        var Boys  = Int()
                        var Girls = Int()
                        
                        if let Total1 = self.dictStudent["Total"] as? Int {
                            Total = Total1
                        }
                        if let Boys1 = self.dictStudent["Boys"] as? Int {
                            Boys = Boys1
                        }
                        if let Girls1 = self.dictStudent["Girls"] as? Int {
                            Girls = Girls1
                        }
                        
                        self.lblAndroidCount?.text = "\(Boys)"
                        self.lbliOSCount?.text     = "\(Girls)"
                        
                        // self.intTotalUser = Boys + Girls
                        if Total != 0 {
                        self.lblAndroidPer?.text = "\(Int((100 * Float(Boys)  / Float(Total)).rounded()))%"
                        self.lbliOSPer?.text     = "\(Int((100 * Float(Girls) / Float(Total)).rounded()))%"
                        }
                        else {
                            self.lblAndroidPer?.text = "0.0%"
                            self.lbliOSPer?.text = "0.0%"
                        }
                        
                        self.updateGraph(color1: self.color1, color2: self.color2, value1: Boys, value2: Girls, str1: "\(Total)", str2: "Students")
                    }
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "Something went wrong", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                        self.navigationController?.popViewController(animated: true)
                    }, secondBtnBlock: {})
                }
            }
        }
    }
    
    func updateGraph(color1: UIColor, color2: UIColor, value1: Int, value2: Int, str1: String, str2: String){
        //will update the graph
        //TODO:-
        //        let entry1  = PieChartDataEntry(value: Double(value1))
        //       let entry2  = PieChartDataEntry(value: Double(value2))
        
        var pieChartDataEntry = [PieChartDataEntry]()
        
        if value1 != 0 {
          pieChartDataEntry.append(PieChartDataEntry(value: Double(value1), label: ""))
        }
        if value2 != 0 {
            pieChartDataEntry.append(PieChartDataEntry(value: Double(value2), label: ""))
        }
//        let entry1  = PieChartDataEntry(value: Double(value1), label: "")
//        let entry2  = PieChartDataEntry(value:Double(value2), label: "")
//
        let dataSet = PieChartDataSet(values:pieChartDataEntry, label: "")
        let data    = PieChartData(dataSet: dataSet)
        pieChart_View?.data = data
        
        let str1 = NSMutableAttributedString(string: "\(str1)\n", attributes: [NSAttributedString.Key.font:R.font.ubuntuBold(size: 20.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x2B2B2B)])
        let str2 = NSMutableAttributedString(string: "\(str2)", attributes: [NSAttributedString.Key.font:R.font.ubuntuRegular(size: 20.0) ?? "", NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x545454)])
        str1.append(str2)
        pieChart_View?.centerText   = str1.string
        pieChart_View?.drawEntryLabelsEnabled = false
        pieChart_View?.chartDescription?.text = ""
        pieChart_View?.highlightPerTapEnabled = true
        pieChart_View?.legend.enabled         = false
        dataSet.colors                        = [color1, color2]
        pieChart_View?.rotationEnabled        = true
        //This must stay at end of function
        pieChart_View?.notifyDataSetChanged()
    }
    
    @IBAction func ClkSegment(_ sender: Any)
    {
        segmentCustom?.changeUnderlinePosition()
        
        switch segmentCustom?.selectedSegmentIndex
        {
        case 0:
            color1 = UIColor(rgb: 0x3E98F9)
            color2 = UIColor(rgb: 0xFF70B7)
            
            lblCircularAndroid?.backgroundColor = UIColor(rgb: 0x3E98F9)
            lblCircularAniOS?.backgroundColor   = UIColor(rgb: 0xFF70B7)
            
            imgView1?.image  = R.image.boys()
            imgView2?.image  = R.image.girls()
            lblGender1?.text = "Boys"
            lblGender2?.text = "Girls"
            
            var Total = Int()
            var Boys  = Int()
            var Girls = Int()
            
            if let Total1 = self.dictStudent["Total"] as? Int {
                Total = Total1
            }
            if let Boys1 = self.dictStudent["Boys"] as? Int {
                Boys = Boys1
            }
            if let Girls1 = self.dictStudent["Girls"] as? Int {
                Girls = Girls1
            }
            self.lblAndroidCount?.text = "\(Boys)"
            self.lbliOSCount?.text     = "\(Girls)"
             if Total != 0 {
            self.lblAndroidPer?.text = "\(Int((100 * Float(Boys)  / Float(Total)).rounded()))%"
            self.lbliOSPer?.text     = "\(Int((100 * Float(Girls) / Float(Total)).rounded()))%"
            }
             else {
                self.lblAndroidPer?.text = "0.0%"
                self.lbliOSPer?.text = "0.0%"
            }
            
            self.updateGraph(color1: self.color1, color2: self.color2, value1: Boys, value2: Girls, str1: "\(Total)", str2: "Students")
        case 1:
            color1 = UIColor(rgb: 0x3E98F9)
            color2 = UIColor(rgb: 0xFF70B7)
            lblCircularAndroid?.backgroundColor = UIColor(rgb: 0x3E98F9)
            lblCircularAniOS?.backgroundColor   = UIColor(rgb: 0xFF70B7)
            
            imgView1?.image  = R.image.male()
            imgView2?.image  = R.image.female()
            lblGender1?.text = "Male"
            lblGender2?.text = "Female"
            
            var Total = Int()
            var Boys  = Int()
            var Girls = Int()
            
            if let Total1 = self.dictTeacher["Total"] as? Int {
                Total  = Total1
            }
            if let Boys1 = self.dictTeacher["Male"] as? Int {
                Boys  = Boys1
            }
            if let Girls1 = self.dictTeacher["Female"] as? Int {
                Girls  = Girls1
            }
            self.lblAndroidCount?.text = "\(Boys)"
            self.lbliOSCount?.text     = "\(Girls)"
             if Total != 0 {
                self.lblAndroidPer?.text = "\(Int((100 * Float(Boys)  / Float(Total)).rounded()))%"
                self.lbliOSPer?.text     = "\(Int((100 * Float(Girls) / Float(Total)).rounded()))%"
            }
             else {
                self.lblAndroidPer?.text = "0.0%"
                self.lbliOSPer?.text = "0.0%"
            }
            self.updateGraph(color1: self.color1, color2: self.color2, value1: Boys, value2: Girls, str1: "\(Total)", str2: "Teachers")
        case 2:
            color1 = UIColor(rgb: 0x4BD964)
            color2 = UIColor(rgb: 0xD1D1D1)
            lblCircularAndroid?.backgroundColor = UIColor(rgb: 0x4BD964)
            lblCircularAniOS?.backgroundColor   = UIColor(rgb: 0xD1D1D1)
            
            imgView1?.image  = R.image.schoolbus()
            imgView2?.image  = R.image.selfbus()
            lblGender1?.text = "School Bus"
            lblGender2?.text = "Self Conveyance"
            
            var Total = Int()
            var Boys  = Int()
            var Girls = Int()
            
            if let Total1 = self.dictTransport["Total"] as? Int {
                Total  = Total1
            }
            if let Boys1 = self.dictTransport["SchoolBus"] as? Int {
                Boys  = Boys1
            }
            if let Girls1 = self.dictTransport["SelfConvene"] as? Int {
                Girls  = Girls1
            }
            self.lblAndroidCount?.text = "\(Boys)"
            self.lbliOSCount?.text     = "\(Girls)"
             if Total != 0 {
            self.lblAndroidPer?.text = "\(Int((100 * Float(Boys)  / Float(Total)).rounded()))%"
            self.lbliOSPer?.text     = "\(Int((100 * Float(Girls) / Float(Total)).rounded()))%"
            }
             else {
                self.lblAndroidPer?.text = "0.0%"
                self.lbliOSPer?.text = "0.0%"
            }
            self.updateGraph(color1: self.color1, color2: self.color2, value1: Boys, value2: Girls, str1: "\(Total)", str2: "Students")
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
extension UISegmentedControl{
    func removeBorder(){
        
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal,      barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected,    barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        
        
        let normalFont = R.font.ubuntuMedium(size: 17)
        
        let normalTextAttributes: [NSObject : AnyObject] = [
            NSAttributedString.Key.foregroundColor as NSObject: UIColor.flatLightBlack,
            NSAttributedString.Key.font as NSObject: normalFont!//normalFont
        ]
        
        let boldTextAttributes: [NSObject : AnyObject] = [
            NSAttributedString.Key.foregroundColor as NSObject : UIColor.flatGreen,
            NSAttributedString.Key.font as NSObject : normalFont!,
            ]
        
        self.setTitleTextAttributes((normalTextAttributes as! [NSAttributedString.Key : Any]), for: .normal)
        self.setTitleTextAttributes((boldTextAttributes as! [NSAttributedString.Key : Any]),   for: .selected)
    }
    
    func addUnderlineForSelectedSegment(){
        removeBorder()
        let underlineWidth: CGFloat  = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineXPosition       = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition       = self.bounds.size.height - 1.0
        let underlineFrame           = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: 3.0)
        print("UnderLineFrame: ",underlineFrame)
        let underline                = UIView(frame: underlineFrame)
        underline.backgroundColor    = UIColor.flatGreen
        underline.tag                = 1
        self.addSubview(underline)
    }
    
    func changeUnderlinePosition(){
        guard let underline         = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
            underline.frame.size.width = CGFloat(self.bounds.width / CGFloat(self.numberOfSegments))
        })
    }
}
extension UIImage{
    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle      = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}
