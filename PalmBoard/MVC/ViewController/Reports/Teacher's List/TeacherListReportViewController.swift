import UIKit

class TeacherListReportViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UISearchControllerDelegate
{
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var lblNoRecord: UILabel?
    var userId        = Int()
    var strSchoolCodeSender = String()
    var userType      = Int()
    var intListChoice = Int()
    var arrayDataList = [[String: Any]]()
    var arraySearchDataList = [[String: Any]]()
    
    var otherUserId      = Int()
    var otherUserType    = Int()
    var otherUserPhoto   = String()
    var otherUserSubject = String()
    var btnTitle         = UIButton()
    var btnSearch        = UIButton()
    
    var filtered:[String]   = []
    var searchActive : Bool = false
    let searchController    = UISearchController(searchResultsController: nil)
    
    // MARK: -  viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    // MARK: -  viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblNoRecord?.isHidden = true
        
        //  guard let themeColor = self.getCurrentSchool()?.themColor else { return}
        
        btnTitle                  = UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame            = CGRect(x : 0,y:  0,width : 120, height: 40)
        btnTitle.setTitle("Teachers List", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        btnTitle.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        self.navigationItem.titleView = btnTitle
        
        btnSearch                = UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnSearch.frame           = CGRect(x : 0,y:  0,width : 40, height: 40)
        btnSearch.addTarget(self, action: #selector(self.ClkBtnSearch(sender:)), for: .touchUpInside)
        btnSearch.setImage(R.image.search(), for: UIControl.State.normal)
        
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation     = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.sizeToFit()
        
        self.searchController.searchBar.alpha = 0
        
        if let schoolCode = self.getCurrentSchool()?.schCode {strSchoolCodeSender =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID    {self.userId         =  Int(userId)}
        if let userType   = self.getCurrentUser()?.userType  {self.userType       =  Int(userType)}
        webApiTeachersList()
    }
    //MARK: searchBar Delegate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("textDidChange")
        searchActive = true
        let searchString = searchController.searchBar.text
        
        self.arraySearchDataList = self.arrayDataList.filter({ (item) -> Bool in
            
            let resultText: NSString = item["Name"] as! NSString
            
            return (resultText.range(of: searchString!, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        print("self.arraySearchDataList \n \(self.arraySearchDataList)")
        
        if self.arraySearchDataList.count == 0 && (searchString?.count)! > 0 {
            lblNoRecord?.isHidden    = false
            collectionView?.isHidden = true
        }
        else{
            lblNoRecord?.isHidden = true
            collectionView?.isHidden = false
            
            collectionView?.reloadData()
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarCancelButtonClicked")
        
        self.searchController.searchBar.alpha = 0
        //self.navigationItem.titleView = nil
        self.navigationItem.titleView = btnTitle
        let barButton = UIBarButtonItem.init(customView: btnSearch)
        self.navigationItem.rightBarButtonItem = barButton
        
        searchActive = false
        self.arraySearchDataList.removeAll()
        self.dismiss(animated: true, completion: nil)
        collectionView?.reloadData()
        lblNoRecord?.isHidden = true
        collectionView?.isHidden = false
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        searchActive = true
        //collectionView.reloadData()
    }
    
    // MARK: - viewDidLayoutSubviews
    override func viewDidLayoutSubviews() {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    // MARK: - ClkBtnView
    @objc func ClkBtnView(sender:UIButton) {
        self.collectionView?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - ClkBtnSearch
    @objc func ClkBtnSearch(sender:UIButton)
    {
        self.searchController.searchBar.alpha = 1
        
        searchController.searchBar.becomeFirstResponder()
        self.navigationItem.titleView = searchController.searchBar
        self.navigationItem.rightBarButtonItem = nil
    }
    
    // MARK: -  segmentChanged
    func webApiTeachersList()
    {
        Utility.shared.loader()
        collectionView?.isHidden = true
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "teacher/StaffList?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())"
        
        print("urlString = \(urlString)")
        
        WebserviceManager.getJsonData(withParameter: urlString)
        { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
                if let result = results{
                    
                    if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                    {
                        //print("List result \n \(result)")
                        self.collectionView?.isHidden = false
                        
                        if let allTeacher = (result["AllTeacher"] as? [[String: Any]]){
                            self.arrayDataList = allTeacher
                            self.arraySearchDataList = allTeacher
                        }
                        self.collectionView?.setContentOffset(CGPoint(x:0,y:0), animated: true)
                        self.collectionView?.reloadData()
                    }
                }
                else{
                    WebserviceManager.showAlert(message: "", title: "The Internet connection appears to be offline.", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                    }, secondBtnBlock: {})
                }
            }
        }
    }
    
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(searchActive){
            return self.arraySearchDataList.count
        }else{
            return self.arrayDataList.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCell", for: indexPath) as! TeacherListCollectionViewCell
        
        //        cell.btnImage?.setBackgroundImage(UIImage(named:  "No Profile_Big"), for: .normal)
        cell.btnImage?.layer.cornerRadius  = (cell.btnImage?.frame.width)! / 2
        cell.btnImage?.layer.masksToBounds = true
        cell.btnImage?.tag                 = indexPath.row
        
        //  cell.btnDiscuss.tag = indexPath.row
        // cell.btnDiscuss.layer.cornerRadius = 5
        //print("self.arrayDataList \n \n \(self.arrayDataList)")
        if(self.arrayDataList.count != 0){
            
            if (searchActive == true){
                if let imgUrl = self.arraySearchDataList[indexPath.row]["Photo"] as? String{
                    cell.imgProfile?.loadURL(imageUrl: imgUrl.getImageUrl(), placeholder: nil , placeholderImage: R.image.noProfile_Big()! , isCroppingReq: false , croppedSize: CGSize(width: 75.0, height: 75.0))
                }
                if let name = self.arraySearchDataList[indexPath.row]["Name"] as? String{
                    cell.lblName?.text = name
                }
                if let subject = self.arraySearchDataList[indexPath.row]["Subject"] as? String{
                    cell.lblSubject?.text = "(\(subject))"
                }
            } else{
                if let imgUrl = self.arrayDataList[indexPath.row]["Photo"] as? String{
                    cell.imgProfile?.loadURL(imageUrl: imgUrl.getImageUrl(), placeholder: nil , placeholderImage: R.image.noProfile_Big()! , isCroppingReq: false , croppedSize: CGSize(width: 75.0, height: 75.0))
                }
                else {
                    cell.imgProfile?.image = R.image.noProfile_Big()
                }
                if let name = self.arrayDataList[indexPath.row]["Name"] as? String{
                    cell.lblName?.text = name
                }
                if let subject = self.arrayDataList[indexPath.row]["Subject"] as? String{
                    cell.lblSubject?.text = "(\(subject))"
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width/2, height: 160)
    }
    // MARK: -  imagePressed
    @IBAction func imagePressed(_ sender: UIButton) {
        
        let row      = sender.tag
        
        var  selectTeacher = [String : Any]()
        if self.arraySearchDataList.count != 0 {
            selectTeacher = self.arraySearchDataList[row]
        }
        else {
            selectTeacher = self.arrayDataList[row]
        }
        if let userId1 = selectTeacher["UserID"] as? Int{
            self.otherUserId      = userId1
        }else{
            self.otherUserId      = 0
        }
        if let userType1 = selectTeacher["UserType"] as? Int{
            self.otherUserType    = userType1
        }else{
            self.otherUserType    = 0
        }
        if let otherUserSubject1 = selectTeacher["Subject"] as? String{
            self.otherUserSubject = otherUserSubject1
        }else{
            self.otherUserSubject = ""
        }
        //showProfile
        // hideBarDelegate?.hideBar(boolHide: true)
        if APIManager.shared.isConnectedToInternet(){
            print("self.getCurrentUser()? = \(/self.getCurrentUser()?.searchEnabled)")
            if (self.getCurrentUser()?.searchEnabled)!  {
                guard let completeProfileVc = R.storyboard.main.studentProfileViewController() else {
                    return
                }
                completeProfileVc.strUserID     = self.otherUserId.toString
                completeProfileVc.strUserType   = self.otherUserType.toString
                completeProfileVc.selectedIndex =  1
                
                self.pushVC(completeProfileVc)
            }
            else {
                guard let shortProfileVC = R.storyboard.main.shortProfileViewController() else { return}
                shortProfileVC.intUserID         = self.otherUserId
                shortProfileVC.intUserType       = self.otherUserType
                shortProfileVC.strteacherSubject = self.otherUserSubject
                self.pushVC(shortProfileVC)
            }
        }
        else{
            Utility.shared.internetConnectionAlert()
        }
    }
 
}
class TeacherListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnImage:   UIButton?
    @IBOutlet weak var lblName:    UILabel?
    @IBOutlet weak var lblSubject: UILabel?
    @IBOutlet weak var imgProfile : UIImageView? {
        didSet {
            self.imgProfile?.layer.cornerRadius = /imgProfile?.frame.size.width / 2.0
        }
    }
}
