//
//  MainSearchViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 30/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import ObjectMapper
import Alamofire
import EZSwiftExtensions
import IQKeyboardManager


class MainSearchViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var txtFieldSearch: UITextField?
    @IBOutlet weak var segmentView:    UISegmentedControl?
    @IBOutlet weak var tblMenu:        UITableView?
    
    var intUserId            = Int()
    var strSchoolCodeSender  = String()
    var intUserType          = Int()
    var intSearchBy          = Int()
    var intSegmentType       = Int()
    var intUserTypeAPI       = Int()
    var intPassUserId        = Int()
    var intPassUserType      = Int()
    
    var arrayResult: [Result]?
    var arrayMenu            = [[String:Any]]()
    var arrayMenuStaff       = [[String:Any]]()
    var classArr : [MyClasses]?
    var containerView = UIView()
    var classID : String?
    
    //MARK: - ClkFilter
    @IBAction func ClkFilter(_ sender: Any) {
        ez.runThisInMainThread {
            self.setUpTableView()
        }
    }
    //MARK: - setUpTableView
    func setUpTableView(){
        
        if !containerView.isHidden {
            return
        }
        containerView  = UIView(frame:CGRect(x:self.view.frame.size.width - 263, y:44,width: 263.0, height: intSegmentType == 1 ? 220.0 : 88.0))
        tblMenu?.frame = containerView.bounds
        containerView.layer.shadowColor   = UIColor.black.withAlphaComponent(0.6).cgColor
        containerView.layer.shadowOffset  = CGSize(width: 0.0, height: 2.0)
        containerView.layer.shadowOpacity = 4.0
        self.view.addSubview(containerView)
        containerView.addSubview(tblMenu!)
        containerView.isHidden = false
        tblMenu?.isHidden = false
        tblMenu?.layer.masksToBounds = true
        tblMenu?.isScrollEnabled = false
        tblMenu?.reloadData()
    }
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblMenu?.isHidden = true
        intUserTypeAPI = 1
        //self.setUpTableView()
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerView.isHidden = true
        
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        intSearchBy    = 1
        intSegmentType = 1
        arrayMenu      = [["name": "Search by Name","id": 1,"image": "Search Name"],["name": "Search by Class","id": 3,"image": "search_by_class"],["name": "Search by Contact No","id": 2,"image": "Search By contact"],["name": "Search by Bill Number","id": 4,"image": "Search by bill"],["name": "Search by Admission No","id": 5,"image": "Search by admission Number"]]
        arrayMenuStaff = [["name": "Search by Name","id": 1,"image": "Search Name"],["name": "Search by Contact Number","id": 2,"image": "Search By contact"]]
        
        //        tblMenu?.tableFooterView  = UIView(frame: CGRect.zero)
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
        if let userId = self.getCurrentUser()?.userID {
            self.intUserId =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        //Adding Padding to left TextField
        let paddingView1: UIView       = UIView.init(frame: CGRect(x: 16, y: 0, width: 16, height: (self.txtFieldSearch?.frame.height)!))
        txtFieldSearch?.leftView        = paddingView1
        txtFieldSearch?.leftViewMode    = .always
        
        // Rightview  to TextField
        let viewPadding = UIView(frame: CGRect(x: 0, y: 0, width: 57 , height: Int((txtFieldSearch?.frame.size.height)!)))
        
        let btn                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btn.frame           = CGRect(x : 0,y:  0,width : 22, height: 22)
        
        // let btn = UIButton (frame:CGRect(x: 0, y: 0, width: 24 , height: 24))
        btn.center = viewPadding.center
        viewPadding .addSubview(btn)
        // let paddingView2: UIImageView       = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 30, height: self.txtFieldSearch.frame.height))
        //btn.image = UIImage(named: "search")
        btn.setImage(R.image.search(), for: UIControl.State.normal)
        btn.addTarget(self, action: #selector(self.ClkMainSearch(sender:)), for: .touchUpInside)
        
        btn.contentMode = UIView.ContentMode.scaleAspectFit
        txtFieldSearch?.rightView     = viewPadding
        txtFieldSearch?.rightViewMode = .always
        //        webMainSearch(strSearch: textField.text!)
        //btnBack                 =  UIButton(type: UIButtonType.custom) as UIButton
        //  btnBack.frame           = CGRect(x : 0,y:  0,width : 22, height: 22)
        // btnBack.addTarget(self, action: #selector(self.ClkCrossLeft(sender:)), for: .touchUpInside)
    }
    //MARK: - ClkMainSearch
    @objc func ClkMainSearch(sender:UIButton) {
        if (txtFieldSearch?.text?.count)! > 0 {
            webMainSearch(strSearch: (txtFieldSearch?.text)!)
        }
        else{
            WebserviceManager.showAlert(message: "Please enter text", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
            }, secondBtnBlock: {})
        }
    }
    //MARK: - textFieldShouldReturn
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        if (txtFieldSearch?.text?.count)! > 0 {
            webMainSearch(strSearch: (txtFieldSearch?.text)!)
        }
        else{
            WebserviceManager.showAlert(message: "Please enter text", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
            }, secondBtnBlock: {})
        }
        return true
    }
    //MARK: - webMainSearch
    func webMainSearch(strSearch: String)
    {
        print("Func: webMainSearch")
        Utility.shared.loader()
        txtFieldSearch?.resignFirstResponder()
        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        switch intSearchBy {
        case 1:
            urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intSearchBy)&UserType=\(intUserTypeAPI)&Name=\(strSearch)&StaffID=\(intUserId)"
        case 2:
            urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intSearchBy)&UserType=\(intUserTypeAPI)&ContactMob=\(strSearch)&StaffID=\(intUserId)"
        case 3:
            urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intSearchBy)&UserType=\(intUserTypeAPI)&ClassID=\(strSearch)&StaffID=\(intUserId)"
        case 4:
            urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intSearchBy)&UserType=\(intUserTypeAPI)&BillNo=\(strSearch)&StaffID=\(intUserId)"
        case 5:
            urlString    += "User/Search?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&SearchBy=\(intSearchBy)&UserType=\(intUserTypeAPI)&AdmNo=\(strSearch)&StaffID=\(intUserId)"
        default:
            break
        }
        print("urlString= \(urlString)")
        Alamofire.request(urlString, method: .get
            , parameters: [:], encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                DispatchQueue.main.async {
                    Utility.shared.removeLoader()
                }
                switch response.result {
                case .success(let data):
                    self.collectionView?.isHidden = false
                    let object = Mapper<AttendanceDetailsModel>().map(JSONObject: data)
                    guard let parsedObject = object else {return}
                    if parsedObject.errorCode == 0 &&  parsedObject.status == "ok" {
                        
                        if let boardArray1 = parsedObject.result{
                            self.arrayResult = boardArray1
                            print("arrayResult Count = \(/self.arrayResult?.count)")
                            self.collectionView?.delegate   = self
                            self.collectionView?.dataSource = self
                            
                            self.collectionView?.reloadData()
                            self.collectionView?.isHidden = false
                        }
                        else{
                            self.collectionView?.isHidden = true
                            Messages.shared.show(alert: .oops, message: "No data found", type: .warning)
                        }
                        if /self.arrayResult?.count == 0{
                            self.collectionView?.isHidden = true
                        }
                    }
                    else {
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                case .failure(_):
                    break
                }
        }
    }
    //MARK: - getClass
    func getClass(){
        APIManager.shared.request(with: HomeEndpoint.myClasses(UserID: intUserId.toString), isLoader : false) {[weak self](response) in
            switch response{
            case .success(let responseValue):
                if let model = responseValue as? ContactsModel ,let classArr = model.myClasses{
                    self?.classArr = classArr
                    self?.openSelectClassVc()
                }
            case .failure(let msg):
                Messages.shared.show(alert: .oops, message: /msg, type: .warning)
            }
        }
    }
    //MARK: - openSelectClassVc
    func openSelectClassVc(){
        let storyboard = UIStoryboard(name :"Reports" , bundle: nil)
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "SelectClassViewController") as? SelectClassViewController {
            selectClassVc.classArr = classArr
            selectClassVc.tempClassArr = classArr
            selectClassVc.delegate = self
            self.presentVC(selectClassVc)
        }
    }
    //MARK: - ClkSegment
    @IBAction func ClkSegment(_ sender: Any) {
        if(segmentView?.selectedSegmentIndex == 0){
            intSegmentType = 1
            intUserTypeAPI = 1
        }
        else if(segmentView?.selectedSegmentIndex == 1){
            intSegmentType = 3
            intUserTypeAPI = 3
        }
        self.txtFieldSearch?.text = nil
        self.arrayResult?.removeAll()
        tblMenu?.isHidden = true
        self.containerView.isHidden = true
        collectionView?.reloadData()
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
//MARK:- UICollectionViewDelegate and UICollectionViewDataSource
extension MainSearchViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return /self.arrayResult?.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.MainSearchCollection.get, for: indexPath) as? MainSearchCollectionViewCell else {return UICollectionViewCell()}
        cell.modal = arrayResult?[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/2, height: 219)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        intPassUserId   = (/arrayResult?[indexPath.row].userID)
        intPassUserType = (/arrayResult?[indexPath.row].userType)
        self.performSegue(withIdentifier: "segueSearchUserStuStaff", sender: self)
    }
    // MARK: - prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueSearchUserStuStaff"
        {
            let segueAtten              = segue.destination as! StudentProfileViewController
            segueAtten.strUserType      = intPassUserType.toString
            segueAtten.strUserID        = intPassUserId.toString
            segueAtten.selectedIndex    =  /segmentView?.selectedSegmentIndex
        }
    }
}
extension MainSearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if intSegmentType == 1 {
            return 5
        }else {
            return 2
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.SearchCollectionCell.get, for: indexPath) as? SearchMenuTableViewCell else {return UITableViewCell()}
        if intSegmentType == 1 {
            cell.lblMenu.text  = (arrayMenu[indexPath.row]["name"] as! String)
            cell.imgView.image = UIImage(named: arrayMenu[indexPath.row]["image"] as! String)
        }
        else{
            cell.lblMenu.text  = (arrayMenuStaff[indexPath.row]["name"] as! String)
            cell.imgView.image = UIImage(named: arrayMenuStaff[indexPath.row]["image"] as! String)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if intSegmentType == 1 {
            intSearchBy                 = (arrayMenu[indexPath.row]["id"] as! Int)
            self.tblMenu?.isHidden      =  true
            txtFieldSearch?.placeholder = (arrayMenu[indexPath.row]["name"] as! String)
            txtFieldSearch?.text        = ""
            
            if indexPath.row == 1
            {
                if classArr != nil {
                    openSelectClassVc()
                }
                else {
                    getClass()
                }
            }
        }
        else{
            intSearchBy                 = (arrayMenuStaff[indexPath.row]["id"] as! Int)
            self.tblMenu?.isHidden      =  true
            txtFieldSearch?.placeholder = (arrayMenuStaff[indexPath.row]["name"] as! String)
            txtFieldSearch?.text        = ""
        }
        if intSearchBy == 2 {
            txtFieldSearch?.keyboardType = .numberPad
        }
        else {
            txtFieldSearch?.keyboardType = .URL
        }
        self.containerView.isHidden = true
        //containerView.removeFromSuperview()
    }
}

class SearchMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {super.setSelected(selected, animated: animated)}
}
class MainSearchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblName:    UILabel!
    @IBOutlet weak var imgView:    UIImageView!
    
    var modal : Any? {
        didSet {
            configure()
        }
    }
    func configure() {
        if let modal = modal as? Result {
            lblName.text   = modal.name
            
            if let SenderType = modal.userType {
                
                if SenderType == 1
                {
                    var strClass      = String()
                    var strName       = String()
                    
                    if let ClassName = modal.className{
                        strClass = ClassName
                    }
                    if let name = modal.name{
                        strName = name
                    }
                    lblName.text = "\(strName), (\(strClass))"
                }
                else if let name = modal.name{
                    if let ChildName = modal.designation{
                        lblName.text = "\(name), \(ChildName)"
                    }
                }
            }
            if let imgUrl = modal.photo
            {
                var strPrefix      = String()
                strPrefix          += strPrefix.imgPath()
                var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
                finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                let url            = URL(string: finalUrlString )
                let imgViewUser = UIImageView()
                imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                    let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                    self.imgView.image = img1
                }
            }
            else{
                self.imgView.image = R.image.noProfile_Big()
            }
        }
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
    }
}
extension MainSearchViewController : SearchByClassDelegate {
    
    func getSelectClass(selectClass: MyClasses?  , classArr : [MyClasses]?) {
        if let selectClass = selectClass {
            self.classID = selectClass.classID?.toString
            webMainSearch(strSearch:  /self.classID)
        }
    }
}
