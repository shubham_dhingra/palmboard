//  PushSettingViewController.swift
//  PalmBoard
//  Created by Shubham Dhingra on 18/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.


import UIKit

class PushSettingViewController: UIViewController {
    
    
    //MARK::- OUTLETS
    @IBOutlet weak var tableView : UITableView?
    
    //MARK::- VARIABLES
    var isFirstTime     : Bool = true
    var settingArr      : [ModuleList]?
    var tableDataSource : TableViewDataSource?{
        didSet{
            tableView?.dataSource = tableDataSource
            tableView?.delegate   = tableDataSource
        }
    }
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = AlertConstants.Settings.get
        reloadTable()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        webAPIGetPush()
    }
    
  
}

//MARK::- Confgiure TableView
extension PushSettingViewController {
    
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: settingArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.SettingsCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
          (cell as? SettingsCell)?.delegate = self
          (cell as? SettingsCell)?.index    = indexpath?.row
          (cell as? SettingsCell)?.modal    = item
        }
    }
    
    //MARK::- RELOAD TABLE
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = settingArr
            tableView?.reloadData()
        }
    }
    
}


//MARK::- PUSH SETTING API
// To get the  list of modules which have the setting option  to control
extension PushSettingViewController {
    
    func webAPIGetPush() {
        guard let user = self.getCurrentUser() else {return}
        APIManager.shared.request(with: HomeEndpoint.PushSetting(roleName: user.role), isLoader : isFirstTime) { (response) in
            self.handleRecentUpdateResponse(response: response)
        }
    }
    
    func handleRecentUpdateResponse(response: Response) {
        switch response {
        
        case .success(let resVal):
            self.handle(response : resVal)
        
        case .failure(_):
            break
        }
    }
    
    //handle response
    func handle(response : Any?){
        if let modal = response as? SettingModal {
            settingArr = modal.moduleList
            reloadTable()
        }
    }
}

//MARK::- SET SETTING API TO ACTIVATE & DEACTIVATE PARTICULAR MODULE
extension PushSettingViewController : ButtonActDelegate {
    
    func sendBtnTag(_ tag: Int) {
       webAPISetSetting(index: tag)
    }
    
    
    //API
    func webAPISetSetting(index: Int) {
        
        guard let moduleID   = self.settingArr?[index] else {return}
        let intModID: Int    = /moduleID.mdlID
        let boolStatus: Bool = !(/moduleID.isActive)
        guard let user = self.getCurrentUser() else {return}
        APIManager.shared.request(with: HomeEndpoint.SetSetting(MDLID: intModID , isActive: boolStatus.description ,roleName: /user.role), isLoader: isFirstTime) { (response) in
            self.handleSetSettingResponse(response: response,index: index)
        }
    }
    
    func handleSetSettingResponse(response: Response,index: Int) {
        
        switch response {
        case .success(let resVal):
            self.handleSetSetting(response : resVal,index: index)
        case .failure(_):
            break
        }
    }
    
    //MARK::- handle
    func handleSetSetting(response : Any?,index: Int){

        if let modal = response as? SettingModal {
            if modal.errorCode  == 0{
                self.settingArr?[index].isActive = !(/self.settingArr?[index].isActive)
                reloadTable()
            }
        }
    }
}
