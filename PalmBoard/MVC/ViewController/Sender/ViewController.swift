//  ViewController.swift
//  e-Care Pro
//  Created by Ravikant on 10/10/17.
//  Copyright © 2017 Franciscan. All rights reserved.

import UIKit
import CoreData
import EZSwiftExtensions

let maxLength = 1
let aSet      = NSCharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted

class ViewController: BaseViewController,UITextFieldDelegate{

    @IBOutlet weak var btnContinue:           CustomButtom?
    @IBOutlet weak var btnHelp:               UIButton?
    @IBOutlet weak var txtField1:             UITextField?
    @IBOutlet weak var txtField2:             UITextField?
    @IBOutlet weak var txtField3:             UITextField?
    @IBOutlet weak var txtField4:             UITextField?
    @IBOutlet weak var txtField5:             UITextField?
    @IBOutlet weak var txtField6:             UITextField?
    @IBOutlet var txtFieldOutletCollection :  [UITextField]!
    @IBOutlet weak var lblInvalid:            UILabel?
    @IBOutlet weak var btnFindCode:           UIButton?
    @IBOutlet weak var topInvalidSchoolCode:  NSLayoutConstraint?
    @IBOutlet weak var stackViewMultiplier:   NSLayoutConstraint?
    @IBOutlet weak var btnBack:               UIButton?
    
    var strSchoolCode       = String()
    var strSchoolName       = String()
    var strSchoolImgLogo    = String()
    var arraySlider         = [[String : Any]]()
    var arraySchoolList     = [[String : Any]]()
    var arrSearchData       = [[String : Any]]()
    var arrSchoolData       : [SchoolCodeTable]?
    
    var search:String         = ""
    var strSchCity            = String()
    var sMS_Prvd_ID           = Int()
    var countryCode           = Int()
    var supportPhone          = String()
    var themColor             = String()
    var webSite               = String()
    var supportEmail          = String()
    var additionalMobileUrl   = String()
    var marksEntryURL         = String()
    
    var feeReportUrl          = String()
    var feePaymentUrl         = String()
    var isFirstTime : Bool    = true
    var forChangeCode : Bool  = false
    var selectSchoolCode : String?
    var fromStarting : Bool   = true
    var isSchoolDeletePerformed : Bool = false
    var arraySliderImageURLs  = [[String : Any]]()
    var noOfControllers : Int = 0

    var yourAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
        NSAttributedString.Key.foregroundColor: UIColor.white,
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    
    // MARK: - viewDidLoad
    // MARK: - viewDidLoad
    override func viewDidLoad(){
        super.viewDidLoad()
        
        let initialCheck =  UserDefaults.standard.integer(forKey:"logged_in")
        if initialCheck == 2 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let UserverifyView = storyboard.instantiateViewController(withIdentifier: "UserverifyViewController") as? UserverifyViewController else {return}
            self.navigationController?.pushViewController(UserverifyView, animated: false)
        }
//        self.isNavBarHidden = true
        setUpForIphoneX()
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        onViewWillAppear()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.isNavBarHidden = false
    }
    
    // MARK: - onViewWillAppear
    func onViewWillAppear() {
      
        // relaod the table when add a new users
        NotificationCenter.default.addObserver(forName: Notification.Name(rawValue: NOTI.UPDATE_SCHOOL_DATA), object: nil, queue: nil) { (_) in
            self.reloadCollectionView()
            guard let vc = self.navigationController?.viewControllers else {return}
            self.noOfControllers = vc.count
            print("No of controllers",self.noOfControllers)
            if vc.count == 3  {
                if !self.isSchoolDeletePerformed {
                    for (_,viewController) in vc.enumerated() {
                        if viewController is AddUserViewController {
                            if let vc = viewController as? AddUserViewController {
                                vc.schoolData = self.arrSchoolData?.last
                                if let selectSchool = self.arrSchoolData?.last {
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.CHANGE_SCHOOL), object: nil, userInfo: ["schoolData" : selectSchool])
                                    self.navigationController?.popToViewController(viewController, animated: true)
                                }
                            }
                        }
                    }
                }
                else {
                    if let selectSchool = self.getCurrentSchool() {
                        self.updateSchool(selectSchool)
                    }
                }
            }
        }
        if forChangeCode || noOfControllers >= 3 {
            reloadCollectionView()
            btnBack?.isHidden  = false
            selectSchoolCode = DBManager.shared.getAllSchools().first?.schCode
            print("CURRENT SCHOOL CODE", /selectSchoolCode)
        }
        else {
            btnBack?.isHidden = true
            UserDefaults.standard.set(1, forKey: "logged_in")
            UserDefaults.standard.synchronize()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        collectionView?.isHidden = /arrSchoolData?.count == 0 || !forChangeCode
        stackViewMultiplier?.constant = 1
        Utility.shared.removeLoader()
      
        for (_,value) in txtFieldOutletCollection.enumerated()  {
                value.borderColorWithGreen()
        }
        yourAttributes = [
            NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
            NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x4EAD4A),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                        attributes: yourAttributes)
        btnFindCode?.setAttributedTitle(attributeString, for: .normal)
        
        topInvalidSchoolCode?.constant = 0
        
        if !(/lblInvalid?.isHidden) {
            for (_,value) in txtFieldOutletCollection.enumerated()  {
                value.borderColorWithGreen()
                value.text = nil
            }
        }
        
        lblInvalid?.isHidden = true
        
        if !forChangeCode {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
            view.addGestureRecognizer(tap)
        }
    }
    
    
    func reloadCollectionView() {
        arrSchoolData = DBManager.shared.getAllSchools()
        if isFirstTime {
            configureCollectionView()
        }
        else {
            collectionDataSource?.items = arrSchoolData
            collectionView?.reloadData()
        }
    }
    
    
    //MARK: textField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.lblInvalid?.isHidden   = true
        yourAttributes = [
            NSAttributedString.Key.font : R.font.ubuntuLight(size: 14.0)! ,
            NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x4EAD4A),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                        attributes: yourAttributes)
        btnFindCode?.setAttributedTitle(attributeString, for: .normal)
        self.txtField1?.textColorYellow(text: (self.txtField1?.text)!)
        self.txtField2?.textColorYellow(text: (self.txtField2?.text)!)
        self.txtField3?.textColorYellow(text: (self.txtField3?.text)!)
        self.txtField4?.textColorYellow(text: (self.txtField4?.text)!)
        self.txtField5?.textColorYellow(text: (self.txtField5?.text)!)
        self.txtField6?.textColorYellow(text: (self.txtField6?.text)!)
        
        for (_,value) in txtFieldOutletCollection.enumerated()  {
            value.borderColorWithGreen()
        }
        /********************************************************/
        if textField.tag != 10{
            animateTxtFld(txtFld: textField , up: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //print("textFieldShouldReturn )")
        if textField.tag == 10{
            return true // We do not want UITextField to insert line-breaks.
        }
        else{
            let nextTag: NSInteger = textField.tag + 1
            let nextResponder = textField.superview?.viewWithTag(nextTag)
            if (nextResponder != nil){
                // Found next responder, so set it.
                nextResponder?.becomeFirstResponder()
            } else {
                // Not found, so remove keyboard.
                textField.resignFirstResponder()
            }
            return false // We do not want UITextField to insert line-breaks.
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.tag == 10{
            return true
        }
        else{
            let currentString           = textField.text! as NSString
            let newString               = currentString.replacingCharacters(in: range, with: string)
            let compSepByCharInSet      = string.components(separatedBy: aSet)
            let numberFiltered          = compSepByCharInSet.joined(separator: "")
            
            if ((newString.count) <= maxLength && string == numberFiltered) == true
            {
                self.topInvalidSchoolCode?.constant = 0
                lblInvalid?.isHidden = true
                
                textField.addTarget(self, action: #selector(self.textFieldMoveNext(textfield: )), for: UIControl.Event.editingChanged)
            }
            return (newString.count) <= maxLength && string == numberFiltered
        }
    }
    @objc func textFieldMoveNext(textfield : UITextField)//Custom
    {
        let newString12 = textfield.text!
        //print("newString12 in next method = \(newString12)")
        if (newString12.count == 0){
            textFieldMoveBack(textfield: textfield)//Custom
        }
        else
        {   let nextTag = textfield.tag + 1
            let nextResponder = textfield.superview?.viewWithTag(nextTag)
            if (nextResponder != nil){
                nextResponder?.becomeFirstResponder()
            } else {
                textfield.resignFirstResponder()
            }
        }
    }
    
    func textFieldMoveBack(textfield : UITextField)//Custom
    {
        let nextTag = textfield.tag - 1
        let nextResponder = textfield.superview?.viewWithTag(nextTag)
        if (nextResponder != nil){
            nextResponder?.becomeFirstResponder()
        } else {
            textfield.resignFirstResponder()
        }
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool{
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        strSchoolCode = String(format:"\(/self.txtField1?.text)\(/self.txtField2?.text!)\(/self.txtField3?.text)\(/self.txtField4?.text)\(/self.txtField5?.text)\(/self.txtField6?.text)")
        
        if textField.tag != 10{
            animateTxtFld(txtFld: textField, up: false)
        }
        if strSchoolCode.count < 6  {
            //  btnContinue.isUserInteractionEnabled = false
            textField.layer.borderColor  = UIColor.flatGreen.cgColor
            if textField.tag != 10 {
                textField.textColorYellow(text: textField.text!)
            }
        }
        else if strSchoolCode.count == 6{
            //            btnContinue.isUserInteractionEnabled = true
        }
    }
    //MARK: - ClkBtnContinue
    @IBAction func ClkBtnContinue(_ sender: Any){
        self.schoolVerifyCode(strSchoolCodeVerify: strSchoolCode)
    }
    func schoolVerifyCode(strSchoolCodeVerify: String)
    {
        if strSchoolCodeVerify.count !=  6 {
            animateForError(msg: AlertMsg.incompleteSchoolCode.get)
            return
        }
        var strSchoolCodeverifyURL = String()
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.webAPIDomainNmae()
        strSchoolCodeverifyURL += "school/DTL?SchCode="
        strSchoolCodeverifyURL += strSchoolCodeVerify.uppercased()
        strSchoolCodeverifyURL += "&key="
        strSchoolCodeverifyURL += strSchoolCodeverifyURL.keyPath()
        Utility.shared.loader()
        btnFindCode?.isUserInteractionEnabled = false
        
        WebserviceManager.getJsonData(withParameter: strSchoolCodeverifyURL) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    if let result = results
                    {
                        if(result["ErrorCode"] as? Int == 0 && result["Status"] as? String == "ok")
                        {
                            Utility.shared.removeLoader()
                            self.strSchoolCode                  = strSchoolCodeVerify
                            self.isSchoolDeletePerformed        = false
                            self.btnFindCode?.isUserInteractionEnabled = true
                            UserDefaults.standard.synchronize()
                            DBManager.shared.SaveDataIntoDB(strSchoolCode: self.strSchoolCode, result: result , fromStarting : self.fromStarting)
                            
                            for (_,value) in self.txtFieldOutletCollection.enumerated()  {
                                value.text = ""
                            }
                            if !self.forChangeCode {

                                if(result["Active"] as? Int == 1){
                                    self.arraySliderImageURLs = (result["Slider"] as? [[String: Any]])!
                                   // self.performSegue(withIdentifier: "userVerify", sender: self)
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    guard let UserverifyView = storyboard.instantiateViewController(withIdentifier: "UserverifyViewController") as? UserverifyViewController else {return}
                                    UserverifyView.strSchoolCodeSender  = self.strSchoolCode
                                    UserverifyView.arraySliderImageURLs = self.arraySliderImageURLs
                                    self.strSchoolCode = ""
                                    self.pushVC(UserverifyView)
                                }
                            }
                        }
                        else if(result["Status"] as? String == "Invalid School Code" )
                        {
                            self.animateForError(msg : AlertMsg.schoolNotRegistered.get)
                            self.topInvalidSchoolCode?.constant        = 5
                            Utility.shared.removeLoader()
                            self.isSchoolDeletePerformed = false
                            self.btnFindCode?.isUserInteractionEnabled = true
                            
                            self.yourAttributes = [
                                NSAttributedString.Key.font : R.font.ubuntuRegular(size: 14.0)! ,
                                NSAttributedString.Key.foregroundColor : UIColor(rgb: 0x4EAD4A),
                                NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
                            let attributeString = NSMutableAttributedString(string: "Find Your School Code",
                                                                            attributes: self.yourAttributes)
                            self.btnFindCode?.setAttributedTitle(attributeString, for: .normal)
                        }
                    }
                    if (error != nil){
                        //print("errorLobby = \(error!)")
                        DispatchQueue.main.async
                            {
                                Utility.shared.removeLoader()
                                //self.btnFindCode?.isUserInteractionEnabled = true
                                switch (errorcode!){
                                case Int(-1009):
                                    let alert = UIAlertController(title: "", message: "The Internet connection appears to be offline.", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    
                                default:
                                    print("errorcode = \(String(describing: errorcode))")
                                }
                        }
                    }
            }
        }
    }
    // MARK: - animateTxtFld
    func animateTxtFld(txtFld: UITextField, up: Bool){
        
        let movementDistance = -120
        let movementDuration  = 0.3
        let movement = (up ? movementDistance : -movementDistance)
        UIView.animate(withDuration: movementDuration) {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: CGFloat(movement))
        }
    }
    
    func animateForError(msg : String){
        for (_,value) in txtFieldOutletCollection.enumerated()  {
            value.borderWidthColor()
            value.txtShakeAnimation()
            btnFindCode?.titleLabel?.font =  R.font.ubuntuBold(size: 14.0)
            // setfont UIFont(name: "Ubuntu-Light", size: 14)!
        }
        self.txtField1?.textColorError(text: (self.txtField1?.text!)!)
        self.txtField2?.textColorError(text: (self.txtField2?.text!)!)
        self.txtField3?.textColorError(text: (self.txtField3?.text!)!)
        self.txtField4?.textColorError(text: (self.txtField4?.text!)!)
        self.txtField5?.textColorError(text: (self.txtField5?.text!)!)
        self.txtField6?.textColorError(text: (self.txtField6?.text!)!)
        self.lblInvalid?.text      = msg
        self.lblInvalid?.isHidden  = false
    }
    
    
    // MARK: - dismissKeyboard
    @objc override func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    // MARK: - ClkBtnFindCode
    @IBAction func ClkBtnFindCode(_ sender: Any){
        let storyboard  = UIStoryboard(name: "Main", bundle: nil)
        guard let vc    = storyboard.instantiateViewController(withIdentifier: "FindSchoolCodeViewController") as? FindSchoolCodeViewController else {return}
        vc.fromStarting = self.fromStarting
        vc.delegate     = self
        vc.fromHelpVc   = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func btnHelpAct(_ sender: UIButton) {
        let storyboard           = UIStoryboard(name: "Main", bundle: nil)
        guard let helpController = storyboard.instantiateViewController(withIdentifier: "EnterCodeViewController") as? EnterCodeViewController else {return}
        self.navigationController?.pushViewController(helpController, animated: true)
    }
    
}
//MARK ::- Configure CollectionView Delegate & DataSource
extension ViewController {
    //MARK: - configure CollectionView..
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: arrSchoolData, collectionView: collectionView, cellIdentifier: CellIdentifiers.SchoolCell.get, headerIdentifier: nil, cellHeight: 128.0 , cellWidth: 120.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell = cell as? SchoolCell else {return}
            cell.index  = indexpath?.row
            cell.selectSchoolCode = self.selectSchoolCode
            cell.delegate = self
            cell.modal = item
        }
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ selectIndex: Int) {
        if selectIndex < /arrSchoolData?.count {
            if let selectSchool = arrSchoolData?[selectIndex] {
                updateSchool(selectSchool,true)
            }
        }
    }
    
    
    func updateSchool(_ selectSchool : SchoolCodeTable ,_ isPopNeeded : Bool? = false) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: NOTI.CHANGE_SCHOOL), object: nil , userInfo : ["schoolData" : selectSchool])
        if /isPopNeeded{
            self.navigationController?.popViewController(animated: false)
        }
        }
  }
extension ViewController : DeleteSchoolDelegate {
    func deleteSchool(_ index: Int) {
        isSchoolDeletePerformed = true
        if let school = arrSchoolData?[index] , let schoolCode = school.schCode , let schoolName = school.schName, arrSchoolData?.count != 0 {
            DBManager.shared.deleteSchoolfromDB(schoolCode , schoolName)
            reloadCollectionView()
        }
    }
}
extension ViewController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != txtField1 && touches.first != txtField2 && touches.first != txtField3 && touches.first != txtField4 && touches.first != txtField5 && touches.first != txtField6{
            self.view.endEditing(true)
        }
    }
}
extension ViewController : FindSchoolDelegate{
    func sendSchoolCode(schoolCode: String?) {
        guard let schoolCode = schoolCode else {return}
        
        if schoolCode.count != 6 {
            return
        }
        if schoolCode.count == 6 {
            strSchoolCode = schoolCode
            self.txtField1?.text = schoolCode[0].toString
            self.txtField2?.text = schoolCode[1].toString
            self.txtField3?.text = schoolCode[2].toString
            self.txtField4?.text = schoolCode[3].toString
            self.txtField5?.text = schoolCode[4].toString
            self.txtField6?.text = schoolCode[5].toString
          
            for (_,value) in txtFieldOutletCollection.enumerated()  {
                value.borderColorWithGreen()
            }
        }
    }
}

