import UIKit
//protocol MyProtocol: class {
//    func hideView(hide: Bool)
//}
class ShortProfileViewController: UIViewController {
    
    @IBOutlet weak var lblName:         UILabel?
    @IBOutlet weak var lblSubject:      UILabel?
    @IBOutlet weak var lblUsername:     UILabel?
    @IBOutlet weak var lblChildName:    UILabel?
    @IBOutlet weak var imgProfile: UIImageView? {
        didSet{
            self.imgProfile?.layer.cornerRadius = (/self.imgProfile?.frame.width / 2.0)
            self.imgProfile?.layer.borderWidth   = 5
            self.imgProfile?.layer.borderColor   = UIColor.white.cgColor
        }
    }
    
    @IBOutlet weak var imgViewCover:    UIImageView?
    @IBOutlet weak var scrollViewBG:    UIScrollView?
    @IBOutlet weak var viewGray:        UIView?
    @IBOutlet weak var imgViewChild:    UIImageView?
    @IBOutlet weak var imgViewParent:   UIImageView?
    @IBOutlet weak var lblParent:       UILabel?
    @IBOutlet weak var viewSocialMedia: UIView?
    @IBOutlet weak var btnFB:           UIButton?
    @IBOutlet weak var btnTwitter:      UIButton?
    @IBOutlet weak var btnLinkedIn:     UIButton?
    
    @IBOutlet weak var viewDOB:              UIView?
    @IBOutlet weak var viewFatherName:       UIView?
    @IBOutlet weak var ViewMotherName:       UIView?
    @IBOutlet weak var ViewAdress:           UIView?
    @IBOutlet weak var ViewContactNo:        UIView?
    
    @IBOutlet weak var lblDOB:               UILabel?
    @IBOutlet weak var lblFatherName:        UILabel?
    @IBOutlet weak var lblMotherName:        UILabel?
    @IBOutlet weak var lblAddress:           UILabel?
   // @IBOutlet weak var lblContactNo:         UILabel?
    @IBOutlet weak var txtViewContactNo:         UITextView?

    @IBOutlet weak var stackView:         UIStackView?
    
    
    var btnBack           = UIButton()
    var dictProfile       = [String: Any]()
    var intUserID         = Int()
    var intUserType       = Int()
    var strteacherSubject = String()
    // var intProfileType   = Int()
    // var delegate: MyProtocol?
    var lblSubjectHeight = CGFloat()
    var strSchoolCode = String()
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        //(for example: student name, Dob, address, contact detail, Father name, mother name etc
        //print("intUserType = \(intUserType)")
        if(self.intUserType == 1){
            self.imgProfile?.isUserInteractionEnabled = false
            
        }
        if(self.intUserType == 1 || self.intUserType == 3){
            viewSocialMedia?.isHidden = true
            lblParent?.isHidden       = true
            imgViewParent?.isHidden   = true
            imgViewChild?.isHidden    = true
            lblChildName?.isHidden    = true
        }
        lblChildName?.isHidden    = self.intUserType == 2 ? false : true
        
        btnBack                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnBack.frame           = CGRect(x : 0,y:  0,width : 22, height: 22)
        btnBack.addTarget(self, action: #selector(self.ClkCrossLeft(sender:)), for: .touchUpInside)
        btnBack.setImage(R.image.back(), for: UIControl.State.normal)
        
        let barButtonBack = UIBarButtonItem.init(customView: btnBack)
        self.navigationItem.leftBarButtonItem = barButtonBack
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCode =  schoolCode
        }
        
        guard let userType = self.getCurrentUser()?.userType else {return}
        
        viewDOB?.isHidden        = (userType == 3 && self.intUserType == 1) ? false : true
        viewFatherName?.isHidden = (userType == 3 && self.intUserType == 1) ? false : true
        ViewMotherName?.isHidden = (userType == 3 && self.intUserType == 1) ? false : true
        ViewAdress?.isHidden     = (userType == 3 && self.intUserType == 1) ? false : true
        ViewContactNo?.isHidden  = (userType == 3 && self.intUserType == 1) ? false : true
        stackView?.isHidden      = (userType == 3 && self.intUserType == 1) ? false : true
    }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        //self.delegate?.hideView(hide: false)
        self.shortProfileAPI(intUserID1: intUserID, intUserType1: intUserType)
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    @objc func ClkCrossLeft(sender:UIButton) {
        // self.delegate?.hideView(hide: false)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ClkBtnMedia(_ sender: UIButton) {
        var dictSocialMedia = [String: Any]()
        var strMedia        = String()
        
        if let SocialMedia = self.dictProfile["SocialMedia"] as? [String: Any]{
            dictSocialMedia = SocialMedia
        }
        if sender.tag == 1{
            if let facebook = dictSocialMedia["facebook"] as? String{
                strMedia = facebook
            }
        }
        else if sender.tag == 2{
            if let Twitter = dictSocialMedia["Twitter"] as? String{
                strMedia = Twitter
            }
        }
        else if sender.tag == 3{
            if let LinkedIn = dictSocialMedia["LinkedIn"] as? String{
                strMedia = LinkedIn
            }
        }
        if strMedia.count > 0 {
            UIApplication.shared.open(NSURL(string:strMedia)! as URL)
        }
        else {
            WebserviceManager.showAlert(message: "URL is not uploaded", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
            }, secondBtnBlock: {})
        }
    }
    // MARK: - shortProfileAPI method
    func shortProfileAPI(intUserID1: Int,intUserType1 : Int )
    {
        scrollViewBG?.isHidden = true
        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "user/Profile?SchCode="
        urlString += strSchoolCode
        urlString += "&key="
        urlString += urlString.keyPath()
        urlString += "&UserID="
        urlString += "\(intUserID1)"
        urlString += "&UserType="
        urlString += "\(intUserType1)"
        
        print("Profile urlString \n \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results,  _ error: Error?, _ errorcode: NSInteger?) in
            DispatchQueue.main.async
                {
                    Utility.shared.removeLoader()
                    if let result = results
                    {
                        if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                        {
                            if let dictProfile1 =  (result["Profile"]) as? [String: Any]{
                                self.dictProfile = dictProfile1
                            }
                            if(self.dictProfile.count == 0){
                                self.navigationItem.rightBarButtonItem = nil
                                for view in self.view.subviews{
                                    view.removeFromSuperview()
                                    view.isHidden = true
                                }
                                AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noDataFound(), fromVC: self)

                            }else{
                                //update Profile
                                self.scrollViewBG?.isHidden = false
                                self.updateLabels()
                            }
                        }else{
                            //print("Error in fetching data")
                            self.navigationController?.navigationItem.rightBarButtonItem = nil
                            self.navigationItem.rightBarButtonItem = nil
                            self.scrollViewBG?.isHidden = true
                            let alert = UIAlertController(title: "", message: "No data found", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
                                
                                //  self.delegate?.hideView(hide: false)
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
            }
        }
    }
    // MARK: - updateLabels method
    func updateLabels(){
        var profilePhotoUrl = String()
        var coverPhotoUrl   = String()
        var designation     = String()
        var childPhotoURL   = String()
        
        switch intUserType
        {
        case 3:
            print("Teachers")
            if let desig1 = self.dictProfile["Designation"] as? String{
                designation = desig1
            }else{
                designation = "N/A"
            }
            self.lblSubject?.text = designation
        case 1:
            print("Student")
            if let className1 = self.dictProfile["ClassName"] as? String{
                self.lblSubject?.text = "(" + className1 + ")"
            }
        case 2:
            print("Parents")
            self.lblSubject?.text = "Parent"
        default:
            break
        }
        if let name1 = self.dictProfile["Name"] as? String{
            self.lblName?.text = name1
        }
//        if let username1 = self.dictProfile["Username"] as? String{
//            self.lblUsername?.text = "@\(username1)"
//        }
        if let ChildName = self.dictProfile["ChildName"] as? String , let className = self.dictProfile["ClassName"] as? String{
            self.lblChildName?.text = "\(ChildName) ,(\(className))"
        }
        if let childPhotoURL1 = self.dictProfile["ChildPhoto"] as? String{
            childPhotoURL = childPhotoURL1
            
            let childUrl = childPhotoURL1.getImageUrl()
            imgViewChild?.loadURL(imageUrl: childUrl, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            
        }else{
            self.imgViewChild?.image = R.image.noProfile_Big()
        }
        if let photoUrl1 = self.dictProfile["Photo"] as? String{
            profilePhotoUrl = photoUrl1
        }
        if let coverUrl1 = self.dictProfile["CoverImg"] as? String{
            coverPhotoUrl = coverUrl1
        }
        if let strFr = self.dictProfile["FatherName"] as? String{
            lblFatherName?.text = strFr
        }
        if let strMo = self.dictProfile["MotherName"] as? String{
            lblMotherName?.text = strMo
        }
        if let strDOB = self.dictProfile["DOB"] as? String{
            lblDOB?.text = strDOB.dateChangeFormat()
        }
        if let strAddress = self.dictProfile["Address"] as? String{
            lblAddress?.text = strAddress
        }
        if let strContactMob = self.dictProfile["ContactMob"] as? String{
          //  lblContactNo?.text = strContactMob
            txtViewContactNo?.text = strContactMob
        }
        if profilePhotoUrl.count != 0 {
            self.imgProfile?.loadURL(imageUrl: profilePhotoUrl.getImageUrl(), placeholder: nil, placeholderImage: R.image.noProfile_Big())
        }
        else {
            self.imgProfile?.image = R.image.noProfile_Big()
        }
        
        if coverPhotoUrl.count != 0 {
            self.imgViewCover?.loadURL(imageUrl: coverPhotoUrl.getImageUrl(), placeholder: nil, placeholderImage: R.image.photogalleryPlaceholder())
        }
        else {
            self.imgViewCover?.image = R.image.noProfile_Big()
        }
       
    }
 
}
