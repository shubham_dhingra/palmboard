//  SurveyResDataViewController.swift
//  PalmBoard
//  Created by Shubham on 12/03/19.
//  Copyright © 2019 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions


class SurveyResDataViewController : BaseViewController {

    @IBOutlet weak var lblQuestion               : UILabel?
    @IBOutlet weak var tableViewHeightConstraint : NSLayoutConstraint?
    @IBOutlet weak var resultTableView           : UITableView?
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerView : UIView?
    
    var questions : SurveyQuestions?
    var pieChartModal : SurveyPieChartModal?
    var usersArr : [String] = ["All","Students","Parent", "Staff"]
    var surveyID    : Int?
    var isFirstTime : Bool = true
    var isFirstTimeLoadUserCollection   : Bool = true
    var isFirstTimeLoadResultCollection : Bool = true
    var selectIndex : Int = 0
    
    var surveyReportTableDataSource : SurveyReportDataSource?{
        didSet{
            resultTableView?.dataSource = surveyReportTableDataSource
            resultTableView?.delegate   = surveyReportTableDataSource
            resultTableView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onViewDidLoad()
    }
    
    func onViewDidLoad() {
        lblQuestion?.text =  /questions?.question?.trimmed()
        getQuestionResponse()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideMenu()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showMenu()
    }

}

//MARK::- CONFIGURE TABLE VIEW
extension SurveyResDataViewController {
    
    func configureResultTableView() {
       
        surveyReportTableDataSource  = SurveyReportDataSource(items: [""], height: 950.0, tableView: resultTableView, cellIdentifier: R.reuseIdentifier.surveyReportTableCell.identifier , estimatedHeight : 500.0 , selectIndex : self.selectIndex)
        
        surveyReportTableDataSource?.viewforHeaderInSection = {(section) in
            
            let surveySectionHeader =   SurveyUserSectionHeader.instanceFromNib() ?? UIView()
            surveySectionHeader.frame.size.height = 52.0
            surveySectionHeader.frame.size.width  = UIScreen.main.bounds.width
            (surveySectionHeader as? SurveyUserSectionHeader)?.surveyUserDTLArr = self.pieChartModal?.surveyUserDTL ?? []
            (surveySectionHeader as? SurveyUserSectionHeader)?.selectIndex = self.selectIndex
            (surveySectionHeader as? SurveyUserSectionHeader)?.reloadUserCollection()
            return surveySectionHeader
        
        }
        
        surveyReportTableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? SurveyReportTableCell)?.modal = self.pieChartModal
            (cell as? SurveyReportTableCell)?.surveyUserArr    =  self.pieChartModal?.surveyUserDTL ?? []
            (cell as? SurveyReportTableCell)?.selectIndex = self.selectIndex
            (cell as? SurveyReportTableCell)?.reloadResultCollection()
        }
        
        surveyReportTableDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
    }
    
    func reloadResultTable() {
        surveyReportTableDataSource?.selectIndex = self.selectIndex
        if isFirstTimeLoadResultCollection {
            configureResultTableView()
            isFirstTimeLoadResultCollection = !isFirstTimeLoadResultCollection
        }
        else {
            surveyReportTableDataSource?.items = [""]
            resultTableView?.reloadData()
        }
    }
    
}


//MARK::- Set the options name in a tableView
extension SurveyResDataViewController {
    
    func configureTableView() {
        guard let options = self.pieChartModal?.options else {
            return
        }
        
        tableDataSource  = TableViewDataSource(items: options, height: 50.0, tableView: tableView, cellIdentifier: CellIdentifiers.SurveyQuestionCell.get , estimatedHeight : 50.0)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? SurveyQuestionCell)?.index  = /indexpath?.row
            (cell as? SurveyQuestionCell)?.option = item
        }
        
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
    }
    
    func reloadTable() {
        
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = questions?.options
            tableView?.reloadData()
        }
        tableViewHeightConstraint?.constant = CGFloat((/questions?.options?.count * 50))
        headerView?.frame.size.height = CGFloat((/questions?.options?.count * 50) + 50)
        
//        containerViewHeight?.constant = (/tableViewHeightConstraint?.constant  + 50.0)
    
    }
}


extension SurveyResDataViewController {
   
    func getQuestionResponse() {
        APIManager.shared.request(with: HomeEndpoint.SurveyRes(SurId: /surveyID?.toString, QueID: /questions?.queID?.toString)) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    
    
    func handle(response : Any?){
        if let modal = response as? SurveyPieChartModal {
            self.pieChartModal = modal
            
            //Assign random color to options
            self.pieChartModal?.options = modal.options?.map({
                if $0.assignColor == nil {
                   $0.assignColor = UIColor.random()
                }
                return $0
            })
            
            //Reload the option Table after assign color to table.
            ez.runThisInMainThread {
                self.reloadTable()
                self.reloadResultTable()
            }
           
        }
    }
}
