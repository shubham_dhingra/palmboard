//  SurveyQuestionViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.

import UIKit

class SurveyQuestionViewController : BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var lblHeading : UILabel?
    
    //MARK::- VARIABLES
    var isFirstTime : Bool = true
    var survey : Survey?
    var surveyModal : SurveyModal?
    
    var surveyQuesTableDataSource : SurveyQuestionTableDataSource?{
        didSet{
            
            tableView?.dataSource = surveyQuesTableDataSource
            tableView?.delegate = surveyQuesTableDataSource
            tableView?.reloadData()
        }
    }
    
    //MARK::- LIFE CYLES
    override func viewDidLoad() {
        super.viewDidLoad()
        getQuestionsList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideMenu()
    }
    
    func getQuestionsList() {
        if let modal = survey {
            APIManager.shared.request(with: ECareEndPoint.surveyQuestions(SurId: /modal.surID?.toString)) { [weak self](response) in
                self?.handleResponse(response: response, responseBack: { (success) in
                    self?.handle(response : success)
                })
            }
        }
    }
    
    func handle(response : AnyObject?){
        if let modal = response as? SurveyModal {
            surveyModal = modal
            surveyModal?.SchoolCode = DBManager.shared.getAllSchools().first?.schCode
            surveyModal?.key = APP_CONSTANTS.KEY
            surveyModal?.SurID = survey?.surID
            surveyModal?.UserID = self.userID?.toInt()
            surveyModal?.UserType = self.userType?.toInt()
            reloadTable()
        }
        
        if (response as? String) != nil {
            if /survey?.resultDeclared {
                guard let vc = R.storyboard.module.surveyResultViewController() else {
                    return
                }
                vc.survey = survey
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                Messages.shared.show(alert: .success, message: AlertMsg.SurveySubmitSuccess.get, type: .success)
                self.popVC()
            }
        }
    }
    
    @IBAction func btnSubmitAct(_ sender : UIButton){
        
        guard let question = surveyModal?.SurveyQuestions else {
            return
        }
        
        for (index,ques) in question.enumerated() {
            
            if /ques.isAnsMandatory {
                if let options = ques.options {
                    let selectOption = options.filter { (item) -> Bool in
                        return /item.isSelected
                    }
                    if selectOption.count == 0 {
                        Messages.shared.show(alert: .oops, message: AlertMsg.mandatoryQuesAttempt.get, type: .warning)
                        tableView?.scrollToRow(at: IndexPath(row: 0, section: index), at: UITableView.ScrollPosition.top, animated: true)
                        return
                    }
                }
            }
        }
        
        var NoOfQuesNotAttempt : Int = 0
        for (_,ques) in question.enumerated() {
            
            if let options = ques.options {
                let selectOption = options.filter { (item) -> Bool in
                    return /item.isSelected
                }
                if selectOption.count == 0 {
                    NoOfQuesNotAttempt += 1
                }
                else {
                    break
                }
            }
        }
        
        if NoOfQuesNotAttempt == question.count {
              Messages.shared.show(alert: .oops, message: AlertMsg.AtleastOneQuestions.get, type: .warning)
              return
        }
        
        APIManager.shared.request(with: ECareEndPoint.postAnswer(body: surveyModal?.toJSON())) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
}


//MARK::- CONFIGURE TABLE VIEW
extension SurveyQuestionViewController {
    
    func configureTableView() {
        surveyQuesTableDataSource  = SurveyQuestionTableDataSource(items: surveyModal?.SurveyQuestions, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.SurveyQuestionCell.get ,fromVc : self)
        
        surveyQuesTableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            if let item = item as? SurveyQuestions {
                (cell as? SurveyQuestionCell)?.index = indexpath?.row
                (cell as? SurveyQuestionCell)?.quesNo = indexpath?.section
                (cell as? SurveyQuestionCell)?.delegate = self
                (cell as? SurveyQuestionCell)?.ismultiSelectQues = /item.isMultiSelect
                (cell as? SurveyQuestionCell)?.modal    = item.options?[/indexpath?.row]
                (cell as? SurveyQuestionCell)?.linearView?.isHidden = !(indexpath?.row == /item.options?.count - 1)
            }
        }
        surveyQuesTableDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            surveyQuesTableDataSource?.items = surveyModal?.SurveyQuestions
            tableView?.reloadData()
        }
    }
}

extension SurveyQuestionViewController : AnswerDelegate {
    
    func optionSelectDelegate(quesNo: Int, optionNo: Int) {
        
        // multi choice ques
        if /self.surveyModal?.SurveyQuestions?[quesNo].isMultiSelect {
            self.surveyModal?.SurveyQuestions?[quesNo].options?[optionNo].isSelected = !(/self.surveyModal?.SurveyQuestions?[quesNo].options?[optionNo].isSelected)
        }
        else {
            
            // single choice question
            self.surveyModal?.SurveyQuestions?[quesNo].options = self.surveyModal?.SurveyQuestions?[quesNo].options?.map({ (option) -> Option in
                option.isSelected = false
                return option
            })
            
            self.surveyModal?.SurveyQuestions?[quesNo].options?[optionNo].isSelected = !(/self.surveyModal?.SurveyQuestions?[quesNo].options?[optionNo].isSelected)
        }
        self.tableView?.reloadSections(IndexSet([quesNo]), with: .none)
    }
}

