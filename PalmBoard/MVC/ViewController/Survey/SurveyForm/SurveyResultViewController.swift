//
//  SurveyResultViewController.swift
//  PalmBoard
//
//  Created by Shubham on 14/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.
//

import UIKit

class SurveyResultViewController: BaseViewController {
    
    var surveyModal : SurveyModal?
    var survey : Survey?
    var forCheckResponses : Bool = false
    var isFirstTime : Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideMenu()
        getSurveyResult()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.showMenu()
    }
    
    var surveyQuesTableDataSource : SurveyQuestionTableDataSource?{
        didSet{
            
            tableView?.dataSource = surveyQuesTableDataSource
            tableView?.delegate = surveyQuesTableDataSource
            tableView?.reloadData()
        }
    }
    
    override func btnBackAct(_ sender: UIButton) {
        guard let viewControllers = self.navigationController?.viewControllers else {
            return
        }
        for (_ , value) in viewControllers.enumerated() {
            if value is SurveyViewController {
                self.navigationController?.popToViewController(value, animated: true)
                break
            }
        }
    }
}


extension SurveyResultViewController {
    func getSurveyResult() {
        
        APIManager.shared.request(with: ECareEndPoint.surveyResult(UserID: self.userID, UserType: self.userType , SurID : /self.survey?.surID) , isLoader : isFirstTime) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    func handle(response : AnyObject?){
        if let modal = response as? SurveyModal {
            surveyModal = modal
            reloadTable()
        }
    }
    
    
    
}

//MARK::- CONFIGURE TABLE VIEW
extension SurveyResultViewController {
    
    func configureTableView() {
        surveyQuesTableDataSource  = SurveyQuestionTableDataSource(items: surveyModal?.SurveyQuestions, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.SurveyQuestionCell.get, isSurveyResult: true , forCheckResponses : self.forCheckResponses , fromVc : self)
        
        surveyQuesTableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            if let item = item as? SurveyQuestions {
                (cell as? SurveyQuestionCell)?.index = indexpath?.row
                (cell as? SurveyQuestionCell)?.quesNo = indexpath?.section
                (cell as? SurveyQuestionCell)?.isResult = true
                (cell as? SurveyQuestionCell)?.totalUserAttempt = /item.response
                (cell as? SurveyQuestionCell)?.ismultiSelectQues = /item.isMultiSelect
                (cell as? SurveyQuestionCell)?.modal    = item.options?[/indexpath?.row]
                (cell as? SurveyQuestionCell)?.linearView?.isHidden = !(indexpath?.row == /item.options?.count - 1)
            }
        }
        surveyQuesTableDataSource?.aRowSelectedListener = {(indexpath,cell) in
        }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            surveyQuesTableDataSource?.items = surveyModal?.SurveyQuestions
            tableView?.reloadData()
        }
    }
}
