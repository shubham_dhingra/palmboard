//  SurveyViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 08/01/19.
//  Copyright © 2019 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class SurveyViewController : BaseViewController {

    //MARK::- OUTLETS
    @IBOutlet weak var lblNoSurvey : UILabel?
    @IBOutlet weak var imgNoSurvey : UIImageView?

    //MARK::- VARIABLES
    var isFirstTime : Bool = true
    var surveyList : [Survey]?
    var pageNo : Int = 1
    var refreshControl = UIRefreshControl()
    var totalSurvey : Int = 0
    var fromWhereGoInside : Bool = false
    var forCheckResponses : Bool = false
    
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
       
        refreshControl.tintColor = UIColor.flatGreen
        addRefereshControl()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        fromWhereGoInside = false
        self.showMenu()
        self.isNavBarHidden = true
        pageNo = 1
        getSurveyList(pg : pageNo)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = fromWhereGoInside
    }
}

//Mark::- API ZONE
extension SurveyViewController {
    func getSurveyList(pg : Int) {

        APIManager.shared.request(with: ECareEndPoint.getSurveyList(UserID: self.userID, UserType: self.userType, isReport : forCheckResponses.description ,pg : pg) , isLoader : isFirstTime) {[weak self](response) in
            self?.refreshControl.endRefreshing()
            self?.handleResponse(response: response, responseBack: { (success) in
            self?.handle(response : success)
            })
        }
    }
    
    
    func handle(response : AnyObject?){
        if let modal = response as? SurveyModal , let totalSurvey = modal.total {
            if modal.errorCode == 0 {
                
                self.totalSurvey = totalSurvey
                imgNoSurvey?.isHidden = self.totalSurvey != 0
                lblNoSurvey?.isHidden =  self.totalSurvey != 0
                
                if let  surveyListArr = modal.surveyList {
                    
                    if pageNo == 1 {
                        self.surveyList = []
                    }
                    if totalSurvey == self.surveyList?.count {
                        print("All survey already loaded")
                        return
                    }
                    if pageNo == 1 {
                        self.surveyList = surveyListArr
                    }
                    else {
                        if surveyListArr.count != 0 {
                            _ =  surveyListArr.map{(self.surveyList?.append($0))}
                        }
                    }
                    reloadTable()
                }
                else {
                    if pageNo == 1 {
                        self.surveyList = []
                        reloadTable()
                    }
                }
            }
        }
    }
}

extension SurveyViewController {
    
    func configureTableView() {
        
        tableDataSource  = TableViewDataSource(items: self.surveyList, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.SurveyListCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? SurveyListCell)?.forCheckResponses = self.forCheckResponses
            (cell as? SurveyListCell)?.indexpath    = indexpath
            (cell as? SurveyListCell)?.modal    = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            if self.forCheckResponses {
                self.surveyList?[indexpath.row].resultDeclared = true
            }
            if let survey = self.surveyList?[indexpath.row] {
                self.forCheckResponses ? self.checkResultDeclaration(survey) : self.didSelect(indexpath.row)
            }
        }
    }
    
    func didSelect(_ index : Int){
        
        guard let selectSurvey  = self.surveyList?[index] else {
            return
        }
        
        if /selectSurvey.isOpen {
            
            if !(/selectSurvey.isResponded) {
                guard let vc = R.storyboard.module.surveyQuestionViewController() else {
                    return
                }
                vc.survey = selectSurvey
                fromWhereGoInside = true
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else {
                self.checkResultDeclaration(selectSurvey)
            }
        }
            
        // survey form is closed
        else {
           self.checkResultDeclaration(selectSurvey)
        }
    }
    
    
    func checkResultDeclaration(_ selectSurvey : Survey) {
        if /selectSurvey.resultDeclared {
            guard let vc = R.storyboard.module.surveyResultViewController() else {
                return
            }
            vc.survey = selectSurvey
            vc.forCheckResponses = self.forCheckResponses
            fromWhereGoInside = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            if /selectSurvey.isResponded {
                 self.showAlert(title: AlertConstants.Attention.get, description: AlertMsg.AlreadySubmitSurvey.get)
            }
            else {
                self.showAlert(title: AlertConstants.Attention.get, description: AlertMsg.SurveyClosed.get)
            }
        }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = surveyList
            tableView?.reloadData()
        }
    }
}

//MARK::- PAGINATION METHODS
extension SurveyViewController {
    
    func addRefereshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView?.addSubview(refreshControl)
    }
    
    // MARK: - refresh
    @objc func refresh(sender:AnyObject) {
        pageNo  = 1
        totalSurvey = 0
        self.getSurveyList(pg: pageNo)
    }
    
    // MARK: - scrollViewDidEndDragging
    func reloadMoreData(_ scrollView : UIScrollView) {
        if self.totalSurvey == /self.tableDataSource?.items?.count {
            //            Messages.shared.show(alert: .oops, message: "No More Updates", type: .warning , style:  .bottom)
            return
        }
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size   = scrollView.contentSize
        let inset  = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        // let reloadDistance = CGFloat(30.0)
        if y > h  {
            
            if self.totalSurvey != /self.tableDataSource?.items?.count {
                pageNo = pageNo + 1
                self.getSurveyList(pg: pageNo)
            }
        }
    }
    
    func showAlert(title : String? , description : String?){
        
        AlertsClass.shared.showAlertController(withTitle: title, message: /description, buttonTitles: [AlertConstants.Ok.get]) { (_) in
            return
        }
    }
    
}



