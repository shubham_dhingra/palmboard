//  StaffSyllabusViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 29/03/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit

class StaffSyllabusViewController: BaseViewController {
    @IBOutlet weak var imgViewNoSyllabus: UIImageView?
    @IBOutlet weak var lblNoSyllabus    : UILabel?
    var arrayStaffSyllabus              : [Syllabus]?
    var arraySyllabusLST                : [SyllabusLST]?
    var arrayClass                      : [String]?
    
    var isFirstTime : Bool           = true
    var isFirstTimeCollection : Bool = true
    
    var strSender    = String()
    var intUserId    = Int()
    var strPrefix    = String()
    var eventIndex   : Int? = 0
    var btnTitle     : UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.title                = "Syllabus"
        btnTitle                  =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle?.frame            = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle?.setTitle("Syllabus", for: UIControl.State.normal)
        btnTitle?.titleLabel?.font = R.font.ubuntuMedium(size: 17)
        btnTitle?.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        self.navigationItem.titleView = btnTitle
        
        if let schoolCode = self.getCurrentSchool()?.schCode {strSender      =  schoolCode}
        if let userId     = self.getCurrentUser()?.userID    {self.intUserId =  Int(userId)}
        
        let nib = UINib(nibName: "Syllabus", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: CellIdentifiers.cellSyllabus.get)
        tableView?.tableFooterView    = UIView(frame: CGRect.zero)
        
        self.webAPISyllabus()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    //MARK:- webAPISyllabus
    func webAPISyllabus(){
        imgViewNoSyllabus?.isHidden = true
        lblNoSyllabus?.isHidden     = true
        
        APIManager.shared.request(with: HomeEndpoint.StaffSyllabus(UserId: self.userID)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK::- handle
    func handle(response : Any?){
        if let model = response as? AllSyllabusModal
        {
            arrayStaffSyllabus = model.syllabus
            self.imgViewNoSyllabus?.isHidden = /(arrayStaffSyllabus?.count) != 0
            self.lblNoSyllabus?.isHidden     = /(arrayStaffSyllabus?.count) != 0
            
            if /arrayStaffSyllabus?.count > 0 {
                arrayClass = arrayStaffSyllabus?.map({ (value) -> String in
                    return /value.className
                })
            }
            if /(arrayStaffSyllabus?.count) > 0  {
                self.reloadCollectionView()
                collectionView?.performBatchUpdates({},completion: { (finished) in
                    self.collectionView?.collectionViewLayout.invalidateLayout()
                    let indexPathForFirstRow = IndexPath(row: 0, section: 0)
                    self.collectionView?.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .top)
                    self.collectionDataSource?.collectionView(self.collectionView!, didSelectItemAt: indexPathForFirstRow)
                })
            }
        }
    }
}
//MARK ::- Configure CollectionView Delegate & DataSource
extension StaffSyllabusViewController {
    //MARK: - configure CollectionView
    func configureCollectionView() {
        collectionDataSource =  CollectionViewDataSource(items: arrayClass, collectionView: collectionView, cellIdentifier: CellIdentifiers.cellStaffSyllabus.get, headerIdentifier: nil, cellHeight: 40.0 , cellWidth: 100.0)
        
        collectionDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let cell  = cell as? StaffSyllabusCollectionViewCell else {return}
            cell.index      = indexpath?.row
            cell.eventIndex = self.eventIndex
            cell.modal      = item
        }
        collectionDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelectCollection(indexpath.row, indexpath)
        }
    }
    func didSelectCollection(_ selectRow: Int,_ indexpath: IndexPath) {
        navigateToPGVc(selectRow,indexpath)
    }
    func navigateToPGVc(_ index : Int,_ indexpath: IndexPath) {
        
        if let array = arrayStaffSyllabus?[index].syllabusLST
        {
            arraySyllabusLST                 = array
            self.imgViewNoSyllabus?.isHidden = true
            self.lblNoSyllabus?.isHidden     = true
            
            collectionView?.setContentOffset(CGPoint.zero, animated: true)
            collectionView?.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
            eventIndex = indexpath.row
            reloadCollectionView()
            reloadTable()
            tableView?.isHidden = false
        }
        else{
            tableView?.isHidden              = true
            self.imgViewNoSyllabus?.isHidden = false
            self.lblNoSyllabus?.isHidden     = false
            collectionView?.setContentOffset(CGPoint.zero, animated: true)
            collectionView?.scrollToItem(at: indexpath, at: .centeredHorizontally, animated: true)
            eventIndex = indexpath.row
            reloadCollectionView()
        }
    }
    func reloadCollectionView() {
        if isFirstTimeCollection {
            configureCollectionView()
            isFirstTimeCollection = false
        }
        else {
            collectionDataSource?.items = arrayClass
            collectionView?.reloadData()
        }
    }
}
//MARK ::- Configure tableView Delegate & DataSource
extension StaffSyllabusViewController {
    
    func configureTableView() {
        tableDataSource = TableViewDataSource(items: self.arraySyllabusLST, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.cellSyllabus.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? SyllabusTableViewCell)?.index = indexpath?.row
            (cell as? SyllabusTableViewCell)?.delegate = self
            (cell as? SyllabusTableViewCell)?.modal = item
           // (cell as? SyllabusTableViewCell)?.btnView?.tag = /indexpath?.row
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    func didSelect(_ index : Int){
        print("didSelect")
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = self.arraySyllabusLST
            tableView?.reloadData()
        }
    }
}
extension StaffSyllabusViewController : SyllabusDelegate {
    // MARK: - viewSyllabus
    func viewSyllabus(tag: Int) {
        if let strFilePath        = arraySyllabusLST?[tag].filePath//arrayStaffSyllabus?[tag].filePath
        {
            guard let vc   = R.storyboard.main.segueWebviewNotice() else {return}
            strPrefix      = strPrefix.imgPath1()
            vc.strfinalUrl = "\(strPrefix)" +  "\(strFilePath)"
            vc.strTitle    = "Syllabus"
            self.pushVC(vc)
        }
        else
        {
            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.fileIsNotCorrect(), fromVC: self)
        }
    }
}
