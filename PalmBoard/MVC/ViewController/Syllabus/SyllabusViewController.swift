//  SyllabusViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 14/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
import UIKit
class SyllabusViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var imgViewNoSyllabus: UIImageView?
    @IBOutlet weak var lblNoSyllabus:     UILabel?
    
    var arraySyllabusLST : [SyllabusLST]?
    var intClassId   = Int()
    var strPrefix    = String()
    var btnTitle     =  UIButton()
    var isFirstTime : Bool   = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        btnTitle                 =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btnTitle.frame           = CGRect(x : 0,y:  0,width : 280, height: 40)
        btnTitle.setTitle("Syllabus", for: UIControl.State.normal)
        btnTitle.titleLabel?.font =  R.font.ubuntuMedium(size: 17)
        btnTitle.setTitleColor(UIColor(rgb: 0x545454), for: .normal)
        self.navigationItem.titleView = btnTitle
        
        if let ClassId = self.getCurrentUser()?.classID{
            intClassId =  Int(ClassId)
        }
        let nib = UINib(nibName: "Syllabus", bundle: nil)
        tableView?.register(nib, forCellReuseIdentifier: CellIdentifiers.cellSyllabus.get)
        tableView?.tableFooterView    = UIView(frame: CGRect.zero)
        
        self.webAPISyllabus()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
    }
    //MARK:- webAPISyllabus
    func webAPISyllabus(){
        imgViewNoSyllabus?.isHidden = true
        lblNoSyllabus?.isHidden     = true
        APIManager.shared.request(with: HomeEndpoint.AllSyllabus(ClassID:intClassId.toString)) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK::- handle
    func handle(response : Any?){
        if let model = response as? AllSyllabusModal
        {
            arraySyllabusLST = model.syllabusLST
            /(arraySyllabusLST?.count) > 0 ? self.reloadTable() : print("Blank Array")
            self.imgViewNoSyllabus?.isHidden = /(arraySyllabusLST?.count) != 0
            self.lblNoSyllabus?.isHidden     = /(arraySyllabusLST?.count) != 0
        }
    }
}
extension SyllabusViewController {
    
    func configureTableView() {
        
        tableDataSource = TableViewDataSource(items: arraySyllabusLST, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.cellSyllabus.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? SyllabusTableViewCell)?.index = indexpath?.row
            (cell as? SyllabusTableViewCell)?.delegate = self
           // (cell as? SyllabusTableViewCell)?.btnView?.tag = /indexpath?.row
            (cell as? SyllabusTableViewCell)?.modal = item
        }
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = self.arraySyllabusLST
            tableView?.reloadData()
        }
    }
}
extension SyllabusViewController : SyllabusDelegate {
    // MARK: - viewSyllabus
    func viewSyllabus(tag: Int) {
        if let strFilePath        = arraySyllabusLST?[tag].filePath
        {
            guard let vc   = R.storyboard.main.segueWebviewNotice() else {return}
            strPrefix       = strPrefix.imgPath1()
            vc.strfinalUrl = "\(strPrefix)" +  "\(strFilePath)"
            vc.strTitle = "Syllabus"
            self.pushVC(vc)
        }
        else
        {
            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.fileIsNotCorrect(), fromVC: self)
        }
    }
}
//MARK: - scrollview Delegate method
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//        if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
//            parent.hideBar(boolHide: true)
//        }
//    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
//            if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
//                parent.hideBar(boolHide: false)
//            }
//        }
//        RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
//    }

