//  TimeTableStaffViewController.swift
//  e-Care Pro
//  Created by Ravikant Bhardwaj on 17/05/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class TimeTableStaffViewController: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var tblViewStaff: UITableView?
    @IBOutlet weak var segmentControl: UISegmentedControl?
    @IBOutlet weak var segmentBgView:  UIView?
    @IBOutlet weak var segmentHeight:  NSLayoutConstraint?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var btnTitle:       UIButton?
    @IBOutlet weak var imgViewNoTimeTable: UIImageView?
    @IBOutlet weak var lblNoTimeTable:     UILabel?
    
    
    var classId             = Int()
    var userId              = Int()
    var strSchoolCodeSender = String()
    var strSelectEntity     : String?
    var userType            = Int()
    var intListChoice       = Int()
    var arrayTeacherwise    = [[String: Any]]()
    var arrayClasswise      = [[String: Any]]()
    
    var otherUserId      = Int()
    var otherUserType    = Int()
    var otherUserPhoto   = String()
    var otherUserSubject = String()
    var themeColor      = String()
    var intSegment      = 1
    var intClassID      = Int()
    var intReceiverID   = Int()
    var intReceiverType = Int()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        btnTitle?.setTitle("Timetable", for: .normal)
        btnTitle?.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        imgViewNoTimeTable?.isHidden = true
        lblNoTimeTable?.isHidden     = true
    }
    
    @objc func ClkBtnView(sender:UIButton) {
        self.tblViewStaff?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    
    
    // MARK: -  viewDidLoad
    override func viewDidLoad()
    {
        super.viewDidLoad()
        arrayTeacherwise.removeAll()
        
        themeColor = self.getCurrentSchool()?.themColor ?? ""
        segmentControl?.backgroundColor = themeColor.hexStringToUIColor()
      
        segmentBgView?.backgroundColor  = themeColor.hexStringToUIColor()
        segmentControl?.selectedSegmentIndex = 0
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.userId =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.userType =  Int(userType)
        }
        
        if let ClassID = self.getCurrentUser()?.classID{
            self.intClassID =  Int(ClassID)
        }
        self.WebAPISegmentIndexFirst()
    }
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        let img = UIImage()
        self.navigationController?.navigationBar.shadowImage = img
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    // MARK: - viewDidLayoutSubviews
    override func viewDidLayoutSubviews() {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    // MARK: - titlePressed
    @IBAction func titlePressed(_ sender: UIButton) {
        self.collectionView?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    // MARK: - WebAPISegmentIndexFirst
    func WebAPISegmentIndexFirst()
    {
       Utility.shared.loader()
        self.collectionView?.isHidden = true
        self.tblViewStaff?.isHidden = true
        
        self.segmentControl?.isUserInteractionEnabled = false
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Teacher/MyClass?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(userId)"
        print("urlString \n \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                print("result",result)
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0){
                    DispatchQueue.main.async {
                       
                        self.segmentControl?.isUserInteractionEnabled = true
                        
                        if let allTeacher = (result["MyClasses"] as? [[String: Any]]){
                            self.arrayClasswise = allTeacher
                            self.tblViewStaff?.isHidden = false
                            self.tblViewStaff?.reloadData()
                        }
                        else{
                            self.imgViewNoTimeTable?.isHidden = false
                            self.lblNoTimeTable?.isHidden     = false
                            self.tblViewStaff?.isHidden       = true
                        }
                    }
                }
            }
            if (error != nil){
                
                DispatchQueue.main.async{
                    self.tblViewStaff?.isHidden = false
                    self.view.isUserInteractionEnabled = true
                    if let code = errorCode {
                        switch (code){
                        case Int(-1009):
                            Utility.shared.internetConnectionAlert()
                        default:
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                            print("errorcode = \(String(describing: code))")
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - WebAPISegmentIndexSecond
    func WebAPISegmentIndexSecond()
    {
        Utility.shared.loader()
        self.collectionView?.isHidden = true
        self.tblViewStaff?.isHidden = true
        
        self.segmentControl?.isUserInteractionEnabled = false
        
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        urlString += "Message/StaffContact?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&UserID=\(userId)&UserType=\(userType)"
        print("urlString \n \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            DispatchQueue.main.async {
                Utility.shared.removeLoader()
            }
            if let result = results{
                print("result ***" ,result)
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0){
                    DispatchQueue.main.async {
                       self.segmentControl?.isUserInteractionEnabled = true
                        
                        if let allTeacher = (result["Contacts"] as? [[String: Any]]){
                            self.arrayTeacherwise = allTeacher
                            self.collectionView?.isHidden = false
                            self.collectionView?.reloadData()
                        }
                        else{
                            self.imgViewNoTimeTable?.isHidden = false
                            self.lblNoTimeTable?.isHidden     = false
                            self.collectionView?.isHidden     = true
                        }
                    }
                }
            }
            if (error != nil){
                
                DispatchQueue.main.async{
                   
                    self.tblViewStaff?.isHidden = false
                    self.view.isUserInteractionEnabled = true
                    if let code = errorCode {
                        switch (code){
                        case Int(-1009):
                            Utility.shared.internetConnectionAlert()
                        default:
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                            print("errorcode = \(String(describing: code))")
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: -  segmentChanged
    @IBAction func segmentChanged(_ sender: UISegmentedControl)
    {
        imgViewNoTimeTable?.isHidden = true
        lblNoTimeTable?.isHidden     = true
        
        if(segmentControl?.selectedSegmentIndex == 0){
            intSegment = 0
            self.WebAPISegmentIndexFirst()
        }
        else if(segmentControl?.selectedSegmentIndex == 1){
            intSegment = 1
            self.WebAPISegmentIndexSecond()
        }
        self.segmentControl?.isUserInteractionEnabled = false
    }
    
    // MARK: -  collectionView delegate methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayTeacherwise.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeTableStaffCollectionCell", for: indexPath) as! TimeTableStaffCollectionViewCell
        
        if let imgUrl = self.arrayTeacherwise[indexPath.row]["Photo"] as? String
        {
            var strPrefix      = String()
            strPrefix          += strPrefix.imgPath()
            var finalUrlString = "\(strPrefix)" +  "\(imgUrl)"
            finalUrlString     = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            let url            = URL(string: finalUrlString )
            let imgViewUser = UIImageView()
            imgViewUser.af_setImage(withURL: url!, placeholderImage: R.image.noProfile_Big(), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false) { (response) in
                
                let img1 = imgViewUser.image?.af_imageRoundedIntoCircle()
                cell.imgViewStaff.image = img1
            }
        }
        else{
            cell.imgViewStaff.image = R.image.noProfile_Big()
        }
        if let name = self.arrayTeacherwise[indexPath.row]["Name"] as? String{
            cell.lblName.text = name
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("Func: didSelectItemAt = \(indexPath.row)")
        if let ReceiverID = self.arrayTeacherwise[indexPath.row]["ReceiverID"] as? Int{
            self.intReceiverID    = ReceiverID
        }
        if let ReceiverType = self.arrayTeacherwise[indexPath.row]["ReceiverType"] as? Int{
            self.intReceiverType = ReceiverType
        }
        if let strSelectEntity = self.arrayTeacherwise[indexPath.row]["Name"] as? String {
            self.strSelectEntity = strSelectEntity
        }
        self.performSegue(withIdentifier: "TTstaffToOther", sender: self)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/2, height: 140)
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TTstaffToOther"
        {
            let segveLikes = segue.destination as! TimeTableViewController
            segveLikes.intClassId     = self.intClassID
            segveLikes.segmentSelected = segmentControl?.selectedSegmentIndex
            segveLikes.strSelectEntity   = self.strSelectEntity
            segveLikes.intUserID      = self.intReceiverID
            segveLikes.intUserType    = self.intReceiverType
            segveLikes.intFromStaff  = 1
        }
    }
}
class TimeTableStaffCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgViewStaff: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
class TimeTableStaffCell: UITableViewCell {
    @IBOutlet weak var lblClassName:         UILabel!
}
extension TimeTableStaffViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayClasswise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellStaffTT", for: indexPath) as! TimeTableStaffCell
        if let ClassName  = self.arrayClasswise[indexPath.row]["ClassName"] as? String{
            cell.lblClassName.text = ClassName
        }
        return cell
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tblViewStaff?.deselectRow(at: indexPath, animated: true)
        if let ClassID  = self.arrayClasswise[indexPath.row]["ClassID"] as? Int{
            intClassID = ClassID
        }
        if let strSelectEntity = self.arrayClasswise[indexPath.row]["ClassName"] as? String {
            self.strSelectEntity = strSelectEntity
        }
        self.performSegue(withIdentifier: "TTstaffToOther", sender: self)
    }
}
