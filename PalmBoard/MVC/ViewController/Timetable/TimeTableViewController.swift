import UIKit

class TimeTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var scrollViewTimetable:     UIScrollView?
    @IBOutlet weak var tblTimeTable:            UITableView?
    @IBOutlet weak var collectionViewTimeTable: UICollectionView?
    @IBOutlet weak var viewBG:                  UIView?
    @IBOutlet weak var btnDaywise:              UIButton?
    @IBOutlet weak var btnToday:                UIButton?
    
    @IBOutlet weak var heightCollectionview: NSLayoutConstraint?
    
    @IBOutlet weak var imgViewNoTimeTable: UIImageView?
    @IBOutlet weak var lblNoTimeTable: UILabel?
    
    var arrayTimetable           = [[String: Any]]()
    var arrayTimetableCollection = [[String: Any]]()
    var arrayTimeToday           = [[String: Any]]()
    
    var intClassId               = Int()
    var intUserID                = Int()
    var intUserType              = Int()
    var intFromStaff              = Int()
    
    var strSchoolCodeSender      = String()
    var strSelectEntity          : String?
    var strDay                   = String()
    
    var segmentSelected   : Int? = -1
    
    /*********************P code*****************/
    var eventIndex    = Int()
    var themeColor = String()
    /********************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func ClkBtnView(sender:UIButton) {
        self.tblTimeTable?.setContentOffset(CGPoint(x:0,y:0), animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Timetable"
        
        if intFromStaff != 1 {
            navigationItem.hidesBackButton = true
        }
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        themeColor = /self.getCurrentSchool()?.themColor
        //collectionViewTimeTable.backgroundColor = themeColor.hexStringToUIColor()
        viewBG?.backgroundColor                 = themeColor.hexStringToUIColor()
        
        self.imgViewNoTimeTable?.isHidden = true
        self.lblNoTimeTable?.isHidden     = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        if let strSelectEntity = strSelectEntity {
            let mainView = UIView(frame : CGRect(x : 0,y:  0,width : 280, height: 40))
            let mainButton = addButton(frame: CGRect(x : 0,y:  0,width : 280, height: 20), title: "Timetable", fontSize: 17.0, textColor: UIColor.flatLightBlack, isEnabled: true)
            mainView.addSubview(mainButton)
            let subHeadingBtn = addButton(frame: CGRect(x : 0,y:  mainButton.frame.size.height ,width : 280, height: 20), title: strSelectEntity, fontSize: 14.0, textColor: UIColor.flatLightBlack, isEnabled: true)
            mainView.addSubview(subHeadingBtn)
            self.navigationItem.titleView = mainView
        }
            
        else {
            let mainButton = addButton(frame: CGRect(x : 0,y:  0,width : 280, height: 40), title: "Timetable", fontSize: 17.0, textColor: UIColor.flatLightBlack, isEnabled: true)
            self.navigationItem.titleView = mainButton
        }
        
        heightCollectionview?.constant = 0
        self.eventIndex = 0
        
        self.collectionViewTimeTable?.delegate   = nil
        self.collectionViewTimeTable?.dataSource = nil
        self.tblTimeTable?.delegate              = nil
        self.tblTimeTable?.dataSource            = nil
        self.tblTimeTable?.tableFooterView       = UIView(frame: CGRect.zero)
        
        btnToday?.titleLabel?.font    = R.font.ubuntuMedium(size: 25)
        btnDaywise?.titleLabel?.font  =  R.font.ubuntuLight(size: 25)
        btnDaywise?.alpha             = 0.50
        
        if let schoolCode = self.getCurrentSchool()?.schCode {
            strSchoolCodeSender =  schoolCode
        }
       
        if segmentSelected == -1 {
            
        if let userId = self.getCurrentUser()?.userID {
                self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
        if let ClassId = self.getCurrentUser()?.classID{
            self.intClassId =  Int(ClassId)
        }
            
        }
        
        if intFromStaff == 1 {
            print("intUserType = \(intUserType)")
            print("intUserID = \(intUserID)")
            
        }
        
        strDay = Date().dayOfWeek()!
        print(Date().dayOfWeek()!)
        
        self.tblTimeTable?.rowHeight            = UITableView.automaticDimension
        self.tblTimeTable?.estimatedRowHeight   = 200
        
        self.tblTimeTable?.isHidden            = true
        self.collectionViewTimeTable?.isHidden = true
        
        self.webApiTimeTable()
    }
    
    func addButton(frame : CGRect , title : String , fontSize : CGFloat , textColor : UIColor? ,isEnabled : Bool) -> UIButton{
        
        let btn              =  UIButton(type: UIButton.ButtonType.custom) as UIButton
        btn.frame           =  frame
        btn.setTitle(title, for: UIControl.State.normal)
        btn.titleLabel?.font =  R.font.ubuntuMedium(size: fontSize)
        if let color = textColor {
            btn.setTitleColor(color, for: .normal)
        }
        if isEnabled{
            btn.addTarget(self, action: #selector(self.ClkBtnView(sender:)), for: .touchUpInside)
        }
        return btn
    }
    
    func webApiTimeTable()
    {

        Utility.shared.loader()
        var urlString = String()
        urlString += urlString.webAPIDomainNmae()
        
        //case of teacher
        if intUserType == 3 {
            
            //case of prinicipal wants to see the atttendance teacher wise and teacher itself wants to see his timetable
            if segmentSelected == -1 || segmentSelected == 1{
                urlString += "Academic/TeacherTimetable?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&TeacherID=\(intUserID)"
            }
            
            //case of principal
            if segmentSelected == 0 {
              urlString += "Academic/Timetable?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"
            }
        }
       //case of parent and student
        else {urlString += "Academic/Timetable?SchCode=\(strSchoolCodeSender)&key=\(urlString.keyPath())&ClassID=\(intClassId)"
        }
        print("urlString = \(urlString)")
        WebserviceManager.getJsonData(withParameter: urlString) { (results, error, errorCode) in
            
            DispatchQueue.main.async {
                  Utility.shared.removeLoader()
            }
            
            if let result = results{
                
                if((result["Status"] as! String) == "ok" && result["ErrorCode"] as! Int == 0)
                {
                    print("Result == \(result)")
                    DispatchQueue.main.async {
                        
                        if let arrayAll = (result["Data"] as? [[String: Any]]){
                            self.arrayTimetable = arrayAll
                        }
                        self.imgViewNoTimeTable?.isHidden = self.arrayTimetable.count != 0
                        self.lblNoTimeTable?.isHidden = self.arrayTimetable.count != 0
                        if self.strDay == "Sunday"{
                            
                            self.imgViewNoTimeTable?.isHidden = false
                            self.lblNoTimeTable?.isHidden     = false
                        }
                        else{
                            for (index,_) in  self.arrayTimetable.enumerated()
                            {
                                var strDayFr0mAPI = String()
                                strDayFr0mAPI    = self.arrayTimetable[index]["Day"] as! String
                                
                                switch strDayFr0mAPI {
                                case "Day 1":
                                    strDayFr0mAPI = "Monday"
                                case "Day 2":
                                    strDayFr0mAPI = "Tuesday"
                                case "Day 3":
                                    strDayFr0mAPI = "Wednesday"
                                case "Day 4":
                                    strDayFr0mAPI = "Thursday"
                                case "Day 5":
                                    strDayFr0mAPI = "Friday"
                                case "Day 6":
                                    strDayFr0mAPI = "Saturday"
                                default:
                                    break
                                }
                                if self.strDay.uppercased() == strDayFr0mAPI.uppercased()
                                {
                                    var tempArray = [[String: Any]]()
                                    tempArray     = self.arrayTimetable[index]["TimeTable"] as! [[String: Any]]
                                    self.arrayTimetableCollection = tempArray
                                    self.arrayTimeToday = tempArray
                                    print("arrayTimetableCollection == \(self.arrayTimetableCollection)")
                                    break
                                }
                            }
                            self.tblTimeTable?.delegate   = self
                            self.tblTimeTable?.dataSource = self
                            self.tblTimeTable?.isHidden   = false
                            self.tblTimeTable?.reloadData()
                        }
                        print("arrayTimetable = \(self.arrayTimetable)")
                    }
                }
            }
            if (error != nil){
                
                DispatchQueue.main.async{
                    self.tblTimeTable?.isHidden = false
                    self.view.isUserInteractionEnabled = true
                    if let code = errorCode {
                        switch (code){
                        case Int(-1009):
                            Utility.shared.internetConnectionAlert()
                        default:
                            AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.somethingWentWrong(), fromVC: self)
                            print("errorcode = \(String(describing: code))")
                        }
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if arrayTimetable.count > 0{
            return arrayTimetable.count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print("Func: cellForItemAt = ")
        
        let cellCollection = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTimetable", for: indexPath) as! CustomTimetableCollectionViewCell
        
        cellCollection.lblDayName?.text = arrayTimetable[indexPath.row]["Day"] as? String
        
        if eventIndex == indexPath.row {
            cellCollection.lblDayName?.font      = R.font.ubuntuMedium(size: 17)
            cellCollection.lblDayName?.textColor = UIColor(rgb: 0x545454)
        }else {
            cellCollection.lblDayName?.font      =  R.font.ubuntuRegular(size: 17)
            cellCollection.lblDayName?.textColor = UIColor(rgb: 0xD6D6D6)
        }
        return cellCollection
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
       // print("Func: didSelectItemAt = \(indexPath.row)")
        
        if self.arrayTimetable.count == 0{
            return
        }
        
        if let array = self.arrayTimetable[indexPath.row]["TimeTable"] as? [[String: Any]]
        {
            self.arrayTimetableCollection = array
            /*******p code*/
            collectionViewTimeTable?.setContentOffset(CGPoint.zero, animated: true)
            collectionViewTimeTable?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            eventIndex = indexPath.row
            collectionView.reloadData()
            /*********/
            
            self.tblTimeTable?.delegate   = self
            self.tblTimeTable?.dataSource = self
            self.tblTimeTable?.isHidden   = false
            
            self.tblTimeTable?.reloadData()
            // self.tblTimeTable.setContentOffset(CGPoint.zero, animated: true)
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.tblTimeTable?.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        else{
            tblTimeTable?.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.arrayTimetableCollection.count > 0{
            return self.arrayTimetableCollection.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellTimetable", for: indexPath as IndexPath) as! customTimetableTableViewCell
        
        var intPeriod = Int()
        intPeriod                     = arrayTimetableCollection[indexPath.row]["Period"]  as! Int
        
        if segmentSelected == -1 {
            if intUserType == 3 {
                cell.lblSubject?.text      = arrayTimetableCollection[indexPath.row]["ClassName"] as? String
                cell.lblTeachBy?.text      = arrayTimetableCollection[indexPath.row]["Subject"] as? String
            }
            else{
                cell.lblSubject?.text      = arrayTimetableCollection[indexPath.row]["Subject"] as? String
                cell.lblTeachBy?.text      = arrayTimetableCollection[indexPath.row]["TeachBy"] as? String
            }
        }
        else {
            if segmentSelected == 1 {
                cell.lblSubject?.text      = arrayTimetableCollection[indexPath.row]["ClassName"] as? String
                cell.lblTeachBy?.text      = arrayTimetableCollection[indexPath.row]["Subject"] as? String
            }
            else{
                cell.lblSubject?.text      = arrayTimetableCollection[indexPath.row]["Subject"] as? String
                cell.lblTeachBy?.text      = arrayTimetableCollection[indexPath.row]["TeachBy"] as? String
            }
        }
        
        
        cell.lblPeriodNumber?.text = String(describing:intPeriod)//String(describing:intPeriod)
        cell.lblPeriodNumber?.textColor = themeColor.hexStringToUIColor()
        cell.lblOrdinal?.textColor = themeColor.hexStringToUIColor()
        cell.lblOrdinal?.text      = intPeriod.ordinal
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tblTimeTable?.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160.0
    }
    /*
    //MARK: - scrollview Delegate method
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if (scrollView == tblTimeTable){
            if   let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                parent.hideBar(boolHide: true)
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if (scrollView == tblTimeTable){
            let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
                if let parent =  self.navigationController?.parent as? CustomTabbarViewController{
                    parent.hideBar(boolHide: false)
                }
            }
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
        }
    }
 */
    @IBAction func ClkBtnToday(_ sender: Any)
    {
        heightCollectionview?.constant = 0
        let indexPath = IndexPath(row: 0, section: 0)
        
        btnToday?.titleLabel?.font   =  R.font.ubuntuMedium(size: 25)
        btnDaywise?.titleLabel?.font =  R.font.ubuntuLight(size: 25)
        btnToday?.alpha              = 1.0
        btnDaywise?.alpha            = 0.50
        self.collectionViewTimeTable?.delegate   = nil
        self.collectionViewTimeTable?.dataSource = nil
        self.collectionViewTimeTable?.isHidden   = true
        
        self.arrayTimetableCollection = arrayTimeToday
        if self.arrayTimetable.count == 0 {
            return
        }
        self.tblTimeTable?.scrollToRow(at: indexPath, at: .top, animated: true)
        self.tblTimeTable?.delegate   = self
        self.tblTimeTable?.dataSource = self
        self.tblTimeTable?.reloadData()
    }
    @IBAction func ClkBtnDaywise(_ sender: Any)
    {
        heightCollectionview?.constant = 52
        btnDaywise?.titleLabel?.font =  R.font.ubuntuMedium(size: 25)
        btnToday?.titleLabel?.font   =  R.font.ubuntuLight(size: 25)
        btnDaywise?.alpha            = 1.00
        btnToday?.alpha              = 0.50
        
        self.tblTimeTable?.delegate   = nil
        self.tblTimeTable?.dataSource = nil
        self.tblTimeTable?.isHidden   = true
        
        self.collectionViewTimeTable?.delegate   = self
        self.collectionViewTimeTable?.dataSource = self
        self.collectionViewTimeTable?.isHidden   = self.arrayTimetable.count == 0
        self.collectionViewTimeTable?.setContentOffset(CGPoint.zero, animated: true)
        
        self.collectionViewTimeTable?.performBatchUpdates({},
                                                          completion: { (finished) in
                                                            print("collection-view finished reload done")
                                                            self.tblTimeTable?.delegate   = self
                                                            self.tblTimeTable?.dataSource = self
                                                            self.tblTimeTable?.isHidden   = false
                                                            
                                                            self.collectionViewTimeTable?.collectionViewLayout.invalidateLayout()
                                                            self.collectionView(self.collectionViewTimeTable!, didSelectItemAt: IndexPath(item: 0, section: 0))
        })
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}
extension Int {
    
    var ordinal: String {
        var suffix: String
        let ones: Int = self % 10
        let tens: Int = (self/10) % 10
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(suffix)"//\(self)
    }
}
extension Date
{
    func dayOfWeek() -> String?
    {
        let dateFormatter        = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}
