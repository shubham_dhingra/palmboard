//
//  customTimetableTableViewCell.swift
//  e-Care Pro
//
//  Created by Ravikant Bhardwaj on 17/11/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import UIKit

class customTimetableTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTeachBy:      UILabel?
    @IBOutlet weak var lblOrdinal: UILabel?
    @IBOutlet weak var lblSubject:      UILabel?
    @IBOutlet weak var lblPeriodNumber: UILabel?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
