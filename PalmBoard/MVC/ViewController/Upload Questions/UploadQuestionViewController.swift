import UIKit
import Alamofire

class UploadQuestionViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView?
    @IBOutlet weak var lblName:         UILabel?
    
    @IBOutlet weak var imgViewProfile:  UIImageView?
    @IBOutlet weak var imgViewQuestion: UIImageView?
    @IBOutlet weak var txtViewQuestion: UITextView?
    @IBOutlet weak var btnAddImage:     UIButton?
    @IBOutlet weak var btnCross:        UIButton?
    @IBOutlet weak var txtViewHeight:   NSLayoutConstraint?
    @IBOutlet weak var imgViewHeight:   NSLayoutConstraint?
    
    @IBOutlet weak var bluredView: UIView?
    
    var userObjects     = [UserTable]()
    var userId          = Int()
    var userCatId       = Int()
    var userType        = Int()
    var strName1        = String()
    var strClass        = String()
    var userPhotoUrl    = String()
    var schoolCode      = String()
    var imagePicker     = UIImagePickerController()
    var imageData: Data?
    var intQuestionType   = Int()
    //var textViewInitialHeight = CGFloat()
    var textViewHeightTemp   = CGFloat()
    var imageName            = String()
    var imgViewInitialHeight = CGFloat()
    var themeColor           = String()
    
    @IBAction func ClkBtnBack(_ sender: Any) {
        if txtViewQuestion?.text.count == 0 {
            self.popVC()
        }
        else if txtViewQuestion?.text != "Type your question" {
            WebserviceManager.showAlert(message: "Are you sure you want to discard this change?", title: "", firstBtnText: "Cancel", secondBtnText: "Discard", firstBtnBlock: {
            }, secondBtnBlock: {
                self.popVC()
            })
        }
        else {
            self.popVC()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
        
        imgViewInitialHeight     = (imgViewHeight?.constant)!
        btnCross?.isHidden        = true
        imgViewQuestion?.isHidden = true
        // textViewInitialHeight  = txtViewHeight.constant
        textViewHeightTemp       = (txtViewHeight?.constant)!
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        themeColor = /self.getCurrentSchool()?.themColor
        txtViewQuestion?.tintColor = themeColor.hexStringToUIColor()
        
        btnAddImage?.setTitleColor(themeColor.hexStringToUIColor(), for: .normal)
        
        self.bluredView?.isHidden            = true
        
        if let schoolCode1 = self.getCurrentSchool()?.schCode {
            schoolCode =  schoolCode1
        }
        if let CatId = self.getCurrentUser()?.categoryId {
            userCatId =  Int(CatId)
        }
        if let userId = self.getCurrentUser()?.userID {
            self.userId =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType {
            self.userType =  Int(userType)
        }
        strName1    = /self.getCurrentUser()?.name
        strClass    = /self.getCurrentUser()?.userClass
        if let url = self.getCurrentUser()?.photoUrl?.getImageUrl() {
            self.imgViewProfile?.loadURL(imageUrl: url, placeholder: nil, placeholderImage: R.image.noProfile_Big())
            self.imgViewProfile?.layer.cornerRadius =  (self.imgViewProfile?.frame.height)! / 2
            self.imgViewProfile?.clipsToBounds = true
        }
        else {
            self.imgViewProfile?.image =  R.image.noProfile_Big()
        }
        
        lblName?.text = "\(strName1), \(strClass)"
        
        NotificationCenter.default.addObserver(self, selector:#selector(UploadQuestionViewController.cancelTextView), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        singleTap.cancelsTouchesInView = false
        singleTap.numberOfTapsRequired = 1
        scrollView?.addGestureRecognizer(singleTap)
    }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        self.cancelTextView()
    }
    @objc func cancelTextView(){
        
        self.txtViewQuestion?.endEditing(true)
        
        let textValue = self.txtViewQuestion?.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(textValue == ""){
            
            self.txtViewQuestion?.text    = "Type your question"
            self.txtViewQuestion?.font = R.font.ubuntuItalic(size: 15)
            self.txtViewQuestion?.textColor = UIColor(red: 217/255 , green: 217/255 , blue: 217/255,alpha: 1.0 )
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if(textView.text == "Type your question"){
            textView.text = ""
        }
        textView.font = R.font.ubuntuRegular(size: 18)
        textView.textColor = UIColor(red: 110/255 , green: 110/255 , blue: 110/255,alpha: 1.0)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var minimumWords = 0
        let contentSize  = self.txtViewQuestion?.sizeThatFits((self.txtViewQuestion?.bounds.size)!)
        let numLinesDecimal = (textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom) / (textView.font?.lineHeight)!
        let numLines = floor(numLinesDecimal)
        var textViewtempframe = textView.frame
        
        if text.count == 0 && range.length > 0 {
            minimumWords = 5
            //print("numLines = \(numLines)")
            if(numLines > 1)
            {
                textViewtempframe.size.height = self.textViewHeightTemp - 20
                txtViewHeight?.constant = textViewtempframe.size.height
            }
            else if(numLines == 1){
                self.textViewHeightTemp = (self.txtViewHeight?.constant)!
            }
        }else
        {
            minimumWords = 3
            if(numLines < 5){
                self.txtViewHeight?.constant = (contentSize?.height)!
                self.textViewHeightTemp = (self.txtViewHeight?.constant)!
            }
        }
        
        if(textView.text.count  > minimumWords ){
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.navigationItem.rightBarButtonItem?.tintColor = themeColor.hexStringToUIColor()//UIColor(red: 255/255, green: 187/255, blue: 0/255, alpha:1)
        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
            
        }
        guard range.location == 0 else {
            return true
        }
        
        let newString = (textView.text as NSString).replacingCharacters(in: range, with: text) as NSString
        return newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if(textView.text == UIPasteboard.general.string)
        {
            let contentSize  = self.txtViewQuestion?.sizeThatFits((self.txtViewQuestion?.bounds.size)!)
            if(textView.text.count > 4)
            {
                self.txtViewHeight?.constant = (contentSize?.height)!
                self.textViewHeightTemp = (self.txtViewHeight?.constant)!
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 255/255, green: 187/255, blue: 0/255, alpha:1)
            }else
            {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                //self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha:1)
                self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
                
            }
            
        }else if(textView.text.count == 0)
        {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            // self.navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha:1)
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.darkGray
            
        }
        
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: textView.text, options: [], range: NSRange(location: 0, length: textView.text.utf16.count))
        
        if(matches.count > 0)
        {
            for match in matches {
                guard let range = Range(match.range, in: textView.text) else { continue }
                textView.text.replaceSubrange(range, with: "")
            }
        }
    }
    @IBAction func addImagePressed(_ sender: UIButton){
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    // MARK: - imagePickerController
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage else {
            return
        }
        let ratio = image.size.width / image.size.height
        
        if self.view.frame.width > imgViewInitialHeight {
            let newHeight              = self.view.frame.width / ratio
            imgViewHeight?.constant     = newHeight
            imgViewQuestion?.frame.size = CGSize(width: self.view.frame.width, height: newHeight)
        }
        else{
            let newWidth               = self.view.frame.height * ratio
            imgViewQuestion?.frame.size = CGSize(width: newWidth, height: (imgViewHeight?.constant)!)
        }
        
        guard let data = image.jpegData(compressionQuality: 0.5) else
        {
            return
        }
        
        imageData =  data
        imagePicker.dismiss(animated: true, completion: nil)
        
        self.btnAddImage?.isHidden     = true
        self.btnCross?.isHidden        = false
        self.imgViewQuestion?.isHidden = false
        self.imgViewQuestion?.image   = image
    }
    // MARK: - uploadImageWithQuestion
    func uploadImageWithQuestion(){
        
        let parameters = [
            "SchoolCode": self.schoolCode
        ]
        let url = APIConstants.basePath + "Upload/Question?SchCode=\(schoolCode)"
        
        if let data = self.imageData {
            print("upload images data \(data)")
            let intMBData = data.count/1024/1024
            print("intMBData \(intMBData)")
            print("self.imageName \(self.imageName)")
            if (intMBData < 5){
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    
                    for (key,value) in parameters {
                        multipartFormData.append((value).data(using: .utf8)!, withName: key)
                    }
                    multipartFormData.append(self.imageData!, withName: "\(self.imageName)" ,fileName: "\(self.imageName)" , mimeType: "image/jpg")
                }, to:url)
                { (result) in
                    
                    switch result{
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            // print("Upload Progress: \(progress.fractionCompleted)")
                        })
                        upload.responseJSON { response in
                            if  let statusCode = response.response?.statusCode{
                                //print("statusCode \(statusCode)")
                                if(statusCode == 201){
                                    self.intQuestionType = 2
                                    self.uploadQuestion()
                                }
                            }else{
                                Utility.shared.removeLoader()
                                self.bluredView?.isHidden            = true
                                WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Retry", secondBtnText: "Cancel", firstBtnBlock: {
                                    
                                    self.uploadImageWithQuestion()
                                    
                                }, secondBtnBlock: {})
                            }
                        }
                        
                    case .failure(let encodingError):
                        Utility.shared.removeLoader()
                        self.bluredView?.isHidden            = true
                        print(encodingError)
                    }
                }
            }
            else{
                WebserviceManager.showAlert(message: "Image with less than 5 MB is allowed to be uploaded.", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
            }
        }
    }
    // MARK: - uploadQuestion
    func uploadQuestion (){
        
        var strKey          = String()
        strKey             += strKey.keyPath()
        let trimmedQuestion = self.txtViewQuestion?.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let parameters: Parameters =
            [
                "SchoolCode": self.schoolCode,
                "key":        strKey,
                "UserType":   self.userType,
                "UserID":     self.userId,
                "Question":   trimmedQuestion,
                "QueType":    self.intQuestionType,
                "QueImg":     imageName,
                "CatID":      self.userCatId
        ]
        
        let urlString = APIConstants.basePath + "Question/Create"
        
        WebserviceManager.post(urlString: urlString, param: parameters, completionBlock: { (results, error, errorCode) in
            
            if let result = results{
                
                if(result["Status"] as? String == "ok" && result["ErrorCode"] as? Int == 0)
                {
                    Utility.shared.removeLoader()
                    self.bluredView?.isHidden            = true
                    
                    WebserviceManager.showAlert(message: "Thanks For Your Submission. Your Question will be added after Verification", title: "", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                        
                        self.txtViewQuestion?.font = R.font.ubuntuItalic(size: 15)
                        self.txtViewQuestion?.textColor = UIColor(red: 217/255 , green: 217/255 , blue: 217/255,alpha: 1.0 )
                        self.txtViewQuestion?.text     = "Type your question"
                        self.txtViewQuestion?.endEditing(true)
                        self.btnCross?.isHidden        = true
                        self.imgViewQuestion?.isHidden = true
                        self.imgViewQuestion?.image    = nil
                        self.btnAddImage?.isHidden     = false
                        self.txtViewHeight?.constant   = 30
                        self.navigationItem.rightBarButtonItem?.isEnabled = false
                        self.scrollView?.setContentOffset(.zero, animated: false)
                        
                        // self.performSegue(withIdentifier: "customTabSegve2", sender: self)
                        
                    }, secondBtnBlock: {})
                }
            }
            if (error != nil){
                
                DispatchQueue.main.async
                    {
                        Utility.shared.removeLoader()
                        
                        self.bluredView?.isHidden            = true
                        
                        switch (errorCode!){
                            
                        case Int(-1009):
                            
                            WebserviceManager.showAlert(message: "Internet connection appears to be offline.", title: "", firstBtnText: "Retry", secondBtnText: "Cancel", firstBtnBlock: {
                                
                                self.uploadQuestion()
                                
                            }, secondBtnBlock: {})
                            
                        default:
                            print("errorcode = \(String(describing: errorCode))")
                        }
                }
            }
        })
    }
    // MARK: - donePressed
    @IBAction func donePressed(_ sender: UIBarButtonItem) {
        Utility.shared.loader()
        self.bluredView?.isHidden            = false
        self.txtViewQuestion?.resignFirstResponder()
        
        if(self.txtViewQuestion?.text != "Type your question"){
            
            if(self.imageData != nil){
                //question with image
                let date     = Date()
                let calendar = Calendar.current
                let month    = calendar.component(.month, from: date)
                let year    = calendar.component(.year, from: date)
                let day     = calendar.component(.day, from: date)
                let hour    = calendar.component(.hour, from: date)
                let minute  = calendar.component(.minute, from: date)
                let second  = calendar.component(.second, from: date)
                
                imageName = "\(self.userId)\(self.userType)\(year)\(month)\(day)\(hour)\(minute)\(second).jpg"
                self.uploadImageWithQuestion()
            }else{
                //simple question
                imageName = ""
                self.intQuestionType = 1
                self.uploadQuestion()
            }
        }
    }
    // MARK: - crossPressed
    @IBAction func crossPressed(_ sender: UIButton) {
        self.imgViewQuestion?.image = UIImage(named: "")
        self.imgViewQuestion?.isHidden = true
        self.btnAddImage?.isHidden   = false
        self.btnCross?.isHidden   = true
        self.imageData = nil
    }
}
