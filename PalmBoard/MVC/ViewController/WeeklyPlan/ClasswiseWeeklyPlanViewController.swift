//  ClasswiseWeeklyPlanViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 02/12/18.
//  Copyright © 2018 Franciscan. All rights reserved

import UIKit
import EZSwiftExtensions

class ClasswiseWeeklyPlanViewController: BaseViewController {
    
    
    //MARK::- OUTLETS
    @IBOutlet weak var btnRightFilter:UIButton?
    @IBOutlet weak var lblSelectClass: UILabel?
    @IBOutlet weak var lblSubject:     UILabel?
    @IBOutlet weak var subHeading   : UILabel?
    @IBOutlet var heightSVConstraint: NSLayoutConstraint?
    @IBOutlet weak var viewClass:    UIView?
    @IBOutlet weak var viewSubject: UIView?
    @IBOutlet weak var imgViewNoWeeklyPlan: UIImageView?
    @IBOutlet weak var lblNoWeeklyPlan:     UILabel?
    
    //MARK::- VARIABLES
     var isFirstTime    : Bool = true
    // var isHeight     : Bool = true
    var weeklyPlanArr  : [WeeklyPlanListSchool]?
    var planList        : [WeeklyPlanList]?
    var intPaggination : Int = 1
    var strSelectDate : String?
    var userRole      : Users?
    var action        : Action?
    var classArr      : [MyClasses]?
    var subjectArr    : [Subjects]?
    var selectSubject : Subjects?
    var selectClass   : MyClasses?
    var subModuleId   : Int?
    var AttType       : AttendanceType?
    var teacherID     : String?
    var strClassID    : String?
    var strSubID      : String?
    var viewType      : Int?
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewType = 2
        
        self.isNavBarHidden = true
        
        imgViewNoWeeklyPlan?.isHidden = true
        lblNoWeeklyPlan?.isHidden     = true
    }
    
//
//    override  func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.isNavBarHidden = false
//    }
    
    override func btnBackAct(_ sender: UIButton) {
          self.navigationController == nil ? dismissVC(completion: nil) : popVC()
          self.isNavBarHidden = false
    }
    
    
    @IBAction func ClkBtnAllAction(_ sender : UIButton){
        print("tag= \(sender.tag)")
        
        switch sender.tag {
        case 1:
            if classArr == nil || classArr?.count == 0 {
                getClassList()
            }
            else {
                openSelectClassVc(tag : sender.tag)
            }
        case 2:
            guard let classs = self.selectClass else {
                Messages.shared.show(alert: .oops, message: AlertMsg.SelectClass.get, type: .warning)
                return
            }
            if lblSelectClass?.text != IOSMessages.SelectClass.get {
                
                if subjectArr?.count == 0 || subjectArr == nil {
                    getSubjectList(classID: /classs.classID)
                }
                else {
                    openSelectClassVc(tag : sender.tag)
                }
            }
        default:
            break
        }
    }
    func getWeeklyPlans() {
        
        if teacherID == nil {
            self.teacherID = self.userID
        }
        
        self.strClassID = self.selectClass?.classID?.toString
        self.strSubID = self.selectSubject?.SubID?.toString
        
        APIManager.shared.request(with: ECareEndPoint.getWeeklyPlans(viewType: /viewType, TeacherID: self.teacherID, ClassID: self.strClassID, SubID: self.strSubID, pg: 1 , UserID: self.userID), isLoader: true) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response : success)
            })
        }
    }
    
    @IBAction func ClkBtnFilter(_ sender : UIButton){
        self.popVC()
    }
}


extension ClasswiseWeeklyPlanViewController : SearchByClassDelegate , SelectSubjectDelegate {
    //MARK: - getSelectClass
    func getSelectClass(selectClass: MyClasses?, classArr: [MyClasses]?) {
        if let classs = selectClass {
            self.selectSubject          = nil
            self.lblSubject?.text = IOSMessages.SelectSubject.get
            lblSelectClass?.text        = classs.ClassName
            self.selectClass            = classs
            self.subjectArr             = []
            self.getSubjectList(classID: selectClass?.classID)
        }
    }
    //MARK: - getSelectSubject
    func getSelectSubject(selectSubject : Subjects?, subjectArr : [Subjects]?) {
        if let subject = selectSubject {
            lblSubject?.text = subject.ShortName
            self.selectSubject     = subject
            
            getWeeklyPlans()
        }
    }
}
//MARK::- API ZONE
extension ClasswiseWeeklyPlanViewController {
    
    //MARK: - getClassList
    func getClassList() {
        APIManager.shared.request(with: HomeEndpoint.getClassListForAttendance(UserID: /self.userID , isSubject: 1), isLoader: true) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    //MARK: - getSubjectList
    func getSubjectList(classID : Int?) {
        guard let user = self.getCurrentUser() , let classID = classID else {return}
        APIManager.shared.request(with: HomeEndpoint.SubjectClassWise(UserID: Int(user.userID).toString , classID: classID.toString), isLoader: true) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK: - handle
    func handle(response : Any?) {
        if let modal = response as? ContactsModel , let arr = modal.myClasses {
            self.classArr = arr
            if /self.classArr?.count != 0 {
                openSelectClassVc(tag : 1)
            }
        }
        if self.classArr?.count == 0 || self.classArr == nil {
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: AlertMsg.NoWeeklyPlanAssigned.get, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                switch tag {
                default:
                    return
                }
            }
        }
        //get subject List
        if let modal = response as? StudentListModal , let subjects = modal.subjects {
            self.subjectArr = subjects
            if /self.subjectArr?.count != 0 {
                openSelectClassVc(tag : 2)
            }
            else {
                Messages.shared.show(alert: .oops, message: AlertMsg.NoSubjectAssigned.get, type: .warning)
                planList = []
                imgViewNoWeeklyPlan?.isHidden = planList?.count != 0
//                lblNoWeeklyPlan?.isHidden     = planList?.count != 0
                reloadTable()
            }
        }
        
        if let modal = response as? PlanModal , let plansArr = modal.planList{
            planList = plansArr
            checkNoData(arr: planList)
            
            if intPaggination == 1{
                imgViewNoWeeklyPlan?.isHidden = planList?.count != 0
                lblNoWeeklyPlan?.isHidden     = planList?.count != 0
            }
            else {
                
            }
            reloadTable()
        }
    }
}

extension ClasswiseWeeklyPlanViewController {
    
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: planList, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.PlanListCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? PlanListCellTableViewCell)?.modal    = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ index : Int){
        if index < /planList?.count {
            navigateToFinalPlan(wpID : /self.planList?[index].WPID)
        }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = planList
            tableView?.reloadData()
        }
    }
    func navigateToFinalPlan(wpID : Int) {
        
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "FinalDesignPlanViewController") as? FinalDesignPlanViewController{
            vc.WPID     = wpID
            vc.userRole = .Cordinator
            vc.action   = .Review
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK: - openSelectClassVc
    func openSelectClassVc(tag : Int){
        let storyboard = Storyboard.Reports.getBoard()
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "SelectClassViewController") as? SelectClassViewController {
            selectClassVc.classArr        = classArr
            selectClassVc.delegate        = self
            selectClassVc.subDelegate     = self
            selectClassVc.selectedClass   = selectClass
            selectClassVc.tag             = tag
            selectClassVc.subjectArr      = self.subjectArr
            selectClassVc.selectedSubject = self.selectSubject
            self.navigationController?.pushViewController(selectClassVc, animated: false)
        }
    }
}
