//  DownloadWeeklyPlanViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 02/12/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class DownloadWeeklyPlanViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet var webView: UIWebView?
    @IBOutlet var viewHeader: UIView?
    @IBOutlet var lblTitle: UILabel?
    @IBOutlet var btnBack: UIButton?
    @IBOutlet var heightView: NSLayoutConstraint?
    
    
    //MARK ::- VARIABLES
    var strURLFile: String?
    var strWebsite : String?
    var module  : Module?
    var timer : Timer?
    
    
    //MARK::- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        webView?.scalesPageToFit = true
        webView?.contentMode = .scaleAspectFit
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isNavBarHidden      = true
        lblTitle?.text = module?.getTitle()
        
        viewHeader?.isHidden     = true
        heightView?.constant     = 0
        btnBack?.isHidden        = true
        webView?.delegate        = self
        webView?.backgroundColor = UIColor.clear
        webView?.scalesPageToFit = module == Module.MarksEntry
        
        if  module == Module.MarksEntry {
            if let baseUrl = self.getCurrentSchool()?.marksEntryURL , let user = self.getCurrentUser(), let username = user.username , let password = user.password{
                strWebsite =  baseUrl + "?usr=\(username)&psw=\(password)"
            }
        }
            
        else  if module == Module.BusLocator {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            navigationItem.hidesBackButton = true
            viewHeader?.isHidden     = false
            if  let schoolCode = self.getCurrentUser()?.schCode, let busNo = self.getCurrentUser()?.vehicleNumber  {
                strWebsite =  "\(APIConstants.imgbasePath)VehicleDTL/Vehicle?SchCode=\(schoolCode)&RegNumber=\(busNo)"
                strWebsite = strWebsite?.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            }
        }
            
        else {
            let parent =  self.navigationController?.parent as! CustomTabbarViewController
            parent.hideBar(boolHide: true)
            
            strWebsite = "\(APIConstants.imgbasePath)" +  "\(/strURLFile)"
            
        }
        self.webAPIWebsite()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = false
        timer?.invalidate()
    }
    
    //MARK:- webAPIWebsite
    func webAPIWebsite(){
        if let url = URL(string: /self.strWebsite){
            if module == Module.BusLocator {
                startTimer()
            }
            let request = URLRequest(url: url)
            self.webView?.loadRequest(request)
        }
        else{
             showAlert(title: "", Description: "URL is not correct")
        }
    }
    
    
    @IBAction func ClkBtnDownload(_ sender : UIButton){
    }
    
    func showAlert(title : String? , Description : String?){
        
    }
}
// MARK: - WEBVIEW DELEGATE METHODS
extension DownloadWeeklyPlanViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        Utility.shared.loader()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        Utility.shared.removeLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        Utility.shared.removeLoader()
        AlertsClass.shared.showNativeAlert(withTitle: "", message: "Can't Connect. Please check your internet Connection", fromVC:  self)
    }
}


extension DownloadWeeklyPlanViewController {
    
    func startTimer() {
        timer =  Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(self.reloadWebView), userInfo: nil, repeats: true)
    }
    
    
    @objc func reloadWebView(_ timer1: Timer) {
        if module == Module.BusLocator  {
            self.webView?.reload()
        }
    }
}
