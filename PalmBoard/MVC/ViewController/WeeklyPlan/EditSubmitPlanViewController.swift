//  WeekPlanListViewController.swift
//  PalmBoard
//  Created by Shubham on 22/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class EditSubmitPlanViewController: BaseViewController {
    
    //MARK::- OUtlets
    @IBOutlet weak var lblDayNo : UILabel?
    @IBOutlet weak var lblDate :  UILabel?
    @IBOutlet weak var lblPlanName : UILabel?
    @IBOutlet weak var btnSubmit : UIButton?
    
    //MARK::- Variables
    var isFirstTime     : Bool = true
    var weekPlanModal : WeekPlanModal?
    var currentDayIndex : Int = 0
    var userRole : Users?
    var dayNo : Int?
    var action : Action?
    
    
    var weekPlanDataSource : WeeklyPlanDataSource?{
        didSet{
            
            tableView?.dataSource = weekPlanDataSource
            tableView?.delegate = weekPlanDataSource
            tableView?.reloadData()
        }
    }
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        if let Title = weekPlanModal?.Title?.trimmed() {
            lblPlanName?.text = /Title
        }
        updateUI()
        reloadTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    
    func updateUI() {
        
        lblDayNo?.text = "Day \(/self.dayNo + 1)"
        zoomInZoomOutAnimation()
        if let date = self.weekPlanModal?.DayPlans[self.currentDayIndex].PlanDate {
            lblDate?.text =  "\(date.getTimeIn12HourFormat(IPFormat: "yyyy-MM-dd'T'HH:mm:ss", OPFormat: "dd MMM, yyyy")) (\(date.convertStringIntoDate().getDayOnParticularDate()))"
        }
    }
    
    func zoomInZoomOutAnimation() {
        
        UIView.animate(withDuration: 1.0, animations: {
            self.lblDayNo?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            self.lblDate?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            
        }) { (true) in
            UIView.animate(withDuration: 1.0, animations: {
                self.lblDayNo?.transform = CGAffineTransform(scaleX: 1.0 , y: 1.0)
                self.lblDate?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    
    @IBAction func btnSubmitAct(_ sender : UIButton){
        var  FinalModal = WeekPlanModal.init()
        
        if  weekPlanModal != nil , let modal = weekPlanModal {
            FinalModal = modal
            if let planDTL = modal.DayPlans[currentDayIndex].WorkPlans.first {
                FinalModal.editPlanDTL = planDTL
                //            FinalModal.DayPlans = []
                FinalModal.SchoolCode = self.getCurrentSchool()?.schCode
                FinalModal.key = APP_CONSTANTS.KEY
                FinalModal.UserID = self.userID?.toInt()
                FinalModal.editPlanDTL.Status = 0
            }
            
            if  FinalModal.editPlanDTL.PlnID != nil {
                APIManager.shared.request(with: ECareEndPoint.weeklyPlanModify(body: FinalModal.toJSON())) { (response) in
                    switch response {
                    case .success(_):
                        Messages.shared.show(alert: .success, message: "Plan Updated Successfully", type: .success)
                        ez.runThisAfterDelay(seconds: 1, after: {
                            self.popVC()
                        })
                    case .failure(_):
                        Messages.shared.show(alert: .oops, message: "Something went wrong", type: .warning)
                    }
                }
            }
        }
    }
    
}
extension EditSubmitPlanViewController {
    
    func configureTableView() {
        weekPlanDataSource  = WeeklyPlanDataSource(items: weekPlanModal?.DayPlans, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.EditDayPlanCell.get , currentDayIndex : currentDayIndex , action : action)
        
        weekPlanDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let index = indexpath?.section else {return}
            (cell as? EditDayPlanCell)?.topMostVC = self
            (cell as? EditDayPlanCell)?.workTypeIndex = index
            (cell as? EditDayPlanCell)?.modal = item
        }
    }
    
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            weekPlanDataSource?.currentDayIndex = self.currentDayIndex
            weekPlanDataSource?.items =  weekPlanModal?.DayPlans
            tableView?.reloadData()
        }
    }
}

