//  FinalDesignPlanViewController.swift
//  PalmBoard
//  Created by Shubham on 23/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class FinalDesignPlanViewController: BaseViewController {
    //MARK::- Variables
    
    var isFirstTime     : Bool = true
    var weekPlanModal   : WeekPlanModal?
    var dayWisePlanArr  : [DayPlanList]?
    var action          : Action?
    var WPID            : Int?
    var userRole         : Users?
    
    var strClassName: String?
    var strSubName: String?

    //Mark ::- Outlets
    @IBOutlet weak var btnSubmit : UIButton?
    @IBOutlet weak var lblPlanName : UILabel?
    
    
    var finalPlanDataSource : FinalWeekPlanDataSource?{
        didSet{
            
            tableView?.dataSource = finalPlanDataSource
            tableView?.delegate = finalPlanDataSource
            tableView?.reloadData()
        }
    }
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let Title = weekPlanModal?.Title {
            lblPlanName?.text = /Title
        }
        //here action (View and create) for teacher and review for cordinator
        
    }
    
    override func btnBackAct(_ sender: UIButton) {
        
        if action == .Create {
            guard let viewControllers = self.navigationController?.viewControllers else {
                return
            }
            for (_,value) in viewControllers.enumerated() {
                if value is NewPlanViewController {
                    self.navigationController?.popToViewController(value, animated: false)
                }
            }
        }
        else {
            self.navigationController == nil ? dismissVC(completion: nil) : popVC()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnSubmit?.isHidden = !(action == .Create)
        
        if (action == .View && WPID != nil) || (action == .Review){
            isFirstTime = true
            getReviewedPlan()
        }
        else {
            guard  let dayWisePlanArr = weekPlanModal?.DayPlans else {
                print("No recorded is maintain")
                return
            }
            self.dayWisePlanArr = dayWisePlanArr
            reloadTable()
        }
    }
    
    func getReviewedPlan(){
        
        APIManager.shared.request(with: ECareEndPoint.weeklyPlanDTL(WPID: /WPID , UserType : self.userType), isLoader: true) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handleSuccess(response : success)
            })
        }
    }
    
    func handleSuccess(response : Any?){
        if let modal = response as? PlanModal , let weekModal = modal.weekPlanModal{
            weekPlanModal = weekModal
            self.dayWisePlanArr = weekModal.DayPlans
            reloadTable()
            if let Title = weekPlanModal?.Title {
                lblPlanName?.text = /Title
            }
        }
    }
    
    @IBAction func btnSubmitAct(_ sender : UIButton){
        guard let weekPlanModal = weekPlanModal else {
            return
        }
        APIManager.shared.request(with: ECareEndPoint.createWeeklyPlan(body: weekPlanModal.toJSON())) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                if (success as? String) != nil {
                   // Messages.shared.show(alert: .success, message: "Plan Created Successfully", type: .success)
                    self.openConfirmVC()

                }
            })
        }
    }
    func openConfirmVC() {
        if let lcVC = storyboard?.instantiateViewController(withIdentifier: "PlanConfirmationViewController") as? PlanConfirmationViewController {
            lcVC.FromDate        = weekPlanModal?.FromDate
            lcVC.EndDate         = weekPlanModal?.EndDate
            lcVC.strClassName    = self.strClassName
            lcVC.strSubjectName  = self.strSubName
            lcVC.strPlan         = weekPlanModal?.Title
            lcVC.flag            = 1
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                parent.hideBar(boolHide: true)
            }
            self.navigationController?.pushViewController(lcVC, animated: false)
        }
    }
}
extension FinalDesignPlanViewController {
    
    func configureTableView() {
        finalPlanDataSource  = FinalWeekPlanDataSource(items: dayWisePlanArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.finalDesignCell.get, fromVC : self , action : action, userrole : userRole , userID : self.userID , userType : self.userType?.toInt())
        }
    
    
    func reloadTable() {
        if isFirstTime {
            
            configureTableView()
            isFirstTime = false
        }
        else {
            tableDataSource?.items = dayWisePlanArr
            tableView?.reloadData()
        }
    }
    
}
