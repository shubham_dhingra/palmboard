//  NewPlanViewController.swift
//  PalmBoard
//  Created by Shubham on 22/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
import EZSwiftExtensions

let maxLengthPlan = 20
let aSetPlan = NSCharacterSet.init(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").inverted

class NewPlanViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet weak var txtFieldPlanName:  UITextField?
    @IBOutlet weak var lblContinue:       UILabel?
    @IBOutlet weak var btnContinue:       UIButton?
    @IBOutlet weak var btnSrartDate:      UIButton?
    @IBOutlet weak var btnEndDate:        UIButton?
    @IBOutlet weak var btnSelectClass:    UIButton?
    @IBOutlet weak var btnSelectSub:      UIButton?
    @IBOutlet weak var lblClassSelect:    UILabel?
    @IBOutlet weak var lblSubjectSelect:  UILabel?
    @IBOutlet weak var classView:         UIView?
    @IBOutlet weak var subjectView:       UIView?
    @IBOutlet weak var lblStartDate:      UILabel?
    @IBOutlet weak var lblEndDate :       UILabel?
    
    var classArr      : [MyClasses]?
    var subjectArr    : [Subjects]?
    var selectSubject : Subjects?
    var selectClass   : MyClasses?
    var subModuleId   : Int?
    var AttType       : AttendanceType?
    var startDate     = Date()
    
    var currentDate     = Date()
    let formatter       = DateFormatter()
    var selectDate      : Date?
    var strClassName    : String?
    var strSubjectName  : String?
    var sessionStartDate: String?
    var sessionEndDate: String?
    var selectToDate     = Date()
    var selectFromDate   = Date()
    var intDayDifference : Int?
    var intBtnFromTo:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "yyyy-MM-dd"
        selectDate           = currentDate
        
        if let check = self.getCurrentUser()?.sessionStart  {
            sessionStartDate =  check
        }
        if let check =  self.getCurrentUser()?.sessionEnd  {
            sessionEndDate =  check
        }
        btnContinue?.isUserInteractionEnabled = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subjectView?.isHidden = AttType == .Class
    }
    // MARK: - UIDatePickerMethods
    func showDatePicker(tag : Int){
        
        //  let lastText = tag == 1 ? (lblStartDate?.text == "Start Date" ? "Start Date" : lblStartDate?.text) : (lblEndDate?.text == "End Date" ?  "End Date" : lblEndDate?.text)
        
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: selectDate ?? Date() , minimumDate: Date(), maximumDate: self.sessionEndDate?.dateChangeFormatWeeklyCalendar(), datePickerMode: .date , lastText:  "") { (date) in
            self.setDateIntoLabel(dateVal : date , lastText : "")
        }
    }
    // MARK: -Set the date into the label after getting response from the date picker
    func setDateIntoLabel(dateVal : Date? ,lastText : String?) {
        if let dateVal = dateVal {
            let dateString = dateVal.dateToString(formatType :"dd MMM, yyyy")//"yyyy-MM-dd"
            formatter.dateFormat = "yyyy-MM-dd"
            
            if intBtnFromTo == 1 {
                
                if lblEndDate?.text != "End Date" {
                    if  dateVal.compare(selectToDate) == .orderedAscending || dateVal.compare(selectToDate) == .orderedSame {
                        startDate = dateVal
                        lblStartDate?.text = dateString
                        selectFromDate = dateVal
                        
                        intDayDifference = getDayDateDifferance(dateA: selectFromDate, dateB: selectToDate)
                        // print("intDayDifference = \(intDayDifference)")
                    }
                    else {
                        WebserviceManager.showAlert(message: "", title: "'From Date' can't be greater than 'To date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                        }, secondBtnBlock: {})
                    }
                }
                else {
                    startDate = dateVal
                    lblStartDate?.text = dateString
                    selectFromDate = dateVal
                }
                
            }
                //Validatin for To date
            else {
                if lblStartDate?.text != "End Date" {
                    if dateVal.compare(selectFromDate) == .orderedDescending || dateVal.compare(selectFromDate) == .orderedSame {
                        lblEndDate?.text   = dateString
                        selectToDate = dateVal
                        intDayDifference = getDayDateDifferance(dateA: selectFromDate, dateB: selectToDate)
                        //  print("intDayDifference = \(intDayDifference)")
                    }
                    else{
                        WebserviceManager.showAlert(message: "", title: "'To Date' can't be before 'From Date'", firstBtnText: "Ok", secondBtnText: nil, firstBtnBlock: {
                        }, secondBtnBlock: {})
                    }
                }
                else{
                    lblEndDate?.text   = dateString
                    selectToDate = dateVal
                }
            }
        }
        else {
            if intBtnFromTo == 1 {
                let strLastText    = /lastText?.isEmpty ? "Start Date" : lastText
                lblStartDate?.text = (strLastText == lblStartDate?.text ) ? "Start Date" : lblStartDate?.text
            }
            else {
                let strLastText  = /lastText?.isEmpty ? "End Date" : lastText
                lblEndDate?.text = (strLastText == lblEndDate?.text ) ?  "End Date" : lblEndDate?.text
            }
        }
        checkBlankInput()
    }
    //Mark: - Date difference
    func getDayDateDifferance(dateA: Date, dateB: Date) -> Int
    {
        let diffInDays = Calendar.current.dateComponents([.day], from: dateA, to: dateB).day
        return /diffInDays
    }
    //MARK: - ClkBtnSrartDate
    @IBAction func ClkBtnSrartDate(_ sender : UIButton){
        intBtnFromTo = 1
        showDatePicker(tag: sender.tag)
    }
    //MARK: - openSelectClassVc
    func openSelectClassVc(tag : Int){
        let storyboard = Storyboard.Reports.getBoard()
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "SelectClassViewController") as? SelectClassViewController {
            selectClassVc.classArr        = classArr
            selectClassVc.delegate        = self
            selectClassVc.subDelegate     = self
            selectClassVc.selectedClass   = selectClass
            selectClassVc.tag             = tag
            selectClassVc.subjectArr      = self.subjectArr
            selectClassVc.selectedSubject = self.selectSubject
            self.navigationController?.pushViewController(selectClassVc, animated: false)
        }
    }
    //MARK: - ClkBtnEndDate
    @IBAction func ClkBtnEndDate(_ sender : UIButton){
        intBtnFromTo        = 2
        showDatePicker(tag: sender.tag)
    }
    //MARK: - ClkBtnSelectClass
    @IBAction func ClkBtnSelectClass(_ sender : UIButton){
        txtFieldPlanName?.resignFirstResponder()
        if classArr == nil || classArr?.count == 0 {
            getClassList()
        }
        else {
            openSelectClassVc(tag : sender.tag)
        }
    }
    //MARK: - ClkBtnSelectSub
    @IBAction func ClkBtnSelectSub(_ sender : UIButton){
        txtFieldPlanName?.resignFirstResponder()
        
        guard let classs = self.selectClass else {
            Messages.shared.show(alert: .oops, message: AlertMsg.SelectClass.get, type: .warning)
            return
        }
        if lblClassSelect?.text != IOSMessages.SelectClass.get {
            
            if subjectArr?.count == 0 || subjectArr == nil {
                getSubjectList(classID: /classs.classID)
            }
            else {
                openSelectClassVc(tag : sender.tag)
            }
        }
    }
    //MARK: - ClkBtnContinue
    @IBAction func ClkBtnContinue(_ sender : UIButton){
        if btnContinue?.alpha == 1.0 {
            openConfirmVC()
        }
    }
    func openConfirmVC() {
        if let lcVC = storyboard?.instantiateViewController(withIdentifier: "PlanConfirmationViewController") as? PlanConfirmationViewController {
            lcVC.ClassID         = self.selectClass?.classID?.toString
            lcVC.SubId           = self.selectSubject?.SubID?.toString
            lcVC.UserId          = self.userID
            lcVC.FromDate        = lblStartDate?.text
            lcVC.EndDate         = lblEndDate?.text
            lcVC.strClassName    = self.lblClassSelect?.text
            lcVC.strSubjectName  = self.lblSubjectSelect?.text
            lcVC.strPlan         = /txtFieldPlanName?.text?.trimmed()
            lcVC.startDate       = self.startDate
            lcVC.delegatePlan    =  self
            lcVC.flag            =  0
            
            if let parent =  self.navigationController?.parent as? CustomTabbarViewController {
                parent.hideBar(boolHide: true)
            }
            self.presentVC(lcVC)
        }
    }
    
    func callCreatePlanAPI(){
        if btnContinue?.alpha == 1.0 {
            APIManager.shared.request(with: ECareEndPoint.isWeeklyPlanCreated(ClassID: self.selectClass?.classID?.toString, SubId: self.selectSubject?.SubID?.toString, UserId: self.userID, FromDate: selectFromDate.dateToString(formatType :"yyyy-MM-dd"), EndDate: selectToDate.dateToString(formatType :"yyyy-MM-dd")), isLoader: true, completion: { [weak self](response) in
                self?.handleResponse(response: response, responseBack: { (success) in
                    self?.handlePlanCreated(response: success)
                })
            })
        }
    }
    
    func handlePlanCreated(response : Any?) {
        if let modal = response as? PlanModal  {
            if !(/modal.hasCreated) {
                let weekPlanModal = WeekPlanModal.init(schoolCode_ : /self.getCurrentSchool()?.schCode , planForDays: /intDayDifference + 1, startDate: startDate , classID: /self.selectClass?.classID?.toString, subID: /self.selectSubject?.SubID?.toString, planName_ : /txtFieldPlanName?.text?.trimmed(), weekID: (/modal.WPID?.toString))
                weekPlanModal.FromDate = lblStartDate?.text
                weekPlanModal.EndDate = lblEndDate?.text
                weekPlanModal.UserID = self.userID?.toInt()
                
                let storyboard = Storyboard.Module.getBoard()
                if let vc      = storyboard.instantiateViewController(withIdentifier: "WeekPlanListViewController") as? WeekPlanListViewController {
                    vc.weekPlanModal = weekPlanModal
                    vc.action = .Create
                    vc.userRole = .Staff
                    vc.strClassName = self.selectClass?.ClassName
                    vc.strSubjectName   = self.lblSubjectSelect?.text
                    
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else {
                Messages.shared.show(alert: .oops, message: "Already Plan", type: .warning)
            }
        }
    }
    
    func checkBlankInput() {
        
        if  (lblClassSelect?.text != "Select Class") && (lblSubjectSelect?.text != "Select Subject") && (lblStartDate?.text != "Start Date" ) &&  (lblEndDate?.text != "End Date" ) && (txtFieldPlanName?.text?.trimmed().count != 0) {
            btnContinue?.alpha  =  1
            lblContinue?.alpha  =  1
            btnContinue?.isUserInteractionEnabled = true
            return
            //return true
        }
        btnContinue?.alpha  =  0.5
        lblContinue?.alpha  =  0.5
        btnContinue?.isUserInteractionEnabled = false
        // return false
    }
    
    //Mark: - TextField Delegate methid
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        
        if (textField.text?.count)! > 0 {
            checkBlankInput()
        }
        return true
    }
    
}
extension NewPlanViewController : SearchByClassDelegate , SelectSubjectDelegate, PlanConfirmDelegate {
    
    //Marks: senderCall
    func senderCallerCreatwPlan() {
        print("senderCall")
        callCreatePlanAPI()
    }
    
    //MARK: - getSelectClass
    func getSelectClass(selectClass: MyClasses?, classArr: [MyClasses]?) {
        if let classs = selectClass {
            self.selectSubject          = nil
            self.lblSubjectSelect?.text = IOSMessages.SelectSubject.get
            lblClassSelect?.text        = classs.ClassName
            self.selectClass            = classs
            self.subjectArr             = []
            self.getSubjectList(classID: selectClass?.classID)
        }
        checkBlankInput()
    }
    
    //MARK: - getSelectSubject
    func getSelectSubject(selectSubject : Subjects?, subjectArr : [Subjects]?) {
        if let subject = selectSubject {
            lblSubjectSelect?.text = subject.ShortName
            self.selectSubject     = subject
        }
        checkBlankInput()
    }
    
}
//MARK::- API ZONE
extension NewPlanViewController {
    
    //MARK: - getClassList
    func getClassList() {
        APIManager.shared.request(with: HomeEndpoint.getClassListForAttendance(UserID: /self.userID , isSubject: 1), isLoader: true) {[weak self] (response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response : success)
            })
        }
    }
    //MARK: - getSubjectList
    func getSubjectList(classID : Int?) {
        guard let user = self.getCurrentUser() , let classID = classID else {return}
        APIManager.shared.request(with: HomeEndpoint.SubjectClassWise(UserID: Int(user.userID).toString, classID: classID.toString), isLoader: true) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK: - handle
    func handle(response : Any?) {
        if let modal = response as? ContactsModel , let arr = modal.myClasses {
            self.classArr = arr
            if /self.classArr?.count != 0 {
                openSelectClassVc(tag : 1)
            }
        }
        if self.classArr?.count == 0 || self.classArr == nil {
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: AlertMsg.NoClassAssigned.get, buttonTitles: [AlertConstants.Ok.get]) { (tag) in
                switch tag {
                default:
                    return
                }
            }
        }
        //get subject List
        if let modal = response as? StudentListModal , let subjects = modal.subjects {
            self.subjectArr = subjects
            if /self.subjectArr?.count != 0 {
                openSelectClassVc(tag : 2)
            }
        }
    }
}

extension NewPlanViewController  {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            return true
    }
}
