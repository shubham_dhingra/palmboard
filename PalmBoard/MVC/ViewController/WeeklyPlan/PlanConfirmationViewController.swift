//  PlanConfirmationViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 02/12/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

protocol PlanConfirmDelegate {
    func senderCallerCreatwPlan()
}
class PlanConfirmationViewController : BaseViewController {

    var delegatePlan : PlanConfirmDelegate?

    @IBOutlet weak var lblConfirmation:UILabel?
    @IBOutlet weak var lblTitle:       UILabel?
    @IBOutlet weak var btnCancel:      UIButton?
    @IBOutlet weak var btnOK:          UIButton?
    @IBOutlet weak var btnOKAY:        UIButton?

    var strClassName: String?
    var strSubjectName: String?
    
    
    var ClassID: String?
    var SubId: String?
    var UserId: String?
    var FromDate: String?
    var EndDate: String?
    var strPlan: String?
    var startDate     = Date()
    var flag     : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch flag {
        case 0:
            
            lblTitle?.text = AlertConstants.Confirmation.get
            btnCancel?.isHidden = false
            btnOK?.isHidden     = false
            btnOKAY?.isHidden   = true
            
            btnCancel?.isUserInteractionEnabled = true
            btnOK?.isUserInteractionEnabled     = true
            btnOKAY?.isUserInteractionEnabled   = false

            
            let strConcatination =  "\(/strClassName), Subject \(/strSubjectName) \(/FromDate)"
            let attributedString1 = NSMutableAttributedString(string:"You are creating weekly plan for ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0)!])
            
            let attributedString2 = NSMutableAttributedString(string:/strConcatination, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)!])
            
            let attributedString3 = NSMutableAttributedString(string:" to ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0)!])
            
            let attributedString4 = NSMutableAttributedString(string:/EndDate, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)!])
            
            attributedString1.append(attributedString2)
            attributedString1.append(attributedString3)
            attributedString1.append(attributedString4)
            
            lblConfirmation?.attributedText = attributedString1
            
        case 1:
            lblTitle?.text = "Thank you"
            
            btnCancel?.isHidden = true
            btnOK?.isHidden     = true
            btnOKAY?.isHidden   = false
            
            btnCancel?.isUserInteractionEnabled = false
            btnOK?.isUserInteractionEnabled     = false
            btnOKAY?.isUserInteractionEnabled   = true
            
            let strConcatination =  " \(/strClassName), Subject \(/strSubjectName) \(/FromDate)"
            let attributedString1 = NSMutableAttributedString(string:"You have successfully submitted ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0)!])
            

            let attributedString2 = NSMutableAttributedString(string:/strPlan, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)!])

            let attributedString3 = NSMutableAttributedString(string:" for ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0)!])

            let attributedString4 = NSMutableAttributedString(string:/strConcatination, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)!])
            
            let attributedString5 = NSMutableAttributedString(string:" to ", attributes: [NSAttributedString.Key.font : R.font.ubuntuLight(size: 16.0)!])
            
            let attributedString6 = NSMutableAttributedString(string:/EndDate, attributes: [NSAttributedString.Key.font : R.font.ubuntuMedium(size: 16.0)!])
            
            attributedString1.append(attributedString2)
            attributedString1.append(attributedString3)
            attributedString1.append(attributedString4)
            attributedString1.append(attributedString5)
            attributedString1.append(attributedString6)

            lblConfirmation?.attributedText = attributedString1
            
        default:
            break
        }
    }
    @IBAction func ClkBtn(_ sender : UIButton){
      
        if sender.tag == 3{
           
            guard let viewControllers  =  (ez.topMostVC?.children.last as? UINavigationController)?.viewControllers
            else {
                return
            }
            for (_,vc) in viewControllers.enumerated() {
                if vc is SelectAttendanceTypeViewController {
                    self.navigationController?.popToViewController(vc, animated: false)
                    break
                }
            }
        }
        else {
            delegatePlan?.senderCallerCreatwPlan()
           if  self.navigationController == nil {
                dismissVC(completion: nil)
            }
            else{
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
}
