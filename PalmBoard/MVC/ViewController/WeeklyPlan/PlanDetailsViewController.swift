//  PlanDetailsViewController.swift
//  PalmBoard
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class PlanDetailsViewController: BaseViewController {
    
    @IBOutlet weak var lblTopic:       UILabel?
    @IBOutlet weak var lblDate:        UILabel?
    @IBOutlet weak var lblMethod:      UILabel?
    @IBOutlet weak var lblDescription: UILabel?
    
    var isFirstTime  : Bool = true
    var planArr      :  [String] =  ["1"]
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        //reloadTable()
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
}


extension PlanDetailsViewController {
    
    //    func configureTableView() {
    //        tableDataSource  = TableViewDataSource(items: planArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.PlanListCell.get)
    //
    //        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
    //            (cell as? PlanListCellTableViewCell)?.modal    = item
    //        }
    //    }
    //    func didSelect(_ index : Int){
    //        print("didSelect")
    //    }
    //    func reloadTable() {
    //        if isFirstTime {
    //            configureTableView()
    //            isFirstTime = !isFirstTime
    //        }
    //        else {
    //            tableDataSource?.items = planArr
    //            tableView?.reloadData()
    //        }
    //    }
}
