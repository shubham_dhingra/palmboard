//
//  PlanListViewController.swift
//  PalmBoard
//
//  Created by Shubham on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class PlanListViewController : BaseViewController {
    
    
    //MARK::- Variables
    var isFirstTime     : Bool = true
    var planList        : [WeeklyPlanList]?
    var viewType        : Int?
    var teacherID        : String?
    var userRole            : Users?
    var action              : Action?
    
    @IBOutlet weak var btnTeacherWise : UIButton?
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getWeeklyPlans()
        isNavBarHidden = true
        btnTeacherWise?.isHidden = true
    }
    
    override func btnBackAct(_ sender: UIButton) {
        isNavBarHidden = false
       self.navigationController == nil ? dismissVC(completion: nil) : popVC()
    }
    
     func getWeeklyPlans() {
        
        if teacherID == nil {
            self.teacherID = self.userID
        }
        
        APIManager.shared.request(with: ECareEndPoint.getWeeklyPlans(viewType: /viewType, TeacherID: self.teacherID, ClassID: nil, SubID: nil, pg: 1, UserID: self.userID), isLoader: isFirstTime) { (response) in
            self.handleResponse(response: response, responseBack: { (success) in
                self.handle(response : success)
            })
        }
    }
    
    func handle(response : Any?){
        if let modal = response as? PlanModal , let plansArr = modal.planList{
                planList = plansArr
                checkNoData(arr: planList)
                reloadTable()
        }
    }
    
}
extension PlanListViewController {
    
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: planList, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.PlanListCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? PlanListCellTableViewCell)?.modal    = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    
    func didSelect(_ index : Int){
        if index < /planList?.count {
            navigateToFinalPlan(wpID : /self.planList?[index].WPID)
        }
    }
    
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = planList
            tableView?.reloadData()
        }
    }
    
    func navigateToFinalPlan(wpID : Int) {
        
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "FinalDesignPlanViewController") as? FinalDesignPlanViewController{
            vc.WPID = wpID
            vc.userRole = userRole
            vc.action = action
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
