//  SelectSubjectViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 30/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.
import UIKit
protocol SelectSubjectWeeklyDelegate {
    func getSelectSubject(selectSubject : Subjects? , subjectArr : [Subjects]?)
}
protocol SelectHalfDayDelegate {
    func passHalfDays(halfDayArr : [HalfDayModal]?)
}
protocol SelectStaffTypeDelegate {
    func getStaffTypeDelegate(selectStaffType : StaffType? , staffTypeArr : [StaffType]?)
}
class SelectSubjectViewController: BaseViewController {
    
    @IBOutlet weak var tableViewHeight : NSLayoutConstraint?
    @IBOutlet weak var lblHeading      : UILabel?
    @IBOutlet weak var viewBtns        : UIView?
    @IBOutlet weak var btnSelectAll    : UIButton?
    @IBOutlet weak var btnDeselectAll  : UIButton?
    
    //MARK::- VARIABLES
    var isFirstTime     : Bool = true
    var subDelegate     : SelectSubjectWeeklyDelegate?
    var leaveDelegate   : SelectHalfDayDelegate?
    var staffDelegate   : SelectStaffTypeDelegate?
    
    var selectedSubject : Subjects?
    var selectedStaffType : StaffType?
    var subjectArr      : [Subjects]?
    var halfDayArr      : [HalfDayModal]?
    var arrStaffType    : [StaffType]?
    
    var tag             : Int?
    var fromLeave       : Bool? = false
    var fromAttendance  : Bool? = false
    
    //MARK::- LIFE CYCLES
    override func viewDidLoad() {
        super.viewDidLoad()
        configureClassTable()
        adjustTableViewHeight()
        viewBtns?.isHidden = /fromLeave
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func adjustTableViewHeight() {
        
        var itemsCount : Int?
        guard let subjectCount = self.subjectArr?.count else {return}
        itemsCount = subjectCount
        
        if /itemsCount != 0 && /itemsCount <= 5 {
            tableViewHeight?.constant = CGFloat(Double(/itemsCount) * 45.0)
            tableView?.isScrollEnabled = false
        }
        else {
            tableViewHeight?.constant = 225.0
            tableView?.isScrollEnabled = true
        }
        tableView?.reloadData()
        if /fromLeave {
            lblHeading?.text = IOSMessages.SelectHalfDay.get//SelectAttendance
        }
        else if /fromAttendance{
            lblHeading?.text = IOSMessages.SelectAttendance.get
        }
        else {
            lblHeading?.text = tag == 2 ? IOSMessages.SelectSubject.get : IOSMessages.SelectClass.get
        }
    }
    //BUTTON btnSubSelection
    @IBAction func btnSubSelection(_ sender: UIButton){
        if !(/fromLeave) {
            didSelect(-1 ,/sender.tag)
        }
    }
    
    //BUTTON ACTION SUBMIT
    @IBAction func btnOkAct(_ sender: UIButton){
        //for subjects
        if /fromLeave {
            leaveDelegate?.passHalfDays(halfDayArr: self.halfDayArr)
        }
        else if /fromAttendance {
            staffDelegate?.getStaffTypeDelegate(selectStaffType: selectedStaffType, staffTypeArr: arrStaffType)
        }
        else {
            subDelegate?.getSelectSubject(selectSubject: selectedSubject, subjectArr: subjectArr)
        }
        self.dismissVC(completion: nil)
    }
}

extension SelectSubjectViewController {
    func configureClassTable() {
        // (/fromLeave ? (halfDayArr) : (/fromAttendance ? arrStaffType : subjectArr))
        //MARK: - ConfigureTableView
        var arr  : [Any]?
        if /fromLeave {
            arr = halfDayArr
        }
        else {
            arr = /fromAttendance ? arrStaffType : subjectArr
        }
        
        tableDataSource = TableViewDataSource(items: arr , height: UITableView.automaticDimension,tableView: tableView, cellIdentifier: /fromLeave ? CellIdentifiers.SelectHalfDayCell.get : CellIdentifiers.DropDownSubjectCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            if /self.fromLeave {
                (cell as? SelectHalfDayCell)?.index    = /indexpath?.row
                (cell as? SelectHalfDayCell)?.modal    = item
                (cell as? SelectHalfDayCell)?.delegate = self
            }
            else {
                (cell as? DropDownSubjectCell)?.model = item
            }
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            if !(/self.fromLeave) {
                self.didSelect(indexpath.row , -1)
            }
        }
    }
    func didSelect(_ selectedIndex : Int , _ tag : Int){
        
        if tag == 1 || tag == 2 {
            
            if /self.fromAttendance
            {
                for  (index,_) in (arrStaffType ?? []).enumerated() {
                    arrStaffType?[index].isSelected = tag == 1 ? true : false
                }
            }
            else  if /self.fromLeave
            {
                for  (index,_) in (subjectArr ?? []).enumerated() {
                    subjectArr?[index].isSelected = tag == 1 ? true : false
                }
            }
        }
        else {
            if /self.fromAttendance
            {
                arrStaffType?[selectedIndex].isSelected = !(/arrStaffType?[selectedIndex].isSelected)
                tableDataSource?.items = arrStaffType
                
            }
            else  if /self.fromLeave {
                subjectArr?[selectedIndex].isSelected = !(/subjectArr?[selectedIndex].isSelected)
                tableDataSource?.items = subjectArr
            }
        }
        tableView?.reloadData()
    }
}
extension SelectSubjectViewController : SelectHalfTypeDelegate {
    func selectHalfTypeIndex(index : Int , halfType : Int){
        
        self.halfDayArr?[index].halfType = halfType
        self.tableDataSource?.items      = self.halfDayArr
        self.tableView?.reloadData()
    }
}

