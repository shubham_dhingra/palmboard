//  WeekPlanListViewController.swift
//  PalmBoard
//  Created by Shubham on 22/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit

class WeekPlanListViewController: BaseViewController {
    
    
    //MARK::- OUtlets
    @IBOutlet weak var btnPrevious  : UIButton?
    @IBOutlet weak var btnNext      : UIButton?
    @IBOutlet weak var lblDayNo     : UILabel?
    @IBOutlet weak var lblDate      :  UILabel?
    @IBOutlet weak var btnSkip      : UIButton?
    @IBOutlet weak var lblPlanName  : UILabel?
    @IBOutlet weak var btnSubmit    : UIButton?
    
    //MARK::- Variables
    var isFirstTime     : Bool = true
    var weekPlanModal : WeekPlanModal?
    var currentDayIndex : Int = 0
    var userRole : Users?
    var action : Action?
    var strClassName: String?
    var strSubjectName: String?
    
    
    var weekPlanDataSource : WeeklyPlanDataSource?{
        didSet{
            
            tableView?.dataSource = weekPlanDataSource
            tableView?.delegate = weekPlanDataSource
            tableView?.reloadData()
        }
    }
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        if let Title = weekPlanModal?.Title?.trimmed() {
            lblPlanName?.text = /Title
        }
        updateUI()
        reloadTable()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: true)
    }
    
    func updateUI() {
        btnPrevious?.isUserInteractionEnabled = currentDayIndex != 0
        btnPrevious?.alpha = currentDayIndex == 0 ? 0.5 : 1.0
        lblDayNo?.text = "Day \(/self.currentDayIndex + 1)"
        zoomInZoomOutAnimation()
        if let date = self.weekPlanModal?.DayPlans[self.currentDayIndex].PlanDateForModal {
            lblDate?.text =  "\(/date.dateToString(formatType: "dd MMM yyyy")) (\(date.getDayOnParticularDate()))"
        }
        btnNext?.setTitle(currentDayIndex == /self.weekPlanModal?.DayPlans.count - 1 ?  AlertConstants.Submit.get : AlertConstants.Next.get , for: .normal)
        
        
        btnPrevious?.isHidden = !(action == .Create)
        btnNext?.isHidden = !(action == .Create)
        btnSkip?.isHidden = !(action == .Create)
        btnSubmit?.isHidden = !(action == .Edit)
    }
    
    func zoomInZoomOutAnimation() {
        
        UIView.animate(withDuration: 1.0, animations: {
            self.lblDayNo?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            self.lblDate?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            
        }) { (true) in
            UIView.animate(withDuration: 1.0, animations: {
                self.lblDayNo?.transform = CGAffineTransform(scaleX: 1.0 , y: 1.0)
                self.lblDate?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    
    @IBAction func btnPreviousAct(_ sender : UIButton){
        handlePreviousNextAction(tag : sender.tag)
    }
    
    @IBAction func btnNextAct(_ sender : UIButton){
        handlePreviousNextAction(tag : sender.tag)
    }
    
    @IBAction func btnSkipAct(_ sender : UIButton){
        handlePreviousNextAction(tag : sender.tag)
    }
    
    
    @IBAction func btnSubmitAct(_ sender : UIButton){
        self.view.endEditing(true)
        navigateToFinalPlan()
    }
    
    func handlePreviousNextAction(tag : Int){
        self.view.endEditing(true)
        //Previous Action
        if tag == 0 {
            self.weekPlanModal?.DayPlans[self.currentDayIndex].isSkip = false
            if self.currentDayIndex != 0 {
                navigateToPreviousDay()
            }
        }
            
            //Next Action
        else if tag == 1{
            self.weekPlanModal?.DayPlans[self.currentDayIndex].isSkip = false
            navigateToNextDay(action : .Next)
        }
            
            //Skip Action
        else {
            let msg = "\(AlertMsg.skipAlertWeekPlan1.get)\(/lblDayNo?.text)\(AlertMsg.skipAlertWeekPlan2.get)"
            AlertsClass.shared.showAlertController(withTitle: AlertConstants.Attention.get, message: msg, buttonTitles: [AlertConstants.Cancel.get , AlertConstants.Skip.get]) { (tag) in
                switch tag {
                case .yes:
                    self.weekPlanModal?.DayPlans[self.currentDayIndex].isSkip = false
                    return
                default:
                    self.weekPlanModal?.DayPlans[self.currentDayIndex].isSkip = true
                    self.navigateToNextDay(action : .Skip)
                }
            }
        }
    }
    
    func navigateToNextDay(action : Action?) {
        if self.currentDayIndex != /self.weekPlanModal?.DayPlans.count - 1{
            self.currentDayIndex = self.currentDayIndex + 1
            updateUI()
            reloadTable(dayChange : true , action : action)
        }
        else {
            navigateToFinalPlan()
        }
    }
    
    func navigateToPreviousDay() {
        if self.currentDayIndex != 0 {
            self.currentDayIndex = self.currentDayIndex  - 1
            updateUI()
            reloadTable(dayChange : true , action : .Previous)
        }
    }
    
    
    func navigateToFinalPlan() {
        
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "FinalDesignPlanViewController") as? FinalDesignPlanViewController{
            vc.weekPlanModal = self.weekPlanModal
            vc.action = .Create
            vc.userRole = userRole
            vc.strClassName    = self.strClassName
            vc.strSubName  = self.strSubjectName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
extension WeekPlanListViewController {
    
    func configureTableView() {
        weekPlanDataSource  = WeeklyPlanDataSource(items: weekPlanModal?.DayPlans, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.DayPlanCell.get , currentDayIndex : currentDayIndex , action : action)
        
        weekPlanDataSource?.configureCellBlock = {(cell, item, indexpath) in
            guard let index = indexpath?.section else {return}
            (cell as? DayPlanCell)?.topMostVC = self
            (cell as? DayPlanCell)?.workTypeIndex = index
            (cell as? DayPlanCell)?.modal = item
        }
    }
    
    
    func reloadTable(dayChange : Bool? = false , action : Action? = .None) {
        print("*** Current day Index ******\(self.currentDayIndex)")
        if isFirstTime {
            configureTableView()
            isFirstTime = false
        }
        else {
            if /dayChange {
                weekPlanDataSource?.sectionArr = []
            }
            
            weekPlanDataSource?.currentDayIndex = self.currentDayIndex
            weekPlanDataSource?.items =  weekPlanModal?.DayPlans
            guard let action = action else {
                return
            }
            
            switch action {
            case .None:
                tableView?.reloadData()
            case .Skip , .Next:
                if let tableView = tableView {
                    UIView.transition(with: tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {tableView.reloadData()}, completion: nil)
                }
            case .Previous:
                if let tableView = tableView {
                    UIView.transition(with: tableView, duration: 0.3, options: .transitionCrossDissolve, animations: {tableView.reloadData()}, completion: nil)
                }
            default:
                break
            }
        }
    }
}


