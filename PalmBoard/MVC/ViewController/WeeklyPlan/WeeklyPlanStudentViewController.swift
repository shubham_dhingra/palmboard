//  WeeklyPlanStudentViewController.swift
//  PalmBoard
//  Created by Ravikant Bhardwaj on 24/11/18.
//  Copyright © 2018 Franciscan. All rights reserved.

import UIKit
import EZSwiftExtensions

class WeeklyPlanStudentViewController : BaseViewController {
    
    @IBOutlet weak var btnRightFilter:UIButton?
    @IBOutlet weak var lblSelectDate: UILabel?
    @IBOutlet weak var lblSubject:    UILabel?
    @IBOutlet      var heightSVConstraint: NSLayoutConstraint?
    @IBOutlet weak var viewDate:    UIView?
    @IBOutlet weak var viewSubject: UIView?
    @IBOutlet weak var imgViewNoWeeklyPlan: UIImageView?
    @IBOutlet weak var lblNoWeeklyPlan:     UILabel?
    
    var selectClass   : MyClasses?
    var subjectArr    : [Subjects]?
    var selectSubject : Subjects?
    var sessionEndDate : String?
    var fromWhere : Bool = false
    
    //MARK::- Variables
    var isFirstTime : Bool = true
    var isHeight    : Bool = true
    var weeklyPlanArr   : [WeeklyPlanListSchool]?
    var intPaggination: Int = 1
    
    var strSelectDate: String?
    var strSubID: String?
    
    var userRole            : Users?
    var action              : Action?
    
    
    //MARK::- Life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDate?.isHidden    = isHeight ? true : false
        viewSubject?.isHidden = isHeight ? true : false
        heightSVConstraint?.constant = isHeight ? 0 : 28
        
        if let check =  self.getCurrentUser()?.sessionEnd  {
            sessionEndDate =  check
        }
        weeklyPlanForStudentAPI(strPg: "\(intPaggination)", strPlanDate:strSelectDate, strSubIDs: strSubID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fromWhere = false
        self.isNavBarHidden = true
        
        imgViewNoWeeklyPlan?.isHidden = true
        lblNoWeeklyPlan?.isHidden     = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNavBarHidden = fromWhere
    }
    
    
    @IBAction func ClkBtnAllAction(_ sender : UIButton){
        print("tag= \(sender.tag)")
        
        switch sender.tag {
        case 1:
            showDatePicker()
        case 2:
            chooseSubject()
        default:
            break
        }
    }
    //MARK: - chooseSubject
    func chooseSubject(){
        var intClassId = Int()
        if let classID = self.getCurrentUser()?.classID {
            intClassId =  Int(classID)
        }
        if subjectArr?.count == 0 || subjectArr == nil {
            getSubjectList(classID: intClassId)
        }
        else {
            openSelectSubjectVc(tag : 2)
        }
    }
    @IBAction func ClkBtnFilter(_ sender : UIButton){
        if isHeight {
            isHeight = !isHeight
            lblSelectDate?.text = "Select Date"
            lblSubject?.text    = "Select Subject"
        }
        else {
            isHeight = !isHeight
            weeklyPlanForStudentAPI(strPg: "1", strPlanDate:"", strSubIDs: strSubID)
        }
        heightSVConstraint?.constant = isHeight ? 0 : 28
        viewDate?.isHidden           = isHeight ? true : false
        viewSubject?.isHidden        = isHeight ? true : false
    }
    //MARK: - WeeklyPlanForStudentAPI
    func weeklyPlanForStudentAPI(strPg: String?, strPlanDate: String?,strSubIDs:String?){
        
        APIManager.shared.request(with: ECareEndPoint.weeklyPlanForStudent(UserID: self.userID, SubIDs: strSubIDs, PlanDate: strPlanDate, pg: strPg), isLoader: true) { [weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handlePlan(response: success)
            })
        }
    }
    func handlePlan(response : Any?) {
        if let model = response as? WeeklyPlanModelStudent  {
            // print("response as? WeeklyPlanModelStudent= \(response as? WeeklyPlanList)")
            weeklyPlanArr = model.WeeklyPlanListSchool
            
            if intPaggination == 1{
                imgViewNoWeeklyPlan?.isHidden = weeklyPlanArr?.count == 0 ? false : true
                lblNoWeeklyPlan?.isHidden     = weeklyPlanArr?.count == 0 ? false : true
            }
            else {
                
            }
            self.reloadTable()
        }
    }
    
    // MARK: - UIDatePickerMethods
    func showDatePicker(){
        
        DatePickerDialog(showCancelButton: true).show(AlertConstants.SelectDate.get, doneButtonTitle:AlertConstants.Done.get , cancelButtonTitle: AlertConstants.Cancel.get, defaultDate: Date() , minimumDate: nil, maximumDate: self.sessionEndDate?.dateChangeFormatWeeklyCalendar(), datePickerMode: .date , lastText:  "") { (date) in
            self.setDateIntoLabel(dateVal : date , lastText :  self.lblSelectDate?.text)
        }
    }
    
    
    func setDateIntoLabel(dateVal : Date? ,lastText : String?) {
        if let dateVal = dateVal {
            let dateString = dateVal.dateToString(formatType :"dd MMM, yyyy")
            lblSelectDate?.text = dateString
            
            let dateString1 = dateVal.dateToString(formatType :"yyyy-MM-dd")
            strSelectDate   = dateString1
            
            weeklyPlanForStudentAPI(strPg: "\(intPaggination)", strPlanDate:strSelectDate, strSubIDs: strSubID)
        }
        else {
            let strLastText    = /lastText?.isEmpty ? "Select Date" : lastText
            lblSelectDate?.text = (strLastText == lblSelectDate?.text ) ? "Start Date" : lblSelectDate?.text
        }
    }
    
}
extension WeeklyPlanStudentViewController: SelectSubjectWeeklyDelegate {
    //MARK: - getSelectSubject
    func getSelectSubject(selectSubject : Subjects?, subjectArr : [Subjects]?) {
        // print("subjectArr?[index]. = \(subjectArr?[0].SubID)")
        let arraySelect =  self.subjectArr?.filter({ (Xyz) -> Bool in
            return Xyz.isSelected
        })
        let arraySubName =  arraySelect?.map({ (subject) -> String in
            return /subject.SubjectName
        })
        let arraySubID =  arraySelect?.map({ (subject) -> String in
            return /subject.SubID?.toString
        })
        
        lblSubject?.text = (/arraySubName?.count == 1) ? arraySubName?[0] : (/arraySubName?.count >= 2 ? "\(/arraySubName?[0]) +\(/arraySubName?.count - 1)" : "Select Subject")
        
        strSubID = /arraySubID?.count > 0 ? arraySubID?.joined(separator: ",") : ""
        
        print("strSubID = \(/strSubID)")
        print("lblSubject?.text = \( /lblSubject?.text)")
        
        if (/strSubID?.count > 0) &&  (lblSubject?.text !=  "Select Subject") {
            weeklyPlanForStudentAPI(strPg: "\(intPaggination)", strPlanDate:strSelectDate, strSubIDs: strSubID)
        }
    }
    //MARK: - getSubjectList
    func getSubjectList(classID : Int?) {
        guard let user = self.getCurrentUser() else {return}
        APIManager.shared.request(with: ECareEndPoint.getStudentSubject(UserId: Int(user.userID).toString), isLoader: true) {[weak self](response) in
            self?.handleResponse(response: response, responseBack: { (success) in
                self?.handle(response: success)
            })
        }
    }
    //MARK: - handle
    func handle(response : Any?) {
        //get subject List
        if let modal = response as? StudentListModal , let subjects = modal.subjects {
            self.subjectArr = subjects
            if /self.subjectArr?.count != 0 {
                openSelectSubjectVc(tag : 2)
            }
            else {
                Messages.shared.show(alert: .oops, message: "No subject found" , type: .warning)
            }
        }
    }
    //MARK: - openSelectSubjectVc
    func openSelectSubjectVc(tag : Int){
        fromWhere = true
        let storyboard = Storyboard.Module.getBoard()
        if let selectClassVc = storyboard.instantiateViewController(withIdentifier: "SelectSubjectViewController") as? SelectSubjectViewController {
            //  selectClassVc.classArr      = classArr
            // selectClassVc.delegate      = self
            selectClassVc.subDelegate     = self
            selectClassVc.fromLeave = false
            // selectClassVc.selectedClass = selectClass
            selectClassVc.tag             = tag
            selectClassVc.subjectArr      = self.subjectArr
            selectClassVc.selectedSubject = self.selectSubject
            self.presentVC(selectClassVc)
        }
    }
}
extension WeeklyPlanStudentViewController {
    
    func configureTableView() {
        tableDataSource  = TableViewDataSource(items: weeklyPlanArr, height: UITableView.automaticDimension, tableView: tableView, cellIdentifier: CellIdentifiers.WeeklyPlanStudentCell.get)
        
        tableDataSource?.configureCellBlock = {(cell, item, indexpath) in
            (cell as? WeeklyPlanTableViewCell)?.modal    = item
        }
        tableDataSource?.aRowSelectedListener = {(indexpath,cell) in
            self.didSelect(indexpath.row)
        }
    }
    func didSelect(_ index : Int){
        print("didSelect")
        if index < /weeklyPlanArr?.count {
            navigateToFinalPlan(wpID : /self.weeklyPlanArr?[index].wPID)
        }
    }
    func reloadTable() {
        if isFirstTime {
            configureTableView()
            isFirstTime = !isFirstTime
        }
        else {
            tableDataSource?.items = weeklyPlanArr
            tableView?.reloadData()
        }
    }
    func navigateToFinalPlan(wpID : Int) {
        fromWhere = true
        let storyboard = Storyboard.Module.getBoard()
        if let vc = storyboard.instantiateViewController(withIdentifier: "FinalDesignPlanViewController") as? FinalDesignPlanViewController{
            vc.WPID      = wpID
            vc.userRole  = .Cordinator
            vc.action    = .View
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
