import UIKit
struct  WhatsNew : Codable{
    let ErrorCode: Int
    let Message:   String
    let Status:    String
    let Total:     Int?
    let Modules: [Modules]?
    
    private enum CodingKeys: String, CodingKey {
        case ErrorCode = "ErrCode"
        case Message   = "Msg"
        case Status
        case Total
        case Modules
    }
}
struct Modules: Codable {
    
    let MdlID: Int?
    let Module: String?
    let Count: Int?
}

class WhatsNewViewController: UIViewController
{

    @IBOutlet weak var tblViewWhats     : UITableView?
    @IBOutlet weak var imgViewNoNotification: UIImageView?
    @IBOutlet weak var lblNoNotification    : UILabel?

    var arrayImg             = [String]()
    //var arrayModule          = [String:Any]()
    var strSchoolCodeSender  = String()
    var strRoleName          = String()
    var intUserType          = Int()
    var intUserID            = Int()
    
    var arrayModules = [Modules]()
   
    override func viewDidLoad() {
        super.viewDidLoad()

        if let schoolCode = self.getCurrentSchool()?.schCode {
            self.strSchoolCodeSender =  schoolCode
        }
        
        if let schoolCode = self.getCurrentUser()?.role {
            self.strRoleName =  schoolCode
        }
        
        if let userId = self.getCurrentUser()?.userID {
            self.intUserID =  Int(userId)
        }
        if let userType = self.getCurrentUser()?.userType{
            self.intUserType =  Int(userType)
        }
   }
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        arrayImg = ["assignmentsNoti","circularNoti","feeNoti","messageNoti","noticeNoti"]
        
        let parent =  self.navigationController?.parent as! CustomTabbarViewController
        parent.hideBar(boolHide: false)
        
        self.webApiNotification()
    }
    func webApiNotification()
    {
        Utility.shared.loader()
        self.tblViewWhats?.isHidden         = true
        imgViewNoNotification?.isHidden      = true
        lblNoNotification?.isHidden          = true
        self.imgViewNoNotification?.isHidden = true
        self.lblNoNotification?.isHidden     = true

        var urlString = String()
        urlString    += urlString.webAPIDomainNmae()
        if let role = self.getCurrentUser()?.role {
        urlString    += "school/Notification?key=\(urlString.keyPath())&SchCode=\(strSchoolCodeSender)&RoleName=\(role)&UserID=\(intUserID)&UserType=\(intUserType)"
        }
        print("urlString= \(urlString)")
        URLSession.shared.dataTask(with: URL(string: urlString)!) { (data, response, error) in
            
            DispatchQueue.main.async {
                
               Utility.shared.removeLoader()
                guard let jsonData = data else {return}
                let decoder = JSONDecoder()
                do{
                    let parsedObject = try decoder.decode(WhatsNew.self, from: jsonData)
                    
                    if parsedObject.ErrorCode == 0 &&  parsedObject.Status == "ok"
                    {
                        if let boardArray = parsedObject.Modules{
                            self.arrayModules = boardArray
                        }
                        else{
                            self.arrayModules = []
                        }
                        self.imgViewNoNotification?.isHidden = /(self.arrayModules.count) != 0
                        self.lblNoNotification?.isHidden     = /(self.arrayModules.count) != 0

                        self.tblViewWhats?.isHidden = false
                        self.tblViewWhats?.reloadData()
                    }
                    else{
                        AlertsClass.shared.showNativeAlert(withTitle: "", message: R.string.localize.noInternetConnectionAvailable(), fromVC: self)
                    }
                    
                }catch let jsonError{
                    print("jsonError \n \(jsonError)")
                }
            }
            }.resume()
    }
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)
     {
        if(segue.identifier == "notifiCircular"){
            let destination = segue.destination as! CircularViewController
        }
     }
}
extension WhatsNewViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayModules.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellWhatsNew", for: indexPath) as! WhatsTableViewCell
        let mdid = self.arrayModules[indexPath.row].MdlID
        
        switch /mdid {
            case 1:
                cell.imgViewModule?.image = R.image.noticeNoti()
            case 3:
                cell.imgViewModule?.image = R.image.circularNoti()
            case 12:
                cell.imgViewModule?.image = R.image.assignmentsNoti()
            case 16:
                cell.imgViewModule?.image = R.image.messageNoti()
            case 15:
                cell.imgViewModule?.image = R.image.feeNoti()
            default:
                break
        }
        //cell.imgViewModule.image = UIImage(named:arrayImg[indexPath.row])

//        cell.imgViewModule?.image  = UIImage(named:arrayImg[indexPath.row])
        cell.lblModule?.text       = self.arrayModules[indexPath.row].Module
        cell.lblNotification?.text = "\(self.arrayModules[indexPath.row].Count!)"
        cell.lblNotification?.isHidden = (/self.arrayModules[indexPath.row].Count == 0)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tblViewWhats?.deselectRow(at: indexPath, animated: true)
        let mdid = self.arrayModules[indexPath.row].MdlID
        
        switch /mdid {
        case 1:
            self.performSegue(withIdentifier: "notifiNotice", sender: self)
            NotificationCenter.default.post(name: .SELECT_UNSELECT_MENUITEM, object: nil, userInfo : ["Selection" : true , "Index" : /mdid])
        case 3:
            self.performSegue(withIdentifier: "notifiCircular", sender: self)
             NotificationCenter.default.post(name: .SELECT_UNSELECT_MENUITEM, object: nil, userInfo : ["Selection" : true , "Index" : /mdid])
        case 12:
            var intUserType          = Int()
           
            if let userType = self.getCurrentUser()?.userType{
                intUserType =  Int(userType)
            }
            if  intUserType == 3{
                self.performSegue(withIdentifier: "notifiStaffAssignment", sender: self)
            }
            else{
                self.performSegue(withIdentifier: "notifiAssignment", sender: self)
            }
        case 16:
            //(self.parent as? CustomTabbarViewController)?.selectedIndex = 3
             NotificationCenter.default.post(name: .SELECT_UNSELECT_TABBAR, object: nil, userInfo : ["Selection" : true , "Index" : 3])
           // let storyboard = UIStoryboard(name: "ComposeMessage", bundle: nil)
          //  guard let shortProfileVc = storyboard.instantiateViewController(withIdentifier: "MessageViewController") as? MessageViewController else {return}
             guard let shortProfileVc   = R.storyboard.composeMessage.messageViewController() else { return}
            self.pushVC(shortProfileVc)
        case 15:
            print("")//cell.imgViewModule.image = UIImage(named:"feeNoti")
        default:
            break
        }
    }
}
class WhatsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblModule:       UILabel?
    @IBOutlet weak var lblNotification: UILabel?
    @IBOutlet weak var imgViewModule:   UIImageView?
    override func awakeFromNib() {super.awakeFromNib()}
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //lblNotification.layer.cornerRadius = lblNotification.frame.size.height / 2
        lblNotification?.layer.cornerRadius = 13

        lblNotification?.layer.masksToBounds = true
    }
}
