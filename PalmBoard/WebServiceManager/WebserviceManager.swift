//
//  WebserviceManager.swift
//  e-Care Pro
//
//  Created by Nupur Sharma on 15/10/17.
//  Copyright © 2017 Franciscan. All rights reserved.
//

import Foundation
import CoreData
import Alamofire
import AlamofireImage

class WebserviceManager: NSObject {
   
    static func getJsonData(withParameter urlString: String, completion: @escaping (_ dictArray: [String: Any]?, _ error: Error?,_ errorcode: NSInteger?) -> Void){
        
        
        let utilityQueue = DispatchQueue.global(qos: .utility)
        var url = urlString
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let startDate = Date()
        Alamofire.request(url).responseJSON (queue: utilityQueue)
        {
            response in
            switch response.result
            {
               
                case .success(let JSON):
                let executionTime = Date().timeIntervalSince(startDate).toString
                print("API Response Time From Web service Manager :\(executionTime)")
                let jsonData = JSON as! [String : Any]
                completion(jsonData, nil, nil)
                
                case .failure(let error):
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        let err = error as NSError
                     completion(nil, err,err.code)
                }
            }
        }
    }
    
    
    class func post(urlString : String,param : [String : Any] = ["":""], completionBlock : @escaping (_ response : [String: Any]?, _ error : Error?,_ errorcode: NSInteger?)->()){
        
        //CommonClass.showLoader()
        Alamofire.request(urlString, method: HTTPMethod.post, parameters: param, encoding: JSONEncoding.default).responseJSON { (resp : DataResponse<Any>) in
           
                switch resp.result
                {
                case .success(let JSON):
                print("JSON \n \(JSON)")
                let jsonData = JSON as! [String : Any]
                print("jsonData \n \(jsonData)")
                completionBlock(jsonData, nil,nil)
                
                case .failure(let error):
                    if(error.localizedDescription == "The Internet connection appears to be offline."){
                        
                        let err = error as NSError
                        print("Please connect to the internet ==",error)
                        
                        completionBlock(nil, err,err.code)
                    }else{
                        let err = error as NSError
                        print("other problem ==",error)
                        
                        completionBlock(nil, err,err.code)
                    }
                   // completionBlock(nil, error)
                }
        }
    }

    
    //++++++++++++++++++++++++++++++++++++++++++++++ALERT++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    static func showAlert (message :String, title : String?, firstBtnText: String?, secondBtnText: String?,  firstBtnBlock:  @escaping () -> (), secondBtnBlock: @escaping () -> ())
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: firstBtnText, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
            
            firstBtnBlock()
        }));
        
        if(secondBtnText != nil){
            
            alertController.addAction(UIAlertAction(title: secondBtnText, style: UIAlertAction.Style.default, handler: {(action:UIAlertAction) in
                
                // _ = self.navigationController?.popViewController(animated: true)
                secondBtnBlock()
            }));
        }
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
 
    
    
    //++++++++++++++++++++++++++++++++++++++++++++++FetchRequest+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    static func fetchPersistentData(entityName: String?, withPredicate: NSPredicate? ) -> [NSManagedObject]{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        //Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName!, in: context)
        
        //Configure Fetch Request
        fetchRequest.entity = entityDescription
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            let searchResults = try context.fetch(fetchRequest) as! [NSManagedObject]
            print("searchResults \n \(searchResults)")
            return searchResults
            
        } catch {
            self.showAlert(message: "No Data Found", title: "", firstBtnText: "OK", secondBtnText: nil, firstBtnBlock: {}, secondBtnBlock: {})
            print("Error with request: \(error)")
        }
        return []
        
    }
}





