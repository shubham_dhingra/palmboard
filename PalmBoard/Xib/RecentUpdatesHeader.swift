//
//  RecentUpdatesHeader.swift
//  PalmBoard
//
//  Created by Ravikant Bhardwaj on 17/10/18.
//  Copyright © 2018 Franciscan. All rights reserved.
//

import UIKit

class RecentUpdatesHeader: UIView {

    class func instanceFromNib() -> UIView? {
        return UINib(nibName: "RecentUpdatesHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
}
